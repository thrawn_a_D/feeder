= Introduction =
Feeder is a feed reader able to parse rss and atom feeds. Feeder can be understood as a google reader clone. It allows the user to read, subscribe and trace incoming changes. It's implemented in java and is designed for scalable and dynamic use. This project was strictly developed using the TDD approach. It's architecture is designed highly pluggable and in most cases, independent of certain technologies. The main features of Feeder are:

* Search for feeds
* View feeds
* Pagination
* Parse a feed using a URL

== Account related features: ==
* Handle user accounts
* Sub/Unsubscribe to/from feeds
* Feed time line of all unread/read feeds

== Technical features: ==
* Developed using the TDD approach
* Unit and Component tests are executable independent of environment runtime (Services are mocked)
* Lot of small modules pluggable to an application
* Clearly defined interfaces between sub modules (REST, Messaging)

== Used technologies: ==
* Couchbase
* ElasticSearch
* Tomcat
* ActiveMQ
* GWT
* Java
* Groovy (Tests)

Basically the main project goal for me, was self-education. In order to recapitalise certain techniques, it was really important to face some "real world" challenges.

== Further topics ==
[[Build and deploy]]
