#### One man show #####

When developing a project on your own, there are not a lot of agile aspects to it. You create an idea of a product, write a couple requirements and start writing the first lines of code. I did not plan a lot, didn't write tasks, nor did I had a burn down chart. My work intensity on feeder varied a lot, depending on my personal desire, current work load or simply other daily time demands. So I didn't had a time schedule. The journey was my reward. And it worked out pretty well so far. I've done a couple of features, learned a ton of stuff and extracted personal benefits out of that knowledge. But there are also some downsides of being a one man show. It's difficult or even impossible to comply some of the basic features of software development. You constantly need to remind yourself to do certain things (like write a comment now and then, test first ...), as nobody will remind you doing so. There is a clear benefit of teamwork missing out when developing on your own. A group of clever minds, each of which has a different view and another skill to contribute. Just to give you an imagination of some key points that I missed the most:

- Reviews (It's one of the most important things to consider while writing some code. It's very helpful to have someone challenging your thoughts. By discussing issues upfront, you can save yourself a lot of trouble.)
- Challenge (test) your implementation (It's quite useful to have someone writing acceptance tests, while another person is developing the production code. Not only that you're faster, but you're also a lot more safe. Two people have oftentimes different perspectives on certain things. It's like a review in some regard, but it's also an additional perspective which develops parallel to your personal one.)
- Push your product and stay focused (While developing feeder on my own, I oftentimes had to motivate myself to get behind the desk and do some work. I oftentimes didn't had the drive to work. This can be the cause of death for a project. It's fine as long as you gain something out of it, but can be crucial if you've haven't reached your goals yet.)

But not everything is bad when developing code on your own. You're free to do whatever you like. No one forces you to do certain things. No one tries to push you in a certain direction. You're your own master mind. You get to decide what approach to follow, what technique to use and how to do certain things. There is one chapter I remember reading in [The Pragmatic Programmer from Andrew Hunt, David Thomas and Ward Cunningham] where they recommend to invest time in your personal knowledge portfolio. Doing so, not only means that you have to read and discuss some things, but also that you implement that logic in a real environment. Oftentimes it is not possible to apply that knowledge in your daily project (due to technical, social or business constraints). So it's very helpful to have a place you can call your own. A little project where you can play around with some of the ideas you catch up.

#### Develop for future ####

Knowing a project is supposed to live for a while, I needed to do my best and write it as clean as possible. I wanted to invest as much time as needed to create a clean codebase. This is necessary to stay fast, constantly delivering new features. This is something the XP movement is preaching us: "Invest some effort writing clean, understandable code and you'll be able to stay fast." This is obviously not an easy task. There are several issues to consider. TDD is helping a lot to push you in the right direction. I constantly tried to use that approach and I don't want to miss it any more. It's very important. After reading [Agile Software Development. Principles, Patterns, and Practices by Robert C. Martin aka Uncle Bob], I realised that there is a lot more to it than TDD. We need to package our software wisely, name our methods properly and design our code based on the solid principles.

------- INFO-BOX Solid principles -----------------------------------------------
S    SRP  Single responsibility principle. A class should have only a single responsibility.

O    OCP  Open/closed principle. Software entities … should be open for extension, but closed for modification.

L    LSP  Liskov substitution principle. Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program. See also design by contract.

I    ISP  Interface segregation principle. Many client-specific interfaces are better than one general-purpose interface.

D    DIP  Dependency inversion principle. One should “Depend upon Abstractions. Do not depend upon concretions.”
---------------------------------------------------------------------------------

I tried to follow those principles as strictly as possible. If you take a look at my code, you'd catch some of the classes being design following the OCP. Take a peek at the GlobalSearchRequestProcessor for instance. It's located at the feeder client and is supposed to delegate incoming search requests to a rest service. The class itself is kept fairly small. We only have two methods: one that provides a mapping class for incoming parameters, and the second one is telling us which command is supposed to be called on the feeder. The rest of the logic is kept generic and is located in the AbstractSearchTemplate. Those two methods are the only thing that differentiates that class from all the other classes deriving from the AbstractSearchTemplate. If we need an additional command to be processed, we add a new class, instead of readjust already written code. It's adding instead of changing, that helps you keep things stable.

************ Diagram to show the OCP **************

As you can see, we actually have two abstract classes involved before we add a concrete class as realization. Those little classes implement little peaces of functionality and allow us to be more and more specific with every step of abstraction. I use this pattern a lot in feeder and it works quite well.

#### Know what's important ####
Starting a project like feeder, I defined several goals to achieve. I wanted to learn, gain some experience, create a useful product and get to know some basic and new frameworks/techniques out there. But when I started to write some code, I needed to concentrate on the essential: the use cases. All the other stuff is implicit and will come eventually. Uncle Bob advocates that the use cases should be the first thing a developer is confronted with. That's what I tried to achieve. I started writing classes that handled a specific use case, independent of certain technologies. But that's not an easy task if you work on a project that requires feeds to be loaded from the internet. That is actually your use case. Nevertheless it does not contradict itself that all the details (like the technical part of loading a feed) should be hidden in the lower regions of code, so that we fully concentrate on our use case. Let's take the feed subscription handler as an example. It contains two methods; one is to subscribe to a feed and the other method handles subscription canceling. What that code describes are basically use cases. We're not getting too technical here. I would say that most developers would know what happens there just by looking at the code.

----------- Code block ----------------------------------------------------------
    @Override
    public void subscribeToFeed(final FeedSubscriptionHandling feedSubscriptionHandle) {
        final String feedUrl = feedSubscriptionHandle.getFeedUrl();
        final String userId = feedSubscriptionHandle.getUserId();
        LOGGER.debug("###########################################################################");
        LOGGER.debug("# Subscribe to feed: {} by userId: {}", feedUrl, userId);
        LOGGER.debug("###########################################################################");

        ensureFeedIsLocallyAvailable(feedUrl, userId);

        FeedToUserAssociation association = new FeedToUserAssociation();
        association.setFeedUrl(feedUrl);
        association.setUserId(userId);
        association.setAssociationType(AssociationType.SUBSCRIBER);

        addFeedColor(association);

        useCaseStorageProvider.getFeedToUserAssociationStorage().save(association);

        LOGGER.debug("Association of feed {} to user {} has been set.", feedUrl, userId);
    }

    @Override
    public void unsubscribeFeed(final FeedSubscriptionHandling feedSubscriptionHandle) {
        final String feedUrl = feedSubscriptionHandle.getFeedUrl();
        final String userId = feedSubscriptionHandle.getUserId();
        boolean isAssociationOfTypeSubscription = useCaseStorageProvider.getFeedToUserAssociationStorage().isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER);
        if(isAssociationOfTypeSubscription){
            useCaseStorageProvider.getFeedToUserAssociationStorage().removeAssociation(feedUrl, userId);
            feederRepositoryProvider.getFeedRepository().informThatUserCanceledSubscription(feedUrl);
        }
    }
----------------------------------------------------------------------------------

To keep a level of abstraction is a constant challenge. You need to review your code, asking yourself if it's legibly enough. Rereading my own code, after a period of time, helps a lot to decide if my own code describes a subject so, that I immediately understand what it does. But at some point it's inevitable that we write technical code. But we should try to suspend that as long as possible, doing it in small steps.

###### Let's grow and prosper ######

My first steps at the beginning were fairly small. I wrote some use cases without thinking too much about some of the less important parts. Those less important parts can include non functional requirements. One of my early mentors always said: "Try to make it work, evaluate the use case, adjust things when needed and only if everyone is happy, we proceed.". That's the kind of premise we need to avoid; developing into the dark. I remember working on projects for weeks, implementing features, setting up all the architecture, developing deployment strategies, writing manuals, only to find out that we missed our customer needs. We missed an important part, something that thousands of projects before us attribute their failure to. We didn't reevaluate our implementation. We haven't had a customer to take a look at it and decide if that's truly what he needs. There has been written thousands of books about this misery and I wanted to avoid this trap.

I concentrated on my use cases first. Developed something I can take a look at, play with it and maybe I'll throw it away. By skipping all the stuff that increase complexity, costs time to develop and is an additional source of potential failure, I was able to focus on my features. I developed my use case library (I also mention this in my architecture part), which contains all the customer related stuff: load feeds, subscribe, search for content, and so on. To actually feel this functionality come into play, I developed a small UI. This was my interface to the actual feed implementation. Using GWT for this task, I was able to include my simple java library on the server side, without taking care of the delivery mechanism like REST or SOAP. I didn't even had a data storage. I simply used my file system. An initial task of many projects is to create a database schema, representing the real world requirements. But actually it's not necessary to do so at the beginning. It depends on the project of course, but trying to postpone that task will give you the time to play with your model, rethink relations, entities and attributes, without being punished for every change you make. The NoSql movement is a good step in the right direction, but even then, it's sometimes easier to go with the most simple solution at first.

After a couple of use cases are finally done, I started to take care of the non functional stuff. Introduce delivery protocols, storage systems, clients. It can take a whole bunch of time to deliver all this stuff. So it's not a bad idea to mix this stuff with some stories that actually have a direct benefit for the customer. Doing so, you will always have something new to show to your client, and at the same time you can take care of the back end stuff too. It's a constant growth, based on the strong fundamental decisions, made by every person involved. Constant reevaluation of your implementation, your approach to daily work, your use cases and customer needs, is a big step to a more stable and successful project.
