package de.wsorg.feeder.processor.api.domain.response.feed.opml

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.10.12
 */
class OutlineTest extends Specification {
    private static final String METADATA_SEPARATOR_TAG = "@#@#@"
    private static final String IS_FAVOURED_META_TAG = "IS_FAVOURED"
    private static final String COLOR_META_TAG = "FEED_UI_HIGHLIGHTING_COLOR"
    private static final String META_TAG_SEPARATOR = ":"

    def "Extract description from a description enriched with metadata"() {
        given:
        def plainDescription = 'test descrioption'
        def enrichedDescription = plainDescription +
                METADATA_SEPARATOR_TAG +
                IS_FAVOURED_META_TAG +
                META_TAG_SEPARATOR +
                true +
                METADATA_SEPARATOR_TAG
        def outline = new Outline(description: enrichedDescription)

        when:
        def receivedDescription = outline.getDescription()

        then:
        receivedDescription == plainDescription
    }

    def "Extract isFavoured from enriched description"() {
        given:
        def plainDescription = 'test descrioption'
        def givenIsFavoured = true
        def enrichedDescription = plainDescription +
                METADATA_SEPARATOR_TAG +
                IS_FAVOURED_META_TAG +
                META_TAG_SEPARATOR +
                givenIsFavoured +
                METADATA_SEPARATOR_TAG
        def outline = new Outline(description: enrichedDescription)

        when:
        def receivedFavoured = outline.isFavoured()

        then:
        receivedFavoured == givenIsFavoured
    }

    def "Extract color from enriched description"() {
        given:
        def plainDescription = 'test descrioption'
        def givenColor = '#kuh2sdf'
        def enrichedDescription = plainDescription +
                METADATA_SEPARATOR_TAG +
                COLOR_META_TAG +
                META_TAG_SEPARATOR +
                givenColor +
                METADATA_SEPARATOR_TAG
        def outline = new Outline(description: enrichedDescription)

        when:
        def receivedColor = outline.getFeedUiHighlightingColor()

        then:
        receivedColor == givenColor
    }

    def "Keep description as is if no metadata is set"() {
        given:
        def plainDescription = 'test descrioption'
        def outline = new Outline(description: plainDescription)


        when:
        def receivedDescription = outline.getDescription()

        then:
        receivedDescription == plainDescription
    }
}
