package de.wsorg.feeder.processor.api.domain.request.global;


import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.09.12
 */
public class GlobalSearchQueryRequestTest extends Specification {

//    def "Add restriction to the internal map"() {
//        given:
//        GlobalSearchQueryRequest searchQueryRequest = new GlobalSearchQueryRequest()
//        GlobalSearchRestriction searchRestriction = GlobalSearchRestriction..USER_ID
//        def value = "sdf"
//
//        when:
//        searchQueryRequest.addRestriction(searchRestriction, value)
//
//        then:
//        searchQueryRequest.searchRestrictions.containsKey(searchRestriction)
//        searchQueryRequest.searchRestrictions.containsValue(value)
//    }
//
//    def "Add restriction to the internal map based on String as restriction type"() {
//        given:
//        FeederSearchQueryRequest searchQueryRequest = new FeederSearchQueryRequest()
//        String searchRestriction = FeederSearchRestriction.USER_ID.name()
//        def value = "sdf"
//
//        when:
//        searchQueryRequest.addRestriction(searchRestriction, value)
//
//        then:
//        searchQueryRequest.searchRestrictions.containsKey(FeederSearchRestriction.USER_ID)
//        searchQueryRequest.searchRestrictions.containsValue(value)
//    }

}
