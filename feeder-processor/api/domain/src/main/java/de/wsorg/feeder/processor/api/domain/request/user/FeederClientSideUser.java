package de.wsorg.feeder.processor.api.domain.request.user;

import de.wsorg.feeder.processor.api.domain.response.Response;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeederClientSideUser extends FeederUseCaseUser implements Response {

    public FeederClientSideUser(){}

    public FeederClientSideUser(final FeederUseCaseUser feederUseCaseUser) {
        super(feederUseCaseUser.getUserId(),
              feederUseCaseUser.getUserName(),
              feederUseCaseUser.getFirstName(),
              feederUseCaseUser.getLastName(),
              feederUseCaseUser.getPassword(),
              feederUseCaseUser.getEMail());
    }
}
