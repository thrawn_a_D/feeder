package de.wsorg.feeder.processor.api.domain.request.feed.search.user;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserRelatedSearchQuery;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserRelatedSearchQueryRequest extends UserRelatedSearchQuery implements Request {
}
