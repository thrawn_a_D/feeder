package de.wsorg.feeder.processor.api.domain.request.user.create;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class CreateUserRequest implements Request {
    private FeederClientSideUser userToBeHandled;
}
