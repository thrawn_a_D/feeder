package de.wsorg.feeder.processor.api.domain.request.user.validation;

import de.wsorg.feeder.processor.api.domain.request.Request;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class UserLoginRequest implements Request {
    private String nickName;
    private String password;
}
