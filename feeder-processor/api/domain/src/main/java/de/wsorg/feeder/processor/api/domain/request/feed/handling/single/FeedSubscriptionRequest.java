package de.wsorg.feeder.processor.api.domain.request.feed.handling.single;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.FeedSubscriptionHandling;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class FeedSubscriptionRequest extends FeedSubscriptionHandling implements Request {
    private boolean favourFeed;
}
