package de.wsorg.feeder.processor.api.domain.request.feed.load;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import lombok.Data;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class LoadFeedRequest extends LoadFeed implements Request, Serializable {
}
