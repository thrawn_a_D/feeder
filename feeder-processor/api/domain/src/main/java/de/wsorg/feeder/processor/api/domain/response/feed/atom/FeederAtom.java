package de.wsorg.feeder.processor.api.domain.response.feed.atom;

import de.wsorg.feeder.processor.api.domain.response.Response;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class FeederAtom extends StandardFeed implements Response {
    protected boolean isFavoured;
    protected boolean additionalPagingPossible;
    protected String feedUiHighlightingColor;
}
