package de.wsorg.feeder.processor.api.domain.response.feed.opml;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class Outline implements Serializable {
    private static final String METADATA_SEPARATOR_TAG = "@#@#@";
    private static final String IS_FAVOURED_META_TAG = "IS_FAVOURED";
    private static final String COLOR_META_TAG = "FEED_UI_HIGHLIGHTING_COLOR";
    private static final String META_TAG_SEPARATOR = ":";
    private static final String IS_FAVOURED_META_TAG_WITH_SEPARATOR = IS_FAVOURED_META_TAG + META_TAG_SEPARATOR;
    private static final String COLOR_META_TAG_WITH_SEPARATOR = COLOR_META_TAG + META_TAG_SEPARATOR;

    protected String title;
    protected String description;
    protected String xmlUrl;
    protected boolean isFavoured;
    protected String feedUiHighlightingColor;

    public void setDescription(final String description){
        if(StringUtils.isNotBlank(description) && description.endsWith(METADATA_SEPARATOR_TAG)){
            extractDescription(description);
            extractIsFavourite(description);
            extractColor(description);
        } else {
            this.description = description;
        }
    }

    private void extractIsFavourite(final String description) {
        if(description.contains(IS_FAVOURED_META_TAG)){
            String isFavouredString = StringUtils.substringAfter(description, IS_FAVOURED_META_TAG_WITH_SEPARATOR);
            isFavouredString = StringUtils.substringBefore(isFavouredString, METADATA_SEPARATOR_TAG);
            this.isFavoured = Boolean.valueOf(isFavouredString);
        }
    }

    private void extractColor(final String description) {
        if(description.contains(COLOR_META_TAG)){
            String colorString = StringUtils.substringAfter(description, COLOR_META_TAG_WITH_SEPARATOR);
            feedUiHighlightingColor = StringUtils.substringBefore(colorString, METADATA_SEPARATOR_TAG);
        }
    }

    private void extractDescription(final String description) {
        final String actualDescription;
        actualDescription = StringUtils.substringBefore(description, METADATA_SEPARATOR_TAG);
        this.description = actualDescription;
    }
}
