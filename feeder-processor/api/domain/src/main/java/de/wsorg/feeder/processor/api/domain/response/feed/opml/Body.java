package de.wsorg.feeder.processor.api.domain.response.feed.opml;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class Body implements Serializable {
    private List<Outline> searchResultList = new ArrayList<Outline>();

    public void addSearchResult(final Outline currentOutline){
        searchResultList.add(currentOutline);
    }
}
