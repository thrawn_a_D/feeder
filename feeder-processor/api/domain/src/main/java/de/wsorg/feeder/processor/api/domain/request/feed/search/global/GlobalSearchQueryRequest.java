package de.wsorg.feeder.processor.api.domain.request.feed.search.global;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GlobalSearchQueryRequest extends GlobalSearchQuery implements Request, Serializable {
}
