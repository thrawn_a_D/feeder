package de.wsorg.feeder.processor.api.domain;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum FeederCommand {
    /** Search Actions **/
    GLOBAL_SEARCH,
    GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA,
    USER_RELATED_SEARCH,

    /** Load Actions **/
    LOAD,
    LOAD_USER_RELATED,

    /** Handling Actions **/
    FAVOUR_FEED,
    MARK_FEED_ENTRY_READABILITY,
    MARK_ALL_FEEDS_READABILITY,

    /** User related Actions **/
    CREATE_USER,
    HANDLE_USER,
    LOGIN_USER
}
