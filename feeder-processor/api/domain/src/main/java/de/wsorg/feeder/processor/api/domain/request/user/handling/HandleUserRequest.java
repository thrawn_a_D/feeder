package de.wsorg.feeder.processor.api.domain.request.user.handling;

import org.springframework.beans.BeanUtils;

import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.processor.production.domain.user.HandledUseCaseUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class HandleUserRequest extends HandledUseCaseUser implements Request, AuthorizedRequest {
    private UserHandlingType handlingType;

    public void setUserToBeHandled(final FeederClientSideUser handleUserRequest) {
        BeanUtils.copyProperties(handleUserRequest, this);
    }
}
