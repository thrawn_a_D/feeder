package de.wsorg.feeder.processor.api.domain.response.result;

import de.wsorg.feeder.processor.api.domain.response.Response;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class GenericResponseResult implements Response {
    private ProcessingResult processingResult;
    private String message;
}
