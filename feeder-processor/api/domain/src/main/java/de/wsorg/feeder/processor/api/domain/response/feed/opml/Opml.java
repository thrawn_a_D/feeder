package de.wsorg.feeder.processor.api.domain.response.feed.opml;

import de.wsorg.feeder.processor.api.domain.response.Response;
import lombok.Data;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class Opml implements Response, Serializable {
    private Body body;
}
