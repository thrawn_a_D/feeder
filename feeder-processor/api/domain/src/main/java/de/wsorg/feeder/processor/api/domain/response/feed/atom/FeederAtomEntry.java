package de.wsorg.feeder.processor.api.domain.response.feed.atom;

import de.wsorg.feeder.processor.api.domain.response.Response;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class FeederAtomEntry extends StandardFeedEntry implements Response {
    protected boolean isEntryAlreadyRead;
    protected String feedUiHighlightingColor;
}
