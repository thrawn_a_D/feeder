package de.wsorg.feeder.processor.api.domain.response;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface Response extends Serializable {
}
