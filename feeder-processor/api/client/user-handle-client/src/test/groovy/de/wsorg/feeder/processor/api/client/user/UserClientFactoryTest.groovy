package de.wsorg.feeder.processor.api.client.user;


import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.09.12
 */
public class UserClientFactoryTest extends Specification {

    @Shared
    SimpleFeederClientConfiguration config

    def setup(){
        UserClientFactory.configProvided = false
    }

   def setupSpec() {
       config = new SimpleFeederClientConfiguration()
   }

    def "Get user handling processor"() {
        given:
        UserClientFactory.setClientConfig(config)


        when:
        def result = UserClientFactory.getUserHandler()

        then:
        result != null
        result.getRequestToTransferMapper() != null
        result.getXmlToDomainMapper() != null
    }

    def "Get user creation processor"() {
        given:
        UserClientFactory.setClientConfig(config)


        when:
        def result = UserClientFactory.getUserCreator()

        then:
        result != null
        result.getRequestToTransferMapper() != null
        result.getXmlToDomainMapper() != null
    }

    def "Check if configuration was set properly when getting user handler"() {

        when:
        UserClientFactory.getUserHandler()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get user validation processor"() {
        given:
        UserClientFactory.setClientConfig(config)

        when:
        def validationProcessor = UserClientFactory.getUserValidator()

        then:
        validationProcessor != null
        validationProcessor.getRequestToTransferMapper() != null
        validationProcessor.getXmlToDomainMapper() != null
    }

    def "Check if configuration was set properly when getting user validator"() {

        when:
        UserClientFactory.getUserValidator()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Check that config was inserted into app context"() {
        when:
        UserClientFactory.setClientConfig(config)

        then:
        def usedConfig = UserClientFactory.applicationContext.getBean(FeederClientConfiguration)
        usedConfig == config
    }
}
