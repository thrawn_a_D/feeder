package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.response.concrete

import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.AbstractUserType
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.FeederUserType
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class JaxbUserPojoToFeedDomainMapperTest extends Specification {
    JaxbUserPojoToFeedDomainMapper jaxbUserPojoToFeedDomainMapper

    def setup(){
        jaxbUserPojoToFeedDomainMapper = new JaxbUserPojoToFeedDomainMapper()
    }

    def "Map jaxb user pojo back to feeder pojo"() {
        given:
        FeederUserType jaxbUser = new FeederUserType()
        jaxbUser.feederUser = new AbstractUserType()
        jaxbUser.feederUser.EMail = 'eMail'
        jaxbUser.feederUser.firstName = 'first'
        jaxbUser.feederUser.lastName = 'last'
        jaxbUser.feederUser.userId = 'id'
        jaxbUser.feederUser.userName = 'nick'
        jaxbUser.feederUser.password = 'pass'

        when:
        def mappedUser = jaxbUserPojoToFeedDomainMapper.mapJaxbPojoToFeederPojo(jaxbUser)

        then:
        mappedUser.userName == jaxbUser.feederUser.userName
        mappedUser.firstName == jaxbUser.feederUser.firstName
        mappedUser.lastName == jaxbUser.feederUser.lastName
        mappedUser.EMail == jaxbUser.feederUser.EMail
        mappedUser.password == jaxbUser.feederUser.password
        mappedUser.userId == jaxbUser.feederUser.userId
    }
}
