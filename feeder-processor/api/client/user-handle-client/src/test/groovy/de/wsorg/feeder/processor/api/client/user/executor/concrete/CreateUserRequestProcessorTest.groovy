package de.wsorg.feeder.processor.api.client.user.executor.concrete

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.06.13
 */
class CreateUserRequestProcessorTest extends Specification {
    CreateUserRequestProcessor createUserRequestProcessor

    def setup(){
        createUserRequestProcessor = new CreateUserRequestProcessor()
    }

    def "Ensure the right request mapper is provided"() {
        given:
        RequestToXmlMapper requestToXmlMapper = Mock(RequestToXmlMapper)
        createUserRequestProcessor.requestToXmlMapper = requestToXmlMapper


        when:
        def mapper = createUserRequestProcessor.requestToTransferMapper

        then:
        mapper == requestToXmlMapper
    }

    def "Ensure the right response mapper is provided"() {
        given:
        XmlToDomainMapper responseToResultMapper = Mock(XmlToDomainMapper)
        createUserRequestProcessor.xmlToDomainMapper = responseToResultMapper


        when:
        def mapper = createUserRequestProcessor.xmlToDomainMapper

        then:
        mapper == responseToResultMapper
    }
}
