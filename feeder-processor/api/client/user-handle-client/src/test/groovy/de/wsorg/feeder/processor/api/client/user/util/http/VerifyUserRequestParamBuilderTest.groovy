package de.wsorg.feeder.processor.api.client.user.util.http

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.02.13
 */
class VerifyUserRequestParamBuilderTest extends Specification {
    VerifyUserRequestParamBuilder verifyUserRequestParamBuilder
    ConfigurationProvider configurationProvider

    def setup(){
        verifyUserRequestParamBuilder = new VerifyUserRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        verifyUserRequestParamBuilder.configurationProvider = configurationProvider
    }

    def "Get creation request"() {
        given:
        def nickName = 'kjb'
        def password = 'asdwq'
        def requestParamMap = [nickName: nickName, password: password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        HttpGet getRequest = verifyUserRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "user/${nickName}?password=${password}"
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
    }

    def "Disallow spaces in passwords"() {
        given:
        def nickName = 'kjb'
        def password = 'as asdd dwq'
        def requestParamMap = [nickName: nickName, password: password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        verifyUserRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided user data contains invalid characters.'
    }
}
