package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.response.concrete

import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class UserResponseToResultMapperTest extends Specification {
    UserXmlToDomainMapper userResponseToResultMapper

    def setup(){
        userResponseToResultMapper = new UserXmlToDomainMapper()
    }

    def "Make sure the right jaxb to feed pojo mapper is provided"() {
        given:
        JaxbPojoToFeederPojoMapper mapper = Mock(JaxbPojoToFeederPojoMapper)
        userResponseToResultMapper.jaxbPojoToFeederPojoMapper = mapper

        when:
        def result=userResponseToResultMapper.jaxbPojoToFeederPojoMapper

        then:
        result == mapper
    }

    def "Make sure the right jaxb package is provided"() {
        when:
        def usedPackage = userResponseToResultMapper.generatedJaxbClassesPackage

        then:
        usedPackage == "de.wsorg.feeder.processor.api.domain.generated.schema.request.user"
    }
}
