package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request;


import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.HandleUserRequestType
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.09.12
 */
public class HandleUserRequestMapperTest extends Specification {
    HandleUserRequestMapper handleUserRequestMapper
    JaxbMarshaller jaxbMarshallerMock

    def setup(){
        handleUserRequestMapper = new HandleUserRequestMapper()
        jaxbMarshallerMock = Mock(JaxbMarshaller)
        handleUserRequestMapper.jaxbMarshaller = jaxbMarshallerMock
    }

    def "Map a request to jaxb pojo and provide it to jaxb"() {
        given:
        HandleUserRequest usedUserRequest = getUserCreationRequestModel()

        and:
        def expectedResult = ""

        when:
        def xmlResult = handleUserRequestMapper.getRequestData(usedUserRequest);

        then:
        xmlResult == expectedResult
        1 * jaxbMarshallerMock.marshal({HandleUserRequestType it -> validateMappedObject(it, usedUserRequest) }) >> expectedResult
    }

    def "Check that an appropriate jaxb namespace is set"() {
        given:
        HandleUserRequest usedUserRequest = getUserCreationRequestModel()

        when:
        handleUserRequestMapper.getRequestData(usedUserRequest)

        then:
        1 * jaxbMarshallerMock.setJaxbGeneratedClassesPackage("de.wsorg.feeder.processor.api.domain.generated.schema.request.user")
    }

    private boolean validateMappedObject(HandleUserRequestType it, HandleUserRequest usedUserRequest) {
        it.requestType.name() == usedUserRequest.handlingType.name() &&
                it.user.firstName == usedUserRequest.firstName &&
                it.user.lastName == usedUserRequest.lastName &&
                it.user.userName == usedUserRequest.userName &&
                it.user.EMail == usedUserRequest.EMail &&
                it.user.password == usedUserRequest.password &&
                it.user.userId == usedUserRequest.userId
    }

    private HandleUserRequest getUserCreationRequestModel() {
        def feederUser = getFeederUserWithAllAttributes()
        HandleUserRequest usedUserRequest = new HandleUserRequest(
                handlingType: UserHandlingType.UPDATE,
                userToBeHandled: feederUser
        )
        usedUserRequest
    }

    private FeederUseCaseUser getFeederUserWithAllAttributes() {
        FeederClientSideUser feederUser = new FeederClientSideUser()
        feederUser.EMail = "bla@bla.com"
        feederUser.firstName = "firstNameValue"
        feederUser.lastName = "lastNameValue"
        feederUser.userName = "nichNameValue"
        feederUser.password = "passwordValue"
        feederUser.userId = "12345"
        return feederUser;
    }
}
