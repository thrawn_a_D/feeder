package de.wsorg.feeder.processor.api.client.user.executor.concrete;


import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.09.12
 */
public class HandleUserRequestProcessorTest extends Specification {
    HandleUserRequestProcessor handleUserRequestProcessor

    def setup(){
        handleUserRequestProcessor = new HandleUserRequestProcessor()
    }

    def "Ensure the right request mapper is provided"() {
        given:
        RequestToXmlMapper requestToXmlMapper = Mock(RequestToXmlMapper)
        handleUserRequestProcessor.requestToXmlMapper = requestToXmlMapper


        when:
        def mapper = handleUserRequestProcessor.requestToTransferMapper

        then:
        mapper == requestToXmlMapper
    }

    def "Ensure the right response mapper is provided"() {
        given:
        XmlToDomainMapper responseToResultMapper = Mock(XmlToDomainMapper)
        handleUserRequestProcessor.xmlToDomainMapper = responseToResultMapper


        when:
        def mapper = handleUserRequestProcessor.xmlToDomainMapper

        then:
        mapper == responseToResultMapper
    }

    def "Ensure appropriate command is given"() {
        given:
        FeederCommand commandToExecute = FeederCommand.HANDLE_USER

        when:
        def usedCommand = handleUserRequestProcessor.commandToExecute

        then:
        usedCommand == commandToExecute
    }
}
