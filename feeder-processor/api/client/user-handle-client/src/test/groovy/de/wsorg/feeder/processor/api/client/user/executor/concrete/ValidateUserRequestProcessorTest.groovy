package de.wsorg.feeder.processor.api.client.user.executor.concrete

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class ValidateUserRequestProcessorTest extends Specification {
    ValidateUserRequestProcessor validateUserRequestProcessor

    def setup(){
        validateUserRequestProcessor = new ValidateUserRequestProcessor()
    }

    def "Ensure the right request mapper is provided"() {
        given:
        RequestToStringMapMapper requestToXmlMapper = Mock(RequestToStringMapMapper)
        validateUserRequestProcessor.requestToXmlMapper = requestToXmlMapper


        when:
        def mapper = validateUserRequestProcessor.requestToTransferMapper

        then:
        mapper == requestToXmlMapper
    }

    def "Ensure the right response mapper is provided"() {
        given:
        XmlToDomainMapper responseToResultMapper = Mock(XmlToDomainMapper)
        validateUserRequestProcessor.xmlToDomainMapper = responseToResultMapper


        when:
        def mapper = validateUserRequestProcessor.xmlToDomainMapper

        then:
        mapper == responseToResultMapper
    }

    def "Ensure appropriate command is given"() {
        given:
        FeederCommand commandToExecute = FeederCommand.LOGIN_USER

        when:
        def usedCommand = validateUserRequestProcessor.commandToExecute

        then:
        usedCommand == commandToExecute
    }
}
