package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request

import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class UserLoginRequestToXmlMapperTest extends Specification {
    UserLoginRequestToXmlMapper userLoginRequestToXmlMapper

    def setup(){
        userLoginRequestToXmlMapper = new UserLoginRequestToXmlMapper()
    }

    def "Map user login request to xml"() {
        given:
        UserLoginRequest loginRequest = new UserLoginRequest(nickName: 'nickName',
                                                             password: 'password')

        when:
        def result = userLoginRequestToXmlMapper.getRequestData(loginRequest)

        then:
        result.nickName == loginRequest.nickName
        result.password == loginRequest.password
    }
}
