package de.wsorg.feeder.processor.api.client.user.util.http

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder
import de.wsorg.feeder.processor.api.domain.FeederCommand
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.02.12
 */
class UserHttpRequestParamBuilderResolverTest extends Specification {

    UserHttpRequestParamBuilderResolver userHttpRequestParamBuilderResolver
    HttpRequestParamBuilder createUserHttpRequestBuilder = Mock(HttpRequestParamBuilder)
    HttpRequestParamBuilder handleUserHttpRequestBuilder = Mock(HttpRequestParamBuilder)
    HttpRequestParamBuilder verifyUserHttpRequestBuilder = Mock(HttpRequestParamBuilder)

    def setup(){
        userHttpRequestParamBuilderResolver = new UserHttpRequestParamBuilderResolver()
        userHttpRequestParamBuilderResolver.createUserHttpRequestBuilder = createUserHttpRequestBuilder
        userHttpRequestParamBuilderResolver.handleUserHttpRequestBuilder = handleUserHttpRequestBuilder
        userHttpRequestParamBuilderResolver.verifyUserHttpRequestBuilder = verifyUserHttpRequestBuilder
    }

    def "Get http request builder for user creation"() {
        when:
        def result = userHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(FeederCommand.CREATE_USER)

        then:
        result == createUserHttpRequestBuilder
    }

    def "Get http request builder for user handling"() {
        when:
        def result = userHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(FeederCommand.HANDLE_USER)

        then:
        result == handleUserHttpRequestBuilder
    }

    def "Get http request builder for user validation"() {
        when:
        def result = userHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(FeederCommand.LOGIN_USER)

        then:
        result == verifyUserHttpRequestBuilder
    }

    def "No request builder found"() {
        when:
        userHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(FeederCommand.LOAD)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'No http parameter builder could be found to execute the command: ' + FeederCommand.LOAD
    }
}
