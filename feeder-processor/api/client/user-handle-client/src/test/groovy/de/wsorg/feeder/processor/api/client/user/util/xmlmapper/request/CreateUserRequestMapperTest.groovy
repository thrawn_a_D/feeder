package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.06.13
 */
class CreateUserRequestMapperTest extends Specification {
    CreateUserRequestMapper createUserRequestMapper
    JaxbMarshaller jaxbMarshallerMock

    def setup(){
        createUserRequestMapper = new CreateUserRequestMapper()
        jaxbMarshallerMock = Mock(JaxbMarshaller)
        createUserRequestMapper.jaxbMarshaller = jaxbMarshallerMock
    }

    def "Map a request to jaxb pojo and provide it to jaxb"() {
        given:
        CreateUserRequest usedUserRequest = getUserCreationRequestModel()

        and:
        def expectedResult = ""

        when:
        def xmlResult = createUserRequestMapper.getRequestData(usedUserRequest);

        then:
        xmlResult == expectedResult
        1 * jaxbMarshallerMock.marshal({de.wsorg.feeder.processor.api.domain.generated.schema.request.user.CreateUserRequestType it -> validateMappedObject(it, usedUserRequest) }) >> expectedResult
    }

    def "Check that an appropriate jaxb namespace is set"() {
        given:
        CreateUserRequest usedUserRequest = getUserCreationRequestModel()

        when:
        createUserRequestMapper.getRequestData(usedUserRequest)

        then:
        1 * jaxbMarshallerMock.setJaxbGeneratedClassesPackage("de.wsorg.feeder.processor.api.domain.generated.schema.request.user")
    }

    private boolean validateMappedObject(de.wsorg.feeder.processor.api.domain.generated.schema.request.user.CreateUserRequestType it, CreateUserRequest usedUserRequest) {
                it.user.firstName == usedUserRequest.userToBeHandled.firstName &&
                it.user.lastName == usedUserRequest.userToBeHandled.lastName &&
                it.user.userName == usedUserRequest.userToBeHandled.userName &&
                it.user.EMail == usedUserRequest.userToBeHandled.EMail &&
                it.user.password == usedUserRequest.userToBeHandled.password &&
                it.user.userId == usedUserRequest.userToBeHandled.userId
    }

    private CreateUserRequest getUserCreationRequestModel() {
        def feederUser = getFeederUserWithAllAttributes()
        CreateUserRequest usedUserRequest = new CreateUserRequest(
                userToBeHandled: feederUser
        )
        usedUserRequest
    }

    private FeederUseCaseUser getFeederUserWithAllAttributes() {
        FeederClientSideUser feederUser = new FeederClientSideUser()
        feederUser.EMail = "bla@bla.com"
        feederUser.firstName = "firstNameValue"
        feederUser.lastName = "lastNameValue"
        feederUser.userName = "nichNameValue"
        feederUser.password = "passwordValue"
        feederUser.userId = "12345"
        return feederUser;
    }
}
