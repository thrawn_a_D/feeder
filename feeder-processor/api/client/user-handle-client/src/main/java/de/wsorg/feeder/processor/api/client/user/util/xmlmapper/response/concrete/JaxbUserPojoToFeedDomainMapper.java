package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.response.concrete;

import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.FeederUserType;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JaxbUserPojoToFeedDomainMapper implements JaxbPojoToFeederPojoMapper<FeederClientSideUser, FeederUserType> {
    @Override
    public FeederClientSideUser mapJaxbPojoToFeederPojo(final FeederUserType objectToMap) {
        FeederClientSideUser mappedUseCaseUserResult = new FeederClientSideUser();

        mappedUseCaseUserResult.setUserName(objectToMap.getFeederUser().getUserName());
        mappedUseCaseUserResult.setFirstName(objectToMap.getFeederUser().getFirstName());
        mappedUseCaseUserResult.setLastName(objectToMap.getFeederUser().getLastName());
        mappedUseCaseUserResult.setEMail(objectToMap.getFeederUser().getEMail());
        mappedUseCaseUserResult.setPassword(objectToMap.getFeederUser().getPassword());
        mappedUseCaseUserResult.setUserId(objectToMap.getFeederUser().getUserId());

        return mappedUseCaseUserResult;
    }
}
