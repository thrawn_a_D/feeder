package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request;

import javax.inject.Inject;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.AbstractUserType;
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.CreateUserRequestType;
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class CreateUserRequestMapper implements RequestToXmlMapper<CreateUserRequest> {

    private static final String JAXB_GENERATED_OBJECT_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.schema.request.user";
    @Inject
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getRequestData(final CreateUserRequest objectToMap) {
        CreateUserRequestType mappedObjectToMarshall = mapRequestToJaxbObject(objectToMap);

        return marshall(mappedObjectToMarshall);
    }

    private CreateUserRequestType mapRequestToJaxbObject(final CreateUserRequest objectToMap) {
        CreateUserRequestType mappedObjectToMarshall = new CreateUserRequestType();

        mapUser(objectToMap, mappedObjectToMarshall);

        return mappedObjectToMarshall;
    }

    private void mapUser(final CreateUserRequest objectToMap, final CreateUserRequestType mappedObjectToMarshall) {
        AbstractUserType mappedUser = new AbstractUserType();
        mappedUser.setFirstName(objectToMap.getUserToBeHandled().getFirstName());
        mappedUser.setLastName(objectToMap.getUserToBeHandled().getLastName());
        mappedUser.setEMail(objectToMap.getUserToBeHandled().getEMail());
        mappedUser.setUserName(objectToMap.getUserToBeHandled().getUserName());
        mappedUser.setUserId(objectToMap.getUserToBeHandled().getUserId());
        mappedUser.setPassword(objectToMap.getUserToBeHandled().getPassword());
        mappedObjectToMarshall.setUser(mappedUser);
    }


    private String marshall(final CreateUserRequestType objectToMap) {
        jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_OBJECT_PACKAGE);
        return jaxbMarshaller.marshal(objectToMap);
    }
}
