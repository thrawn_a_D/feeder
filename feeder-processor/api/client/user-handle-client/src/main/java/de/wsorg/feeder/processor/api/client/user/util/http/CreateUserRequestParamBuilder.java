package de.wsorg.feeder.processor.api.client.user.util.http;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CreateUserRequestParamBuilder implements HttpRequestParamBuilder<String> {

    private static final String HANDLE_USER_RESOURCE = "user";

    @Inject
    private ConfigurationProvider configurationProvider;

    @Override
    public HttpRequestBase buildHttpRequest(final String requestParams) {
        final String feederServiceUrl = configurationProvider.getFeederClientConfiguration().getFeederRestServiceUrl();
        HttpPost createUserRequest = new HttpPost(feederServiceUrl + HANDLE_USER_RESOURCE);

        try {
            createUserRequest.setEntity(new StringEntity(requestParams));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return createUserRequest;
    }
}
