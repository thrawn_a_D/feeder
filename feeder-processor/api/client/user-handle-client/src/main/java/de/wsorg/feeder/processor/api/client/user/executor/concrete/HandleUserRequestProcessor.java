package de.wsorg.feeder.processor.api.client.user.executor.concrete;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.client.abstraction.executor.AbstractProcessorTemplate;
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class HandleUserRequestProcessor extends AbstractProcessorTemplate<HandleUserRequest, GenericResponseResult> {

    @Inject
    @Named(value = "handleUserRequestMapper")
    private RequestToXmlMapper<HandleUserRequest> requestToXmlMapper;

    @Inject
    @Named(value = "handleUserResponseMapper")
    private XmlToDomainMapper<GenericResponseResult> xmlToDomainMapper;

    @Override
    protected RequestToXmlMapper<HandleUserRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    protected XmlToDomainMapper<GenericResponseResult> getXmlToDomainMapper() {
        return xmlToDomainMapper;
    }

    @Override
    protected FeederCommand getCommandToExecute() {
        return FeederCommand.HANDLE_USER;
    }
}
