package de.wsorg.feeder.processor.api.client.user.util.http;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilderResolver;
import de.wsorg.feeder.processor.api.domain.FeederCommand;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserHttpRequestParamBuilderResolver implements HttpRequestParamBuilderResolver {

    @Inject
    @Named("handleUserRequestParamBuilder")
    private HttpRequestParamBuilder handleUserHttpRequestBuilder;
    @Inject
    @Named("createUserRequestParamBuilder")
    private HttpRequestParamBuilder createUserHttpRequestBuilder;
    @Inject
    @Named("verifyUserRequestParamBuilder")
    private HttpRequestParamBuilder verifyUserHttpRequestBuilder;

    @Override
    public HttpRequestParamBuilder findHttpRequestParamBuilder(final FeederCommand feederCommand) {

        if(feederCommand == FeederCommand.HANDLE_USER) {
            return handleUserHttpRequestBuilder;
        } else if(feederCommand == FeederCommand.CREATE_USER) {
            return createUserHttpRequestBuilder;
        } else if(feederCommand == FeederCommand.LOGIN_USER) {
            return verifyUserHttpRequestBuilder;
        } else {
            final String errorMessage = "No http parameter builder could be found to execute the command: " + feederCommand;
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
