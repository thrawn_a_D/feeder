package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserLoginRequestToXmlMapper implements RequestToStringMapMapper<UserLoginRequest> {

    @Override
    public Map getRequestData(final UserLoginRequest objectToMap) {
        Map<String, String> result = new HashMap<>();

        result.put("nickName", objectToMap.getNickName());
        result.put("password", objectToMap.getPassword());

        return result;
    }
}
