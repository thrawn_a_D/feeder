package de.wsorg.feeder.processor.api.client.user.executor.concrete;

import de.wsorg.feeder.processor.api.client.abstraction.executor.AbstractProcessorTemplate;
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ValidateUserRequestProcessor extends AbstractProcessorTemplate<UserLoginRequest, FeederClientSideUser> {

    @Inject
    @Named("userLoginRequestToMapMapper")
    private RequestToStringMapMapper<UserLoginRequest> requestToXmlMapper;

    @Inject
    @Named("userResponseToResultMapper")
    private XmlToDomainMapper<FeederClientSideUser> xmlToDomainMapper;

    @Override
    protected RequestToStringMapMapper<UserLoginRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    protected XmlToDomainMapper<FeederClientSideUser> getXmlToDomainMapper() {
        return xmlToDomainMapper;
    }

    @Override
    protected FeederCommand getCommandToExecute() {
        return FeederCommand.LOGIN_USER;
    }
}
