package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.request;

import javax.inject.Inject;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.AbstractUserType;
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.HandleUserRequestType;
import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.UserHandleType;
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class HandleUserRequestMapper implements RequestToXmlMapper<HandleUserRequest> {

    private static final String JAXB_GENERATED_OBJECT_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.schema.request.user";
    @Inject
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getRequestData(final HandleUserRequest objectToMap) {
        HandleUserRequestType mappedObjectToMarshall = mapRequestToJaxbObject(objectToMap);

        return marshall(mappedObjectToMarshall);
    }

    private HandleUserRequestType mapRequestToJaxbObject(final HandleUserRequest objectToMap) {
        HandleUserRequestType mappedObjectToMarshall = new HandleUserRequestType();

        mapHandlingType(objectToMap, mappedObjectToMarshall);
        mapUser(objectToMap, mappedObjectToMarshall);

        return mappedObjectToMarshall;
    }

    private void mapUser(final HandleUserRequest objectToMap, final HandleUserRequestType mappedObjectToMarshall) {
        AbstractUserType mappedUser = new AbstractUserType();
        mappedUser.setFirstName(objectToMap.getFirstName());
        mappedUser.setLastName(objectToMap.getLastName());
        mappedUser.setEMail(objectToMap.getEMail());
        mappedUser.setUserName(objectToMap.getUserName());
        mappedUser.setUserId(objectToMap.getUserId());
        mappedUser.setPassword(objectToMap.getPassword());
        mappedObjectToMarshall.setUser(mappedUser);
    }

    private void mapHandlingType(final HandleUserRequest objectToMap, final HandleUserRequestType mappedObjectToMarshall) {
        final UserHandlingType handlingType = objectToMap.getHandlingType();
        UserHandleType mappedHandlingType = UserHandleType.fromValue(handlingType.name());
        mappedObjectToMarshall.setRequestType(mappedHandlingType);
    }

    private String marshall(final HandleUserRequestType objectToMap) {
        jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_OBJECT_PACKAGE);
        return jaxbMarshaller.marshal(objectToMap);
    }
}
