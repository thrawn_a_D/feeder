package de.wsorg.feeder.processor.api.client.user.util.xmlmapper.response.concrete;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.generated.schema.request.user.FeederUserType;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapperTemplate;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserXmlToDomainMapper extends XmlToDomainMapperTemplate<FeederUseCaseUser, FeederUserType> {

    private static final String JAXB_GENERATED_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.schema.request.user";

    @Inject
    @Named("jaxbUserPojoToFeedDomainMapper")
    private JaxbPojoToFeederPojoMapper<FeederUseCaseUser, FeederUserType> jaxbPojoToFeederPojoMapper;

    @Override
    protected String getGeneratedJaxbClassesPackage() {
        return JAXB_GENERATED_PACKAGE;
    }

    @Override
    protected JaxbPojoToFeederPojoMapper<FeederUseCaseUser, FeederUserType> getJaxbPojoToFeederPojoMapper() {
        return jaxbPojoToFeederPojoMapper;
    }
}
