package de.wsorg.feeder.processor.api.client.user.util.http;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class VerifyUserRequestParamBuilder implements HttpRequestParamBuilder<Map<String, String>> {

    private static final String USER_RESOURCE = "user";
    @Inject
    private ConfigurationProvider configurationProvider;

    @Override
    public HttpRequestBase buildHttpRequest(final Map<String, String> requestParams) {
        final FeederClientConfiguration configuration = configurationProvider.getFeederClientConfiguration();

        final String nickName = requestParams.get("nickName");
        final String password = requestParams.get("password");

        final String uri = configuration.getFeederRestServiceUrl() + USER_RESOURCE + "/" + nickName + "?password=" + password;
        try {
            return new HttpGet(uri);
        } catch (Exception e) {
            throw new IllegalArgumentException("The provided user data contains invalid characters.");
        }
    }
}
