package de.wsorg.feeder.processor.api.client.user;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserClientFactory {
    private static boolean useFileSystemProtocol=false;

    private static ClassPathXmlApplicationContext applicationContext;

    private static boolean configProvided=false;

    private static final String ROOT_SPRING_CONTEXT = "classpath:spring/user-handle-client-spring-context.xml";

    public static FeederRequestProcessor<HandleUserRequest, GenericResponseResult> getUserHandler(){
        validateState();
        return getApplicationContext().getBean("handleUserRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<CreateUserRequest, GenericResponseResult> getUserCreator(){
        validateState();
        return getApplicationContext().getBean("createUserRequestProcessor", FeederRequestProcessor.class);
    }


    public static FeederRequestProcessor<UserLoginRequest, FeederClientSideUser> getUserValidator(){
        validateState();
        return getApplicationContext().getBean("validateUserRequestProcessor", FeederRequestProcessor.class);
    }

    public static void setClientConfig(final FeederClientConfiguration clientConfig) {
        if(!getApplicationContext().containsBean("clientConfig"))
            getApplicationContext().getBeanFactory().registerSingleton("clientConfig", clientConfig);

        configProvided = true;
    }

    private static ClassPathXmlApplicationContext getApplicationContext(){
        if(applicationContext == null){

            if(useFileSystemProtocol)
                applicationContext = new ClassPathXmlApplicationContext(ROOT_SPRING_CONTEXT, "classpath:spring/executionprotocol/userFileBasedExecutionContext.xml");
            else
                applicationContext = new ClassPathXmlApplicationContext(ROOT_SPRING_CONTEXT, "classpath:spring/executionprotocol/userHttpBasedExecutionContext.xml");


        }

        return applicationContext;
    }

    private static void validateState() {
        if(!configProvided)
            throw new IllegalStateException("Please provide a valid client configuration first");
    }
}
