package de.wsorg.feeder.processor.api.client.abstraction;


import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.09.12
 */
public class ClientAbstractionFactoryTest extends Specification {

    def "Get an feeder result mapper"() {
        when:
        def mapper = ClientAbstractionFactory.getAbstractResponseMapper()

        then:
        mapper != null
    }
}
