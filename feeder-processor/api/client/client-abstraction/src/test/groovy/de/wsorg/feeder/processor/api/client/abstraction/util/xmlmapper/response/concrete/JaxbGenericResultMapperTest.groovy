package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.response.concrete;


import de.wsorg.feeder.processor.api.domain.generated.response.generic.processResult.FeederProcessingResultType
import de.wsorg.feeder.processor.api.domain.generated.response.generic.processResult.ProcessResultType
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.09.12
 */
public class JaxbGenericResultMapperTest extends Specification {
    JaxbGenericResultMapper jaxbGenericResultMapper

    def setup(){
        jaxbGenericResultMapper = new JaxbGenericResultMapper()
    }


    def "Map handler xml result to a pojo"() {
        given:
        def providedHandlerResult = getSimpleHandlerResult()
        JAXBElement processingResultAsJaxb = new JAXBElement(new QName(''), FeederProcessingResultType, providedHandlerResult)

        when:
        GenericResponseResult result = jaxbGenericResultMapper.mapJaxbPojoToFeederPojo(processingResultAsJaxb)

        then:
        result.message == providedHandlerResult.message
        result.processingResult.name() == providedHandlerResult.processResult.name()
    }

    def "Provide something that is not a handler response"() {
        given:
        def somethingInvalid = 'asd'
        JAXBElement processingResultAsJaxb = new JAXBElement(new QName(''), FeederProcessingResultType, somethingInvalid)

        when:
        jaxbGenericResultMapper.mapJaxbPojoToFeederPojo(processingResultAsJaxb)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided jaxb element does not contain an object of type FeederProcessingResultType"
    }

    private FeederProcessingResultType getSimpleHandlerResult() {
        FeederProcessingResultType feederProcessingResultType = new FeederProcessingResultType()
        feederProcessingResultType.message = 'bla'
        feederProcessingResultType.processResult = ProcessResultType.SUCCESS
        return feederProcessingResultType
    }
}
