package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.authentication

import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import org.aspectj.lang.ProceedingJoinPoint
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.01.13
 */
class AuthenticatedRequestToXmlMapperTest extends Specification {
    AuthenticatedRequestToXmlMapper authenticatedRequestToXmlMapper

    def setup(){
        authenticatedRequestToXmlMapper = new AuthenticatedRequestToXmlMapper()
    }

    def "Wrap the provided xml with authenticated xml"() {
        given:
        def joinPoint = Mock(ProceedingJoinPoint)

        and:
        def userName = 'asd'
        def password = 'passwd'
        def userId = '123123'
        def authenticatedRequest = Mock(UserRelatedLoadFeedRequest)
        authenticatedRequest.userName >> userName
        authenticatedRequest.password >> password
        authenticatedRequest.userId >> userId
        joinPoint.getArgs() >> [authenticatedRequest]

        and:
        def calculatedXmlByActualMapper = 'asasd'

        when:
        def generatedXml = authenticatedRequestToXmlMapper.invoke(joinPoint)

        then:
        1 * joinPoint.proceed(joinPoint.getArgs()) >> calculatedXmlByActualMapper
        new XmlSlurper().parseText((String)generatedXml).'actualRequestContent'.text() == calculatedXmlByActualMapper as String
        new XmlSlurper().parseText((String)generatedXml).'userName'.text() == userName as String
        new XmlSlurper().parseText((String)generatedXml).'password'.text() == password as String
        new XmlSlurper().parseText((String)generatedXml).'userId'.text() == userId as String
    }

    def "Only adjust authentication when the actual domain model implements AuthorizedRequest interface"() {
        given:
        def joinPoint = Mock(ProceedingJoinPoint)

        and:
        def authenticatedRequest = Mock(LoadFeedRequest)
        joinPoint.getArgs() >> [authenticatedRequest]

        and:
        def calculatedXmlByActualMapper = 'asasd'

        when:
        def generatedXml = authenticatedRequestToXmlMapper.invoke(joinPoint)

        then:
        generatedXml == calculatedXmlByActualMapper
        1 * joinPoint.proceed(joinPoint.getArgs()) >> calculatedXmlByActualMapper
    }
}
