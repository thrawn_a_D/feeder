package de.wsorg.feeder.processor.api.client.abstraction.util.file;


import de.wsorg.feeder.processor.api.client.abstraction.util.FileHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class FileHandlerTest extends Specification {

    final static String SYSTZEM_TMP_FOLDER=System.getProperty("java.io.tmpdir");
    final static String TMP_FILE_TO_WRITE_READ=SYSTZEM_TMP_FOLDER + File.separator + "testFile.out"


    FileHandler fileHandler


    def setup(){
        fileHandler = new FileHandler()
    }

    def "Write content to file"() {
        given:
        final String contentToBeWritten="My text to write";

        when:
        fileHandler.write(TMP_FILE_TO_WRITE_READ, contentToBeWritten)

        then:
        new File(TMP_FILE_TO_WRITE_READ).text == contentToBeWritten
    }

    def "Write content using invalid file. Expect an exception"() {
        given:
        final String invalidFilePath = "asd\\//@²ä";
        final String contentToWrite = "sdf"

        when:
        fileHandler.write(invalidFilePath, contentToWrite)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "Invalid file path was given or insufficient writing privileges: " + invalidFilePath;
    }

    def "Read a given file"() {
        given:
        final String contentToExpectFromFile = "test content to read"

        and:
        prepareFileForRead(contentToExpectFromFile)


        when:
        def readContent = fileHandler.read(TMP_FILE_TO_WRITE_READ)

        then:
        readContent == contentToExpectFromFile
    }

    def "Invalid filepath to read given"() {
        given:
        final String givenFilePath = "sdf/\\äl`asd"

        when:
        fileHandler.read(givenFilePath)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "Not able to read the provided file path:" + givenFilePath
    }

    private File prepareFileForRead(String contentToExpectFromFile) {
        def fileToWriteTo = new File(TMP_FILE_TO_WRITE_READ)

        if(fileToWriteTo.exists())
            fileToWriteTo.delete()

        fileToWriteTo.createNewFile()

        fileToWriteTo << contentToExpectFromFile
    }

}
