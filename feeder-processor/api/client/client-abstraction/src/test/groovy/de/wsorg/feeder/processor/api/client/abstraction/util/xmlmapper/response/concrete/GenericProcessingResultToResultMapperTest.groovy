package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.response.concrete;


import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.09.12
 */
public class GenericProcessingResultToResultMapperTest extends Specification {
    GenericProcessingResultToDomainMapper genericResponseToResultMapper

    def setup() {
        genericResponseToResultMapper = new GenericProcessingResultToDomainMapper()
    }

    def "Make sure the correct jaxb package is provided"() {
        given:
        def jaxbGeneratedPackage = "de.wsorg.feeder.processor.api.domain.generated.response.generic.processResult"

        when:
        def usedPackage = genericResponseToResultMapper.generatedJaxbClassesPackage

        then:
        usedPackage == jaxbGeneratedPackage
    }

    def "Provide the appropriate jaxb mapper"() {
        given:
        def jaxbToFeederMapper = Mock(JaxbPojoToFeederPojoMapper)
        genericResponseToResultMapper.jaxbPojoToFeederPojoMapper = jaxbToFeederMapper

        when:
        def usedMapper = genericResponseToResultMapper.jaxbPojoToFeederPojoMapper

        then:
        usedMapper == jaxbToFeederMapper
    }
}
