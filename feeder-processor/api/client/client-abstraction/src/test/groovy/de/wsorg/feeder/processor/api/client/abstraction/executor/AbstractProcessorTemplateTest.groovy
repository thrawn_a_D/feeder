package de.wsorg.feeder.processor.api.client.abstraction.executor;


import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.FeederRequestCall
import de.wsorg.feeder.processor.api.client.abstraction.util.FileHandler
import de.wsorg.feeder.processor.api.client.abstraction.util.MainExecutorProxy
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestModelToTransferMapper
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.response.concrete.GenericProcessingResultToDomainMapper
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.Request
import de.wsorg.feeder.processor.api.domain.response.Response
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class AbstractProcessorTemplateTest extends Specification {

    AbstractProcessorTemplate fileBasedProcessorTemplate = new AbstractProcessorTestClass()

    static RequestModelToTransferMapper requestModelToTransferMapper;
    static XmlToDomainMapper responseToResultMapperMock;
    static XmlToDomainMapper genericResponseMapper;
    static FeederCommand feederCommand;
    static MainExecutorProxy mainExecutorProxyMock;

    FeederRequestCall feederRequestCall

    FileHandler fileHandlerMock;


    def setup(){
        requestModelToTransferMapper = Mock(RequestToXmlMapper.class);
        feederRequestCall = Mock(FeederRequestCall)
        responseToResultMapperMock = Mock(XmlToDomainMapper.class);
        genericResponseMapper = Mock(GenericProcessingResultToDomainMapper.class);
        fileHandlerMock = Mock(FileHandler)
        mainExecutorProxyMock = Mock(MainExecutorProxy)
        feederCommand = FeederCommand.LOAD;

        fileBasedProcessorTemplate.fileHandler = fileHandlerMock
        fileBasedProcessorTemplate.genericResponseMapper = genericResponseMapper
        fileBasedProcessorTemplate.feederRequestCall = feederRequestCall
    }

    def "Simple command execution"(){
        given:
        Request myRequest = new Request(){};

        and:
        def givenRequestAsXml = "<tag></tag>"
        def responseXml = "response xml"
        def expectedResponse = new Response(){}

        when:
        def executionResult = fileBasedProcessorTemplate.executeRequest(myRequest);

        then:
        1 * requestModelToTransferMapper.getRequestData(myRequest) >> givenRequestAsXml
        1 * feederRequestCall.processRequestCall(feederCommand, givenRequestAsXml) >> responseXml
        1 * responseToResultMapperMock.getResult(responseXml) >> expectedResponse
        executionResult == expectedResponse
    }

    def "Search query does not contain any query"() {
        when:
        def result = fileBasedProcessorTemplate.executeRequest(null)

        then:
        result != null
    }

    def "Check if response is generic and contains an exception"() {
        given:
        Request myRequest = new Request(){};

        and:
        def givenRequestAsXml = "<tag></tag>"
        def responseXml = "response xml"
        def expectedResponse = new GenericResponseResult()
        expectedResponse.processingResult = ProcessingResult.FAILED
        expectedResponse.message = "An error occured"

        when:
        fileBasedProcessorTemplate.executeRequest(myRequest);

        then:
        def ex = thrown(FeederProcessingException)
        ex.message == expectedResponse.message
        1 * requestModelToTransferMapper.getRequestData(myRequest) >> givenRequestAsXml
        1 * feederRequestCall.processRequestCall(feederCommand, givenRequestAsXml) >> responseXml
        0 * responseToResultMapperMock.getResult(responseXml) >> expectedResponse
        1 * genericResponseMapper.getResult(responseXml) >> expectedResponse
    }

    def "Got generic response but processing was successfully and generic response was expected by actual template user"() {
        given:
        Request myRequest = new Request(){};

        and:
        def givenRequestAsXml = "<tag></tag>"
        def responseXml = "response xml"
        def expectedResponse = new GenericResponseResult()
        expectedResponse.processingResult = ProcessingResult.SUCCESS

        and:
        AbstractProcessorTemplateTest.responseToResultMapperMock = genericResponseMapper

        when:
        def result = fileBasedProcessorTemplate.executeRequest(myRequest);

        then:
        result == expectedResponse
        1 * requestModelToTransferMapper.getRequestData(myRequest) >> givenRequestAsXml
        1 * feederRequestCall.processRequestCall(feederCommand, givenRequestAsXml) >> responseXml
        1 * genericResponseMapper.getResult(responseXml) >> expectedResponse
    }

    def "Got generic response but processing was successfully and generic response was NOT expected by actual template user"() {
        given:
        Request myRequest = new Request(){};

        and:
        def givenRequestAsXml = "<tag></tag>"
        def responseXml = "response xml"
        def expectedResponse = new GenericResponseResult()
        expectedResponse.processingResult = ProcessingResult.SUCCESS

        when:
        fileBasedProcessorTemplate.executeRequest(myRequest);

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'Positive generic response received but wanted something different'
        1 * requestModelToTransferMapper.getRequestData(myRequest) >> givenRequestAsXml
        1 * feederRequestCall.processRequestCall(feederCommand, givenRequestAsXml) >> responseXml
        0 * responseToResultMapperMock.getResult(responseXml) >> expectedResponse
        1 * genericResponseMapper.getResult(responseXml) >> expectedResponse
    }

    def "When parsing for generic response catch an exception and use the actual parser"() {
        given:
        Request myRequest = new Request(){};

        and:
        def givenRequestAsXml = "<tag></tag>"
        def responseXml = "response xml"
        def expectedResponse = new Response() {}

        when:
        def result  = fileBasedProcessorTemplate.executeRequest(myRequest);

        then:
        result == expectedResponse
        1 * requestModelToTransferMapper.getRequestData(myRequest) >> givenRequestAsXml
        1 * feederRequestCall.processRequestCall(feederCommand, givenRequestAsXml) >> responseXml
        1 * responseToResultMapperMock.getResult(responseXml) >> expectedResponse
        1 * genericResponseMapper.getResult(responseXml) >> {throw new RuntimeException()}
    }

    final static class AbstractProcessorTestClass extends AbstractProcessorTemplate<Request, Response> {

        @Override
        RequestModelToTransferMapper<String, Request> getRequestToTransferMapper() {
            return AbstractProcessorTemplateTest.requestModelToTransferMapper;
        }

        @Override
        XmlToDomainMapper<Response> getXmlToDomainMapper() {
            return AbstractProcessorTemplateTest.responseToResultMapperMock;
        }

        @Override
        FeederCommand getCommandToExecute() {
            return AbstractProcessorTemplateTest.feederCommand;
        }
    }
}
