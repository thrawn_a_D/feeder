package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.find

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.02.12
 */
class FileFinderTest extends Specification {
    private static final String FILE_NAME = 'ClientAbstractionFactory.java'
    private static final String EXPECTED_FILE_PATH = "api/client/client-abstraction/src/main/java/de/wsorg/feeder/processor/api/client/abstraction/${FILE_NAME}"

    FileFinder fileFinder

    def setup(){
        fileFinder = new FileFinder()
    }

    def "Find a file in the project"() {

        when:
        def foundFilePath = fileFinder.getFilePathOfFile(FILE_NAME)

        then:
        foundFilePath.endsWith(EXPECTED_FILE_PATH)
    }

    def "Provide a not existing filename, return null"() {
        given:
        def fileName = 'sdfsdsd234fsdf43'

        when:
        def foundFilePath = fileFinder.getFilePathOfFile(fileName)

        then:
        foundFilePath == null
    }
}
