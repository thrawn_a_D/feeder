package de.wsorg.feeder.processor.api.client.abstraction.configuration

import org.springframework.context.ApplicationContext
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.02.12
 */
class ConfigurationProviderTest extends Specification {
    ConfigurationProvider configurationProvider
    ApplicationContext applicationContext

    def setup(){
        configurationProvider = new ConfigurationProvider()
        applicationContext = Mock(ApplicationContext)
        configurationProvider.applicationContext = applicationContext
    }

    def "Provide a configuration from app context"() {
        given:
        def config = Mock(FeederClientConfiguration)

        and:
        applicationContext.getBean(FeederClientConfiguration) >> config

        when:
        def result = configurationProvider.getFeederClientConfiguration()

        then:
        result == config
    }

    def "If no config is set yet, throw an exception"() {
        given:
        applicationContext.getBean(FeederClientConfiguration) >> {throw new RuntimeException()}


        when:
        configurationProvider.getFeederClientConfiguration()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'No client configuration provided! Please provide a valid configuration first.'
    }
}
