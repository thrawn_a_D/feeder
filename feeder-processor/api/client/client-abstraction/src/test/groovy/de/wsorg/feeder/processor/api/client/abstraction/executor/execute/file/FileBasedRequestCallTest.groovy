package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file

import de.wsorg.feeder.processor.api.client.abstraction.util.FileHandler
import de.wsorg.feeder.processor.api.client.abstraction.util.MainExecutorProxy
import de.wsorg.feeder.processor.api.domain.FeederCommand
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.01.13
 */
class FileBasedRequestCallTest extends Specification {
    FileBasedRequestCall fileBasedRequestCall
    MainExecutorProxy mainExecutorProxy
    FileHandler fileHandler
    RequestObjectToFileTransitionHelper requestObjectToFileTransitionHelper

    FeederCommand feederCommand

    def setup(){
        fileBasedRequestCall = new FileBasedRequestCall()

        mainExecutorProxy = Mock(MainExecutorProxy)
        fileHandler = Mock(FileHandler)
        requestObjectToFileTransitionHelper = Mock(RequestObjectToFileTransitionHelper)

        fileBasedRequestCall.mainExecutorProxy = mainExecutorProxy
        fileBasedRequestCall.fileHandler = fileHandler
        fileBasedRequestCall.requestObjectToFileTransitionHelper = requestObjectToFileTransitionHelper

        feederCommand = FeederCommand.LOAD;
    }

    def "Call the feeder main with all correct properties"() {
        given:
        def params = 'asd'

        when:
        fileBasedRequestCall.processRequestCall(feederCommand, params)

        then:
        1 * mainExecutorProxy.invoke({String[] arguments->
                                        arguments[0] == feederCommand &&
                                        arguments[1] == fileBasedRequestCall.getDefaultRequestFile(feederCommand)
                                        arguments[2] == fileBasedRequestCall.getDefaultResponseFile(feederCommand)
        })
    }

    def "Check that content has been written to file"() {
        given:
        def params = 'asd'

        and:
        def filePath = fileBasedRequestCall.getDefaultRequestFile(feederCommand)

        and:
        def writeToFileString = 'kiiunoi'

        when:
        fileBasedRequestCall.processRequestCall(feederCommand, params)

        then:
        1 * requestObjectToFileTransitionHelper.transformObjectToWritableString(params) >> writeToFileString
        1 * fileHandler.write(filePath, writeToFileString)
    }

    def "Check that response has been read from file"() {
        given:
        def params = 'asd'

        and:
        def responseFile = fileBasedRequestCall.getDefaultResponseFile(feederCommand)

        and:
        def responseXml = 'aaaa'

        when:
        def result = fileBasedRequestCall.processRequestCall(feederCommand, params)

        then:
        result == responseXml
        1 * fileHandler.read(responseFile) >> responseXml
    }

    def "Check communication files have a command in its name"() {
        when:
        def requestFile = fileBasedRequestCall.getDefaultRequestFile(feederCommand)
        def responseFile = fileBasedRequestCall.getDefaultResponseFile(feederCommand)

        then:
        requestFile.contains(feederCommand.name() + "-")
        responseFile.contains(feederCommand.name() + "-")
    }

    def "Check file separator is adequate for current os"() {

        when:
        def defaultRequestPath = fileBasedRequestCall.getDefaultRequestFile(feederCommand)
        def defaultResponsePath = fileBasedRequestCall.getDefaultResponseFile(feederCommand)

        then:
        defaultResponsePath.contains(File.separator)
        defaultRequestPath.contains(File.separator)
    }
}
