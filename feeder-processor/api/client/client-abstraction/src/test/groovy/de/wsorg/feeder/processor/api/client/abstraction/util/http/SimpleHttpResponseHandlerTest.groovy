package de.wsorg.feeder.processor.api.client.abstraction.util.http

import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.02.13
 */
class SimpleHttpResponseHandlerTest extends Specification {
    SimpleHttpResponseHandler simpleHttpResponseHandler

    def setup(){
        simpleHttpResponseHandler = new SimpleHttpResponseHandler()
    }

    def "Transform response content into a string"() {
        given:
        def response = Mock(HttpResponse)
        def responseEntity = Mock(HttpEntity)

        and:
        response.getEntity() >> responseEntity

        and:
        def actualContent = 'blablub'
        def contentStream = new ByteArrayInputStream(actualContent.bytes)
        responseEntity.getContent() >> contentStream

        when:
        def result = simpleHttpResponseHandler.getResponseAsString(response)

        then:
        result == actualContent
    }

    def "Error while reading content"() {
        given:
        def response = Mock(HttpResponse)

        and:
        response.getEntity() >> {throw new IOException("bla")}

        when:
        simpleHttpResponseHandler.getResponseAsString(response)

        then:
        thrown(RuntimeException)
    }
}
