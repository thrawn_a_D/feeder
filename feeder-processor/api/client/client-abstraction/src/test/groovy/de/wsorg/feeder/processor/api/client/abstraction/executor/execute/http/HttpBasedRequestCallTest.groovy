package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.wrapper.HttpClientWrapper
import de.wsorg.feeder.processor.api.client.abstraction.util.http.HttpResponseHandler
import de.wsorg.feeder.processor.api.domain.FeederCommand
import org.apache.http.HttpResponse
import org.apache.http.StatusLine
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpRequestBase
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.01.13
 */
class HttpBasedRequestCallTest extends Specification {
    HttpBasedRequestCall httpBasedRequestCall
    HttpRequestParamBuilderResolver httpRequestParamBuilderResolver
    HttpClientWrapper httpClientWrapper
    HttpResponseHandler httpResponseHandler

    def setup(){
        httpBasedRequestCall = new HttpBasedRequestCall()
        httpClientWrapper = Mock(HttpClientWrapper)
        httpResponseHandler = Mock(HttpResponseHandler)
        httpRequestParamBuilderResolver = Mock(HttpRequestParamBuilderResolver)
        httpBasedRequestCall.httpRequestParamBuilderResolver = httpRequestParamBuilderResolver
        httpBasedRequestCall.httpClientWrapper = httpClientWrapper
        httpBasedRequestCall.httpResponseHandler = httpResponseHandler
    }

    def "Execute a call using http"() {
        given:
        def command = FeederCommand.CREATE_USER
        def argument = 'asd'

        and:
        def requestParamBuilder = Mock(HttpRequestParamBuilder)

        and:
        def httpRequestParam = Mock(HttpRequestBase)

        and:
        def httpClient = Mock(HttpClient)

        and:
        def response = Mock(HttpResponse)
        def status = Mock(StatusLine)
        response.getStatusLine() >> status
        status.getStatusCode() >> 200

        and:
        def responseContent = 'asdasd'

        when:
        def result = httpBasedRequestCall.processRequestCall(command, argument)

        then:
        result == responseContent
        1 * httpRequestParamBuilderResolver.findHttpRequestParamBuilder(command) >> requestParamBuilder
        1 * requestParamBuilder.buildHttpRequest(argument) >> httpRequestParam
        1 * httpClientWrapper.getHttpClient() >> httpClient
        1 * httpClient.execute(httpRequestParam) >> response
        1 * httpResponseHandler.getResponseAsString(response) >> responseContent
    }

    def "Exception occurs during http call"() {
        given:
        def command = FeederCommand.CREATE_USER
        def argument = 'asd'

        and:
        def requestParamBuilder = Mock(HttpRequestParamBuilder)

        and:
        def httpRequestParam = Mock(HttpRequestBase)

        and:
        def httpClient = Mock(HttpClient)

        and:
        httpRequestParamBuilderResolver.findHttpRequestParamBuilder(command) >> requestParamBuilder
        requestParamBuilder.buildHttpRequest(argument) >> httpRequestParam
        httpClientWrapper.getHttpClient() >> httpClient
        httpClient.execute(httpRequestParam) >> {throw new IOException("bla")}

        when:
        httpBasedRequestCall.processRequestCall(command, argument)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'Error while executing call to feeder rest API:bla'
    }

    def "Response code is different then 200"() {
        given:
        def command = FeederCommand.CREATE_USER
        def argument = 'asd'

        and:
        def requestParamBuilder = Mock(HttpRequestParamBuilder)

        and:
        def httpRequestParam = Mock(HttpRequestBase)

        and:
        def httpClient = Mock(HttpClient)

        and:
        def response = Mock(HttpResponse)
        def status = Mock(StatusLine)
        response.getStatusLine() >> status
        status.getStatusCode() >> 500

        and:
        httpRequestParamBuilderResolver.findHttpRequestParamBuilder(command) >> requestParamBuilder
        requestParamBuilder.buildHttpRequest(argument) >> httpRequestParam
        httpClientWrapper.getHttpClient() >> httpClient
        httpClient.execute(httpRequestParam) >> response

        when:
        httpBasedRequestCall.processRequestCall(command, argument)

        then:
        def ex = thrown(RuntimeException)
        ex.message=='An error occurred while communicating with the feeder service. Response code was: 500'
    }
}
