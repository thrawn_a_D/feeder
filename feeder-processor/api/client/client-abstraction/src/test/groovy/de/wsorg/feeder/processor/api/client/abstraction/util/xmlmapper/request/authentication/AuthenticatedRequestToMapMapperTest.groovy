package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.authentication

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest
import org.aspectj.lang.ProceedingJoinPoint
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.01.13
 */
class AuthenticatedRequestToMapMapperTest extends Specification {
    AuthenticatedRequestToMapMapper authenticatedRequestToMapMapper

    def setup(){
        authenticatedRequestToMapMapper = new AuthenticatedRequestToMapMapper()
    }

    def "Provide the params to the actual mapper and then insert credentials in the resulted map"() {
        given:
        def params = Mock(AuthorizedRequest)
        params.userId >> '123'
        params.userName >> 'userName'
        params.password >> 'passwd'

        and:
        def methodInvocation = Mock(ProceedingJoinPoint)
        methodInvocation.args >> [params]

        and:
        def processResult = new HashMap<String, String>()

        when:
        def result = authenticatedRequestToMapMapper.invoke(methodInvocation)

        then:
        result['userName'] == params.userName
        result['password'] == params.password
        result['userId'] == params.userId
        1 * methodInvocation.proceed(methodInvocation.args) >> processResult
    }

    def "Provided param is not of type AuthorizedRequest"() {
        given:
        def params = ''

        and:
        def methodInvocation = Mock(ProceedingJoinPoint)
        methodInvocation.args >> [params]

        and:
        def processResult = new HashMap<String, String>()

        when:
        def result = authenticatedRequestToMapMapper.invoke(methodInvocation)

        then:
        result == processResult
        1 * methodInvocation.proceed(methodInvocation.args) >> processResult
    }
}
