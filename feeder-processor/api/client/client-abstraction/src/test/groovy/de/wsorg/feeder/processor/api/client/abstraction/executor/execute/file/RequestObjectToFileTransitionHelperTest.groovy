package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.01.13
 */
class RequestObjectToFileTransitionHelperTest extends Specification {
    RequestObjectToFileTransitionHelper requestObjectToFileTransitionHelper

    def setup(){
        requestObjectToFileTransitionHelper = new RequestObjectToFileTransitionHelper()
    }

    def "Provide a string and receive it back"() {
        given:
        def transformObject = 'asd'

        when:
        def result = requestObjectToFileTransitionHelper.transformObjectToWritableString(transformObject)

        then:
        result == transformObject
    }

    def "Provide a map of strings, retrieve an appropriate result"() {
        given:
        def mapOfValues = ['bla':'blub', 'pi':'papo']

        when:
        def result = requestObjectToFileTransitionHelper.transformObjectToWritableString(mapOfValues)

        then:
        result.readLines()[0] == 'bla@blub;'
        result.readLines()[1] == 'pi@papo;'
    }

    def "The provided object is not of type String or Map"() {
        given:
        def transformObject = []

        when:
        requestObjectToFileTransitionHelper.transformObjectToWritableString(transformObject)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.getMessage() == 'The provided request param object needs to be either String or Map<String, String>.'
    }

    def "Provide a map with invalid types"() {
        given:
        def mapOfValues = new HashMap<Date, Boolean>()
        mapOfValues.put(new Date(), true)

        when:
        requestObjectToFileTransitionHelper.transformObjectToWritableString(mapOfValues)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided request param object needs to be either String or Map<String, String>.'
    }
}
