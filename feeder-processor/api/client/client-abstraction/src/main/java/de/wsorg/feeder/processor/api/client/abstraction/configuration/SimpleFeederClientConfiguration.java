package de.wsorg.feeder.processor.api.client.abstraction.configuration;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SimpleFeederClientConfiguration implements FeederClientConfiguration {
    private String feederRestServiceUrl;
    private boolean debugProcessing;

    @Override
    public String getFeederRestServiceUrl() {
        return feederRestServiceUrl;
    }

    @Override
    public void setFeederRestServiceUrl(final String feederRestServiceUrl) {
        this.feederRestServiceUrl = feederRestServiceUrl;
    }

    @Override
    public boolean getDebugProcessing() {
        return debugProcessing;
    }

    @Override
    public void setDebugProcessing(final boolean debugProcessing) {
        this.debugProcessing = debugProcessing;
    }
}
