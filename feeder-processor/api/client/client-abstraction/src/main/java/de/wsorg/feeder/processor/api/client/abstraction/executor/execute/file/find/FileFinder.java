package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.find;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Slf4j
public class FileFinder {
    public String getFilePathOfFile(final String fileNamePattern) {
        final List<String> foundFiles = new ArrayList<>();


        File current = new File(".");
        while (current.getAbsoluteFile() != null && current.getAbsolutePath().contains("feeder-processor")) {
            current = current.getAbsoluteFile().getParentFile();
        }

        log.debug("Looking for jar-file to execute in:{}", current.getAbsolutePath());

        Path startDir = Paths.get(current.getAbsolutePath());

        FileSystem fs = FileSystems.getDefault();
        final PathMatcher matcher = fs.getPathMatcher("glob:" + fileNamePattern);

        FileVisitor<Path> matcherVisitor = getMatchFilter(foundFiles, matcher);

        executeSearch(startDir, matcherVisitor);

        if (foundFiles.size() > 0) {
            final String jarFile = foundFiles.get(0);
            log.debug("Found jar to execute: {}", jarFile);
            return jarFile;
        } else {
            return null;
        }
    }

    private FileVisitor<Path> getMatchFilter(final List<String> foundFiles, final PathMatcher matcher) {
        return new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attribs) {
                    Path name = file.getFileName();
                    if (matcher.matches(name)) {
                        foundFiles.add(file.toAbsolutePath().toString());
                    }
                    return FileVisitResult.CONTINUE;
                }
            };
    }

    private void executeSearch(final Path startDir, final FileVisitor<Path> matcherVisitor) {
        try {
            Files.walkFileTree(startDir, matcherVisitor);
        } catch (IOException e) {
            throw new RuntimeException("Error while looking for files in project.", e);
        }
    }
}
