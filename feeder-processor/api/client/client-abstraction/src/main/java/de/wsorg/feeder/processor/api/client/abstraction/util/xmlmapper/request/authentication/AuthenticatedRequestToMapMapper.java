package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.authentication;

import java.util.Map;

import org.aspectj.lang.ProceedingJoinPoint;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AuthenticatedRequestToMapMapper {

    public Object invoke(final ProceedingJoinPoint methodInvocation) throws Throwable{
        Map<String, String> processingResult = (Map<String, String>) methodInvocation.proceed(methodInvocation.getArgs());

        if (methodInvocation.getArgs()[0] instanceof AuthorizedRequest) {
            AuthorizedRequest requestParam = (AuthorizedRequest) methodInvocation.getArgs()[0];

            processingResult.put("userId", requestParam.getUserId());
            processingResult.put("userName", requestParam.getUserName());
            processingResult.put("password", requestParam.getPassword());
        }

        return processingResult;
    }
}
