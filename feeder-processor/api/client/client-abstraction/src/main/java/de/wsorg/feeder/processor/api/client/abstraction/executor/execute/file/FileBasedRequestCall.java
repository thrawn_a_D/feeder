package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file;

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.FeederRequestCall;
import de.wsorg.feeder.processor.api.client.abstraction.util.FileHandler;
import de.wsorg.feeder.processor.api.client.abstraction.util.MainExecutorProxy;
import de.wsorg.feeder.processor.api.domain.FeederCommand;

import javax.inject.Inject;
import java.io.File;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedRequestCall<T> implements FeederRequestCall<T> {
    private static final String DEFAULT_REQUEST_FILE_NAME="request.xml";
    private static final String DEFAULT_RESPONSE_FILE_NAME="response.xml";
    private static final String DEFAULT_FOLDER =System.getProperty("java.io.tmpdir");

    private MainExecutorProxy mainExecutorProxy;

    @Inject
    protected FileHandler fileHandler;
    @Inject
    protected RequestObjectToFileTransitionHelper requestObjectToFileTransitionHelper;

    @Override
    public String processRequestCall(final FeederCommand commandToExecute, final T requestParams) {
        writeRequestToFile(commandToExecute, requestParams);
        String[] executionArguments = getExecutionArguments(commandToExecute);
        mainExecutorProxy.invoke(executionArguments);
        return getResponseXmlFromFile(commandToExecute);
    }

    protected String[] getExecutionArguments(final FeederCommand commandToExecute) {
        String[] executionArguments = new String[3];
        executionArguments[0] = commandToExecute.toString();
        executionArguments[1] = getDefaultRequestFile(commandToExecute);
        executionArguments[2] = getDefaultResponseFile(commandToExecute);
        return executionArguments;
    }

    private void writeRequestToFile(final FeederCommand commandToExecute, final T requestObject) {
        final String requestToWrite = requestObjectToFileTransitionHelper.transformObjectToWritableString(requestObject);
        fileHandler.write(getDefaultRequestFile(commandToExecute), requestToWrite);
    }

    protected String getResponseXmlFromFile(final FeederCommand commandToExecute) {
        return fileHandler.read(getDefaultResponseFile(commandToExecute));
    }

    protected String getDefaultResponseFile(final FeederCommand commandToExecute) {
        return DEFAULT_FOLDER + File.separator + commandToExecute + "-" + DEFAULT_RESPONSE_FILE_NAME;
    }

    protected String getDefaultRequestFile(final FeederCommand commandToExecute) {
        return DEFAULT_FOLDER + File.separator + commandToExecute + "-" + DEFAULT_REQUEST_FILE_NAME;
    }

    public void setMainExecutorProxy(final MainExecutorProxy mainExecutorProxy) {
        this.mainExecutorProxy = mainExecutorProxy;
    }
}
