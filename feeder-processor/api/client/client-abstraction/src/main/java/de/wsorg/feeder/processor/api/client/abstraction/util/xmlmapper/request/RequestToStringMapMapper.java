package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request;

import de.wsorg.feeder.processor.api.domain.request.Request;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface RequestToStringMapMapper<T extends Request> extends RequestModelToTransferMapper<Map, T> {
}
