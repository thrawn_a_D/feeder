package de.wsorg.feeder.processor.api.client.abstraction.util;


/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface MainExecutorProxy {
    void invoke(final String... executionArguments);
}
