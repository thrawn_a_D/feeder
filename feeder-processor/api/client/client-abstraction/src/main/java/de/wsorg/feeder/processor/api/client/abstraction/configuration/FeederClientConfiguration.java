package de.wsorg.feeder.processor.api.client.abstraction.configuration;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederClientConfiguration {
    String getFeederRestServiceUrl();
    void setFeederRestServiceUrl(final String feederRestServiceUrl);
    boolean getDebugProcessing();
    void  setDebugProcessing(final boolean debugProcessing);
}
