package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.wrapper;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class HttpClientWrapper {
    public HttpClient getHttpClient() {
        return new DefaultHttpClient();
    }
}
