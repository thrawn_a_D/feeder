package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http;

import org.apache.http.client.methods.HttpRequestBase;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface HttpRequestParamBuilder<T> {
    HttpRequestBase buildHttpRequest(final T requestParams);
}
