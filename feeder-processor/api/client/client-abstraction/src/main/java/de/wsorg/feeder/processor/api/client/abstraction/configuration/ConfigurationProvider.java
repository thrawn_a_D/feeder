package de.wsorg.feeder.processor.api.client.abstraction.configuration;

import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ConfigurationProvider {

    @Inject
    private ApplicationContext applicationContext;

    public FeederClientConfiguration getFeederClientConfiguration() {
        try {
            return applicationContext.getBean(FeederClientConfiguration.class);
        } catch (RuntimeException e) {
            throw new IllegalStateException("No client configuration provided! Please provide a valid configuration first.");
        }
    }
}
