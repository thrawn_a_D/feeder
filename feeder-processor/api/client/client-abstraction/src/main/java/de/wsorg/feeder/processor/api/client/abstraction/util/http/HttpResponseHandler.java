package de.wsorg.feeder.processor.api.client.abstraction.util.http;

import org.apache.http.HttpResponse;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface HttpResponseHandler {
    public String getResponseAsString(HttpResponse response);
}
