package de.wsorg.feeder.processor.api.client.abstraction.exception;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederProcessingException extends RuntimeException {
    public FeederProcessingException() {
    }

    public FeederProcessingException(final String message) {
        super(message);
    }
}
