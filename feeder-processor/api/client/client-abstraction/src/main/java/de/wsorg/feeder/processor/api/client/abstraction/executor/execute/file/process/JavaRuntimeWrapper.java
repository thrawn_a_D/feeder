package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.process;

import javax.inject.Named;
import java.io.IOException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class JavaRuntimeWrapper {
    public Process execCommand(String command) throws IOException {
        return Runtime.getRuntime().exec(command);
    }
}
