package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class RequestObjectToFileTransitionHelper {
    public String transformObjectToWritableString(final Object objectToTransform) {
        String transformationResult = "";

        validateInputValues(objectToTransform);

        if(objectToTransform instanceof String)
            transformationResult = (String) objectToTransform;
        else if(objectToTransform instanceof Map) {
            try {
                Map<String, String> transformValues = (Map<String, String>) objectToTransform;
                for (final String requestParamName : transformValues.keySet()) {
                    final String requestParamValue = transformValues.get(requestParamName);

                    transformationResult += requestParamName + "@" + requestParamValue + ";\n";
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("The provided request param object needs to be either String or Map<String, String>.");
            }
        }

        return transformationResult;
    }

    private void validateInputValues(final Object objectToTransform) {
        if(!(objectToTransform instanceof String ||
             objectToTransform instanceof Map)) {
            throw new IllegalArgumentException("The provided request param object needs to be either String or Map<String, String>.");
        }
    }
}
