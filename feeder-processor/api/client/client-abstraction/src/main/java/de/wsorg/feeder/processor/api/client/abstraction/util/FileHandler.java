package de.wsorg.feeder.processor.api.client.abstraction.util;

import javax.inject.Named;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FileHandler {


    public String read(final String filePath){
        Path filePathToRead = Paths.get(filePath);
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = Files.newBufferedReader(filePathToRead, Charset.forName("UTF-8"));
        } catch (IOException e) {
            throw new IllegalArgumentException("Not able to read the provided file path:" + filePath);
        }

        String readLine;
        String fileContent="";
        try {
            while ((readLine = bufferedReader.readLine()) != null) {
                fileContent += readLine;
            }
        } catch (IOException e) {
            throw new IllegalArgumentException("Not able to read the provided file path:" + filePath);
        }

        try {
            bufferedReader.close();
        } catch (IOException e) {
            throw new IllegalArgumentException("Not able to close the provided file stream:" + filePath);
        }

        return fileContent;
    }


    public void write(final String filePath, final String contentToWrite) {
        Path fileToWriteTo= createOrOverwriteFile(filePath);

        try {
            Files.write(fileToWriteTo, contentToWrite.getBytes(), StandardOpenOption.WRITE);
        } catch (IOException e) {
            String invalidFileMessage = "Invalid file path was given or insufficient writing privileges: " + filePath;
            throw new IllegalArgumentException(invalidFileMessage, e);
        }
    }

    private Path createOrOverwriteFile(final String filePath) {
        Path fileToWriteTo=Paths.get(filePath);

        if(Files.exists(fileToWriteTo)) {
            deleteFile(filePath, fileToWriteTo);
        }
        fileToWriteTo = createFile(filePath, fileToWriteTo);

        return fileToWriteTo;
    }

    private void deleteFile(final String filePath, final Path fileToWriteTo) {
        try {
            Files.delete(fileToWriteTo);
        } catch (IOException e) {
            String invalidFileMessage = "File could not be deleted: " + filePath;
            throw new IllegalArgumentException(invalidFileMessage, e);
        }
    }

    private Path createFile(final String filePath, Path fileToWriteTo) {
        try {
            fileToWriteTo = Files.createFile(fileToWriteTo);
        } catch (IOException e) {
            String invalidFileMessage = "Invalid file path was given or insufficient writing privileges: " + filePath;
            throw new IllegalArgumentException(invalidFileMessage, e);
        }
        return fileToWriteTo;
    }
}
