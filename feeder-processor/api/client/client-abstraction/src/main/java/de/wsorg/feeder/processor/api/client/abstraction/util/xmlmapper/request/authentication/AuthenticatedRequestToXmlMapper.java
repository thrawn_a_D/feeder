package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.authentication;

import org.aspectj.lang.ProceedingJoinPoint;

import de.wsorg.feeder.processor.api.domain.generated.request.generic.authentication.AuthenticatedRequestType;
import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AuthenticatedRequestToXmlMapper {
    private static final String JAXB_MARSHALL_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.request.generic.authentication";

    private JaxbMarshaller jaxbMarshaller;

    public Object invoke(final ProceedingJoinPoint methodInvocation) throws Throwable{
        final String actualRequestXml = (String) methodInvocation.proceed(methodInvocation.getArgs());

        if(methodInvocation.getArgs()[0] instanceof AuthorizedRequest) {
            AuthorizedRequest authorizedRequest = (AuthorizedRequest) methodInvocation.getArgs()[0];
            AuthenticatedRequestType authenticatedRequestType = new AuthenticatedRequestType();
            authenticatedRequestType.setActualRequestContent(actualRequestXml);
            authenticatedRequestType.setUserName(authorizedRequest.getUserName());
            authenticatedRequestType.setPassword(authorizedRequest.getPassword());
            authenticatedRequestType.setUserId(authorizedRequest.getUserId());

            return getJaxbMarshaller().marshal(authenticatedRequestType);
        } else
            return actualRequestXml;
    }


    private JaxbMarshaller getJaxbMarshaller(){
        if(jaxbMarshaller == null){
            jaxbMarshaller = UtilsFactory.getJaxbMarshaller();
            jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_MARSHALL_PACKAGE);
        }

        return jaxbMarshaller;
    }
}
