package de.wsorg.feeder.processor.api.client.abstraction;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ClientAbstractionFactory {

    private static ClassPathXmlApplicationContext applicationContext;

    static {
        final String[] springContextFiles = {"classpath:spring/client-abstraction-spring-context.xml"};
        applicationContext = new ClassPathXmlApplicationContext(springContextFiles);
    }

    public static XmlToDomainMapper<GenericResponseResult> getAbstractResponseMapper(){
        return applicationContext.getBean("genericProcessingResultToResultMapper", XmlToDomainMapper.class);
    }
}
