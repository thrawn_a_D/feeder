package de.wsorg.feeder.processor.api.client.abstraction.executor;


import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.response.Response;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederRequestProcessor<T extends Request, F extends Response> {
    F executeRequest(final T requestOption);
}
