package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.response.concrete;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapperTemplate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GenericProcessingResultToDomainMapper extends XmlToDomainMapperTemplate<GenericResponseResult, JAXBElement> {

    private static final String JAXB_GENERATED_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.response.generic.processResult";

    @Inject
    @Named(value = "genericResultMapper")
    private JaxbPojoToFeederPojoMapper<GenericResponseResult, JAXBElement> jaxbPojoToFeederPojoMapper;

    @Override
    protected String getGeneratedJaxbClassesPackage() {
        return JAXB_GENERATED_PACKAGE;
    }

    @Override
    protected JaxbPojoToFeederPojoMapper<GenericResponseResult, JAXBElement> getJaxbPojoToFeederPojoMapper() {
        return jaxbPojoToFeederPojoMapper;
    }
}
