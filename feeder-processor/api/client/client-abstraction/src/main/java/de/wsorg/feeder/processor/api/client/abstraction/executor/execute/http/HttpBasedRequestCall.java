package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http;

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.FeederRequestCall;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.wrapper.HttpClientWrapper;
import de.wsorg.feeder.processor.api.client.abstraction.util.http.HttpResponseHandler;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;

import javax.inject.Inject;
import java.io.IOException;


/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class HttpBasedRequestCall<T> implements FeederRequestCall<T> {
    @Inject
    private HttpRequestParamBuilderResolver httpRequestParamBuilderResolver;
    @Inject
    private HttpClientWrapper httpClientWrapper;
    @Inject
    private HttpResponseHandler httpResponseHandler;

    @Override
    public String processRequestCall(final FeederCommand commandToExecute, final T requestParams) {

        final HttpRequestParamBuilder<T> parameterBuilder = httpRequestParamBuilderResolver.findHttpRequestParamBuilder(commandToExecute);
        final HttpRequestBase httpRequestParams = parameterBuilder.buildHttpRequest(requestParams);

        final HttpClient httpClient = httpClientWrapper.getHttpClient();

        HttpResponse response=null;
        try {
            response = httpClient.execute(httpRequestParams);
        } catch (IOException e) {
            final String errorMessage = "Error while executing call to feeder rest API:" + e.getMessage();
            throw new RuntimeException(errorMessage);
        }

        final int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode != 200) {
            final String message = "An error occurred while communicating with the feeder service. Response code was: " + statusCode;
            throw new RuntimeException(message);
        }

        return httpResponseHandler.getResponseAsString(response);
    }
}
