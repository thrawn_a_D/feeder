package de.wsorg.feeder.processor.api.client.abstraction.util.http;

import org.apache.http.HttpResponse;

import javax.inject.Named;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class SimpleHttpResponseHandler implements HttpResponseHandler {
    @Override
    public String getResponseAsString(final HttpResponse response) {
        String result = "";
        try {
            // Get the response
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
        } catch (IOException e) {
            final String errorMessage = "Error while reading http response content: " + e.getMessage();
            throw new RuntimeException(errorMessage, e);
        }

        return result;
    }
}
