package de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http;

import de.wsorg.feeder.processor.api.domain.FeederCommand;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface HttpRequestParamBuilderResolver {
    public HttpRequestParamBuilder findHttpRequestParamBuilder(final FeederCommand feederCommand);
}
