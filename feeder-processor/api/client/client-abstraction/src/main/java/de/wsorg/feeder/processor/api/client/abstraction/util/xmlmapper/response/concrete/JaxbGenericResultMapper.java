package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.response.concrete;

import de.wsorg.feeder.processor.api.domain.generated.response.generic.processResult.FeederProcessingResultType;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JaxbGenericResultMapper implements JaxbPojoToFeederPojoMapper<GenericResponseResult, JAXBElement> {
    @Override
    public GenericResponseResult mapJaxbPojoToFeederPojo(final JAXBElement objectToMap) {
        validateInput(objectToMap);

        GenericResponseResult mappedResult = new GenericResponseResult();

        FeederProcessingResultType handleFeedResponseType = (FeederProcessingResultType) objectToMap.getValue();
        ProcessingResult processResult = getProcessResult(handleFeedResponseType);

        mappedResult.setMessage(handleFeedResponseType.getMessage());
        mappedResult.setProcessingResult(processResult);


        return mappedResult;
    }

    private void validateInput(final JAXBElement objectToMap) {
        if(!(objectToMap.getValue() instanceof FeederProcessingResultType)){
            String errorMessage = "The provided jaxb element does not contain an object of type FeederProcessingResultType";
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private ProcessingResult getProcessResult(final FeederProcessingResultType handleFeedResponseType) {
        String actualProcessResultAsString = handleFeedResponseType.getProcessResult().value();
        return ProcessingResult.valueOf(actualProcessResultAsString);
    }
}
