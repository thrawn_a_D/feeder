package de.wsorg.feeder.processor.api.client.abstraction.executor.execute;

import de.wsorg.feeder.processor.api.domain.FeederCommand;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederRequestCall<T> {
    String processRequestCall(final FeederCommand commandToExecute, final T requestParams);
}
