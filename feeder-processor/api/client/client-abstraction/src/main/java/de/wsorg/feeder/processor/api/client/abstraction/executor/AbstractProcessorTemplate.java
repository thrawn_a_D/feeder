package de.wsorg.feeder.processor.api.client.abstraction.executor;

import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.FeederRequestCall;
import de.wsorg.feeder.processor.api.client.abstraction.util.FileHandler;
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestModelToTransferMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.response.Response;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class AbstractProcessorTemplate<T extends Request, F extends Response> implements FeederRequestProcessor<T, F> {
    @Inject
    protected FileHandler fileHandler;

    @Inject
    @Named("genericProcessingResultToResultMapper")
    protected XmlToDomainMapper<GenericResponseResult> genericResponseMapper;

    @Inject
    protected FeederRequestCall feederRequestCall;

    protected abstract RequestModelToTransferMapper<?,T> getRequestToTransferMapper();
    protected abstract XmlToDomainMapper<F> getXmlToDomainMapper();
    protected abstract FeederCommand getCommandToExecute();


    public F executeRequest(final T request){
        F executionResult = (F) new Response(){};

        if(request != null){
            final Object ourRequest = getRequestToTransferMapper().getRequestData(request);

            final String responseXml = feederRequestCall.processRequestCall(getCommandToExecute(), ourRequest);

            executionResult = getResponseModel(responseXml);
        }

        return executionResult;
    }

    private F getResponseModel(final String responseXml) {
        final F searchResult;
        searchResult = mapResponseOrThrowPotentialException(responseXml);

        return searchResult;
    }

    private F mapResponseOrThrowPotentialException(final String responseXml) {
        F searchResult = tryToProcessGenericResponse(responseXml);

        if(searchResult == null)
            searchResult = mapResponseWithActualMapper(responseXml);

        return searchResult;
    }

    private F mapResponseWithActualMapper(final String responseXml) {
        return getXmlToDomainMapper().getResult(responseXml);
    }

    private F tryToProcessGenericResponse(final String responseXml) {
        F searchResult;
        GenericResponseResult mappingToGenericResponseResult;

        try {
            mappingToGenericResponseResult = genericResponseMapper.getResult(responseXml);
        } catch (RuntimeException e) {
            mappingToGenericResponseResult = null;
        }

        if(isGenericResult(mappingToGenericResponseResult)) {
            if(isResultErroneous(mappingToGenericResponseResult)) {
                throw new FeederProcessingException(mappingToGenericResponseResult.getMessage());
            } else if(isSuccessExpected(mappingToGenericResponseResult)) {
                searchResult = (F) mappingToGenericResponseResult;
            } else {
                throw new RuntimeException("Positive generic response received but wanted something different");
            }
        } else {
            searchResult = null;
        }

        return searchResult;
    }

    private boolean isGenericResult(final GenericResponseResult mappingToGenericResponseResult) {
        return mappingToGenericResponseResult != null;
    }

    private boolean isSuccessExpected(final GenericResponseResult mappingToGenericResponseResult) {
        return mappingToGenericResponseResult.getProcessingResult() == ProcessingResult.SUCCESS &&
                getXmlToDomainMapper().getClass().equals(genericResponseMapper.getClass());
    }

    private boolean isResultErroneous(final GenericResponseResult mappingToGenericResponseResult) {
        return mappingToGenericResponseResult.getProcessingResult() == ProcessingResult.FAILED;
    }
}
