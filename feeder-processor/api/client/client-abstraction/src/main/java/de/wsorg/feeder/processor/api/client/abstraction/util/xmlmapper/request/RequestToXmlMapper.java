package de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request;

import de.wsorg.feeder.processor.api.domain.request.Request;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface RequestToXmlMapper<T extends Request> extends RequestModelToTransferMapper<String, T> {
}
