package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling;


import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.08.12
 */
public class FeedSubscriptionRequestProcessorTest extends Specification {

    FeedSubscriptionRequestProcessor handleFeedRequestProcessor

    def setup(){
        handleFeedRequestProcessor = new FeedSubscriptionRequestProcessor()
    }

    def "Make sure the right request to xml mapper is returned"() {
        given:
        RequestToXmlMapper requestToXmlMapperMock = Mock(RequestToXmlMapper)
        handleFeedRequestProcessor.requestToXmlMapper = requestToXmlMapperMock

        when:
        def usedMapper = handleFeedRequestProcessor.getRequestToTransferMapper()

        then:
        usedMapper == requestToXmlMapperMock
    }
}
