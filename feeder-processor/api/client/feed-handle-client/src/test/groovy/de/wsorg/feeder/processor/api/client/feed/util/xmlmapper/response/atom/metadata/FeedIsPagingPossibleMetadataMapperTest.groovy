package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.metadata

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class FeedIsPagingPossibleMetadataMapperTest extends Specification {
    FeedIsPagingPossibleMetadataMapper feedIsPagingPossible

    def setup(){
        feedIsPagingPossible = new FeedIsPagingPossibleMetadataMapper()
    }

    def "Map paging status from jaxb to a feed"() {
        given:
        def atom = new FeederAtom()
        def qname = new QName('IS_PAGING_POSSIBLE')
        def pagingPossible = true

        when:
        feedIsPagingPossible.mapFeedMetadata(atom, qname, String.valueOf(pagingPossible))

        then:
        atom.additionalPagingPossible == pagingPossible
    }

    def "Do nothing if not the right identifier is provided"() {
        given:
        def atom = new FeederAtom()
        def qname = new QName('asd')

        when:
        feedIsPagingPossible.mapFeedMetadata(atom, qname, null)

        then:
        atom.additionalPagingPossible == false
    }
}
