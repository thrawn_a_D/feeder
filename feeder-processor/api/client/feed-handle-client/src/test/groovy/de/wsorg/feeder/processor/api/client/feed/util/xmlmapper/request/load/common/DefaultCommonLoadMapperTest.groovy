package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common

import de.wsorg.feeder.processor.production.domain.feeder.PagingRange
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.01.13
 */
class DefaultCommonLoadMapperTest extends Specification {
    DefaultCommonLoadMapper defaultCommonLoadMapper
    
    def setup(){
        defaultCommonLoadMapper = new DefaultCommonLoadMapper()
    }

    def "Map a common load feed model to a jaxb model object"() {
        given:
        def loadModel = new LoadFeed()
        loadModel.setFeedUrl('feedUrl')
        def range = new PagingRange()
        range.setEndIndex(1)
        range.setEndIndex(2)
        loadModel.setFeedEntryPagingRange(range)

        when:
        def result = defaultCommonLoadMapper.mapCommonLoadObject(loadModel)

        then:
        result['feedUrl'] == loadModel.feedUrl
        result['pageRangeStartIndex'] == loadModel.feedEntryPagingRange.startIndex.toString()
        result['pageRangeEndIndex'] == loadModel.feedEntryPagingRange.endIndex.toString()
    }

    def "No page range was provided, use default"() {
        given:
        def loadModel = new LoadFeed()
        loadModel.setFeedUrl('feedUrl')
        loadModel.setFeedEntryPagingRange(null)

        when:
        def result = defaultCommonLoadMapper.mapCommonLoadObject(loadModel)

        then:
        result['feedUrl'] == loadModel.feedUrl
        result['pageRangeStartIndex'] == '1'
        result['pageRangeEndIndex'] == Integer.MAX_VALUE.toString()
    }
}
