package de.wsorg.feeder.processor.api.client.feed.util.http.search

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper.UrlEncodingWrapper
import de.wsorg.feeder.processor.api.client.feed.util.http.url.HttpParamCalculator
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 03.02.13
 */
class UserRelatedSearchRequestParamBuilderTest extends Specification {
    UserRelatedSearchRequestParamBuilder userRelatedSearchRequestParamBuilder
    ConfigurationProvider configurationProvider
    UrlEncodingWrapper urlEncodingWrapper
    HttpParamCalculator optionalParamCalculator

    def setup(){
        userRelatedSearchRequestParamBuilder = new UserRelatedSearchRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        urlEncodingWrapper = Mock(UrlEncodingWrapper)
        optionalParamCalculator = Mock(HttpParamCalculator)
        userRelatedSearchRequestParamBuilder.configurationProvider = configurationProvider
        userRelatedSearchRequestParamBuilder.urlEncodingWrapper = urlEncodingWrapper
        userRelatedSearchRequestParamBuilder.httpParamCalculator = optionalParamCalculator
    }

    def "Build request method for a load command"() {
        given:
        def searchTerm = 'asdasd'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def requestParamMap = [searchTerm: searchTerm, userId:userId, userName: userName, password:password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def encodedSearchTerm = 'asdkm'

        and:
        def calculatedParams = 'asdqwdasd'

        and:
        def expectedUrl = serviceUrl + "feeds/${userId}/subscriptions${calculatedParams}"

        when:
        HttpGet getRequest = userRelatedSearchRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == expectedUrl
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * urlEncodingWrapper.encode({it == searchTerm}, UrlEncodingWrapper.DEFAULT_ENCODING) >> encodedSearchTerm
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedParams
    }

    def "Provide unread entries attribute"() {
        given:
        def searchTerm = 'asdasd'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def showUnreadEntries = 'true'
        def requestParamMap = [searchTerm: searchTerm,
                userId:userId,
                userName: userName,
                password:password,
                showUnreadEntries: showUnreadEntries]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def encodedSearchTerm = 'asdkm'

        and:
        def calculatedParams = 'asdqwdasd'

        and:
        def expectedUrl = serviceUrl + "feeds/${userId}/subscriptions${calculatedParams}"

        when:
        HttpGet getRequest = userRelatedSearchRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == expectedUrl
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * urlEncodingWrapper.encode({it == searchTerm}, UrlEncodingWrapper.DEFAULT_ENCODING) >> encodedSearchTerm
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedParams
    }

    def "Url encoding went wrong"() {
        given:
        def searchTerm = 'asdasd'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def requestParamMap = [searchTerm: searchTerm, userId:userId, userName: userName, password:password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        configurationProvider.getFeederClientConfiguration() >> configuration
        configuration.getFeederRestServiceUrl() >> serviceUrl
        urlEncodingWrapper.encode(_, _) >> {throw new UnsupportedEncodingException()}

        when:
        userRelatedSearchRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'Could not build a request url out of provided arguments.'
    }
}
