package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.global;


import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper
import de.wsorg.feeder.processor.api.client.feed.executor.ConcreteExecutorTestTemplate
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.07.12
 */
public class GlobalSearchRequestProcessorTest extends ConcreteExecutorTestTemplate {

    GlobalSearchRequestProcessor searchRequestProcessor;

    RequestToStringMapMapper requestToXmlMapperMock;

    def setup(){
        searchRequestProcessor = new GlobalSearchRequestProcessor();

        requestToXmlMapperMock = Mock(RequestToStringMapMapper)

        searchRequestProcessor.requestToXmlMapper = requestToXmlMapperMock
    }

    def "Check generics type of request to xml mapper"() {
        given:
        def fieldName = "requestToXmlMapper"
        def expectedTypeToUse = GlobalSearchQueryRequest

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, GlobalSearchRequestProcessor)

        then:
        genericsType == expectedTypeToUse
    }

    def "Make sure a proper mapper is provided"() {
        when:
        def mapper = searchRequestProcessor.getRequestToTransferMapper()

        then:
        mapper == requestToXmlMapperMock
    }

    def "Command to execute is search"() {

        when:
        def commandToExecute = searchRequestProcessor.getCommandToExecute()

        then:
        commandToExecute == FeederCommand.GLOBAL_SEARCH
    }
}
