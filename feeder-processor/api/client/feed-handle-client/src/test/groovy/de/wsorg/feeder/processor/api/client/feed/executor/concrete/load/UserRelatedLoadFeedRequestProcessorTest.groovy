package de.wsorg.feeder.processor.api.client.feed.executor.concrete.load

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper
import de.wsorg.feeder.processor.api.client.feed.executor.ConcreteExecutorTestTemplate
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.01.13
 */
class UserRelatedLoadFeedRequestProcessorTest extends ConcreteExecutorTestTemplate {
    UserRelatedLoadFeedRequestProcessor loadRequestProcessor;

    RequestToStringMapMapper requestToStringMapMapper;
    XmlToDomainMapper responseToResultMapperMock;

    def setup(){
        loadRequestProcessor = new UserRelatedLoadFeedRequestProcessor();

        requestToStringMapMapper = Mock(RequestToStringMapMapper)
        responseToResultMapperMock = Mock(XmlToDomainMapper)

        loadRequestProcessor.requestToXmlMapper = requestToStringMapMapper
        loadRequestProcessor.xmlToDomainMapper = responseToResultMapperMock
    }

    def "Check that getter truly return the right mappers"() {

        when:
        def usedRequestToXmlMapper = loadRequestProcessor.getRequestToTransferMapper()
        def usedResponseToResultMapper = loadRequestProcessor.getXmlToDomainMapper()

        then:
        usedRequestToXmlMapper == requestToStringMapMapper
        usedResponseToResultMapper == responseToResultMapperMock
    }

    def "Check generics type of request to xml mapper"() {
        given:
        def fieldName = "requestToXmlMapper"
        def expectedTypeToUse = UserRelatedLoadFeedRequest

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, UserRelatedLoadFeedRequestProcessor)

        then:
        genericsType == expectedTypeToUse
    }

    def "Check generics type of response to result mapper"() {
        given:
        def fieldName = "xmlToDomainMapper"
        def expectedTypeToUse = FeederAtom

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, UserRelatedLoadFeedRequestProcessor)

        then:
        genericsType == expectedTypeToUse
    }

    def "Command to execute is search"() {

        when:
        def commandToExecute = loadRequestProcessor.getCommandToExecute()

        then:
        commandToExecute == FeederCommand.LOAD_USER_RELATED
    }
}
