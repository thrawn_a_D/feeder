package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling

import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.06.13
 */
class MarkAllFeedsReadabilityRequestToXmlMapperTest extends Specification {
    MarkAllFeedsReadabilityRequestToXmlMapper markAllFeedsReadabilityRequestToXmlMapper

    def setup(){
        markAllFeedsReadabilityRequestToXmlMapper = new MarkAllFeedsReadabilityRequestToXmlMapper()
    }

    def "Map domain class to mark all feeds as read"() {
        given:
        def request = new MarkAllFeedsReadabilityRequest()
        request.feedsReadStatus = true
        request.category = 'someCategory'

        when:
        def result = markAllFeedsReadabilityRequestToXmlMapper.getRequestData(request)
        def resultAsXmlSlurper = new XmlSlurper().parseText(result)

        then:
        resultAsXmlSlurper != ''
        resultAsXmlSlurper.'markFeedsAsReadType' == request.feedsReadStatus
        resultAsXmlSlurper.'category' == request.category
    }
}
