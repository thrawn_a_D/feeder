package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling;


import de.wsorg.feeder.processor.api.domain.generated.request.handler.HandleFeedType
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.08.12
 */
public class FeedSubscriptionRequestToXmlMapperTest extends Specification {

    FeedSubscriptionRequestToXmlMapper handleRequestToXmlMapper
    JaxbMarshaller jaxbMarshallerMock

    def setup(){
        handleRequestToXmlMapper = new FeedSubscriptionRequestToXmlMapper()
        jaxbMarshallerMock = Mock(JaxbMarshaller)
        handleRequestToXmlMapper.jaxbMarshaller = jaxbMarshallerMock
    }

    def "Map handle request object to an xml"() {
        given:
        def handleFeedRequest = new FeedSubscriptionRequest(
                feedUrl: 'http://feedUrl.de/feed',
                favourFeed: true)

        and:
        def expectedResult = "<someXml>"

        when:
        def result = handleRequestToXmlMapper.getRequestData(handleFeedRequest);

        then:
        result == expectedResult
        1 * jaxbMarshallerMock.marshal({HandleFeedType mappedObject ->
            mappedObject.feedUrl == handleFeedRequest.feedUrl &&
            mappedObject.favour == handleFeedRequest.favourFeed
        }) >> expectedResult
    }


    def "Make sure the marshaller gets the right jaxb package"() {
        given:
        def handleFeedRequest = new FeedSubscriptionRequest(
                feedUrl: 'http://feedUrl.de/feed',
                favourFeed: true)
        def expectedPackage = "de.wsorg.feeder.processor.api.domain.generated.request.handler"

        when:
        handleRequestToXmlMapper.getRequestData(handleFeedRequest);

        then:
        1 * jaxbMarshallerMock.setJaxbGeneratedClassesPackage(expectedPackage)
    }
}
