package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.global

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper
import de.wsorg.feeder.processor.api.client.feed.executor.ConcreteExecutorTestTemplate
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class GlobalSearchWithUserDataRequestProcessorTest extends ConcreteExecutorTestTemplate {

    GlobalSearchWithUserDataRequestProcessor globalSearchWithUserDataRequestProcessor;

    RequestToStringMapMapper requestToStringMapMapper;

    def setup(){
        globalSearchWithUserDataRequestProcessor = new GlobalSearchWithUserDataRequestProcessor();

        requestToStringMapMapper = Mock(RequestToStringMapMapper)

        globalSearchWithUserDataRequestProcessor.requestToStringMapMapper = requestToStringMapMapper
    }

    def "Check generics type of request to xml mapper"() {
        given:
        def fieldName = "requestToStringMapMapper"
        def expectedTypeToUse = GlobalSearchQueryEnrichedWithUserDataRequest

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, GlobalSearchWithUserDataRequestProcessor)

        then:
        genericsType == expectedTypeToUse
    }

    def "Make sure a proper mapper is provided"() {
        when:
        def mapper = globalSearchWithUserDataRequestProcessor.getRequestToTransferMapper()

        then:
        mapper == requestToStringMapMapper
    }

    def "Command to execute is search"() {

        when:
        def commandToExecute = globalSearchWithUserDataRequestProcessor.getCommandToExecute()

        then:
        commandToExecute == FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA
    }

}
