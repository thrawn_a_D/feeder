package de.wsorg.feeder.processor.api.client.feed.util.http

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder
import de.wsorg.feeder.processor.api.domain.FeederCommand
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
class FeedHttpRequestParamBuilderResolverTest extends Specification {
    FeedHttpRequestParamBuilderResolver feedHttpRequestParamBuilderResolver
    HttpRequestParamBuilder loadFeedRequestParamBuilder
    HttpRequestParamBuilder loadFeedForUserRequestParamBuilder
    HttpRequestParamBuilder globalSearchRequestParamBuilder
    HttpRequestParamBuilder globalSearchForUserRequestParamBuilder
    HttpRequestParamBuilder subscriptionRequestParamBuilder
    HttpRequestParamBuilder markFeedEntryReadabilityRequestParamBuilder
    HttpRequestParamBuilder markAllFeedsReadabilityRequestParamBuilder
    HttpRequestParamBuilder userRelatedSearchRequestParamBuilder

    def setup(){
        feedHttpRequestParamBuilderResolver = new FeedHttpRequestParamBuilderResolver()
        loadFeedRequestParamBuilder = Mock(HttpRequestParamBuilder)
        loadFeedForUserRequestParamBuilder = Mock(HttpRequestParamBuilder)
        globalSearchRequestParamBuilder = Mock(HttpRequestParamBuilder)
        globalSearchForUserRequestParamBuilder = Mock(HttpRequestParamBuilder)
        subscriptionRequestParamBuilder = Mock(HttpRequestParamBuilder)
        markFeedEntryReadabilityRequestParamBuilder = Mock(HttpRequestParamBuilder)
        userRelatedSearchRequestParamBuilder = Mock(HttpRequestParamBuilder)
        markAllFeedsReadabilityRequestParamBuilder = Mock(HttpRequestParamBuilder)
        feedHttpRequestParamBuilderResolver.loadFeedRequestParamBuilder = loadFeedRequestParamBuilder
        feedHttpRequestParamBuilderResolver.loadFeedForUserRequestParamBuilder = loadFeedForUserRequestParamBuilder
        feedHttpRequestParamBuilderResolver.globalSearchRequestParamBuilder = globalSearchRequestParamBuilder
        feedHttpRequestParamBuilderResolver.globalSearchForUserRequestParamBuilder = globalSearchForUserRequestParamBuilder
        feedHttpRequestParamBuilderResolver.subscriptionRequestParamBuilder = subscriptionRequestParamBuilder
        feedHttpRequestParamBuilderResolver.markFeedEntryReadabilityRequestParamBuilder = markFeedEntryReadabilityRequestParamBuilder
        feedHttpRequestParamBuilderResolver.userRelatedSearchRequestParamBuilder = userRelatedSearchRequestParamBuilder
        feedHttpRequestParamBuilderResolver.markAllFeedsReadabilityRequestParamBuilder = markAllFeedsReadabilityRequestParamBuilder
    }

    def "get param builder for loader command"() {
        given:
        def command = FeederCommand.LOAD

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == loadFeedRequestParamBuilder
    }

    def "get param builder for load for user command"() {
        given:
        def command = FeederCommand.LOAD_USER_RELATED

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == loadFeedForUserRequestParamBuilder
    }

    def "get param builder for global search"() {
        given:
        def command = FeederCommand.GLOBAL_SEARCH

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == globalSearchRequestParamBuilder
    }

    def "get param builder for global search for user"() {
        given:
        def command = FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == globalSearchForUserRequestParamBuilder
    }

    def "get param builder for subscription"() {
        given:
        def command = FeederCommand.FAVOUR_FEED

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == subscriptionRequestParamBuilder
    }

    def "get param builder for read entry markage"() {
        given:
        def command = FeederCommand.MARK_FEED_ENTRY_READABILITY

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == markFeedEntryReadabilityRequestParamBuilder
    }

    def "get param builder for feed read markage"() {
        given:
        def command = FeederCommand.MARK_ALL_FEEDS_READABILITY

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == markAllFeedsReadabilityRequestParamBuilder
    }

    def "get param builder for user related search"() {
        given:
        def command = FeederCommand.USER_RELATED_SEARCH

        when:
        def result = feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(command)

        then:
        result == userRelatedSearchRequestParamBuilder
    }

    def "If no param builder is found, throw exception"() {
        when:
        feedHttpRequestParamBuilderResolver.findHttpRequestParamBuilder(FeederCommand.CREATE_USER)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'Could not find a related http parameter builder for requested command: ' + FeederCommand.CREATE_USER
    }
}
