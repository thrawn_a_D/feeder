package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search

import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserSearchRestriction
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.10.12
 */
class UserRelatedSearchRequestToMapMapperTest extends Specification {

    UserRelatedSearchRequestToMapMapper searchRequestMapper

    def setup(){
        searchRequestMapper = new UserRelatedSearchRequestToMapMapper()
    }

    def "Generate xml out of a search request model"() {
        given:
        final UserRelatedSearchQueryRequest searchQueryRequest = new UserRelatedSearchQueryRequest(searchTerm: 'some query')

        when:
        def generatedMap = searchRequestMapper.getRequestData(searchQueryRequest);

        then:
        generatedMap.searchTerm == searchQueryRequest.searchTerm
    }

    def "Provide an empty search term"() {
        given:
        final UserRelatedSearchQueryRequest searchQueryRequest = new UserRelatedSearchQueryRequest()

        when:
        def generatedMap = searchRequestMapper.getRequestData(searchQueryRequest)

        then:
        generatedMap != null
        generatedMap.size() == 0
    }

    def "Make sure restrictions are mapped correctly"() {
        given:
        final UserRelatedSearchQueryRequest searchQueryRequest = new UserRelatedSearchQueryRequest(searchTerm: 'some query')
        searchQueryRequest.addRestriction(UserSearchRestriction.UNREAD_ENTRIES, true);


        when:
        def xmlResult = searchRequestMapper.getRequestData(searchQueryRequest)

        then:
        xmlResult.showUnreadEntries == 'true'
    }
}
