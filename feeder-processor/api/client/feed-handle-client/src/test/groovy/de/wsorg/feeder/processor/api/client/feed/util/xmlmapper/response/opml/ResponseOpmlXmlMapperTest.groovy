package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.opml;


import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class ResponseOpmlXmlMapperTest extends Specification {

    OpmlXmlToDomainMapper responseOpmlXmlMapper
    JaxbPojoToFeederPojoMapper jaxbAtomToFeederApiAtom

    def setup(){
        responseOpmlXmlMapper = new OpmlXmlToDomainMapper()
        jaxbAtomToFeederApiAtom = Mock(JaxbPojoToFeederPojoMapper)
        responseOpmlXmlMapper.jaxBOpmlToFeederApiOpml = jaxbAtomToFeederApiAtom;
    }

    def "Check default package is provided correctly"() {
        when:
        def jaxbPackage = responseOpmlXmlMapper.generatedJaxbClassesPackage

        then:
        jaxbPackage == "de.wsorg.feeder.processor.api.domain.generated.response.opml"
    }

    def "Check the jaxb mapper is correct"() {
        when:
        def jaxbMapper = responseOpmlXmlMapper.jaxbPojoToFeederPojoMapper

        then:
        jaxbMapper == jaxbAtomToFeederApiAtom
    }
}
