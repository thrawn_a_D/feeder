package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper
import de.wsorg.feeder.processor.api.client.feed.executor.ConcreteExecutorTestTemplate
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.10.12
 */
class UserRelatedSearchRequestProcessorTest extends ConcreteExecutorTestTemplate {

    UserRelatedSearchRequestProcessor searchRequestProcessor;

    RequestToStringMapMapper requestToStringMapMapper;

    def setup(){
        searchRequestProcessor = new UserRelatedSearchRequestProcessor();

        requestToStringMapMapper = Mock(RequestToStringMapMapper)

        searchRequestProcessor.requestToStringMapMapper = requestToStringMapMapper
    }

    def "Check generics type of request to xml mapper"() {
        given:
        def fieldName = "requestToStringMapMapper"
        def expectedTypeToUse = UserRelatedSearchQueryRequest

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, UserRelatedSearchRequestProcessor)

        then:
        genericsType == expectedTypeToUse
    }

    def "Make sure a proper mapper is provided"() {
        when:
        def mapper = searchRequestProcessor.getRequestToTransferMapper()

        then:
        mapper == requestToStringMapMapper
    }

    def "Command to execute is search"() {

        when:
        def commandToExecute = searchRequestProcessor.getCommandToExecute()

        then:
        commandToExecute == FeederCommand.USER_RELATED_SEARCH
    }
}
