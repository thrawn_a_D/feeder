package de.wsorg.feeder.processor.api.client.feed.executor

import spock.lang.Specification

import java.lang.reflect.Field
import java.lang.reflect.ParameterizedType

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.08.12
 */
class ConcreteExecutorTestTemplate extends Specification {

    def Class<?> getGenericFieldType(String fieldName, Class classToCheck) {
        Field fieldToBeChecked = classToCheck.getDeclaredField(fieldName);
        ParameterizedType fieldToCheckType = (ParameterizedType) fieldToBeChecked.getGenericType();
        Class<?> genericsType = (Class<?>) fieldToCheckType.getActualTypeArguments()[0];
        genericsType
    }
}
