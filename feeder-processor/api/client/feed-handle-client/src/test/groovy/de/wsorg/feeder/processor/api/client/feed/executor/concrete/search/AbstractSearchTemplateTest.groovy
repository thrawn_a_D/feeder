package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestModelToTransferMapper
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.api.client.feed.executor.ConcreteExecutorTestTemplate
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.Request
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.10.12
 */
class AbstractSearchTemplateTest extends ConcreteExecutorTestTemplate {

    AbstractSearchTestClass searchRequestProcessor;

    private static RequestToXmlMapper requestToXmlMapperMock;
    XmlToDomainMapper responseToResultMapperMock;

    def setup(){
        searchRequestProcessor = new AbstractSearchTestClass();

        requestToXmlMapperMock = Mock(RequestToXmlMapper)
        responseToResultMapperMock = Mock(XmlToDomainMapper)

        searchRequestProcessor.xmlToDomainMapper = responseToResultMapperMock
    }

    def "Check that getter truly return the right mappers"() {

        when:
        def usedRequestToXmlMapper = searchRequestProcessor.getRequestToTransferMapper()
        def usedResponseToResultMapper = searchRequestProcessor.getXmlToDomainMapper()

        then:
        usedRequestToXmlMapper == requestToXmlMapperMock
        usedResponseToResultMapper == responseToResultMapperMock
    }

    def "Check generics type of response to result mapper"() {
        given:
        def fieldName = "xmlToDomainMapper"
        def expectedTypeToUse = Opml

        when:
        Class<?> genericsType = getGenericFieldType(fieldName, AbstractSearchTemplate)

        then:
        genericsType == expectedTypeToUse
    }

    private static class AbstractSearchTestClass extends AbstractSearchTemplate {

        @Override
        protected RequestModelToTransferMapper<String, Request> getRequestToTransferMapper() {
            return AbstractSearchTemplateTest.requestToXmlMapperMock;
        }

        @Override
        protected FeederCommand getCommandToExecute() {
            return FeederCommand.GLOBAL_SEARCH;
        }
    }
}
