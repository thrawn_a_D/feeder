package de.wsorg.feeder.processor.api.client.feed.util.http.search.global

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper.UrlEncodingWrapper
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
class GlobalSearchRequestParamBuilderTest extends Specification {
    GlobalSearchRequestParamBuilder globalSearchRequestParamBuilder
    ConfigurationProvider configurationProvider
    UrlEncodingWrapper urlEncodingWrapper

    def setup(){
        globalSearchRequestParamBuilder = new GlobalSearchRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        urlEncodingWrapper = Mock(UrlEncodingWrapper)
        globalSearchRequestParamBuilder.configurationProvider = configurationProvider
        globalSearchRequestParamBuilder.urlEncodingWrapper = urlEncodingWrapper
    }

    def "Build request method for a load command"() {
        given:
        def searchTerm = 'asdqwd'
        def encodedSearchTerm = 'sad23k3'
        def requestParamMap = [searchTerm: searchTerm]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        HttpGet getRequest = globalSearchRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feeds?searchTerm=${encodedSearchTerm}"
        1 * urlEncodingWrapper.encode(searchTerm, UrlEncodingWrapper.DEFAULT_ENCODING) >> encodedSearchTerm
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
    }
}
