package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common

import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class BasicCommonGlobalMapperTest extends Specification {


    BasicCommonGlobalMapper searchRequestMapper

    def setup(){
        searchRequestMapper = new BasicCommonGlobalMapper()
    }

    def "Calculate common global search"() {
        given:
        final GlobalSearchQueryRequest searchQueryRequest = new GlobalSearchQueryRequest(searchTerm: 'some query')

        when:
        def result = searchRequestMapper.getCommonSearchQuery(searchQueryRequest);

        then:
        result.searchTerm == searchQueryRequest.searchTerm
    }

    def "Calculate common global search with no search term provided"() {
        given:
        final GlobalSearchQueryRequest searchQueryRequest = new GlobalSearchQueryRequest(searchTerm: '')

        when:
        def result = searchRequestMapper.getCommonSearchQuery(searchQueryRequest);

        then:
        result.size() == 0
    }
}
