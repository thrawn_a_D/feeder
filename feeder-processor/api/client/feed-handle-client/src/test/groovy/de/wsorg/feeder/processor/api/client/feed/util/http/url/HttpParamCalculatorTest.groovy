package de.wsorg.feeder.processor.api.client.feed.util.http.url

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 03.02.13
 */
class HttpParamCalculatorTest extends Specification {
    HttpParamCalculator optionalParamCalculator

    def setup(){
        optionalParamCalculator = new HttpParamCalculator()
    }

    def "Map a param using a question mark"() {
        given:
        def param='test=test'

        when:
        def result = optionalParamCalculator.calculateOptionalParamString(param)

        then:
        result == "?${param}"
    }

    def "Map two params using a question mark and an (&)"() {
        given:
        def param1='test=test'
        def param2='test=test'

        when:
        def result = optionalParamCalculator.calculateOptionalParamString(param1, param2)

        then:
        result == "?${param1}&${param2}"
    }

    def "Encode single parameters to avoid f.e. url parametrization problems"() {
        given:
        def urlParam='feedUrl=http://blablub.cs/bl?test=cc&bla=bl'

        when:
        def result = optionalParamCalculator.calculateOptionalParamString(urlParam)

        then:
        result == "?feedUrl=http%3A%2F%2Fblablub.cs%2Fbl%3Ftest%3Dcc%26bla%3Dbl"
    }

    def "Provide invalid parameter which do not match the key valie syntax"() {
        given:
        def param='kjb'

        when:
        optionalParamCalculator.calculateOptionalParamString(param)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided http parameter is not valid. Should be like \'key=value\''
    }
}
