package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load

import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common.CommonLoadMapper
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.01.13
 */
class UserRelatedLoadRequestToXmlMapperTest extends Specification {

    UserRelatedLoadRequestToXmlMapper loadRequestToXmlMapper
    CommonLoadMapper commonLoadMapper

    def setup(){
        loadRequestToXmlMapper = new UserRelatedLoadRequestToXmlMapper()
        commonLoadMapper = Mock(CommonLoadMapper)
        loadRequestToXmlMapper.commonLoadMapper = commonLoadMapper
    }

    def "Generate xml out of a search request model"() {
        given:
        def userId = '234234'
        def userName = 'palasad'
        def password = 'passwd'
        final UserRelatedLoadFeedRequest loadFeedRequest = new UserRelatedLoadFeedRequest(feedUrl: 'http://feed.com/rss',
                                                                    userId: userId,
                                                                    userName: userName,
                                                                    password: password)

        and:
        def loadFeedRequestMap = new HashMap<>()

        when:
        def result = loadRequestToXmlMapper.getRequestData(loadFeedRequest);

        then:
        result == loadFeedRequestMap
        1 * commonLoadMapper.mapCommonLoadObject(loadFeedRequest) >> loadFeedRequestMap
    }

    def "Map showOnlyUnreadEntries if true"() {
        given:
        def loadFeedRequest = new UserRelatedLoadFeedRequest()
        loadFeedRequest.setShowOnlyUnreadEntries(true)

        and:
        def loadFeedRequestMap = new HashMap<>()

        when:
        def result = loadRequestToXmlMapper.getRequestData(loadFeedRequest)

        then:
        1 * commonLoadMapper.mapCommonLoadObject(loadFeedRequest) >> loadFeedRequestMap
        result['showOnlyUnreadEntries'] == 'true'
    }

    def "Don't map showOnlyUnreadEntries if false"() {
        given:
        def loadFeedRequest = new UserRelatedLoadFeedRequest()
        loadFeedRequest.setShowOnlyUnreadEntries(false)

        and:
        def loadFeedRequestMap = new HashMap<>()

        when:
        def result = loadRequestToXmlMapper.getRequestData(loadFeedRequest)

        then:
        1 * commonLoadMapper.mapCommonLoadObject(loadFeedRequest) >> loadFeedRequestMap
        result.containsKey('showOnlyUnreadEntries') == false
    }
}
