package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.domain

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtomEntry
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.12.12
 */
class AtomDomainBuilderTest extends Specification {
    def "Build a feeder atom"() {
        when:
        def result = new AtomDomainBuilder().getAtomModelObject();

        then:
        result != null
        result instanceof FeederAtom
    }

    def "Build a feed entry model"() {
        when:
        def entry = new AtomDomainBuilder().getAtomEntryModelObject()

        then:
        entry != null
        entry instanceof FeederAtomEntry
    }
}
