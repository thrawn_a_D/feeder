package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.metadata

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class FeedColorMetadataMapperTest extends Specification {
    FeedColorMetadataMapper feedColorMetadataMapper

    def setup(){
        feedColorMetadataMapper = new FeedColorMetadataMapper()
    }

    def "Map favoured status from jaxb to a feed"() {
        given:
        def atom = new FeederAtom()
        def qname = new QName('FEED_UI_HIGHLIGHTING_COLOR')
        def color = "#sdf"

        when:
        feedColorMetadataMapper.mapFeedMetadata(atom, qname, color)

        then:
        atom.feedUiHighlightingColor == color
    }

    def "Do nothing if not the right identifier is provided"() {
        given:
        def atom = new FeederAtom()
        def qname = new QName('asd')

        when:
        feedColorMetadataMapper.mapFeedMetadata(atom, qname, null)

        then:
        atom.feedUiHighlightingColor == null
    }
}
