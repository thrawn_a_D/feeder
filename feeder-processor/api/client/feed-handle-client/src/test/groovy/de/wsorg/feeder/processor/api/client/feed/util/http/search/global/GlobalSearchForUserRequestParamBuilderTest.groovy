package de.wsorg.feeder.processor.api.client.feed.util.http.search.global

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper.UrlEncodingWrapper
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
class GlobalSearchForUserRequestParamBuilderTest extends Specification {
    GlobalSearchForUserRequestParamBuilder globalSearchForUserRequestParamBuilder
    ConfigurationProvider configurationProvider
    UrlEncodingWrapper urlEncodingWrapper

    def setup(){
        globalSearchForUserRequestParamBuilder = new GlobalSearchForUserRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        urlEncodingWrapper = Mock(UrlEncodingWrapper)
        globalSearchForUserRequestParamBuilder.configurationProvider = configurationProvider
        globalSearchForUserRequestParamBuilder.urlEncodingWrapper = urlEncodingWrapper
    }

    def "Build request method for a search command"() {
        given:
        def searchTerm = 'asdq'
        def encodedSearchTerm = 'jhbuozgouzb7u'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def requestParamMap = [searchTerm: searchTerm, userId:userId, userName: userName, password:password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        HttpGet getRequest = globalSearchForUserRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feeds/${userId}?searchTerm=${encodedSearchTerm}"
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * urlEncodingWrapper.encode(searchTerm, UrlEncodingWrapper.DEFAULT_ENCODING) >> encodedSearchTerm
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
    }

    def "Search term is empty"() {
        given:
        def searchTerm = ''
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def requestParamMap = [searchTerm: searchTerm, userId:userId, userName: userName, password:password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        HttpGet getRequest = globalSearchForUserRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feeds/${userId}"
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        0 * urlEncodingWrapper.encode(_, UrlEncodingWrapper.DEFAULT_ENCODING)
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
    }
}
