package de.wsorg.feeder.processor.api.client.feed.util.http.handling

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import org.apache.http.client.methods.HttpPost
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 03.02.13
 */
class MarkFeedEntryReadabilityRequestParamBuilderTest extends Specification {
    MarkFeedEntryReadabilityRequestParamBuilder markFeedEntryReadabilityRequestParamBuilder
    ConfigurationProvider configurationProvider

    def setup(){
        markFeedEntryReadabilityRequestParamBuilder = new MarkFeedEntryReadabilityRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        markFeedEntryReadabilityRequestParamBuilder.configurationProvider = configurationProvider
    }

    def "Get creation request"() {
        given:
        def creationXml = 'bkjas'

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        when:
        HttpPost postRequest = markFeedEntryReadabilityRequestParamBuilder.buildHttpRequest(creationXml)

        then:
        postRequest
        postRequest.getURI().toASCIIString() == serviceUrl + "feeds/DUMMY/subscriptions/read"
        getStreamAsString(postRequest.getEntity().getContent()) == creationXml
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
    }

    public String getStreamAsString(final InputStream stream) {
        String result = "";
        try {
            // Get the response
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));

            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                result += line;
            }
        } catch (IOException e) {
            final String errorMessage = "Error while reading http response content: " + e.getMessage();
            throw new RuntimeException(errorMessage, e);
        }

        return result;
    }
}
