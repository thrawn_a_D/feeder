package de.wsorg.feeder.processor.api.client.feed.util.http.load

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.util.http.url.HttpParamCalculator
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
class LoadFeedForUserRequestParamBuilderTest extends Specification {
    LoadFeedForUserRequestParamBuilder loadFeedRequestParamBuilder
    ConfigurationProvider configurationProvider
    HttpParamCalculator optionalParamCalculator

    def setup(){
        loadFeedRequestParamBuilder = new LoadFeedForUserRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        optionalParamCalculator = Mock(HttpParamCalculator)
        loadFeedRequestParamBuilder.configurationProvider = configurationProvider
        loadFeedRequestParamBuilder.httpParamCalculator = optionalParamCalculator
    }

    def "Build request method for a load command"() {
        given:
        def feedUrl = 'http://asdjn'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def requestParamMap = [feedUrl: feedUrl, userId:userId, userName: userName, password:password]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def calculatedOptions = '231423'

        when:
        HttpGet getRequest = loadFeedRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feed/${userId}${calculatedOptions}"
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedOptions
    }

    def "Load a feed and use paging"() {
        given:
        def feedUrl = 'http://asdjn'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def pageRangeStartIndex = '0'
        def pageRangeEndIndex = '10'
        def requestParamMap = [feedUrl: feedUrl,
                               userId:userId,
                               userName: userName,
                               password:password,
                               pageRangeStartIndex:pageRangeStartIndex,
                               pageRangeEndIndex:pageRangeEndIndex]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def calculatedOptions = '231423'

        when:
        HttpGet getRequest = loadFeedRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feed/${userId}${calculatedOptions}"
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedOptions
    }

    def "Load a feed and show only unread entries"() {
        given:
        def feedUrl = 'http://asdjn'
        def userId = '23423wed'
        def userName = 'userName'
        def password = 'passwd'
        def pageRangeStartIndex = '0'
        def pageRangeEndIndex = '10'
        def showOnlyUnreadEntries = 'true'
        def requestParamMap = [feedUrl: feedUrl,
                userId:userId,
                userName: userName,
                password:password,
                pageRangeStartIndex:pageRangeStartIndex,
                pageRangeEndIndex:pageRangeEndIndex,
                showOnlyUnreadEntries:showOnlyUnreadEntries]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def calculatedOptions = '231423'

        when:
        HttpGet getRequest = loadFeedRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feed/${userId}${calculatedOptions}"
        getRequest.getFirstHeader('userName').value == userName
        getRequest.getFirstHeader('password').value == password
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedOptions
    }

}
