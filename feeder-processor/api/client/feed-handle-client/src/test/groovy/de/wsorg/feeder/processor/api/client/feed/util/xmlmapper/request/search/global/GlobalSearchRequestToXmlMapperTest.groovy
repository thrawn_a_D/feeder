package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global;


import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common.CommonGlobalMapper
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class GlobalSearchRequestToXmlMapperTest extends Specification {


    GlobalSearchRequestToXmlMapper searchRequestMapper
    CommonGlobalMapper commonGlobalMapper

    def setup(){
        searchRequestMapper = new GlobalSearchRequestToXmlMapper()
        commonGlobalMapper = Mock(CommonGlobalMapper)
        searchRequestMapper.commonGlobalMapper = commonGlobalMapper
    }

    def "Map common object and provide result"() {
        given:
        final GlobalSearchQueryRequest searchQueryRequest = new GlobalSearchQueryRequest(searchTerm: 'some query')

        and:
        def expectedMap = new HashMap<>()

        when:
        def generatedMap = searchRequestMapper.getRequestData(searchQueryRequest);

        then:
        generatedMap == expectedMap
        1 * commonGlobalMapper.getCommonSearchQuery(searchQueryRequest) >> expectedMap
    }
}
