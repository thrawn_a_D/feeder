package de.wsorg.feeder.processor.api.client.feed.util.http.load

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.util.http.url.HttpParamCalculator
import org.apache.http.client.methods.HttpGet
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
class LoadFeedRequestParamBuilderTest extends Specification {
    LoadFeedRequestParamBuilder loadFeedRequestParamBuilder
    ConfigurationProvider configurationProvider
    HttpParamCalculator optionalParamCalculator

    def setup(){
        loadFeedRequestParamBuilder = new LoadFeedRequestParamBuilder()
        configurationProvider = Mock(ConfigurationProvider)
        optionalParamCalculator = Mock(HttpParamCalculator)
        loadFeedRequestParamBuilder.configurationProvider = configurationProvider
        loadFeedRequestParamBuilder.httpParamCalculator = optionalParamCalculator
    }

    def "Build request method for a load command"() {
        given:
        def feedUrl = 'http://asdjn'
        def requestParamMap = [feedUrl: feedUrl]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def calculatedOptions = '231423'

        when:
        HttpGet getRequest = loadFeedRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        getRequest
        getRequest.getURI().toASCIIString() == serviceUrl + "feed${calculatedOptions}"
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * optionalParamCalculator.calculateOptionalParamString(_) >> calculatedOptions
    }

    def "Load a feed and use paging"() {
        given:
        def feedUrl = 'http://asdjn'
        def pageRangeStartIndex = '0'
        def pageRangeEndIndex = '10'
        def requestParamMap = [feedUrl: feedUrl,
                               pageRangeStartIndex:pageRangeStartIndex,
                               pageRangeEndIndex:pageRangeEndIndex]

        and:
        def serviceUrl = 'http://localhost:8080/feeder-handle/'

        and:
        def configuration = Mock(FeederClientConfiguration)

        and:
        def calculatedOptions = '231423'

        when:
        loadFeedRequestParamBuilder.buildHttpRequest(requestParamMap)

        then:
        1 * configurationProvider.getFeederClientConfiguration() >> configuration
        1 * configuration.getFeederRestServiceUrl() >> serviceUrl
        1 * optionalParamCalculator.calculateOptionalParamString(_, _, _) >> calculatedOptions
    }
}
