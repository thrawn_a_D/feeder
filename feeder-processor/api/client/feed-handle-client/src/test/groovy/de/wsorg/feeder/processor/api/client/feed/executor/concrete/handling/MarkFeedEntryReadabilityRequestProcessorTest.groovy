package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.api.domain.FeederCommand
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.10.12
 */
class MarkFeedEntryReadabilityRequestProcessorTest extends Specification {
    MarkFeedEntryReadabilityRequestProcessor handleFeedRequestProcessor

    def setup(){
        handleFeedRequestProcessor = new MarkFeedEntryReadabilityRequestProcessor()
    }

    def "Make sure the right request to xml mapper is returned"() {
        given:
        RequestToXmlMapper requestToXmlMapperMock = Mock(RequestToXmlMapper)
        handleFeedRequestProcessor.requestToXmlMapper = requestToXmlMapperMock

        when:
        def usedMapper = handleFeedRequestProcessor.getRequestToTransferMapper()

        then:
        usedMapper == requestToXmlMapperMock
    }


    def "Get an appropriate command to execute"() {
        when:
        def commandToExecute = handleFeedRequestProcessor.commandToExecute

        then:
        commandToExecute == FeederCommand.MARK_FEED_ENTRY_READABILITY
    }
}
