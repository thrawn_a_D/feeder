package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestModelToTransferMapper
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper
import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.api.domain.request.Request
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.10.12
 */
class HandleFeedRequestProcessorTemplateTest extends Specification {
    HandleFeedRequestProcessorTemplate handleFeedRequestProcessor
    static RequestToXmlMapper requestToXmlMapper

    def setup(){
        handleFeedRequestProcessor = new HandleFeedTestRequestProcessorTemplate()
        requestToXmlMapper = Mock(RequestToXmlMapper)
    }

    def "Make sure the right response to result mapper is returned"() {
        given:
        XmlToDomainMapper responseToResultMapper = Mock(XmlToDomainMapper)
        handleFeedRequestProcessor.xmlToDomainMapper = responseToResultMapper

        when:
        def usedMapper = handleFeedRequestProcessor.getXmlToDomainMapper()

        then:
        usedMapper == responseToResultMapper
    }

    private class HandleFeedTestRequestProcessorTemplate extends HandleFeedRequestProcessorTemplate {

        @Override
        protected RequestModelToTransferMapper<String, Request> getRequestToTransferMapper() {
            return HandleFeedRequestProcessorTemplateTest.requestToXmlMapper;
        }

        @Override
        protected FeederCommand getCommandToExecute() {
            return FeederCommand.MARK_FEED_ENTRY_READABILITY
        }
    }
}
