package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.AtomFeederMapperBuilder
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.12.12
 */
class FeederClientAtomMapperTest extends Specification {
    XmlToDomainMapper<StandardFeed> standardAtomResponseToResultMapper
    AtomFeederMapperBuilder atomFeederMapperBuilder
    FeederClientAtomMapper feederClientAtomMapper

    def setup(){
        standardAtomResponseToResultMapper = Mock(XmlToDomainMapper)
        atomFeederMapperBuilder = Mock(AtomFeederMapperBuilder)
        feederClientAtomMapper = new FeederClientAtomMapper()
        feederClientAtomMapper.atomFeederMapperBuilder = atomFeederMapperBuilder
    }

    def "Add a list of metadata mappers for feed"() {
        given:
        def metaDataMapperList = [Mock(FeedMetadataMapper)]
        def entryMetaDataMapperList = [Mock(FeedEntryMetadataMapper)]
        def domainObjectBuilder = Mock(DomainBuilderFactory)

        and:
        feederClientAtomMapper.metaDataMapperList = metaDataMapperList
        feederClientAtomMapper.entryMetaDataMapperList = entryMetaDataMapperList
        feederClientAtomMapper.domainBuilderFactory = domainObjectBuilder

        when:
        feederClientAtomMapper.getResult(null)

        then:
        1 * atomFeederMapperBuilder.feedMetadataMappers(metaDataMapperList) >> atomFeederMapperBuilder
        1 * atomFeederMapperBuilder.feedEntryMetadataMappers(entryMetaDataMapperList) >> atomFeederMapperBuilder
        1 * atomFeederMapperBuilder.domainBuilderFactory(domainObjectBuilder) >> atomFeederMapperBuilder
        1 * atomFeederMapperBuilder.build() >> standardAtomResponseToResultMapper
    }

    def "Map the result using the jaxb mapper and then return a feeder atom domain object"() {
        given:
        def atomToMap = 'asdasd'

        def metaDataMapperList = [Mock(FeedMetadataMapper)]
        def entryMetaDataMapperList = [Mock(FeedEntryMetadataMapper)]
        def domainObjectBuilder = Mock(DomainBuilderFactory)

        and:
        feederClientAtomMapper.metaDataMapperList = metaDataMapperList
        feederClientAtomMapper.entryMetaDataMapperList = entryMetaDataMapperList
        feederClientAtomMapper.domainBuilderFactory = domainObjectBuilder

        and:
        def expectedResult = new FeederAtom()

        and:
        atomFeederMapperBuilder.feedMetadataMappers(metaDataMapperList) >> atomFeederMapperBuilder
        atomFeederMapperBuilder.feedEntryMetadataMappers(entryMetaDataMapperList) >> atomFeederMapperBuilder
        atomFeederMapperBuilder.domainBuilderFactory(domainObjectBuilder) >> atomFeederMapperBuilder
        atomFeederMapperBuilder.build() >> standardAtomResponseToResultMapper

        when:
        def result = feederClientAtomMapper.getResult(atomToMap)

        then:
        result == expectedResult
        1 * standardAtomResponseToResultMapper.getResult(atomToMap) >> expectedResult
    }
}
