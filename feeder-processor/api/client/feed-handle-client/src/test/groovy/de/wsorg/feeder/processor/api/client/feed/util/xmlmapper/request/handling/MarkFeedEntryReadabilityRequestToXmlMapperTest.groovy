package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling

import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.10.12
 */
class MarkFeedEntryReadabilityRequestToXmlMapperTest extends Specification {

    MarkFeedEntryReadabilityRequestToXmlMapper markFeedEntryReadabilityRequestToXmlMapper

    def setup(){
        markFeedEntryReadabilityRequestToXmlMapper = new MarkFeedEntryReadabilityRequestToXmlMapper()
    }

    def "Generate xml from request object"() {
        given:
        MarkFeedEntryReadabilityRequest request = new MarkFeedEntryReadabilityRequest()
        request.feedUrl = 'http://feedUrl'
        request.feedEntryUidWithReadabilityStatus << ['http://feedUrl#oiuhb98hiubh87': true,
                                                      'http://feedUrl#oh09u9j908u98s': false]

        when:
        def result = markFeedEntryReadabilityRequestToXmlMapper.getRequestData(request)
        def resultAsXmlSlurper = new XmlSlurper().parseText(result)

        then:
        resultAsXmlSlurper != ''
        resultAsXmlSlurper.'feedUrl' == request.feedUrl
        resultAsXmlSlurper.'markFeedEntryWithReadStatus'.'item'.size() == 2
        resultAsXmlSlurper.'markFeedEntryWithReadStatus'.'item'[0].'uid' == 'http://feedUrl#oh09u9j908u98s'
        resultAsXmlSlurper.'markFeedEntryWithReadStatus'.'item'[0].'isRead' == 'false'
        resultAsXmlSlurper.'markFeedEntryWithReadStatus'.'item'[1].'uid' == 'http://feedUrl#oiuhb98hiubh87'
        resultAsXmlSlurper.'markFeedEntryWithReadStatus'.'item'[1].'isRead' == 'true'
    }
}
