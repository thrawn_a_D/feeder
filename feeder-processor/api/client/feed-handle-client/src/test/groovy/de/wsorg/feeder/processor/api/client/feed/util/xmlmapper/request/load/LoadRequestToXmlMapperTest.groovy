package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load;


import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common.CommonLoadMapper
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class LoadRequestToXmlMapperTest extends Specification {

    LoadRequestToXmlMapper loadRequestToXmlMapper
    CommonLoadMapper commonLoadMapper

    def setup(){
        loadRequestToXmlMapper = new LoadRequestToXmlMapper()
        commonLoadMapper = Mock(CommonLoadMapper)
        loadRequestToXmlMapper.commonLoadMapper = commonLoadMapper
    }

    def "Generate request out of a search request model"() {
        given:
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: 'http://feed.com/rss')

        and:
        def loadFeedRequestMap = new HashMap()

        when:
        def result = loadRequestToXmlMapper.getRequestData(loadFeedRequest);

        then:
        result == loadFeedRequestMap
        1 * commonLoadMapper.mapCommonLoadObject(loadFeedRequest) >> loadFeedRequestMap
    }
}
