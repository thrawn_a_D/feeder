package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.opml;


import de.wsorg.feeder.processor.api.domain.generated.response.opml.Body
import de.wsorg.feeder.processor.api.domain.generated.response.opml.OPML
import de.wsorg.feeder.processor.api.domain.generated.response.opml.Outline
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * <p/>
 * User: wschneider
 * Date: 14.07.12
 */
public class JaxBOpmlToFeederApiOpmlTest extends Specification {
    JaxBOpmlToFeederApiOpml jaxBOpmlToFeederApiOpml

    def setup(){
        jaxBOpmlToFeederApiOpml = new JaxBOpmlToFeederApiOpml()
    }

    def "Test opml outline is empty"(){
        given:
        def opml = Mock(OPML)
        def opmlJaxbWrap = wrapInJaxbElement(opml)
        def body = Mock(Body)
        addEmptyOutlineList(body)
        opml.body >> body

        when:
        Opml results = jaxBOpmlToFeederApiOpml.mapJaxbPojoToFeederPojo(opmlJaxbWrap)

        then:
        results != null
        results.body.searchResultList.size() == 0
    }

    def "Test opml outline is null"(){
        given:
        def opml = Mock(OPML)
        def opmlJaxbWrap = wrapInJaxbElement(opml)
        def body = Mock(Body)
        body.outline >> null
        opml.body >> body

        when:
        Opml results = jaxBOpmlToFeederApiOpml.mapJaxbPojoToFeederPojo(opmlJaxbWrap)

        then:
        results != null
        results.body.searchResultList.size() == 0
    }

    def "Map opml to feed with the necessary fields"(){
        given: "OPML test data"
        def opml = Mock(OPML)
        def opmlJaxbWrap = wrapInJaxbElement(opml)
        Outline innerOutline = getFinalOutlineStub(opml)

        and: "Prepare the test values"
        def titleValue = 'Title'
        def testDescriptionValue = 'Test description'
        def feedUrl = 'http://bla.blub.com/lalala.xml'

        innerOutline.title >> titleValue
        innerOutline.description >> testDescriptionValue
        innerOutline.xmlUrl >> feedUrl

        when:
        Opml results = jaxBOpmlToFeederApiOpml.mapJaxbPojoToFeederPojo(opmlJaxbWrap)

        then:
        results.body.searchResultList != null
        results.body.searchResultList.size() == 1
        results.body.searchResultList[0].title == titleValue
        results.body.searchResultList[0].description == testDescriptionValue
        results.body.searchResultList[0].xmlUrl == feedUrl
    }

    def "Check that input contails an opml element"() {
        given:
        def somethingNotJaxbElementLike = ""
        def jaxbElement = new JAXBElement(new QName(""), String, somethingNotJaxbElementLike)

        when:
        jaxBOpmlToFeederApiOpml.mapJaxbPojoToFeederPojo(jaxbElement);

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided jaxb element does not contail an opml object"
    }

    private JAXBElement wrapInJaxbElement(OPML opml) {
        JAXBElement opmlJaxbWrap = new JAXBElement(new QName(""), OPML, opml)
        opmlJaxbWrap
    }

    private void addEmptyOutlineList(Body body) {
        def outerOutlineList = new ArrayList<Outline>();
        outerOutlineList.size() >> 0
        body.outline >> outerOutlineList
    }

    private Outline getFinalOutlineStub(final OPML opml) {
        def body = Mock(Body)
        def outerOutlineList = new ArrayList<Outline>();
        def innerOutlineList = new ArrayList<Outline>();
        def outerOutline = Mock(Outline)
        def innerOutline = Mock(Outline)

        outerOutlineList.add(outerOutline);
        outerOutline.outline >> innerOutlineList
        innerOutlineList.add(innerOutline);
        body.outline >> outerOutlineList
        opml.body >> body
        return innerOutline
    }
}
