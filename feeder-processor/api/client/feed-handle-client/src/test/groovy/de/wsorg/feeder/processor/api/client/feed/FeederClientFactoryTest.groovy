package de.wsorg.feeder.processor.api.client.feed;


import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling.FeedSubscriptionRequestProcessor
import de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling.MarkAllFeedsReadabilityRequestProcessor
import de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling.MarkFeedEntryReadabilityRequestProcessor
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class FeederClientFactoryTest extends Specification {
    @Shared
    SimpleFeederClientConfiguration config

    def setup(){
        FeederClientFactory.configProvided = false
    }

    def setupSpec() {
        config = new SimpleFeederClientConfiguration()
    }

    def "Create an instance of a global feeder finder"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeederRequestProcessor factoryResult = FeederClientFactory.getGlobalFeedFinder()

        then:
        factoryResult != null
        factoryResult.requestToXmlMapper != null
        factoryResult.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting global feeder finder"() {

        when:
        FeederClientFactory.getGlobalFeedFinder()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Create an instance of a global feeder finder for user"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeederRequestProcessor factoryResult = FeederClientFactory.getGlobalFeedFinderForUser()

        then:
        factoryResult != null
        factoryResult.requestToStringMapMapper != null
        factoryResult.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting global feeder finder for user"() {

        when:
        FeederClientFactory.getGlobalFeedFinderForUser()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }


    def "Create an instance of a user related feeder finder"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeederRequestProcessor factoryResult = FeederClientFactory.getUserRelatedFeedFinder()

        then:
        factoryResult != null
        factoryResult.requestToStringMapMapper != null
        factoryResult.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting user related feeder finder"() {

        when:
        FeederClientFactory.getUserRelatedFeedFinder()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get an instance of feed loader"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeederRequestProcessor feedLoader = FeederClientFactory.getFeedLoader();

        then:
        feedLoader != null
        feedLoader.requestToXmlMapper != null
        feedLoader.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting a feed loader"() {

        when:
        FeederClientFactory.getFeedLoader()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get an instance of a feed handler"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeedSubscriptionRequestProcessor handleFeedRequestProcessor = FeederClientFactory.getFeedSubscriptionHandler()

        then:
        handleFeedRequestProcessor.requestToXmlMapper != null
        handleFeedRequestProcessor.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting a feed handler"() {

        when:
        FeederClientFactory.getFeedSubscriptionHandler()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get an instance of a feed read marker"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        MarkFeedEntryReadabilityRequestProcessor handleFeedRequestProcessor = FeederClientFactory.getFeedEntryReadMarker()

        then:
        handleFeedRequestProcessor.requestToXmlMapper != null
        handleFeedRequestProcessor.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting a feed entry marker"() {

        when:
        FeederClientFactory.getFeedEntryReadMarker()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get an instance of a all feeds read marker"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        MarkAllFeedsReadabilityRequestProcessor handleFeedRequestProcessor = FeederClientFactory.getAllFeedsReadMarker()

        then:
        handleFeedRequestProcessor.requestToXmlMapper != null
        handleFeedRequestProcessor.xmlToDomainMapper != null
    }

    def "Check if configuration was set properly when getting an all feed marker"() {

        when:
        FeederClientFactory.getAllFeedsReadMarker()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid client configuration first'
    }

    def "Get an instance of a user related loader"() {
        given:
        FeederClientFactory.setClientConfig(config)

        when:
        FeederRequestProcessor feedLoader = FeederClientFactory.getUserRelatedFeedLoader();

        then:
        feedLoader != null
        feedLoader.requestToXmlMapper != null
        feedLoader.xmlToDomainMapper != null
    }

    def "Check that config was inserted into app context"() {
        when:
        FeederClientFactory.setClientConfig(config)

        then:
        def usedConfig = FeederClientFactory.applicationContext.getBean(FeederClientConfiguration)
        usedConfig == config
    }
}
