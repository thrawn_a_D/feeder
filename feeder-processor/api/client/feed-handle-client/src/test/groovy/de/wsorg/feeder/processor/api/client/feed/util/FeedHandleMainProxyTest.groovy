package de.wsorg.feeder.processor.api.client.feed.util

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.find.FileFinder
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.process.JavaRuntimeWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.02.12
 */
class FeedHandleMainProxyTest extends Specification {
    FeedHandleMainProxy feedHandleMainProxy
    JavaRuntimeWrapper javaRuntimeWrapper
    FileFinder fileFinder
    ConfigurationProvider configurationProvider
    FeederClientConfiguration feederClientConfiguration


    def setup(){
        feedHandleMainProxy = new FeedHandleMainProxy()
        javaRuntimeWrapper = Mock(JavaRuntimeWrapper)
        fileFinder = Mock(FileFinder)
        configurationProvider = Mock(ConfigurationProvider)
        feederClientConfiguration = Mock(FeederClientConfiguration)
        feedHandleMainProxy.javaRuntimeWrapper = javaRuntimeWrapper
        feedHandleMainProxy.fileFinder = fileFinder
        feedHandleMainProxy.configurationProvider = configurationProvider
        configurationProvider.getFeederClientConfiguration() >> feederClientConfiguration
    }

    def "Execute command with provided arguments"() {
        given:
        String[] argumentsToExecute = ['asd', 'asd']

        and:
        def jarFileName = 'feeder-processor-feed-execution-1.0-SNAPSHOT-jar-with-dependencies.jar'
        def foundFilePath = 'filePath'
        def expectedExecutionCommand = "java -jar ${foundFilePath} ${argumentsToExecute[0]} ${argumentsToExecute[1]}"

        and:
        def process = Mock(Process)

        when:
        feedHandleMainProxy.invoke(argumentsToExecute)

        then:
        1 * javaRuntimeWrapper.execCommand(expectedExecutionCommand) >> process
        1 * fileFinder.getFilePathOfFile(jarFileName) >> foundFilePath
        1 * process.waitFor()
    }

    def "Check if debug options are set"() {
        given:
        String[] argumentsToExecute = ['asd', 'asd']
        feederClientConfiguration.getDebugProcessing() >> true

        and:
        def expectedDebugOptions = ' -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8008 '
        def foundFilePath = 'filePath'

        and:
        def process = Mock(Process)

        when:
        feedHandleMainProxy.invoke(argumentsToExecute)

        then:
        1 * javaRuntimeWrapper.execCommand({it.contains(expectedDebugOptions)}) >> process
        1 * fileFinder.getFilePathOfFile(_) >> foundFilePath
        1 * process.waitFor()
    }


    def "Error while starting execution"() {
        given:
        String[] argumentsToExecute = ['asd', 'asd']

        and:
        javaRuntimeWrapper.execCommand(_) >> {throw new IOException()}

        and:
        fileFinder.getFilePathOfFile(_) >> 'sd'

        when:
        feedHandleMainProxy.invoke(argumentsToExecute)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'An error occurred while executing feeder execution JAR-File!'
    }

    def "Error while waiting for execution to finish"() {
        given:
        String[] argumentsToExecute = ['asd', 'asd']

        and:
        def process = Mock(Process)
        javaRuntimeWrapper.execCommand(_) >> process
        process.waitFor() >> {throw new InterruptedException()}

        and:
        fileFinder.getFilePathOfFile(_) >> 'sd'

        when:
        feedHandleMainProxy.invoke(argumentsToExecute)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'An error occurred while waiting for feeder JAR-File execution to finish!'
    }

    def "No jar file found"() {
        given:
        String[] argumentsToExecute = ['asd', 'asd']

        and:
        def jarFileName = 'feeder-processor-feed-execution-1.0-SNAPSHOT-jar-with-dependencies.jar'

        and:
        fileFinder.getFilePathOfFile(jarFileName) >> null

        when:
        feedHandleMainProxy.invoke(argumentsToExecute)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'The jar file to be executed could not be found:' + jarFileName
    }
}
