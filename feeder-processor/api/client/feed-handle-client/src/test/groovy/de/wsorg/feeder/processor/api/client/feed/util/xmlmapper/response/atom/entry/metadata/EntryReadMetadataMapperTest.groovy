package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.entry.metadata

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtomEntry
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class EntryReadMetadataMapperTest extends Specification {

    EntryReadMetadataMapper entryReadMetadataMapper

    def setup(){
        entryReadMetadataMapper = new EntryReadMetadataMapper()
    }

    def "Provide a description and expect the value to be mapped to the feed entry"() {
        given:
        def feedEntry = new FeederAtomEntry()
        def qname = new QName('IS_READ')
        def readStatus = true

        when:
        entryReadMetadataMapper.mapFeedEntryMetadata(feedEntry, qname, String.valueOf(readStatus))

        then:
        feedEntry.entryAlreadyRead == true
    }

    def "Entry does not match the identifier, do nothing"() {
        given:
        def feedEntry = new FeederAtomEntry()
        def qname = new QName('asd')
        def readStatus = true

        when:
        entryReadMetadataMapper.mapFeedEntryMetadata(feedEntry, qname, String.valueOf(readStatus))

        then:
        !feedEntry.entryAlreadyRead
    }
}
