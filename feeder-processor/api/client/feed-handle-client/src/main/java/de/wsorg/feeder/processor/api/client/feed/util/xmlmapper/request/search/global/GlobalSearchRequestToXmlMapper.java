package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common.CommonGlobalMapper;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "globalSearchQueryMapper")
public class GlobalSearchRequestToXmlMapper implements RequestToStringMapMapper<GlobalSearchQueryRequest> {

    @Inject
    private CommonGlobalMapper commonGlobalMapper;

    @Override
    public Map getRequestData(final GlobalSearchQueryRequest objectToMap) {
        return commonGlobalMapper.getCommonSearchQuery(objectToMap);
    }
}
