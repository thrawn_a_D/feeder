package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common.CommonLoadMapper;
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "loadRequestMapper")
public class LoadRequestToXmlMapper implements RequestToStringMapMapper<LoadFeedRequest> {

    @Inject
    private CommonLoadMapper commonLoadMapper;

    @Override
    public Map getRequestData(final LoadFeedRequest objectToMap) {
        return commonLoadMapper.mapCommonLoadObject(objectToMap);
    }
}
