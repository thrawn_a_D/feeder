package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common;

import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonLoadMapper {
    Map<String, String> mapCommonLoadObject(final LoadFeed objectToMap);
}
