package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.generated.request.handler.FeedSubscriptionType;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "feedSubscriptionRequestToXmlMapper")
public class FeedSubscriptionRequestToXmlMapper implements RequestToXmlMapper<FeedSubscriptionRequest> {

    private static final String EXPECTED_JAXB_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.request.handler";

    @Inject
    JaxbMarshaller jaxbMarshaller;

    @Override
    public String getRequestData(final FeedSubscriptionRequest objectToMap) {
        FeedSubscriptionType feedSubscriptionType = new FeedSubscriptionType();
        feedSubscriptionType.setFeedUrl(objectToMap.getFeedUrl());
        feedSubscriptionType.setFavour(objectToMap.isFavourFeed());

        return getMarshaller().marshal(feedSubscriptionType);
    }

    private JaxbMarshaller getMarshaller() {
        jaxbMarshaller.setJaxbGeneratedClassesPackage(EXPECTED_JAXB_PACKAGE);
        return jaxbMarshaller;
    }
}
