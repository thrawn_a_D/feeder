package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.metadata;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedIsFavouredMetadataMapper implements FeedMetadataMapper<FeederAtom> {
    private static final String METADATA_IS_FAVOURED="IS_FAVOURED";

    @Override
    public void mapFeedMetadata(final FeederAtom StandardAtom, final QName qName, final String value) {
        if(qName.getLocalPart().equals(METADATA_IS_FAVOURED))
            StandardAtom.setFavoured(Boolean.valueOf(value));
    }
}
