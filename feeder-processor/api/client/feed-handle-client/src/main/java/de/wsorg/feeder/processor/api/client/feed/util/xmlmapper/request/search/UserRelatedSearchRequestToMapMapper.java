package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserSearchRestriction;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "userRelatedSearchQueryMapper")
public class UserRelatedSearchRequestToMapMapper  implements RequestToStringMapMapper<UserRelatedSearchQueryRequest> {

    @Override
    public Map getRequestData(final UserRelatedSearchQueryRequest objectToMap) {
        Map<String, String> resultingMapping = new HashMap<String, String>();

        if (StringUtils.isNotBlank(objectToMap.getSearchTerm())) {
            resultingMapping.put("searchTerm", objectToMap.getSearchTerm());
        }

        final Map<UserSearchRestriction, Object> searchRestrictions = objectToMap.getSearchRestrictions();
        if(searchRestrictions.size() > 0) {
            for (UserSearchRestriction currentSearchRestriction : searchRestrictions.keySet()) {
                if(currentSearchRestriction == UserSearchRestriction.UNREAD_ENTRIES) {
                    final String restrictionValue = String.valueOf(searchRestrictions.get(UserSearchRestriction.UNREAD_ENTRIES));
                    resultingMapping.put("showUnreadEntries", restrictionValue);
                }
            }
        }

        return resultingMapping;
    }
}
