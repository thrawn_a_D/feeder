package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common.CommonLoadMapper;
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserRelatedLoadRequestToXmlMapper implements RequestToStringMapMapper<UserRelatedLoadFeedRequest> {
    private static final String SHOW_ONLY_UNREAD_ENTRIES = "showOnlyUnreadEntries";

    @Inject
    private CommonLoadMapper commonLoadMapper;

    @Override
    public Map getRequestData(final UserRelatedLoadFeedRequest objectToMap) {
        final Map<String, String> mappedLoadParams = commonLoadMapper.mapCommonLoadObject(objectToMap);

        if(objectToMap.isShowOnlyUnreadEntries()) {
            mappedLoadParams.put(SHOW_ONLY_UNREAD_ENTRIES, String.valueOf(objectToMap.isShowOnlyUnreadEntries()));
        }

        return mappedLoadParams;
    }
}
