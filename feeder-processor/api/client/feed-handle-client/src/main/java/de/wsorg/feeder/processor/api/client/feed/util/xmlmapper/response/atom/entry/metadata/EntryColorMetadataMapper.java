package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.entry.metadata;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtomEntry;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EntryColorMetadataMapper implements FeedEntryMetadataMapper<FeederAtomEntry> {
    private static final String METADATA_UI_FEED_COLOR="FEED_UI_HIGHLIGHTING_COLOR";

    @Override
    public void mapFeedEntryMetadata(final FeederAtomEntry newEntryFeeder, final QName qName, final String value) {
        if(qName.getLocalPart().equals(METADATA_UI_FEED_COLOR))
            newEntryFeeder.setFeedUiHighlightingColor(value);
    }
}
