package de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper;

import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UrlEncodingWrapper {

    public static final String DEFAULT_ENCODING = "UTF-8";

    public String encode(final String url, final String encoding) throws UnsupportedEncodingException {
        return URLEncoder.encode(url, encoding);
    }
}
