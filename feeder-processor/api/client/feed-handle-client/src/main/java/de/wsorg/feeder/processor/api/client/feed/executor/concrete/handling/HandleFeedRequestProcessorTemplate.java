package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling;

import de.wsorg.feeder.processor.api.client.abstraction.executor.AbstractProcessorTemplate;
import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class HandleFeedRequestProcessorTemplate<T extends Request> extends AbstractProcessorTemplate<T, GenericResponseResult> {

    @Inject
    @Named(value = "genericProcessingResultToResultMapper")
    protected XmlToDomainMapper xmlToDomainMapper;

    @Override
    protected XmlToDomainMapper<GenericResponseResult> getXmlToDomainMapper() {
        return xmlToDomainMapper;
    }
}
