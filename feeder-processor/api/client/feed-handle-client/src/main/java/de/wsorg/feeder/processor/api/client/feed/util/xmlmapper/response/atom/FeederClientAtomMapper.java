package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.util.mapping.AtomFeederMapperBuilder;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeederClientAtomMapper implements XmlToDomainMapper<FeederAtom> {

    @Resource(name = "feedMetadataMappers")
    private List<FeedMetadataMapper> metaDataMapperList;

    @Resource(name = "feedEntryMetadataMappers")
    private List<FeedEntryMetadataMapper> entryMetaDataMapperList;

    @Inject
    private AtomFeederMapperBuilder atomFeederMapperBuilder;
    @Inject
    private DomainBuilderFactory domainBuilderFactory;

    private XmlToDomainMapper<StandardFeed> standardAtomXmlToDomainMapper;

    @Override
    public FeederAtom getResult(final String responseXml) {
        standardAtomXmlToDomainMapper = atomFeederMapperBuilder.feedMetadataMappers(metaDataMapperList)
                                                                            .feedEntryMetadataMappers(entryMetaDataMapperList)
                                                                            .domainBuilderFactory(domainBuilderFactory)
                                                                            .build();

        return (FeederAtom) standardAtomXmlToDomainMapper.getResult(responseXml);
    }
}
