package de.wsorg.feeder.processor.api.client.feed.util.http.load;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import de.wsorg.feeder.processor.api.client.feed.util.http.url.HttpParamCalculator;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class LoadFeedRequestParamBuilder implements HttpRequestParamBuilder<Map<String, String>> {

    private static final String FEED_RESOURCE = "feed";
    private static final String PARAM_FEED_URL = "feedUrl";
    private static final String PARAM_PAGE_RANGE_START_INDEX = "pageRangeStartIndex";
    private static final String PARAM_PAGE_RANGE_END_INDEX = "pageRangeEndIndex";
    @Inject
    private ConfigurationProvider configurationProvider;
    @Inject
    private HttpParamCalculator httpParamCalculator;

    @Override
    public HttpRequestBase buildHttpRequest(final Map<String, String> requestParams) {
        final FeederClientConfiguration configuration = configurationProvider.getFeederClientConfiguration();

        final String feedUrl = PARAM_FEED_URL + "=" + requestParams.get("feedUrl");
        String pageRangeStartIndex = requestParams.get(PARAM_PAGE_RANGE_START_INDEX);
        String pageRangeEndIndex = requestParams.get(PARAM_PAGE_RANGE_END_INDEX);

        pageRangeStartIndex = getStartPagingOption(pageRangeStartIndex);
        pageRangeEndIndex = getEndPagingOption(pageRangeEndIndex);

        final String httpOptions = httpParamCalculator.calculateOptionalParamString(feedUrl,
                                                                                        pageRangeStartIndex,
                                                                                        pageRangeEndIndex);

        final String uri = configuration.getFeederRestServiceUrl() + FEED_RESOURCE + httpOptions;

        return new HttpGet(uri);
    }

    private String getStartPagingOption(String pageRangeStartIndex) {
        if(StringUtils.isNotBlank(pageRangeStartIndex)) {
            pageRangeStartIndex = PARAM_PAGE_RANGE_START_INDEX + "=" + pageRangeStartIndex;
        } else {
            pageRangeStartIndex = "";
        }
        return pageRangeStartIndex;
    }

    private String getEndPagingOption(String pageRangeEndIndex) {
        if(StringUtils.isNotBlank(pageRangeEndIndex)) {
            pageRangeEndIndex = PARAM_PAGE_RANGE_END_INDEX + "=" + pageRangeEndIndex;
        } else {
            pageRangeEndIndex = "";
        }
        return pageRangeEndIndex;
    }
}
