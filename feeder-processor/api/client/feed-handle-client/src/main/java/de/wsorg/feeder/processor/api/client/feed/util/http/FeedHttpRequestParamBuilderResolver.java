package de.wsorg.feeder.processor.api.client.feed.util.http;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilderResolver;
import de.wsorg.feeder.processor.api.domain.FeederCommand;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedHttpRequestParamBuilderResolver implements HttpRequestParamBuilderResolver {

    @Inject
    @Named("loadFeedRequestParamBuilder")
    private HttpRequestParamBuilder loadFeedRequestParamBuilder;
    @Inject
    @Named("loadFeedForUserRequestParamBuilder")
    private HttpRequestParamBuilder loadFeedForUserRequestParamBuilder;
    @Inject
    @Named("globalSearchRequestParamBuilder")
    private HttpRequestParamBuilder globalSearchRequestParamBuilder;
    @Inject
    @Named("globalSearchForUserRequestParamBuilder")
    private HttpRequestParamBuilder globalSearchForUserRequestParamBuilder;
    @Inject
    @Named("subscriptionRequestParamBuilder")
    private HttpRequestParamBuilder subscriptionRequestParamBuilder;
    @Inject
    @Named("markFeedEntryReadabilityRequestParamBuilder")
    private HttpRequestParamBuilder markFeedEntryReadabilityRequestParamBuilder;
    @Inject
    @Named("markAllFeedsReadabilityRequestParamBuilder")
    private HttpRequestParamBuilder markAllFeedsReadabilityRequestParamBuilder;
    @Inject
    @Named("userRelatedSearchRequestParamBuilder")
    private HttpRequestParamBuilder userRelatedSearchRequestParamBuilder;

    @Override
    public HttpRequestParamBuilder findHttpRequestParamBuilder(final FeederCommand feederCommand) {
        if(feederCommand == FeederCommand.LOAD) {
            return loadFeedRequestParamBuilder;
        } else if(feederCommand == FeederCommand.LOAD_USER_RELATED){
            return loadFeedForUserRequestParamBuilder;
        } else if(feederCommand == FeederCommand.GLOBAL_SEARCH){
            return globalSearchRequestParamBuilder;
        } else if(feederCommand == FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA){
            return globalSearchForUserRequestParamBuilder;
        } else if(feederCommand == FeederCommand.FAVOUR_FEED){
            return subscriptionRequestParamBuilder;
        } else if(feederCommand == FeederCommand.MARK_FEED_ENTRY_READABILITY){
            return markFeedEntryReadabilityRequestParamBuilder;
        } else if(feederCommand == FeederCommand.MARK_ALL_FEEDS_READABILITY){
            return markAllFeedsReadabilityRequestParamBuilder;
        } else if(feederCommand == FeederCommand.USER_RELATED_SEARCH){
            return userRelatedSearchRequestParamBuilder;
        } else {
            final String errorMessage = "Could not find a related http parameter builder for requested command: " + feederCommand;
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
