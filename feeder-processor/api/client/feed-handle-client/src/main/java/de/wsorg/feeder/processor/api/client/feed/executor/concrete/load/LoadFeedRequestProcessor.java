package de.wsorg.feeder.processor.api.client.feed.executor.concrete.load;

import de.wsorg.feeder.processor.api.client.abstraction.executor.AbstractProcessorTemplate;
import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "loadRequestProcessor")
public class LoadFeedRequestProcessor extends AbstractProcessorTemplate<LoadFeedRequest, FeederAtom> {
    @Inject
    @Named(value = "loadRequestMapper")
    private RequestToStringMapMapper<LoadFeedRequest> requestToXmlMapper;
    @Inject
    @Named(value = "feederClientAtomMapper")
    private XmlToDomainMapper<FeederAtom> xmlToDomainMapper;

    @Override
    public RequestToStringMapMapper<LoadFeedRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    public XmlToDomainMapper<FeederAtom> getXmlToDomainMapper() {
        return this.xmlToDomainMapper;
    }

    @Override
    public FeederCommand getCommandToExecute() {
        return FeederCommand.LOAD;
    }
}
