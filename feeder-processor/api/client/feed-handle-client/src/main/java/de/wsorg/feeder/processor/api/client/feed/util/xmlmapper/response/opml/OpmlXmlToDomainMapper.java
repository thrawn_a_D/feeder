package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.opml;

import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapperTemplate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "opmlResponseMapper")
public class OpmlXmlToDomainMapper extends XmlToDomainMapperTemplate<Opml, JAXBElement> {
    private static final String JAXB_GENERATED_ATOM_PACKAGE = "de.wsorg.feeder.processor.api.domain.generated.response.opml";
    @Inject
    private JaxbPojoToFeederPojoMapper<Opml, JAXBElement> jaxBOpmlToFeederApiOpml;

    @Override
    protected String getGeneratedJaxbClassesPackage() {
        return JAXB_GENERATED_ATOM_PACKAGE;
    }

    @Override
    protected JaxbPojoToFeederPojoMapper<Opml, JAXBElement> getJaxbPojoToFeederPojoMapper() {
        return jaxBOpmlToFeederApiOpml;
    }
}
