package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserRelatedSearchRequestProcessor extends AbstractSearchTemplate<UserRelatedSearchQueryRequest> {
    @Inject
    @Named(value = "userRelatedSearchQueryMapper")
    protected RequestToStringMapMapper<UserRelatedSearchQueryRequest> requestToStringMapMapper;


    @Override
    protected RequestToStringMapMapper<UserRelatedSearchQueryRequest> getRequestToTransferMapper() {
        return requestToStringMapMapper;
    }

    @Override
    protected FeederCommand getCommandToExecute() {
        return FeederCommand.USER_RELATED_SEARCH;
    }
}
