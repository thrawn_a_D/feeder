package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "markFeedEntryReadabilityRequestProcessor")
public class MarkFeedEntryReadabilityRequestProcessor  extends HandleFeedRequestProcessorTemplate<MarkFeedEntryReadabilityRequest> {

    @Inject
    @Named(value = "markFeedEntryReadabilityRequestToXmlMapper")
    private RequestToXmlMapper requestToXmlMapper;

    @Override
    protected RequestToXmlMapper<MarkFeedEntryReadabilityRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    protected FeederCommand getCommandToExecute() {
        return FeederCommand.MARK_FEED_ENTRY_READABILITY;
    }
}
