package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.load.common;

import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class DefaultCommonLoadMapper implements CommonLoadMapper {

    private static final String FEED_URL = "feedUrl";
    private static final String FEED_ENTRY_PAGE_RANGE_START_INDEX = "pageRangeStartIndex";
    private static final String FEED_ENTRY_PAGE_RANGE_END_INDEX = "pageRangeEndIndex";

    @Override
    public Map<String, String> mapCommonLoadObject(final LoadFeed objectToMap) {
        Map<String, String> loadFeed = new HashMap<String, String>();
        if (StringUtils.isNotBlank(objectToMap.getFeedUrl())) {
            loadFeed.put(FEED_URL, objectToMap.getFeedUrl());

            mapPageRange(objectToMap, loadFeed);
        }

        return loadFeed;
    }

    protected void mapPageRange(final LoadFeed objectToMap, final Map<String, String> loadFeed) {
        if (objectToMap.getFeedEntryPagingRange() == null) {
            setDefaultPageRange(loadFeed);
        } else {
            final int startIndex = objectToMap.getFeedEntryPagingRange().getStartIndex();
            final int endIndex = objectToMap.getFeedEntryPagingRange().getEndIndex();

            loadFeed.put(FEED_ENTRY_PAGE_RANGE_START_INDEX, String.valueOf(startIndex));
            loadFeed.put(FEED_ENTRY_PAGE_RANGE_END_INDEX, String.valueOf(endIndex));
        }
    }

    private void setDefaultPageRange(final Map<String, String> loadFeed) {
        loadFeed.put(FEED_ENTRY_PAGE_RANGE_START_INDEX, getRangeValueOfInteger(1));
        loadFeed.put(FEED_ENTRY_PAGE_RANGE_END_INDEX, getRangeValueOfInteger(Integer.MAX_VALUE));
    }

    private String getRangeValueOfInteger(final int rangeValue) {
        return String.valueOf(rangeValue);
    }
}
