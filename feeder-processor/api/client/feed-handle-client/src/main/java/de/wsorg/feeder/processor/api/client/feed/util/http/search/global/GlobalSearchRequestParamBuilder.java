package de.wsorg.feeder.processor.api.client.feed.util.http.search.global;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper.UrlEncodingWrapper;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class GlobalSearchRequestParamBuilder implements HttpRequestParamBuilder<Map<String, String>> {

    private static final String FEEDS_RESOURCE = "feeds";
    @Inject
    private ConfigurationProvider configurationProvider;
    @Inject
    private UrlEncodingWrapper urlEncodingWrapper;

    @Override
    public HttpRequestBase buildHttpRequest(final Map<String, String> requestParams) {
        final FeederClientConfiguration configuration = configurationProvider.getFeederClientConfiguration();

        final String searchTerm = encodeSearchTerm(requestParams.get("searchTerm"));

        final String uri = configuration.getFeederRestServiceUrl() + FEEDS_RESOURCE + "?searchTerm=" + searchTerm;

        return new HttpGet(uri);
    }

    private String encodeSearchTerm(final String uri) {
        String encodedUrl=null;
        try {
            encodedUrl = urlEncodingWrapper.encode(uri, UrlEncodingWrapper.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Could not build a request url out of provided arguments.");
        }
        return encodedUrl;
    }
}
