package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.generated.request.handler.MarkAllFeedsAsReadType;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class MarkAllFeedsReadabilityRequestToXmlMapper implements RequestToXmlMapper<MarkAllFeedsReadabilityRequest> {
    private static final String JAXB_GENERATED_PAXKAGE_NAME = "de.wsorg.feeder.processor.api.domain.generated.request.handler";
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getRequestData(final MarkAllFeedsReadabilityRequest objectToMap) {
        MarkAllFeedsAsReadType markAllFeedsAsReadType = new MarkAllFeedsAsReadType();
        markAllFeedsAsReadType.setMarkFeedsAsReadType(objectToMap.isFeedsReadStatus());
        markAllFeedsAsReadType.setCategory(objectToMap.getCategory());
        return getJaxbMarshaller().marshal(markAllFeedsAsReadType);
    }

    private JaxbMarshaller getJaxbMarshaller(){
        if(jaxbMarshaller == null) {
            jaxbMarshaller = UtilsFactory.getJaxbMarshaller();
            jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_PAXKAGE_NAME);
        }

        return jaxbMarshaller;
    }
}
