package de.wsorg.feeder.processor.api.client.feed;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederClientFactory {
    private static boolean useFileSystemProtocol=false;

    private static ClassPathXmlApplicationContext applicationContext;

    private static final String ROOT_SPRING_CONTEXT = "classpath:spring/applicationContext.xml";

    private static boolean configProvided=false;

    public static FeederRequestProcessor<GlobalSearchQueryRequest, Opml> getGlobalFeedFinder(){
        validateState();
        return getApplicationContext().getBean("globalSearchRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<GlobalSearchQueryEnrichedWithUserDataRequest, Opml> getGlobalFeedFinderForUser(){
        validateState();
        return getApplicationContext().getBean("globalSearchEnrichedWithUserData", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<UserRelatedSearchQueryRequest, Opml> getUserRelatedFeedFinder(){
        validateState();
        return getApplicationContext().getBean("userRelatedSearchRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<LoadFeedRequest, FeederAtom> getFeedLoader(){
        validateState();
        return getApplicationContext().getBean("loadRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<UserRelatedLoadFeedRequest, FeederAtom> getUserRelatedFeedLoader(){
        validateState();
        return getApplicationContext().getBean("userRelatedLoadFeedRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<FeedSubscriptionRequest, GenericResponseResult> getFeedSubscriptionHandler(){
        validateState();
        return getApplicationContext().getBean("feedSubscriptionRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<MarkFeedEntryReadabilityRequest, GenericResponseResult> getFeedEntryReadMarker(){
        validateState();
        return getApplicationContext().getBean("markFeedEntryReadabilityRequestProcessor", FeederRequestProcessor.class);
    }

    public static FeederRequestProcessor<MarkAllFeedsReadabilityRequest, GenericResponseResult> getAllFeedsReadMarker(){
        validateState();
        return getApplicationContext().getBean("markAllFeedsReadabilityRequestProcessor", FeederRequestProcessor.class);
    }

    public static void setClientConfig(final FeederClientConfiguration clientConfig) {
        if(!getApplicationContext().containsBean("clientConfig"))
            getApplicationContext().getBeanFactory().registerSingleton("clientConfig", clientConfig);

        configProvided = true;
    }

    private static void validateState() {
        if(!configProvided)
            throw new IllegalStateException("Please provide a valid client configuration first");
    }

    private static ClassPathXmlApplicationContext getApplicationContext(){
        if(applicationContext == null){

            if(useFileSystemProtocol)
                applicationContext = new ClassPathXmlApplicationContext(ROOT_SPRING_CONTEXT, "classpath:spring/executionprotocol/feedFileBasedExecutionContext.xml");
            else
                applicationContext = new ClassPathXmlApplicationContext(ROOT_SPRING_CONTEXT, "classpath:spring/executionprotocol/feedHttpBasedExecutionContext.xml");


        }

        return applicationContext;
    }
}
