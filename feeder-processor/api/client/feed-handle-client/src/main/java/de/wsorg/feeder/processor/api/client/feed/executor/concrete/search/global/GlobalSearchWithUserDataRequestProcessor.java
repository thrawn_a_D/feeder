package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.global;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.AbstractSearchTemplate;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * <p/>
 *
 *
 */
@Named("globalSearchEnrichedWithUserData")
public class GlobalSearchWithUserDataRequestProcessor extends AbstractSearchTemplate<GlobalSearchQueryEnrichedWithUserDataRequest> {

    @Inject
    @Named(value = "globalSearchQueryEnrichedWithUserDataMapper")
    private RequestToStringMapMapper<GlobalSearchQueryEnrichedWithUserDataRequest> requestToStringMapMapper;

    @Override
    public RequestToStringMapMapper<GlobalSearchQueryEnrichedWithUserDataRequest> getRequestToTransferMapper() {
        return requestToStringMapMapper;
    }

    @Override
    public FeederCommand getCommandToExecute() {
        return FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA;
    }
}
