package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.global;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.executor.concrete.search.AbstractSearchTemplate;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "globalSearchRequestProcessor")
public class GlobalSearchRequestProcessor extends AbstractSearchTemplate<GlobalSearchQueryRequest> {

    @Inject
    @Named(value = "globalSearchQueryMapper")
    private RequestToStringMapMapper<GlobalSearchQueryRequest> requestToXmlMapper;

    @Override
    public RequestToStringMapMapper<GlobalSearchQueryRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    public FeederCommand getCommandToExecute() {
        return FeederCommand.GLOBAL_SEARCH;
    }
}
