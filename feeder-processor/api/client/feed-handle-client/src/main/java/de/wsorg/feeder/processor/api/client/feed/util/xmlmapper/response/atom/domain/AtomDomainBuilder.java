package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.domain;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtomEntry;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AtomDomainBuilder implements DomainBuilderFactory<FeederAtom, FeederAtomEntry> {
    @Override
    public FeederAtom getAtomModelObject() {
        return new FeederAtom();
    }

    @Override
    public FeederAtomEntry getAtomEntryModelObject() {
        return new FeederAtomEntry();
    }
}
