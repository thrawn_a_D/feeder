package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToStringMapMapper;
import de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common.CommonGlobalMapper;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class GlobalSearchQueryEnrichedWithUserDataMapper implements RequestToStringMapMapper<GlobalSearchQueryEnrichedWithUserDataRequest> {

    @Inject
    private CommonGlobalMapper commonGlobalMapper;

    @Override
    public Map getRequestData(final GlobalSearchQueryEnrichedWithUserDataRequest objectToMap) {
        return commonGlobalMapper.getCommonSearchQuery(objectToMap);
    }
}
