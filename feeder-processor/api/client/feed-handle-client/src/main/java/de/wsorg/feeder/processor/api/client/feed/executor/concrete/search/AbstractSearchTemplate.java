package de.wsorg.feeder.processor.api.client.feed.executor.concrete.search;

import de.wsorg.feeder.processor.api.client.abstraction.executor.AbstractProcessorTemplate;
import de.wsorg.feeder.processor.api.domain.request.Request;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class AbstractSearchTemplate<T extends Request> extends AbstractProcessorTemplate<T, Opml> {
    @Inject
    @Named(value = "opmlResponseMapper")
    protected XmlToDomainMapper<Opml> xmlToDomainMapper;

    @Override
    protected XmlToDomainMapper<Opml> getXmlToDomainMapper() {
        return xmlToDomainMapper;
    }
}
