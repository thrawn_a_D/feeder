package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.opml;

import de.wsorg.feeder.processor.api.domain.generated.response.opml.OPML;
import de.wsorg.feeder.processor.api.domain.generated.response.opml.Outline;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Body;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import java.util.ArrayList;
import java.util.List;

@Named
public class JaxBOpmlToFeederApiOpml implements JaxbPojoToFeederPojoMapper<Opml, JAXBElement> {

    public Opml mapJaxbPojoToFeederPojo(final JAXBElement objectToMap) {
        validateInput(objectToMap);

        OPML unmarshallResult = (OPML) objectToMap.getValue();
        Opml opmlResult = new Opml();
        Body opmlBody = new Body();

        if(unmarshallResult.getBody() != null && unmarshallResult.getBody().getOutline() != null) {
            for (Outline currentOutline : unmarshallResult.getBody().getOutline()) {
                opmlBody.getSearchResultList().addAll(getFeedRecursivelyFromOutline(currentOutline));
            }
        }

        opmlResult.setBody(opmlBody);
        return opmlResult;
    }

    private void validateInput(final JAXBElement objectToMap) {
        if (!(objectToMap.getValue() instanceof OPML))
            throw new IllegalArgumentException("The provided jaxb element does not contail an opml object");
    }

    private List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> getFeedRecursivelyFromOutline(Outline feedOutline) {
        List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> result = new ArrayList<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline>();

        result = getCurrentFeedInfo(feedOutline);
        result.addAll(getChildFeedInfos(feedOutline));

        return result;
    }

    private List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> getChildFeedInfos(Outline feedOutline) {
        List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> result = new ArrayList<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline>();

        if (feedOutline.getOutline() != null &&
                feedOutline.getOutline().size() > 0) {
            for (Outline childOutline : feedOutline.getOutline()) {
                List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> childFeeds = getFeedRecursivelyFromOutline(childOutline);
                result.addAll(childFeeds);
            }
        }

        return result;
    }

    private List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> getCurrentFeedInfo(Outline feedOutline) {
        List<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline> result = new ArrayList<de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline>();

        if (isValidFeed(feedOutline)) {
            de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline currentOutline = getSingleFeedFromOutline(feedOutline);
            result.add(currentOutline);
        }

        return result;
    }

    private boolean isValidFeed(Outline feedOutline) {
        return StringUtils.isNotBlank(feedOutline.getTitle());
    }

    private de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline getSingleFeedFromOutline(Outline usedOutline) {
        de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline result = new de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline();
        result.setTitle(usedOutline.getTitle());
        result.setDescription(usedOutline.getDescription());
        result.setXmlUrl(usedOutline.getXmlUrl());
        return result;
    }
}