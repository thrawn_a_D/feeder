package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common;

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonGlobalMapper {
    Map<String, String> getCommonSearchQuery(final GlobalSearchQuery searchQuery);
}
