package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.response.atom.metadata;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedColorMetadataMapper implements FeedMetadataMapper<FeederAtom> {
    private static final String METADATA_UI_FEED_COLOR="FEED_UI_HIGHLIGHTING_COLOR";

    @Override
    public void mapFeedMetadata(final FeederAtom StandardAtom, final QName qName, final String value) {
        if(qName.getLocalPart().equals(METADATA_UI_FEED_COLOR))
            StandardAtom.setFeedUiHighlightingColor(value);
    }
}