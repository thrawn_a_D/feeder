package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.search.global.common;

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BasicCommonGlobalMapper implements CommonGlobalMapper {

    private static final String SEARCH_TERM = "searchTerm";

    @Override
    public Map<String, String> getCommonSearchQuery(final GlobalSearchQuery searchQuery) {
        Map<String, String> mappingResult = new HashMap<>();

        if(StringUtils.isNotBlank(searchQuery.getSearchTerm()))
            mappingResult.put(SEARCH_TERM, searchQuery.getSearchTerm());

        return mappingResult;
    }
}
