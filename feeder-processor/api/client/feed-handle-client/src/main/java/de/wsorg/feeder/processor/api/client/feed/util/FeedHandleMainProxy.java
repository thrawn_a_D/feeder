package de.wsorg.feeder.processor.api.client.feed.util;


import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.find.FileFinder;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.file.process.JavaRuntimeWrapper;
import de.wsorg.feeder.processor.api.client.abstraction.util.MainExecutorProxy;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class FeedHandleMainProxy implements MainExecutorProxy {

    private static final String JAR_FILE_NAME = "feeder-processor-feed-execution-1.0-SNAPSHOT-jar-with-dependencies.jar";
    @Inject
    private JavaRuntimeWrapper javaRuntimeWrapper;
    @Inject
    private FileFinder fileFinder;
    @Inject
    private ConfigurationProvider configurationProvider;

    @Override
    public void invoke(final String... executionArguments) {
        String executionCommand = getExecutionCommand(executionArguments);

        Process process = startProcess(executionCommand);
        waitForFinish(process);
    }

    private void waitForFinish(final Process process) {
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            throw new RuntimeException("An error occurred while waiting for feeder JAR-File execution to finish!");
        }
    }

    private Process startProcess(final String executionCommand) {
        Process process = null;
        try {
            process = javaRuntimeWrapper.execCommand(executionCommand);
        } catch (IOException e) {
            throw new RuntimeException("An error occurred while executing feeder execution JAR-File!");
        }
        return process;
    }

    private String getExecutionCommand(final String[] executionArguments) {
        final String jarFilePath = getJarFilePath();

        String debugOptions = getDebugOptions();

        String executionCommand = "java" + debugOptions + " -jar " + jarFilePath;
        for (String executionArgument : executionArguments) {
            executionCommand += " " + executionArgument;
        }
        return executionCommand;
    }

    private String getDebugOptions() {
        String debugOptions = "";
        final FeederClientConfiguration configuration = configurationProvider.getFeederClientConfiguration();
        if(configuration.getDebugProcessing()) {
            final int debugPort = 8008;
            debugOptions = " -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address="+debugPort+" ";
            log.debug("Setting logging options to debug execution. Debug-port: " + debugPort);
        }
        return debugOptions;
    }

    private String getJarFilePath() {
        final String jarFilePath = fileFinder.getFilePathOfFile(JAR_FILE_NAME);
        if(StringUtils.isBlank(jarFilePath)) {
            throw new IllegalStateException("The jar file to be executed could not be found:" + JAR_FILE_NAME);
        }
        return jarFilePath;
    }
}
