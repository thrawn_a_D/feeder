package de.wsorg.feeder.processor.api.client.feed.executor.concrete.handling;

import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "feedSubscriptionRequestProcessor")
public class FeedSubscriptionRequestProcessor extends HandleFeedRequestProcessorTemplate<FeedSubscriptionRequest> {

    @Inject
    @Named(value = "feedSubscriptionRequestToXmlMapper")
    private RequestToXmlMapper<FeedSubscriptionRequest> requestToXmlMapper;

    @Override
    protected RequestToXmlMapper<FeedSubscriptionRequest> getRequestToTransferMapper() {
        return requestToXmlMapper;
    }

    @Override
    protected FeederCommand getCommandToExecute() {
        return FeederCommand.FAVOUR_FEED;
    }
}
