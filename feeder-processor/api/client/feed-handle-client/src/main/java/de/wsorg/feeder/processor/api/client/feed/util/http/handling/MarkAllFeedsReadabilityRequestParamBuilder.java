package de.wsorg.feeder.processor.api.client.feed.util.http.handling;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;

@Named
public class MarkAllFeedsReadabilityRequestParamBuilder implements HttpRequestParamBuilder<String> {

    private static final String READ_STATUS_RESOURCE = "feeds/DUMMY/subscriptions/read/all";

    @Inject
    private ConfigurationProvider configurationProvider;

    @Override
    public HttpRequestBase buildHttpRequest(final String requestParams) {
        final String feederServiceUrl = configurationProvider.getFeederClientConfiguration().getFeederRestServiceUrl();
        HttpPost subscribeToFeedRequest = new HttpPost(feederServiceUrl + READ_STATUS_RESOURCE);

        try {
            subscribeToFeedRequest.setEntity(new StringEntity(requestParams));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return subscribeToFeedRequest;
    }
}
