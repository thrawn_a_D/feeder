package de.wsorg.feeder.processor.api.client.feed.util.http.url;

import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class HttpParamCalculator {
    public String calculateOptionalParamString(final String... params){
        String result = "";

        for (String param : params) {
            if (StringUtils.isNotBlank(param)) {
                if(StringUtils.isNotBlank(result))
                    result += "&";
                else
                    result = "?";


                if (param.contains("=")) {
                    final String[] paramElements = StringUtils.split(param, "=", 2);
                    final String paramKey=paramElements[0];
                    final String paramValue=paramElements[1];

                    try {
                        result += paramKey + "=" + URLEncoder.encode(paramValue, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        result += param;
                    }
                } else  {
                    throw new IllegalArgumentException("The provided http parameter is not valid. Should be like 'key=value'");
                }
            }
        }

        return result;
    }
}
