package de.wsorg.feeder.processor.api.client.feed.util.http.search;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.ConfigurationProvider;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.execute.http.HttpRequestParamBuilder;
import de.wsorg.feeder.processor.api.client.feed.util.http.search.wrapper.UrlEncodingWrapper;
import de.wsorg.feeder.processor.api.client.feed.util.http.url.HttpParamCalculator;
import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserRelatedSearchRequestParamBuilder implements HttpRequestParamBuilder<Map<String, String>> {

    private static final String FEEDS_RESOURCE = "feeds";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_USER_NAME = "userName";
    private static final String PARAM_USER_ID = "userId";
    private static final String PARAM_SEARCH_TERM = "searchTerm";
    private static final String PARAM_SHOW_UNREAD_ENTRIES = "showUnreadEntries";

    @Inject
    private ConfigurationProvider configurationProvider;
    @Inject
    private UrlEncodingWrapper urlEncodingWrapper;
    @Inject
    private HttpParamCalculator httpParamCalculator;

    @Override
    public HttpRequestBase buildHttpRequest(final Map<String, String> requestParams) {
        final FeederClientConfiguration configuration = configurationProvider.getFeederClientConfiguration();

        String searchTerm = requestParams.get(PARAM_SEARCH_TERM);
        final String userId = requestParams.get(PARAM_USER_ID);
        final String userName = requestParams.get(PARAM_USER_NAME);
        final String password = requestParams.get(PARAM_PASSWORD);
        String showUnreadEntries = requestParams.get(PARAM_SHOW_UNREAD_ENTRIES);

        searchTerm = getSearchTerm(searchTerm);
        showUnreadEntries = getShowEntriesParam(showUnreadEntries);

        final String optionalParams = httpParamCalculator.calculateOptionalParamString(searchTerm,
                showUnreadEntries);

        final String uri = configuration.getFeederRestServiceUrl() +
                FEEDS_RESOURCE +
                "/" + userId + "/" +
                "subscriptions" +
                optionalParams;


        final HttpGet httpGet = new HttpGet(uri);

        httpGet.addHeader(PARAM_USER_NAME, userName);
        httpGet.addHeader(PARAM_PASSWORD, password);

        return httpGet;
    }

    private String getShowEntriesParam(String showUnreadEntries) {
        if(StringUtils.isNotBlank(showUnreadEntries)) {
            showUnreadEntries = "showUnreadEntries=" + showUnreadEntries;
        } else {
            showUnreadEntries = "";
        }
        return showUnreadEntries;
    }

    private String getSearchTerm(String searchTerm) {
        if(StringUtils.isNotBlank(searchTerm))
            searchTerm = "searchTerm=" + encodeSearchTerm(searchTerm);
        else
            searchTerm = "";
        return searchTerm;
    }

    private String encodeSearchTerm(final String uri) {
        String encodedUrl=null;
        try {
            encodedUrl = urlEncodingWrapper.encode(uri, UrlEncodingWrapper.DEFAULT_ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("Could not build a request url out of provided arguments.");
        }
        return encodedUrl;
    }
}
