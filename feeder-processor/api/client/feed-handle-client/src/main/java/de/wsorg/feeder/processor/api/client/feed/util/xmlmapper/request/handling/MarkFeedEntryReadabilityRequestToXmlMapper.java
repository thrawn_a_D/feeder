package de.wsorg.feeder.processor.api.client.feed.util.xmlmapper.request.handling;


import de.wsorg.feeder.processor.api.client.abstraction.util.xmlmapper.request.RequestToXmlMapper;
import de.wsorg.feeder.processor.api.domain.generated.request.handler.MarkFeedAsReadMapItemType;
import de.wsorg.feeder.processor.api.domain.generated.request.handler.MarkFeedEntryAsReadMapType;
import de.wsorg.feeder.processor.api.domain.generated.request.handler.MarkFeedEntryAsReadType;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class MarkFeedEntryReadabilityRequestToXmlMapper implements RequestToXmlMapper<MarkFeedEntryReadabilityRequest> {
    private static final String JAXB_GENERATED_PAXKAGE_NAME = "de.wsorg.feeder.processor.api.domain.generated.request.handler";
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getRequestData(final MarkFeedEntryReadabilityRequest objectToMap) {
        MarkFeedEntryAsReadType markFeedEntryAsReadType = new MarkFeedEntryAsReadType();

        markFeedEntryAsReadType.setFeedUrl(objectToMap.getFeedUrl());

        setReadabilityStatus(objectToMap, markFeedEntryAsReadType);

        return getJaxbMarshaller().marshal(markFeedEntryAsReadType);
    }

    private void setReadabilityStatus(final MarkFeedEntryReadabilityRequest objectToMap, final MarkFeedEntryAsReadType markFeedEntryAsReadType) {
        MarkFeedEntryAsReadMapType readEntryMap = new MarkFeedEntryAsReadMapType();

        markFeedEntryAsReadType.setMarkFeedEntryWithReadStatus(readEntryMap);

        final Map<String,Boolean> feedEntryUidWithReadabilityStatus = objectToMap.getFeedEntryUidWithReadabilityStatus();

        for (final String feedEntryUid : feedEntryUidWithReadabilityStatus.keySet()) {
            final Boolean feedReadStatus = feedEntryUidWithReadabilityStatus.get(feedEntryUid);

            final MarkFeedAsReadMapItemType mappedReadItemType = new MarkFeedAsReadMapItemType();
            mappedReadItemType.setUid(feedEntryUid);
            mappedReadItemType.setIsRead(feedReadStatus);

            readEntryMap.getItem().add(mappedReadItemType);
        }
    }

    private JaxbMarshaller getJaxbMarshaller(){
        if(jaxbMarshaller == null) {
           jaxbMarshaller = UtilsFactory.getJaxbMarshaller();
            jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_PAXKAGE_NAME);
        }

        return jaxbMarshaller;
    }
}
