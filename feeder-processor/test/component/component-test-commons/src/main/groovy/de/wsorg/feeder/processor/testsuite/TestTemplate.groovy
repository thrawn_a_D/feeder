package de.wsorg.feeder.processor.testsuite

import de.wsorg.feeder.processor.storage.feed.filebased.FeedFileBasedStorage
import de.wsorg.feeder.processor.storage.feed.filebased.FeedLoadStatisticsFileBasedAccess
import de.wsorg.feeder.processor.storage.feed.filebased.FeedToUserFileBasedAccess
import de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased.FileBasedFeedRegistrator
import de.wsorg.feeder.utils.persistence.JavaObjectPersistence
import org.apache.commons.configuration.PropertiesConfiguration
import org.apache.commons.lang.StringUtils
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class TestTemplate extends Specification {
    @Shared
    boolean originalValueStorageUsage

    def setupSpec() {
        setUseFileSystemAsStorage(true)
    }

    def cleanupSpec() {
        setUseFileSystemAsStorage(originalValueStorageUsage)
    }

    def setup(){
        def feedsFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS)
        def associationFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS)
        def loadStatisticsFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS)
        def repositorySubscriptionsFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedFeedRegistrator.UPDATE_MESSAGE_FOLDER)
        def repositorySubscriptionCanceledFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedFeedRegistrator.CANCEL_UPDATING_MESSAGE_FOLDER)

        if(feedsFolder.exists())
            feedsFolder.deleteDir()
        if(associationFolder.exists())
            associationFolder.deleteDir()
        if(loadStatisticsFolder.exists())
            loadStatisticsFolder.deleteDir()
        if(repositorySubscriptionsFolder.exists())
            repositorySubscriptionsFolder.deleteDir()
        if(repositorySubscriptionCanceledFolder.exists())
            repositorySubscriptionCanceledFolder.deleteDir()
    }

    def setUseFileSystemAsStorage(boolean useFileSystemAsStorage) {
        setStoragePropertyInConfiguration('/etc/feeder/feeder_usecase.properties', useFileSystemAsStorage)
        setStoragePropertyInConfiguration('/etc/feeder/user_usecase.properties', useFileSystemAsStorage)
        setStoragePropertyInConfiguration('/etc/feeder/update_manager.properties', useFileSystemAsStorage)
        setStoragePropertyInConfiguration('/etc/feeder/update_receiver.properties', useFileSystemAsStorage)
        setStoragePropertyInConfiguration('/etc/feeder/update_service.properties', useFileSystemAsStorage)
    }

    private void setStoragePropertyInConfiguration(String configurationPath, boolean useFileSystemAsStorage) {
        originalValueStorageUsage = Boolean.valueOf(replacePropertyValueInConfigFile(configurationPath, 'useFileSystemAsStorage', useFileSystemAsStorage as String))
    }

    protected def replacePropertyValueInConfigFile(String configFilePath, String propertyNameToReplace, String setValueTo) {

        def originalValue = ''
        if (StringUtils.isNotBlank(setValueTo)) {
            try {
                def propertiesFile = new File(configFilePath)

                if (propertiesFile.exists()) {
                    def properties = new PropertiesConfiguration(configFilePath)
                    if (properties.getProperty(propertyNameToReplace) != setValueTo) {
                        originalValue = properties.getProperty(propertyNameToReplace)
                        properties.setProperty(propertyNameToReplace, setValueTo)
                        properties.save()
                    }
                } else {
                    def errorMessage = "No local configuration found in ${configFilePath}. This is needed to execute comp-tests."
                    println errorMessage
                    throw new IllegalStateException(errorMessage)
                }
            } catch (Exception ex) {
                println 'An error occured while adjusting properties file to use file system as storage'
                ex.printStackTrace()
                throw ex
            }
        }

        return originalValue
    }

    private String getPropertyValue(String propertiesFileContent, String propertyNameToReplace) {
        def originalValue
        if (propertiesFileContent.endsWith('\n'))
            originalValue = StringUtils.substringsBetween(propertiesFileContent, propertyNameToReplace + '=', '\n')[0]
        else
            originalValue = StringUtils.substringAfter(propertiesFileContent, propertyNameToReplace + '=')
        originalValue
    }
}
