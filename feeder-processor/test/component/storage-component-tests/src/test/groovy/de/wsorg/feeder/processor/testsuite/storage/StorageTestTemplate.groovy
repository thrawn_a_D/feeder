package de.wsorg.feeder.processor.testsuite.storage

import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.storage.user.UserStorageFactory
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.02.13
 */
class StorageTestTemplate extends Specification {
    @Shared
    StorageConfig storageConfig

    def setupSpec() {
        storageConfig = new StorageConfig()

        CouchbaseConfig couchbaseConfig = new CouchbaseConfig()
        couchbaseConfig.addCouchbaseHost('192.168.56.101', '8091')
        couchbaseConfig.feedsBucketName = 'feederFeed'
        couchbaseConfig.feedAssociationBucketName = 'feederFeedAssoziation'
        couchbaseConfig.feederUserBucketName = 'feederUser'
        couchbaseConfig.loadStatisticsBucketName = 'feederLoadStatistics'
        couchbaseConfig.couchbasePassword = '123qweasd'

        ElasticSearchConfig elasticSearchConfig = new ElasticSearchConfig()
        elasticSearchConfig.addElasticSearchHost('192.168.56.101', '9300')
        elasticSearchConfig.setClusterName('elasticsearch')
        elasticSearchConfig.setFeedsIndexName('feeder-feed')
        elasticSearchConfig.setFeedsAssociationIndexName('feeder-feed-assoziation')
        elasticSearchConfig.setFeedsLoadStatisticsIndexName('feeder-feeds-load-statistics')

        storageConfig.setCouchbaseConfig(couchbaseConfig)
        storageConfig.setElasticSearchConfig(elasticSearchConfig)

        FeedStorageFactory.setClientConfig(storageConfig)
        UserStorageFactory.setClientConfig(storageConfig)
    }
}
