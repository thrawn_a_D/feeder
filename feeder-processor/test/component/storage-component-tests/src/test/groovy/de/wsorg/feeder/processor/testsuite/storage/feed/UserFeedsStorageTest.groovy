package de.wsorg.feeder.processor.testsuite.storage.feed

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.storage.StorageTestTemplate
import spock.lang.Ignore

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.02.13
 */
@Ignore
class UserFeedsStorageTest extends StorageTestTemplate {

    def userId = '111333'

    def "Find feeds of a user"() {
        given:
        prepareDataOnStorage()

        when:
        def result = FeedStorageFactory.getUserFeedsDAO().getUserFeeds(userId);

        then:
        result
        result.size() > 0
    }

    def "Find feeds of user and title text"() {
        given:
        prepareDataOnStorage()
        def title='test1'

        when:
        def result = FeedStorageFactory.getUserFeedsDAO().getUserFeeds(userId, title);

        then:
        result
        result.size() == 1
    }

    def prepareDataOnStorage() {
        def feedModel = new FeedModel()
        feedModel.feedUrl = 'http://fesdf1'
        feedModel.title = 'test1'
        feedModel.description = 'testdesc'

        def feedModel2 = new FeedModel()
        feedModel2.feedUrl = 'http://fesdf2'
        feedModel2.title = 'test2'
        feedModel2.description = 'testdesc'

        def subscription1 = new FeedToUserAssociation()
        subscription1.feedUrl = 'http://fesdf1'
        subscription1.userId = userId
        subscription1.associationType = AssociationType.SUBSCRIBER

        def subscription2 = new FeedToUserAssociation()
        subscription2.feedUrl = 'http://fesdf2'
        subscription2.userId = userId
        subscription2.associationType = AssociationType.SUBSCRIBER

        FeedStorageFactory.getFeedDAO().save(feedModel)
        FeedStorageFactory.getFeedDAO().save(feedModel2)
        FeedStorageFactory.feedToUserAssociationDAO.save(subscription1)
        FeedStorageFactory.feedToUserAssociationDAO.save(subscription2)
    }
}
