package de.wsorg.feeder.processor.testsuite.storage.feed

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.storage.StorageTestTemplate
import spock.lang.Ignore

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.02.13
 */
@Ignore
class FeedStorageTest extends StorageTestTemplate {

    def feedUrl1 = 'http://fesdf1'
    def feedUrl2 = 'http://fesdf2'

    def "Store a feed model in the storage"() {
        given:
        def feedModel = new FeedModel()
        feedModel.feedUrl = 'http://fesdf22'
        feedModel.title = 'test3'
        feedModel.description = 'testdesc'
        feedModel.feedEntries = [new FeedEntryModel(title: 'entryTitle')]

        when:
        FeedStorageFactory.getFeedDAO().save(feedModel)
        def result = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedModel.feedUrl)

        then:
        result.feedUrl == feedModel.feedUrl
        result.title == feedModel.title
    }

    def "Get all feeds stored"() {
        given:
        prepareDataOnStorage()

        when:
        def result = FeedStorageFactory.getFeedDAO().findAllLocalFeeds()

        then:
        result.size() >= 2
    }

    def "Find feed by title or description"() {
        given:
        prepareDataOnStorage()

        when:
        def result = FeedStorageFactory.getFeedDAO().findByTitleOrDescription('test1')

        then:
        result.size() == 1
    }

    def "Find by feed url and should match title or description"() {
        given:
        prepareDataOnStorage()

        when:
        def result = FeedStorageFactory.getFeedDAO().findByFeedUrlAndTitleOrDescription(feedUrl1, 'test1')

        then:
        result
    }

    def "Remove a feed from the storage"() {
        given:
        prepareDataOnStorage()


        when:
        def preRemovedFeed = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl1)
        FeedStorageFactory.getFeedDAO().deleteFeed(feedUrl1)
        def removedFeed = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl1)

        then:
        preRemovedFeed != null
        removedFeed == null
    }

    def prepareDataOnStorage() {
        def feedModel = new FeedModel()
        feedModel.feedUrl = feedUrl1
        feedModel.title = 'test1'
        feedModel.description = 'testdesc'

        def feedModel2 = new FeedModel()
        feedModel2.feedUrl = feedUrl2
        feedModel2.title = 'test2'
        feedModel2.description = 'testdesc'

        FeedStorageFactory.setClientConfig(storageConfig)

        FeedStorageFactory.getFeedDAO().save(feedModel)
        FeedStorageFactory.getFeedDAO().save(feedModel2)
    }
}
