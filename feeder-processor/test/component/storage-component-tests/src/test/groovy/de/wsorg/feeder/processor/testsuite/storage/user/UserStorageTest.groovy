package de.wsorg.feeder.processor.testsuite.storage.user

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.storage.user.UserStorageFactory
import de.wsorg.feeder.processor.testsuite.storage.StorageTestTemplate
import spock.lang.Ignore

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.02.13
 */
@Ignore
class UserStorageTest extends StorageTestTemplate {
    def "Store user in storage"() {
        given:
        def userModel = new FeederUseCaseUser()
        userModel.userName = 'mr_white'
        userModel.EMail = 'bla@blaub.de'
        userModel.firstName = 'first'
        userModel.password = 'uzg96g6g'

        when:
        UserStorageFactory.getUserDAO().save(userModel)
        def result = UserStorageFactory.getUserDAO().findByNickName(userModel.getUserName())

        then:
        result == userModel
    }
}
