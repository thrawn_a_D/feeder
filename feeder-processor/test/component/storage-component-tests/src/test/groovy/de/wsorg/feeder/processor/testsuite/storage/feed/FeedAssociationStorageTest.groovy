package de.wsorg.feeder.processor.testsuite.storage.feed

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.storage.StorageTestTemplate
import spock.lang.Ignore

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 13.02.13
 */
@Ignore
public class FeedAssociationStorageTest extends StorageTestTemplate {

    def "Test storing of an association"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://jhbasd'
        model.userId = 'asdkjn98h'
        model.associationType = AssociationType.SUBSCRIBER

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        def result = FeedStorageFactory.feedToUserAssociationDAO.getUserFeedAssociation(model.feedUrl, model.userId)

        then:
        result
    }

    def "Find feeds of a particular user"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://jhbasd'
        model.userId = 'asdkjn98h'
        model.associationType = AssociationType.SUBSCRIBER

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        def feeds = FeedStorageFactory.feedToUserAssociationDAO.getUserFeeds(model.userId)

        then:
        feeds
        feeds.size() > 0
        feeds[0].feedUrl == model.feedUrl
        feeds[0].userId == model.userId
    }

    def "Find associations of related to a feedUrl"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://jhbasd321'
        model.userId = 'asdkjn98h'
        model.associationType = AssociationType.SUBSCRIBER

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        def feeds = FeedStorageFactory.feedToUserAssociationDAO.getAssociationsOfFeed(model.feedUrl)

        then:
        feeds
        feeds.size() > 0
        feeds[0].feedUrl == model.feedUrl
        feeds[0].userId == model.userId
    }

    def "Get feed association by userId and feedUrl"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://jhbasd'
        model.userId = 'asdkjn98h'

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        def result = FeedStorageFactory.feedToUserAssociationDAO.getUserFeedAssociation(model.feedUrl, model.userId)

        then:
        result.feedUrl == model.feedUrl
        result.userId == model.userId
    }

    def "Check is association given"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://neu'
        model.userId = '123qweasd'
        model.associationType = AssociationType.OWNER

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        def result = FeedStorageFactory.feedToUserAssociationDAO.isAssociationGiven(model.feedUrl, model.userId, model.associationType)

        then:
        result == true
    }

    def "Remove association from user to feed"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http://jhbasd'
        model.userId = 'asdkjn98h'
        model.associationType = AssociationType.SUBSCRIBER

        when:
        FeedStorageFactory.feedToUserAssociationDAO.save(model)
        FeedStorageFactory.feedToUserAssociationDAO.removeAssociation(model.feedUrl, model.userId)
        def result = FeedStorageFactory.feedToUserAssociationDAO.getUserFeedAssociation(model.feedUrl, model.userId)


        then:
        result == null
    }
}
