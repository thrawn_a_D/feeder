package de.wsorg.feeder.processor.testsuite.storage.feed;


import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.storage.StorageTestTemplate
import spock.lang.Ignore

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.03.13
 */
@Ignore
public class LoadStatisticsStorageTest extends StorageTestTemplate {

    def feedUrl = 'http://lihih.de'
    def alternativeFeedUrl = 'http://lihih2.de'

    def setup() {
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl)
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(alternativeFeedUrl)
    }

    def "Store load statistic in couchbase"() {
        when:
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().addNewFeedLoadStatistic(feedUrl)
        def loadStatistic = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)

        then:
        loadStatistic
        loadStatistic.feedUrl == feedUrl
    }

    def "Update statistic"() {
        given:
        def newLastUploadedDate = new Date(2030, 04, 04)
        def updateStatistic = new FeedLoadStatistic(feedUrl: feedUrl, lastLoaded: newLastUploadedDate)

        when:
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().addNewFeedLoadStatistic(feedUrl)
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().updateFeedLoadStatistics(updateStatistic)
        def loadStatistic = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)

        then:
        loadStatistic
        loadStatistic.feedUrl == feedUrl
        loadStatistic.lastLoaded == newLastUploadedDate
    }

    def "Remove load statistic"() {
        when:
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().addNewFeedLoadStatistic(feedUrl)
        def loadStatistic = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl)
        def removedStatistic = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)

        then:
        loadStatistic
        removedStatistic == null
    }

    def "Load feeds which are older then"() {
        given:
        def loadStat1 = new FeedLoadStatistic(feedUrl: feedUrl)
        def loadStat2 = new FeedLoadStatistic(feedUrl: alternativeFeedUrl, lastLoaded: new Date(2030, 1, 1))

        and:
        def dateToLookFor = new Date(2020, 12, 12)

        when:
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().updateFeedLoadStatistics(loadStat1)
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().updateFeedLoadStatistics(loadStat2)
        def loadStatistic = FeedStorageFactory.getFeedLoadStatisticsDataAccess().findLoadStatisticsOlderThen(dateToLookFor)

        then:
        loadStatistic
        loadStatistic.size() == 1
        loadStatistic[0].lastLoaded.date == loadStat1.lastLoaded.date
    }
}
