package de.wsorg.feeder.processor.testsuite.user

import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException
import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.storage.user.filebased.UserFileBasedStorage
import de.wsorg.feeder.processor.testsuite.user.template.UserTestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.09.12
 */
public class UserHandlingTest extends UserTestTemplate {
    def "Update an existing user"() {
        given:
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: 'nickname123',
                                                                     password: 'myPassword',
                                                                     firstName: 'asdasd',
                                                                     lastName: 'asdasasaa')

        and:
        HandleUserRequest updateUserRequest = new HandleUserRequest()
        updateUserRequest.handlingType = UserHandlingType.UPDATE
        updateUserRequest.userToBeHandled = createUserRequest.userToBeHandled
        updateUserRequest.firstName = 'updatedFirstName'
        updateUserRequest.lastName = 'updatedLastName'

        and:

        UserLoginRequest userLoginRequest = new UserLoginRequest(nickName: createUserRequest.userToBeHandled.userName,
                                                                 password: createUserRequest.userToBeHandled.password)

        and:
        def creationResult = UserClientFactory.userCreator.executeRequest(createUserRequest)
        def loginResult = UserClientFactory.userValidator.executeRequest(userLoginRequest)

        and:
        updateUserRequest.userId = loginResult.userId

        when:
        def updateResult = UserClientFactory.userHandler.executeRequest(updateUserRequest)

        then:
        creationResult.processingResult == ProcessingResult.SUCCESS
        loginResult
        updateResult.processingResult == ProcessingResult.SUCCESS

        and:
        persistance.getFolderContent(UserFileBasedStorage.STORAGE_FOLDER_NAME)[0].firstName == 'updatedFirstName'
        persistance.getFolderContent(UserFileBasedStorage.STORAGE_FOLDER_NAME)[0].lastName == 'updatedLastName'
        persistance.getFolderContent(UserFileBasedStorage.STORAGE_FOLDER_NAME)[0].password == 'myPassword'
        persistance.getFolderContent(UserFileBasedStorage.STORAGE_FOLDER_NAME)[0].userName == 'nickname123'
    }

    def "Make sure user updating is authenticated"() {
        given:
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: 'nickname123',
                password: 'myPassword',
                firstName: 'asdasd',
                lastName: 'asdasasaa')

        and:
        HandleUserRequest updateUserRequest = new HandleUserRequest()
        updateUserRequest.handlingType = UserHandlingType.UPDATE
        updateUserRequest.userName = createUserRequest.userToBeHandled.userName
        updateUserRequest.firstName = 'asdasd'
        updateUserRequest.lastName = 'lkmlkmlkn'
        updateUserRequest.password = 'htresdfhj'

        when:
        def creationResult = UserClientFactory.userCreator.executeRequest(createUserRequest)
        UserClientFactory.userHandler.executeRequest(updateUserRequest)

        then:
        creationResult.processingResult == ProcessingResult.SUCCESS

        and:
        def ex = thrown(FeederProcessingException)
        ex.message == 'The provided user credentials are not valid'

    }
}
