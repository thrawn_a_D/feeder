package de.wsorg.feeder.processor.testsuite.user

import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.testsuite.user.template.UserTestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.09.12
 */
class UserLoginTest extends UserTestTemplate {

    def "Create a user and login with this user"() {
        given:
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: 'someNickName',
                                                           EMail: 'test@bla.com',
                                                           password: 'myPassword',
                                                           lastName: 'lastN',
                                                           firstName: 'firstN')

        and:
        UserLoginRequest userLoginRequest = new UserLoginRequest(nickName: createUserRequest.userToBeHandled.userName,
                                                                 password: createUserRequest.userToBeHandled.password)


        when:
        def creationResult = UserClientFactory.userCreator.executeRequest(createUserRequest)
        def loggedInUser = UserClientFactory.userValidator.executeRequest(userLoginRequest)

        then:
        creationResult.processingResult == ProcessingResult.SUCCESS
        loggedInUser != null
        loggedInUser.EMail == createUserRequest.userToBeHandled.EMail
        loggedInUser.userName == createUserRequest.userToBeHandled.userName
        loggedInUser.password == createUserRequest.userToBeHandled.password
        loggedInUser.firstName == createUserRequest.userToBeHandled.firstName
        loggedInUser.lastName == createUserRequest.userToBeHandled.lastName

        loggedInUser.userId != ""
        loggedInUser.userId != createUserRequest.userToBeHandled.userId
    }
}