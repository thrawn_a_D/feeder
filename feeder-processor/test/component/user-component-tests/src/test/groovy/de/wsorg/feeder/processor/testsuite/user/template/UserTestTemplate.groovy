package de.wsorg.feeder.processor.testsuite.user.template

import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration
import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.storage.user.filebased.UserFileBasedStorage
import de.wsorg.feeder.processor.testsuite.TestTemplate
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import de.wsorg.feeder.utils.persistence.JavaObjectPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.01.13
 */
class UserTestTemplate extends TestTemplate {
    @Shared
    FileBasedPersistence persistance

    def setupSpec() {
        UserClientFactory.useFileSystemProtocol = true

        persistance = UtilsFactory.fileBasedPersistence

        FeederClientConfiguration configuration = new SimpleFeederClientConfiguration()
        configuration.setFeederRestServiceUrl("http://localhost:8080/user-handling/")
        UserClientFactory.setClientConfig(configuration)
    }

    def setup(){
        def usersFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + UserFileBasedStorage.STORAGE_FOLDER_NAME)

        if(usersFolder.exists())
            usersFolder.deleteDir()
    }
}
