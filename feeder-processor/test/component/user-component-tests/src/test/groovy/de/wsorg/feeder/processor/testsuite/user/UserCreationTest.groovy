package de.wsorg.feeder.processor.testsuite.user

import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException
import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.testsuite.user.template.UserTestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.06.13
 */
class UserCreationTest extends UserTestTemplate {
    def "Create user"() {
        given:
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: 'nickname123',
                password: 'myPassword')

        when:
        def result = UserClientFactory.userCreator.executeRequest(createUserRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
    }

    def "Try to create a redundant user"() {
        given:
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: 'nickname123',
                password: 'myPassword')

        when:
        def result1 = UserClientFactory.userCreator.executeRequest(createUserRequest)
        UserClientFactory.userCreator.executeRequest(createUserRequest)

        then:
        result1.processingResult == ProcessingResult.SUCCESS
        def result2 = thrown(FeederProcessingException)
        result2.message == "The provided username is already in use!"
    }

}
