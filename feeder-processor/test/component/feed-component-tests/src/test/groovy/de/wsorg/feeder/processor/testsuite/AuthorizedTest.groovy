package de.wsorg.feeder.processor.testsuite

import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.testsuite.feed.FeedTestTemplate
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.01.13
 */
class AuthorizedTest extends FeedTestTemplate {
    static final String DEFAULT_USER_NAME='Mr.Wulf'
    static final String DEFAULT_USER_PASSWORD='somePassWd'

    @Shared
    FeederUseCaseUser loggedInUser

    def setupSpec() {

        try {
            loggedInUser = loginUser()
        } catch (Exception e) {
            try {
                createUser()
                loggedInUser = loginUser()
            } catch (RuntimeException ex) {
                ex.printStackTrace()
            }
        }
    }

    private FeederClientSideUser loginUser() {
        UserLoginRequest userLoginRequest = new UserLoginRequest(nickName: DEFAULT_USER_NAME,
                                                                 password: DEFAULT_USER_PASSWORD)
        loggedInUser = UserClientFactory.userValidator.executeRequest(userLoginRequest)
        loggedInUser
    }

    private void createUser() {
        CreateUserRequest createUserRequest = new CreateUserRequest()
        createUserRequest.userToBeHandled = new FeederClientSideUser(userName: DEFAULT_USER_NAME,
                                                                     password: DEFAULT_USER_PASSWORD)

        UserClientFactory.userCreator.executeRequest(createUserRequest)
    }
}
