package de.wsorg.feeder.processor.testsuite.feed.loader

import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.testsuite.feed.FeedTestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
class FeedLoaderTest extends FeedTestTemplate {

    def "Load a feed using an url"() {
        given:
        final def feedUrl = "http://cre.fm/feed"
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: feedUrl)

        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        feedLoadingResult != null
        feedLoadingResult.title
        feedLoadingResult.description
        feedLoadingResult.feedEntries.size() > 0
        feedLoadingResult.feedEntries.every {it.feedUrl == feedUrl}
        feedLoadingResult.feedEntries[0].links
    }

    def "Load an atom feed"() {
        given:
        def atomFeed = 'http://rss.golem.de/rss.php?feed=ATOM1.0'
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: atomFeed)

        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        feedLoadingResult != null
        feedLoadingResult.feedEntries.size() > 0
        feedLoadingResult.feedEntries.every {it.feedUrl == atomFeed}
        feedLoadingResult.feedEntries[0].links
    }

    def "feed update date is set"() {
        given:
        def atomFeed = 'http://rss.golem.de/rss.php?feed=RSS1.0'
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: atomFeed)

        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        feedLoadingResult != null
        feedLoadingResult.feedEntries.size() > 0
        feedLoadingResult.feedEntries.every {it.updated != null}
    }

    def "Load a feed using an other specific rss url"() {
        given:
        final def feedUrl = "http://feeds.feedburner.com/musicwewspodcast"
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: feedUrl)

        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        feedLoadingResult != null
        feedLoadingResult.feedEntries.size() > 0
    }
}
