package de.wsorg.feeder.processor.testsuite.feed.exception

import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException
import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.testsuite.TestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.01.13
 */
public class ExceptionHandlingTest extends TestTemplate {

    def "Expect an exception for an invalid url"() {
        given:
        def atomFeed = 'http://lifehacker.com/'
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: atomFeed)

        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        def ex = thrown(FeederProcessingException)
        println ex.message
    }
}