package de.wsorg.feeder.processor.testsuite.feed.subscription

import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.testsuite.AuthorizedTest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.08.12
 */
class FeedSubscriptionTest extends AuthorizedTest {
    def "Mark a feed as favoured"() {
        given:
        def feedUrl = 'http://cre.fm/feed'
        def handleFeedRequest = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: true, userId: loggedInUser.userId)
        def loadFeedRequest = new UserRelatedLoadFeedRequest(feedUrl: feedUrl, userId: loggedInUser.userId)
        loadFeedRequest.userName = DEFAULT_USER_NAME
        loadFeedRequest.password = DEFAULT_USER_PASSWORD
        handleFeedRequest.userName = DEFAULT_USER_NAME
        handleFeedRequest.password = DEFAULT_USER_PASSWORD

        when:
        def result = FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)
        FeederAtom atom = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeedRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
        atom.favoured
    }

    def "Unset favoured status of a feed"() {
        given:
        def feedUrl = 'http://cre.fm/feed'
        def markFeedAsFavoured = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: true, userId: loggedInUser.userId)
        def unmarkFeedAsFavoured = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: false, userId: loggedInUser.userId)
        def loadFeedRequest = new UserRelatedLoadFeedRequest(feedUrl: feedUrl, userId: loggedInUser.userId)
        loadFeedRequest.userName = DEFAULT_USER_NAME
        loadFeedRequest.password = DEFAULT_USER_PASSWORD
        markFeedAsFavoured.userName = DEFAULT_USER_NAME
        markFeedAsFavoured.password = DEFAULT_USER_PASSWORD
        unmarkFeedAsFavoured.userName = DEFAULT_USER_NAME
        unmarkFeedAsFavoured.password = DEFAULT_USER_PASSWORD

        when:
        def resultOfFavourSetting = FeederClientFactory.feedSubscriptionHandler.executeRequest(markFeedAsFavoured)
        FeederAtom atomAfterFeedSubscribtion = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeedRequest)

        def resultOfUnfavourSetting = FeederClientFactory.feedSubscriptionHandler.executeRequest(unmarkFeedAsFavoured)
        FeederAtom atomAfterFeedUnsubscribtion = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeedRequest)

        then:
        resultOfFavourSetting.processingResult == ProcessingResult.SUCCESS
        resultOfUnfavourSetting.processingResult == ProcessingResult.SUCCESS
        atomAfterFeedSubscribtion.favoured
        !atomAfterFeedUnsubscribtion.favoured
    }
}
