package de.wsorg.feeder.processor.testsuite.feed.loader.paging

import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.testsuite.feed.FeedTestTemplate

/**
 * @author wschneider
 * Date: 04.12.12
 * Time: 15:30
 */
class FeedEntryPagingTest extends FeedTestTemplate {
    def "Load a feed using paging functionality"() {
        given:
        final def feedUrl = "http://cre.fm/feed"
        final LoadFeedRequest loadFeedRequest = new LoadFeedRequest(feedUrl: feedUrl)

        and:"set a page range"
        loadFeedRequest.getFeedEntryPagingRange().startIndex = 10
        loadFeedRequest.getFeedEntryPagingRange().endIndex = 20


        when:
        FeederAtom feedLoadingResult = FeederClientFactory.feedLoader.executeRequest(loadFeedRequest)

        then:
        feedLoadingResult != null
        feedLoadingResult.feedEntries.size() == 10
        feedLoadingResult.additionalPagingPossible == true
    }
}
