package de.wsorg.feeder.processor.testsuite.feed.authentication

import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException
import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.testsuite.AuthorizedTest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.01.13
 */
class AuthenticatedLoadFeed extends AuthorizedTest {
    def "Use a user id not related to the provided username and password"() {
        given:
        def feedUrl = 'http://cre.fm/feed'
        def handleFeedRequest = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: true, userId: '111')
        handleFeedRequest.userName = DEFAULT_USER_NAME
        handleFeedRequest.password = DEFAULT_USER_PASSWORD

        when:
        FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)

        then:
        def ex = thrown(FeederProcessingException)
        ex.message == 'The provided user id does not match username and password'
    }
}
