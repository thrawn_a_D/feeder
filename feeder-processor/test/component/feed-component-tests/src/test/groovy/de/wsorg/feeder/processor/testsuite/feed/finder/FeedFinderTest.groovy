package de.wsorg.feeder.processor.testsuite.feed.finder;


import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml
import de.wsorg.feeder.processor.testsuite.AuthorizedTest
import org.apache.commons.lang.StringUtils
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.07.12
 */
class FeedFinderTest extends AuthorizedTest {

    @Shared
    FeederRequestProcessor<GlobalSearchQueryRequest, Opml> globalFeedFinder

    @Shared
    FeederRequestProcessor<GlobalSearchQueryEnrichedWithUserDataRequest, Opml> globalFeedFinderWithEnrichment

    @Shared
    FeederRequestProcessor<UserRelatedSearchQueryRequest, Opml> userFeedFinder

    def setupSpec(){
        globalFeedFinder = FeederClientFactory.globalFeedFinder
        globalFeedFinderWithEnrichment = FeederClientFactory.globalFeedFinderForUser
        userFeedFinder = FeederClientFactory.userRelatedFeedFinder
    }

    def "Find a feed using a simple query"() {
        given:
        GlobalSearchQueryRequest searchRequest = new GlobalSearchQueryRequest(searchTerm: 'news')

        when:
        Opml searchResult = globalFeedFinder.executeRequest(searchRequest)

        then:
        searchResult?.body?.searchResultList?.size() > 0
        searchResult?.body?.searchResultList?.any {!it.isFavoured()}
        StringUtils.isNotBlank(searchResult?.body?.searchResultList[0].title)
        StringUtils.isNotBlank(searchResult?.body?.searchResultList[0].description)
        StringUtils.isNotBlank(searchResult?.body?.searchResultList[0].xmlUrl)
    }

    def "Find user feeds"() {
        given:
        FeedSubscriptionRequest handleFeedRequest = new FeedSubscriptionRequest(feedUrl: 'http://cre.fm/feed',
                                                                                favourFeed: true,
                                                                                userId: loggedInUser.userId,
                                                                                userName: DEFAULT_USER_NAME,
                                                                                password: DEFAULT_USER_PASSWORD)

        UserRelatedSearchQueryRequest searchQueryRequest = new UserRelatedSearchQueryRequest(userId: loggedInUser.userId,
                                                                                             userName: DEFAULT_USER_NAME,
                                                                                             password: DEFAULT_USER_PASSWORD)

        when:
        FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)
        Opml searchResult = userFeedFinder.executeRequest(searchQueryRequest)

        then:
        searchResult?.body?.searchResultList?.size() == 1
        searchResult?.body?.searchResultList.any {it.xmlUrl == 'http://cre.fm/feed' && it.isFavoured()}
        searchResult?.body?.searchResultList.every {!it.feedUiHighlightingColor.isEmpty()}
    }

    def "Find user feeds containing text"() {
        given:
        FeedSubscriptionRequest handleFeedRequest = new FeedSubscriptionRequest(feedUrl: 'http://cre.fm/feed',
                                                                                favourFeed: true,
                                                                                userId: loggedInUser.userId,
                                                                                userName: DEFAULT_USER_NAME,
                                                                                password: DEFAULT_USER_PASSWORD)

        UserRelatedSearchQueryRequest searchQueryRequest = new UserRelatedSearchQueryRequest(userId: loggedInUser.userId,
                                                                                             searchTerm: 'CRE: Technik, Kultur, Gesellschaft',
                                                                                             userName: DEFAULT_USER_NAME,
                                                                                             password: DEFAULT_USER_PASSWORD)

        when:
        FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)
        Opml searchResult = userFeedFinder.executeRequest(searchQueryRequest)

        then:
        searchResult?.body?.searchResultList?.size() == 1
        searchResult?.body?.searchResultList.any {it.xmlUrl == 'http://cre.fm/feed' && it.isFavoured()}
    }

    def "Find global feeds considering a user id"() {
        given:
        GlobalSearchQueryEnrichedWithUserDataRequest globalSearch = new GlobalSearchQueryEnrichedWithUserDataRequest(searchTerm: 'news',
                                                                                                                     userId: loggedInUser.userId,
                                                                                                                     userName: DEFAULT_USER_NAME,
                                                                                                                     password: DEFAULT_USER_PASSWORD)


        and:
        Opml firstSearchResult = globalFeedFinderWithEnrichment.executeRequest(globalSearch)

        and:
        def feedUrlToSubscribe = firstSearchResult.body.searchResultList[0].xmlUrl
        FeedSubscriptionRequest handleFeedRequest = new FeedSubscriptionRequest(feedUrl: feedUrlToSubscribe,
                                                                                favourFeed: true,
                                                                                userId: loggedInUser.userId,
                                                                                userName: DEFAULT_USER_NAME,
                                                                                password: DEFAULT_USER_PASSWORD)
        FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)

        and:
        globalSearch.setUserId(loggedInUser.userId)

        when:
        Opml secondSearchResult = globalFeedFinderWithEnrichment.executeRequest(globalSearch)

        then:
        secondSearchResult?.body?.searchResultList?.size() > 1
        secondSearchResult?.body?.searchResultList.any {it.xmlUrl == feedUrlToSubscribe && it.isFavoured()}
        secondSearchResult?.body?.searchResultList.findAll {it.xmlUrl == feedUrlToSubscribe}.size() == 1
    }
}