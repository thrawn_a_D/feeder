package de.wsorg.feeder.processor.testsuite.feed;


import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration
import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.client.user.UserClientFactory
import de.wsorg.feeder.processor.testsuite.TestTemplate

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.02.13
 */
public class FeedTestTemplate extends TestTemplate {
    def setupSpec() {
        setupConfig()

        FeederClientFactory.useFileSystemProtocol = true

        FeederClientConfiguration configuration = new SimpleFeederClientConfiguration()
        configuration.feederRestServiceUrl = 'http://localhost:8080/feeder-handling/'

        FeederClientFactory.setClientConfig(configuration)
    }

    def cleanupSpec() {
        setUseFileSystemAsStorage(originalValueStorageUsage)
    }

    private void setupConfig() {
        FeederClientFactory.useFileSystemProtocol = true
        UserClientFactory.useFileSystemProtocol = true

        FeederClientConfiguration userConfiguration = new SimpleFeederClientConfiguration()
        userConfiguration.setFeederRestServiceUrl("http://localhost:8080/user-handling/")
        FeederClientConfiguration feedConfiguration = new SimpleFeederClientConfiguration()
        feedConfiguration.setFeederRestServiceUrl("http://localhost:8080/feeder-handling/")

        // Set to true if debugging of jar is wanted
        userConfiguration.setDebugProcessing(false);
        feedConfiguration.setDebugProcessing(false);

        UserClientFactory.setClientConfig(userConfiguration)
        FeederClientFactory.setClientConfig(feedConfiguration)
    }

}
