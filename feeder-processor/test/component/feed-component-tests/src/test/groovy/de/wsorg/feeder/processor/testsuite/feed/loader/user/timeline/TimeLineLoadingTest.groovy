package de.wsorg.feeder.processor.testsuite.feed.loader.user.timeline

import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.testsuite.AuthorizedTest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.12.12
 */
class TimeLineLoadingTest extends AuthorizedTest {
    def "Show users time line"() {
        given:
        def handleFeedRequest1 = new FeedSubscriptionRequest(feedUrl: 'http://cre.fm/feed', favourFeed: true, userId: loggedInUser.userId)
        handleFeedRequest1.userName = DEFAULT_USER_NAME
        handleFeedRequest1.password = DEFAULT_USER_PASSWORD
        def handleFeedRequest2 = new FeedSubscriptionRequest(feedUrl: 'http://golem.de.dynamic.feedsportal.com/pf/578068/http://rss.golem.de/rss.php?feed=RSS2.0', favourFeed: true, userId: loggedInUser.userId)
        handleFeedRequest2.userName = DEFAULT_USER_NAME
        handleFeedRequest2.password = DEFAULT_USER_PASSWORD
        def loadFeedRequest = new UserRelatedLoadFeedRequest(feedUrl: 'http://feeder.net/timeline',
                                                              userId: loggedInUser.userId,
                                                              userName: DEFAULT_USER_NAME,
                                                              password: DEFAULT_USER_PASSWORD)


        when:
        def subscriptionResult1 = FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest1)
        def subscriptionResult2 = FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest2)
        FeederAtom atom = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeedRequest)

        then:
        subscriptionResult1.processingResult == ProcessingResult.SUCCESS
        subscriptionResult2.processingResult == ProcessingResult.SUCCESS
        atom.feedEntries.size() > 0
        atom.feedEntries.any {it.links[0].href}
        atom.feedEntries.any {it.id}
        atom.feedEntries.any {!it.feedUiHighlightingColor.isEmpty()}
    }
}
