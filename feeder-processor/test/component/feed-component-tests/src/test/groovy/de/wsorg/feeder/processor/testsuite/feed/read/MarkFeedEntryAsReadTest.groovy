package de.wsorg.feeder.processor.testsuite.feed.read

import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.testsuite.AuthorizedTest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.11.12
 */
class MarkFeedEntryAsReadTest extends AuthorizedTest {
    def "Mark feedentry as read"() {
        given:
        def feedUrl = 'http://cre.fm/feed'
        def handleFeedRequest = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: true, userId: loggedInUser.userId)
        def readStatus = new MarkFeedEntryReadabilityRequest(feedUrl: feedUrl, userId: loggedInUser.userId)
        def loadFeed = new UserRelatedLoadFeedRequest(feedUrl: feedUrl, userId: loggedInUser.userId)
        loadFeed.userName = DEFAULT_USER_NAME
        loadFeed.password = DEFAULT_USER_PASSWORD
        handleFeedRequest.userName = DEFAULT_USER_NAME
        handleFeedRequest.password = DEFAULT_USER_PASSWORD
        readStatus.userName = DEFAULT_USER_NAME
        readStatus.password = DEFAULT_USER_PASSWORD

        and:
        def subscriptionResult = FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)
        def myFeed = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeed)

        //ATTENTION: there are no checks which ensure that the uid of the feed entry indeed exists
        def readEntry = myFeed.feedEntries[0]
        readStatus.feedEntryUidWithReadabilityStatus.put(readEntry.id, true)

        when:
        def markResult = FeederClientFactory.feedEntryReadMarker.executeRequest(readStatus)

        and:
        def myFeed2 = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeed)

        then:
        subscriptionResult.processingResult == ProcessingResult.SUCCESS
        markResult.processingResult == ProcessingResult.SUCCESS
        myFeed2.feedEntries.find {it.entryAlreadyRead}.id == readEntry.id
    }

    def "Load a feed and show only unread entries"() {
        given:
        def feedUrl = 'http://cre.fm/feed'
        def handleFeedRequest = new FeedSubscriptionRequest(feedUrl: feedUrl, favourFeed: true, userId: loggedInUser.userId)
        def readStatus = new MarkFeedEntryReadabilityRequest(feedUrl: feedUrl, userId: loggedInUser.userId)
        def loadFeed = new UserRelatedLoadFeedRequest(feedUrl: feedUrl, userId: loggedInUser.userId, showOnlyUnreadEntries: true)
        loadFeed.userName = DEFAULT_USER_NAME
        loadFeed.password = DEFAULT_USER_PASSWORD
        handleFeedRequest.userName = DEFAULT_USER_NAME
        handleFeedRequest.password = DEFAULT_USER_PASSWORD
        readStatus.userName = DEFAULT_USER_NAME
        readStatus.password = DEFAULT_USER_PASSWORD

        and:
        def subscriptionResult = FeederClientFactory.feedSubscriptionHandler.executeRequest(handleFeedRequest)
        def myFeed = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeed)

        //ATTENTION: there are no checks which ensure that the uid of the feed entry indeed exists
        def readEntry = myFeed.feedEntries[0]
        readStatus.feedEntryUidWithReadabilityStatus.put(readEntry.id, true)

        when:
        def markResult = FeederClientFactory.feedEntryReadMarker.executeRequest(readStatus)

        and:
        def myFeed2 = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeed)

        then:
        subscriptionResult.processingResult == ProcessingResult.SUCCESS
        markResult.processingResult == ProcessingResult.SUCCESS
        myFeed2.feedEntries.every {it.isEntryAlreadyRead == false}
    }

    def "Mark all feeds and their entries as read"() {
        given:
        def feedUrl1 = 'http://cre.fm/feed'
        def feedUrl2 = 'http://cre.fm/feed'

        and:
        def subscribeToFeedRequest1 = new FeedSubscriptionRequest(feedUrl: feedUrl1, favourFeed: true, userId: loggedInUser.userId)
        def subscribeToFeedRequest2 = new FeedSubscriptionRequest(feedUrl: feedUrl2, favourFeed: true, userId: loggedInUser.userId)

        and:
        def markFeedsAsRead = new MarkAllFeedsReadabilityRequest(userId: loggedInUser.userId, feedsReadStatus: true)
        def loadFeed = new UserRelatedLoadFeedRequest(feedUrl: 'http://feeder.net/timeline', userId: loggedInUser.userId)

        loadFeed.userName = DEFAULT_USER_NAME
        loadFeed.password = DEFAULT_USER_PASSWORD
        subscribeToFeedRequest1.userName = DEFAULT_USER_NAME
        subscribeToFeedRequest1.password = DEFAULT_USER_PASSWORD
        subscribeToFeedRequest2.userName = DEFAULT_USER_NAME
        subscribeToFeedRequest2.password = DEFAULT_USER_PASSWORD
        markFeedsAsRead.userName = DEFAULT_USER_NAME
        markFeedsAsRead.password = DEFAULT_USER_PASSWORD

        and:
        def subscriptionResult1 = FeederClientFactory.feedSubscriptionHandler.executeRequest(subscribeToFeedRequest1)
        def subscriptionResult2 = FeederClientFactory.feedSubscriptionHandler.executeRequest(subscribeToFeedRequest2)

        when:
        def markResult = FeederClientFactory.allFeedsReadMarker.executeRequest(markFeedsAsRead)

        and:
        def feedTimeLine = FeederClientFactory.userRelatedFeedLoader.executeRequest(loadFeed)

        then:
        subscriptionResult1.processingResult == ProcessingResult.SUCCESS
        subscriptionResult2.processingResult == ProcessingResult.SUCCESS
        markResult.processingResult == ProcessingResult.SUCCESS
        feedTimeLine.feedEntries.every {it.entryAlreadyRead == true}
    }
}
