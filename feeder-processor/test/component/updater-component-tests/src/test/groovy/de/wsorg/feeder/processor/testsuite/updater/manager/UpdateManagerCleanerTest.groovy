package de.wsorg.feeder.processor.testsuite.updater.manager

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.filebased.FeedFileBasedStorage
import de.wsorg.feeder.processor.storage.feed.filebased.FeedLoadStatisticsFileBasedAccess
import de.wsorg.feeder.processor.storage.feed.filebased.FeedToUserFileBasedAccess
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.processor.updater.manager.util.communicator.filebased.FileBasedCallDelegator
import de.wsorg.feeder.processor.updater.manager.util.testsupport.UpdateManagerCleanProcessMain
import de.wsorg.feeder.processor.updater.manager.util.testsupport.eventlistener.UpdateManagerEventConstants
import de.wsorg.feeder.processor.updater.service.util.communication.filebased.FileBasedCommunicator
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.03.13
 */
class UpdateManagerCleanerTest extends FeedUpdaterTestTemplate {
    @Shared
    String originalValueLocalCleanupTime
    @Shared
    String originalValueExternalCleanupTime
    @Shared
    FileBasedPersistence persistance

    def setupSpec() {
        persistance = UtilsFactory.fileBasedPersistence
        originalValueLocalCleanupTime = replacePropertyValueInConfigFile('/etc/feeder/update_manager.properties',
                                                                         'updater.localCleanupTimeAsCronFormat',
                                                                         '* * * * * *')
        originalValueExternalCleanupTime = replacePropertyValueInConfigFile('/etc/feeder/update_manager.properties',
                                                                         'updater.superfeedrCleanupTimeAsCronFormat',
                                                                         '* * * * * *')
    }

    def cleanupSpec() {
        replacePropertyValueInConfigFile('/etc/feeder/update_manager.properties',
                                         'updater.localCleanupTimeAsCronFormat',
                                         originalValueLocalCleanupTime)
        replacePropertyValueInConfigFile('/etc/feeder/update_manager.properties',
                                         'updater.superfeedrCleanupTimeAsCronFormat',
                                         originalValueExternalCleanupTime)
    }

    def "Test cleanup of feed subscriptions based on the local system (expired load stats exists)"() {
        given:
        def feedUrl = 'http://myfeed.de'
        def expiredLoadStatistic = new FeedLoadStatistic(feedUrl: feedUrl, lastLoaded: new Date(2010, 10, 11))
        def actualFeed = new FeedModel(feedUrl: feedUrl)

        and:
        startServiceEndpoint(UpdateManagerCleanProcessMain, [UpdateManagerEventConstants.CLEANUP_SUBSCRIBED_FEEDS])

        when:
        persistance.save(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS, expiredLoadStatistic.feedUrl, expiredLoadStatistic)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, actualFeed.feedUrl, actualFeed)

        and:
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER)[0]
        persistance.getFolderContent(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER)[0].identifier == actualFeed.feedUrl
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS)[0]
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS)[0].feedUrl == actualFeed.feedUrl
        persistance.getFolderContent(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS).size() == 0
    }

    def "Test cleanup based on the superfeedr"() {
        given:
        def feedUrlCorrectSubscription = 'http://correct.de'
        def feedUrlNoExternalSubscription = 'http://invalid.de'

        and:
        def correctSubscriptionLoadStatistic = new FeedLoadStatistic(feedUrl: feedUrlCorrectSubscription)
        def correctSubscriptionFeed = new FeedModel(feedUrl: feedUrlCorrectSubscription)
        def correctSubscriptionFeedToUSerAssociation = new FeedToUserAssociation(feedUrl: feedUrlCorrectSubscription)
        def noExternalLoadStatistic = new FeedLoadStatistic(feedUrl: feedUrlNoExternalSubscription)
        def noExternalActualFeed = new FeedModel(feedUrl: feedUrlNoExternalSubscription)

        and:
        startServiceEndpoint(UpdateManagerCleanProcessMain, [UpdateManagerEventConstants.SUPERFEEDER_BASED_CLEANUP_SUBSCRIBED_FEEDS])

        when:
        def feedUrls = [feedUrlCorrectSubscription, feedUrlNoExternalSubscription].toString()
        def message = new StringJobMessage(feedUrls)
        persistance.save(FileBasedCommunicator.GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER, feedUrls, message)

        and:
        persistance.save(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS, correctSubscriptionFeedToUSerAssociation.feedUrl, correctSubscriptionFeedToUSerAssociation)
        persistance.save(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS, correctSubscriptionLoadStatistic.feedUrl, correctSubscriptionLoadStatistic)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, correctSubscriptionFeed.feedUrl, correctSubscriptionFeed)
        persistance.save(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS, noExternalLoadStatistic.feedUrl, noExternalLoadStatistic)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, noExternalActualFeed.feedUrl, noExternalActualFeed)

        and:
        waitTillWantedTestingEventsOccurred()

        then:
        //Request for all subscribed feeds is initiated
        persistance.getFolderContent(FileBasedCallDelegator.GET_ALL_SUBSCRIBED_FEEDS_FOLDER).size() >= 1
        //Cleanup has been processed properly
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS).size() == 1
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS)[0].feedUrl == feedUrlCorrectSubscription
        persistance.getFolderContent(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS).size() == 1
        persistance.getFolderContent(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS)[0].feedUrl == feedUrlCorrectSubscription
        persistance.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS).size() == 1
        persistance.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS)[0].feedUrl == feedUrlCorrectSubscription
    }
}
