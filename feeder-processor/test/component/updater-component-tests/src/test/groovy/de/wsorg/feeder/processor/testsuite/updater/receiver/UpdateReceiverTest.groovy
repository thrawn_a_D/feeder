package de.wsorg.feeder.processor.testsuite.updater.receiver

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.filebased.FeedFileBasedStorage
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.filebased.FileBasedCallDelegator
import de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport.FeederListenerContextFactory
import de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport.eventlistener.UpdateReceiverEventConstants
import de.wsorg.feeder.processor.updater.service.util.communication.filebased.FileBasedCommunicator
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.03.13
 */
class UpdateReceiverTest extends FeedUpdaterTestTemplate {
    @Shared
    FileBasedPersistence persistance
    @Shared
    String originalValueUsePollingForUpdating

    def setupSpec() {
        persistance = UtilsFactory.getFileBasedPersistence()

        originalValueUsePollingForUpdating = replacePropertyValueInConfigFile('/etc/feeder/update_receiver.properties',
                                                                              'updateFeedsUsingLocalPolling',
                                                                              'false')
    }

    def cleanupSpec() {
        replacePropertyValueInConfigFile('/etc/feeder/update_receiver.properties',
                                         'updateFeedsUsingLocalPolling',
                                         originalValueUsePollingForUpdating)
    }

    def "Test receiving of feed entries"() {
        given:
        def feedUrl = 'http://feedUrl'
        def description = 'kjhkjb'
        def title = 'title'
        def id = 'id'

        def feedModelAsJson = """{"feedEntriesCollection":{"additionalPagingPossible":false},
"feedEntries":[{
"title":"${title}",
"id":"${id}",
"updated":"Apr 9, 2013 9:29:44 AM",
"feedUrl":"${feedUrl}",
"description":"${description}"}],
"feedUrl":"http://feedUrl"}"""

        def wrappedModel = new StringJobMessage(feedModelAsJson)

        and:
        startServiceEndpoint(FeederListenerContextFactory, [UpdateReceiverEventConstants.SAVE_UPDATED_FEED])

        when:
        def feedModelAlreadyInStorage = new FeedModel(feedUrl: feedUrl)
        persistance.save(FileBasedCommunicator.FEED_UPDATE_RESPONSE_FOLDER, wrappedModel.getIdentifier(), wrappedModel)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, feedUrl, feedModelAlreadyInStorage)

        and:
        waitTillWantedTestingEventsOccurred()

        and:
        FeedModel editedFeedModel = persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS)[0]

        then:
        editedFeedModel.feedUrl == feedUrl
        editedFeedModel.feedEntries[0].feedUrl == feedUrl
        editedFeedModel.feedEntries[0].description == description
        editedFeedModel.feedEntries[0].id == id
        editedFeedModel.feedEntries[0].title == title
        editedFeedModel.feedEntries[0].updated
    }

    def "Test unknown feed to update provided, unsubscribe from update"() {
        given:
        def feedUrl = 'http://feedUrl'
        def description = 'kjhkjb'
        def title = 'title'
        def id = 'id'

        def feedModelAsJson = """{"feedEntriesCollection":{"additionalPagingPossible":false},
"feedEntries":[{
"title":"${title}",
"id":"${id}",
"updated":"Apr 9, 2013 9:29:44 AM",
"feedUrl":"${feedUrl}",
"description":"${description}"}],
"feedUrl":"http://feedUrl"}"""

        def wrappedModel = new StringJobMessage(feedModelAsJson)

        and:
        startServiceEndpoint(FeederListenerContextFactory, [UpdateReceiverEventConstants.UNSUBSCRIBE_FEED_FROM_SUPERFEEDR])

        when:
        persistance.save(FileBasedCommunicator.FEED_UPDATE_RESPONSE_FOLDER, wrappedModel.getIdentifier(), wrappedModel)

        and:
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER)[0].identifier == feedUrl
    }
}
