package de.wsorg.feeder.processor.testsuite.updater.manager

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.filebased.FeedFileBasedStorage
import de.wsorg.feeder.processor.storage.feed.filebased.FeedLoadStatisticsFileBasedAccess
import de.wsorg.feeder.processor.storage.feed.filebased.FeedToUserFileBasedAccess
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.manager.util.communicator.filebased.FileBasedCallDelegator
import de.wsorg.feeder.processor.updater.manager.util.testsupport.UpdateManagerCleanProcessMain
import de.wsorg.feeder.processor.updater.manager.util.testsupport.eventlistener.UpdateManagerEventConstants
import de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased.FileBasedFeedRegistrator
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.03.13
 */
class UpdateManagerJobListenerTest extends FeedUpdaterTestTemplate {
    @Shared
    FileBasedPersistence persistance

    def setupSpec() {
        persistance = UtilsFactory.fileBasedPersistence
    }

    def "Cancel a subscription when no other is using it and cleanup content"() {
        given:
        def cancelJobMessage = new UpdaterJobMessage(identifier: 'http://testfeed.de')
        def feed = new FeedModel(feedUrl: cancelJobMessage.identifier)
        def feedLoadStatistic = new FeedLoadStatistic(feedUrl: cancelJobMessage.identifier)

        and:
        startServiceEndpoint(UpdateManagerCleanProcessMain, [UpdateManagerEventConstants.CANCEL_SUBSCRIPTION])

        when:
        persistance.save(FileBasedFeedRegistrator.CANCEL_UPDATING_MESSAGE_FOLDER, cancelJobMessage.identifier, cancelJobMessage)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, feed.feedUrl, feed)
        persistance.save(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS, feedLoadStatistic.feedUrl, feedLoadStatistic)

        and:
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER)[0].identifier == cancelJobMessage.identifier
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS).size() == 0
        persistance.getFolderContent(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS).size() == 0
    }

    def "Cancel a subscription but someone else is still subscribed to it"() {
        given:
        def cancelJobMessage = new UpdaterJobMessage(identifier: 'http://testfeed.de')
        def feed = new FeedModel(feedUrl: cancelJobMessage.identifier)
        def feedLoadStatistic = new FeedLoadStatistic(feedUrl: cancelJobMessage.identifier)
        def feedAssociation = new FeedToUserAssociation(feedUrl: cancelJobMessage.identifier)

        and:
        startServiceEndpoint(UpdateManagerCleanProcessMain, [UpdateManagerEventConstants.CANCEL_SUBSCRIPTION])

        when:
        persistance.save(FileBasedFeedRegistrator.CANCEL_UPDATING_MESSAGE_FOLDER, cancelJobMessage.identifier, cancelJobMessage)
        persistance.save(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS, feed.feedUrl, feed)
        persistance.save(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS, feedLoadStatistic.feedUrl, feedLoadStatistic)
        persistance.save(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS, feedAssociation.feedUrl, feedAssociation)

        and:
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER).size() == 0
        persistance.getFolderContent(FeedFileBasedStorage.STORAGE_FOLDER_FOR_FEEDS).size() == 1
        persistance.getFolderContent(FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS).size() == 1
        persistance.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS).size() == 1
    }

    //TODO: Write a test on manager feed subscription  : de.wsorg.feeder.processor.updater.manager.executor.SubscribeToFeed
}
