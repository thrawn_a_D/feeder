package de.wsorg.feeder.processor.testsuite.updater.repository

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.repository.FeedRepositoryFactory

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.04.13
 */
class TestUserFeedRepository extends FeedUpdaterTestTemplate {
    def feedUrl1 = 'http://cre.fm/feed'
    def feedUrl2 = 'http://rss.golem.de/rss.php?feed=ATOM1.0'
    def userId = 'asdasd'

    def setup(){
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl1)
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl2)
    }

    def "Read feeds of a given user having no current update subscription"() {
        given:
        prepareDataOnStorage()

        when:
        def userFeeds = FeedRepositoryFactory.getUserFeedRepository().loadUserFeeds(userId)
        def loadStatistics1 = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl1)
        def feedInStorage1 = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl1)
        def loadStatistics2 = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl2)
        def feedInStorage2 = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl2)

        then:
        userFeeds.every{it.feedUrl == feedUrl1 || it.feedUrl == feedUrl2}
        loadStatistics1
        loadStatistics2
        feedInStorage1
        feedInStorage2
    }

    def "Read feeds of a given user HAVING current update subscription"() {
        given:
        prepareDataOnStorage()
        FeedStorageFactory.feedLoadStatisticsDataAccess.addNewFeedLoadStatistic(feedUrl1)
        FeedStorageFactory.feedLoadStatisticsDataAccess.addNewFeedLoadStatistic(feedUrl2)

        when:
        def userFeeds = FeedRepositoryFactory.getUserFeedRepository().loadUserFeeds(userId)
        def loadStatistics1 = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl1)
        def feedInStorage1 = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl1)
        def loadStatistics2 = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl2)
        def feedInStorage2 = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl2)

        then:
        userFeeds.every{it.feedUrl == feedUrl1 || it.feedUrl == feedUrl2}
        loadStatistics1
        loadStatistics2
        feedInStorage1
        feedInStorage2
    }

    def prepareDataOnStorage() {
        def feedModel = new FeedModel()
        feedModel.feedUrl = feedUrl1
        feedModel.title = 'test1'
        feedModel.description = 'testdesc'

        def feedModel2 = new FeedModel()
        feedModel2.feedUrl = feedUrl2
        feedModel2.title = 'test2'
        feedModel2.description = 'testdesc'

        def subscription1 = new FeedToUserAssociation()
        subscription1.feedUrl = feedUrl1
        subscription1.userId = userId
        subscription1.associationType = AssociationType.SUBSCRIBER

        def subscription2 = new FeedToUserAssociation()
        subscription2.feedUrl = feedUrl2
        subscription2.userId = userId
        subscription2.associationType = AssociationType.SUBSCRIBER

        FeedStorageFactory.getFeedDAO().save(feedModel)
        FeedStorageFactory.getFeedDAO().save(feedModel2)
        FeedStorageFactory.feedToUserAssociationDAO.save(subscription1)
        FeedStorageFactory.feedToUserAssociationDAO.save(subscription2)
    }
}
