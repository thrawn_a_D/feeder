package de.wsorg.feeder.processor.testsuite.updater.repository

import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.repository.FeedRepositoryFactory
import de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased.FileBasedFeedRegistrator
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.03.13
 */
class TestFeedRepository extends FeedUpdaterTestTemplate {
    def feedUrl = 'http://cre.fm/feed'

    def setup(){
        FeedStorageFactory.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl)
    }

    def "Load a feed using an url"() {
        when:
        def feed = FeedRepositoryFactory.getFeedRepository().loadFeed(feedUrl)
        def loadStatistics = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)
        def feedInStorage = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl)

        then:
        feed
        loadStatistics
        feedInStorage
    }

    def "Load a feed which has already been loaded and should be locally present"() {
        when:
        def feed = FeedRepositoryFactory.getFeedRepository().loadFeed(feedUrl)
        def loadStatistics = FeedStorageFactory.getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl)
        def feedInStorage = FeedStorageFactory.getFeedDAO().findByFeedUrl(feedUrl)
        def localFeed = FeedRepositoryFactory.getFeedRepository().loadFeed(feedUrl)

        then:
        feed
        loadStatistics
        feedInStorage
        localFeed
    }

    def "Feed subscription has been canceled"() {
        when:
        FeedRepositoryFactory.getFeedRepository().informThatUserCanceledSubscription(feedUrl)
        def canceledSubscriptionsFolderContent = getFolderContent(FileBasedFeedRegistrator.CANCEL_UPDATING_MESSAGE_FOLDER)

        then:
        canceledSubscriptionsFolderContent.size() == 1
        canceledSubscriptionsFolderContent[0].identifier == feedUrl
    }

    def getFolderContent(final String folderName) {
        FileBasedPersistence<UpdaterJobMessage> persistence = UtilsFactory.getFileBasedPersistence()
        persistence.getFolderContent(folderName)
    }
}
