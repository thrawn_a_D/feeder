package de.wsorg.feeder.processor.testsuite.updater.serviceendpoint

import com.google.gson.Gson
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.testsuite.updater.FeedUpdaterTestTemplate
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.manager.util.communicator.filebased.FileBasedCallDelegator
import de.wsorg.feeder.processor.updater.service.util.communication.filebased.FileBasedCommunicator
import de.wsorg.feeder.processor.updater.service.util.service.mock.SuperfeedrMock
import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.FileBasedIncomingFeedListener
import de.wsorg.feeder.processor.updater.service.util.testsupport.FeederUpdateServiceContextFactory
import de.wsorg.feeder.processor.updater.service.util.testsupport.eventlistener.ServiceEndPointEventConstants
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class ServiceEndpointTest extends FeedUpdaterTestTemplate {
    @Shared
    FileBasedPersistence persistance
    @Shared
    String originalValueUsePollingForUpdating
    @Shared
    String originalValueUseFileSystemAsStorage
    @Shared
    String originalSuperfeedrMocked

    def setupSpec() {
        persistance = UtilsFactory.fileBasedPersistence

        originalValueUsePollingForUpdating = replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                                                              'updateFeedsUsingLocalPolling',
                                                                              'false')

        originalValueUseFileSystemAsStorage = replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                                                                'useFileSystemAsStorage',
                                                                                'true')

        originalSuperfeedrMocked = replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                                                    'superfeedr.protocol',
                                                                    'mocked')
    }

    def cleanupSpec() {
        replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                         'updateFeedsUsingLocalPolling',
                                         originalValueUsePollingForUpdating)

        replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                         'useFileSystemAsStorage',
                                         originalValueUseFileSystemAsStorage)

        replacePropertyValueInConfigFile('/etc/feeder/update_service.properties',
                                            'superfeedr.protocol',
                                            originalSuperfeedrMocked)
    }

    def "Subscribe to a feed"() {
        given:
        def subscriptionMessage = new UpdaterJobMessage(identifier: 'http://testfeed.de', action: UpdateManagerAction.REGISTER_FEED_FOR_UPDATE)

        and:
        startServiceEndpoint(FeederUpdateServiceContextFactory, [ServiceEndPointEventConstants.SUBSCRIBE_FEED_TO_SUPERFEEDR])

        when:
        persistance.save(FileBasedCallDelegator.UPDATE_MESSAGE_FOLDER, subscriptionMessage.identifier, subscriptionMessage)
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_FOLDER)[0] == subscriptionMessage.identifier
    }

    def "Unsubscribe from feed"() {
        given:
        def unsubscribeMessage = new UpdaterJobMessage(identifier: 'http://testfeed.de', action: UpdateManagerAction.CANCEL_UPDATE_OF_FEED)

        and:
        startServiceEndpoint(FeederUpdateServiceContextFactory, [ServiceEndPointEventConstants.UNSUBSCRIBE_FEED_FROM_SUPERFEEDR])

        when:
        persistance.save(FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER, unsubscribeMessage.identifier, unsubscribeMessage)
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_CANCELED_FOLDER)[0] == unsubscribeMessage.identifier
    }

    def "Get all subscribed feeds"() {
        given:
        def getSubscribedFeeds = new UpdaterJobMessage(identifier: '123', action: UpdateManagerAction.GET_SUBSCRIBED_FEEDS)

        and:
        def feedUrl1 = 'http://asd.de'
        def feedUrl2 = 'http://lknlök.de'

        and:
        startServiceEndpoint(FeederUpdateServiceContextFactory, [ServiceEndPointEventConstants.SUBSCRIBED_FEED_LIST_PUBLISHED_TO_COMPONENTS])

        when:
        persistance.save(FileBasedCallDelegator.GET_ALL_SUBSCRIBED_FEEDS_FOLDER, getSubscribedFeeds.identifier, getSubscribedFeeds)
        persistance.save(SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_FOLDER, feedUrl1, feedUrl1)
        persistance.save(SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_FOLDER, feedUrl2, feedUrl2)
        waitTillWantedTestingEventsOccurred()

        then:
        persistance.getFolderContent(FileBasedCommunicator.GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER)[0]
        persistance.getFolderContent(FileBasedCommunicator.GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER)[0].content.contains(feedUrl1)
        persistance.getFolderContent(FileBasedCommunicator.GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER)[0].content.contains(feedUrl2)
    }

    def "Test receiving of feed entries"() {
        given:
        def feedUrl = 'http://myfeed.de/feed'
        def entryId = 'http://myfeed.de/feed#1'
        def entryLink = 'http://myfeed.de/feed#link'
        def entryLinkType = 'http/html'
        def entryPublished = new Date()
        def entryUpdated = new Date()
        def entryTitle = 'title'
        def entrySummary = 'summary'
        def entryContent = 'kjkuuk'

        def mapOfValues = ['FEED_URL':feedUrl,
                'ENTRY_ID':entryId,
                'ENTRY_LINK':entryLink,
                'ENTRY_LINK_TYPE':entryLinkType,
                'ENTRY_PUBLISHED':entryPublished,
                'ENTRY_UPDATED':entryUpdated,
                'ENTRY_TITLE':entryTitle,
                'ENTRY_SUMMARY':entrySummary,
                'ENTRY_CONTENT':entryContent]

        and:
        startServiceEndpoint(FeederUpdateServiceContextFactory, [ServiceEndPointEventConstants.SEND_UPDATED_FEED_TO_UPDATER_MODULES])

        when:
        persistance.save(FileBasedIncomingFeedListener.INCOMING_SUBSCRIPTIONS_FOLDER, feedUrl, mapOfValues)
        waitTillWantedTestingEventsOccurred()

        and:
        StringJobMessage feedUpdateModelAsJson = persistance.getFolderContent(FileBasedCommunicator.FEED_UPDATE_RESPONSE_FOLDER)[0]
        FeedModel feedModel =  new Gson().fromJson(feedUpdateModelAsJson.content, FeedModel)

        then:
        feedModel.feedUrl == feedUrl
        feedModel.feedEntries[0].feedUrl == feedUrl
        feedModel.feedEntries[0].id == entryId
        feedModel.feedEntries[0].updated.date == entryUpdated.date
        feedModel.feedEntries[0].title == entryTitle
        feedModel.feedEntries[0].description == entrySummary
    }
}
