package de.wsorg.feeder.processor.testsuite.updater

import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory
import de.wsorg.feeder.processor.storage.feed.filebased.FeedLoadStatisticsFileBasedAccess
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.testsuite.TestTemplate
import de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventCollector
import de.wsorg.feeder.processor.updater.manager.util.communicator.filebased.FileBasedCallDelegator
import de.wsorg.feeder.processor.updater.manager.util.testsupport.UpdateManagerCleanProcessMain
import de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport.FeederListenerContextFactory
import de.wsorg.feeder.processor.updater.repository.FeedRepositoryFactory
import de.wsorg.feeder.processor.updater.repository.utils.config.RepositoryConfig
import de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased.FileBasedFeedRegistrator
import de.wsorg.feeder.processor.updater.service.util.communication.filebased.FileBasedCommunicator
import de.wsorg.feeder.processor.updater.service.util.service.mock.SuperfeedrMock
import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.FileBasedIncomingFeedListener
import de.wsorg.feeder.processor.updater.service.util.testsupport.FeederUpdateServiceContextFactory
import de.wsorg.feeder.utils.persistence.JavaObjectPersistence
import spock.lang.Shared

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class FeedUpdaterTestTemplate extends TestTemplate implements de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventListener {

    @Shared
    StorageConfig storageConfig
    @Shared
    RepositoryConfig repositoryConfig

    boolean wantedTestingEventsOccurred

    def setup() {
        def loadStatisticsFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FeedLoadStatisticsFileBasedAccess.STORAGE_FOLDER_FOR_LOAD_STATISTICS)
        def cancelUpdatingFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedFeedRegistrator.CANCEL_UPDATING_MESSAGE_FOLDER)

        def externalServiceIncomingFeedFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedIncomingFeedListener.INCOMING_SUBSCRIPTIONS_FOLDER)

        def updateExternalSuperfeederSubscriptionsFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_FOLDER)
        def updateExternalSuperfeederSubscriptionCanceledFolder = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_CANCELED_FOLDER)
        def updateExternalSuperfeederRunningSubscriptions = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + SuperfeedrMock.UPDATER_FEED_SUBSCRIPTION_FOLDER)

        def updateServiceEndpointGetSubscribedFeeds = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedCallDelegator.GET_ALL_SUBSCRIBED_FEEDS_FOLDER)
        def updateServiceEndpointGetSubscribedFeedsResponse = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedCommunicator.GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER)
        def updateServiceEndpointSubscribeToFeed = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedCallDelegator.UPDATE_MESSAGE_FOLDER)
        def updateServiceEndpointCancelFeedSubscription = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedCallDelegator.CANCEL_UPDATING_MESSAGE_FOLDER)
        def updateServiceEndpointFeedUpdate = new File(JavaObjectPersistence.FEEDER_STORAGE_FOLDER + FileBasedCommunicator.FEED_UPDATE_RESPONSE_FOLDER)


        if(loadStatisticsFolder.exists())
            loadStatisticsFolder.deleteDir()
        if(cancelUpdatingFolder.exists())
            cancelUpdatingFolder.deleteDir()

        if(externalServiceIncomingFeedFolder.exists())
                externalServiceIncomingFeedFolder.deleteDir()

        if(updateExternalSuperfeederSubscriptionsFolder.exists())
                updateExternalSuperfeederSubscriptionsFolder.deleteDir()
        if(updateExternalSuperfeederSubscriptionCanceledFolder.exists())
                updateExternalSuperfeederSubscriptionCanceledFolder.deleteDir()
        if(updateExternalSuperfeederRunningSubscriptions.exists())
                updateExternalSuperfeederRunningSubscriptions.deleteDir()

        if(updateServiceEndpointGetSubscribedFeeds.exists())
                updateServiceEndpointGetSubscribedFeeds.deleteDir()
        if(updateServiceEndpointGetSubscribedFeedsResponse.exists())
                updateServiceEndpointGetSubscribedFeedsResponse.deleteDir()
        if(updateServiceEndpointSubscribeToFeed.exists())
                updateServiceEndpointSubscribeToFeed.deleteDir()
        if(updateServiceEndpointCancelFeedSubscription.exists())
                updateServiceEndpointCancelFeedSubscription.deleteDir()
        if(updateServiceEndpointFeedUpdate.exists())
                updateServiceEndpointFeedUpdate.deleteDir()

        wantedTestingEventsOccurred=false
    }

    def cleanup() {
        FeederUpdateServiceContextFactory?.applicationContext?.destroy()
        UpdateManagerCleanProcessMain?.applicationContext?.destroy()
        FeederListenerContextFactory?.applicationContext?.destroy()
    }

    def setupSpec() {
        storageConfig = new StorageConfig()
        repositoryConfig = new RepositoryConfig()

        CouchbaseConfig couchbaseConfig = new CouchbaseConfig()
        couchbaseConfig.addCouchbaseHost('localhost', '8091')
        couchbaseConfig.feedsBucketName = 'feederFeed'
        couchbaseConfig.feedAssociationBucketName = 'feederFeedAssoziation'
        couchbaseConfig.feederUserBucketName = 'feederUser'
        couchbaseConfig.loadStatisticsBucketName = 'feederLoadStatistics'
        couchbaseConfig.couchbasePassword = '123qweasd'

        ElasticSearchConfig elasticSearchConfig = new ElasticSearchConfig()
        elasticSearchConfig.addElasticSearchHost('127.0.0.1', '9300')
        elasticSearchConfig.setClusterName('elasticsearch')
        elasticSearchConfig.setFeedsIndexName('feeder-feed')
        elasticSearchConfig.setFeedsAssociationIndexName('feeder-feed-assoziation')
        elasticSearchConfig.setFeedsLoadStatisticsIndexName('feeder-feeds-load-statistics')

        storageConfig.setCouchbaseConfig(couchbaseConfig)
        storageConfig.setElasticSearchConfig(elasticSearchConfig)

        repositoryConfig.setActiveMQUrl('localhost')
        repositoryConfig.setActiveMQPort('61616')

        repositoryConfig.setStorageConfig(storageConfig)

        FeedRepositoryFactory.useFileSystemAsStorage = true

        FeedRepositoryFactory.setClientConfig(repositoryConfig)
        FeedStorageFactory.setClientConfig(storageConfig)
    }

    @Override
    void interestedEventsOccurred() {
        wantedTestingEventsOccurred = true
    }

    @Override
    void exceptionOccurred(final Exception exception) {
        throw exception
    }

    def startServiceEndpoint(def springContextFactory, ArrayList<String> expectedEventsToOccur) {
        springContextFactory.initContext()
        def eventCollector = springContextFactory.applicationContext.getBean(EventCollector)
        eventCollector.setEventListener(this, expectedEventsToOccur as Set)
    }

    def waitTillWantedTestingEventsOccurred() {
        def maxSleepTime=15000
        def currentSleepTime=0
        def sleepTime = 50

        while (!wantedTestingEventsOccurred) {
            sleep(sleepTime)
            currentSleepTime+=sleepTime
            if(currentSleepTime >= maxSleepTime) {
                throw new RuntimeException("Events did not occurr within 15 Seconds.")
            }
        }
    }
}
