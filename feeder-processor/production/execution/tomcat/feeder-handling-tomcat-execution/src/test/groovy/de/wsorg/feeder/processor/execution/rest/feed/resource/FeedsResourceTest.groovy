package de.wsorg.feeder.processor.execution.rest.feed.resource

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper
import spock.lang.Specification

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.01.13
 */
class FeedsResourceTest extends Specification {
    FeedsResource feedsResource
    FeederRequestResponseHandler feederRequestResponseHandler
    ResponseWrapper responseWrapper

    def setup(){
        feedsResource = new FeedsResource()
        feederRequestResponseHandler = Mock(FeederRequestResponseHandler)
        responseWrapper = Mock(ResponseWrapper)
        feedsResource.feederRequestResponseHandler = feederRequestResponseHandler
        feedsResource.responseWrapper = responseWrapper
    }


    def "Process a common search request"() {
        given:
        def searchTerm = 'test%20message'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.searchFeeds(searchTerm)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.GLOBAL_SEARCH, {it == "searchTerm@${searchTerm};"}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }



    def "Process a search for a user request"() {
        given:
        def searchTerm = 'test%20message'
        def userId = '09j098j98j'
        def userName = 'bla'
        def password = 'passwd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.searchFeedsForUser(userName, password, userId, searchTerm)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA, {it.contains("searchTerm@${searchTerm};") &&
                                                                                                              it.contains("userId@${userId};") &&
                                                                                                              it.contains("userName@${userName};") &&
                                                                                                              it.contains("password@${password};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }
}
