package de.wsorg.feeder.processor.execution.rest.feed.resource

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper
import spock.lang.Specification

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.01.13
 */
class FeedResourceTest extends Specification {
    FeedResource loadFeedResource
    FeederRequestResponseHandler feederRequestResponseHandler
    ResponseWrapper responseWrapper

    def setup(){
        loadFeedResource = new FeedResource()
        feederRequestResponseHandler = Mock(FeederRequestResponseHandler)
        responseWrapper = Mock(ResponseWrapper)
        loadFeedResource.feederRequestResponseHandler = feederRequestResponseHandler
        loadFeedResource.responseWrapper = responseWrapper
    }

    def "Process a common load request"() {
        given:
        def feedUrl = 'http://asdlkn'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        loadFeedResource.loadFeed(feedUrl, null, null)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOAD, {it == "feedUrl@${feedUrl};"}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Process a common load request using page raging"() {
        given:
        def feedUrl = 'http://asdlkn'
        def pageRangeStartIndex = '1'
        def pageRangeEndIndex = '10'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        loadFeedResource.loadFeed(feedUrl, pageRangeStartIndex, pageRangeEndIndex)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOAD, {String it -> it.contains("feedUrl@${feedUrl};") &&
                                                                                               it.contains("pageRangeStartIndex@${pageRangeStartIndex};") &&
                                                                                               it.contains("pageRangeEndIndex@${pageRangeEndIndex};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Process a load request related to a user using page raging"() {
        given:
        def feedUrl = 'http://asdlkn'
        def userId='98z87'
        def userName='name'
        def password='asdasd'
        def pageRangeStartIndex = '1'
        def pageRangeEndIndex = '10'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        loadFeedResource.loadFeedForUser(userName,
                                           password,
                                           userId,
                                           feedUrl,
                                           pageRangeStartIndex,
                                           pageRangeEndIndex,
                                           null)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOAD_USER_RELATED,
                                                            {it.contains("feedUrl@${feedUrl};") &&
                                                             it.contains("userId@${userId};") &&
                                                             it.contains("userName@${userName};") &&
                                                             it.contains("password@${password};") &&
                                                             it.contains("pageRangeStartIndex@${pageRangeStartIndex};") &&
                                                             it.contains("pageRangeEndIndex@${pageRangeEndIndex};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Process a load request related to a user"() {
        given:
        def feedUrl = 'http://asdlkn'
        def userId='98z87'
        def userName='name'
        def password='asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        loadFeedResource.loadFeedForUser(userName, password, userId, feedUrl, null, null, null)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOAD_USER_RELATED,
                {it.contains("feedUrl@${feedUrl};") &&
                        it.contains("userId@${userId};") &&
                        it.contains("userName@${userName};") &&
                        it.contains("password@${password};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Process a load request related to a user and show only unread entries"() {
        given:
        def feedUrl = 'http://asdlkn'
        def userId='98z87'
        def userName='name'
        def password='asdasd'
        def showOnlyUnreadEntries = 'true'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        loadFeedResource.loadFeedForUser(userName, password, userId, feedUrl, null, null, showOnlyUnreadEntries)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOAD_USER_RELATED,
                {it.contains("showOnlyUnreadEntries@${showOnlyUnreadEntries};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }
}
