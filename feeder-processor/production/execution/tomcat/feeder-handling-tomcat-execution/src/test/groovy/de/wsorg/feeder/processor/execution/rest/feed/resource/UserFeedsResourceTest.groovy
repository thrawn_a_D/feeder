package de.wsorg.feeder.processor.execution.rest.feed.resource

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper
import spock.lang.Specification

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 29.01.13
 */
class UserFeedsResourceTest extends Specification {
    UserFeedsResource feedsResource
    FeederRequestResponseHandler feederRequestResponseHandler
    ResponseWrapper responseWrapper

    def setup(){
        feedsResource = new UserFeedsResource()
        feederRequestResponseHandler = Mock(FeederRequestResponseHandler)
        responseWrapper = Mock(ResponseWrapper)
        feedsResource.feederRequestResponseHandler = feederRequestResponseHandler
        feedsResource.responseWrapper = responseWrapper
    }

    def "Get user subscribed feeds"() {
        given:
        def searchTerm = 'test%20message'
        def showUnreadEntries = 'true'
        def userId = '09j098j98j'
        def userName = 'bla'
        def password = 'passwd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.searchUserSubscribedFeeds(userName,
                password,
                userId,
                searchTerm,
                showUnreadEntries)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.USER_RELATED_SEARCH,
                {it.contains("searchTerm@${searchTerm};") &&
                        it.contains("showUnreadEntries@${showUnreadEntries};") &&
                        it.contains("userId@${userId};") &&
                        it.contains("userName@${userName};") &&
                        it.contains("password@${password};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Provide null for searchterm and restrictions"() {
        given:
        def searchTerm = null
        def showUnreadEntries = null
        def userId = '09j098j98j'
        def userName = 'bla'
        def password = 'passwd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.searchUserSubscribedFeeds(userName,
                password,
                userId,
                searchTerm,
                showUnreadEntries)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.USER_RELATED_SEARCH,
                {!it.contains("searchTerm") &&
                        !it.contains("showUnreadEntries") &&
                        it.contains("userId@${userId};") &&
                        it.contains("userName@${userName};") &&
                        it.contains("password@${password};")}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Subscribe to a feed"() {
        given:
        def xmlRequest = 'asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.subscribeToFeed(xmlRequest)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.FAVOUR_FEED, xmlRequest) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Mark feed as read"() {
        given:
        def xmlRequest = 'asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.markReadStatusOfFeed(xmlRequest)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.MARK_FEED_ENTRY_READABILITY, xmlRequest) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Mark all feeds as read"() {
        given:
        def xmlRequest = 'asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        feedsResource.markReadStatusOfAllFeeds(xmlRequest)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.MARK_ALL_FEEDS_READABILITY, xmlRequest) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }
}
