package de.wsorg.feeder.processor.execution.rest.feed.resource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.ExecutionRequestParameterMap;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Path("/feeds/{userId}/subscriptions/")
public class UserFeedsResource {
    @Inject
    private FeederRequestResponseHandler feederRequestResponseHandler;
    @Inject
    private ResponseWrapper responseWrapper;


    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response searchUserSubscribedFeeds(@HeaderParam("userName") String userName,
                                              @HeaderParam("password") String password,
                                              @PathParam("userId") String userId,
                                              @QueryParam("searchTerm") String searchTerm,
                                              @QueryParam("showUnreadEntries") String showUnreadEntries) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();


        addOptionalKeyValue(feederParameterMap, "searchTerm", searchTerm);
        addOptionalKeyValue(feederParameterMap, "showUnreadEntries", showUnreadEntries);
        feederParameterMap.put("userId", userId);
        feederParameterMap.put("userName", userName);
        feederParameterMap.put("password", password);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.USER_RELATED_SEARCH,
                feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    public Response subscribeToFeed(final String subscriptionRequestXml) {

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.FAVOUR_FEED,
                subscriptionRequestXml);

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    @POST
    @Path("/read")
    @Produces(MediaType.APPLICATION_XML)
    public Response markReadStatusOfFeed(final String subscriptionRequestXml) {

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.MARK_FEED_ENTRY_READABILITY,
                                                                            subscriptionRequestXml);

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    @POST
    @Path("/read/all")
    @Produces(MediaType.APPLICATION_XML)
    public Response markReadStatusOfAllFeeds(final String subscriptionRequestXml) {

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.MARK_ALL_FEEDS_READABILITY,
                subscriptionRequestXml);

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    private void addOptionalKeyValue(final ExecutionRequestParameterMap feederParameterMap, final String key, final String value) {
        if (StringUtils.isNotBlank(value)) {
            feederParameterMap.put(key, value);
        }
    }
}
