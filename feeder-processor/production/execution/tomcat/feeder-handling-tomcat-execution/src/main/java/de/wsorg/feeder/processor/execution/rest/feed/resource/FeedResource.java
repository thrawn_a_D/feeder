package de.wsorg.feeder.processor.execution.rest.feed.resource;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.ExecutionRequestParameterMap;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Path("/feed/")
public class FeedResource {
    @Inject
    private FeederRequestResponseHandler feederRequestResponseHandler;
    @Inject
    private ResponseWrapper responseWrapper;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response loadFeed(@QueryParam("feedUrl") String feedUrl,
                              @QueryParam("pageRangeStartIndex") String pageRangeStartIndex,
                              @QueryParam("pageRangeEndIndex") String pageRangeEndIndex) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();
        feederParameterMap.put("feedUrl", feedUrl);

        addPagingParams(pageRangeStartIndex, pageRangeEndIndex, feederParameterMap);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.LOAD,
                                                                                  feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_XML)
    public Response loadFeedForUser(@HeaderParam("userName") String userName,
                                    @HeaderParam("password") String password,
                                    @PathParam("userId") String userId,
                                    @QueryParam("feedUrl") String feedUrl,
                                    @QueryParam("pageRangeStartIndex") String pageRangeStartIndex,
                                    @QueryParam("pageRangeEndIndex") String pageRangeEndIndex,
                                    @QueryParam("showOnlyUnreadEntries") String showOnlyUnreadEntries) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();
        feederParameterMap.put("feedUrl", feedUrl);
        feederParameterMap.put("userId", userId);
        feederParameterMap.put("userName", userName);
        feederParameterMap.put("password", password);

        addPagingParams(pageRangeStartIndex, pageRangeEndIndex, feederParameterMap);
        addShowOnlyUnreadFeedsParam(showOnlyUnreadEntries, feederParameterMap);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.LOAD_USER_RELATED,
                                                                                 feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    private void addShowOnlyUnreadFeedsParam(final String showOnlyUnreadEntries, final ExecutionRequestParameterMap feederParameterMap) {
        if(StringUtils.isNotBlank(showOnlyUnreadEntries)) {
            feederParameterMap.put("showOnlyUnreadEntries", showOnlyUnreadEntries);
        }
    }

    private void addPagingParams(final String pageRangeStartIndex, final String pageRangeEndIndex, final ExecutionRequestParameterMap feederParameterMap) {
        if(StringUtils.isNotBlank(pageRangeStartIndex))
            feederParameterMap.put("pageRangeStartIndex", pageRangeStartIndex);
        if(StringUtils.isNotBlank(pageRangeEndIndex))
            feederParameterMap.put("pageRangeEndIndex", pageRangeEndIndex);
    }
}
