package de.wsorg.feeder.processor.execution.rest.feed.resource;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import de.wsorg.feeder.processor.execution.rest.feed.resource.wrapper.ResponseWrapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.ExecutionRequestParameterMap;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Path("/feeds/")
public class FeedsResource {
    @Inject
    private FeederRequestResponseHandler feederRequestResponseHandler;
    @Inject
    private ResponseWrapper responseWrapper;

    @GET
    @Produces(MediaType.APPLICATION_XML)
    public Response searchFeeds(@QueryParam("searchTerm") String searchTerm) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();
        feederParameterMap.put("searchTerm", searchTerm);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.GLOBAL_SEARCH,
                                                                            feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }


    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_XML)
    public Response searchFeedsForUser(@HeaderParam("userName") String userName,
                                       @HeaderParam("password") String password,
                                       @PathParam("userId") String userId,
                                       @QueryParam("searchTerm") String searchTerm) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();
        feederParameterMap.put("searchTerm", searchTerm);
        feederParameterMap.put("userId", userId);
        feederParameterMap.put("userName", userName);
        feederParameterMap.put("password", password);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA,
                                                                            feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }
}
