package de.wsorg.feeder.processor.execution.rest.user.resource

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import de.wsorg.feeder.processor.execution.rest.user.resource.wrapper.ResponseWrapper
import spock.lang.Specification

import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

/**
 * @author wschneider
 * Date: 31.01.13
 * Time: 14:25
 */
class UserResourceTest extends Specification {
    UserResource userResource
    FeederRequestResponseHandler feederRequestResponseHandler
    ResponseWrapper responseWrapper

    def setup(){
        userResource = new UserResource()
        feederRequestResponseHandler = Mock(FeederRequestResponseHandler)
        responseWrapper = Mock(ResponseWrapper)
        userResource.feederRequestResponseHandler = feederRequestResponseHandler
        userResource.responseWrapper = responseWrapper
    }

    def "Process a common load request"() {
        given:
        def nickName='user'
        def password='passwd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        userResource.verifyUserLogin(nickName, password)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.LOGIN_USER, {it == "nickName@${nickName};password@${password};"}) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Create a new user"() {
        given:
        def xmlRequest = 'asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        userResource.createUser(xmlRequest)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.CREATE_USER, xmlRequest) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }

    def "Handle a new user"() {
        given:
        def xmlRequest = 'asdasd'

        and:
        def processingResult = '98z9h980h'
        def okResponseBuilder = Mock(Response.ResponseBuilder)
        def typeResponseBuilder = Mock(Response.ResponseBuilder)

        when:
        userResource.handleUser(xmlRequest)

        then:
        1 * feederRequestResponseHandler.processRequest(FeederCommand.HANDLE_USER, xmlRequest) >> processingResult
        1 * responseWrapper.ok(processingResult) >> okResponseBuilder
        1 * okResponseBuilder.type(MediaType.TEXT_XML) >> typeResponseBuilder
        1 * typeResponseBuilder.build()
    }
}
