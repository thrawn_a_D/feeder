package de.wsorg.feeder.processor.execution.rest.user.resource;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import de.wsorg.feeder.processor.execution.rest.user.resource.wrapper.ResponseWrapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.ExecutionRequestParameterMap;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Path("/user/")
public class UserResource {
    @Inject
    private FeederRequestResponseHandler feederRequestResponseHandler;
    @Inject
    private ResponseWrapper responseWrapper;

    @GET
    @Path("/{nickName}")
    @Produces(MediaType.APPLICATION_XML)
    public Response verifyUserLogin(@PathParam("nickName") String nickName,
                                     @QueryParam("password") String password) {
        final ExecutionRequestParameterMap feederParameterMap = new ExecutionRequestParameterMap();
        feederParameterMap.put("nickName", nickName);
        feederParameterMap.put("password", password);

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.LOGIN_USER,
                                                                            feederParameterMap.toFeederMapString());

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }



    @POST
    @Produces(MediaType.APPLICATION_XML)
    public Response createUser(final String userCreationRequestXml) {

        final String response = feederRequestResponseHandler.processRequest(FeederCommand.CREATE_USER,
                                                                            userCreationRequestXml);

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }

    @PUT
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_XML)
    public Response handleUser(final String userHandleRequestXml) {
        final String response = feederRequestResponseHandler.processRequest(FeederCommand.HANDLE_USER,
                                                                            userHandleRequestXml);

        return responseWrapper.ok(response).type(MediaType.TEXT_XML).build();
    }
}
