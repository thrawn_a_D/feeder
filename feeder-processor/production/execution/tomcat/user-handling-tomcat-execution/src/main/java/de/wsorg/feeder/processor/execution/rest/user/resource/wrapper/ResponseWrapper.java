package de.wsorg.feeder.processor.execution.rest.user.resource.wrapper;

import javax.inject.Named;
import javax.ws.rs.core.Response;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ResponseWrapper {
    public Response.ResponseBuilder ok(final Object response) {
        return Response.ok(response);
    }
}
