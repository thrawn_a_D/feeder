package de.wsorg.feeder.processor.execution.user.request.executor

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.usecases.user.verify.UserVerifier
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.09.12
 */
class LoginUserExecutorTest extends Specification {
    LoginUserExecutor loginUserExecutor
    UserVerifier userVerifier

    def setup(){
        loginUserExecutor = new LoginUserExecutor()
        userVerifier = Mock(UserVerifier)
        loginUserExecutor.userVerifier = userVerifier
    }

    def "Show that the right request model mapper is provided"() {
        given:
        RequestModelMapper requestModelMapper = Mock(MapBasedRequestModelMapper)
        loginUserExecutor.requestModelMapper = requestModelMapper

        when:
        def usedMapper = loginUserExecutor.requestModelMapper

        then:
        usedMapper == requestModelMapper
    }

    def "Execute request using verifier and retrieve the result"(){
        given:
        UserLoginRequest userLoginRequest = new UserLoginRequest(nickName: 'nick', password: 'pass')

        and:
        FeederClientSideUser processingUser = new FeederClientSideUser(userName: 'nick',
                                                                       password: 'pass',
                                                                       firstName: 'first',
                                                                       lastName: 'last',
                                                                       EMail: 'mail',
                                                                       userId: '123')

        when:
        def result = loginUserExecutor.executeConcreteRequest(userLoginRequest)
        
        then:
        result.userName == processingUser.userName
        result.password == processingUser.password
        result.firstName == processingUser.firstName
        result.lastName == processingUser.lastName
        result.userId == processingUser.userId
        result.EMail == processingUser.EMail

        1 * userVerifier.loginUser(userLoginRequest.nickName, userLoginRequest.password) >> processingUser
    }

    def "Execute request using verifier but no user found"(){
        given:
        UserLoginRequest userLoginRequest = new UserLoginRequest(nickName: 'nick', password: 'pass')

        when:
        def result = loginUserExecutor.executeConcreteRequest(userLoginRequest)

        then:
        result == null

    }
}