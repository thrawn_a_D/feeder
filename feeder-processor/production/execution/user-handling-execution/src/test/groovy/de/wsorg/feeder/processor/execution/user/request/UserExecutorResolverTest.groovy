package de.wsorg.feeder.processor.execution.user.request

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutor
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.09.12
 */
class UserExecutorResolverTest extends Specification {
    UserExecutorResolver userExecutorResolver

    def setup(){
        userExecutorResolver = new UserExecutorResolver()

    }

    def "Resolve the right executor for user creation"() {
        given:
        XmlBasedRequestExecutor executor = Mock(XmlBasedRequestExecutor)
        userExecutorResolver.userCreationExecutor = executor;

        and:
        def FeederCommand command = FeederCommand.CREATE_USER

        when:
        def result = userExecutorResolver.resolveExecutor(command)

        then:
        result == executor
    }

    def "Resolve the right executor for user handling"() {
        given:
        XmlBasedRequestExecutor executor = Mock(XmlBasedRequestExecutor)
        userExecutorResolver.userHandleExecutor = executor;

        and:
        def FeederCommand command = FeederCommand.HANDLE_USER

        when:
        def result = userExecutorResolver.resolveExecutor(command)

        then:
        result == executor
    }

    def "Get an executor for user login validation"() {
        given:
        def loginCommand = FeederCommand.LOGIN_USER

        and:
        XmlBasedRequestExecutor loginRequest = Mock(XmlBasedRequestExecutor)
        userExecutorResolver.userLoginExecutor = loginRequest

        when:
        def result = userExecutorResolver.resolveExecutor(loginCommand)

        then:
        loginRequest == result
    }

    def "Provide an invalid command and receive null"() {
        when:
        def result = userExecutorResolver.resolveExecutor(FeederCommand.FAVOUR_FEED)

        then:
        result == null;
    }
}
