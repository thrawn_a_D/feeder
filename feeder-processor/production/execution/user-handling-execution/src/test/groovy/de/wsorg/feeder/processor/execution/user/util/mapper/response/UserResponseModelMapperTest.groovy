package de.wsorg.feeder.processor.execution.user.util.mapper.response

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.FeederUserType
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.09.12
 */
class UserResponseModelMapperTest extends Specification {
    UserResponseModelMapper userResponseModelMapper
    JaxbMarshaller jaxbMarshaller

    def setup(){
        userResponseModelMapper = new UserResponseModelMapper()
        jaxbMarshaller = Mock(JaxbMarshaller)
        userResponseModelMapper.jaxbMarshaller = jaxbMarshaller
    }

    def "Provide a user model to map and receive a valid xml"() {
        given:
        FeederClientSideUser userToMap = new FeederClientSideUser(firstName: 'firstName',
                                                                  lastName: 'lasta',
                                                                  EMail: 'eMail',
                                                                  userName: 'nickName',
                                                                  userId: 'id',
                                                                  password: 'passwd')

        and:
        def expectedResult = ''

        when:
        def result = userResponseModelMapper.getResponseModel(userToMap)

        then:
        result == expectedResult
        1*jaxbMarshaller.marshal({validateUser(it, userToMap)}) >> expectedResult
    }

    private boolean validateUser(FeederUserType it, FeederUseCaseUser userToMap) {
        it instanceof FeederUserType &&
        it.feederUser.firstName == userToMap.firstName &&
        it.feederUser.lastName == userToMap.lastName &&
        it.feederUser.EMail == userToMap.EMail &&
        it.feederUser.password == userToMap.password &&
        it.feederUser.userName == userToMap.userName &&
        it.feederUser.userId == userToMap.userId
    }
}
