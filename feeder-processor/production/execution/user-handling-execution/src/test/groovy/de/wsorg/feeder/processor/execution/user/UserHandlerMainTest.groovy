package de.wsorg.feeder.processor.execution.user

import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.09.12
 */
class UserHandlerMainTest extends Specification {
    UserHandlerMain userHandlerMain
    FeederRequestResponseHandler feederExecutor;

    def setup(){
        userHandlerMain = new UserHandlerMain()
        feederExecutor = Mock(FeederRequestResponseHandler)
        userHandlerMain.feederRequestResponseHandler = feederExecutor
    }

    def "Execute Main"() {
        given:
        UserHandlerMain userHandlerMainMock = Mock(UserHandlerMain)
        UserHandlerMain.userHandlerMain = userHandlerMainMock

        and:
        def givenArgs = ""

        when:
        UserHandlerMain.main(givenArgs);

        then:
        1 * userHandlerMainMock.processRequest(givenArgs)
    }

    def "Check if right request executor"() {
        when:
        def result = userHandlerMain.getFeederRequestResponseHandler()

        then:
        result != null
    }
}
