package de.wsorg.feeder.processor.execution.user.request.executor

import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.09.12
 */
class HandleUserExecutorTest extends Specification {
    HandleUserExecutor createUserExecutor
    FeedUserHandler userHandlerMock

    def setup(){
        userHandlerMock = Mock(FeedUserHandler)
        createUserExecutor = new HandleUserExecutor()
        createUserExecutor.userHandler = userHandlerMock
    }

    def "Receive a relevant RequestModelMapper"() {
        given:
        def requestModelMapperMock = Mock(RequestModelMapper)
        createUserExecutor.requestModelMapper = requestModelMapperMock

        when:
        def result = createUserExecutor.getRequestModelMapper()

        then:
        result == requestModelMapperMock
    }

    def "Update a user"() {
        given:
        def request = new HandleUserRequest()
        request.firstName = 'someFirstName'
        request.lastName = 'someLastName'
        request.EMail = 'eMail'
        request.password = 'password'
        request.userName = 'nickName'
        request.handlingType = UserHandlingType.UPDATE

        when:
        def result = createUserExecutor.executeConcreteRequest(request);

        then:
        result.processingResult == ProcessingResult.SUCCESS
        1 * userHandlerMock.updateUser({FeederUseCaseUser user -> validateUser(user, request)})
    }

    def "Make sure the right jaxb package name is provided"() {
        given:
        def packageName = 'de.wsorg.feeder.processor.execution.domain.generated.schema.request.user'

        when:
        def usedJaxbPackageName = createUserExecutor.jaxbGeneratedPackageName

        then:
        usedJaxbPackageName == packageName
    }

    private boolean validateUser(FeederUseCaseUser user, HandleUserRequest request) {
        user.firstName == request.firstName &&
        user.lastName == request.lastName &&
        user.EMail == request.EMail &&
        user.userName == request.userName &&
        user.password == request.password
    }
}
