package de.wsorg.feeder.processor.execution.user.response.processor

import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.09.12
 */
class UserResponseProcessorTest extends Specification {
    UserResponseProcessor userResponseProcessor

    def setup(){
        userResponseProcessor = new UserResponseProcessor()
    }

    def "Provide a response model mapper"() {
        given:
        ResponseModelMapper responseModelMapper = Mock(ResponseModelMapper)
        userResponseProcessor.responseModelMapper = responseModelMapper

        when:
        def usedMapper = userResponseProcessor.responseModelMapper

        then:
        usedMapper == responseModelMapper
    }

    def "Validate input object is indeed of type feed processing user"() {
        given:
        def someObject = ''

        when:
        userResponseProcessor.validateInputObjectType(someObject)

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message == "The provided response object is not of type FeederUseCaseUser"
    }
}
