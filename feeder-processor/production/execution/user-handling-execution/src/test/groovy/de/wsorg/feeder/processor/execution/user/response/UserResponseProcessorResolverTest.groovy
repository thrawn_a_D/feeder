package de.wsorg.feeder.processor.execution.user.response

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.09.12
 */
class UserResponseProcessorResolverTest extends Specification {
    UserResponseProcessorResolver userResponseProcessorResolver
    ResponseProcessorResolver genericResponseResolver
    ResponseProcessor userSpecificResponseProcessor

    def setup(){
        userResponseProcessorResolver = new UserResponseProcessorResolver()
        genericResponseResolver = Mock(ResponseProcessorResolver)
        userResponseProcessorResolver.genericResponseResolver = genericResponseResolver
        userSpecificResponseProcessor = Mock(ResponseProcessor)
        userResponseProcessorResolver.userResponseProcessor = userSpecificResponseProcessor
    }

    def "Resolve response using also a generic resolver"() {
        given:
        def someResponseToResolver=""

        and:
        def expectedResponseProcessor = Mock(ResponseProcessor)

        when:
        def genericResolverResult = userResponseProcessorResolver.resolveResponseProcessor(someResponseToResolver)

        then:
        genericResolverResult == expectedResponseProcessor
        1 * genericResponseResolver.resolveResponseProcessor(someResponseToResolver) >> expectedResponseProcessor
    }

    def "Resolve a user response processor by given feeder user"() {
        given:
        FeederClientSideUser feederUser = new FeederClientSideUser()

        and:
        genericResponseResolver.resolveResponseProcessor(feederUser) >> null

        when:
        def responseProcessor = userResponseProcessorResolver.resolveResponseProcessor(feederUser)

        then:
        responseProcessor == userSpecificResponseProcessor
    }

    def "Throw an exception if no processor could be found"() {
        given:
        def someResponseToResolver=""

        and:
        genericResponseResolver.resolveResponseProcessor(someResponseToResolver) >> null

        when:
        userResponseProcessorResolver.resolveResponseProcessor(someResponseToResolver)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "No valid response processor could be found for " + someResponseToResolver
    }
}
