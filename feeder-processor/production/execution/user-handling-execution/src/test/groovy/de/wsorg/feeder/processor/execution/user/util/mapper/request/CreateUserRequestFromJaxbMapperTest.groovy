package de.wsorg.feeder.processor.execution.user.util.mapper.request

import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.AbstractUserType
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.CreateUserRequestType
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.06.13
 */
class CreateUserRequestFromJaxbMapperTest extends Specification {
    CreateUserRequestFromJaxbMapper createUserRequestFromJaxbMapper

    def setup(){
        createUserRequestFromJaxbMapper = new CreateUserRequestFromJaxbMapper()
    }

    def "Map from jaxb model to feeder model"() {
        given:
        def requestModel = new CreateUserRequestType()
        requestModel.user = new AbstractUserType(firstName: 'blaFirst',
                lastName: 'blaLast',
                eMail: 'blaMail',
                userName: 'blaNick',
                password: 'blaPass',
                userId: 'blaId')

        when:
        def result = createUserRequestFromJaxbMapper.getRequestModel(requestModel)

        then:
        result.userToBeHandled.firstName == requestModel.user.firstName
        result.userToBeHandled.lastName == requestModel.user.lastName
        result.userToBeHandled.userName == requestModel.user.userName
        result.userToBeHandled.EMail == requestModel.user.EMail
        result.userToBeHandled.password == requestModel.user.password
        result.userToBeHandled.userId == requestModel.user.userId
    }
}
