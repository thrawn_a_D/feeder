package de.wsorg.feeder.processor.execution.user

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.01.13
 */
class UserHandlingExecutionFactoryTest extends Specification {
    def "Get a main executor"() {
        when:
        def result = UserHandlingExecutionFactory.getUserRequestResponseHandler()

        then:
        result != null
    }
}
