package de.wsorg.feeder.processor.execution.user.util.mapper.request

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.09.12
 */
class UserLoginRequestFromMapMapperTest extends Specification {
    UserLoginRequestFromMapMapper userLoginRequestFromJaxbMapper

    def setup(){
        userLoginRequestFromJaxbMapper = new UserLoginRequestFromMapMapper()
    }

    def "Map user login map model to feeder model"() {
        given:
        Map<String, String> loginUserRequestType = new HashMap<String, String>()
        loginUserRequestType.nickName = 'nick'
        loginUserRequestType.password = 'pass'

        when:
        def result = userLoginRequestFromJaxbMapper.getRequestModel(loginUserRequestType)

        then:
        result.nickName == loginUserRequestType.nickName
        result.password == loginUserRequestType.password
    }
}
