package de.wsorg.feeder.processor.execution.user.request.executor

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.06.13
 */
class CreateUserExecutorTest extends Specification {
    CreateUserExecutor createUserExecutor
    FeedUserHandler userHandlerMock

    def setup(){
        userHandlerMock = Mock(FeedUserHandler)
        createUserExecutor = new CreateUserExecutor()
        createUserExecutor.userHandler = userHandlerMock
    }

    def "Receive a relevant RequestModelMapper"() {
        given:
        def requestModelMapperMock = Mock(RequestModelMapper)
        createUserExecutor.requestModelMapper = requestModelMapperMock

        when:
        def result = createUserExecutor.getRequestModelMapper()

        then:
        result == requestModelMapperMock
    }

    def "Create a user"() {
        given:
        def request = new CreateUserRequest()
        request.userToBeHandled = new FeederClientSideUser()
        request.userToBeHandled.firstName = 'someFirstName'
        request.userToBeHandled.lastName = 'someLastName'
        request.userToBeHandled.EMail = 'eMail'
        request.userToBeHandled.password = 'password'
        request.userToBeHandled.userName = 'nickName'

        when:
        def result = createUserExecutor.executeConcreteRequest(request);

        then:
        result.processingResult == ProcessingResult.SUCCESS
        1 * userHandlerMock.createUser({FeederUseCaseUser user -> validateUser(user, request)})
    }

    def "Make sure the right jaxb package name is provided"() {
        given:
        def packageName = 'de.wsorg.feeder.processor.execution.domain.generated.schema.request.user'

        when:
        def usedJaxbPackageName = createUserExecutor.jaxbGeneratedPackageName

        then:
        usedJaxbPackageName == packageName
    }

    private boolean validateUser(FeederUseCaseUser user, CreateUserRequest request) {
        user.firstName == request.userToBeHandled.firstName &&
                user.lastName == request.userToBeHandled.lastName &&
                user.EMail == request.userToBeHandled.EMail &&
                user.userName == request.userToBeHandled.userName &&
                user.password == request.userToBeHandled.password
    }
}
