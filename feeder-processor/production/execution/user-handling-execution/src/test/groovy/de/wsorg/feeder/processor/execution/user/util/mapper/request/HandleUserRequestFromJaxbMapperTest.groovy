package de.wsorg.feeder.processor.execution.user.util.mapper.request

import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.AbstractUserType
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.HandleUserRequestType
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.UserHandleType
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.09.12
 */
class HandleUserRequestFromJaxbMapperTest extends Specification {
    HandleUserRequestFromJaxbMapper handleUserRequestFromJaxbMapper

    def setup(){
        handleUserRequestFromJaxbMapper = new HandleUserRequestFromJaxbMapper()
    }

    def "Map from jaxb model to feeder model"() {
        given:
        def requestModel = new HandleUserRequestType()
        requestModel.requestType = UserHandleType.UPDATE
        requestModel.user = new AbstractUserType(firstName: 'blaFirst',
                                                 lastName: 'blaLast',
                                                 eMail: 'blaMail',
                                                 userName: 'blaNick',
                                                 password: 'blaPass',
                                                 userId: 'blaId')

        when:
        def result = handleUserRequestFromJaxbMapper.getRequestModel(requestModel)

        then:
        result.handlingType == UserHandlingType.UPDATE
        result.firstName == requestModel.user.firstName
        result.lastName == requestModel.user.lastName
        result.userName == requestModel.user.userName
        result.EMail == requestModel.user.EMail
        result.password == requestModel.user.password
        result.userId == requestModel.user.userId
    }
}
