package de.wsorg.feeder.processor.execution.user;

import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserHandlingExecutionFactory {

    private static ClassPathXmlApplicationContext appContext;

    static {
        String[] configurationFiles = new String[]{"classpath:executionPluginContext.xml"};
        appContext = new ClassPathXmlApplicationContext(configurationFiles);
    }

    public static FeederRequestResponseHandler getUserRequestResponseHandler() {
        return appContext.getBean("basicFeederExecutor", FeederRequestResponseHandler.class);
    }
}
