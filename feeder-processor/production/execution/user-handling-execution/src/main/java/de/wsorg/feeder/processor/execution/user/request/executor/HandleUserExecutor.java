package de.wsorg.feeder.processor.execution.user.request.executor;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.HandleUserRequestType;
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.usecases.user.FeedUserHandlerFactory;
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "handleUserExecutor")
public class HandleUserExecutor extends XmlBasedRequestExecutorTemplate<GenericResponseResult, HandleUserRequest> {

    private static final String JAXB_GENERATED_PACKAGE_NAME = "de.wsorg.feeder.processor.execution.domain.generated.schema.request.user";

    @Inject
    @Named(value = "handleUserRequestFromJaxbMapper")
    private RequestModelMapper<HandleUserRequest, HandleUserRequestType> requestModelMapper;
    private FeedUserHandler userHandler;

    @Override
    protected RequestModelMapper<HandleUserRequest, HandleUserRequestType> getRequestModelMapper() {
        return requestModelMapper;
    }

    @Override
    protected GenericResponseResult executeConcreteRequest(final HandleUserRequest executionParameter) {
        GenericResponseResult result = new GenericResponseResult();

        if (executionParameter.getHandlingType() == UserHandlingType.UPDATE)
            getUserHandler().updateUser(executionParameter);

        result.setProcessingResult(ProcessingResult.SUCCESS);

        return result;
    }

    @Override
    protected String getJaxbGeneratedPackageName() {
        return JAXB_GENERATED_PACKAGE_NAME;
    }



    private FeedUserHandler getUserHandler(){
        if(userHandler == null){
            userHandler = FeedUserHandlerFactory.getUserHandler();
        }

        return userHandler;
    }
}
