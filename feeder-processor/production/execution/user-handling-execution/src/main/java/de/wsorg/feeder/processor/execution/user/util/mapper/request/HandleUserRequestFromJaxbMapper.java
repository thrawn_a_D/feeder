package de.wsorg.feeder.processor.execution.user.util.mapper.request;

import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.HandleUserRequestType;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("handleUserRequestFromJaxbMapper")
public class HandleUserRequestFromJaxbMapper implements RequestModelMapper<HandleUserRequest, HandleUserRequestType> {
    @Override
    public HandleUserRequest getRequestModel(final HandleUserRequestType requestModel) {
        HandleUserRequest mappedObject = new HandleUserRequest();
        mapHandlingType(requestModel, mappedObject);

        mapUserToHandle(requestModel, mappedObject);

        return mappedObject;
    }

    private void mapUserToHandle(final HandleUserRequestType userRequestType, final HandleUserRequest mappedObject) {
        mappedObject.setFirstName(userRequestType.getUser().getFirstName());
        mappedObject.setLastName(userRequestType.getUser().getLastName());
        mappedObject.setUserName(userRequestType.getUser().getUserName());
        mappedObject.setEMail(userRequestType.getUser().getEMail());
        mappedObject.setPassword(userRequestType.getUser().getPassword());
        mappedObject.setUserId(userRequestType.getUser().getUserId());
    }

    private void mapHandlingType(final HandleUserRequestType userRequestType, final HandleUserRequest mappedObject) {
        UserHandlingType mappedHandlingType = UserHandlingType.valueOf(userRequestType.getRequestType().name());
        mappedObject.setHandlingType(mappedHandlingType);
    }
}
