package de.wsorg.feeder.processor.execution.user.response.processor;

import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userResponseProcessor")
public class UserResponseProcessor extends ResponseProcessorTemplate<FeederUseCaseUser> {

    @Inject
    @Named("userResponseModelMapper")
    ResponseModelMapper<FeederUseCaseUser> responseModelMapper;

    @Override
    protected ResponseModelMapper getResponseModelMapper() {
        return responseModelMapper;
    }

    @Override
    protected void validateInputObjectType(final Object objectToTreatAsResponse) {
        if(!(objectToTreatAsResponse instanceof FeederUseCaseUser))
            throw new IllegalArgumentException("The provided response object is not of type FeederUseCaseUser");
    }
}
