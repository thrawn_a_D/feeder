package de.wsorg.feeder.processor.execution.user.request.executor;

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.processor.execution.request.executor.mapbased.MapBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.FeedUserHandlerFactory;
import de.wsorg.feeder.processor.usecases.user.verify.UserVerifier;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class LoginUserExecutor extends MapBasedRequestExecutorTemplate<FeederClientSideUser, UserLoginRequest> {

    @Inject
    @Named(value = "userLoginRequestFromMapMapper")
    private MapBasedRequestModelMapper<UserLoginRequest> requestModelMapper;

    private UserVerifier userVerifier;

    @Override
    protected MapBasedRequestModelMapper getRequestModelMapper() {
        return  requestModelMapper;
    }

    @Override
    protected FeederClientSideUser executeConcreteRequest(final UserLoginRequest executionParameter) {
        FeederUseCaseUser resultingUseCaseUser = getUserVerifier().loginUser(executionParameter.getNickName(),
                                                                         executionParameter.getPassword());

        if(isLoginGranted(resultingUseCaseUser))
            return new FeederClientSideUser(resultingUseCaseUser);
        else
            return null;
    }

    private boolean isLoginGranted(final FeederUseCaseUser resultingUseCaseUser) {
        return resultingUseCaseUser != null;
    }

    private UserVerifier getUserVerifier(){
        if(userVerifier == null)
            userVerifier = FeedUserHandlerFactory.getUserVerifier();

        return userVerifier;
    }
}
