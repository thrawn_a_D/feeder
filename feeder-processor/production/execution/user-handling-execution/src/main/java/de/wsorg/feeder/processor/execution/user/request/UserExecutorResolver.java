package de.wsorg.feeder.processor.execution.user.request;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.request.ExecutorResolver;
import de.wsorg.feeder.processor.execution.request.executor.RequestExecutor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userExecutorResolver")
public class UserExecutorResolver implements ExecutorResolver {

    @Inject
    @Named(value = "createUserExecutor")
    private RequestExecutor userCreationExecutor;

    @Inject
    @Named(value = "handleUserExecutor")
    private RequestExecutor userHandleExecutor;

    @Inject
    @Named("loginUserExecutor")
    private RequestExecutor userLoginExecutor;

    @Override
    public RequestExecutor resolveExecutor(final FeederCommand feederCommand) {
        RequestExecutor chosenExecutor = null;
        if(feederCommand == FeederCommand.CREATE_USER) {
            chosenExecutor = userCreationExecutor;
        } else if(feederCommand == FeederCommand.HANDLE_USER) {
            chosenExecutor = userHandleExecutor;
        } else if (feederCommand == FeederCommand.LOGIN_USER) {
            chosenExecutor = userLoginExecutor;
        }

        return chosenExecutor;
    }
}
