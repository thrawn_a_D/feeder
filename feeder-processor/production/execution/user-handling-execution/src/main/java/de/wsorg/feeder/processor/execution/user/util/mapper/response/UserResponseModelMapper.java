package de.wsorg.feeder.processor.execution.user.util.mapper.response;

import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.AbstractUserType;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.FeederUserType;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userResponseModelMapper")
public class UserResponseModelMapper implements ResponseModelMapper<FeederClientSideUser> {

    private static final String JAXB_GENERATED_PACKAGE = "de.wsorg.feeder.processor.execution.domain.generated.schema.request.user";
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getResponseModel(final FeederClientSideUser modelToMap) {
        FeederUserType mappedUser = mapUserToJaxbClass(modelToMap);
        return getJaxbMarshaller().marshal(mappedUser);
    }

    private FeederUserType mapUserToJaxbClass(final FeederUseCaseUser modelToMap) {
        FeederUserType mappedUserWrapper = new FeederUserType();
        AbstractUserType mappedUser = new AbstractUserType();

        mappedUserWrapper.setFeederUser(mappedUser);

        mappedUser.setFirstName(modelToMap.getFirstName());
        mappedUser.setLastName(modelToMap.getLastName());
        mappedUser.setEMail(modelToMap.getEMail());
        mappedUser.setUserId(modelToMap.getUserId());
        mappedUser.setPassword(modelToMap.getPassword());
        mappedUser.setUserName(modelToMap.getUserName());

        return mappedUserWrapper;
    }

    private JaxbMarshaller getJaxbMarshaller(){
        if(jaxbMarshaller == null){
            jaxbMarshaller = UtilsFactory.getJaxbMarshaller();
        }

        jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_PACKAGE);

        return jaxbMarshaller;
    }
}
