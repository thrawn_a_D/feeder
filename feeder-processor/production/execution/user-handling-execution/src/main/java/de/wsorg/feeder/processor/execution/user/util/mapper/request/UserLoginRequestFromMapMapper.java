package de.wsorg.feeder.processor.execution.user.util.mapper.request;

import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userLoginRequestFromMapMapper")
public class UserLoginRequestFromMapMapper implements MapBasedRequestModelMapper<UserLoginRequest> {
    @Override
    public UserLoginRequest getRequestModel(final Map<String, String> requestModel) {
        UserLoginRequest mappedResult = new UserLoginRequest();

        final String nickName = requestModel.get("nickName");
        final String password = requestModel.get("password");

        mappedResult.setNickName(nickName);
        mappedResult.setPassword(password);

        return mappedResult;
    }
}
