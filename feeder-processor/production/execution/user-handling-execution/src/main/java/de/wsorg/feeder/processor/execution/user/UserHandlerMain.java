package de.wsorg.feeder.processor.execution.user;

import de.wsorg.feeder.processor.execution.FeederMain;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import de.wsorg.feeder.processor.execution.request.ExecutorResolver;
import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserHandlerMain extends FeederMain {

    private static UserHandlerMain userHandlerMain;
    private ExecutorResolver userRequestExecutorResolver;
    private ResponseProcessorResolver userResponseProcessorResolver;
    private ClassPathXmlApplicationContext appContext;
    private FeederRequestResponseHandler feederRequestResponseHandler;


    public static void main(String[] args) {
        getFeedHandler().processRequest(args);
    }

    @Override
    protected FeederRequestResponseHandler getFeederRequestResponseHandler() {
        if(feederRequestResponseHandler == null){
            initApplicationContext();
            feederRequestResponseHandler = appContext.getBean("basicFeederExecutor", FeederRequestResponseHandler.class);
        }
        return feederRequestResponseHandler;
    }

    private void initApplicationContext(){
        String[] configurationFiles = new String[]{"classpath:executionPluginContext.xml"};
        appContext = new ClassPathXmlApplicationContext(configurationFiles);
    }

    private static UserHandlerMain getFeedHandler(){
        if(userHandlerMain == null)
            userHandlerMain = new UserHandlerMain();

        return userHandlerMain;
    }
}
