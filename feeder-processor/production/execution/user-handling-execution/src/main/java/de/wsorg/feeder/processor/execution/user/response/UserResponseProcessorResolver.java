package de.wsorg.feeder.processor.execution.user.response;

import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver;
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userResponseProcessorResolver")
public class UserResponseProcessorResolver implements ResponseProcessorResolver {

    @Inject
    @Named("genericResponseResolver")
    private ResponseProcessorResolver genericResponseResolver;

    @Inject
    @Named("userResponseProcessor")
    private ResponseProcessor<FeederUseCaseUser> userResponseProcessor;

    @Override
    public ResponseProcessor resolveResponseProcessor(final Object responseObject) {
        ResponseProcessor foundResponseProcessor = null;

        foundResponseProcessor = genericResponseResolver.resolveResponseProcessor(responseObject);

        if(foundResponseProcessor == null){
            foundResponseProcessor = tryToFindUserSpecificExecutor(responseObject, foundResponseProcessor);
            validateProcessor(foundResponseProcessor, responseObject);
        }

        return foundResponseProcessor;
    }

    private void validateProcessor(final ResponseProcessor foundResponseProcessor, final Object responseObject) {
        if(foundResponseProcessor == null)
            throw new IllegalArgumentException("No valid response processor could be found for " + responseObject);
    }

    private ResponseProcessor tryToFindUserSpecificExecutor(final Object responseObject, ResponseProcessor foundResponseProcessor) {
        if(responseObject instanceof FeederUseCaseUser)
            foundResponseProcessor = userResponseProcessor;
        return foundResponseProcessor;
    }
}
