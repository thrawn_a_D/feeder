package de.wsorg.feeder.processor.execution.user.util.mapper.request;

import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.CreateUserRequestType;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("createUserRequestFromJaxbMapper")
public class CreateUserRequestFromJaxbMapper implements RequestModelMapper<CreateUserRequest, CreateUserRequestType> {
    @Override
    public CreateUserRequest getRequestModel(final CreateUserRequestType requestModel) {
        CreateUserRequest mappedObject = new CreateUserRequest();

        mapUserToHandle(requestModel, mappedObject);

        return mappedObject;
    }

    private void mapUserToHandle(final CreateUserRequestType userRequestType, final CreateUserRequest mappedObject) {
        final FeederClientSideUser mappedUseCaseUserToHandle = new FeederClientSideUser();
        mappedUseCaseUserToHandle.setFirstName(userRequestType.getUser().getFirstName());
        mappedUseCaseUserToHandle.setLastName(userRequestType.getUser().getLastName());
        mappedUseCaseUserToHandle.setUserName(userRequestType.getUser().getUserName());
        mappedUseCaseUserToHandle.setEMail(userRequestType.getUser().getEMail());
        mappedUseCaseUserToHandle.setPassword(userRequestType.getUser().getPassword());
        mappedUseCaseUserToHandle.setUserId(userRequestType.getUser().getUserId());
        mappedObject.setUserToBeHandled(mappedUseCaseUserToHandle);
    }
}
