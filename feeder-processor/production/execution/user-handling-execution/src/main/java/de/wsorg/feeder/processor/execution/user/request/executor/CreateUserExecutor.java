package de.wsorg.feeder.processor.execution.user.request.executor;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.execution.domain.generated.schema.request.user.HandleUserRequestType;
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.FeedUserHandlerFactory;
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "createUserExecutor")
public class CreateUserExecutor extends XmlBasedRequestExecutorTemplate<GenericResponseResult, CreateUserRequest> {

    private static final String JAXB_GENERATED_PACKAGE_NAME = "de.wsorg.feeder.processor.execution.domain.generated.schema.request.user";

    @Inject
    @Named(value = "createUserRequestFromJaxbMapper")
    private RequestModelMapper<CreateUserRequest, HandleUserRequestType> requestModelMapper;
    private FeedUserHandler userHandler;

    @Override
    protected RequestModelMapper<CreateUserRequest, HandleUserRequestType> getRequestModelMapper() {
        return requestModelMapper;
    }

    @Override
    protected GenericResponseResult executeConcreteRequest(final CreateUserRequest executionParameter) {
        GenericResponseResult result = new GenericResponseResult();

        FeederUseCaseUser feederUseCaseUser = executionParameter.getUserToBeHandled();

        getUserHandler().createUser(feederUseCaseUser);

        result.setProcessingResult(ProcessingResult.SUCCESS);

        return result;
    }

    @Override
    protected String getJaxbGeneratedPackageName() {
        return JAXB_GENERATED_PACKAGE_NAME;
    }



    private FeedUserHandler getUserHandler(){
        if(userHandler == null){
            userHandler = FeedUserHandlerFactory.getUserHandler();
        }

        return userHandler;
    }
}
