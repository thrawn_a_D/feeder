package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries;

import javax.inject.Inject;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.AtomManipulator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EntryDescriptionFieldManipulator implements AtomManipulator {
    private static final String DESCRIPTION_FIELD_NAME = "summary";

    @Inject
    private AtomModelMappingHelper atomModelMappingHelper;


    @Override
    public void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate) {
        if (StringUtils.isNotBlank(modelToMap.getDescription())) {
            atomModelMappingHelper.addTextType(modelToMap.getDescription(), DESCRIPTION_FIELD_NAME, attributeObjectsToManipulate);
        }
    }
}
