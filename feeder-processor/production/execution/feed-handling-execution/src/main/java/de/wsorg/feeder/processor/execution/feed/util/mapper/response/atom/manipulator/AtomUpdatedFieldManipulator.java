package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator;

import javax.inject.Inject;
import java.util.List;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomUpdatedFieldManipulator implements AtomManipulator {
    private static final String UPDATED_FIELD_NAME = "updated";

    @Inject
    private AtomModelMappingHelper atomModelMappingHelper;


    @Override
    public void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate) {
        if (modelToMap.getUpdated() != null) {
            atomModelMappingHelper.addDateTimeType(modelToMap.getUpdated(), UPDATED_FIELD_NAME, attributeObjectsToManipulate);
        }
    }
}
