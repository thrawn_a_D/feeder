package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "aggregatingOpmlMetadataBuilder")
public class AggregatingOpmlMetadataBuilder implements OpmlMetadataBuilder {
    private static final String METADATA_SEPARATOR_TAG = "@#@#@";

    @Resource(name = "opmlMetadataBuilders")
    final List<OpmlMetadataBuilder> metadataBuilderList = new ArrayList<OpmlMetadataBuilder>();

    @Override
    public String getMetadata(final FeedModel currentFeedModel) {
        String result = "";
        for (OpmlMetadataBuilder opmlMetadataBuilder : metadataBuilderList) {
            String builderResult = opmlMetadataBuilder.getMetadata(currentFeedModel);
            if (StringUtils.isNotBlank(builderResult)) {
                result += METADATA_SEPARATOR_TAG + builderResult;
            }
        }

        result = adjustSeparatorTag(result);

        return result;
    }

    private String adjustSeparatorTag(String result) {
        if(result.startsWith(METADATA_SEPARATOR_TAG))
            result += METADATA_SEPARATOR_TAG;
        return result;
    }
}
