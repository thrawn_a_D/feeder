package de.wsorg.feeder.processor.execution.feed.request.executor.handling;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry.FeedEntryReadMarker;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedEntryReadMarkerRequestExecutor extends XmlBasedRequestExecutorTemplate<GenericResponseResult, MarkFeedEntryReadStatusHandling> {

    private static final String JAXB_GENERATED_PACKAGE_NAME = "de.wsorg.feeder.processor.execution.domain.generated.request.handler";
    @Inject
    @Named(value = "feedEntryReadMarkerModelMapper")
    private RequestModelMapper requestModelMapper;

    private FeedEntryReadMarker feedEntryReadMarker;

    @Override
    protected RequestModelMapper getRequestModelMapper() {
        return requestModelMapper;
    }

    @Override
    protected GenericResponseResult executeConcreteRequest(final MarkFeedEntryReadStatusHandling executionParameter) {
        GenericResponseResult executionResult = new GenericResponseResult();

        try {
            getFeedModifier().markFeedEntries(executionParameter);
            executionResult.setProcessingResult(ProcessingResult.SUCCESS);
        } catch (Exception ex) {
            executionResult.setProcessingResult(ProcessingResult.FAILED);
            executionResult.setMessage(ex.getMessage());
        }


        return executionResult;
    }

    @Override
    protected String getJaxbGeneratedPackageName() {
        return JAXB_GENERATED_PACKAGE_NAME;
    }

    private FeedEntryReadMarker getFeedModifier(){
        if(feedEntryReadMarker == null){
            feedEntryReadMarker = FeedProcessorFactory.getFeedEntryReadMarker();
        }

        return feedEntryReadMarker;
    }
}
