package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common;

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;

import javax.inject.Named;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class DefaultCommonGlobalSearchRequestMapper implements CommonGlobalSearchRequestMapper{

    private static final String SEARCH_TERM = "searchTerm";

    @Override
    public <T extends GlobalSearchQuery> T mapJaxbToModel(final Map<String, String> modelToMap,
                                                          final Class<T> objectToMapTo) {
        GlobalSearchQuery resultingModelObject = getModelInstance(objectToMapTo);

        if(modelToMap.containsKey(SEARCH_TERM)) {
            final String searchTerm = modelToMap.get(SEARCH_TERM);
            resultingModelObject.setSearchTerm(searchTerm);
        }

        return (T) resultingModelObject;
    }

    private <T extends GlobalSearchQuery> T getModelInstance(final Class<T> objectToMapTo) {
        T loadFeed = null;
        try {
            loadFeed = (T) objectToMapTo.getConstructors()[0].newInstance(new Object[0]);
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvocationTargetException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return loadFeed;
    }
}
