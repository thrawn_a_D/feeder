package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import org.apache.commons.lang.StringUtils;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomUiHighlightingColorManipulator implements OtherValuesManipulator {
    private static final String METADATA_UI_FEED_COLOR="FEED_UI_HIGHLIGHTING_COLOR";

    @Override
    public void manipulate(final FeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom) {
        if(modelToMap instanceof UserRelatedFeedModel){
            UserRelatedFeedModel userFeedModel = (UserRelatedFeedModel) modelToMap;
            if (StringUtils.isNotBlank(userFeedModel.getFeedUiHighlightingColor())) {
                String uiFeedColor = userFeedModel.getFeedUiHighlightingColor();
                otherAttributesOfAtom.put(new QName(METADATA_UI_FEED_COLOR), uiFeedColor);
            }
        }
    }
}
