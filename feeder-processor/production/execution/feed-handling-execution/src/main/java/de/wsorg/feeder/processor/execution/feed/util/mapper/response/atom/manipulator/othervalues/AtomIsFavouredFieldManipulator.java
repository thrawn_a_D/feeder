package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomIsFavouredFieldManipulator implements OtherValuesManipulator {
    private static final String METADATA_IS_FAVOURED="IS_FAVOURED";

    @Override
    public void manipulate(final FeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom) {
        String isFeedFavoured = "false";
        if(modelToMap instanceof UserRelatedFeedModel){
            isFeedFavoured = "true";
        } else {
            isFeedFavoured = "false";
        }
        otherAttributesOfAtom.put(new QName(METADATA_IS_FAVOURED), isFeedFavoured);
    }
}
