package de.wsorg.feeder.processor.execution.feed.response;

import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver;
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "feedResponseProcessorResolver")
public class FeedResponseProcessorResolver implements ResponseProcessorResolver {

    @Inject
    @Named(value = "opmlResponseProcessor")
    private ResponseProcessor searchResponseProcessor;
    @Inject
    @Named(value = "atomResponseProcessor")
    private ResponseProcessor loadResponseProcessor;
    @Inject
    @Named(value = "genericResponseResolver")
    private ResponseProcessorResolver genericResponseResolver;

    @Override
    public ResponseProcessor resolveResponseProcessor(final Object responseObject) {
        ResponseProcessor someGenericProcessor = genericResponseResolver.resolveResponseProcessor(responseObject);
        if(someGenericProcessor != null) {
            return someGenericProcessor;
        } else  if(isSearchResponse(responseObject)){
            return searchResponseProcessor;
        } else if (isLoadResponse(responseObject)){
            return loadResponseProcessor;
        } else {
            final String errorMessage = "The provided response object could not be mapped to a response processor.";
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private boolean isLoadResponse(final Object responseObject) {
        return responseObject instanceof FeedModel;
    }

    private boolean isSearchResponse(final Object responseObject) {
        boolean isSearchResponse = false;

        if(responseObject instanceof List){
            List responseObjectList = (List) responseObject;
            if(responseObjectList.size() > 0) {
                Object listContent = responseObjectList.get(0);
                if(listContent instanceof FeedModel)
                    isSearchResponse=true;
            } else {
                isSearchResponse=true;
            }
        }

        return isSearchResponse;
    }
}
