package de.wsorg.feeder.processor.execution.feed.response.processor;

import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "atomResponseProcessor")
public class AtomResponseProcessor extends ResponseProcessorTemplate<FeederAtom> {
    @Inject
    @Named(value = "atomResponseModelMapper")
    private ResponseModelMapper atomResponseMapper;

    @Override
    protected ResponseModelMapper getResponseModelMapper() {
        return atomResponseMapper;
    }

    @Override
    protected void validateInputObjectType(final Object objectToTreatAsResponse) {
        if(!(objectToTreatAsResponse instanceof FeedModel)){
            throw new IllegalArgumentException("The provided object is not of type FeedModel");
        }
    }
}
