package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler;

import javax.inject.Named;

import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedAsReadMapItemType;
import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedEntryAsReadMapType;
import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedEntryAsReadType;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedEntryReadMarkerModelMapper implements RequestModelMapper<MarkFeedEntryReadStatusHandling, MarkFeedEntryAsReadType> {
    @Override
    public MarkFeedEntryReadStatusHandling getRequestModel(final MarkFeedEntryAsReadType requestModel) {

        MarkFeedEntryReadStatusHandling mappedResult = new MarkFeedEntryReadStatusHandling();

        mappedResult.setFeedUrl(requestModel.getFeedUrl());

        mapFeedReadStatus(requestModel, mappedResult);

        return mappedResult;
    }

    private void mapFeedReadStatus(final MarkFeedEntryAsReadType jaxbHandleFeedType, final MarkFeedEntryReadStatusHandling mappedResult) {
        final MarkFeedEntryAsReadMapType markFeedEntryWithReadStatus = jaxbHandleFeedType.getMarkFeedEntryWithReadStatus();

        for (MarkFeedAsReadMapItemType markFeedAsReadMapItemType : markFeedEntryWithReadStatus.getItem()) {
            final String uid = markFeedAsReadMapItemType.getUid();
            final boolean isRead = markFeedAsReadMapItemType.isIsRead();
            mappedResult.getFeedEntryUidWithReadabilityStatus().put(uid, isRead);
        }
    }
}

