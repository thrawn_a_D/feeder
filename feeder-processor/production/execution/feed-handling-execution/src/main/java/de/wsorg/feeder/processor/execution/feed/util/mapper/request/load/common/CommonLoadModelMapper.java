package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common;

import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonLoadModelMapper {
    <T extends LoadFeed> T mapJaxbToModel(final Map<String, String> modelToMap, final Class<T> objectToMapTo);
}
