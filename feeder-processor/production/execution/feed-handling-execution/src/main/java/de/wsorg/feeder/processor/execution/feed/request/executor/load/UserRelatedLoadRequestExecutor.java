package de.wsorg.feeder.processor.execution.feed.request.executor.load;

import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserRelatedLoadRequestExecutor extends LoadRequestExecutor {
    @Inject
    @Named(value = "userRelatedLoadRequestModelMapper")
    private MapBasedRequestModelMapper<UserRelatedLoadFeedRequest> loadRequestModelMapper;

    @Override
    protected MapBasedRequestModelMapper getRequestModelMapper() {
        return loadRequestModelMapper;
    }

}
