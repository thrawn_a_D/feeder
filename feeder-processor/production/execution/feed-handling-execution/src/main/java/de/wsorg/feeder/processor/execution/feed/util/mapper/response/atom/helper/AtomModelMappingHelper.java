package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper;

import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.DateTimeType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.IdType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LogoType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.TextType;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AtomModelMappingHelper {
    public static final String ATOM_NAMESPACE="http://www.w3.org/2005/Atom";
    private static final String DEFAULT_TEXT_TYPE = "html";

    public void addIdType(final String id, final String fieldName, final List<Object> attributeObjects) {
        IdType feedId = new IdType();
        feedId.setValue(id);
        addElementToFeedType(attributeObjects, fieldName, feedId, IdType.class);
    }

    public void addDateTimeType(final Date date, final String fieldName, final List<Object> attributeObjects) {
        DateTimeType feedUpdated = new DateTimeType();
        GregorianCalendar publishedDate = new GregorianCalendar();

        if(date != null) {
            publishedDate.setTime(date);
        }

        feedUpdated.setValue(new XMLGregorianCalendarImpl(publishedDate));
        addElementToFeedType(attributeObjects, fieldName, feedUpdated, DateTimeType.class);
    }

    public void addTextType(final String text, final String fieldName, final List<Object> attributeObjects) {
        TextType feedTitle = new TextType();
        feedTitle.getContent().add(text);
        feedTitle.setType(DEFAULT_TEXT_TYPE);
        addElementToFeedType(attributeObjects, fieldName, feedTitle, TextType.class);
    }

    public void addLogoType(final String imageUrl, final String fieldName, final List<Object> authorOrCategoryOrContributor) {
        LogoType feedLogo = new LogoType();
        feedLogo.setBase(imageUrl);
        addElementToFeedType(authorOrCategoryOrContributor, fieldName, feedLogo, LogoType.class);
    }

    public void addLinkType(final Link link, final List<Object> authorOrCategoryOrContributor) {
        LinkType linkType = new LinkType();
        linkType.setHref(link.getHref());
        if(link.getRelation() != null)
            linkType.setRel(link.getRelation().getLinkRfcCode());
        if(link.getLinkType() != null)
            linkType.setType(link.getLinkType().getLinkType());
        addElementToFeedType(authorOrCategoryOrContributor, "link", linkType, LinkType.class);
    }

    public void addElementToFeedType(final List<Object> attributeObjects,
                                        final String fieldName,
                                        final Object fieldToAdd,
                                        final Class<?> fieldType) {
        JAXBElement jaxbElement = new JAXBElement(new QName(ATOM_NAMESPACE, fieldName), fieldType, fieldToAdd);
        attributeObjects.add(jaxbElement);
    }
}
