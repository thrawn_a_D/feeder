package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler;

import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.processor.execution.domain.generated.request.handler.FeedSubscriptionType;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "feedSubscriptionModelMapper")
public class FeedSubscriptionModelMapper implements RequestModelMapper<FeedSubscriptionRequest, FeedSubscriptionType> {
    @Override
    public FeedSubscriptionRequest getRequestModel(final FeedSubscriptionType requestModel) {

        FeedSubscriptionRequest mappedResult = new FeedSubscriptionRequest();

        mappedResult.setFeedUrl(requestModel.getFeedUrl());
        mappedResult.setFavourFeed(requestModel.isFavour());
        return mappedResult;
    }
}
