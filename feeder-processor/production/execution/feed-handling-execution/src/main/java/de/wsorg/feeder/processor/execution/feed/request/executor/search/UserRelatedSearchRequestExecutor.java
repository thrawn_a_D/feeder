package de.wsorg.feeder.processor.execution.feed.request.executor.search;

import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.execution.request.executor.mapbased.MapBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.finder.userrelated.UserRelatedFeedFinder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userRelatedSearchRequestExecutor")
public class UserRelatedSearchRequestExecutor extends MapBasedRequestExecutorTemplate<List<UserRelatedFeedModel>, UserRelatedSearchQueryRequest> {
    private UserRelatedFeedFinder userRelatedFeedFinder;
    @Inject
    @Named(value = "userRelatedSearchRequestModelMapper")
    private MapBasedRequestModelMapper<UserRelatedSearchQueryRequest> searchRequestModelMapper;

    @Override
    protected MapBasedRequestModelMapper getRequestModelMapper() {
        return searchRequestModelMapper;
    }

    @Override
    protected List<UserRelatedFeedModel> executeConcreteRequest(final UserRelatedSearchQueryRequest executionParameter) {
        return getFeedFinder().findFeeds(executionParameter);
    }

    private UserRelatedFeedFinder getFeedFinder(){
        if(userRelatedFeedFinder == null)
            userRelatedFeedFinder = FeedProcessorFactory.getUserRelatedFeedFinder();

        return userRelatedFeedFinder;
    }
}
