package de.wsorg.feeder.processor.execution.feed.response.processor;

import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "opmlResponseProcessor")
public class OpmlResponseProcessor extends ResponseProcessorTemplate<List<FeedModel>> {

    @Inject
    @Named(value = "opmlResponseModelMapper")
    private ResponseModelMapper opmlResponseModelMapper;

    @Override
    protected ResponseModelMapper getResponseModelMapper() {
        return opmlResponseModelMapper;
    }

    @Override
    protected void validateInputObjectType(final Object objectToTreatAsResponse) {
        if(!(objectToTreatAsResponse instanceof List)) {
            throw new IllegalArgumentException("The provided response object is not of type List!");
        } else if (((List) objectToTreatAsResponse).size() > 0 &&
                !(((List) objectToTreatAsResponse).get(0) instanceof FeedModel)){
            throw new IllegalArgumentException("The provided response object is not of type List<FeedModel>!");
        }
    }
}
