package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator;

import javax.inject.Inject;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomLogoFieldManipulator implements AtomManipulator {
    private static final String LOGO_FIELD_NAME = "logo";

    @Inject
    private AtomModelMappingHelper atomModelMappingHelper;


    @Override
    public void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate) {
        if(modelToMap instanceof FeedModel) {
            final FeedModel feedModel = (FeedModel) modelToMap;
            if (StringUtils.isNotBlank(feedModel.getLogoUrl())) {
                atomModelMappingHelper.addLogoType(feedModel.getLogoUrl(), LOGO_FIELD_NAME, attributeObjectsToManipulate);
            }
        }
    }
}
