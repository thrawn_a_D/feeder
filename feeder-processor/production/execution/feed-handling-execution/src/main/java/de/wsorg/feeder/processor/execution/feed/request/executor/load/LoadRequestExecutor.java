package de.wsorg.feeder.processor.execution.feed.request.executor.load;

import de.wsorg.feeder.processor.execution.request.executor.mapbased.MapBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.loader.CommonFeedLoader;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "loadRequestExecutor")
public class LoadRequestExecutor extends MapBasedRequestExecutorTemplate<FeedModel, LoadFeed> {

    private CommonFeedLoader simpleFeedLoader;
    @Inject
    @Named(value = "loadRequestModelMapper")
    private MapBasedRequestModelMapper<LoadFeed> loadRequestModelMapper;

    @Override
    protected MapBasedRequestModelMapper<LoadFeed> getRequestModelMapper() {
        return loadRequestModelMapper;
    }

    @Override
    protected FeedModel executeConcreteRequest(final LoadFeed executionParameter) {
        return getSimpleFeedLoader().loadFeed(executionParameter);
    }

    private CommonFeedLoader getSimpleFeedLoader(){
        if(simpleFeedLoader == null){
            simpleFeedLoader = FeedProcessorFactory.getFeedLoader();
        }

        return simpleFeedLoader;
    }
}
