package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface AtomManipulator {
    void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate);
}
