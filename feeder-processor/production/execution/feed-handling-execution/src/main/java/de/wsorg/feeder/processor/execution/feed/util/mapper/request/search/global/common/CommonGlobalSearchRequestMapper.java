package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common;

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonGlobalSearchRequestMapper {
    <T extends GlobalSearchQuery> T mapJaxbToModel(final Map<String, String> modelToMap, final Class<T> objectToMapTo);
}
