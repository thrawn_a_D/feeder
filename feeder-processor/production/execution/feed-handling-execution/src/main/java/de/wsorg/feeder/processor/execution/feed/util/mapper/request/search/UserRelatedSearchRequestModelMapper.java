package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search;

import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import javax.inject.Named;
import java.util.Map;

import static de.wsorg.feeder.processor.production.domain.feeder.search.user.UserSearchRestriction.UNREAD_ENTRIES;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("userRelatedSearchRequestModelMapper")
public class UserRelatedSearchRequestModelMapper implements MapBasedRequestModelMapper<UserRelatedSearchQueryRequest> {

    private static final String SEARCH_TERM = "searchTerm";
    private static final String SHOW_UNREAD_ENTRIES = "showUnreadEntries";

    @Override
    public UserRelatedSearchQueryRequest getRequestModel(final Map<String, String> requestModel) {
        UserRelatedSearchQueryRequest resultingModelObject = new UserRelatedSearchQueryRequest();

        if(requestModel.containsKey(SEARCH_TERM)) {
            final String searchTerm = requestModel.get(SEARCH_TERM);
            resultingModelObject.setSearchTerm(searchTerm);
        }

        if (requestModel.containsKey(SHOW_UNREAD_ENTRIES)) {
            final String showUnreadEntries = requestModel.get(SHOW_UNREAD_ENTRIES);
            resultingModelObject.addRestriction(UNREAD_ENTRIES, Boolean.valueOf(showUnreadEntries));
        }

        return resultingModelObject;
    }
}