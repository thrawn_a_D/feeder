package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load;

import de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common.CommonLoadModelMapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "loadRequestModelMapper")
public class LoadRequestModelMapper implements MapBasedRequestModelMapper<LoadFeed> {

    @Inject
    private CommonLoadModelMapper commonLoadModelMapper;

    @Override
    public LoadFeed getRequestModel(final Map<String, String> requestModel) {
        return commonLoadModelMapper.mapJaxbToModel(requestModel, LoadFeed.class);
    }
}
