package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.metadata;

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import org.apache.commons.lang.StringUtils;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedColorMetadataBuilder implements OpmlMetadataBuilder {

    private static final String FEED_COLOR_METADATA_TAG = "FEED_UI_HIGHLIGHTING_COLOR";

    @Override
    public String getMetadata(final FeedModel currentFeedModel) {
        String result = "";
        if(currentFeedModel instanceof UserRelatedFeedModel) {
            UserRelatedFeedModel userFeedModel = (UserRelatedFeedModel) currentFeedModel;
            if(StringUtils.isNotBlank(userFeedModel.getFeedUiHighlightingColor()))
                result = FEED_COLOR_METADATA_TAG + OpmlMetadataBuilder.META_TAG_SEPARATOR + userFeedModel.getFeedUiHighlightingColor();
        }
        return result;
    }
}
