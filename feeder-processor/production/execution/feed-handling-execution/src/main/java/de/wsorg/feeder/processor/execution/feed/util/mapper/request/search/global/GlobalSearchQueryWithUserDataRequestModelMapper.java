package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global;

import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common.CommonGlobalSearchRequestMapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class GlobalSearchQueryWithUserDataRequestModelMapper implements MapBasedRequestModelMapper<GlobalSearchQueryEnrichedWithUserDataRequest> {

    @Inject
    private CommonGlobalSearchRequestMapper commonGlobalSearchRequestMapper;

    public GlobalSearchQueryEnrichedWithUserDataRequest getRequestModel(final Map<String, String> requestModel) {
        return commonGlobalSearchRequestMapper.mapJaxbToModel(requestModel, GlobalSearchQueryEnrichedWithUserDataRequest.class);
    }
}
