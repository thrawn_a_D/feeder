package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EntryReadStatusManipulator implements UserRelatedAtomEntryOtherManipulator {
    private static final String METADATA_IS_FEED_ENTRY_READ="IS_READ";

    @Override
    public void manipulate(final FeedEntryModel feedEntryModel, final UserRelatedFeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom) {
        Boolean isFeedEntryRead = modelToMap.getFeedEntriesReadStatus().get(feedEntryModel.getId());

        if (isFeedEntryRead != null) {
            otherAttributesOfAtom.put(new QName(METADATA_IS_FEED_ENTRY_READ), Boolean.toString(isFeedEntryRead));
        }
    }
}
