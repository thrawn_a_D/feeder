package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.general;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ParentFeedUrlManipulator implements GenericAtomEntryOtherManipulator {
    private static final String METADATA_PARENT_FEED_URL="PARENT_FEED_URL";

    @Override
    public void manipulate(final FeedEntryModel feedEntryModel, final Map<QName, String> otherAttributesOfAtom) {
        String feedUrl = feedEntryModel.getFeedUrl();

        if (feedUrl != null) {
            otherAttributesOfAtom.put(new QName(METADATA_PARENT_FEED_URL), feedUrl);
        }
    }
}
