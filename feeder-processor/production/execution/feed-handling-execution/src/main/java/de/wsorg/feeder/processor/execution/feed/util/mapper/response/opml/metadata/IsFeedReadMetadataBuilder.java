package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.metadata;

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class IsFeedReadMetadataBuilder implements OpmlMetadataBuilder {
    private static final String IS_FAVOURED_META_TAG = "IS_FAVOURED";

    @Override
    public String getMetadata(final FeedModel currentFeedModel) {
        String isFeedFavoured = "";
        if(currentFeedModel instanceof UserRelatedFeedModel){
            isFeedFavoured = IS_FAVOURED_META_TAG + META_TAG_SEPARATOR + "true";
        }

        return isFeedFavoured;
    }
}
