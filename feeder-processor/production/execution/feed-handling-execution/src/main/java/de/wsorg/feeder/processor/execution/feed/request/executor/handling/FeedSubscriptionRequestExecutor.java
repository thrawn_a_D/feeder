package de.wsorg.feeder.processor.execution.feed.request.executor.handling;

import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.handling.subscribtion.FeedSubscriptionHandler;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "feedSubscriptionRequestExecutor")
public class FeedSubscriptionRequestExecutor extends XmlBasedRequestExecutorTemplate<GenericResponseResult, FeedSubscriptionRequest> {

    private static final String JAXB_GENERATED_PACKAGE_NAME = "de.wsorg.feeder.processor.execution.domain.generated.request.handler";
    @Inject
    @Named(value = "feedSubscriptionModelMapper")
    private RequestModelMapper requestModelMapper;

    private FeedSubscriptionHandler feedFavourizer;

    @Override
    protected RequestModelMapper getRequestModelMapper() {
        return requestModelMapper;
    }

    @Override
    protected GenericResponseResult executeConcreteRequest(final FeedSubscriptionRequest executionParameter) {
        GenericResponseResult executionResult = new GenericResponseResult();

        try {
            handleFeedSubscription(executionParameter);
            executionResult.setProcessingResult(ProcessingResult.SUCCESS);
        } catch (Exception ex) {
            executionResult.setProcessingResult(ProcessingResult.FAILED);
            executionResult.setMessage(ex.getMessage());
        }


        return executionResult;
    }

    private void handleFeedSubscription(final FeedSubscriptionRequest executionParameter) {
        if(executionParameter.isFavourFeed())
            getFeedModifier().subscribeToFeed(executionParameter);
        else
            getFeedModifier().unsubscribeFeed(executionParameter);
    }

    @Override
    protected String getJaxbGeneratedPackageName() {
        return JAXB_GENERATED_PACKAGE_NAME;
    }

    private FeedSubscriptionHandler getFeedModifier(){
        if(feedFavourizer == null){
            feedFavourizer = FeedProcessorFactory.getFeedFavourModifier();
        }

        return feedFavourizer;
    }
}
