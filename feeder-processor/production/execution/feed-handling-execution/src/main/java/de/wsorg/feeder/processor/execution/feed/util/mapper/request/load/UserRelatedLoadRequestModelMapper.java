package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load;

import de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common.CommonLoadModelMapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserRelatedLoadRequestModelMapper implements MapBasedRequestModelMapper<UserRelatedLoadFeed> {
    private static final String SHOW_ONLY_UNREAD_ENTRIES = "showOnlyUnreadEntries";

    @Inject
    private CommonLoadModelMapper commonLoadModelMapper;

    @Override
    public UserRelatedLoadFeed getRequestModel(final Map<String, String> requestModel) {
        final UserRelatedLoadFeed userRelatedLoadFeed = commonLoadModelMapper.mapJaxbToModel(requestModel, UserRelatedLoadFeed.class);

        final String showOnlyUnreadEntries = requestModel.get(SHOW_ONLY_UNREAD_ENTRIES);
        userRelatedLoadFeed.setShowOnlyUnreadEntries(Boolean.parseBoolean(showOnlyUnreadEntries));

        return userRelatedLoadFeed;
    }
}
