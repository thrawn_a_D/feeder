package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface OpmlMetadataBuilder {
    static final String META_TAG_SEPARATOR = ":";

    String getMetadata(final FeedModel currentFeedModel);
}
