package de.wsorg.feeder.processor.execution.feed.request.executor.search.global;

import de.wsorg.feeder.processor.execution.request.executor.mapbased.MapBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "globalSearchRequestExecutor")
public class GlobalSearchRequestExecutor<T extends GlobalSearchQuery> extends MapBasedRequestExecutorTemplate<List<FeedModel>, T> {
    private GlobalFeedFinder globalFeedFinder;
    @Inject
    @Named(value = "globalSearchRequestModelMapper")
    private MapBasedRequestModelMapper<T> searchRequestModelMapper;

    @Override
    protected MapBasedRequestModelMapper getRequestModelMapper() {
        return searchRequestModelMapper;
    }

    @Override
    protected List<FeedModel> executeConcreteRequest(final T executionParameter) {
     return getFeedFinder().findFeeds(executionParameter);
    }

    private GlobalFeedFinder getFeedFinder(){
        if(globalFeedFinder == null)
            globalFeedFinder = FeedProcessorFactory.getGlobalFeedFinder();

        return globalFeedFinder;
    }
}
