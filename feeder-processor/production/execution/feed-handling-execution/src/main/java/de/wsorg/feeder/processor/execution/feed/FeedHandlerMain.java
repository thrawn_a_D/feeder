package de.wsorg.feeder.processor.execution.feed;

import de.wsorg.feeder.processor.execution.FeederMain;
import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedHandlerMain extends FeederMain {

    private static FeedHandlerMain feedHandlerMain;
    private FeederRequestResponseHandler feederRequestResponseHandler;
    private ClassPathXmlApplicationContext appContext;

    public static void main(String[] args) {
        getFeedHandler().processRequest(args);
    }

    @Override
    protected FeederRequestResponseHandler getFeederRequestResponseHandler() {
        if(feederRequestResponseHandler == null){
            initApplicationContext();
            feederRequestResponseHandler = appContext.getBean("basicFeederExecutor", FeederRequestResponseHandler.class);
        }
        return feederRequestResponseHandler;
    }

    private void initApplicationContext(){
        if(appContext == null){
            String[] configurationFiles = new String[]{"classpath:spring/executionPluginContext.xml"};
            appContext = new ClassPathXmlApplicationContext(configurationFiles);
        }
    }

    private static FeedHandlerMain getFeedHandler(){
        if(feedHandlerMain == null) {
            feedHandlerMain = new FeedHandlerMain();
        }

        return feedHandlerMain;
    }
}
