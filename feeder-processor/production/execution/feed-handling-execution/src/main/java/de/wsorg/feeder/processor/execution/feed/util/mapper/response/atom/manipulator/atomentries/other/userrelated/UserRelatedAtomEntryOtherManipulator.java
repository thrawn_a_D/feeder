package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserRelatedAtomEntryOtherManipulator {
    void manipulate(final FeedEntryModel feedEntryModel, final UserRelatedFeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom);
}
