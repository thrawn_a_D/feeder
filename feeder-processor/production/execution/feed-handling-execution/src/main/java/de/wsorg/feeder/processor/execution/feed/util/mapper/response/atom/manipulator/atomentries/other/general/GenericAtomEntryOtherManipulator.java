package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.general;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */

public interface GenericAtomEntryOtherManipulator {
    void manipulate(final FeedEntryModel feedEntryModel, final Map<QName, String> otherAttributesOfAtom);
}
