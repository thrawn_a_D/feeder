package de.wsorg.feeder.processor.execution.feed.request.executor.search.global;

import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQueryEnrichedWithUserData;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class GlobalSearchWithUserDataRequestExecutor extends GlobalSearchRequestExecutor<GlobalSearchQueryEnrichedWithUserData> {

    @Inject
    @Named(value = "globalSearchQueryWithUserDataRequestModelMapper")
    private MapBasedRequestModelMapper<GlobalSearchQueryEnrichedWithUserData> searchRequestModelMapper;

    private GlobalFeedFinder globalFeedFinder;

    @Override
    protected MapBasedRequestModelMapper getRequestModelMapper() {
        return searchRequestModelMapper;
    }

    @Override
    protected List<FeedModel> executeConcreteRequest(final GlobalSearchQueryEnrichedWithUserData executionParameter) {
        return getFeedFinder().findFeeds(executionParameter);
    }

    private GlobalFeedFinder getFeedFinder(){
        if(globalFeedFinder == null)
            globalFeedFinder = FeedProcessorFactory.getGlobalFeedFinderEnrichedWithData();

        return globalFeedFinder;
    }
}
