package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.AtomManipulator;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.general.GenericAtomEntryOtherManipulator;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated.UserRelatedAtomEntryOtherManipulator;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues.OtherValuesManipulator;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.EntryType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.FeedType;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "atomResponseModelMapper")
public class AtomModelMapper implements ResponseModelMapper<FeedModel> {

    private static final String JAXB_GENERATED_CLASSES_PACKAGE = "de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom";
    private static final String ENTRY_FIELD_NAME = "entry";

    @Inject
    private JaxbMarshaller jaxbMarshaller;

    @Inject
    AtomModelMappingHelper atomModelMappingHelper;

    @Resource(name = "headerFieldManipulators")
    private List<AtomManipulator> headerFieldManipulators;

    @Resource(name = "otherValuesHeaderFieldManipulators")
    private List<OtherValuesManipulator> otherValuesHeaderFieldManipulators;

    @Resource(name = "feedEntryManipulators")
    private List<AtomManipulator> feedEntryManipulators;

    @Resource(name = "userAtomEntryOtherManipulators")
    List<UserRelatedAtomEntryOtherManipulator> userRelatedAtomEntryOtherManipulators;

    @Resource(name = "genericAtomEntryOtherManipulators")
    List<GenericAtomEntryOtherManipulator> genericAtomEntryOtherManipulators;

    @Override
    public String getResponseModel(final FeedModel modelToMap) {
        JAXBElement mappedJaxbElement = getJaxbMappings(modelToMap);
        jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_CLASSES_PACKAGE);
        return jaxbMarshaller.marshal(mappedJaxbElement);
    }

    private JAXBElement getJaxbMappings(final FeedModel modelToMap) {
        FeedType feedTypeAsMapResult = new FeedType();

        addHeaderFields(modelToMap, feedTypeAsMapResult);

        for(FeedEntryModel feedEntry : modelToMap.getFeedEntryModels()){
            addFeedEntry(modelToMap, feedEntry, feedTypeAsMapResult);
        }

        return wrapInJaxbElement(feedTypeAsMapResult);
    }

    private void addHeaderFields(final FeedModel modelToMap, final FeedType feedTypeAsMapResult) {
        for (AtomManipulator headerFieldManipulator : headerFieldManipulators) {
            headerFieldManipulator.manipulate(modelToMap, feedTypeAsMapResult.getAuthorOrCategoryOrContributor());
        }

        for (OtherValuesManipulator otherValuesManipulator : otherValuesHeaderFieldManipulators) {
            otherValuesManipulator.manipulate(modelToMap, feedTypeAsMapResult.getOtherAttributes());
        }
    }

    private void addFeedEntry(final FeedModel modelToMap,
                              final FeedEntryModel feedEntryModel,
                              final FeedType feedTypeAsMapResult){
        EntryType entryType = new EntryType();

        for (AtomManipulator feedEntryManipulator : feedEntryManipulators) {
            feedEntryManipulator.manipulate(feedEntryModel, entryType.getAuthorOrCategoryOrContent());
        }

        if(modelToMap instanceof UserRelatedFeedModel) {
            UserRelatedFeedModel userRelatedFeedModel = (UserRelatedFeedModel) modelToMap;
            for (UserRelatedAtomEntryOtherManipulator userRelatedAtomEntryOtherManipulator : userRelatedAtomEntryOtherManipulators) {
                userRelatedAtomEntryOtherManipulator.manipulate(feedEntryModel, userRelatedFeedModel, entryType.getOtherAttributes());
            }
        }

        for (GenericAtomEntryOtherManipulator genericAtomEntryOtherManipulator : genericAtomEntryOtherManipulators) {
            genericAtomEntryOtherManipulator.manipulate(feedEntryModel, entryType.getOtherAttributes());
        }

        atomModelMappingHelper.addElementToFeedType(feedTypeAsMapResult.getAuthorOrCategoryOrContributor(),
                             ENTRY_FIELD_NAME,
                             entryType,
                             EntryType.class);
    }

    private JAXBElement<FeedType> wrapInJaxbElement(final FeedType feedTypeAsMapResult) {
        return new JAXBElement<FeedType>(new QName(AtomModelMappingHelper.ATOM_NAMESPACE, "feed"), FeedType.class, feedTypeAsMapResult);
    }
}
