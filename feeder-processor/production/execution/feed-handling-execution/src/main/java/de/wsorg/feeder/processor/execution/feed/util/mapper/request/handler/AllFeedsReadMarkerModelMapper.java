package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler;

import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkAllFeedsAsReadType;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AllFeedsReadMarkerModelMapper implements RequestModelMapper<MarkAllFeedsReadStatusHandling, MarkAllFeedsAsReadType> {
    @Override
    public MarkAllFeedsReadStatusHandling getRequestModel(final MarkAllFeedsAsReadType requestModel) {
        MarkAllFeedsReadStatusHandling mappedResult = new MarkAllFeedsReadStatusHandling();

        mappedResult.setFeedsReadStatus(requestModel.isMarkFeedsAsReadType());
        mappedResult.setCategory(requestModel.getCategory());

        return mappedResult;
    }
}

