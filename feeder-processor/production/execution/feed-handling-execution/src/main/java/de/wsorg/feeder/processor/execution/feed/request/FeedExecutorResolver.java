package de.wsorg.feeder.processor.execution.feed.request;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.request.ExecutorResolver;
import de.wsorg.feeder.processor.execution.request.executor.RequestExecutor;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("feedExecutorResolver")
public class FeedExecutorResolver implements ExecutorResolver {

    @Inject
    @Named(value = "globalSearchRequestExecutor")
    private RequestExecutor searchRequestExecutor;

    @Inject
    @Named(value = "globalSearchWithUserDataRequestExecutor")
    private RequestExecutor searchRequestWithUSerDataExecutor;

    @Inject
    @Named(value = "userRelatedSearchRequestExecutor")
    private RequestExecutor userRelatedSearchRequestExecutor;

    @Inject
    @Named(value = "loadRequestExecutor")
    private RequestExecutor loadRequestExecutor;
    @Inject
    @Named(value = "userRelatedLoadRequestExecutor")
    private RequestExecutor userRelatedLoadRequestExecutor;

    @Inject
    @Named(value = "feedSubscriptionRequestExecutor")
    private RequestExecutor subscriptionRequestExecutor;

    @Inject
    @Named(value = "feedEntryReadMarkerRequestExecutor")
    private RequestExecutor feedEntryReadMarkerRequestExecutor;

    @Inject
    @Named(value = "allFeedsReadMarkerRequestExecutor")
    private RequestExecutor allFeedsReadMarkerRequestExecutor;


    @Override
    public RequestExecutor resolveExecutor(final FeederCommand feederCommand) {
        switch (feederCommand){
            case GLOBAL_SEARCH:
                return searchRequestExecutor;
            case GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA:
                return searchRequestWithUSerDataExecutor;
            case USER_RELATED_SEARCH:
                return userRelatedSearchRequestExecutor;
            case LOAD:
                return loadRequestExecutor;
            case LOAD_USER_RELATED:
                return userRelatedLoadRequestExecutor;
            case FAVOUR_FEED:
                return subscriptionRequestExecutor;
            case MARK_FEED_ENTRY_READABILITY:
                return feedEntryReadMarkerRequestExecutor;
            case MARK_ALL_FEEDS_READABILITY:
                return allFeedsReadMarkerRequestExecutor;
            default:
                return null;
        }
    }
}
