package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomIsPagingPossibleFieldManipulator implements OtherValuesManipulator {
    private static final String METADATA_IS_PAGING_POSSIBLE="IS_PAGING_POSSIBLE";

    @Override
    public void manipulate(final FeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom) {
        Boolean isPagingPossible = modelToMap.getFeedEntriesCollection().isAdditionalPagingPossible();

        if (isPagingPossible != null) {
            otherAttributesOfAtom.put(new QName(METADATA_IS_PAGING_POSSIBLE), Boolean.toString(isPagingPossible));
        }
    }
}
