package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries;

import javax.inject.Inject;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.AtomManipulator;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EntryGuidManipulator implements AtomManipulator {
    private static final String ID_FIELD_NAME = "id";

    @Inject
    private AtomModelMappingHelper atomModelMappingHelper;


    @Override
    public void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate) {
        if (modelToMap instanceof FeedEntryModel) {
            FeedEntryModel feedEntryModel = (FeedEntryModel) modelToMap;
            if (StringUtils.isNotBlank(feedEntryModel.getId())) {
                atomModelMappingHelper.addIdType(feedEntryModel.getId(), ID_FIELD_NAME, attributeObjectsToManipulate);
            }
        }
    }
}
