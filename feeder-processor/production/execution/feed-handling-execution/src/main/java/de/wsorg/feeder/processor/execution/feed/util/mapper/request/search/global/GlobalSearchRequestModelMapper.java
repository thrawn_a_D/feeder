package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global;

import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common.CommonGlobalSearchRequestMapper;
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "globalSearchRequestModelMapper")
public class GlobalSearchRequestModelMapper implements MapBasedRequestModelMapper<GlobalSearchQueryRequest> {

    @Inject
    private CommonGlobalSearchRequestMapper commonGlobalSearchRequestMapper;

    public GlobalSearchQueryRequest getRequestModel(final Map<String, String> requestModel) {
        return commonGlobalSearchRequestMapper.mapJaxbToModel(requestModel, GlobalSearchQueryRequest.class);
    }
}
