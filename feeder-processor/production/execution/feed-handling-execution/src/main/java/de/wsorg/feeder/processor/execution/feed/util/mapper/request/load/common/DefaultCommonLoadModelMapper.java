package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common;

import de.wsorg.feeder.processor.production.domain.feeder.PagingRange;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

import javax.inject.Named;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class DefaultCommonLoadModelMapper implements CommonLoadModelMapper {

    private static final String FEED_URL = "feedUrl";
    private static final String FEED_ENTRY_PAGE_RANGE_START_INDEX = "pageRangeStartIndex";
    private static final String FEED_ENTRY_PAGE_RANGE_END_INDEX = "pageRangeEndIndex";

    @Override
    public <T extends LoadFeed> T mapJaxbToModel(final Map<String, String> modelToMap, final Class<T> objectToMapTo) {
        T loadFeed = getModelInstance(modelToMap, objectToMapTo);

        mapPageRange(modelToMap, loadFeed);

        return loadFeed;
    }

    private <T extends LoadFeed> T getModelInstance(final Map<String, String> modelToMap, final Class<T> objectToMapTo) {
        T loadFeed = createDestinationObject(objectToMapTo);

        mapFeedUrl(modelToMap, loadFeed);

        return loadFeed;
    }

    private <T extends LoadFeed> void mapFeedUrl(final Map<String, String> modelToMap, final T loadFeed) {
        final String feedUrl = modelToMap.get(FEED_URL);
        loadFeed.setFeedUrl(feedUrl);
    }

    private <T extends LoadFeed> T createDestinationObject(final Class<T> objectToMapTo) {
        T loadFeed = null;
        try {
            loadFeed = (T) objectToMapTo.getConstructors()[0].newInstance(new Object[0]);
        } catch (InstantiationException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IllegalAccessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (InvocationTargetException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return loadFeed;
    }

    private void mapPageRange(final Map<String, String> requestModel, final LoadFeed mappedFeedRequest) {
        if (requestModel.containsKey(FEED_ENTRY_PAGE_RANGE_START_INDEX) &&
             requestModel.containsKey(FEED_ENTRY_PAGE_RANGE_END_INDEX)) {
            final String pageRangeStart = requestModel.get(FEED_ENTRY_PAGE_RANGE_START_INDEX);
            final String pageRangeEnd = requestModel.get(FEED_ENTRY_PAGE_RANGE_END_INDEX);

            final PagingRange mappedRange = new PagingRange();
            mappedRange.setStartIndex(Integer.valueOf(pageRangeStart));
            mappedRange.setEndIndex(Integer.valueOf(pageRangeEnd));

            mappedFeedRequest.setFeedEntryPagingRange(mappedRange);
        }
    }
}
