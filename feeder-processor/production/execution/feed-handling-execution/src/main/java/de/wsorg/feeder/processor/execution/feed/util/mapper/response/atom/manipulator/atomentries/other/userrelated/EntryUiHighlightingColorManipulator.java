package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import javax.xml.namespace.QName;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EntryUiHighlightingColorManipulator implements UserRelatedAtomEntryOtherManipulator {
    private static final String METADATA_UI_FEED_COLOR="FEED_UI_HIGHLIGHTING_COLOR";

    @Override
    public void manipulate(final FeedEntryModel feedEntryModel, final UserRelatedFeedModel modelToMap, final Map<QName, String> otherAttributesOfAtom) {
        String feedEntryColor = modelToMap.getFeedEntriesUiHighlightingColor().get(feedEntryModel.getId());

        if (feedEntryColor != null) {
            otherAttributesOfAtom.put(new QName(METADATA_UI_FEED_COLOR), feedEntryColor);
        }
    }
}
