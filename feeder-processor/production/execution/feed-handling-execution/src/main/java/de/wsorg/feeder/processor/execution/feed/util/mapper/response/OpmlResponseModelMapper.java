package de.wsorg.feeder.processor.execution.feed.util.mapper.response;

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.Body;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.OPML;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.Outline;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "opmlResponseModelMapper")
public class OpmlResponseModelMapper implements ResponseModelMapper<List<FeedModel>> {

    private static final String JAXB_MODEL_PACKAGE = "de.wsorg.feeder.processor.usecases.domain.generated.opml";

    @Inject
    private OpmlMetadataBuilder opmlMetadataBuilder;
    @Inject
    private JaxbMarshaller marshaller;

    public String getResponseModel(final List<FeedModel> feedModelToResponse) {
        validateInputList(feedModelToResponse);

        final OPML opmlToMarshall = buildModelToMarshall(feedModelToResponse);
        return marshall(opmlToMarshall);
    }

    private void validateInputList(final List<FeedModel> feedModelToResponse) {
        for (FeedModel currentModel : feedModelToResponse){
            if (StringUtils.isBlank(currentModel.getTitle()))
                throw new IllegalArgumentException("One of the FeedModels has an empty text field. It is not allowed to leave it empty");
            else if(StringUtils.isBlank(currentModel.getFeedUrl()))
                throw new IllegalArgumentException("One of the FeedModels has an empty feedUrl field. It is not allowed to leave it empty");
        }
    }

    private OPML buildModelToMarshall(final List<FeedModel> feedModelToProcess) {
        OPML mappedModelResult = new OPML();
        Body mappedBodyResult = new Body();
        mappedModelResult.setBody(mappedBodyResult);

        for(final FeedModel currentFeedModel : feedModelToProcess) {
            Outline currentOutlineMappingResult = new Outline();

            currentOutlineMappingResult.setTitle(currentFeedModel.getTitle());
            setDescriptionFieldAdjustedWithMetadata(currentFeedModel, currentOutlineMappingResult);
            currentOutlineMappingResult.setXmlUrl(currentFeedModel.getFeedUrl());

            mappedBodyResult.getOutline().add(currentOutlineMappingResult);
        }

        return mappedModelResult;
    }

    private void setDescriptionFieldAdjustedWithMetadata(final FeedModel currentFeedModel, final Outline currentOutlineMappingResult) {
        String description = currentFeedModel.getDescription();

        final String metaData = opmlMetadataBuilder.getMetadata(currentFeedModel);
        description += metaData;

        currentOutlineMappingResult.setDescription(description);
    }

    private String marshall(final OPML opmlToMarshall) {
        marshaller.setJaxbGeneratedClassesPackage(JAXB_MODEL_PACKAGE);
        return marshaller.marshal(opmlToMarshall);
    }
}
