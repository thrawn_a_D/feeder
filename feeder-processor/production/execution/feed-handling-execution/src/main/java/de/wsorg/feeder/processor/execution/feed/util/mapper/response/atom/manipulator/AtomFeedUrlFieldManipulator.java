package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator;

import javax.inject.Inject;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomFeedUrlFieldManipulator implements AtomManipulator {
    private static final String ID_FIELD_NAME = "id";

    @Inject
    private AtomModelMappingHelper atomModelMappingHelper;


    @Override
    public void manipulate(final CommonFeedData modelToMap, final List<Object> attributeObjectsToManipulate) {
        if (StringUtils.isNotBlank(modelToMap.getFeedUrl())) {
            atomModelMappingHelper.addIdType(modelToMap.getFeedUrl(), ID_FIELD_NAME, attributeObjectsToManipulate);
        }
    }
}
