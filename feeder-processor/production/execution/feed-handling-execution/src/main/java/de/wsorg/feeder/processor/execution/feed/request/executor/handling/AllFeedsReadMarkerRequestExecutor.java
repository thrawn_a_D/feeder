package de.wsorg.feeder.processor.execution.feed.request.executor.handling;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;
import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling;
import de.wsorg.feeder.processor.usecases.feed.FeedProcessorFactory;
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all.AllFeedsReadMarker;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AllFeedsReadMarkerRequestExecutor extends XmlBasedRequestExecutorTemplate<GenericResponseResult, MarkAllFeedsReadStatusHandling> {

    private static final String JAXB_GENERATED_PACKAGE_NAME = "de.wsorg.feeder.processor.execution.domain.generated.request.handler";
    @Inject
    @Named(value = "allFeedsReadMarkerModelMapper")
    private RequestModelMapper requestModelMapper;

    private AllFeedsReadMarker allFeedsReadMarker;

    @Override
    protected RequestModelMapper getRequestModelMapper() {
        return requestModelMapper;
    }

    @Override
    protected GenericResponseResult executeConcreteRequest(final MarkAllFeedsReadStatusHandling executionParameter) {
        GenericResponseResult executionResult = new GenericResponseResult();

        try {
            getFeedModifier().markAllFeeds(executionParameter);
            executionResult.setProcessingResult(ProcessingResult.SUCCESS);
        } catch (Exception ex) {
            executionResult.setProcessingResult(ProcessingResult.FAILED);
            executionResult.setMessage(ex.getMessage());
        }


        return executionResult;
    }

    @Override
    protected String getJaxbGeneratedPackageName() {
        return JAXB_GENERATED_PACKAGE_NAME;
    }

    private AllFeedsReadMarker getFeedModifier(){
        if(allFeedsReadMarker == null){
            allFeedsReadMarker = FeedProcessorFactory.getAllFeedsReadMarker();
        }

        return allFeedsReadMarker;
    }
}
