package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler

import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkAllFeedsAsReadType
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.10.12
 */
class AllFeedsReadMarkerModelMapperTest extends Specification {
    AllFeedsReadMarkerModelMapper handleFeedModelMapper

    def setup(){
        handleFeedModelMapper = new AllFeedsReadMarkerModelMapper()
    }

    def "Map jaxb handle request to feed request"() {
        given:
        MarkAllFeedsAsReadType feedsAsReadType = new MarkAllFeedsAsReadType()
        feedsAsReadType.markFeedsAsReadType = true
        feedsAsReadType.category = 'cathegory'

        when:
        def mappedResult = handleFeedModelMapper.getRequestModel(feedsAsReadType)

        then:
        mappedResult.feedsReadStatus == feedsAsReadType.markFeedsAsReadType
        mappedResult.category == feedsAsReadType.category
    }
}
