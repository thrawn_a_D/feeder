package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.metadata

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 17.12.12
 * Time: 11:07
 */
class FeedColorMetadataBuilderTest extends Specification {

    FeedColorMetadataBuilder feedColorMetadataBuilder

    def setup(){
        feedColorMetadataBuilder = new FeedColorMetadataBuilder()
    }

    def "Extract read information out of feed data"() {
        given:
        def feedColor = '#ads23s'
        def feed = new UserRelatedFeedModel(new FeedModel(),
                                            new FeedToUserAssociation(feedUiHighlightingColor: feedColor))

        when:
        def result = feedColorMetadataBuilder.getMetadata(feed)

        then:
        result == "FEED_UI_HIGHLIGHTING_COLOR" + OpmlMetadataBuilder.META_TAG_SEPARATOR + feedColor
    }

    def "No color information can be extracted, return an empty string"() {
        given:
        def feed = new UserRelatedFeedModel(new FeedModel(),
                new FeedToUserAssociation(feedUiHighlightingColor: ''))

        when:
        def result = feedColorMetadataBuilder.getMetadata(feed)

        then:
        result == ''
    }
}
