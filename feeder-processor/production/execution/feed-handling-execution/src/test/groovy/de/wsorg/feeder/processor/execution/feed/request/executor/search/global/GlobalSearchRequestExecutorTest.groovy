package de.wsorg.feeder.processor.execution.feed.request.executor.search.global;


import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.GlobalSearchRequestModelMapper
import de.wsorg.feeder.processor.execution.util.mapper.request.JaxbMarshallAndMappingHelper
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class GlobalSearchRequestExecutorTest extends Specification {

    GlobalFeedFinder feedFinderMock
    GlobalSearchRequestExecutor searchRequestExecutor;
    GlobalSearchRequestModelMapper requestModelMapperMock
    JaxbMarshallAndMappingHelper jaxbMarshallAndMappingHelper
    MapBasedRequestModelMapper requestModelMapper

    def setup(){
        searchRequestExecutor = new GlobalSearchRequestExecutor();
        feedFinderMock = Mock(GlobalFeedFinder)
        requestModelMapperMock = Mock(GlobalSearchRequestModelMapper)
        jaxbMarshallAndMappingHelper = Mock(JaxbMarshallAndMappingHelper)
        searchRequestExecutor.globalFeedFinder=feedFinderMock
        searchRequestExecutor.searchRequestModelMapper = requestModelMapper
    }

    def "Perform search call using the request xml"() {
        given:
        final String searchTerm='suche eines feeds'

        and:"Testdata"
        GlobalSearchQueryRequest requestQuery = new GlobalSearchQueryRequest(searchTerm: searchTerm)
        def searchResult = [new FeedModel()]
        def expectedSearchQueryToUse = new GlobalSearchQuery(searchTerm: searchTerm)

        when:
        def result = searchRequestExecutor.executeConcreteRequest(requestQuery)

        then:
        1 * feedFinderMock.findFeeds({GlobalSearchQuery it ->
                                      it.searchTerm == expectedSearchQueryToUse.searchTerm}) >> searchResult;

        result != null
        result==searchResult
    }
}
