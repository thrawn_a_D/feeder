package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common

import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.01.13
 */
class DefaultCommonLoadModelMapperTest extends Specification {
    DefaultCommonLoadModelMapper defaultCommonLoadModelMapper

    def setup(){
        defaultCommonLoadModelMapper = new DefaultCommonLoadModelMapper()
    }

    def "Map a loader request model"() {
        given:
        Map<String, String> loadFeed = new HashMap<String, String>()
        loadFeed.put('feedUrl', "http://feedurl.com/bla")

        when:
        LoadFeedRequest result = defaultCommonLoadModelMapper.mapJaxbToModel(loadFeed, LoadFeedRequest)

        then:
        result != null
        result.feedUrl == loadFeed.feedUrl
    }

    def "No page range provided so expect default settings from 1 to Integer.Max"() {
        given:
        Map<String, String> loadFeed = new HashMap<String, String>()
        loadFeed.put('feedUrl', "http://feedurl.com/bla")

        when:
        LoadFeedRequest result = defaultCommonLoadModelMapper.mapJaxbToModel(loadFeed, LoadFeedRequest)

        then:
        result != null
        result.feedEntryPagingRange.startIndex == 1
        result.feedEntryPagingRange.endIndex == Integer.MAX_VALUE
    }

    def "Provided page range is mapped correctly"() {
        given:
        Map<String, String> loadFeed = new HashMap<String, String>()
        loadFeed.put('feedUrl', "http://feedurl.com/bla")
        loadFeed.put('pageRangeStartIndex', '3')
        loadFeed.put('pageRangeEndIndex', '11')

        when:
        LoadFeedRequest result = defaultCommonLoadModelMapper.mapJaxbToModel(loadFeed, LoadFeedRequest)

        then:
        result != null
        result.feedEntryPagingRange.startIndex == 3
        result.feedEntryPagingRange.endIndex == 11
    }
}
