package de.wsorg.feeder.processor.execution.feed.util.mapper.response;


import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.AtomModelMapper
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.AtomManipulator
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.general.GenericAtomEntryOtherManipulator
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated.UserRelatedAtomEntryOtherManipulator
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues.OtherValuesManipulator
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.EntryType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.FeedType
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class AtomModelMapperTest extends Specification {

    private static final String EXPECTED_JAXB_PACKAGE_NAME = "de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom"
    AtomModelMapper atomResponseModelMapper
    JaxbMarshaller jaxbMarshallerMock
    AtomModelMappingHelper atomModelMappingHelper

    List<AtomManipulator> headerFieldManipulators = []
    List<OtherValuesManipulator> otherValuesHeaderFieldManipulators = []
    List<AtomManipulator> feedEntryManipulators = []
    List<UserRelatedAtomEntryOtherManipulator> userRelatedAtomEntryOtherManipulators = []
    List<GenericAtomEntryOtherManipulator> genericAtomEntryOtherManipulators = []

    def setup(){
        atomResponseModelMapper = new AtomModelMapper()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        jaxbMarshallerMock = Mock(JaxbMarshaller)
        atomResponseModelMapper.jaxbMarshaller = jaxbMarshallerMock
        atomResponseModelMapper.atomModelMappingHelper = atomModelMappingHelper

        headerFieldManipulators << Mock(AtomManipulator)
        otherValuesHeaderFieldManipulators << Mock(OtherValuesManipulator)
        feedEntryManipulators << Mock(AtomManipulator)
        userRelatedAtomEntryOtherManipulators << Mock(UserRelatedAtomEntryOtherManipulator)
        genericAtomEntryOtherManipulators << Mock(GenericAtomEntryOtherManipulator)

        atomResponseModelMapper.headerFieldManipulators = headerFieldManipulators
        atomResponseModelMapper.otherValuesHeaderFieldManipulators = otherValuesHeaderFieldManipulators
        atomResponseModelMapper.feedEntryManipulators = feedEntryManipulators
        atomResponseModelMapper.userRelatedAtomEntryOtherManipulators = userRelatedAtomEntryOtherManipulators
        atomResponseModelMapper.genericAtomEntryOtherManipulators = genericAtomEntryOtherManipulators
    }

    def "Make sure feed header entries are build"() {
        given:
        def feedModel = new FeedModel(title: 'title',
                                      description: 'asd',
                                      feedUrl: 'asd',
                                      logoUrl: 'asd23sd')

        when:
        atomResponseModelMapper.getResponseModel(feedModel)

        then:
        1 * headerFieldManipulators[0].manipulate(feedModel, {it != null})
        1 * otherValuesHeaderFieldManipulators[0].manipulate(feedModel, {it != null})
    }

    def "Make sure feed entries are set as they should be"() {
        given:
        def feedModel = new UserRelatedFeedModel()
        feedModel.feedEntryModels << new FeedEntryModel()

        when:
        atomResponseModelMapper.getResponseModel(feedModel)

        then:
        1 * feedEntryManipulators[0].manipulate(feedModel.feedEntryModels[0], {it != null})
        1 * userRelatedAtomEntryOtherManipulators[0].manipulate(feedModel.feedEntryModels[0], feedModel, {it != null})
        1 * atomModelMappingHelper.addElementToFeedType({it != null}, "entry", {EntryType it -> it != null}, EntryType)
        1 * genericAtomEntryOtherManipulators[0].manipulate(feedModel.feedEntryModels[0], {it != null})
    }

    def "Make sure feed entries but no user specific values are added"() {
        given:
        def feedModel = new FeedModel()
        feedModel.feedEntryModels << new FeedEntryModel()


        when:
        atomResponseModelMapper.getResponseModel(feedModel)

        then:
        1 * feedEntryManipulators[0].manipulate(feedModel.feedEntryModels[0], {it != null})
        0 * userRelatedAtomEntryOtherManipulators[0].manipulate(feedModel.feedEntryModels[0], feedModel, {it != null})
    }

    def "Make sure elements are wrapped in a JaxbElement"() {
        given:
        def feedModel = new FeedModel(title: 'title',
                description: 'asd',
                feedUrl: 'asd',
                logoUrl: 'asd23sd')

        and:
        def expectedMarshalResult = ''

        when:
        def result = atomResponseModelMapper.getResponseModel(feedModel)

        then:
        result == expectedMarshalResult
        1 * jaxbMarshallerMock.marshal({
            it instanceof JAXBElement
            def jaxbElement = it as JAXBElement
            def feedType = jaxbElement.value as FeedType
            jaxbElement.name.localPart == "feed"
            jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
            feedType != null }) >> expectedMarshalResult
    }

    def "Make sure jaxb package name is set properly"() {
        given:
        def feedModel = new FeedModel(title: 'title',
                description: 'asd',
                feedUrl: 'asd',
                logoUrl: 'asd23sd')

        when:
        atomResponseModelMapper.getResponseModel(feedModel)

        then:
        1 * jaxbMarshallerMock.setJaxbGeneratedClassesPackage(EXPECTED_JAXB_PACKAGE_NAME)
    }
}
