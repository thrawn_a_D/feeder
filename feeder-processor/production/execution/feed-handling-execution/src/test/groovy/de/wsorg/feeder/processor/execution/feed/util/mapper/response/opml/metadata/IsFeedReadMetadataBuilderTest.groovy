package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.metadata

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.12.12
 */
class IsFeedReadMetadataBuilderTest extends Specification {

    IsFeedReadMetadataBuilder isFeedReaderMetadataBuilder

    def setup(){
        isFeedReaderMetadataBuilder = new IsFeedReadMetadataBuilder()
    }

    def "Extract read information out of feed data"() {
        given:
        def feed = new UserRelatedFeedModel()

        when:
        def result = isFeedReaderMetadataBuilder.getMetadata(feed)

        then:
        result == "IS_FAVOURED" + OpmlMetadataBuilder.META_TAG_SEPARATOR + 'true'
    }

    def "No read information can be extracted, return an empty string"() {
        given:
        def feed = new FeedModel()

        when:
        def result = isFeedReaderMetadataBuilder.getMetadata(feed)

        then:
        result == ''
    }
}
