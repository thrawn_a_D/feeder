package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class EntryDescriptionFieldManipulatorTest extends Specification {
    EntryDescriptionFieldManipulator atomDescriptionFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomDescriptionFieldManipulator = new EntryDescriptionFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomDescriptionFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add description to the list of entries"() {
        given:
        def feedModel = new FeedModel(description: 'feedModel')
        def listOfAttributes = []

        when:
        atomDescriptionFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addTextType(feedModel.description, "summary", listOfAttributes)
    }

    def "Make sure nothing was actually done when description is empty or null"() {
        given:
        def feedModel = new FeedModel(description: '')
        def listOfAttributes = []

        when:
        atomDescriptionFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addTextType(feedModel.description, "summary", listOfAttributes)
    }
}
