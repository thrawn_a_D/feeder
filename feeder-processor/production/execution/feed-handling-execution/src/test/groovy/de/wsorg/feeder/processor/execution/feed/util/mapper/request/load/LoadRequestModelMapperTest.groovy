package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load;


import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common.CommonLoadModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class LoadRequestModelMapperTest extends Specification {

    LoadRequestModelMapper loadRequestModelMapper
    CommonLoadModelMapper commonLoadModelMapper

    def setup(){
        loadRequestModelMapper = new LoadRequestModelMapper()
        commonLoadModelMapper = Mock(CommonLoadModelMapper)
        loadRequestModelMapper.commonLoadModelMapper = commonLoadModelMapper
    }

    def "Map a loader request model"() {
        given:
        def loadFeedParam = new HashMap<String, String>()

        and:
        def expectedResult = Mock(LoadFeedRequest)

        when:
        LoadFeedRequest result = loadRequestModelMapper.getRequestModel(loadFeedParam)

        then:
        result == expectedResult
        1 * commonLoadModelMapper.mapJaxbToModel(loadFeedParam, LoadFeed) >> expectedResult
    }
}
