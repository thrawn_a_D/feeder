package de.wsorg.feeder.processor.execution.feed.request.executor.handling

import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all.AllFeedsReadMarker
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.10.12
 */
class AllFeedsReadMarkerRequestExecutorTest extends Specification {

    AllFeedsReadMarkerRequestExecutor handleFeedRequestExecutor
    AllFeedsReadMarker allFeedsReadMarker;

    def setup(){
        handleFeedRequestExecutor = new AllFeedsReadMarkerRequestExecutor()
        allFeedsReadMarker = Mock(AllFeedsReadMarker)
        handleFeedRequestExecutor.allFeedsReadMarker = allFeedsReadMarker
    }

    def "Check if proper mapper is provided"() {
        given:
        RequestModelMapper handlerModelMapper = Mock(RequestModelMapper)
        handleFeedRequestExecutor.requestModelMapper = handlerModelMapper

        when:
        def usedMapper = handleFeedRequestExecutor.getRequestModelMapper()

        then:
        usedMapper == handlerModelMapper
    }

    def "Make sure a proper jaxb package is provided"() {
        given:
        def packageName="de.wsorg.feeder.processor.execution.domain.generated.request.handler"

        when:
        def usedPackageName = handleFeedRequestExecutor.getJaxbGeneratedPackageName()

        then:
        usedPackageName == packageName
    }

    def "Ensure feed modifier is executet properly"() {
        given:
        MarkAllFeedsReadStatusHandling givenModificationRequest = new MarkAllFeedsReadStatusHandling(
                userId: '234876',
                feedsReadStatus: true,
                category: 'asd')

        when:
        handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        1 * allFeedsReadMarker.markAllFeeds({it-> it.userId &&
                                                    it.feedsReadStatus &&
                                                    it.category == givenModificationRequest.category})

    }

    def "Validate result in case of valid request"() {
        given:
        MarkAllFeedsReadStatusHandling givenModificationRequest = new MarkAllFeedsReadStatusHandling(userId: '234876',
                                                                                                      feedsReadStatus: true,
                                                                                                      category: 'asd')


        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
    }

    def "Validate result in case of a failed execution"() {
        given:
        MarkAllFeedsReadStatusHandling givenModificationRequest = new MarkAllFeedsReadStatusHandling(userId: '234876',
                                                                                                     feedsReadStatus: true,
                                                                                                     category: 'asd')


        and:
        def exceptionMessage = "Exception message"
        allFeedsReadMarker.markAllFeeds(_) >> {throw new NullPointerException(exceptionMessage)}

        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.FAILED
        result.message == exceptionMessage
    }
}
