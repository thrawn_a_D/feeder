package de.wsorg.feeder.processor.execution.feed.request;


import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.feed.request.executor.handling.AllFeedsReadMarkerRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.handling.FeedEntryReadMarkerRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.handling.FeedSubscriptionRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.load.LoadRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.load.UserRelatedLoadRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.search.UserRelatedSearchRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.search.global.GlobalSearchRequestExecutor
import de.wsorg.feeder.processor.execution.feed.request.executor.search.global.GlobalSearchWithUserDataRequestExecutor
import de.wsorg.feeder.processor.execution.request.ExecutorResolver
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class FeedExecutorResolverTest extends Specification {

    ExecutorResolver executorResolver;
    GlobalSearchRequestExecutor searchRequestExecutorMock
    GlobalSearchWithUserDataRequestExecutor searchRequestWithUserDataExecutorMock
    UserRelatedSearchRequestExecutor userRelatedSearchRequestExecutor
    LoadRequestExecutor loadRequestExecutorMock;
    UserRelatedLoadRequestExecutor userRelatedLoadRequestExecutor;
    FeedSubscriptionRequestExecutor subscriptionRequestExecutor;
    FeedEntryReadMarkerRequestExecutor feedEntryReadMarkerRequestExecutor;
    AllFeedsReadMarkerRequestExecutor allFeedsReadMarkerRequestExecutor

    def setup(){
        executorResolver = new FeedExecutorResolver();
        searchRequestExecutorMock = Mock(GlobalSearchRequestExecutor)
        searchRequestWithUserDataExecutorMock = Mock(GlobalSearchWithUserDataRequestExecutor)
        loadRequestExecutorMock = Mock(LoadRequestExecutor)
        userRelatedLoadRequestExecutor = Mock(UserRelatedLoadRequestExecutor)
        subscriptionRequestExecutor = Mock(FeedSubscriptionRequestExecutor)
        userRelatedSearchRequestExecutor = Mock(UserRelatedSearchRequestExecutor)
        feedEntryReadMarkerRequestExecutor = Mock(FeedEntryReadMarkerRequestExecutor)
        allFeedsReadMarkerRequestExecutor = Mock(AllFeedsReadMarkerRequestExecutor)
        executorResolver.searchRequestExecutor = searchRequestExecutorMock;
        executorResolver.searchRequestWithUSerDataExecutor = searchRequestWithUserDataExecutorMock;
        executorResolver.loadRequestExecutor = loadRequestExecutorMock
        executorResolver.userRelatedLoadRequestExecutor = userRelatedLoadRequestExecutor
        executorResolver.subscriptionRequestExecutor = subscriptionRequestExecutor
        executorResolver.userRelatedSearchRequestExecutor = userRelatedSearchRequestExecutor
        executorResolver.feedEntryReadMarkerRequestExecutor = feedEntryReadMarkerRequestExecutor
        executorResolver.allFeedsReadMarkerRequestExecutor = allFeedsReadMarkerRequestExecutor
    }

    def "Get an executor for the global search command"() {
        given:
        final command=FeederCommand.GLOBAL_SEARCH

        when:
        def executor=executorResolver.resolveExecutor(command);

        then:
        executor instanceof GlobalSearchRequestExecutor
        executor == searchRequestExecutorMock
    }

    def "Get an executor for the global search with user data command"() {
        given:
        final command=FeederCommand.GLOBAL_SEARCH_ENRICHED_WITH_USER_DATA

        when:
        def executor=executorResolver.resolveExecutor(command);

        then:
        executor instanceof GlobalSearchWithUserDataRequestExecutor
        executor == searchRequestWithUserDataExecutorMock
    }

    def "Get an executor for the user related search command"() {
        given:
        final command=FeederCommand.USER_RELATED_SEARCH

        when:
        def executor=executorResolver.resolveExecutor(command);

        then:
        executor instanceof UserRelatedSearchRequestExecutor
        executor == userRelatedSearchRequestExecutor
    }

    def "Get an executor for the load command"() {
        given:
        final command=FeederCommand.LOAD

        when:
        def executor = executorResolver.resolveExecutor(command)

        then:
        executor != null
        executor == loadRequestExecutorMock

    }

    def "Get an executor for the user related load command"() {
        given:
        final command=FeederCommand.LOAD_USER_RELATED

        when:
        def executor = executorResolver.resolveExecutor(command)

        then:
        executor != null
        executor == userRelatedLoadRequestExecutor

    }

    def "Get a request for the subscription executor"() {
        given:
        final command=FeederCommand.FAVOUR_FEED

        when:
        def executor = executorResolver.resolveExecutor(command)

        then:
        executor == subscriptionRequestExecutor
    }

    def "Get a request for the feed entry read marker executor"() {
        given:
        final command=FeederCommand.MARK_FEED_ENTRY_READABILITY

        when:
        def executor = executorResolver.resolveExecutor(command)

        then:
        executor == feedEntryReadMarkerRequestExecutor
    }

    def "Get a request for the feeds read marker executor"() {
        given:
        final command=FeederCommand.MARK_ALL_FEEDS_READABILITY

        when:
        def executor = executorResolver.resolveExecutor(command)

        then:
        executor == allFeedsReadMarkerRequestExecutor
    }
}
