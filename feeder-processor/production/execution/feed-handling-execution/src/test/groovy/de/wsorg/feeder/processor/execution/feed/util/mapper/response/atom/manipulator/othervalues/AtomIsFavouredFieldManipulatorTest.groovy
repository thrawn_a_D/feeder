package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomIsFavouredFieldManipulatorTest extends Specification {
    AtomIsFavouredFieldManipulator atomIsFavouredFieldManipulator

    def setup(){
        atomIsFavouredFieldManipulator = new AtomIsFavouredFieldManipulator()
    }

    def "Add is favoured property to the list of other attributes"() {
        given:
        def feedModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomIsFavouredFieldManipulator.manipulate(feedModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_FAVOURED"
        otherAttributesOfAtom.values().iterator().next() == "true"
    }

    def "Add false as favoured property when feed is non user related"() {
        given:
        def feedModel = new FeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomIsFavouredFieldManipulator.manipulate(feedModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_FAVOURED"
        otherAttributesOfAtom.values().iterator().next() == "false"
    }
}
