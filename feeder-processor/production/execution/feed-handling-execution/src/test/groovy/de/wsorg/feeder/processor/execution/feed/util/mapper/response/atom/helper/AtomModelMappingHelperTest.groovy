package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.DateTimeType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.IdType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LogoType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.TextType
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.12.12
 */
class AtomModelMappingHelperTest extends Specification {
    AtomModelMappingHelper abstractAtomModelMapper

    def setup(){
        abstractAtomModelMapper = new AtomModelMappingHelper()
    }

    def "Add Id attribute to a list of objects"() {
        given:
        def id = 'ioh'
        def listToManipulate = []
        def attributeName = 'name'

        when:
        abstractAtomModelMapper.addIdType(id, attributeName, listToManipulate)

        then:
        listToManipulate.size() > 0
        listToManipulate[0] instanceof JAXBElement
        def jaxbElement = listToManipulate[0] as JAXBElement
        jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
        jaxbElement.name.localPart == attributeName
        jaxbElement.declaredType == IdType
        jaxbElement.value instanceof IdType
        def value = jaxbElement.value as IdType
        value.value == id
    }

    def "Add text attribute to a list of entries"() {
        given:
        def text = 'ioh'
        def listToManipulate = []
        def attributeName = 'name'

        when:
        abstractAtomModelMapper.addTextType(text, attributeName, listToManipulate)

        then:
        listToManipulate.size() > 0
        listToManipulate[0] instanceof JAXBElement
        def jaxbElement = listToManipulate[0] as JAXBElement
        jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
        jaxbElement.name.localPart == attributeName
        jaxbElement.declaredType == TextType
        jaxbElement.value instanceof TextType
        def value = jaxbElement.value as TextType
        value.content[0] == text
    }

    def "Add date time attribute to a list of entries"() {
        given:
        def date = new Date()
        def listToManipulate = []
        def attributeName = 'name'

        when:
        abstractAtomModelMapper.addDateTimeType(date, attributeName, listToManipulate)

        then:
        listToManipulate.size() > 0
        listToManipulate[0] instanceof JAXBElement
        def jaxbElement = listToManipulate[0] as JAXBElement
        jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
        jaxbElement.name.localPart == attributeName
        jaxbElement.declaredType == DateTimeType
        jaxbElement.value instanceof DateTimeType
        def value = jaxbElement.value as DateTimeType
        value.value.toGregorianCalendar().getTime().equals(date)
    }

    def "Add logo to a list of attributes"() {
        given:
        def logo = 'ioh'
        def listToManipulate = []
        def attributeName = 'logos'

        when:
        abstractAtomModelMapper.addLogoType(logo, attributeName, listToManipulate)

        then:
        listToManipulate.size() > 0
        listToManipulate[0] instanceof JAXBElement
        def jaxbElement = listToManipulate[0] as JAXBElement
        jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
        jaxbElement.name.localPart == attributeName
        jaxbElement.declaredType == LogoType
        jaxbElement.value instanceof LogoType
        def value = jaxbElement.value as LogoType
        value.base == logo
    }

    def "Add link type"() {
        given:
        def href = 'ioh'
        def type = new LinkType('image/jpeg')
        def relation = LinkRelation.CANONICAL
        def linkType = new Link(linkType: type, href: href, relation: relation)
        def listToManipulate = []

        when:
        abstractAtomModelMapper.addLinkType(linkType, listToManipulate)

        then:
        listToManipulate.size() > 0
        listToManipulate[0] instanceof JAXBElement
        def jaxbElement = listToManipulate[0] as JAXBElement
        jaxbElement.name.namespaceURI == AtomModelMappingHelper.ATOM_NAMESPACE
        jaxbElement.name.localPart == 'link'
        jaxbElement.declaredType == de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType
        jaxbElement.value instanceof de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType
        def value = jaxbElement.value as de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType
        value.href == href
        value.rel == relation.linkRfcCode
        value.type == type.linkType
    }
}
