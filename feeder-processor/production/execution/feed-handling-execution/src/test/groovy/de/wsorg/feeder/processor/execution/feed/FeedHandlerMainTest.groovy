package de.wsorg.feeder.processor.execution.feed;


import de.wsorg.feeder.processor.execution.FeederRequestResponseHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.09.12
 */
public class FeedHandlerMainTest extends Specification {
    FeedHandlerMain feedHandlerMain
    FeederRequestResponseHandler feederExecutor;

    def setup(){
        feedHandlerMain = new FeedHandlerMain()
        feederExecutor = Mock(FeederRequestResponseHandler)
        feedHandlerMain.feederRequestResponseHandler = feederExecutor
    }

    def "Execute Main"() {
        given:
        FeedHandlerMain feedHandlerMainMock = Mock(FeedHandlerMain)
        FeedHandlerMain.feedHandlerMain = feedHandlerMainMock

        and:
        def givenArgs = ""

        when:
        FeedHandlerMain.main(givenArgs);

        then:
        1 * feedHandlerMainMock.processRequest(givenArgs)
    }

    def "Check if right request executor"() {
        when:
        def result = feedHandlerMain.getFeederRequestResponseHandler()

        then:
        result != null
    }
}
