package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 22.01.13
 */
class AtomDescriptionFieldManipulatorTest extends Specification {
    AtomDescriptionFieldManipulator atomDescriptionFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomDescriptionFieldManipulator = new AtomDescriptionFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomDescriptionFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add title to the list of entries"() {
        given:
        def feedModel = new FeedModel(description: 'feedModel')
        def listOfAttributes = []

        when:
        atomDescriptionFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addTextType(feedModel.description, "subtitle", listOfAttributes)
    }

    def "Make sure nothing was actually done when title is empty or null"() {
        given:
        def feedModel = new FeedModel(description: '')
        def listOfAttributes = []

        when:
        atomDescriptionFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addTextType(feedModel.title, "subtitle", listOfAttributes)
    }
}
