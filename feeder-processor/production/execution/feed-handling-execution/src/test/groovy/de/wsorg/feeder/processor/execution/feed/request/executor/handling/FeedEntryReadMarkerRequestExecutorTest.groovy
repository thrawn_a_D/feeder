package de.wsorg.feeder.processor.execution.feed.request.executor.handling

import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry.FeedEntryReadMarker
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.10.12
 */
class FeedEntryReadMarkerRequestExecutorTest extends Specification {

    FeedEntryReadMarkerRequestExecutor handleFeedRequestExecutor
    FeedEntryReadMarker feedEntryReadMarker;

    def setup(){
        handleFeedRequestExecutor = new FeedEntryReadMarkerRequestExecutor()
        feedEntryReadMarker = Mock(FeedEntryReadMarker)
        handleFeedRequestExecutor.feedEntryReadMarker = feedEntryReadMarker
    }

    def "Check if proper mapper is provided"() {
        given:
        RequestModelMapper handlerModelMapper = Mock(RequestModelMapper)
        handleFeedRequestExecutor.requestModelMapper = handlerModelMapper

        when:
        def usedMapper = handleFeedRequestExecutor.getRequestModelMapper()

        then:
        usedMapper == handlerModelMapper
    }

    def "Make sure a proper jaxb package is provided"() {
        given:
        def packageName="de.wsorg.feeder.processor.execution.domain.generated.request.handler"

        when:
        def usedPackageName = handleFeedRequestExecutor.getJaxbGeneratedPackageName()

        then:
        usedPackageName == packageName
    }

    def "Ensure feed modifier is executet properly"() {
        given:
        MarkFeedEntryReadabilityRequest givenModificationRequest = new MarkFeedEntryReadabilityRequest(
                userId: '234876',
                feedUrl: 'bla',
                feedEntryUidWithReadabilityStatus: ['asd': false])

        when:
        handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        1 * feedEntryReadMarker.markFeedEntries({it-> it.userId &&
                                                    it.feedUrl &&
                                                    it.feedEntryUidWithReadabilityStatus})

    }

    def "Validate result in case of valid request"() {
        given:
        MarkFeedEntryReadabilityRequest givenModificationRequest = new MarkFeedEntryReadabilityRequest(
                                                                            userId: '234876',
                                                                            feedUrl: 'bla',
                                                                            feedEntryUidWithReadabilityStatus: ['asd': false])


        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
    }

    def "Validate result in case of a failed execution"() {
        given:
        MarkFeedEntryReadabilityRequest givenModificationRequest = new MarkFeedEntryReadabilityRequest(
                                                                            userId: '234876',
                                                                            feedUrl: 'bla',
                                                                            feedEntryUidWithReadabilityStatus: ['asd': false])


        and:
        def exceptionMessage = "Exception message"
        feedEntryReadMarker.markFeedEntries(_) >> {throw new NullPointerException(exceptionMessage)}

        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.FAILED
        result.message == exceptionMessage
    }
}
