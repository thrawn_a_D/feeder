package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomTitleFieldManipulatorTest extends Specification {
    AtomTitleFieldManipulator atomTitleFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomTitleFieldManipulator = new AtomTitleFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomTitleFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add title to the list of entries"() {
        given:
        def feedModel = new FeedModel(title: 'feedModel')
        def listOfAttributes = []

        when:
        atomTitleFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addTextType(feedModel.title, "title", listOfAttributes)
    }

    def "Make sure nothing was actually done when title is empty or null"() {
        given:
        def feedModel = new FeedModel(title: '')
        def listOfAttributes = []

        when:
        atomTitleFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addTextType(feedModel.title, "title", listOfAttributes)
    }
}
