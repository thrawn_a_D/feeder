package de.wsorg.feeder.processor.execution.feed.request.executor.load

import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.01.13
 */
class UserRelatedLoadRequestExecutorTest extends Specification {
    UserRelatedLoadRequestExecutor userRelatedLoadRequestExecutor

    def setup(){
        userRelatedLoadRequestExecutor = new UserRelatedLoadRequestExecutor()
    }

    def "Get the right model mapper"() {
        given:
        def modelMapper = Mock(MapBasedRequestModelMapper)
        userRelatedLoadRequestExecutor.loadRequestModelMapper = modelMapper

        when:
        def result = userRelatedLoadRequestExecutor.getRequestModelMapper()

        then:
        result == modelMapper
    }
}
