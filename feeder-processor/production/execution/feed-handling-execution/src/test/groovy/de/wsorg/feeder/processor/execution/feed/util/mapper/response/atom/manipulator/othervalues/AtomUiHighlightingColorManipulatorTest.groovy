package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomUiHighlightingColorManipulatorTest extends Specification {
    AtomUiHighlightingColorManipulator atomUiHighlightingColorManipulator

    def setup(){
        atomUiHighlightingColorManipulator = new AtomUiHighlightingColorManipulator()
    }

    def "Set highlighting color based on user specific feed entry"() {
        given:
        def feed = new UserRelatedFeedModel()
        feed.feedToUserAssociation.setFeedUiHighlightingColor('#98zkub')

        and:
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomUiHighlightingColorManipulator.manipulate(feed, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.keySet().iterator().next().localPart == "FEED_UI_HIGHLIGHTING_COLOR"
        otherAttributesOfAtom.values().iterator().next() == feed.getFeedUiHighlightingColor()
    }

    def "No coloring is set"() {
        given:
        def feed = new UserRelatedFeedModel()
        feed.feedToUserAssociation.setFeedUiHighlightingColor('')

        and:
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomUiHighlightingColorManipulator.manipulate(feed, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.isEmpty()
    }

    def "No user related feed is given, make no entries"() {
        given:
        def feed = new FeedModel()
        and:
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomUiHighlightingColorManipulator.manipulate(feed, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.isEmpty()
    }
}
