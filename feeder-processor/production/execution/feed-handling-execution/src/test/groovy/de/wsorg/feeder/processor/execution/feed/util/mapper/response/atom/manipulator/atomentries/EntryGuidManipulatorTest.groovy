package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class EntryGuidManipulatorTest extends Specification {
    EntryGuidManipulator entryGuidManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        entryGuidManipulator = new EntryGuidManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        entryGuidManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add guid to the list of attributes#"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'kjh')
        def listOfAttributes = []


        when:
        entryGuidManipulator.manipulate(feedEntry, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addIdType(feedEntry.id, "id", listOfAttributes)
    }

    def "No FeedEntry is provided but something else"() {
        given:
        def feedEntry = new FeedModel()
        def listOfAttributes = []


        when:
        entryGuidManipulator.manipulate(feedEntry, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addIdType(_, "id", listOfAttributes)
    }
}
