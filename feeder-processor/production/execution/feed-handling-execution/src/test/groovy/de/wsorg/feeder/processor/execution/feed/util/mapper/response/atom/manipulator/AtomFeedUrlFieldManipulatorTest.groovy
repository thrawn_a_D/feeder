package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomFeedUrlFieldManipulatorTest extends Specification {
    AtomFeedUrlFieldManipulator atomFeedUrlFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomFeedUrlFieldManipulator = new AtomFeedUrlFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomFeedUrlFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add feed url to the list of entries"() {
        given:
        def feedModel = new FeedModel(feedUrl: 'http://sdf')
        def listOfAttributes = []

        when:
        atomFeedUrlFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addIdType(feedModel.feedUrl, "id", listOfAttributes)
    }

    def "Make sure nothing was actually done when feed url is empty or null"() {
        given:
        def feedModel = new FeedModel(feedUrl: '')
        def listOfAttributes = []

        when:
        atomFeedUrlFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addIdType(feedModel.feedUrl, "id", listOfAttributes)
    }
}
