package de.wsorg.feeder.processor.execution.feed.response.processor;


import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.07.12
 */
public class OpmlResponseProcessorTest extends Specification {

    private static String DEFAULT_TEMP_FOLDER;

    static {
        DEFAULT_TEMP_FOLDER=System.getProperty("java.io.tmpdir") + File.separator + "response.xml"
    }

    OpmlResponseProcessor fileBasedResponse;
    ResponseModelMapper responseModelMapper

    def setup(){
        fileBasedResponse=new OpmlResponseProcessor();
        responseModelMapper = Mock(ResponseModelMapper)
        fileBasedResponse.opmlResponseModelMapper = responseModelMapper
    }

    def "Provide an unsupported domain model and expect an exception"() {
        given:
        final HashMap someInvalidObject = new HashMap()

        when:
        fileBasedResponse.validateInputObjectType(someInvalidObject);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided response object is not of type List!"
    }

    def "Provide an unsupported domain model and expect a more precise exception"() {
        given:
        final List<String> someInvalidObject = new ArrayList<String>()
        someInvalidObject << "Bla"

        when:
        fileBasedResponse.validateInputObjectType(someInvalidObject);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided response object is not of type List<FeedModel>!"
    }

    def "Check that the appropriate response model mapper is returned"() {
        when:
        def usedResponseModelMapper = fileBasedResponse.responseModelMapper

        then:
        usedResponseModelMapper == responseModelMapper
    }
}
