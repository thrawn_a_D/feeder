package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class EntryReadStatusManipulatorTest extends Specification {
    EntryReadStatusManipulator entryReadStatusManipulator

    def setup(){
        entryReadStatusManipulator = new EntryReadStatusManipulator()
    }

    def "Set entry read status based on provided data"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        def userModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        and:
        userModel.feedEntriesReadStatus.put(feedEntry.id, true)

        when:
        entryReadStatusManipulator.manipulate(feedEntry, userModel, otherAttributesOfAtom)

        then:
        !otherAttributesOfAtom.isEmpty()
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_READ"
        otherAttributesOfAtom.values().iterator().next() == 'true'
    }

    def "No feed entries are marked as read"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        def userModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        entryReadStatusManipulator.manipulate(feedEntry, userModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.isEmpty()
    }

    def "Feed read status is marked as false"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        def userModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        and:
        userModel.feedEntriesReadStatus.put(feedEntry.id, false)

        when:
        entryReadStatusManipulator.manipulate(feedEntry, userModel, otherAttributesOfAtom)

        then:
        !otherAttributesOfAtom.isEmpty()
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_READ"
        otherAttributesOfAtom.values().iterator().next() == 'false'
    }
}
