package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global

import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common.CommonGlobalSearchRequestMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class GlobalSearchQueryWithUserDataRequestModelMapperTest extends Specification {

    GlobalSearchQueryWithUserDataRequestModelMapper requestModelMapper
    CommonGlobalSearchRequestMapper commonGlobalSearchRequestMapper

    def setup(){
        requestModelMapper = new GlobalSearchQueryWithUserDataRequestModelMapper()
        commonGlobalSearchRequestMapper = Mock(CommonGlobalSearchRequestMapper)
        requestModelMapper.commonGlobalSearchRequestMapper = commonGlobalSearchRequestMapper
    }

    def "Get feeder search query model based on generated domain object"() {
        given: "Some test data"
        def searchQueryToMap = getSearchRequestObject()

        and:
        def expectedGlobalSearchQuery = new GlobalSearchQueryEnrichedWithUserDataRequest()

        when:
        def result = requestModelMapper.getRequestModel(searchQueryToMap);

        then:
        result == expectedGlobalSearchQuery
        1 * commonGlobalSearchRequestMapper.mapJaxbToModel(searchQueryToMap, GlobalSearchQueryEnrichedWithUserDataRequest) >> expectedGlobalSearchQuery
    }

    private Map getSearchRequestObject() {
        def searchQueryToMap = [searchTerm:'lalal to search']
        searchQueryToMap
    }
}
