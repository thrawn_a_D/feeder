package de.wsorg.feeder.processor.execution.feed.request.executor.handling;


import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.usecases.feed.handling.subscribtion.FeedSubscriptionHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.08.12
 */
public class FeedSubscriptionRequestExecutorTest extends Specification {
    FeedSubscriptionRequestExecutor handleFeedRequestExecutor
    FeedSubscriptionHandler feedModifier

    def setup(){
        handleFeedRequestExecutor = new FeedSubscriptionRequestExecutor()
        feedModifier = Mock(FeedSubscriptionHandler)
        handleFeedRequestExecutor.feedFavourizer = feedModifier
    }

    def "Check if proper mapper is provided"() {
        given:
        RequestModelMapper handlerModelMapper = Mock(RequestModelMapper)
        handleFeedRequestExecutor.requestModelMapper = handlerModelMapper

        when:
        def usedMapper = handleFeedRequestExecutor.getRequestModelMapper()

        then:
        usedMapper == handlerModelMapper
    }

    def "Make sure a proper jaxb package is provided"() {
        given:
        def packageName="de.wsorg.feeder.processor.execution.domain.generated.request.handler"

        when:
        def usedPackageName = handleFeedRequestExecutor.getJaxbGeneratedPackageName()

        then:
        usedPackageName == packageName
    }

    def "Ensure feed modifier is executet properly"() {
        given:
        FeedSubscriptionRequest givenModificationRequest = new FeedSubscriptionRequest(feedUrl: 'bla', favourFeed: true)

        when:
        handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        1 * feedModifier.subscribeToFeed(givenModificationRequest);

    }

    def "Validate result in case of valid request"() {
        given:
        FeedSubscriptionRequest givenModificationRequest = new FeedSubscriptionRequest(feedUrl: 'bla', favourFeed: true)


        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
    }

    def "Validate result in case of a failed execution"() {
        given:
        FeedSubscriptionRequest givenModificationRequest = new FeedSubscriptionRequest(feedUrl: 'bla', favourFeed: true)

        and:
        def exceptionMessage = "Exception message"
        feedModifier.subscribeToFeed(_) >> {throw new NullPointerException(exceptionMessage)}

        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.FAILED
        result.message == exceptionMessage
    }

    def "Unsubscribe feed if a related flag is set to false"() {
        given:
        FeedSubscriptionRequest givenModificationRequest = new FeedSubscriptionRequest(feedUrl: 'bla', favourFeed: false)

        when:
        def result = handleFeedRequestExecutor.executeConcreteRequest(givenModificationRequest)

        then:
        result.processingResult == ProcessingResult.SUCCESS
        1 * feedModifier.unsubscribeFeed(givenModificationRequest)
    }
}
