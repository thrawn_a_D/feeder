package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handling;


import de.wsorg.feeder.processor.execution.domain.generated.request.handler.FeedSubscriptionType
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler.FeedSubscriptionModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.08.12
 */
public class FeedSubscriptionModelMapperTest extends Specification {
    FeedSubscriptionModelMapper handleFeedModelMapper

    def setup(){
        handleFeedModelMapper = new FeedSubscriptionModelMapper()
    }

    def "Map jaxb handle request to feed request"() {
        given:
        FeedSubscriptionType handleFeedType = new FeedSubscriptionType(feedUrl: 'url', favour: true)

        when:
        def mappedResult = handleFeedModelMapper.getRequestModel(handleFeedType)

        then:
        mappedResult.feedUrl == handleFeedType.feedUrl
        mappedResult.favourFeed == handleFeedType.favour
    }
}
