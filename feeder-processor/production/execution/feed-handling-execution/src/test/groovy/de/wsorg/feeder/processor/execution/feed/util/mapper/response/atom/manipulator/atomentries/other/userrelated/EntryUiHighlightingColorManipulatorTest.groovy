package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.userrelated

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class EntryUiHighlightingColorManipulatorTest extends Specification {
    EntryUiHighlightingColorManipulator entryUiHighlightingColorManipulator
    
    def setup(){
        entryUiHighlightingColorManipulator = new EntryUiHighlightingColorManipulator()
    }

    def "Set feed entry ui coloring attribute"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        def userModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        and:
        def color = '#kujhb'
        userModel.feedToUserAssociation.feedEntriesUiHighlightingColor.put(feedEntry.id, color)

        when:
        entryUiHighlightingColorManipulator.manipulate(feedEntry, userModel, otherAttributesOfAtom)

        then:
        !otherAttributesOfAtom.isEmpty()
        otherAttributesOfAtom.keySet().iterator().next().localPart == "FEED_UI_HIGHLIGHTING_COLOR"
        otherAttributesOfAtom.values().iterator().next() == color
    }

    def "No highlighting color is set"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        def userModel = new UserRelatedFeedModel()
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        entryUiHighlightingColorManipulator.manipulate(feedEntry, userModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.isEmpty()
    }
}
