package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries.other.general

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.07.12
 */
class ParentFeedUrlManipulatorTest extends Specification {
    ParentFeedUrlManipulator parentFeedUrlManipulator

    def setup(){
        parentFeedUrlManipulator = new ParentFeedUrlManipulator()
    }

    def "Set feed entry parent feed url attribute"() {
        given:
        def feedUrl = 'http://asddasd'
        def feedEntry = new FeedEntryModel(id: 'asd', feedUrl: feedUrl)
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        parentFeedUrlManipulator.manipulate(feedEntry, otherAttributesOfAtom)

        then:
        !otherAttributesOfAtom.isEmpty()
        otherAttributesOfAtom.keySet().iterator().next().localPart == "PARENT_FEED_URL"
        otherAttributesOfAtom.values().iterator().next() == feedUrl
    }

    def "No parent feed url is set"() {
        given:
        def feedEntry = new FeedEntryModel(id: 'asd')
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        parentFeedUrlManipulator.manipulate(feedEntry, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.isEmpty()
    }
}
