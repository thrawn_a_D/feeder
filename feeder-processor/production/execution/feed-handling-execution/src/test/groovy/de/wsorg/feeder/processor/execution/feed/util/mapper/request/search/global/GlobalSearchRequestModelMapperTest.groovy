package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global;


import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common.CommonGlobalSearchRequestMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.07.12
 */
public class GlobalSearchRequestModelMapperTest extends Specification {

    GlobalSearchRequestModelMapper requestModelMapper
    CommonGlobalSearchRequestMapper commonGlobalSearchRequestMapper

    def setup(){
        requestModelMapper = new GlobalSearchRequestModelMapper()
        commonGlobalSearchRequestMapper = Mock(CommonGlobalSearchRequestMapper)
        requestModelMapper.commonGlobalSearchRequestMapper = commonGlobalSearchRequestMapper
    }

    def "Get feeder search query model based on generated domain object"() {
        given: "Some test data"
        def searchQueryToMap = getSearchRequestObject()

        and:
        def expectedGlobalSearchQuery = new GlobalSearchQueryRequest()

        when:
        GlobalSearchQueryRequest result = requestModelMapper.getRequestModel(searchQueryToMap);

        then:
        result == expectedGlobalSearchQuery
        1 * commonGlobalSearchRequestMapper.mapJaxbToModel(searchQueryToMap, GlobalSearchQueryRequest) >> expectedGlobalSearchQuery
    }

    private Map getSearchRequestObject() {
        def givenSearchTerm = "lalal to search"
        def searchQueryToMap = ['searchTerm':givenSearchTerm]
        searchQueryToMap
    }
}
