package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomLogoFieldManipulatorTest extends Specification {
    AtomLogoFieldManipulator atomLogoFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomLogoFieldManipulator = new AtomLogoFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomLogoFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add logo to the list of entries"() {
        given:
        def feedModel = new FeedModel(logoUrl: 'asdhg')
        def listOfAttributes = []

        when:
        atomLogoFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addLogoType(feedModel.logoUrl, "logo", listOfAttributes)
    }

    def "Make sure nothing was actually done when image url is empty or null"() {
        given:
        def feedModel = new FeedModel(logoUrl: '')
        def listOfAttributes = []

        when:
        atomLogoFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addLogoType(feedModel.logoUrl, "logo", listOfAttributes)
    }

    def "Provided model is not if FeedModel"() {
        given:
        def feedModel = new FeedEntryModel()
        def listOfAttributes = []

        when:
        atomLogoFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addLogoType(_, "logo", listOfAttributes)
    }
}
