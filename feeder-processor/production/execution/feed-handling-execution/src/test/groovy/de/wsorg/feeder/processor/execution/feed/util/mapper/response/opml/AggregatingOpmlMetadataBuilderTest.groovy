package de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.12.12
 */
class AggregatingOpmlMetadataBuilderTest extends Specification {
    AggregatingOpmlMetadataBuilder simpleOpmlMetadataBuilder
    OpmlMetadataBuilder opmlMetadataBuilder1
    OpmlMetadataBuilder opmlMetadataBuilder2
    OpmlMetadataBuilder opmlMetadataBuilder3

    def setup(){
        simpleOpmlMetadataBuilder = new AggregatingOpmlMetadataBuilder()
        opmlMetadataBuilder1 = Mock(OpmlMetadataBuilder)
        opmlMetadataBuilder2 = Mock(OpmlMetadataBuilder)
        opmlMetadataBuilder3 = Mock(OpmlMetadataBuilder)
        simpleOpmlMetadataBuilder.metadataBuilderList << opmlMetadataBuilder1
        simpleOpmlMetadataBuilder.metadataBuilderList << opmlMetadataBuilder2
        simpleOpmlMetadataBuilder.metadataBuilderList << opmlMetadataBuilder3
    }

    def "Use a set of extractors, provide them the right model and concat the result to one piece"() {
        given:
        def feedModel = new FeedModel()

        and:
        def expectedResultFromFeed1 = 'someResult1'
        def expectedResultFromFeed2 = 'someResult2'
        def expectedResultFromFeed3 = 'someResult3'

        when:
        def result = simpleOpmlMetadataBuilder.getMetadata(feedModel)

        then:
        result != null
        result == "@#@#@" + expectedResultFromFeed1 + "@#@#@" + expectedResultFromFeed2 + "@#@#@" + expectedResultFromFeed3 + "@#@#@"
        1 * opmlMetadataBuilder1.getMetadata(feedModel) >> expectedResultFromFeed1
        1 * opmlMetadataBuilder2.getMetadata(feedModel) >> expectedResultFromFeed2
        1 * opmlMetadataBuilder3.getMetadata(feedModel) >> expectedResultFromFeed3
    }

    def "If no metadata is build then no separator should occur"() {
        given:
        def feedModel = new FeedModel()

        when:
        def result = simpleOpmlMetadataBuilder.getMetadata(feedModel)

        then:
        result != null
        !result.contains("@#@#@")
        1 * opmlMetadataBuilder1.getMetadata(feedModel) >> ''
    }
}
