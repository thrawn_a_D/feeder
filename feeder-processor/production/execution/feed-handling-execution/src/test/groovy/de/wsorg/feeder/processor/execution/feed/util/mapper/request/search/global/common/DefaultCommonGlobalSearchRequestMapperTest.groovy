package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.common

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class DefaultCommonGlobalSearchRequestMapperTest extends Specification {
    DefaultCommonGlobalSearchRequestMapper defaultCommonGlobalSearchRequestMapper

    def setup(){
        defaultCommonGlobalSearchRequestMapper = new DefaultCommonGlobalSearchRequestMapper()
    }

    def "Map search request model to feeder model"() {
        given:
        def searchQuery = ['searchTerm':'term']

        when:
        def result = defaultCommonGlobalSearchRequestMapper.mapJaxbToModel(searchQuery, GlobalSearchQuery)

        then:
        result.searchTerm == searchQuery.searchTerm
    }

    def "An empty request map is provided"() {
        given:
        def searchQuery = new HashMap<String, String>()

        when:
        def result = defaultCommonGlobalSearchRequestMapper.mapJaxbToModel(searchQuery, GlobalSearchQuery)

        then:
        result.searchTerm == null
    }
}
