package de.wsorg.feeder.processor.execution.feed.util.mapper.request.handler

import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedAsReadMapItemType
import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedEntryAsReadMapType
import de.wsorg.feeder.processor.execution.domain.generated.request.handler.MarkFeedEntryAsReadType
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.10.12
 */
class FeedEntryReadMarkerModelMapperTest extends Specification {
    FeedEntryReadMarkerModelMapper handleFeedModelMapper

    def setup(){
        handleFeedModelMapper = new FeedEntryReadMarkerModelMapper()
    }

    def "Map jaxb handle request to feed request"() {
        given:
        MarkFeedEntryAsReadMapType entryMarkMap = new MarkFeedEntryAsReadMapType()
        def feedMark1 = new MarkFeedAsReadMapItemType(uid: 'feedUid1', isRead: true)
        def feedMark2 = new MarkFeedAsReadMapItemType(uid: 'feedUid2', isRead: false)
        entryMarkMap.item << feedMark1
        entryMarkMap.item << feedMark2

        MarkFeedEntryAsReadType handleFeedType = new MarkFeedEntryAsReadType(
                feedUrl: 'url',
                markFeedEntryWithReadStatus: entryMarkMap)

        when:
        def mappedResult = handleFeedModelMapper.getRequestModel(handleFeedType)

        then:
        mappedResult.feedUrl == handleFeedType.feedUrl
        mappedResult.feedEntryUidWithReadabilityStatus[feedMark1.uid] == feedMark1.isRead
        mappedResult.feedEntryUidWithReadabilityStatus[feedMark2.uid] == feedMark2.isRead
    }
}
