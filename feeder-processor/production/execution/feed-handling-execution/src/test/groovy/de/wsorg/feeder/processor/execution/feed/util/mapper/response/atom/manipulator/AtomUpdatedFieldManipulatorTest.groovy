package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomUpdatedFieldManipulatorTest extends Specification {
    AtomUpdatedFieldManipulator atomUpdatedFieldManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        atomUpdatedFieldManipulator = new AtomUpdatedFieldManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        atomUpdatedFieldManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add updated time to the list of entries"() {
        given:
        def feedModel = new FeedModel(updated: new Date())
        def listOfAttributes = []

        when:
        atomUpdatedFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addDateTimeType(feedModel.updated, "updated", listOfAttributes)
    }

    def "Make sure nothing was actually done when updated field is null"() {
        given:
        def feedModel = new FeedModel(updated: null)
        def listOfAttributes = []

        when:
        atomUpdatedFieldManipulator.manipulate(feedModel, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addDateTimeType(feedModel.updated, "updated", listOfAttributes)
    }
}
