package de.wsorg.feeder.processor.execution.feed.response;


import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.execution.response.GenericResponseResolver
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 13.08.12
 */
public class FeedResponseProcessorResolverTest extends Specification {
    FeedResponseProcessorResolver responseProcessorResolver
    ResponseProcessor searchResponseProcessorMock
    ResponseProcessor loadResponseProcessorMock
    GenericResponseResolver genericResponseResolverMock


    def setup(){
        responseProcessorResolver = new FeedResponseProcessorResolver()
        searchResponseProcessorMock = Mock(ResponseProcessor)
        loadResponseProcessorMock = Mock(ResponseProcessor)
        genericResponseResolverMock = Mock(GenericResponseResolver)
        responseProcessorResolver.genericResponseResolver = genericResponseResolverMock
        responseProcessorResolver.searchResponseProcessor = searchResponseProcessorMock
        responseProcessorResolver.loadResponseProcessor = loadResponseProcessorMock
    }

    def "Get a response processor for a search result"() {
        given:
        def searchCommand = new ArrayList<FeedModel>()

        when:
        def responseProcessor = responseProcessorResolver.resolveResponseProcessor(searchCommand)

        then:
        responseProcessor == searchResponseProcessorMock
    }

    def "Check that a proper exception is given when no resolver was found"() {
        given:
        def searchCommand = [""]

        when:
        responseProcessorResolver.resolveResponseProcessor(searchCommand)

        then:
        def ex = thrown(IllegalArgumentException)
    }

    def "Get a search processor based on a list with one valid entry type"() {
        given:
        def searchResponse = [new FeedModel(), new FeedModel()]

        when:
        def responseProcessor = responseProcessorResolver.resolveResponseProcessor(searchResponse)

        then:
        responseProcessor == searchResponseProcessorMock
    }

    def "Get a response processor for a load response"() {
        given:
        def loadedFeedResponse = new FeedModel()

        when:
        def responseProcessor = responseProcessorResolver.resolveResponseProcessor(loadedFeedResponse)

        then:
        responseProcessor == loadResponseProcessorMock
    }

    def "Get a response processor for a handler response"() {
        given:
        GenericResponseResult handleFeedResponse = new GenericResponseResult()

        and:
        ResponseProcessor genericProcessor = Mock(ResponseProcessor)

        and:
        genericResponseResolverMock.resolveResponseProcessor(handleFeedResponse) >> genericProcessor

        when:
        def processor = responseProcessorResolver.resolveResponseProcessor(handleFeedResponse);

        then:
        processor == genericProcessor
    }
}
