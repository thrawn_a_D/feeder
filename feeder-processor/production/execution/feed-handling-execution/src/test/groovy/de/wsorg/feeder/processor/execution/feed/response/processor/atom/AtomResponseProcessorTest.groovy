package de.wsorg.feeder.processor.execution.feed.response.processor.atom;


import de.wsorg.feeder.processor.execution.feed.response.processor.AtomResponseProcessor
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class AtomResponseProcessorTest extends Specification {
    AtomResponseProcessor atomResponseProcessor
    ResponseModelMapper atomResponseMapperMock

    def setup(){
        atomResponseProcessor = new AtomResponseProcessor()
        atomResponseMapperMock = Mock(ResponseModelMapper)
        atomResponseProcessor.atomResponseMapper = atomResponseMapperMock
    }

    def "Check that the propper response model mapper is returned"() {
        when:
        def usedAtomMapper = atomResponseProcessor.responseModelMapper

        then:
        usedAtomMapper == atomResponseMapperMock
    }

    def "Check that validation of provided object goes well and don't throw an exception"() {
        given:
        FeedModel aValidFeed = new FeedModel()

        when:
        atomResponseProcessor.validateInputObjectType(aValidFeed)

        then:
        true
    }

    def "Throw an exception if the provided object is not of expected type"() {
        given:
        def someObject = "bla"

        when:
        atomResponseProcessor.validateInputObjectType(someObject)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided object is not of type FeedModel"
    }
}