package de.wsorg.feeder.processor.execution.feed.request.executor.search

import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.search.global.GlobalSearchRequestModelMapper
import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserRelatedSearchQuery
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserSearchRestriction
import de.wsorg.feeder.processor.usecases.feed.finder.userrelated.UserRelatedFeedFinder
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.10.12
 */
class UserRelatedSearchRequestExecutorTest extends Specification {
    UserRelatedFeedFinder feedFinderMock
    UserRelatedSearchRequestExecutor searchRequestExecutor;
    MapBasedRequestModelMapper requestModelMapperMock
    JaxbUnmarshaller unmarshallerMock

    def setup(){
        searchRequestExecutor = new UserRelatedSearchRequestExecutor();
        feedFinderMock = Mock(UserRelatedFeedFinder)
        requestModelMapperMock = Mock(GlobalSearchRequestModelMapper)
        unmarshallerMock = Mock(JaxbUnmarshaller)
        searchRequestExecutor.userRelatedFeedFinder=feedFinderMock
        searchRequestExecutor.searchRequestModelMapper=requestModelMapperMock
    }

    def "Perform search call using the request xml"() {
        given:
        final String searchTerm='suche eines feeds'
        def userId = "1234"

        and:"Testdata"
        UserRelatedSearchQueryRequest requestQuery = new UserRelatedSearchQueryRequest(searchTerm: searchTerm, userId: userId)
        requestQuery.addRestriction(UserSearchRestriction.UNREAD_ENTRIES, true);
        def searchResult = [new FeedModel()]
        def expectedSearchQueryToUse = new UserRelatedSearchQuery(searchTerm: searchTerm, userId: userId)

        when:
        def result = searchRequestExecutor.executeConcreteRequest(requestQuery)

        then:
        1 * feedFinderMock.findFeeds({UserRelatedSearchQuery it ->
            it.searchTerm == expectedSearchQueryToUse.searchTerm &&
            it.userId == expectedSearchQueryToUse.userId &&
            it.searchRestrictions.containsKey(UserSearchRestriction.UNREAD_ENTRIES) &&
            it.searchRestrictions[UserSearchRestriction.UNREAD_ENTRIES] == true}) >> searchResult;

        result != null
        result==searchResult
    }
}
