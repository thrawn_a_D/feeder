package de.wsorg.feeder.processor.execution.feed.util.mapper.request.search

import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserSearchRestriction
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.10.12
 */
class UserRelatedSearchRequestModelMapperTest extends Specification {

    UserRelatedSearchRequestModelMapper requestModelMapper

    def setup(){
        requestModelMapper = new UserRelatedSearchRequestModelMapper()
    }

    def "Get feeder search query model based on generated domain object"() {
        given: "Some test data"
        def searchQueryToMap = getSearchRequestObject()

        when:
        UserRelatedSearchQueryRequest result = requestModelMapper.getRequestModel(searchQueryToMap);

        then:
        result != null
        result.searchTerm == searchQueryToMap.searchTerm
        result.searchRestrictions[UserSearchRestriction.UNREAD_ENTRIES]
    }

    def "Provided map is empty"() {
        given: "Some test data"
        def searchQueryToMap = new HashMap()

        when:
        UserRelatedSearchQueryRequest result = requestModelMapper.getRequestModel(searchQueryToMap);

        then:
        result != null
        result.searchTerm == null
        result.searchRestrictions.size() == 0
    }

    private Map getSearchRequestObject() {
        def givenSearchTerm = "lalal to search"
        def searchQueryToMap = new HashMap<String, String>()
        searchQueryToMap.put('searchTerm', givenSearchTerm)

        searchQueryToMap.put('showUnreadEntries', 'true')

        searchQueryToMap
    }
}
