package de.wsorg.feeder.processor.execution.feed.request.executor.search.global

import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQueryEnrichedWithUserData
import de.wsorg.feeder.processor.usecases.feed.finder.global.enriched.GlobalFeedFinderEnriched
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class GlobalSearchWithUserDataRequestExecutorTest extends Specification {
    GlobalSearchWithUserDataRequestExecutor globalSearchWithUserDataRequestExecutor
    GlobalFeedFinderEnriched globalFeedFinder
    MapBasedRequestModelMapper requestModelMapper

    def setup(){
        globalSearchWithUserDataRequestExecutor = new GlobalSearchWithUserDataRequestExecutor()
        requestModelMapper = Mock(MapBasedRequestModelMapper)
        globalFeedFinder = Mock(GlobalFeedFinderEnriched)
        globalSearchWithUserDataRequestExecutor.searchRequestModelMapper = requestModelMapper
        globalSearchWithUserDataRequestExecutor.globalFeedFinder = globalFeedFinder
    }

    def "Provide the correct mapper"() {
        when:
        def result = globalSearchWithUserDataRequestExecutor.getRequestModelMapper()

        then:
        result == requestModelMapper
    }

    def "Use correct executor"() {
        given:
        def executionParam = new GlobalSearchQueryEnrichedWithUserData()

        and:
        def expectedSearchResult = [new FeedModel()]

        when:
        def result = globalSearchWithUserDataRequestExecutor.executeConcreteRequest(executionParam)

        then:
        result == expectedSearchResult
        1 * globalFeedFinder.findFeeds(executionParam) >> expectedSearchResult
    }
}
