package de.wsorg.feeder.processor.execution.feed.util.mapper.request.load

import de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.common.CommonLoadModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.01.13
 */ 
class UserRelatedLoadRequestModelMapperTest extends Specification {
    UserRelatedLoadRequestModelMapper userRelatedLoadRequestModelMapper
    CommonLoadModelMapper commonLoadModelMapper

    def setup(){
        userRelatedLoadRequestModelMapper = new UserRelatedLoadRequestModelMapper()
        commonLoadModelMapper = Mock(CommonLoadModelMapper)
        userRelatedLoadRequestModelMapper.commonLoadModelMapper = commonLoadModelMapper
    }

    def "Map user related model"() {
        given:
        def userLoadModel = new HashMap<String, String>()

        and:
        def expectedModel = new UserRelatedLoadFeed()

        when:
        def result = userRelatedLoadRequestModelMapper.getRequestModel(userLoadModel)

        then:
        result == expectedModel
        1 * commonLoadModelMapper.mapJaxbToModel(userLoadModel, UserRelatedLoadFeed) >> expectedModel
    }

    def "Map show unreadEntries if a property value is given"() {
        given:
        Map<String, String> userLoadModel = new HashMap<String, String>()
        userLoadModel.put('showOnlyUnreadEntries', "true")

        and:
        def expectedModel = new UserRelatedLoadFeed()
        commonLoadModelMapper.mapJaxbToModel(userLoadModel, UserRelatedLoadFeed) >> expectedModel

        when:
        def result = userRelatedLoadRequestModelMapper.getRequestModel(userLoadModel)

        then:
        result != null
        result.isShowOnlyUnreadEntries()
    }

    def "Map show unreadEntries if a property value is not"() {
        given:
        Map<String, String> userLoadModel = new HashMap<String, String>()

        and:
        def expectedModel = new UserRelatedLoadFeed()
        commonLoadModelMapper.mapJaxbToModel(userLoadModel, UserRelatedLoadFeed) >> expectedModel

        when:
        def result = userRelatedLoadRequestModelMapper.getRequestModel(userLoadModel)

        then:
        result != null
        result.isShowOnlyUnreadEntries() == false
    }

    def "Map show unreadEntries if a wrong property is given"() {
        given:
        Map<String, String> userLoadModel = new HashMap<String, String>()
        userLoadModel.put('showOnlyUnreadEntries', "asd")

        and:
        def expectedModel = new UserRelatedLoadFeed()
        commonLoadModelMapper.mapJaxbToModel(userLoadModel, UserRelatedLoadFeed) >> expectedModel

        when:
        def result = userRelatedLoadRequestModelMapper.getRequestModel(userLoadModel)

        then:
        result != null
        result.isShowOnlyUnreadEntries() == false
    }
}
