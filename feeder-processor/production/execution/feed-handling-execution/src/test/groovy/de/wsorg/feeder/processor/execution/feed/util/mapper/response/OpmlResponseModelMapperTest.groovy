package de.wsorg.feeder.processor.execution.feed.util.mapper.response

import de.wsorg.feeder.processor.execution.feed.util.mapper.response.opml.OpmlMetadataBuilder
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.utils.UtilsFactory
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
class OpmlResponseModelMapperTest extends Specification {

    OpmlResponseModelMapper opmlResponseModelMapper;
    OpmlMetadataBuilder opmlMetadataBuilder
    JaxbMarshaller jaxbMarshaller;

    def setup(){
        opmlResponseModelMapper = new OpmlResponseModelMapper();
        jaxbMarshaller = UtilsFactory.jaxbMarshaller
        opmlMetadataBuilder = Mock(OpmlMetadataBuilder)
        opmlResponseModelMapper.marshaller = jaxbMarshaller;
        opmlResponseModelMapper.opmlMetadataBuilder = opmlMetadataBuilder;
    }

    def "Build an opml xml based on a single FeedModel"() {
        given:
        def simpleFeedModel = new FeedModel(
                title: 'Feed title',
                description: 'Some description',
                feedUrl: 'http://feedUrl/feed-xml')
        def feedModelListTestStub = [simpleFeedModel]

        when:
        def xmlResult = opmlResponseModelMapper.getResponseModel(feedModelListTestStub)

        then:
        validateOpmlXml(xmlResult, simpleFeedModel, 0)
    }

    def "Build an opml xml for several FeedModels"() {
        given:
        def firstModel = new FeedModel(
                title: 'Feed title',
                description: 'Some description',
                feedUrl: 'http://feedUrl/feed-xml')
        def secondModel = new FeedModel(
                title: 'Feed title',
                description: 'Some description',
                feedUrl: 'http://feedUrl/feed-xml')

        def feedModelListTestStub = [firstModel, secondModel]

        when:
        def xmlResult = opmlResponseModelMapper.getResponseModel(feedModelListTestStub)

        then:
        validateOpmlXml(xmlResult, firstModel, 0)
        validateOpmlXml(xmlResult, secondModel, 1)
    }

    def "Build an xml using an empty input list"() {
        given:
        def emptyList = new ArrayList<FeedModel>()

        when:
        def xmlResult = opmlResponseModelMapper.getResponseModel(emptyList)

        then:
        xmlResult != null
        def resultOpml = new XmlSlurper().parseText(xmlResult)
        resultOpml.body.outline.size() == 0
    }

    def "Validate input to have text set"() {
        given:
        def simpleFeedModel = new FeedModel(
                description: 'Some description',
                feedUrl: 'http://feedUrl/feed-xml')
        def feedModelListTestStub = [simpleFeedModel]

        when:
        opmlResponseModelMapper.getResponseModel(feedModelListTestStub)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "One of the FeedModels has an empty text field. It is not allowed to leave it empty"
    }

    def "Validate input to have feedUrl set"() {
        given:
        def simpleFeedModel = new FeedModel(
                title: 'Feed title',
                description: 'Some description')
        def feedModelListTestStub = [simpleFeedModel]

        when:
        opmlResponseModelMapper.getResponseModel(feedModelListTestStub)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "One of the FeedModels has an empty feedUrl field. It is not allowed to leave it empty"
    }

    def "Make sure subscribed feeds are represented"() {

        given:
        def firstModel = new FeedModel(
                title: 'Feed title',
                description: 'Some description',
                feedUrl: 'http://feedUrl/feed-xml');

        def feedModelListTestStub = [firstModel]

        and:
        def expectedOpmlMetadata = 'resultingString'

        when:
        def xmlResult = opmlResponseModelMapper.getResponseModel(feedModelListTestStub)

        then:
        1 * opmlMetadataBuilder.getMetadata(firstModel) >> expectedOpmlMetadata

        validateOpmlXml(xmlResult, firstModel, 0)
        def resultOpml = new XmlSlurper().parseText(xmlResult)
        resultOpml.body.outline[0].@description == firstModel.description + expectedOpmlMetadata
    }

    private void validateOpmlXml(String xmlResult, FeedModel feedModel, int expectedItemIndex) {
        xmlResult != null
        def resultOpml = new XmlSlurper().parseText(xmlResult)
        resultOpml.body.outline.size() == 1
        resultOpml.body.outline[expectedItemIndex].@title == feedModel.title
        resultOpml.body.outline[expectedItemIndex].@description == feedModel.description
        resultOpml.body.outline[expectedItemIndex].@xmlUrl == feedModel.feedUrl
    }
}
