package de.wsorg.feeder.processor.execution.feed.request.executor.load;


import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.execution.feed.util.mapper.request.load.LoadRequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import de.wsorg.feeder.processor.usecases.feed.loader.CommonFeedLoader
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class LoadRequestExecutorTest extends Specification {

    def LoadRequestExecutor loadRequestExecutor;
    def LoadRequestModelMapper loadRequestModelMapperMock;
    def CommonFeedLoader feedLoaderMock


    def setup(){
        loadRequestExecutor = new LoadRequestExecutor();
        loadRequestModelMapperMock = Mock(LoadRequestModelMapper)
        feedLoaderMock = Mock(CommonFeedLoader)
        loadRequestExecutor.simpleFeedLoader = feedLoaderMock
    }

    def "Execute the parsed command and retrieve the result"() {
        given:
        LoadFeedRequest loadFeedRequest = new LoadFeedRequest()
        loadFeedRequest.feedUrl = "http://someurl.de"

        and:
        def processResult = new FeedModel()

        when:
        def result = loadRequestExecutor.executeConcreteRequest(loadFeedRequest)

        then:
        result == processResult
        1 * feedLoaderMock.loadFeed({LoadFeed it-> it.feedUrl == loadFeedRequest.feedUrl}) >> processResult
    }
}
