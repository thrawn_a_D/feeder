package de.wsorg.feeder.processor.execution.feed

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.01.13
 */
class FeedHandlingExecutionFactoryTest extends Specification {
    def "Get a main executor"() {
        when:
        def result = FeedHandlingExecutionFactory.getFeederRequestResponseHandler()

        then:
        result != null
    }
}
