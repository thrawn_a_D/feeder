package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.atomentries

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.helper.AtomModelMappingHelper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.05.13
 */
class EntryLinkManipulatorTest extends Specification {
    EntryLinkManipulator entryLinkManipulator
    AtomModelMappingHelper atomModelMappingHelper

    def setup(){
        entryLinkManipulator = new EntryLinkManipulator()
        atomModelMappingHelper = Mock(AtomModelMappingHelper)
        entryLinkManipulator.atomModelMappingHelper = atomModelMappingHelper
    }

    def "Add link to the list of attributes#"() {
        given:
        def link = new Link(href: 'asdasd')
        def feedEntry = new FeedEntryModel(links: [link])
        def listOfAttributes = []


        when:
        entryLinkManipulator.manipulate(feedEntry, listOfAttributes)

        then:
        1 * atomModelMappingHelper.addLinkType(link, listOfAttributes)
    }

    def "No FeedEntry is provided but something else"() {
        given:
        def feedEntry = new FeedModel()
        def listOfAttributes = []


        when:
        entryLinkManipulator.manipulate(feedEntry, listOfAttributes)

        then:
        0 * atomModelMappingHelper.addIdType(_, _, listOfAttributes)
    }
}
