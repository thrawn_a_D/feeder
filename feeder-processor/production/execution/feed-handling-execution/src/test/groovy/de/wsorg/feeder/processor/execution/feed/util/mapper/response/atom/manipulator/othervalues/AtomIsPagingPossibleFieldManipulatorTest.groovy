package de.wsorg.feeder.processor.execution.feed.util.mapper.response.atom.manipulator.othervalues

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.12.12
 */
class AtomIsPagingPossibleFieldManipulatorTest extends Specification {
    AtomIsPagingPossibleFieldManipulator atomIsPagingPossibleFieldManipulator

    def setup(){
        atomIsPagingPossibleFieldManipulator = new AtomIsPagingPossibleFieldManipulator()
    }

    def "Set paging attribute based on setting in user feed model"() {
        given:
        def feedModel = new FeedModel()
        feedModel.feedEntriesCollection.setAdditionalPagingPossible(true)

        and:
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomIsPagingPossibleFieldManipulator.manipulate(feedModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_PAGING_POSSIBLE"
        otherAttributesOfAtom.values().iterator().next() == 'true'
    }

    def "Set paging as false when no paging is wanted"() {
        given:
        def feedModel = new FeedModel()
        feedModel.feedEntriesCollection.setAdditionalPagingPossible(false)

        and:
        Map<QName, String> otherAttributesOfAtom = new HashMap<QName, String>()

        when:
        atomIsPagingPossibleFieldManipulator.manipulate(feedModel, otherAttributesOfAtom)

        then:
        otherAttributesOfAtom.keySet().iterator().next().localPart == "IS_PAGING_POSSIBLE"
        otherAttributesOfAtom.values().iterator().next() == 'false'
    }
}
