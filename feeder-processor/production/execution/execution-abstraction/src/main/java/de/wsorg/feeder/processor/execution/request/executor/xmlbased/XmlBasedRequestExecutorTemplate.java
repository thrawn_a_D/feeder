package de.wsorg.feeder.processor.execution.request.executor.xmlbased;

import de.wsorg.feeder.processor.execution.util.mapper.request.JaxbMarshallAndMappingHelper;
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper;

import javax.inject.Inject;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class XmlBasedRequestExecutorTemplate<T, F> implements XmlBasedRequestExecutor<T> {

    @Inject
    protected JaxbMarshallAndMappingHelper<T, F> jaxbMarshallAndMappingHelper;

    protected abstract RequestModelMapper getRequestModelMapper();
    protected abstract T executeConcreteRequest(final F executionParameter);
    protected abstract String getJaxbGeneratedPackageName();

    @Override
    public T executeRequest(final String requestAsXml) {
        F mappedExecutionParameter = jaxbMarshallAndMappingHelper.getUseCaseModel(requestAsXml,
                                                                                      getRequestModelMapper(),
                                                                                      getJaxbGeneratedPackageName());
        return executeConcreteRequest(mappedExecutionParameter);
    }
}
