package de.wsorg.feeder.processor.execution.util.mapper.response;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.execution.domain.generated.response.generic.FeederProcessingResultType;
import de.wsorg.feeder.processor.execution.domain.generated.response.generic.ProcessResultType;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;

import javax.inject.Inject;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GenericResponseModelMapper implements ResponseModelMapper<GenericResponseResult> {

    private static final String JAXB_GENERATED_PACKAGE = "de.wsorg.feeder.processor.execution.domain.generated.response.generic";
    private static final String ROOT_FIELD_NAME = "feederProcessingResult";
    @Inject
    private JaxbMarshaller jaxbMarshaller;

    @Override
    public String getResponseModel(final GenericResponseResult modelToMap) {
        FeederProcessingResultType mappingResult = getMappedResponse(modelToMap);
        JAXBElement objectToMarshall = new JAXBElement(new QName(ROOT_FIELD_NAME), FeederProcessingResultType.class, mappingResult);

        jaxbMarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_PACKAGE);
        return jaxbMarshaller.marshal(objectToMarshall);
    }

    private FeederProcessingResultType getMappedResponse(final GenericResponseResult modelToMap) {
        FeederProcessingResultType mappingResult = new FeederProcessingResultType();
        mappingResult.setMessage(modelToMap.getMessage());
        ProcessResultType mappedResultType = getMappedResultType(modelToMap);
        mappingResult.setProcessResult(mappedResultType);
        return mappingResult;
    }

    private ProcessResultType getMappedResultType(final GenericResponseResult modelToMap) {
        String nameOfResultType = modelToMap.getProcessingResult().name();
        return ProcessResultType.valueOf(nameOfResultType);
    }
}
