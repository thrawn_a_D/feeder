package de.wsorg.feeder.processor.execution.util.mapper.request;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface RequestModelMapper<T,F> {
    T getRequestModel(final F requestModel);
}
