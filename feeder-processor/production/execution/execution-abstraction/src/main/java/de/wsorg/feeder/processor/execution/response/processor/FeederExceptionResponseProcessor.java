package de.wsorg.feeder.processor.execution.response.processor;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult;
import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederExceptionResponseProcessor implements ResponseProcessor<Exception> {

    @Inject
    @Named("genericResponseProcessor")
    private ResponseProcessor<GenericResponseResult> genericFeedResponseProcessor;

    @Override
    public String processResponse(final Exception objectToTreatAsResponse) {
        final GenericResponseResult genericResponseModel = new GenericResponseResult();
        genericResponseModel.setProcessingResult(ProcessingResult.FAILED);

        if(objectToTreatAsResponse instanceof UseCaseProcessException)
            genericResponseModel.setMessage(objectToTreatAsResponse.getMessage());
        else
            genericResponseModel.setMessage("An unexpected error has occurred! The development team has been informed.");

        return genericFeedResponseProcessor.processResponse(genericResponseModel);
    }
}
