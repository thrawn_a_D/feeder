package de.wsorg.feeder.processor.execution;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeederMain {

    protected abstract FeederRequestResponseHandler getFeederRequestResponseHandler();

    public void processRequest(String[] args) {
        final String command=args[0];
        final String requestFilePath=args[1];
        final String responseAddress=args[2];
        final FeederCommand commandRequested= getCommandFromString(command);
        final String requestXml = getRequestXmlFromFile(requestFilePath);

        final String result = getFeederRequestResponseHandler().processRequest(commandRequested, requestXml);
        writeContentToFile(responseAddress, result);
    }

    private void writeContentToFile(final String responseAddress, final String xmlToResponse) {
        validateResponseAddress(responseAddress);

        Path deliverResponseToAddressPath = Paths.get(responseAddress);

        try {
            if(!Files.exists(deliverResponseToAddressPath))
                deliverResponseToAddressPath= Files.createFile(deliverResponseToAddressPath);

            Files.write(deliverResponseToAddressPath, xmlToResponse.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void validateRequestFilePath(final String requestFilePath) {
        final String errorMessage = "The provided request path is empty. Please provided a valid request path.";
        final String invalidFileErrorMessage = "The provided request path is not a valid file or could not be read: " + requestFilePath;
        validateString(requestFilePath, errorMessage);
        validateFile(requestFilePath, invalidFileErrorMessage);
    }

    private void validateResponseAddress(final String responseAddress) {
        final String errorMessage = "The provided response address is empty. Please provided a valid response address.";
        validateString(responseAddress, errorMessage);
    }

    private void validateFile(final String requestFilePath, final String invalidFileErrorMessage) {
        if(!Files.isReadable(Paths.get(requestFilePath))) {
            throw new IllegalArgumentException(invalidFileErrorMessage);
        }
    }

    private void validateString(final String stringToCheck, final String message) {
        if(StringUtils.isBlank(stringToCheck)){
            throw  new IllegalArgumentException(message);
        }
    }

    private FeederCommand getCommandFromString(final String command) {
        try {
            return FeederCommand.valueOf(command);
        } catch (IllegalArgumentException e) {
            String validValues = Arrays.toString(FeederCommand.values());
            final String errorMessage="The provided command does not match any of the values:"+ validValues;
            throw new IllegalArgumentException(errorMessage,e);
        }
    }

    private String getRequestXmlFromFile(final String requestFilePath) {
        validateRequestFilePath(requestFilePath);

        StringBuffer requestContent = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(requestFilePath));
            requestContent = new StringBuffer();
            String line = reader.readLine();
            while (line != null) {
                requestContent.append(line);
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return requestContent.toString();
    }
}
