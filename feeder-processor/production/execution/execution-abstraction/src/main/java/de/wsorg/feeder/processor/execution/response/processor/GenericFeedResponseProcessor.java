package de.wsorg.feeder.processor.execution.response.processor;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GenericFeedResponseProcessor extends ResponseProcessorTemplate<GenericResponseResult> {

    @Inject
    @Named(value = "genericResponseMapper")
    private ResponseModelMapper<GenericResponseResult> responseModelMapper;

    @Override
    protected ResponseModelMapper getResponseModelMapper() {
        return responseModelMapper;
    }

    @Override
    protected void validateInputObjectType(final Object objectToTreatAsResponse) {
        if(!(objectToTreatAsResponse instanceof GenericResponseResult)){
            String errorMessage = "Wrong response object passed to process I would expect GenericResponseResult";
            throw new IllegalArgumentException(errorMessage);
        }
    }
}