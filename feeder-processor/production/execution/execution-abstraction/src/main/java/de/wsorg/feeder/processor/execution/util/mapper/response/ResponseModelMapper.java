package de.wsorg.feeder.processor.execution.util.mapper.response;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ResponseModelMapper<F> {
    String getResponseModel(final F modelToMap);
}
