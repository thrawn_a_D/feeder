package de.wsorg.feeder.processor.execution;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.request.ExecutorResolver;
import de.wsorg.feeder.processor.execution.request.executor.RequestExecutor;
import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver;
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class BasicFeederRequestResponseHandler implements FeederRequestResponseHandler {

    private ExecutorResolver executorResolver;
    private ResponseProcessorResolver responseProcessorResolver;

    @Override
    public String processRequest(final FeederCommand command, final Object requestModel) {
        final RequestExecutor requestExecutor = executorResolver.resolveExecutor(command);
        checkExecutorIsNotNull(command, requestExecutor);
        return executeAndProcessResponse(requestExecutor, requestModel);
    }

    private void checkExecutorIsNotNull(final FeederCommand command, final RequestExecutor requestExecutor) {
        if(requestExecutor == null) {
            final String errorMessage = "No executor class was found for the related command: " + command;
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private String executeAndProcessResponse(final RequestExecutor requestExecutor, final Object requestModel) {
        Object feederResult;
        try {
            feederResult = requestExecutor.executeRequest(requestModel);
        } catch (Exception feederException) {
            feederResult = feederException;
            log.error("An error occurred during processing:{}", feederException);
        }

        return processResponse(feederResult);
    }

    private String processResponse(final Object feederResult) {
        final ResponseProcessor responseProcessor = responseProcessorResolver.resolveResponseProcessor(feederResult);
        return responseProcessor.processResponse(feederResult);
    }

    public void setExecutorResolver(final ExecutorResolver executorResolver) {
        this.executorResolver = executorResolver;
    }

    public void setResponseProcessorResolver(final ResponseProcessorResolver responseProcessorResolver) {
        this.responseProcessorResolver = responseProcessorResolver;
    }
}
