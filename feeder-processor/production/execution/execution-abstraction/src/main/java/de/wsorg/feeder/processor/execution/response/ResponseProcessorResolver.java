package de.wsorg.feeder.processor.execution.response;

import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ResponseProcessorResolver {
    ResponseProcessor resolveResponseProcessor(final Object responseObject);
}
