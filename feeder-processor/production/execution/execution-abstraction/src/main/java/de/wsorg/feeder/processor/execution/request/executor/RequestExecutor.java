package de.wsorg.feeder.processor.execution.request.executor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface RequestExecutor<T, F> {
    T executeRequest(F requestModel);
}
