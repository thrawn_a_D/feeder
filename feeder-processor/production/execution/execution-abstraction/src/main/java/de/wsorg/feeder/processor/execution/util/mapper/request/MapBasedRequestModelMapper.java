package de.wsorg.feeder.processor.execution.util.mapper.request;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface MapBasedRequestModelMapper<T> extends RequestModelMapper<T, Map<String, String>> {
}
