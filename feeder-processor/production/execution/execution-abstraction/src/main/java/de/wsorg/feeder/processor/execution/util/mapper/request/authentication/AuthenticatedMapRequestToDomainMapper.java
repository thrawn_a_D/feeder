package de.wsorg.feeder.processor.execution.util.mapper.request.authentication;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import org.aspectj.lang.ProceedingJoinPoint;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AuthenticatedMapRequestToDomainMapper {
    public Object extractAuthenticationModelFromXml(final ProceedingJoinPoint methodInvocation) throws Throwable{
        Object processingResult = methodInvocation.proceed(methodInvocation.getArgs());

        if (processingResult instanceof AuthorizedRequest &&
            methodInvocation.getArgs()[0] instanceof Map) {
            AuthorizedRequest authorizedRequest = (AuthorizedRequest) processingResult;
            Map<String, String> requestParamMap = (Map<String, String>) methodInvocation.getArgs()[0];

            authorizedRequest.setUserId(requestParamMap.get("userId"));
            authorizedRequest.setUserName(requestParamMap.get("userName"));
            authorizedRequest.setPassword(requestParamMap.get("password"));
        }

        return processingResult;
    }
}
