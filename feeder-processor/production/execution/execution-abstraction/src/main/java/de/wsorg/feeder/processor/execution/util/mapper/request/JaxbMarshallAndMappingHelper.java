package de.wsorg.feeder.processor.execution.util.mapper.request;

import de.wsorg.feeder.utils.xml.JaxbUnmarshaller;

import javax.inject.Inject;
import javax.xml.bind.JAXBException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JaxbMarshallAndMappingHelper<T, F> {

    @Inject
    protected JaxbUnmarshaller jaxbUnmarshaller;

    public F getUseCaseModel(final String requestAsXml,
                               final RequestModelMapper requestModelMapper,
                               final String jaxbGeneratedPackageName) {
        Object executionParameter = getExecutionParameter(requestAsXml, jaxbGeneratedPackageName);
        return (F) requestModelMapper.getRequestModel(executionParameter);
    }

    private Object getExecutionParameter(final String requestAsXml, final String jaxbGeneratedPackageName) {
        try {
            jaxbUnmarshaller.setJaxbGeneratedClassesPackage(jaxbGeneratedPackageName);
            return jaxbUnmarshaller.unmarshal(requestAsXml);
        } catch (JAXBException e) {
            throw new IllegalArgumentException("The provided request xml is invalid!", e);
        }
    }
}
