package de.wsorg.feeder.processor.execution.util.mapper.request;

import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.util.HashMap;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ExecutionRequestParameterMap {

    final HashMap<String, String> parameterMap = new HashMap<>();

    public void put(final String key, final String value) {
        if (StringUtils.isNotBlank(key)) {
            parameterMap.put(key, value);
        }
    }

    public String toFeederMapString() {
        String propertiesMap = "";

        for (final String paramKey : parameterMap.keySet()) {
            final String paramValue = parameterMap.get(paramKey);

            propertiesMap += paramKey + '@' + paramValue + ';';
        }

        return propertiesMap;
    }
}
