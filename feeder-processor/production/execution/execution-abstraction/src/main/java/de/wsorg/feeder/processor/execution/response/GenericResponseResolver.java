package de.wsorg.feeder.processor.execution.response;

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GenericResponseResolver implements ResponseProcessorResolver {

    @Inject
    @Named(value = "genericResponseProcessor")
    private ResponseProcessor genericResponseProcessor;

    @Inject
    @Named(value = "feederExceptionResponseProcessor")
    private ResponseProcessor feederExceptionResponseProcessor;

    @Override
    public ResponseProcessor resolveResponseProcessor(final Object responseObject) {
        ResponseProcessor chosenResponseProcessor = null;
        if(responseObject instanceof GenericResponseResult){
            chosenResponseProcessor = genericResponseProcessor;
        } else if(responseObject instanceof Exception) {
            chosenResponseProcessor = feederExceptionResponseProcessor;
        }

        return chosenResponseProcessor;
    }
}
