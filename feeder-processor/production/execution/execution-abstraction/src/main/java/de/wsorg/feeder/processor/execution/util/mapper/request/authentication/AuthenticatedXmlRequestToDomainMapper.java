package de.wsorg.feeder.processor.execution.util.mapper.request.authentication;


import de.wsorg.feeder.processor.execution.domain.generated.request.authentication.AuthenticatedRequestType;
import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller;
import org.aspectj.lang.ProceedingJoinPoint;

import javax.xml.bind.JAXBException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AuthenticatedXmlRequestToDomainMapper {
    private static final String JAXB_MAPPING_CONSTANT = "de.wsorg.feeder.processor.execution.domain.generated.request.authentication";
    private JaxbUnmarshaller jaxbUnmarshaller;

    public Object extractAuthenticationModelFromXml(final ProceedingJoinPoint methodInvocation) throws Throwable{
        String requestXml = (String) methodInvocation.getArgs()[0];

        try {
            final AuthenticatedRequestType authenticatedRequestType = getAuthorizedRequestModelAsJaxb(requestXml);
            AuthorizedRequest mappingResult = executeActualMappingRequest(methodInvocation, authenticatedRequestType);
            mappingResult.setPassword(authenticatedRequestType.getPassword());
            mappingResult.setUserName(authenticatedRequestType.getUserName());
            mappingResult.setUserId(authenticatedRequestType.getUserId());
            return mappingResult;
        } catch (JAXBException e) {
            return methodInvocation.proceed(methodInvocation.getArgs());
        }
    }

    private AuthorizedRequest executeActualMappingRequest(final ProceedingJoinPoint methodInvocation,
                                                           final AuthenticatedRequestType authenticatedRequestType) throws Throwable {
        Object[] processingXml = methodInvocation.getArgs();
        processingXml[0] = authenticatedRequestType.getActualRequestContent();
        Object result = methodInvocation.proceed(processingXml);
        if(result instanceof AuthorizedRequest) {
            return (AuthorizedRequest) result;
        } else {
            final String errorMessage = "The actual request object does not derive from AuthorizedRequest but was wrapped by authorized xml.";
            throw new IllegalStateException(errorMessage);
        }
    }

    private AuthenticatedRequestType getAuthorizedRequestModelAsJaxb(final String requestXml) throws JAXBException {
        AuthenticatedRequestType authenticatedRequestType;
        authenticatedRequestType = (AuthenticatedRequestType) getJaxbUnmarshaller().unmarshal(requestXml);
        return authenticatedRequestType;
    }

    private JaxbUnmarshaller getJaxbUnmarshaller() {
        if(jaxbUnmarshaller == null) {
            jaxbUnmarshaller = UtilsFactory.getJaxbUnmarshaller();
            jaxbUnmarshaller.setJaxbGeneratedClassesPackage(JAXB_MAPPING_CONSTANT);
        }

        return jaxbUnmarshaller;
    }
}
