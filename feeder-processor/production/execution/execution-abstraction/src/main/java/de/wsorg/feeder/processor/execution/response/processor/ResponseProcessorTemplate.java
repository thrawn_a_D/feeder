package de.wsorg.feeder.processor.execution.response.processor;

import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class ResponseProcessorTemplate<T> implements ResponseProcessor<T> {

    protected abstract ResponseModelMapper getResponseModelMapper();
    protected abstract void validateInputObjectType(final Object objectToTreatAsResponse);

    @Override
    public String processResponse(final T objectToTreatAsResponse) {
        validateInputObjectType(objectToTreatAsResponse);
        return getResponseModelMapper().getResponseModel(objectToTreatAsResponse);
    }
}
