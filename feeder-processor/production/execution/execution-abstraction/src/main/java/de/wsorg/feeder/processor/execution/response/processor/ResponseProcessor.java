package de.wsorg.feeder.processor.execution.response.processor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ResponseProcessor<T> {
    /**
     * Process a request and return the result xml.
     * @param objectToTreatAsResponse
     * @return result as xml
     */
    String processResponse(final T objectToTreatAsResponse);
}
