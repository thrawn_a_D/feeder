package de.wsorg.feeder.processor.execution;

import de.wsorg.feeder.processor.api.domain.FeederCommand;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederRequestResponseHandler<T> {
    /**
     * Execute a particular request using a given command to execute and a request model.
     * @return Processing result as xml.
     */
    public String processRequest(final FeederCommand command, final T requestModel);
}
