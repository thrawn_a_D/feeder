package de.wsorg.feeder.processor.execution.request.executor.mapbased;

import de.wsorg.feeder.processor.execution.request.executor.RequestExecutor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface MapBasedRequestExecutor<T> extends RequestExecutor<T, String> {
}
