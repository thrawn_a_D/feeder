package de.wsorg.feeder.processor.execution.request.executor.mapbased;

import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class MapBasedRequestExecutorTemplate<T, F> implements MapBasedRequestExecutor<T> {

    protected abstract MapBasedRequestModelMapper<F> getRequestModelMapper();
    protected abstract T executeConcreteRequest(final F executionParameter);

    @Override
    public T executeRequest(final String requestModel) {

        Map<String, String> requestModelAsMap = parseRequestModelAsMap(requestModel);

        F mappedModel = getRequestModelMapper().getRequestModel(requestModelAsMap);
        return executeConcreteRequest(mappedModel);
    }

    private Map<String, String> parseRequestModelAsMap(final String requestModel) {
        Map<String, String> requestModelAsMap = new HashMap<>();

        if (requestModel.contains(";")) {
            String[] requestParams = requestModel.split(";");
            for (int i = 0; i < requestParams.length; i++) {
                final String currentRequestParam = requestParams[i];
                final String[] keyValuePair = currentRequestParam.split("@");
                if (keyValuePair.length == 2) {
                    final String paramKey = keyValuePair[0];
                    final String paraValue = keyValuePair[1];

                    requestModelAsMap.put(paramKey, paraValue);
                } else {
                    throw new IllegalArgumentException("The provided request model is not a valid map as expected");
                }
            }
        } else {
            throw new IllegalArgumentException("The provided request model is not a valid map as expected");
        }
        return requestModelAsMap;
    }
}
