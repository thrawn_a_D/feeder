package de.wsorg.feeder.processor.execution.request;

import de.wsorg.feeder.processor.api.domain.FeederCommand;
import de.wsorg.feeder.processor.execution.request.executor.RequestExecutor;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ExecutorResolver {
    RequestExecutor resolveExecutor(final FeederCommand feederCommand);
}
