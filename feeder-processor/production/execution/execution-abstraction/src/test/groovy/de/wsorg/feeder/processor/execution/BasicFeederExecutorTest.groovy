package de.wsorg.feeder.processor.execution

import de.wsorg.feeder.processor.api.domain.FeederCommand
import de.wsorg.feeder.processor.execution.request.ExecutorResolver
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutor
import de.wsorg.feeder.processor.execution.response.ResponseProcessorResolver
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.01.13
 */
class BasicFeederExecutorTest extends Specification {
    BasicFeederRequestResponseHandler basicFeederExecutor
    ExecutorResolver executorResolverMock
    ResponseProcessorResolver responseProcessorResolverMock

    def setup(){
        basicFeederExecutor = new BasicFeederRequestResponseHandler()
        executorResolverMock = Mock(ExecutorResolver)
        responseProcessorResolverMock = Mock(ResponseProcessorResolver)
        basicFeederExecutor.executorResolver = executorResolverMock
        basicFeederExecutor.responseProcessorResolver = responseProcessorResolverMock
    }

    def "Delegate request to a processor"() {
        given:
        final FeederCommand command = FeederCommand.GLOBAL_SEARCH;
        final String xml = 'asd'

        and:"Prepare some mocks"
        XmlBasedRequestExecutor requestExecutor = Mock(XmlBasedRequestExecutor);

        and:
        def responseXml = 'response'

        and:
        def feederResult = []

        and:
        def responseProcessor = Mock(ResponseProcessor)

        when:
        def result = basicFeederExecutor.processRequest(command, xml)

        then:
        result == responseXml
        1 * executorResolverMock.resolveExecutor(FeederCommand.GLOBAL_SEARCH) >> requestExecutor
        1 * requestExecutor.executeRequest(_) >> feederResult
        1 * responseProcessorResolverMock.resolveResponseProcessor(feederResult) >> responseProcessor
        1 * responseProcessor.processResponse(feederResult) >> responseXml
    }

    def "Handle exception while actual feeder usecase execution"() {
        given:
        final FeederCommand command = FeederCommand.GLOBAL_SEARCH;
        final String xml = 'asd'

        and:"Prepare some mocks"
        XmlBasedRequestExecutor requestExecutor = Mock(XmlBasedRequestExecutor);
        def responseProcessor = Mock(ResponseProcessor)

        and:
        def expectedException = new RuntimeException("My exception")

        when:
        basicFeederExecutor.processRequest(command, xml)

        then:
        1 * executorResolverMock.resolveExecutor(FeederCommand.GLOBAL_SEARCH) >> requestExecutor
        1 * requestExecutor.executeRequest(_) >> {throw expectedException}
        1 * responseProcessorResolverMock.resolveResponseProcessor(expectedException) >> responseProcessor
    }
}
