package de.wsorg.feeder.processor.execution.util.mapper.request

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.01.13
 */ class ExecutionRequestParameterMapTest extends Specification {
    ExecutionRequestParameterMap feederParameterMap

    def setup(){
        feederParameterMap = new ExecutionRequestParameterMap()
    }

    def "Add a key and a value and finally get the map as string"() {
        given:
        def key='asd'
        def value='val'

        when:
        feederParameterMap.put(key, value)
        def result = feederParameterMap.toFeederMapString()

        then:
        result == key + '@' + value + ';'
    }

    def "Add several key/values and finally get the map as string"() {
        given:
        def key1='asd'
        def value1='val'
        def key2='asd2'
        def value2='val2'

        when:
        feederParameterMap.put(key1, value1)
        feederParameterMap.put(key2, value2)
        def result = feederParameterMap.toFeederMapString()

        then:
        result == key1 + '@' + value1 + ';' + key2 + '@' + value2 + ';'
    }

    def "No key value provided, nothing should be stored"() {
        given:
        def value='val'

        when:
        feederParameterMap.put(null, value)
        def result = feederParameterMap.toFeederMapString()

        then:
        result == ''
    }
}
