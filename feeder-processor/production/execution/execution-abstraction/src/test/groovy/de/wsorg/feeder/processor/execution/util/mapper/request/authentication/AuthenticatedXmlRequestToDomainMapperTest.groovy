package de.wsorg.feeder.processor.execution.util.mapper.request.authentication

import de.wsorg.feeder.processor.execution.domain.generated.request.authentication.AuthenticatedRequestType
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import org.aspectj.lang.ProceedingJoinPoint
import spock.lang.Specification

import javax.xml.bind.JAXBException

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.01.13
 */
class AuthenticatedXmlRequestToDomainMapperTest extends Specification {
    AuthenticatedXmlRequestToDomainMapper authenticatedRequestToDomainMapper
    JaxbUnmarshaller jaxbUnmarshaller

    def setup(){
        authenticatedRequestToDomainMapper = new AuthenticatedXmlRequestToDomainMapper()
        jaxbUnmarshaller = Mock(JaxbUnmarshaller)
        authenticatedRequestToDomainMapper.jaxbUnmarshaller = jaxbUnmarshaller
    }

    def "Map a request xml back to authenticated model and give the enclosing xml to the actual process"() {
        given:
        def authenticatedXmlToBeProcessed = 'asd'

        and:
        def userName = 'userName'
        def password = 'passwd'
        def userId = '5445'
        def actualRequestXmlToProcess = 'asdklnasd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        def modelMapperMock = Mock(RequestModelMapper)
        def jaxbPackageName = 'some name'
        def params = [authenticatedXmlToBeProcessed, modelMapperMock, jaxbPackageName]
        def executionParams = [actualRequestXmlToProcess, modelMapperMock, jaxbPackageName]
        jointPoint.getArgs() >> params

        and:
        def expectedAuthenticatedObject = Mock(AuthenticatedRequestType)
        expectedAuthenticatedObject.actualRequestContent >> actualRequestXmlToProcess
        expectedAuthenticatedObject.userName >> userName
        expectedAuthenticatedObject.password >> password
        expectedAuthenticatedObject.userId >> userId

        and:
        def expectedProcessedObject = Mock(AuthorizedRequest)

        when:
        AuthorizedRequest result = authenticatedRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        result == expectedProcessedObject
        1 * expectedProcessedObject.setUserName(userName)
        1 * expectedProcessedObject.setPassword(password)
        1 * jaxbUnmarshaller.unmarshal(authenticatedXmlToBeProcessed) >> expectedAuthenticatedObject
        1 * jointPoint.proceed(executionParams) >> expectedProcessedObject
    }

    def "Mapped object does not derive from AuthorizedRequest interface as expected"() {
        given:
        def authenticatedXmlToBeProcessed = 'asd'

        and:
        def userName = 'userName'
        def password = 'passwd'
        def actualRequestXmlToProcess = 'asdklnasd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        def modelMapperMock = Mock(RequestModelMapper)
        def jaxbPackageName = 'some name'
        def params = [authenticatedXmlToBeProcessed, modelMapperMock, jaxbPackageName]
        def executionParams = [actualRequestXmlToProcess, modelMapperMock, jaxbPackageName]
        jointPoint.getArgs() >> params

        and:
        def expectedAuthenticatedObject = Mock(AuthenticatedRequestType)
        expectedAuthenticatedObject.actualRequestContent >> actualRequestXmlToProcess
        expectedAuthenticatedObject.userName >> userName
        expectedAuthenticatedObject.password >> password

        and:
        def expectedProcessedObject = 'asdasd'

        when:
        authenticatedRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'The actual request object does not derive from AuthorizedRequest but was wrapped by authorized xml.'
        1 * jaxbUnmarshaller.unmarshal(authenticatedXmlToBeProcessed) >> expectedAuthenticatedObject
        1 * jointPoint.proceed(executionParams) >> expectedProcessedObject
    }

    def "Request is not authorized, do nothing and provide original xml for execution"() {
        given:
        def noAuthorizationNeededXml = 'asdasd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        jointPoint.getArgs() >> [noAuthorizationNeededXml]

        and:
        def expectedProcessedObject = ''

        when:
        def result = authenticatedRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        result == expectedProcessedObject
        1 * jaxbUnmarshaller.unmarshal(noAuthorizationNeededXml) >> {throw new JAXBException("")}
        1 * jointPoint.proceed([noAuthorizationNeededXml]) >> expectedProcessedObject
    }
}
