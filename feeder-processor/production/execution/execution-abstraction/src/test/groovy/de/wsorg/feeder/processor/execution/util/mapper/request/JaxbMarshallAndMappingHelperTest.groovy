package de.wsorg.feeder.processor.execution.util.mapper.request

import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import spock.lang.Specification

import javax.xml.bind.JAXBException

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.01.13
 */
class JaxbMarshallAndMappingHelperTest extends Specification {
    static String jaxbGeneratedClasses = "de.wsorg.lalala"

    JaxbMarshallAndMappingHelper jaxbMarshallAndMappingHelper

    RequestModelMapper requestModelMapperMock
    JaxbUnmarshaller jaxbUnmarshallerMock

    def setup(){
        jaxbMarshallAndMappingHelper = new JaxbMarshallAndMappingHelper()
        requestModelMapperMock = Mock(RequestModelMapper)
        jaxbUnmarshallerMock = Mock(JaxbUnmarshaller)
        jaxbMarshallAndMappingHelper.jaxbUnmarshaller = jaxbUnmarshallerMock
    }


    def "Unmarshall request pass it to a mapper and to concrete execution"() {
        given:
        def requestAsXmlToProcess = "<load>kjhkjh</load>"

        and:
        def jaxbUnmarshalledObject = new Object()

        and:
        def actualRequestObject = new LoadFeedRequest()

        when:
        def result = jaxbMarshallAndMappingHelper.getUseCaseModel(requestAsXmlToProcess,
                                                                       requestModelMapperMock,
                                                                       jaxbGeneratedClasses)

        then:
        1 * jaxbUnmarshallerMock.setJaxbGeneratedClassesPackage(jaxbGeneratedClasses)
        1 * jaxbUnmarshallerMock.unmarshal(requestAsXmlToProcess) >> jaxbUnmarshalledObject
        1 * requestModelMapperMock.getRequestModel(jaxbUnmarshalledObject) >> actualRequestObject

        actualRequestObject == result
    }

    def "Give proper message when an invalid xml is provided"() {
        given:
        def invalidXml = "bla"

        and: "Throw an exception"
        jaxbUnmarshallerMock.unmarshal(invalidXml) >> {throw new JAXBException("")}

        when:
        jaxbMarshallAndMappingHelper.getUseCaseModel(invalidXml,
                                                         requestModelMapperMock,
                                                         jaxbGeneratedClasses)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided request xml is invalid!"
    }
}
