package de.wsorg.feeder.processor.execution.response.processor

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.01.13
 */
class FeederExceptionResponseProcessorTest extends Specification {
    FeederExceptionResponseProcessor feederExceptionResponseProcessor
    ResponseProcessor genericFeedResponseProcessor

    def setup(){
        feederExceptionResponseProcessor = new FeederExceptionResponseProcessor()
        genericFeedResponseProcessor = Mock(ResponseProcessor)
        feederExceptionResponseProcessor.genericFeedResponseProcessor = genericFeedResponseProcessor
    }

    def "Prepare a feeder exception which is addressed to user and give it to a generic processor"() {
        given:
        def message = "Error message"
        def exception = new UseCaseProcessException(message)

        when:
        feederExceptionResponseProcessor.processResponse(exception)

        then:
        1 * genericFeedResponseProcessor.processResponse({it instanceof GenericResponseResult &&
                                                              it.processingResult == ProcessingResult.FAILED &&
                                                              it.message == message})
    }

    def "Process an exception which is not addressed to the user"() {
        given:
        def message = "Error message"
        def exception = new RuntimeException(message)

        and:
        def expectedErrorMessage = "An unexpected error has occurred! The development team has been informed."

        when:
        feederExceptionResponseProcessor.processResponse(exception)

        then:
        1 * genericFeedResponseProcessor.processResponse({it instanceof GenericResponseResult &&
                                                              it.processingResult == ProcessingResult.FAILED &&
                                                              it.message == expectedErrorMessage})
    }
}
