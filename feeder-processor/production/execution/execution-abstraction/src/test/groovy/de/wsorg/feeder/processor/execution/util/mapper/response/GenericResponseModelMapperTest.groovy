package de.wsorg.feeder.processor.execution.util.mapper.response

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.api.domain.response.result.ProcessingResult
import de.wsorg.feeder.processor.execution.domain.generated.response.generic.FeederProcessingResultType
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.09.12
 */
class GenericResponseModelMapperTest extends Specification {
    GenericResponseModelMapper genericResponseModelMapper
    JaxbMarshaller jaxbMarshaller

    def setup(){
        genericResponseModelMapper = new GenericResponseModelMapper()
        jaxbMarshaller = Mock(JaxbMarshaller)
        genericResponseModelMapper.jaxbMarshaller = jaxbMarshaller
    }

    def "Map a handler response to an xml string"() {
        given:
        GenericResponseResult handleFeedResponse = new GenericResponseResult(processingResult: ProcessingResult.SUCCESS,
                message: 'myMessage')

        when:
        def result = genericResponseModelMapper.getResponseModel(handleFeedResponse)

        then:
        1 * jaxbMarshaller.marshal({it instanceof JAXBElement &&
                it.declaredType == FeederProcessingResultType.class &&
                it.value instanceof FeederProcessingResultType &&
                it.value.message == handleFeedResponse.message &&
                it.value.processResult.name() == handleFeedResponse.processingResult.name()})
    }

    def "Make sure the marshaller has the right jaxb package set"() {
        given:
        GenericResponseResult handleFeedResponse = new GenericResponseResult(processingResult: ProcessingResult.SUCCESS,
                message: 'myMessage')

        when:
        genericResponseModelMapper.getResponseModel(handleFeedResponse)

        then:
        1 * jaxbMarshaller.setJaxbGeneratedClassesPackage("de.wsorg.feeder.processor.execution.domain.generated.response.generic")
    }
}
