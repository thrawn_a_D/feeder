package de.wsorg.feeder.processor.execution.util.mapper.request.authentication

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest
import org.aspectj.lang.ProceedingJoinPoint
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.01.13
 */
class AuthenticatedMapRequestToDomainMapperTest extends Specification {
    AuthenticatedMapRequestToDomainMapper authenticatedMapRequestToDomainMapper

    def setup(){
        authenticatedMapRequestToDomainMapper = new AuthenticatedMapRequestToDomainMapper()
    }

    def "Map provided map based authorized request"() {
        given:
        def params = Mock(Map)
        params.get('userId') >> '123'
        params.get('userName') >> 'usern'
        params.get('password') >> 'passwd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        jointPoint.args >> [params]

        and:
        def processingResult= Mock(AuthorizedRequest)

        when:
        AuthorizedRequest result = authenticatedMapRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        result == processingResult
        1 * processingResult.setUserId('123')
        1 * processingResult.setUserName('usern')
        1 * processingResult.setPassword('passwd')
        1 * jointPoint.proceed(jointPoint.getArgs()) >> processingResult
    }

    def "Processing result is not of type Authorized request"() {
        given:
        def params = Mock(Map)
        params.get('userId') >> '123'
        params.get('userName') >> 'usern'
        params.get('password') >> 'passwd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        jointPoint.args >> [params]

        and:
        def processingResult= ''

        when:
        def result = authenticatedMapRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        result == processingResult
        1 * jointPoint.proceed(jointPoint.getArgs()) >> processingResult
    }

    def "Provided param is not a map, do nothing"() {
        given:
        def params = 'asd'

        and:
        def jointPoint = Mock(ProceedingJoinPoint)
        jointPoint.args >> [params]

        and:
        def processingResult= Mock(AuthorizedRequest)

        when:
        def result = authenticatedMapRequestToDomainMapper.extractAuthenticationModelFromXml(jointPoint)

        then:
        result == processingResult
        1 * jointPoint.proceed(jointPoint.getArgs()) >> processingResult
    }
}
