package de.wsorg.feeder.processor.execution.response.processor

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.09.12
 */
class GenericFeedResponseProcessorTest extends Specification {
    GenericFeedResponseProcessor genericFeedResponseProcessor

    def setup(){
        genericFeedResponseProcessor = new GenericFeedResponseProcessor()
    }

    def "Make sure the correct mapper is provided"() {
        given:
        ResponseModelMapper responseModelMapper = Mock(ResponseModelMapper)
        genericFeedResponseProcessor.responseModelMapper = responseModelMapper

        when:
        def usedMapper = genericFeedResponseProcessor.getResponseModelMapper()

        then:
        usedMapper == responseModelMapper
    }

    def "Validate input type properly"() {
        given:
        def somethingInvalid = ""

        when:
        genericFeedResponseProcessor.validateInputObjectType(somethingInvalid)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "Wrong response object passed to process I would expect GenericResponseResult"
    }

    def "Provide valid object to validate"() {
        given:
        GenericResponseResult validInput = new GenericResponseResult()

        when:
        genericFeedResponseProcessor.validateInputObjectType(validInput);

        then:
        true
    }
}
