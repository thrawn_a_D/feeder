package de.wsorg.feeder.processor.execution.request.executor.mapbased

import de.wsorg.feeder.processor.execution.util.mapper.request.MapBasedRequestModelMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.01.13
 */
class MapBasedRequestExecutorTemplateTest extends Specification {

    MapBasedRequestExecutorTestClass mapBasedRequestExecutor

    static MapBasedRequestModelMapper requestModelMapper

    static def providedExecutionParameter
    static def executionResult = 'bla'

    def setup(){
        mapBasedRequestExecutor = new MapBasedRequestExecutorTestClass()
        requestModelMapper = Mock(MapBasedRequestModelMapper)
    }

    def "When providing a map of entries, convert the string and provide this to a mapper"() {
        given:
        def mapEntriesString = 'bla@blub;pi@pa'

        and:
        def mappedModelToExecute = 'asd'

        when:
        def result = mapBasedRequestExecutor.executeRequest(mapEntriesString);

        then:
        1 * requestModelMapper.getRequestModel({it instanceof Map &&
                                                  it['bla'] == 'blub' &&
                                                  it['pi'] == 'pa'}) >> mappedModelToExecute

        providedExecutionParameter == mappedModelToExecute
        result == executionResult
    }

    def "Throw exception when no map is provided"() {
        given:
        def mapEntriesString = 'bla'

        when:
        mapBasedRequestExecutor.executeRequest(mapEntriesString);

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided request model is not a valid map as expected'
    }

    def "Throw exception when map keys are not separated properly"() {
        given:
        def mapEntriesString = 'bla-s;'

        when:
        mapBasedRequestExecutor.executeRequest(mapEntriesString);

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided request model is not a valid map as expected'
    }

    private static class MapBasedRequestExecutorTestClass extends MapBasedRequestExecutorTemplate<String, String> {

        @Override
        protected MapBasedRequestModelMapper getRequestModelMapper() {
            MapBasedRequestExecutorTemplateTest.requestModelMapper
        }

        @Override
        protected String executeConcreteRequest(final String executionParameter) {
            MapBasedRequestExecutorTemplateTest.providedExecutionParameter = executionParameter
            MapBasedRequestExecutorTemplateTest.executionResult

        }
    }
}
