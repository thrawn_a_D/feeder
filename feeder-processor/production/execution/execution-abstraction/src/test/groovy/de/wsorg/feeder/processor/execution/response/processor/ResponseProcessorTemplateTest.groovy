package de.wsorg.feeder.processor.execution.response.processor;


import de.wsorg.feeder.processor.execution.util.mapper.response.ResponseModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.08.12
 */
public class ResponseProcessorTemplateTest extends Specification {
    private static String DEFAULT_TEMP_FOLDER;

    static {
        DEFAULT_TEMP_FOLDER=System.getProperty("java.io.tmpdir") + File.separator + "response.xml"
    }

    static Object passedForValidation

    TestResponseProcessor responseProcessor;
    static ResponseModelMapper responseModelMapper

    def setup(){
        responseProcessor=new TestResponseProcessor();
        responseModelMapper = Mock(ResponseModelMapper)
    }

    def "Deliver response to a given file address"() {
        given:
        final List<FeedModel> feedModelAsStub = new ArrayList<FeedModel>()

        and:
        def processingResult = 'processingResult'

        when:
        def result = responseProcessor.processResponse(feedModelAsStub)

        then:
        result == processingResult
        1 * responseModelMapper.getResponseModel(feedModelAsStub)>> processingResult
    }

    def "Make sure object is passed for validation"() {
        given:
        final List<FeedModel> feedModelAsStub = new ArrayList<FeedModel>()


        when:
        responseProcessor.processResponse(feedModelAsStub)

        then:
        passedForValidation == feedModelAsStub
    }

    final static class TestResponseProcessor extends ResponseProcessorTemplate {

        @Override
        protected ResponseModelMapper getResponseModelMapper() {
            return ResponseProcessorTemplateTest.responseModelMapper;
        }

        @Override
        protected void validateInputObjectType(final Object objectToTreatAsResponse) {
            passedForValidation = objectToTreatAsResponse
        }
    }
}
