package de.wsorg.feeder.processor.execution;


import de.wsorg.feeder.processor.api.domain.FeederCommand
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.07.12
 */
public class FeederMainTest extends Specification {

    private static final String SYSTEM_TEMP_DIR=System.getProperty("java.io.tmpdir") + File.separator
    private static final String DEFAULT_RESPONSE_DESTINATION = SYSTEM_TEMP_DIR + "response.xml"
    private static final String DEFAULT_REQUEST_SOURCE = SYSTEM_TEMP_DIR + "request.xml"
    static FeederRequestResponseHandler feederExecutor
    SpecificMain feederMain

    def setupSpec(){
        new File(DEFAULT_REQUEST_SOURCE).createNewFile()
    }

    def setup(){
        feederExecutor = Mock(FeederRequestResponseHandler)

        feederMain = new SpecificMain()
    }

    def "Validate input argument for command"() {
        given:
        final String invalidCommand="bla";
        final String[] requestArgs = [invalidCommand, DEFAULT_REQUEST_SOURCE, DEFAULT_RESPONSE_DESTINATION]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided command does not match any of the values:"+FeederCommand.values()
    }

    def "Validate input argument for a nullable request path"() {
        given:
        final String invalidCommand=FeederCommand.LOAD;
        final String aValidPath=null;
        final String[] requestArgs = [invalidCommand, aValidPath, DEFAULT_RESPONSE_DESTINATION]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided request path is empty. Please provided a valid request path.";
    }

    def "Validate input argument for an invalid request path"() {
        given:
        final String invalidCommand=FeederCommand.LOAD;
        final String anInvalidPath="\\//dsffsd";
        final String[] requestArgs = [invalidCommand, anInvalidPath, DEFAULT_RESPONSE_DESTINATION]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided request path is not a valid file or could not be read: " + anInvalidPath;
    }

    def "Validate input argument for an empty request path"() {
        given:
        final String invalidCommand=FeederCommand.LOAD;
        final String aValidXml='';
        final String[] requestArgs = [invalidCommand, aValidXml, DEFAULT_RESPONSE_DESTINATION]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided request path is empty. Please provided a valid request path.";
    }

    def "Validate input argument for a nullable response address"() {
        given:
        final String invalidCommand=FeederCommand.LOAD;
        final String responseAddress=null;
        final String[] requestArgs = [invalidCommand, DEFAULT_REQUEST_SOURCE, responseAddress]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided response address is empty. Please provided a valid response address.";
    }

    def "Validate input argument for an empty response address"() {
        given:
        final String invalidCommand=FeederCommand.LOAD;
        final String responseAddress='';
        final String[] requestArgs = [invalidCommand, DEFAULT_REQUEST_SOURCE, responseAddress]

        when:
        feederMain.processRequest(requestArgs);

        then:
        def ex=thrown(IllegalArgumentException)
        ex.message=="The provided response address is empty. Please provided a valid response address.";
    }

    def "Read request file, extract the content and pass the xml to process"() {
        given:
        final FeederCommand command = FeederCommand.GLOBAL_SEARCH;
        final String[] requestArgs = [command.toString(), DEFAULT_REQUEST_SOURCE, DEFAULT_RESPONSE_DESTINATION]

        and:"Write request file"
        def requestString='blablabla blablabla blabla bla'
        writeContentToFile(DEFAULT_REQUEST_SOURCE, requestString)

        and:
        def responseString = 'asd'

        when:
        feederMain.processRequest(requestArgs)

        then:
        1 * feederExecutor.processRequest(FeederCommand.GLOBAL_SEARCH, requestString) >> responseString
        File responseFile = new File(DEFAULT_RESPONSE_DESTINATION)
        responseFile.exists()
    }

    def "Deliver response to a given file address and validate the output"() {
        given:
        final FeederCommand command = FeederCommand.GLOBAL_SEARCH;
        final String[] requestArgs = [command.toString(), DEFAULT_REQUEST_SOURCE, DEFAULT_RESPONSE_DESTINATION]

        and:"Write request file"
        def requestString='blablabla blablabla blabla bla'
        writeContentToFile(DEFAULT_REQUEST_SOURCE, requestString)

        and:
        def responseString = 'asd'

        when:
        feederMain.processRequest(requestArgs)

        then:
        1 * feederExecutor.processRequest(FeederCommand.GLOBAL_SEARCH, requestString) >> responseString
        File responseFile = new File(DEFAULT_RESPONSE_DESTINATION)
        responseFile.exists()
        responseFile.text == responseString
    }

    private void writeContentToFile(final String fileToWrite, final String contentToWriteToFile){
        def destinationFile=new File(fileToWrite)
        destinationFile.write(contentToWriteToFile);
    }

    private static class SpecificMain extends FeederMain {

        @Override
        protected FeederRequestResponseHandler getFeederRequestResponseHandler() {
            FeederMainTest.feederExecutor
        }
    }

}
