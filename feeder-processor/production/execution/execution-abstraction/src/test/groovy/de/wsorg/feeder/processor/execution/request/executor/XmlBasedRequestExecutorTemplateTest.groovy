package de.wsorg.feeder.processor.execution.request.executor;


import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.execution.request.executor.xmlbased.XmlBasedRequestExecutorTemplate
import de.wsorg.feeder.processor.execution.util.mapper.request.JaxbMarshallAndMappingHelper
import de.wsorg.feeder.processor.execution.util.mapper.request.RequestModelMapper
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.08.12
 */
public class XmlBasedRequestExecutorTemplateTest extends Specification {
    static String jaxbGeneratedClasses = "de.wsorg.lalala"

    static RequestModelMapper requestModelMapperMock
    JaxbMarshallAndMappingHelper jaxbMarshallAndMappingHelper

    TestRequestExecutor requestExecutor

    static LoadFeedRequest providedExecutionParam
    static FeedModel executionResult=new FeedModel()

    def setup(){
        requestExecutor = new TestRequestExecutor()
        requestModelMapperMock = Mock(RequestModelMapper)
        jaxbMarshallAndMappingHelper = Mock(JaxbMarshallAndMappingHelper)

        requestExecutor.jaxbMarshallAndMappingHelper = jaxbMarshallAndMappingHelper

        providedExecutionParam=null
    }

    def "Map the request data and execute the content"() {
        given:
        def requestXml = 'asdasd'

        and:
        def mappedModel = new LoadFeedRequest()

        when:
        def request = requestExecutor.executeRequest(requestXml)

        then:
        request == XmlBasedRequestExecutorTemplateTest.executionResult
        1 * jaxbMarshallAndMappingHelper.getUseCaseModel(requestXml,
                                                             requestModelMapperMock,
                                                             jaxbGeneratedClasses) >> mappedModel
        providedExecutionParam == mappedModel
    }

    private static class TestRequestExecutor extends XmlBasedRequestExecutorTemplate<FeedModel, LoadFeedRequest> {

        @Override
        protected RequestModelMapper getRequestModelMapper() {
            return XmlBasedRequestExecutorTemplateTest.requestModelMapperMock
        }

        @Override
        protected FeedModel executeConcreteRequest(final LoadFeedRequest executionParameter) {
            providedExecutionParam = executionParameter
            return XmlBasedRequestExecutorTemplateTest.executionResult
        }

        @Override
        protected String getJaxbGeneratedPackageName() {
            return XmlBasedRequestExecutorTemplateTest.jaxbGeneratedClasses
        }
    }
}
