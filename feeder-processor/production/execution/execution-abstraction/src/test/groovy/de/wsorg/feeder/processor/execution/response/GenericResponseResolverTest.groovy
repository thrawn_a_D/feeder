package de.wsorg.feeder.processor.execution.response

import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.processor.execution.response.processor.ResponseProcessor
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.09.12
 */
class GenericResponseResolverTest extends Specification {
    GenericResponseResolver genericResponseResolver

    def setup(){
        genericResponseResolver = new GenericResponseResolver()
    }

    def "Resolve response processor for a generic response"() {
        given:
        def responseProcessor = Mock(ResponseProcessor)
        genericResponseResolver.genericResponseProcessor = responseProcessor

        and:
        GenericResponseResult response = new GenericResponseResult()

        when:
        def result = genericResponseResolver.resolveResponseProcessor(response)

        then:
        result == responseProcessor
    }

    def "Resolve a response processor for an exception been thrown"() {
        given:
        def objectThrown = new Exception()

        and:
        def responseProcessor = Mock(ResponseProcessor)
        genericResponseResolver.feederExceptionResponseProcessor = responseProcessor

        when:
        def result = genericResponseResolver.resolveResponseProcessor(objectThrown)

        then:
        result == responseProcessor
    }
}
