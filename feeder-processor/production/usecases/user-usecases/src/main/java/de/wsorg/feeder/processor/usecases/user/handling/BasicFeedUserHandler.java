package de.wsorg.feeder.processor.usecases.user.handling;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.production.domain.user.HandledUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.handling.exception.UserHandlingException;
import de.wsorg.feeder.processor.usecases.util.user.storage.UserStorageProvider;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class BasicFeedUserHandler implements FeedUserHandler {

    @Inject
    private UserStorageProvider userStorageProvider;

    @Override
    public void createUser(final FeederUseCaseUser useCaseUserToCreate) {
        if(nickNameIsSet(useCaseUserToCreate)) {
            final FeederUseCaseUser foundUseCaseUser = isUserAlreadyInStore(useCaseUserToCreate);
            if(nickNameIsFreeToUse(foundUseCaseUser)) {
                if(isPasswordSaveEnough(useCaseUserToCreate)){
                    saveUserInStore(useCaseUserToCreate);
                } else {
                    throwExceptionAsPasswordNotSafe();
                }
            } else {
                throwExceptionAsUserAlreadyExists();
            }
        } else {
            throwExceptionAsNickNameIsEmpty();
        }
    }

    @Override
    public void updateUser(final HandledUseCaseUser useCaseUserToUpdate) {
        FeederUseCaseUser foundUser = userStorageProvider.getUserDataAccess().findByNickName(useCaseUserToUpdate.getUserName());

        userExist(foundUser);

        if(isPasswordSaveEnough(useCaseUserToUpdate)){
            mapUserParamsToUpdate(useCaseUserToUpdate, foundUser);
            userStorageProvider.getUserDataAccess().save(foundUser);
        } else {
            throwExceptionAsPasswordNotSafe();
        }
    }

    private void mapUserParamsToUpdate(final FeederUseCaseUser useCaseUserToUpdate, final FeederUseCaseUser foundUser) {
        foundUser.setFirstName(useCaseUserToUpdate.getFirstName());
        foundUser.setLastName(useCaseUserToUpdate.getLastName());
        foundUser.setEMail(useCaseUserToUpdate.getEMail());
        foundUser.setPassword(useCaseUserToUpdate.getPassword());
    }

    private void userExist(final FeederUseCaseUser foundUser) {
        if (foundUser == null) {
            throw new UserHandlingException("User to update does not exist!");
        }
    }

    private void throwExceptionAsNickNameIsEmpty() {
        throw new UserHandlingException("The provided user has no nickname!");
    }

    private boolean nickNameIsSet(final FeederUseCaseUser useCaseUserToCreate) {
        return StringUtils.isNotBlank(useCaseUserToCreate.getUserName());
    }

    private void throwExceptionAsPasswordNotSafe() {
        throw new UserHandlingException("The provided password is not save enough. It shall not be null or small 8 characters.");
    }

    private boolean isPasswordSaveEnough(final FeederUseCaseUser useCaseUserToCreate) {
        return StringUtils.isNotBlank(useCaseUserToCreate.getPassword()) && useCaseUserToCreate.getPassword().length() >= 8;
    }

    private void throwExceptionAsUserAlreadyExists() {
        throw new UserHandlingException("The provided username is already in use!");
    }

    private void saveUserInStore(final FeederUseCaseUser useCaseUserToCreate) {
        userStorageProvider.getUserDataAccess().save(useCaseUserToCreate);
    }

    private boolean nickNameIsFreeToUse(final FeederUseCaseUser foundUseCaseUser) {
        return foundUseCaseUser == null;
    }

    private FeederUseCaseUser isUserAlreadyInStore(final FeederUseCaseUser useCaseUserToCreate) {
        return userStorageProvider.getUserDataAccess().findByNickName(useCaseUserToCreate.getUserName());
    }

    @Override
    public FeederUseCaseUser findUserByNickName(final String nickName) {
        return userStorageProvider.getUserDataAccess().findByNickName(nickName);
    }
}
