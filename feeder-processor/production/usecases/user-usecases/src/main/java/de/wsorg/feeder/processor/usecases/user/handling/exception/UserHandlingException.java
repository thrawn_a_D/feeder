package de.wsorg.feeder.processor.usecases.user.handling.exception;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserHandlingException extends UseCaseProcessException {
    public UserHandlingException() {
    }

    public UserHandlingException(final String message) {
        super(message);
    }
}