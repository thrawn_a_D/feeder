package de.wsorg.feeder.processor.usecases.user.verify.exception;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserVerificationException extends UseCaseProcessException {
    public UserVerificationException() {
    }

    public UserVerificationException(final String message) {
        super(message);
    }
}
