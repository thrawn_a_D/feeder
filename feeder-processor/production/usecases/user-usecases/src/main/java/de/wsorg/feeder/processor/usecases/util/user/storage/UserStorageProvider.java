package de.wsorg.feeder.processor.usecases.util.user.storage;

import de.wsorg.feeder.processor.storage.user.UserDataAccess;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserStorageProvider {
    UserDataAccess getUserDataAccess();
}
