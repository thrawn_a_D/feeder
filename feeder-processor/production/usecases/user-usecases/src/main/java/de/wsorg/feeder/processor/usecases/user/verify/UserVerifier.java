package de.wsorg.feeder.processor.usecases.user.verify;

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.verify.exception.UserVerificationException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserVerifier {
    FeederUseCaseUser loginUser(final String nickName, final String password) throws UserVerificationException;
}
