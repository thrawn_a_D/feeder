package de.wsorg.feeder.processor.usecases.util.user.authentication;

import javax.inject.Named;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.FeedUserHandlerFactory;
import de.wsorg.feeder.processor.usecases.user.verify.UserVerifier;
import de.wsorg.feeder.processor.usecases.user.verify.exception.UserVerificationException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AuthenticatedUseCaseVerifier {
    private UserVerifier userVerifier;

    public void invoke(final Object params) throws Throwable{
        if(params instanceof AuthorizedRequest) {
            AuthorizedRequest authorizedRequest = (AuthorizedRequest) params;
            final FeederUseCaseUser userLoggedIn = getUserVerifier().loginUser(authorizedRequest.getUserName(),
                                                                               authorizedRequest.getPassword());

            if(!userLoggedIn.getUserId().equals(authorizedRequest.getUserId())) {
                throw new UserVerificationException("The provided user id does not match username and password");
            }
        } else {
            throw new IllegalArgumentException("The provided validation object contains no credential information.");
        }
    }

    public UserVerifier getUserVerifier() {
        if(userVerifier == null) {
            userVerifier = FeedUserHandlerFactory.getUserVerifier();
        }
        return userVerifier;
    }
}
