package de.wsorg.feeder.processor.usecases.util.user.storage;

import de.wsorg.feeder.processor.storage.user.UserDataAccess;
import de.wsorg.feeder.processor.storage.user.UserStorageFactory;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;

import javax.inject.Inject;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserStorageProxy implements UserStorageProvider {
    @Override
    public UserDataAccess getUserDataAccess() {
        return UserStorageFactory.getUserDAO();
    }

    @Inject
    public void setConfigProvider(final StorageConfig storageConfig) {
        UserStorageFactory.setEnableFileBasedStorage(storageConfig.isUseFileSystemAsStorage());
        UserStorageFactory.setClientConfig(storageConfig);
    }
}
