package de.wsorg.feeder.processor.usecases.user;

import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler;
import de.wsorg.feeder.processor.usecases.user.verify.UserVerifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedUserHandlerFactory {
    private static ClassPathXmlApplicationContext applicationContext;
    
    static {
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring/user/userHandlingContext.xml");
    }
    
    public static FeedUserHandler getUserHandler(){
        return applicationContext.getBean(FeedUserHandler.class);
    }

    public static UserVerifier getUserVerifier(){
        return applicationContext.getBean(UserVerifier.class);
    }
}
