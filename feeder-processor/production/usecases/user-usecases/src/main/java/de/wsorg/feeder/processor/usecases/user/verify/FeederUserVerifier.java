package de.wsorg.feeder.processor.usecases.user.verify;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler;
import de.wsorg.feeder.processor.usecases.user.verify.exception.UserVerificationException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederUserVerifier implements UserVerifier {

    @Inject
    private FeedUserHandler feedUserHandler;

    @Override
    public FeederUseCaseUser loginUser(final String nickName, final String password) {
        FeederUseCaseUser foundUseCaseUser = null;

        validateNickname(nickName);
        validatePassword(password);

        FeederUseCaseUser useCaseUserInStore = feedUserHandler.findUserByNickName(nickName);
        if(successfullyFound(useCaseUserInStore)){
            if(isPasswordEquals(password, useCaseUserInStore)){
                foundUseCaseUser = useCaseUserInStore;
            }
        }

        if(foundUseCaseUser == null)
            throw new UserVerificationException("The provided user credentials are not valid");

        return foundUseCaseUser;
    }

    private void validatePassword(final String password) {
        if (StringUtils.isBlank(password)) {
            throw new UseCaseProcessException("The provided password is null or empty!");
        }
    }

    private boolean isPasswordEquals(final String password, final FeederUseCaseUser useCaseUserInStore) {
        return password.equals(useCaseUserInStore.getPassword());
    }

    private boolean successfullyFound(final FeederUseCaseUser useCaseUserInStore) {
        return useCaseUserInStore != null;
    }

    private void validateNickname(final String nickName) {
        if(nickName == null) {
            String nickNameNullError = "The provided nickname is null";
            throw new UseCaseProcessException(nickNameNullError);

        } else  if(StringUtils.isBlank(nickName)) {
            String nickNameEmptyError = "The provided nickname is empty";
            throw new UseCaseProcessException(nickNameEmptyError);
        }
    }
}
