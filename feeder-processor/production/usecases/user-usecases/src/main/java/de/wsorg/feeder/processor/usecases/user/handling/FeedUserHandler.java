package de.wsorg.feeder.processor.usecases.user.handling;

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.production.domain.user.HandledUseCaseUser;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedUserHandler {
    void createUser(final FeederUseCaseUser useCaseUserToCreate);
    void updateUser(final HandledUseCaseUser useCaseUserToCreate);
    FeederUseCaseUser findUserByNickName(final String nickName);
}
