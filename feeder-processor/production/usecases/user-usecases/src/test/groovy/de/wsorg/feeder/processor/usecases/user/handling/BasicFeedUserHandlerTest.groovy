package de.wsorg.feeder.processor.usecases.user.handling

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.production.domain.user.HandledUseCaseUser
import de.wsorg.feeder.processor.storage.user.UserDataAccess
import de.wsorg.feeder.processor.usecases.user.handling.exception.UserHandlingException
import de.wsorg.feeder.processor.usecases.util.user.storage.UserStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.09.12
 */
class BasicFeedUserHandlerTest extends Specification {

    BasicFeedUserHandler feedUserDAO
    UserDataAccess userDataAccess
    UserStorageProvider userStorageProvider

    def setup(){
        feedUserDAO = new BasicFeedUserHandler()
        userDataAccess = Mock(UserDataAccess)
        userStorageProvider = Mock(UserStorageProvider)
        userStorageProvider.getUserDataAccess() >> userDataAccess
        feedUserDAO.userStorageProvider = userStorageProvider
    }

    def "Create a given user"() {
        given:
        FeederUseCaseUser feederUseCaseUser = getDefaultUser()

        when:
        feedUserDAO.createUser(feederUseCaseUser)

        then:
        1 * userDataAccess.save(feederUseCaseUser)
    }

    def "Exception occures expect to throw it again"() {
        given:
        FeederUseCaseUser feederUseCaseUser = getDefaultUser()


        when:
        feedUserDAO.createUser(feederUseCaseUser)

        then:
        thrown(RuntimeException)
        1 * userDataAccess.save(feederUseCaseUser) >> {throw new Exception()}
    }

    def "Provided user already exists"() {
        given:
        FeederUseCaseUser feederUseCaseUser = getDefaultUser()

        and:
        userDataAccess.findByNickName(feederUseCaseUser.userName) >> feederUseCaseUser

        when:
        feedUserDAO.createUser(feederUseCaseUser)

        then:
        def ex = thrown(UserHandlingException)
        ex.message == "The provided username is already in use!"
    }

    def "Provided password is empty"() {
        given:
        FeederUseCaseUser feederUseCaseUser = getDefaultUser()
        feederUseCaseUser.password = ''

        when:
        feedUserDAO.createUser(feederUseCaseUser)

        then:
        def ex = thrown(UserHandlingException)
        ex.message == "The provided password is not save enough. It shall not be null or small 8 characters."
    }

    def "Find a user by his nickname"() {
        given:
        def nickName = 'nickname'

        and:
        def foundUser=new FeederUseCaseUser()

        when:
        def result = feedUserDAO.findUserByNickName(nickName)

        then:
        foundUser == result
        1 * userDataAccess.findByNickName(nickName) >> foundUser
    }

    def "No valid nickname provided"() {
        given:
        def user = getDefaultUser()
        user.userName = ''

        when:
        feedUserDAO.createUser(user)

        then:
        def ex = thrown(UserHandlingException)
        ex.message == "The provided user has no nickname!"
    }

    def "Update an existing user"() {
        given:
        HandledUseCaseUser feederUseCaseUser = getDefaultUserToHandle()

        and:
        def existingUser = new FeederUseCaseUser()

        when:
        feedUserDAO.updateUser(feederUseCaseUser)

        then:
        1 * userDataAccess.findByNickName(feederUseCaseUser.userName) >> existingUser
        1 * userDataAccess.save({it instanceof FeederUseCaseUser})
    }

    def "Check if user exists before update"() {
        given:
        HandledUseCaseUser feederUseCaseUser = getDefaultUserToHandle()

        and:
        userDataAccess.findByNickName(feederUseCaseUser.userName) >> null

        when:
        feedUserDAO.updateUser(feederUseCaseUser)

        then:
        def ex = thrown(UserHandlingException)
        ex.message == "User to update does not exist!"
        0 * userDataAccess.save(_)
    }

    def "User is not allowed to change the nick name"() {
        given:
        HandledUseCaseUser feederUseCaseUser = getDefaultUserToHandle()

        and:
        def existingUser = new FeederUseCaseUser(userName: 'nickname_orig')

        when:
        feedUserDAO.updateUser(feederUseCaseUser)

        then:
        1 * userDataAccess.findByNickName(feederUseCaseUser.userName) >> existingUser
        1 * userDataAccess.save({it.userName == existingUser.userName})
    }

    def "Check password of the updated user"() {
        given:
        HandledUseCaseUser feederUseCaseUser = getDefaultUserToHandle()
        feederUseCaseUser.password = 'asd'

        and:
        def existingUser = new FeederUseCaseUser()

        and:
        userDataAccess.findByNickName(feederUseCaseUser.userName) >> existingUser

        when:
        feedUserDAO.updateUser(feederUseCaseUser)

        then:
        0 * userDataAccess.save(_)
        def ex = thrown(UserHandlingException)
        ex.message == "The provided password is not save enough. It shall not be null or small 8 characters."
    }

    private FeederUseCaseUser getDefaultUser(){
        FeederUseCaseUser FeederUser = new FeederUseCaseUser(firstName: 'first',
                                                                            lastName: 'last',
                                                                            eMail: 'eMail',
                                                                            userName: 'nickname',
                                                                            password: 'passwd08')
    }

    private HandledUseCaseUser getDefaultUserToHandle(){
        HandledUseCaseUser FeederUser = new HandledUseCaseUser(firstName: 'first',
                                                                lastName: 'last',
                                                                EMail: 'eMail',
                                                                userName: 'nickname',
                                                                password: 'passwd08')
    }
}
