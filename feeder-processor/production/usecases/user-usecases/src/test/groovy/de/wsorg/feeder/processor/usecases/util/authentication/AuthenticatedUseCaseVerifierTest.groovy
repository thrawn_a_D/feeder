package de.wsorg.feeder.processor.usecases.util.authentication

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.usecases.user.verify.UserVerifier
import de.wsorg.feeder.processor.usecases.user.verify.exception.UserVerificationException
import de.wsorg.feeder.processor.usecases.util.user.authentication.AuthenticatedUseCaseVerifier
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.01.13
 */
class AuthenticatedUseCaseVerifierTest extends Specification {
    AuthenticatedUseCaseVerifier authenticatedUseCaseVerifier
    UserVerifier userVerifier

    def setup(){
        authenticatedUseCaseVerifier = new AuthenticatedUseCaseVerifier()
        userVerifier = Mock(UserVerifier)
        authenticatedUseCaseVerifier.userVerifier = userVerifier
    }

    def "Verify users credentials before the actual execution"() {
        given:
        def userId = '123'
        def providedParam = Mock(AuthorizedRequest)
        providedParam.userName >> 'usern'
        providedParam.password >> 'passwd'
        providedParam.userId >> userId

        and:
        def userAuthentication = new FeederUseCaseUser()
        userAuthentication.userId = userId

        when:
        authenticatedUseCaseVerifier.invoke(providedParam)

        then:
        1 * userVerifier.loginUser(providedParam.userName, providedParam.password) >> userAuthentication
    }

    def "Verify users credentials before the actual execution and make sure userId matches the calculated user by username and password"() {
        given:
        def providedParam = Mock(AuthorizedRequest)
        providedParam.userName >> 'usern'
        providedParam.password >> 'passwd'
        providedParam.userId >> '123'

        and:
        def userAuthentication = new FeederUseCaseUser()
        userAuthentication.userId = '23423'

        and:
        userVerifier.loginUser(providedParam.userName, providedParam.password) >> userAuthentication

        when:
        authenticatedUseCaseVerifier.invoke(providedParam)

        then:
        def ex = thrown(UserVerificationException)
        ex.message == 'The provided user id does not match username and password'
    }

    def "Users credentials are invalid"() {
        given:
        def providedParam = Mock(AuthorizedRequest)
        providedParam.userName >> 'usern'
        providedParam.password >> 'passwd'

        and:
        userVerifier.loginUser(providedParam.userName, providedParam.password) >> { throw new UserVerificationException() }

        when:
        authenticatedUseCaseVerifier.invoke(providedParam)

        then:
        thrown(UserVerificationException)
    }

    def "Provided object is not of expected type"() {
        given:
        def providedParam = ''

        when:
        authenticatedUseCaseVerifier.invoke(providedParam)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided validation object contains no credential information.'
    }
}
