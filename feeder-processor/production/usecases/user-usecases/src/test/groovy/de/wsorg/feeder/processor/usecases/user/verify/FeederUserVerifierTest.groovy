package de.wsorg.feeder.processor.usecases.user.verify

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.usecases.user.handling.FeedUserHandler
import de.wsorg.feeder.processor.usecases.user.verify.exception.UserVerificationException
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 22.09.12
 */
class FeederUserVerifierTest extends Specification {
    FeederUserVerifier feederUserVerificator
    FeedUserHandler userHandler

    def setup(){
        feederUserVerificator = new FeederUserVerifier()
        userHandler = Mock(FeedUserHandler)
        feederUserVerificator.feedUserHandler = userHandler
    }

    def "Verify that a user truly exists and password matches"() {
        given:
        def nickName = 'nickName'
        def password = 'password'

        and:
        def foundUser = new FeederUseCaseUser(userName: nickName, password: password)

        when:
        def loggedInUser = feederUserVerificator.loginUser(nickName, password)

        then:
        1 * userHandler.findUserByNickName(nickName) >> foundUser
        loggedInUser.userName == nickName
        loggedInUser.password == password
    }

    def "Nickname is null but password is set"() {
        def password = 'password'

        when:
        def loggedInUser = feederUserVerificator.loginUser(null, password)

        then:
        def ex = thrown(UseCaseProcessException)
        ex.message == "The provided nickname is null"
    }

    def "Nickname is empty but password is set"() {
        def password = 'password'

        when:
        def loggedInUser = feederUserVerificator.loginUser('', password)

        then:
        def ex = thrown(UseCaseProcessException)
        ex.message == "The provided nickname is empty"
    }

    def "No user found for this login"() {
        given:
        def nickName = 'nickName'
        def password = 'password'


        when:
        feederUserVerificator.loginUser(nickName, password)

        then:
        def ex = thrown(UserVerificationException)
        ex.message == "The provided user credentials are not valid"
        1 * userHandler.findUserByNickName(nickName) >> null
    }

    def "Provided password is null"() {
        given:
        def nickName = 'nickName'
        def password = null


        when:
        feederUserVerificator.loginUser(nickName, password)

        then:
        def ex = thrown(UseCaseProcessException)
        ex.message == "The provided password is null or empty!"
    }

    def "Verify user which exists and password does not matches"() {
        given:
        def nickName = 'nickName'
        def password = 'password'

        and:
        def foundUser = new FeederUseCaseUser(userName: nickName, password: 'someOtherPass')

        when:
        feederUserVerificator.loginUser(nickName, password)

        then:
        def ex = thrown(UserVerificationException)
        ex.message == "The provided user credentials are not valid"
        1 * userHandler.findUserByNickName(nickName) >> foundUser
    }
}
