package de.wsorg.feeder.processor.usecases.util.feed.url;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BasicUrlTypeIdentifier implements UrlTypeIdentifier {
    public static final String TIME_LINE_FEED_IDENTIFIER = "feeder.net/timeline";
    public static final String NORMAL_INTERNAL_FEED_IDENTIFIER = "feeder.net";

    @Override
    public UrlType determinateUrlType(final String feedUrl) {
        if(feedUrl.toLowerCase().contains(TIME_LINE_FEED_IDENTIFIER))
            return UrlType.INTERNAL_TIME_LINE;
        else if(feedUrl.toLowerCase().contains(NORMAL_INTERNAL_FEED_IDENTIFIER))
            return UrlType.NORMAL_INTERNAL;
        else
            return UrlType.NORMAL;
    }
}
