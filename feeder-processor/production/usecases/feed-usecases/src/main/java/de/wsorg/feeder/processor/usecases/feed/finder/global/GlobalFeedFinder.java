package de.wsorg.feeder.processor.usecases.feed.finder.global;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import de.wsorg.feeder.processor.usecases.feed.finder.FeedFinder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface GlobalFeedFinder<T extends GlobalSearchQuery> extends FeedFinder<FeedModel, T> {
}
