package de.wsorg.feeder.processor.usecases.feed.finder.global;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class LocalFeedFinder implements GlobalFeedFinder<GlobalSearchQuery> {
    Logger LOGGER = LoggerFactory.getLogger(LocalFeedFinder.class);

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public List<FeedModel> findFeeds(final GlobalSearchQuery queryToUse) {
        LOGGER.debug("Search for feeds using a local feed finder as source.");

        List<FeedModel> allLocalFeeds = useCaseStorageProvider.getFeedDataAccess().findByTitleOrDescription(queryToUse.getSearchTerm());

        LOGGER.debug("Search operation finished. Found " + allLocalFeeds.size() + " feeds.");

        return allLocalFeeds;
    }
}
