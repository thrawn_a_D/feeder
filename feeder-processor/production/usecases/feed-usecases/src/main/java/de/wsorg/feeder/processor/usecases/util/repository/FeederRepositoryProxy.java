package de.wsorg.feeder.processor.usecases.util.repository;

import de.wsorg.feeder.processor.updater.repository.FeedRepository;
import de.wsorg.feeder.processor.updater.repository.FeedRepositoryFactory;
import de.wsorg.feeder.processor.updater.repository.UserFeedRepository;
import de.wsorg.feeder.processor.updater.repository.utils.config.RepositoryConfig;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederRepositoryProxy implements FeederRepositoryProvider {
    @Override
    public FeedRepository getFeedRepository() {
        return FeedRepositoryFactory.getFeedRepository();
    }

    @Override
    public UserFeedRepository getUserFeedRepository() {
        return FeedRepositoryFactory.getUserFeedRepository();
    }

    public void setConfiguration(final RepositoryConfig repositoryConfig) {
        final boolean useFileSystemAsStorage = repositoryConfig.getStorageConfig().isUseFileSystemAsStorage();
        FeedRepositoryFactory.setUseFileSystemAsStorage(useFileSystemAsStorage);

        FeedRepositoryFactory.setClientConfig(repositoryConfig);
    }
}
