package de.wsorg.feeder.processor.usecases.feed.finder.global.source;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.OPML;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;
import de.wsorg.feeder.processor.usecases.util.feed.mapper.opml.OpmlToFeedMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 *
 */
public class DigitalPodcastFeedFinder implements GlobalFeedFinder<GlobalSearchQuery> {
    Logger LOGGER = LoggerFactory.getLogger(DigitalPodcastFeedFinder.class);

    static final String APPLICATION_ID = "77f10bd2d062186f034027810ecc4d9e";
    static final String FORMAT = "rssopml";
    static final String SORT = "rating";
    static final String SEARCH_SOURCE = "title";
    static final String CONTENT_FILTER = "nofilter";

    static final String FIND_FEED_GET = "http://www.digitalpodcast.com/podcastsearchservice/v2b/search/?appid={appId}&keywords={keyWords}&format={format}&sort={sort}&searchsource={searchSource}&contentfilter={contentFilter}";

    @Inject
    private RestTemplate restTemplate = null;

    @Inject
    private OpmlToFeedMapper opmlToFeedMapper = null;

    @Override
    public List<FeedModel> findFeeds(GlobalSearchQuery queryToUse) {
        LOGGER.debug("Search for feeds using DigitalPodcast api as source.");

        List<FeedModel> searchResult = new ArrayList<>();

        if(StringUtils.isNotBlank(queryToUse.getSearchTerm())){
            Map<String, String> requestVars = getRequestVars(queryToUse);
            OPML res = performRestCall(requestVars);
            searchResult = opmlToFeedMapper.getFeedByOpml(res);
        }

        LOGGER.debug("Search operation finished. Found " + searchResult.size() + " feeds.");

        return searchResult;
    }

    private OPML performRestCall(Map<String, String> requestVars) {
        return restTemplate.getForObject(DigitalPodcastFeedFinder.FIND_FEED_GET, OPML.class, requestVars);
    }

    private Map<String, String> getRequestVars(GlobalSearchQuery queryToUse) {
        Map<String, String> requestVars = this.getBasicRequestVars();
        requestVars.put("keyWords", queryToUse.getSearchTerm());
        return requestVars;
    }

    private Map<String, String> getBasicRequestVars(){
        Map<String, String> requestVars = new HashMap<String, String>();

        requestVars.put("appId", DigitalPodcastFeedFinder.APPLICATION_ID);
        requestVars.put("format", DigitalPodcastFeedFinder.FORMAT);
        requestVars.put("sort", DigitalPodcastFeedFinder.SORT);
        requestVars.put("searchSource", DigitalPodcastFeedFinder.SEARCH_SOURCE);
        requestVars.put("contentFilter", DigitalPodcastFeedFinder.CONTENT_FILTER);

        return requestVars;
    }
}
