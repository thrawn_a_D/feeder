package de.wsorg.feeder.processor.usecases.feed.loader;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed;
import de.wsorg.feeder.processor.usecases.feed.loader.user.UserFeedLoader;
import de.wsorg.feeder.processor.usecases.feed.loader.user.timeline.FeedsTimeLineLoader;
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlType;
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlTypeIdentifier;
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("feedSourceSwitcher")
public class FeedSourceSwitcher implements CommonFeedLoader {

    @Inject
    private FeederRepositoryProvider feederRepositoryProvider;

    @Inject
    @Named("commonUserFeedLoader")
    private UserFeedLoader commonUserFeedLoader;

    @Inject
    @Named("userFeedsTimeLineLoader")
    private FeedsTimeLineLoader timeLineLoader;

    @Inject
    private UrlTypeIdentifier urlTypeIdentifier;

    @Override
    public FeedModel loadFeed(final LoadFeed feedLoadingMetrics) {
        final UrlType feedSourceToUse = urlTypeIdentifier.determinateUrlType(feedLoadingMetrics.getFeedUrl());

        if(feedSourceToUse == UrlType.NORMAL) {
            if(feedLoadingMetrics instanceof UserRelatedLoadFeed) {
                return commonUserFeedLoader.loadFeed((UserRelatedLoadFeed) feedLoadingMetrics);
            } else {
                return feederRepositoryProvider.getFeedRepository().loadFeed(feedLoadingMetrics.getFeedUrl());
            }
        } else if(feedSourceToUse == UrlType.INTERNAL_TIME_LINE) {
            if(feedLoadingMetrics instanceof UserRelatedLoadFeed) {
                UserRelatedLoadFeed userRelatedFeedLoadMetrics = (UserRelatedLoadFeed) feedLoadingMetrics;
                return timeLineLoader.loadFeed(userRelatedFeedLoadMetrics);
            } else {
                throw new IllegalArgumentException("Time line feed wanted but no user related search model provided.");
            }
        } else {
            throw new IllegalArgumentException("The provided maps top internal feeds. This functionality is not yet supported");
        }
    }
}
