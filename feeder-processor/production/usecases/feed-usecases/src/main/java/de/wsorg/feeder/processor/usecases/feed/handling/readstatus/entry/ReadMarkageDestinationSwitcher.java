package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry;

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlType;
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlTypeIdentifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ReadMarkageDestinationSwitcher implements FeedEntryReadMarker {
    final Logger LOGGER = LoggerFactory.getLogger(ReadMarkageDestinationSwitcher.class);
    @Inject
    private UrlTypeIdentifier urlTypeIdentifier;
    @Inject
    @Named("basicFeedEntryMarker")
    private FeedEntryReadMarker normalFeedEntryMarker;
    @Inject
    @Named("timeLineFeedEntryMarker")
    private FeedEntryReadMarker timeLineFeedEntryMarker;

    @Override
    public void markFeedEntries(final MarkFeedEntryReadStatusHandling feedEntryReadStatusHandling) {
        final String feedUrl = feedEntryReadStatusHandling.getFeedUrl();
        final String userId = feedEntryReadStatusHandling.getUserId();

        LOGGER.debug("###########################################################################");
        LOGGER.debug("# Set read status for feed: {} and userId: {}", feedUrl, userId);
        LOGGER.debug("###########################################################################");

        if(urlTypeIdentifier.determinateUrlType(feedUrl) == UrlType.INTERNAL_TIME_LINE){
            LOGGER.debug("Set feed read status related to a time line feed.");
            timeLineFeedEntryMarker.markFeedEntries(feedEntryReadStatusHandling);
        } else {
            LOGGER.debug("Set feed read status related to an ordinary feed.");
            normalFeedEntryMarker.markFeedEntries(feedEntryReadStatusHandling);
        }
    }
}
