package de.wsorg.feeder.processor.usecases.feed.loader;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import de.wsorg.feeder.processor.usecases.feed.loader.modification.FeedModificatorDelegator;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("feedLoaderAndModifier")
public class FeedLoaderAndModifier implements CommonFeedLoader {

    @Inject
    @Named("feedSourceSwitcher")
    private CommonFeedLoader feedLoader;

    @Inject
    private FeedModificatorDelegator modificatorDelegator;

    @Override
    public FeedModel loadFeed(final LoadFeed feedLoadingMetrics) {
        FeedModel loadedFeedModel = feedLoader.loadFeed(feedLoadingMetrics);
        return modificatorDelegator.modifyFeed(loadedFeedModel, feedLoadingMetrics);
    }
}
