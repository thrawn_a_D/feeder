package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all;

import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface AllFeedsReadMarker {
    /**
     * Mark feed entries using the given settings provided in the map.
     */
    void markAllFeeds(final MarkAllFeedsReadStatusHandling feedEntryReadStatusHandling);
}
