package de.wsorg.feeder.processor.usecases.feed.finder.global;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AggregatedFeedFinder implements GlobalFeedFinder<GlobalSearchQuery> {
    @Setter
    private List<GlobalFeedFinder> sourcesToUseForSearch = new ArrayList<GlobalFeedFinder>();

    @Override
    public List<FeedModel> findFeeds(GlobalSearchQuery queryToUse) {
        List<FeedModel> result = new ArrayList<FeedModel>();

        for(GlobalFeedFinder finderGlobal : sourcesToUseForSearch) {
            List<FeedModel> foundFeedModels = finderGlobal.findFeeds(queryToUse);
            result.addAll(foundFeedModels);
        }

        return result;
    }
}
