package de.wsorg.feeder.processor.usecases.feed.loader.user.timeline;

import de.wsorg.feeder.processor.usecases.feed.loader.user.UserFeedLoader;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedsTimeLineLoader extends UserFeedLoader {
}
