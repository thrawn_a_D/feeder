package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BasicFeedEntryMarker implements FeedEntryReadMarker {
    final Logger LOGGER = LoggerFactory.getLogger(BasicFeedEntryMarker.class);

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public void markFeedEntries(final MarkFeedEntryReadStatusHandling feedEntryReadStatusHandling) {
        final String feedUrl = feedEntryReadStatusHandling.getFeedUrl();
        final String userId = feedEntryReadStatusHandling.getUserId();
        final Map<String, Boolean> feedEntriesToMark = feedEntryReadStatusHandling.getFeedEntryUidWithReadabilityStatus();
        boolean associationGiven = useCaseStorageProvider.getFeedToUserAssociationStorage().isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER);

        if (associationGiven) {
            FeedToUserAssociation association = useCaseStorageProvider.getFeedToUserAssociationStorage().getUserFeedAssociation(feedUrl, userId);

            for (String uidOfFeedEntryToMark : feedEntriesToMark.keySet()) {
                final Boolean isFeadAlreadyRead = feedEntriesToMark.get(uidOfFeedEntryToMark);

                if(isFeadAlreadyRead) {
                    markAsRead(association, uidOfFeedEntryToMark, isFeadAlreadyRead);
                } else {
                    feedNotReadSoCheckAndRemoveExistingMarkage(association, uidOfFeedEntryToMark);
                }

            }

            useCaseStorageProvider.getFeedToUserAssociationStorage().save(association);
            LOGGER.debug("Feed entries marked as read (count):" + feedEntriesToMark.size());
        } else {
            String message = "Could not mark feed entries as read, as no subscription to this feed is currently given (userId: "+userId+", feedUrl:"+feedUrl+").";
            LOGGER.debug(message, userId, feedUrl);
            throw new UseCaseProcessException(message);
        }
    }

    private void markAsRead(final FeedToUserAssociation association, final String uidOfFeedEntryToMark, final Boolean feadAlreadyRead) {
        association.getFeedEntriesReadStatus().put(uidOfFeedEntryToMark, feadAlreadyRead);
    }

    private void feedNotReadSoCheckAndRemoveExistingMarkage(final FeedToUserAssociation association, final String uidOfFeedEntryToMark) {
        if(association.getFeedEntriesReadStatus().containsKey(uidOfFeedEntryToMark)){
            association.getFeedEntriesReadStatus().remove(uidOfFeedEntryToMark);
        }
    }
}
