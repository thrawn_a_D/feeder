package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry;

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class TimeLineFeedEntryMarker implements FeedEntryReadMarker {
    final Logger LOGGER = LoggerFactory.getLogger(TimeLineFeedEntryMarker.class);

    @Inject
    @Named("basicFeedEntryMarker")
    private FeedEntryReadMarker singleFeedEntryMarker;

    @Override
    public void markFeedEntries(final MarkFeedEntryReadStatusHandling feedEntryReadStatusHandling) {
        final Map<String, Boolean> feedEntriesToMark = feedEntryReadStatusHandling.getFeedEntryUidWithReadabilityStatus();
        final Map<String, Map<String, Boolean>> feedsWithFeedEntryReadMarkage = seperateMapFeedReadStatus(feedEntriesToMark);

        for (final String actualFeedUrl : feedsWithFeedEntryReadMarkage.keySet()) {
            LOGGER.debug("Set read status for url: {}", actualFeedUrl);
            final Map<String, Boolean> feedEntryReadStatusMap = feedsWithFeedEntryReadMarkage.get(actualFeedUrl);

            final MarkFeedEntryReadStatusHandling actualFeedEntryReadStatusHandling = new MarkFeedEntryReadStatusHandling();
            actualFeedEntryReadStatusHandling.setFeedUrl(actualFeedUrl);
            actualFeedEntryReadStatusHandling.setUserId(feedEntryReadStatusHandling.getUserId());
            actualFeedEntryReadStatusHandling.setUserName(feedEntryReadStatusHandling.getUserName());
            actualFeedEntryReadStatusHandling.setPassword(feedEntryReadStatusHandling.getPassword());
            actualFeedEntryReadStatusHandling.setFeedEntryUidWithReadabilityStatus(feedEntryReadStatusMap);

            singleFeedEntryMarker.markFeedEntries(actualFeedEntryReadStatusHandling);
            LOGGER.debug("Total entries marked: {}", feedEntryReadStatusMap.size());
        }
    }

    private Map<String, Map<String, Boolean>> seperateMapFeedReadStatus(final Map<String, Boolean> feedEntriesToMark) {
        final Map<String, Map<String, Boolean>> feedsWithFeedEntryReadMarkage = new HashMap<>();

        for (final String feedIdFeedEntryId : feedEntriesToMark.keySet()) {
            if (feedIdFeedEntryId.contains("@")) {
                final String[] feedIds = feedIdFeedEntryId.split("@");
                final String parentFeedUrl = feedIds[0];
                final String feedEntryId = feedIds[1];
                final Boolean feedEntryReadStatus = feedEntriesToMark.get(feedIdFeedEntryId);

                checkIfFeedAlreadyInitialized(feedsWithFeedEntryReadMarkage, parentFeedUrl);
                feedsWithFeedEntryReadMarkage.get(parentFeedUrl).put(feedEntryId, feedEntryReadStatus);
            } else {
                String message = "The provided feed entry of the time line does not contain the apropriate separator (@)";
                throw new IllegalArgumentException(message);
            }
        }
        return feedsWithFeedEntryReadMarkage;
    }

    private void checkIfFeedAlreadyInitialized(final Map<String, Map<String, Boolean>> feedsWithFeedEntryReadMarkage, final String parentFeedUrl) {
        if(!feedsWithFeedEntryReadMarkage.containsKey(parentFeedUrl)){
            Map<String, Boolean> feedEntryReadMarkage = new HashMap<>();
            feedsWithFeedEntryReadMarkage.put(parentFeedUrl, feedEntryReadMarkage);
        }
    }
}
