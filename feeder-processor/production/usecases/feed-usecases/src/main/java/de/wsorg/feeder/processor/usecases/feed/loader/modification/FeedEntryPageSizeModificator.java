package de.wsorg.feeder.processor.usecases.feed.loader.modification;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedEntryPageSizeModificator implements FeedModificator {
    final Logger LOGGER = LoggerFactory.getLogger(FeedEntryPageSizeModificator.class);

    @Override
    public FeedModel modifyFeed(final FeedModel feedToModify, final LoadFeed modificationParameter) {
        LOGGER.debug("Start modifying paging size with paging range : {}", modificationParameter.getFeedEntryPagingRange());

        if(modificationParameter.getFeedEntryPagingRange() != null &&
           isThereSomethingToBeCut(feedToModify)) {
            validatePageRanges(modificationParameter);

            if(noPagingSet(modificationParameter)) {
                return feedToModify;
            } else {
                final int startIndex = getStartIndex(modificationParameter);
                final int endIndex = getEndIndex(feedToModify, modificationParameter);

                final List<FeedEntryModel> cuttedList = feedToModify.getFeedEntryModels().subList(startIndex, endIndex);
                setIsPagingStillPossible(feedToModify, cuttedList);
                feedToModify.setFeedEntryModels(cuttedList);
                return feedToModify;
            }
        } else {
            return feedToModify;
        }
    }

    private int getStartIndex(final LoadFeed modificationParameter) {
        return modificationParameter.getFeedEntryPagingRange().getStartIndex()-1;
    }

    private int getEndIndex(final FeedModel feedToModify, final LoadFeed modificationParameter) {
        int endIndex = modificationParameter.getFeedEntryPagingRange().getEndIndex()-1;

        if(endIndex >= feedToModify.getFeedEntryModels().size())
            endIndex = feedToModify.getFeedEntryModels().size();
        return endIndex;
    }

    private boolean isThereSomethingToBeCut(final FeedModel feedToModify) {
        return feedToModify != null &&
                feedToModify.getFeedEntryModels() != null &&
                feedToModify.getFeedEntryModels().size() > 0;
    }

    private void setIsPagingStillPossible(final FeedModel feedToModify, final List<FeedEntryModel> cuttedList) {
        final boolean weHaveItemsLeftForPaging = cuttedList.size() != feedToModify.getFeedEntryModels().size();
        feedToModify.getFeedEntriesCollection().setAdditionalPagingPossible(weHaveItemsLeftForPaging);
    }

    private void validatePageRanges(final LoadFeed modificationParameter) {
        validateStartingPageRange(modificationParameter);
    }

    private void validateStartingPageRange(final LoadFeed modificationParameter) {
        if(modificationParameter.getFeedEntryPagingRange() != null &&
           modificationParameter.getFeedEntryPagingRange().getStartIndex() <1) {
            final String message = "The provided page range is invalid! Starting page is under 1.";
            LOGGER.error(message);
            throw new IllegalArgumentException(message);
        }
    }

    private boolean noPagingSet(final LoadFeed modificationParameter) {
        return modificationParameter.getFeedEntryPagingRange().getStartIndex() == 1 &&
           modificationParameter.getFeedEntryPagingRange().getEndIndex() == Integer.MAX_VALUE;
    }
}
