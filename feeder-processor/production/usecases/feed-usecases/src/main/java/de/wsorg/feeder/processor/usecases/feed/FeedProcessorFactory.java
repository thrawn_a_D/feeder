package de.wsorg.feeder.processor.usecases.feed;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;
import de.wsorg.feeder.processor.usecases.feed.finder.userrelated.UserRelatedFeedFinder;
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all.AllFeedsReadMarker;
import de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry.FeedEntryReadMarker;
import de.wsorg.feeder.processor.usecases.feed.handling.subscribtion.FeedSubscriptionHandler;
import de.wsorg.feeder.processor.usecases.feed.loader.CommonFeedLoader;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedProcessorFactory {

    private static ClassPathXmlApplicationContext applicationContext;

    static
    {
        String[] contextFiles = new String[]{"classpath:spring/feed/feederCoreContext.xml"};
        applicationContext = new ClassPathXmlApplicationContext(contextFiles);
    }

    public static GlobalFeedFinder getGlobalFeedFinder(){
        return applicationContext.getBean("mainFeedFinder", GlobalFeedFinder.class);
    }

    public static GlobalFeedFinder getGlobalFeedFinderEnrichedWithData(){
        return applicationContext.getBean("mainFeedFinderEnrichedWithUserData", GlobalFeedFinder.class);
    }

    public static UserRelatedFeedFinder getUserRelatedFeedFinder(){
        return applicationContext.getBean(UserRelatedFeedFinder.class);
    }

    public static CommonFeedLoader getFeedLoader(){
        return applicationContext.getBean("feedLoaderAndModifier", CommonFeedLoader.class);
    }

    public static FeedDataAccess getLocalFeedStorage(){
        return applicationContext.getBean(UseCaseStorageProvider.class).getFeedDataAccess();
    }

    public static FeedSubscriptionHandler getFeedFavourModifier(){
        return applicationContext.getBean(FeedSubscriptionHandler.class);
    }

    public static FeedEntryReadMarker getFeedEntryReadMarker(){
        return applicationContext.getBean("readMarkageDestinationSwitcher", FeedEntryReadMarker.class);
    }

    public static AllFeedsReadMarker getAllFeedsReadMarker() {
        return applicationContext.getBean("basicAllFeedsReadMarker", AllFeedsReadMarker.class);
    }
}
