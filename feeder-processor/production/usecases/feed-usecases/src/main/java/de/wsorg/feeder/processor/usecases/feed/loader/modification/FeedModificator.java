package de.wsorg.feeder.processor.usecases.feed.loader.modification;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedModificator {
    public FeedModel modifyFeed(final FeedModel feedToModify, final LoadFeed modificationParameter);
}
