package de.wsorg.feeder.processor.usecases.util.wrapper;

import javax.inject.Named;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class JavaUrlWrapper {
    public HttpURLConnection getUrlConnection(final String url) throws IOException {
        return (HttpURLConnection) new URL(url).openConnection();
    }
}
