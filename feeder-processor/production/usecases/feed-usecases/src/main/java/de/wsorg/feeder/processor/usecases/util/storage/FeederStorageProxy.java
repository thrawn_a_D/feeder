package de.wsorg.feeder.processor.usecases.util.storage;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederStorageProxy implements UseCaseStorageProvider {

    public FeedDataAccess getFeedDataAccess() {
        return FeedStorageFactory.getFeedDAO();
    }

    public UsersFeedsStorage getUsersFeedsStorage() {
        return FeedStorageFactory.getUserFeedsDAO();
    }

    public FeedToUserAssociationStorage getFeedToUserAssociationStorage() {
        return FeedStorageFactory.getFeedToUserAssociationDAO();
    }

    public void setStorageConfig(final StorageConfig configProvider) {
        final boolean useFileSystemAsStorage = configProvider.isUseFileSystemAsStorage();
        FeedStorageFactory.setEnableFileBasedStorage(useFileSystemAsStorage);

        FeedStorageFactory.setClientConfig(configProvider);
    }
}
