package de.wsorg.feeder.processor.usecases.feed.modifier;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedModifier<T> {
    /**
     * The feed adjustment is performed for a user with the given id.
     * If no id is provided, no adjustment is performed. In addition
     * to this. The provided user must already have an ownership
     * association with the feed to be modified.
     *
     * @param feedUrl
     * @param userId
     * @param value
     */
    void adjustFeed(final String feedUrl, final String userId, final T value);
}
