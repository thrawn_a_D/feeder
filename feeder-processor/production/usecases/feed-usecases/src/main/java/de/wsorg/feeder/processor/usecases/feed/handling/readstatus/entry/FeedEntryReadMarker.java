package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry;

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedEntryReadMarker {
    /**
     * Mark feed entries using the given settings provided in the map.
     */
    void markFeedEntries(final MarkFeedEntryReadStatusHandling feedEntryReadStatusHandling);
}
