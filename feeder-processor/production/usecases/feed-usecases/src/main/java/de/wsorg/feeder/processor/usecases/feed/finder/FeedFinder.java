package de.wsorg.feeder.processor.usecases.feed.finder;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.common.FeederSearchQuery;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedFinder<F extends FeedModel, T extends FeederSearchQuery> {
    List<F> findFeeds(T queryToUse);
}
