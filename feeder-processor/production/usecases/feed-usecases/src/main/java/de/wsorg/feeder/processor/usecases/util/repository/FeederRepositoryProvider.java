package de.wsorg.feeder.processor.usecases.util.repository;

import de.wsorg.feeder.processor.updater.repository.FeedRepository;
import de.wsorg.feeder.processor.updater.repository.UserFeedRepository;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederRepositoryProvider {
    FeedRepository getFeedRepository();
    UserFeedRepository getUserFeedRepository();
}
