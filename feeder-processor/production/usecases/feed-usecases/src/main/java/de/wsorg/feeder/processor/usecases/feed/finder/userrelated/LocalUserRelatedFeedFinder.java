package de.wsorg.feeder.processor.usecases.feed.finder.userrelated;

import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserRelatedSearchQuery;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class LocalUserRelatedFeedFinder implements UserRelatedFeedFinder {
    Logger LOGGER = LoggerFactory.getLogger(LocalUserRelatedFeedFinder.class);

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public List<UserRelatedFeedModel> findFeeds(final UserRelatedSearchQuery queryToUse) {
        List<UserRelatedFeedModel> userFeeds = new ArrayList<>();
        LOGGER.debug("Perform user related search!");

        if(StringUtils.isNotBlank(queryToUse.getSearchTerm())) {
            userFeeds = useCaseStorageProvider.getUsersFeedsStorage().getUserFeeds(queryToUse.getUserId(), queryToUse.getSearchTerm());
        }
        else
            userFeeds = useCaseStorageProvider.getUsersFeedsStorage().getUserFeeds(queryToUse.getUserId());

        LOGGER.debug("User related search finished. Got " + userFeeds.size() + " feeds.");

        return userFeeds;
    }
}
