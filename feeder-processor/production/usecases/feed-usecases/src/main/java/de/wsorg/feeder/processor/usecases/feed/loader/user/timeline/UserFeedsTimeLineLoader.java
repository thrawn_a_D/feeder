package de.wsorg.feeder.processor.usecases.feed.loader.user.timeline;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed;
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserFeedsTimeLineLoader implements FeedsTimeLineLoader {
    final Logger LOGGER = LoggerFactory.getLogger(UserFeedsTimeLineLoader.class);

    @Inject
    private FeederRepositoryProvider feederRepositoryProvider;

    @Override
    public FeedModel loadFeed(final UserRelatedLoadFeed feedIdentification) {
        LOGGER.debug("Load users ({}) feed time line.", feedIdentification.getUserId());

        UserRelatedFeedModel userFeedsAsTimeLine = new UserRelatedFeedModel();
        final List<UserRelatedFeedModel> userFeeds = feederRepositoryProvider.getUserFeedRepository().loadUserFeeds(feedIdentification.getUserId());

        List<FeedEntryModel> allFeedEntries = new ArrayList<>();

        for (UserRelatedFeedModel userFeed : userFeeds) {
            allFeedEntries.addAll(userFeed.getFeedEntryModels());

            setFeedEntryHighlightingColor(userFeedsAsTimeLine, userFeed);
            setFeedEntryReadStatus(userFeedsAsTimeLine, userFeed);
        }

        Collections.sort(allFeedEntries, new Comparator<FeedEntryModel>() {
            public int compare(FeedEntryModel m1, FeedEntryModel m2) {
                if (m2.getUpdated() == m1.getUpdated()) {
                    return 0;
                }
                if (m1.getUpdated() == null) {
                    return -1;
                }
                if (m2.getUpdated() == null) {
                    return 1;
                }

                return m2.getUpdated().compareTo(m1.getUpdated());
            }
        });

        userFeedsAsTimeLine.getFeedEntryModels().addAll(allFeedEntries);

        return userFeedsAsTimeLine;
    }

    private void setFeedEntryHighlightingColor(final UserRelatedFeedModel userFeedsAsTimeLine, final UserRelatedFeedModel userFeed) {
        if (StringUtils.isNotBlank(userFeed.getFeedUiHighlightingColor())) {
            final String feedHighlightingColor = userFeed.getFeedUiHighlightingColor();
            for (FeedEntryModel feedEntryModel : userFeed.getFeedEntryModels()) {
                final String feedEntryUid = feedEntryModel.getId();
                userFeedsAsTimeLine.getFeedEntriesUiHighlightingColor().put(feedEntryUid, feedHighlightingColor);
            }
        }
    }

    private void setFeedEntryReadStatus(final UserRelatedFeedModel userFeedsAsTimeLine, final UserRelatedFeedModel userFeed) {
        for (String feedEntryId : userFeed.getFeedEntriesReadStatus().keySet()) {
            final boolean feedEntryReadStatus = userFeed.getFeedEntriesReadStatus().get(feedEntryId);
            userFeedsAsTimeLine.getFeedEntriesReadStatus().put(feedEntryId, feedEntryReadStatus);
        }
    }
}
