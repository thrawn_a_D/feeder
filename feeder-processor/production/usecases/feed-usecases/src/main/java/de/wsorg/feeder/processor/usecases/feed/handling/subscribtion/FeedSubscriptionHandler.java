package de.wsorg.feeder.processor.usecases.feed.handling.subscribtion;

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.FeedSubscriptionHandling;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedSubscriptionHandler {
    void subscribeToFeed(final FeedSubscriptionHandling feedSubscriptionHandle);
    void unsubscribeFeed(final FeedSubscriptionHandling feedSubscriptionHandle);
}
