package de.wsorg.feeder.processor.usecases.feed.finder.userrelated;

import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserRelatedSearchQuery;
import de.wsorg.feeder.processor.usecases.feed.finder.FeedFinder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserRelatedFeedFinder extends FeedFinder<UserRelatedFeedModel, UserRelatedSearchQuery> {
}
