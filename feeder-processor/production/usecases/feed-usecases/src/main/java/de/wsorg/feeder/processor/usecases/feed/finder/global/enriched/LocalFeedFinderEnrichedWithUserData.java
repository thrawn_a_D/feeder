package de.wsorg.feeder.processor.usecases.feed.finder.global.enriched;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQueryEnrichedWithUserData;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class LocalFeedFinderEnrichedWithUserData implements GlobalFeedFinderEnriched {
    Logger LOGGER = LoggerFactory.getLogger(LocalFeedFinderEnrichedWithUserData.class);

    private GlobalFeedFinder localGlobalFeedFinder;
    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public List<FeedModel> findFeeds(final GlobalSearchQueryEnrichedWithUserData queryToUse) {
        List<FeedModel> localSearchResult = localGlobalFeedFinder.findFeeds(queryToUse);

        LOGGER.debug("Enriching found feeds with user data ({})", queryToUse.getUserId());

        localSearchResult = enrichWithUserRelatedData(queryToUse, localSearchResult);
        return localSearchResult;
    }

    private List<FeedModel> enrichWithUserRelatedData(final GlobalSearchQueryEnrichedWithUserData queryToUse, final List<FeedModel> allLocalFeeds) {
        List<FeedModel> finalResult = new ArrayList<>();
        List<UserRelatedFeedModel> userSubscriptions = useCaseStorageProvider.getUsersFeedsStorage().getUserFeeds(queryToUse.getUserId());
        addAllRelevantUserSubscribedFeeds(allLocalFeeds, finalResult, userSubscriptions);
        addDistinctExternalFeeds(allLocalFeeds, finalResult);
        return finalResult;
    }

    private void addDistinctExternalFeeds(final List<FeedModel> allLocalFeeds, final List<FeedModel> finalResult) {
        for (FeedModel allLocalFeed : allLocalFeeds) {
            if(!finalResult.contains(allLocalFeed))
                finalResult.add(allLocalFeed);
        }
    }

    private void addAllRelevantUserSubscribedFeeds(final List<FeedModel> allLocalFeeds, final List<FeedModel> finalResult, final List<UserRelatedFeedModel> userSubscriptions) {
        for (UserRelatedFeedModel userSubscription : userSubscriptions) {
            for (FeedModel localFeed : allLocalFeeds) {
                if(userSubscription.getFeedUrl().equals(localFeed.getFeedUrl())) {
                    if(!finalResult.contains(userSubscription))
                        finalResult.add(userSubscription);
                }
            }
        }
    }

    public void setLocalGlobalFeedFinder(final GlobalFeedFinder localGlobalFeedFinder) {
        this.localGlobalFeedFinder = localGlobalFeedFinder;
    }
}
