package de.wsorg.feeder.processor.usecases.feed.loader;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonFeedLoader {
    FeedModel loadFeed(final LoadFeed feedLoadingMetrics);
}
