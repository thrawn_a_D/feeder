package de.wsorg.feeder.processor.usecases.feed.modifier;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeedModifierTemplate<T> implements FeedModifier<T> {

    @Inject
    protected FeedDataAccess feedDataAccess;
    @Inject
    protected FeederRepositoryProvider feederRepositoryProvider;
    @Inject
    protected FeedToUserAssociationStorage feedToUserAssociationStorage;

    abstract protected FeedModel manipulateFeed(FeedModel feedModel, T manipulatingData);

    public void adjustFeed(final String feedUrl, final String userId, final T value){
        validateInput(feedUrl, userId);
        FeedModel feedModelToManipulate = getFeedToManipulate(feedUrl);

        FeedModel manipulatedFeed = manipulateFeed(feedModelToManipulate, value);
        feedDataAccess.save(manipulatedFeed);
    }

    private FeedModel getFeedToManipulate(final String feedUrl) {
        FeedModel feedModelToManipulate = feedDataAccess.findByFeedUrl(feedUrl);

        if(feedModelToManipulate == null){
            feedModelToManipulate = feederRepositoryProvider.getFeedRepository().loadFeed(feedUrl);

            validateLoaderResult(feedModelToManipulate);
        }
        return feedModelToManipulate;
    }

    private void validateInput(final String feedUrl, final String userId) {
        if(StringUtils.isBlank(userId))
            throw new IllegalArgumentException("Feed modification can not be performed. No userId is provided.");
        if(!feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.OWNER)) {
            final String errorMessage = "The provided user (" + userId + ") has no ownership over this feedUrl: " + feedUrl;
            throw new IllegalStateException(errorMessage);
        }
    }

    private void validateLoaderResult(final FeedModel feedModelToManipulate) {
        if(feedModelToManipulate == null) {
            String errorMessage = "The provided feed url is not in DB and can not be loaded from an external source!";
            throw new IllegalArgumentException(errorMessage);
        }
    }

}
