package de.wsorg.feeder.processor.usecases.feed.loader.modification;

/**
 *
 *
 *
 */
public interface FeedModificatorDelegator extends FeedModificator {
    void addFeedModifier(final FeedModificator feedModificator);
}
