package de.wsorg.feeder.processor.usecases.util.feed.url;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum UrlType {
    NORMAL,
    INTERNAL_TIME_LINE,
    NORMAL_INTERNAL
}
