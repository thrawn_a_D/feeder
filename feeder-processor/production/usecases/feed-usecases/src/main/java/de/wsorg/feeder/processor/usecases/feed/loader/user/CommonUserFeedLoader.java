package de.wsorg.feeder.processor.usecases.feed.loader.user;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed;
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CommonUserFeedLoader implements UserFeedLoader {

    @Inject
    private FeederRepositoryProvider feederRepositoryProvider;

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public FeedModel loadFeed(final UserRelatedLoadFeed feedLoadingMetrics) {
        FeedModel result = feederRepositoryProvider.getFeedRepository().loadFeed(feedLoadingMetrics.getFeedUrl());

        if(result != null) {
            UserRelatedFeedModel userEnrichedData = getFeedEnrichedWithUserData(feedLoadingMetrics, result);
            if(userEnrichedData != null)
                result = userEnrichedData;
        }

        return result;
    }

    private UserRelatedFeedModel getFeedEnrichedWithUserData(final LoadFeed feedLoadingMetrics,
                                                             final FeedModel result) {
        UserRelatedFeedModel userRelatedFeedModel = null;
        if(feedLoadingMetrics instanceof UserRelatedLoadFeed){
            final UserRelatedLoadFeed userRelatedFeedLoadMetrics = (UserRelatedLoadFeed) feedLoadingMetrics;
            if (StringUtils.isNotBlank(userRelatedFeedLoadMetrics.getUserId())) {
                FeedToUserAssociation association = useCaseStorageProvider.getFeedToUserAssociationStorage().getUserFeedAssociation(feedLoadingMetrics.getFeedUrl(),
                        userRelatedFeedLoadMetrics.getUserId());
                if(association != null) {
                    userRelatedFeedModel = new UserRelatedFeedModel(result, association);
                }
            }
        }

        return userRelatedFeedModel;
    }

}
