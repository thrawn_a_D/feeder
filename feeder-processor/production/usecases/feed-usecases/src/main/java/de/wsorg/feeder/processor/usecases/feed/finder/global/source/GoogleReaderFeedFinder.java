package de.wsorg.feeder.processor.usecases.feed.finder.global.source;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;
import de.wsorg.feeder.processor.usecases.util.wrapper.JavaUrlWrapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GoogleReaderFeedFinder implements GlobalFeedFinder<GlobalSearchQuery> {
    Logger LOGGER = LoggerFactory.getLogger(GoogleReaderFeedFinder.class);

    @Inject
    private JavaUrlWrapper javaUrlWrapper;

    @Override
    public List<FeedModel> findFeeds(final GlobalSearchQuery queryToUse) {
        LOGGER.debug("Search for feeds using google reader api as source.");

        List<FeedModel> result = new ArrayList<>();

        String requestUrl = "https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=" + queryToUse.getSearchTerm().replace(" ", "%20");

        StringBuilder stringBuilder = performRequest(requestUrl);
        result = mapResultToFeeds(stringBuilder);

        LOGGER.debug("Search operation finished. Found " + result.size() + " feeds.");

        return result;

    }

    private StringBuilder performRequest(final String requestUrl) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            final URLConnection connection = javaUrlWrapper.getUrlConnection(requestUrl);

            stringBuilder = getResponseAsString(connection);

        }  catch (IOException e) {
            String message = "Could not perform request to GoogleReader.";
            LOGGER.error(message + e.getMessage());
            throw new RuntimeException(message);
        }
        return stringBuilder;
    }

    private StringBuilder getResponseAsString(final URLConnection connection) throws IOException {
        String line;
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        while((line = reader.readLine()) != null) {
            builder.append(line);
        }
        return builder;
    }

    private List<FeedModel> mapResultToFeeds(final StringBuilder builder) {
        final  List<FeedModel> result = new ArrayList<>();
        JSONObject json = null;
        try {
            json = new JSONObject(builder.toString());

            JSONObject responseData = (JSONObject) json.get("responseData");
            JSONArray entries = responseData.getJSONArray("entries");

            for (int i = 0; i < entries.length(); i++) {
                FeedModel feed = new FeedModel();
                JSONObject entry = (JSONObject) entries.get(i);
                final String title = entry.getString("title");
                final String url = entry.getString("url");
                final String description = entry.getString("contentSnippet");
                feed.setTitle(title);
                feed.setFeedUrl(url);
                feed.setDescription(description);
                result.add(feed);
            }
        } catch (JSONException e) {
            String message = "Error during parsing of response from google reader.";
            LOGGER.error(message + e.getMessage());
            throw new RuntimeException(message);
        }
        return result;
    }
}
