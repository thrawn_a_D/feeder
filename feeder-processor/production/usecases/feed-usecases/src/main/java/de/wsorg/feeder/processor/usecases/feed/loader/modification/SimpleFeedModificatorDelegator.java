package de.wsorg.feeder.processor.usecases.feed.loader.modification;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;

import javax.annotation.Resource;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class SimpleFeedModificatorDelegator implements FeedModificatorDelegator {

    @Resource(name = "feedModifiers")
    private List<FeedModificator> feedModificators = new ArrayList<>();

    @Override
    public void addFeedModifier(final FeedModificator feedModificator) {
        if (feedModificator != null) {
            feedModificators.add(feedModificator);
        }
    }

    @Override
    public FeedModel modifyFeed(final FeedModel feedToModify, final LoadFeed modificationParameter) {
        FeedModel modificationResult = feedToModify;
        for (FeedModificator feedModificator : feedModificators) {
            modificationResult = feedModificator.modifyFeed(modificationResult, modificationParameter);
        }

        return modificationResult;
    }
}
