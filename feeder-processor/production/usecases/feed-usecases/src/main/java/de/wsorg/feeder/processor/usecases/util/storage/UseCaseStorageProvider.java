package de.wsorg.feeder.processor.usecases.util.storage;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UseCaseStorageProvider {
    FeedDataAccess getFeedDataAccess();
    UsersFeedsStorage getUsersFeedsStorage();
    FeedToUserAssociationStorage getFeedToUserAssociationStorage();
}
