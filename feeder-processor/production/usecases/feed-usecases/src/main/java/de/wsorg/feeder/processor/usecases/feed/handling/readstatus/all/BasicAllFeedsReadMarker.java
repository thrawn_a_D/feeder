package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BasicAllFeedsReadMarker implements AllFeedsReadMarker {
    final Logger LOGGER = LoggerFactory.getLogger(BasicAllFeedsReadMarker.class);

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;

    @Override
    public void markAllFeeds(final MarkAllFeedsReadStatusHandling feedEntryReadStatusHandling) {
        final FeedToUserAssociationStorage feedToUserAssociationStorage = useCaseStorageProvider.getFeedToUserAssociationStorage();
        final List<FeedToUserAssociation> userFeedAssociations = feedToUserAssociationStorage.getUserFeeds(feedEntryReadStatusHandling.getUserId());

        for (FeedToUserAssociation userFeedAssociation : userFeedAssociations) {
            final FeedModel feedData = useCaseStorageProvider.getFeedDataAccess().findByFeedUrl(userFeedAssociation.getFeedUrl());

            for (FeedEntryModel feedEntryModel : feedData.getFeedEntryModels()) {
                userFeedAssociation.getFeedEntriesReadStatus().put(feedEntryModel.getId(), feedEntryReadStatusHandling.isFeedsReadStatus());
            }

            feedToUserAssociationStorage.save(userFeedAssociation);
        }
    }
}
