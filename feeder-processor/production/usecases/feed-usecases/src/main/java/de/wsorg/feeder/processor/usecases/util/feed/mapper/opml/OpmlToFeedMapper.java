package de.wsorg.feeder.processor.usecases.util.feed.mapper.opml;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.OPML;
import de.wsorg.feeder.processor.usecases.domain.generated.opml.Outline;
import org.apache.commons.lang.StringUtils;

import javax.inject.Named;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class OpmlToFeedMapper {
    public List<FeedModel> getFeedByOpml(OPML opmlToMapToFeed) {
        List<FeedModel> resultedFeedModelList = new ArrayList<FeedModel>();
        List<Outline> outline = opmlToMapToFeed.getBody().getOutline();

        if(outline != null){
            for(Outline currentOutline : outline){
                resultedFeedModelList.addAll(getFeedRecursivelyFromOutline(currentOutline));
            }
        }

        return resultedFeedModelList;
    }

    private List<FeedModel> getFeedRecursivelyFromOutline(Outline feedOutline){
        List<FeedModel> result = new ArrayList<FeedModel>();

        result = getCurrentFeedInfo(feedOutline);
        result.addAll(getChildFeedInfos(feedOutline));

        return result;
    }

    private List<FeedModel> getChildFeedInfos(Outline feedOutline) {
        List<FeedModel> result = new ArrayList<FeedModel>();

        if(feedOutline.getOutline() != null &&
           feedOutline.getOutline().size() > 0) {
            for(Outline childOutline : feedOutline.getOutline()){
                List<FeedModel> childFeeds = getFeedRecursivelyFromOutline(childOutline);
                result.addAll(childFeeds);
            }
        }

        return result;
    }

    private List<FeedModel> getCurrentFeedInfo(Outline feedOutline) {
        List<FeedModel> result = new ArrayList<FeedModel>();

        if(isValidFeed(feedOutline)){
            FeedModel currentFeed = getSingleFeedFromOutline(feedOutline);
            result.add(currentFeed);
        }

        return result;
    }

    private boolean isValidFeed(Outline feedOutline){
        return StringUtils.isNotBlank(feedOutline.getText());
    }


    private FeedModel getSingleFeedFromOutline(Outline usedOutline){
        FeedModel result = new FeedModel();
        result.setTitle(usedOutline.getText());
        result.setDescription(usedOutline.getDescription());
        result = setCreationDate(usedOutline, result);
        result.setFeedUrl(usedOutline.getXmlUrl());
        return result;
    }

    private FeedModel setCreationDate(Outline usedOutline, FeedModel result) {
        if(usedOutline.getCreated() != null){
            result.setUpdated(getDateFromString(usedOutline));
        }

        return result;
    }

    private Date getDateFromString(Outline usedOutline) {
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return dateFormat.parse(usedOutline.getCreated());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
