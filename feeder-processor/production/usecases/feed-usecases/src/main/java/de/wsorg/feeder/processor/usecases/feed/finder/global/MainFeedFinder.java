package de.wsorg.feeder.processor.usecases.feed.finder.global;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class MainFeedFinder implements GlobalFeedFinder<GlobalSearchQuery> {

    Logger LOGGER = LoggerFactory.getLogger(MainFeedFinder.class);

    private GlobalFeedFinder externalGlobalFeedFinder;
    private GlobalFeedFinder localGlobalFeedFinder;

    @Override
    public List<FeedModel> findFeeds(final GlobalSearchQuery queryToUse) {
        List<FeedModel> externalSearchResult = new ArrayList<FeedModel>();
        List<FeedModel> localSearchResult = new ArrayList<FeedModel>();
        List<FeedModel> consolidatedSearchResult = new ArrayList<FeedModel>();

        LOGGER.debug("##################################################################################");
        LOGGER.debug("# Start searching for feeds using the query: " + queryToUse);
        LOGGER.debug("##################################################################################");

        localSearchResult = localGlobalFeedFinder.findFeeds(queryToUse);
        try {
            externalSearchResult = externalGlobalFeedFinder.findFeeds(queryToUse);
        } catch (Exception e) {
            LOGGER.error("Error while searching for feeds using an external finder: " + e.getMessage());
        }

        consolidatedSearchResult.addAll(externalSearchResult);

        consolidatedSearchResult = addOnlyDistinctLocalFeeds(localSearchResult, consolidatedSearchResult);

        return consolidatedSearchResult;
    }

    private List<FeedModel> addOnlyDistinctLocalFeeds(final List<FeedModel> localSearchResult,
                                                        final List<FeedModel> externalSearchResult) {
        List<FeedModel> searchResult = new ArrayList<>(externalSearchResult);
        for (FeedModel localFeedModel : localSearchResult) {
            boolean foundInLocalFeeds = false;
            for (FeedModel externalModel : externalSearchResult) {
                if(localFeedModel.getFeedUrl().equals(externalModel.getFeedUrl())){
                    searchResult.remove(externalModel);
                    searchResult.add(localFeedModel); //We choose always local feeds as this are supposed to be up to date and ca contain users data
                    foundInLocalFeeds=true;
                    break;
                }
            }
            if(!foundInLocalFeeds)
                searchResult.add(localFeedModel);
        }
        return searchResult;
    }

    public void setExternalFeedFinder(final GlobalFeedFinder externalGlobalFeedFinder) {
        this.externalGlobalFeedFinder = externalGlobalFeedFinder;
    }

    public void setLocalFeedFinder(final GlobalFeedFinder localGlobalFeedFinder) {
        this.localGlobalFeedFinder = localGlobalFeedFinder;
    }
}
