package de.wsorg.feeder.processor.usecases.feed.loader.modification;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed;
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ShowOnlyUnreadEntriesModificator implements FeedModificator {
    @Override
    public FeedModel modifyFeed(final FeedModel feedToModify, final LoadFeed modificationParameter) {

        if (modificationParameter instanceof UserRelatedLoadFeed) {
            UserRelatedLoadFeed loadFeedParams = (UserRelatedLoadFeed) modificationParameter;

            if(loadFeedParams.isShowOnlyUnreadEntries()) {
                List<FeedEntryModel> unreadEntries = new ArrayList<>();

                UserRelatedFeedModel userFeedModel = (UserRelatedFeedModel) feedToModify;

                for (FeedEntryModel feedEntryModel : feedToModify.getFeedEntries()) {
                    final Map<String,Boolean> feedEntriesReadStatus = userFeedModel.getFeedEntriesReadStatus();
                    if(isEntryAlreadyRead(feedEntryModel, feedEntriesReadStatus)) {
                        feedEntriesReadStatus.remove(feedEntryModel.getId());
                    } else {
                        unreadEntries.add(feedEntryModel);
                    }
                }

                feedToModify.setFeedEntries(unreadEntries);
            }

        }

        return feedToModify;
    }

    private boolean isEntryAlreadyRead(final FeedEntryModel feedEntryModel, final Map<String, Boolean> feedEntriesReadStatus) {
        return feedEntriesReadStatus.containsKey(feedEntryModel.getId()) &&
               feedEntriesReadStatus.get(feedEntryModel.getId());
    }
}
