package de.wsorg.feeder.processor.usecases.feed.finder.global.enriched;

import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQueryEnrichedWithUserData;
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface GlobalFeedFinderEnriched extends GlobalFeedFinder<GlobalSearchQueryEnrichedWithUserData> {
}
