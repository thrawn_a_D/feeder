package de.wsorg.feeder.processor.usecases.feed.handling.subscribtion;

import javax.inject.Inject;
import javax.inject.Named;
import java.awt.*;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.FeedSubscriptionHandling;
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider;
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BasicFeedSubscriptionHandler implements FeedSubscriptionHandler {
    Logger LOGGER = LoggerFactory.getLogger(BasicFeedSubscriptionHandler.class);

    @Inject
    private UseCaseStorageProvider useCaseStorageProvider;
    @Inject
    private FeederRepositoryProvider feederRepositoryProvider;

    @Override
    public void subscribeToFeed(final FeedSubscriptionHandling feedSubscriptionHandle) {
        final String feedUrl = feedSubscriptionHandle.getFeedUrl();
        final String userId = feedSubscriptionHandle.getUserId();
        LOGGER.debug("###########################################################################");
        LOGGER.debug("# Subscribe to feed: {} by userId: {}", feedUrl, userId);
        LOGGER.debug("###########################################################################");

        ensureFeedIsLocallyAvailable(feedUrl, userId);

        FeedToUserAssociation association = new FeedToUserAssociation();
        association.setFeedUrl(feedUrl);
        association.setUserId(userId);
        association.setAssociationType(AssociationType.SUBSCRIBER);

        addFeedColor(association);

        useCaseStorageProvider.getFeedToUserAssociationStorage().save(association);

        LOGGER.debug("Association of feed {} to user {} has been set.", feedUrl, userId);
    }

    private void addFeedColor(final FeedToUserAssociation association) {
        String randomColor = "#";
        Random random=new Random();
        int red=random.nextInt(256);
        int green=random.nextInt(256);
        int blue=random.nextInt(256);
        Color color = new Color(red, green, blue);
        randomColor += Integer.toHexString(color.getRed());
        randomColor += Integer.toHexString(color.getGreen());
        randomColor += Integer.toHexString(color.getBlue());
        association.setFeedUiHighlightingColor(randomColor);
    }

    private void ensureFeedIsLocallyAvailable(final String feedUrl, final String userId) {
        FeedModel localFeed = useCaseStorageProvider.getFeedDataAccess().findByFeedUrl(feedUrl);

        if(localFeed == null){
            LOGGER.debug("Feed to subscribe is not locally present. Load external feed and save it locally.");
            FeedModel loadedExternalFeed = feederRepositoryProvider.getFeedRepository().loadFeed(feedUrl);
            useCaseStorageProvider.getFeedDataAccess().save(loadedExternalFeed);
        }
    }

    @Override
    public void unsubscribeFeed(final FeedSubscriptionHandling feedSubscriptionHandle) {
        final String feedUrl = feedSubscriptionHandle.getFeedUrl();
        final String userId = feedSubscriptionHandle.getUserId();
        boolean isAssociationOfTypeSubscription = useCaseStorageProvider.getFeedToUserAssociationStorage().isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER);
        if(isAssociationOfTypeSubscription){
            useCaseStorageProvider.getFeedToUserAssociationStorage().removeAssociation(feedUrl, userId);
            feederRepositoryProvider.getFeedRepository().informThatUserCanceledSubscription(feedUrl);
        }
    }
}
