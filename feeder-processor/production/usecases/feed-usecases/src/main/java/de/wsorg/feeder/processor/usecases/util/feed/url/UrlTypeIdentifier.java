package de.wsorg.feeder.processor.usecases.util.feed.url;

/**
 *
 *
 *
 */
public interface UrlTypeIdentifier {
    UrlType determinateUrlType(String feedUrl);
}
