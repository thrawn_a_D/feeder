package de.wsorg.feeder.processor.usecases.feed.loader.modification

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 04.12.12
 * Time: 12:47
 */
class SimpleFeedModificatorDelegatorTest extends Specification {
    SimpleFeedModificatorDelegator simpleFeedModificatorDelegator

    def setup(){
        simpleFeedModificatorDelegator = new SimpleFeedModificatorDelegator()
    }

    def "Register modificator"() {
        given:
        def modifier = Mock(FeedModificator)

        when:
        simpleFeedModificatorDelegator.addFeedModifier(modifier)

        then:
        simpleFeedModificatorDelegator.feedModificators.contains(modifier)
    }

    def "Should be not able to add nullable values"() {
        when:
        simpleFeedModificatorDelegator.addFeedModifier(null)

        then:
        simpleFeedModificatorDelegator.feedModificators.size() == 0
    }

    def "All registered feed modifier are executed during run"() {
        given:
        def modifier1 = Mock(FeedModificator)
        def modifier2 = Mock(FeedModificator)
        simpleFeedModificatorDelegator.addFeedModifier(modifier1)
        simpleFeedModificatorDelegator.addFeedModifier(modifier2)

        and:
        def feedToModify = Mock(FeedModel)
        def loadFeedModel = Mock(LoadFeed)

        and:
        def modificationResultOfModifier1 = Mock(FeedModel)
        def modificationResultOfModifier2 = Mock(FeedModel)

        when:
        def result = simpleFeedModificatorDelegator.modifyFeed(feedToModify, loadFeedModel)

        then:
        result == modificationResultOfModifier2
        1 * modifier1.modifyFeed(feedToModify, loadFeedModel) >> modificationResultOfModifier1
        1 * modifier2.modifyFeed(modificationResultOfModifier1, loadFeedModel) >> modificationResultOfModifier2
    }
}
