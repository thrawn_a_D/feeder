package de.wsorg.feeder.processor.usecases.feed.loader

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed
import de.wsorg.feeder.processor.updater.repository.FeedRepository
import de.wsorg.feeder.processor.usecases.feed.loader.user.UserFeedLoader
import de.wsorg.feeder.processor.usecases.feed.loader.user.timeline.FeedsTimeLineLoader
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlType
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlTypeIdentifier
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.12.12
 */
class FeedSourceSwitcherTest extends Specification {
    FeedSourceSwitcher feedSourceSwitcher
    UrlTypeIdentifier urlTypeIdentifier
    FeedRepository feedRepository
    FeederRepositoryProvider feederRepositoryProvider
    FeedsTimeLineLoader timeLineLoader
    UserFeedLoader commonUserFeedLoader

    def setup(){
        feedSourceSwitcher = new FeedSourceSwitcher()
        urlTypeIdentifier = Mock(UrlTypeIdentifier)
        feedRepository = Mock(FeedRepository)
        timeLineLoader = Mock(FeedsTimeLineLoader)
        commonUserFeedLoader = Mock(UserFeedLoader)
        feederRepositoryProvider = Mock(FeederRepositoryProvider)

        feederRepositoryProvider.getFeedRepository() >> feedRepository

        feedSourceSwitcher.urlTypeIdentifier = urlTypeIdentifier
        feedSourceSwitcher.feederRepositoryProvider = feederRepositoryProvider
        feedSourceSwitcher.timeLineLoader = timeLineLoader
        feedSourceSwitcher.commonUserFeedLoader = commonUserFeedLoader
    }

    def "Terminate which feed source to choose for external feed and execute source"() {
        given:
        def feedUrl = "externalFeedUrl"
        def loadFeed = new LoadFeed(feedUrl: feedUrl)

        and:
        def externalSource = UrlType.NORMAL

        and:
        def expectedFeedModel = Mock(FeedModel)

        when:
        def result = feedSourceSwitcher.loadFeed(loadFeed)

        then:
        result == expectedFeedModel
        1 * urlTypeIdentifier.determinateUrlType(feedUrl) >> externalSource
        1 * feedRepository.loadFeed(feedUrl) >> expectedFeedModel
        0 * commonUserFeedLoader.loadFeed(loadFeed)

    }

    def "If feed url is external and load request model is of type UserRelatedFeedModel then use a user related loader"() {
        given:
        def feedUrl = "externalFeedUrl"
        def userId = '2344'
        def loadFeed = new UserRelatedLoadFeed(feedUrl: feedUrl, userId: userId)

        and:
        def externalSource = UrlType.NORMAL

        and:
        def expectedFeedModel = Mock(FeedModel)

        when:
        def result = feedSourceSwitcher.loadFeed(loadFeed)

        then:
        result == expectedFeedModel
        1 * urlTypeIdentifier.determinateUrlType(feedUrl) >> externalSource
        0 * feedRepository.loadFeed(feedUrl)
        1 * commonUserFeedLoader.loadFeed(loadFeed) >> expectedFeedModel
    }

    def "Terminate which feed source to choose for internal feed and execute source"() {
        given:
        def feedUrl = "externalFeedUrl"
        def userId = '2344'
        def loadFeed = new UserRelatedLoadFeed(feedUrl: feedUrl, userId: userId)

        and:
        def internalSource = UrlType.INTERNAL_TIME_LINE

        and:
        def expectedFeedModel = Mock(FeedModel)

        when:
        def result = feedSourceSwitcher.loadFeed(loadFeed)

        then:
        result == expectedFeedModel
        1 * urlTypeIdentifier.determinateUrlType(feedUrl) >> internalSource
        1 * timeLineLoader.loadFeed(loadFeed) >> expectedFeedModel
    }

    def "Provide a url which is a normal internal url"() {
        given:
        def feedUrl = "externalFeedUrl"
        def loadFeed = new LoadFeed(feedUrl: feedUrl)

        and:
        1 * urlTypeIdentifier.determinateUrlType(feedUrl) >> UrlType.NORMAL_INTERNAL

        when:
        feedSourceSwitcher.loadFeed(loadFeed)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided maps top internal feeds. This functionality is not yet supported'
    }
}
