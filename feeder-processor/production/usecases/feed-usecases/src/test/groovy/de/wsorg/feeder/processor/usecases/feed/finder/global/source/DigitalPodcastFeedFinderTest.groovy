package de.wsorg.feeder.processor.usecases.feed.finder.global.source

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import de.wsorg.feeder.processor.usecases.domain.generated.opml.OPML
import de.wsorg.feeder.processor.usecases.util.feed.mapper.opml.OpmlToFeedMapper
import org.springframework.context.ApplicationContext
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

/**
 * User: wschneider
 * Date: 25.06.12
 * Time: 12:09
 */
class DigitalPodcastFeedFinderTest extends Specification {

    DigitalPodcastFeedFinder feedFinder
    RestTemplate restTemplateMock
    OpmlToFeedMapper opmlToFeedMapper
    ApplicationContext applicationContext

    def setup(){
        feedFinder = new DigitalPodcastFeedFinder()
        setupMocks(feedFinder)

    }

    def "Check if finder uses the right criteria while searching for feeds"(){
        given: "Some mocks"
        def opmlRestResult = Mock(OPML)

        and: "Some test stubs"
        def feedQuery = new GlobalSearchQuery(searchTerm: 'Test search')

        when:
        feedFinder.findFeeds(feedQuery)

        then:
        1 * restTemplateMock.getForObject(DigitalPodcastFeedFinder.FIND_FEED_GET, OPML.class, {Map usedRequestVars->
            usedRequestVars['appId'] == DigitalPodcastFeedFinder.APPLICATION_ID
            usedRequestVars['format'] == DigitalPodcastFeedFinder.FORMAT
            usedRequestVars['sort'] == DigitalPodcastFeedFinder.SORT
            usedRequestVars['searchSource'] == DigitalPodcastFeedFinder.SEARCH_SOURCE
            usedRequestVars['contentFilter'] == DigitalPodcastFeedFinder.CONTENT_FILTER
            usedRequestVars['keyWords'] == feedQuery.searchTerm
        }) >> opmlRestResult

        1 * opmlToFeedMapper.getFeedByOpml(opmlRestResult) >> new ArrayList<FeedModel>();
    }

    def "Check if FeedModels are wrapped with a feed buisness object"(){
        given: "Finder should return some models"
        List<FeedModel> stubFeedModels = [new FeedModel(feedUrl: 'feedUrl1'), new FeedModel(feedUrl: 'feedUrl2')]
        opmlToFeedMapper.getFeedByOpml(_) >> stubFeedModels

        def feedQuery = new GlobalSearchQuery(searchTerm: 'Test search')

        when:
        def result = feedFinder.findFeeds(feedQuery)

        then:
        result != null
        result.size() == 2
        checkContent(result, stubFeedModels)
    }

    def "Make sure an empty list is returned when no query is provided"() {
        given:
        def feedQuery = new GlobalSearchQuery(searchTerm: '')


        when:
        def result = feedFinder.findFeeds(feedQuery)

        then:
        result.size() == 0
    }

    private void checkContent(List<FeedModel> result, ArrayList<FeedModel> stubFeedModels) {
        result[0].feedUrl == stubFeedModels[0].feedUrl
        result[1].feedUrl == stubFeedModels[1].feedUrl
    }

    private void setupMocks(DigitalPodcastFeedFinder feedFinder) {
        restTemplateMock = Mock(RestTemplate)
        feedFinder.restTemplate = restTemplateMock
        opmlToFeedMapper = Mock(OpmlToFeedMapper)
        feedFinder.opmlToFeedMapper = opmlToFeedMapper
    }
}
