package de.wsorg.feeder.processor.usecases.feed.handling.subscribtion

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.FeedSubscriptionHandling
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.updater.repository.FeedRepository
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.09.12
 */
class BasicFeedSubscriptionHandlerTest extends Specification {
    BasicFeedSubscriptionHandler basicFeedSubscriber
    FeedToUserAssociationStorage feedToUserAssociationStorage
    FeedDataAccess feedDataAccess
    FeedRepository feedLoader
    UseCaseStorageProvider useCaseStorageProvider
    FeederRepositoryProvider feederRepositoryProvider

    def setup(){
        basicFeedSubscriber = new BasicFeedSubscriptionHandler()
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        feedDataAccess = Mock(FeedDataAccess)
        feedLoader = Mock(FeedRepository)
        feederRepositoryProvider = Mock(FeederRepositoryProvider)
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getFeedToUserAssociationStorage() >> feedToUserAssociationStorage
        feederRepositoryProvider.getFeedRepository() >> feedLoader
        useCaseStorageProvider.getFeedDataAccess() >> feedDataAccess
        basicFeedSubscriber.feederRepositoryProvider = feederRepositoryProvider
        basicFeedSubscriber.useCaseStorageProvider = useCaseStorageProvider
    }

    def "Subscribe to a feed under given userId and feedurl (feed already exists locally)"() {
        given:
        def userId = 'user123'
        def feedUrl = 'url://123'
        def feedSubscriptionHandle = new FeedSubscriptionHandling(userId: userId, feedUrl: feedUrl)

        when:
        basicFeedSubscriber.subscribeToFeed(feedSubscriptionHandle)

        then:
        1 * feedToUserAssociationStorage.save({it instanceof FeedToUserAssociation &&
                           it.feedUrl == feedUrl &&
                           it.userId == userId &&
                           it.associationType == AssociationType.SUBSCRIBER})
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> new FeedModel()
    }

    def "Subscribe to a feed but no local feed is there"() {
        given:
        def userId = 'user123'
        def feedUrl = 'url://123'
        def feedSubscriptionHandle = new FeedSubscriptionHandling(userId: userId, feedUrl: feedUrl)

        and:
        def loadedExternalFeed = new FeedModel()

        when:
        basicFeedSubscriber.subscribeToFeed(feedSubscriptionHandle)

        then:
        1 * feedToUserAssociationStorage.save({it instanceof FeedToUserAssociation &&
                it.feedUrl == feedUrl &&
                it.userId == userId &&
                it.associationType == AssociationType.SUBSCRIBER})
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> null
        1 * feedLoader.loadFeed(feedUrl) >> loadedExternalFeed
        1 * feedDataAccess.save(loadedExternalFeed)
    }

    def "Remove association if it's type is SUBSCRIBTION"() {
        given:
        def userId = '123i8z'
        def feedUrl = 'url'
        def feedSubscriptionHandle = new FeedSubscriptionHandling(userId: userId, feedUrl: feedUrl)

        when:
        basicFeedSubscriber.unsubscribeFeed(feedSubscriptionHandle)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> true
        1 * feedToUserAssociationStorage.removeAssociation(feedUrl, userId)
        1 * feedLoader.informThatUserCanceledSubscription(feedUrl)
    }

    def "If subscribtion type is owner do not remove association"() {
        given:
        def userId = '123i8z'
        def feedUrl = 'url'
        def feedSubscriptionHandle = new FeedSubscriptionHandling(userId: userId, feedUrl: feedUrl)

        when:
        basicFeedSubscriber.unsubscribeFeed(feedSubscriptionHandle)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> false
        0 * feedToUserAssociationStorage.removeAssociation(feedUrl, userId)
    }

    def "Generate new feed color on subscription"() {
        given:
        def userId = 'user123'
        def feedUrl = 'url://123'
        def feedSubscriptionHandle = new FeedSubscriptionHandling(userId: userId, feedUrl: feedUrl)

        when:
        basicFeedSubscriber.subscribeToFeed(feedSubscriptionHandle)

        then:
        1 * feedToUserAssociationStorage.save({it instanceof FeedToUserAssociation &&
                it.feedUiHighlightingColor.startsWith("#") &&
                it.feedUiHighlightingColor.size() > 1})
    }
}
