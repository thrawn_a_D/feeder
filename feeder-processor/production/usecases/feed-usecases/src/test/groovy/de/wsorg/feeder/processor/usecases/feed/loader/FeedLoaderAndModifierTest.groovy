package de.wsorg.feeder.processor.usecases.feed.loader

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import de.wsorg.feeder.processor.usecases.feed.loader.modification.FeedModificatorDelegator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.12.12
 */
class FeedLoaderAndModifierTest extends Specification {
    CommonFeedLoader userFeedLoader
    FeedModificatorDelegator modificatorDelegator
    FeedLoaderAndModifier feedLoaderAndModifier

    def setup(){
        userFeedLoader = Mock(CommonFeedLoader)
        modificatorDelegator = Mock(FeedModificatorDelegator)
        feedLoaderAndModifier = new FeedLoaderAndModifier()
        feedLoaderAndModifier.feedLoader = userFeedLoader
        feedLoaderAndModifier.modificatorDelegator = modificatorDelegator
    }

    def "Load feed, provide result to modificator and return this result back to user"() {
        given:
        def feedLoad = new LoadFeed(feedUrl: 'feedUrl')

        and:
        def feedLoadingResult = new FeedModel()

        when:
        feedLoaderAndModifier.loadFeed(feedLoad)

        then:
        1 * userFeedLoader.loadFeed(feedLoad) >> feedLoadingResult
        1 * modificatorDelegator.modifyFeed(feedLoadingResult, feedLoad)
    }
}
