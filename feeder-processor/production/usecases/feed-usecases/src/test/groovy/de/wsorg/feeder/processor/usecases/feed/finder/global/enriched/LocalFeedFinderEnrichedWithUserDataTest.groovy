package de.wsorg.feeder.processor.usecases.feed.finder.global.enriched

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQueryEnrichedWithUserData
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage
import de.wsorg.feeder.processor.usecases.feed.finder.global.GlobalFeedFinder
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.01.13
 */
class LocalFeedFinderEnrichedWithUserDataTest extends Specification {
    LocalFeedFinderEnrichedWithUserData localFeedFinderEnrichedWithUserData
    GlobalFeedFinder localGlobalFeedFinder
    UsersFeedsStorage usersFeedsStorage
    UseCaseStorageProvider useCaseStorageProvider

    def setup(){
        localFeedFinderEnrichedWithUserData = new LocalFeedFinderEnrichedWithUserData()
        localGlobalFeedFinder = Mock(GlobalFeedFinder)
        usersFeedsStorage = Mock(UsersFeedsStorage)
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getUsersFeedsStorage() >> usersFeedsStorage
        localFeedFinderEnrichedWithUserData.useCaseStorageProvider = useCaseStorageProvider
        localFeedFinderEnrichedWithUserData.localGlobalFeedFinder = localGlobalFeedFinder
    }

    def "Find all local feeds and enriche them with user data"() {
        given:
        def searchQuery = new GlobalSearchQueryEnrichedWithUserData(userId: '1525')

        and:
        def expectedSearchResultOfLocalFeeds = [new FeedModel(feedUrl: 'bla')]
        def expectedSearchResultOfFavouredFeeds = [new UserRelatedFeedModel(new FeedModel(feedUrl: 'bla2'), new FeedToUserAssociation(feedUrl: 'bla2')),
                new UserRelatedFeedModel(new FeedModel(feedUrl: 'bla'), new FeedToUserAssociation(feedUrl: 'bla'))]

        when:
        def result = localFeedFinderEnrichedWithUserData.findFeeds(searchQuery)

        then:
        1 * localGlobalFeedFinder.findFeeds(searchQuery) >> expectedSearchResultOfLocalFeeds

        result.size() == 1
        result.any {it.feedUrl == 'bla' && it instanceof UserRelatedFeedModel}
        result.findAll {it.feedUrl == 'bla'}.size() == 1
        usersFeedsStorage.getUserFeeds(searchQuery.userId) >> expectedSearchResultOfFavouredFeeds
    }
}
