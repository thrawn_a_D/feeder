package de.wsorg.feeder.processor.usecases.feed.loader.user.timeline

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed
import de.wsorg.feeder.processor.updater.repository.UserFeedRepository
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.12.12
 */
class UserFeedsTimeLineLoaderTest extends Specification {
    UserFeedsTimeLineLoader userFeedsTimeLineLoader
    UserFeedRepository userFeedRepository;
    FeederRepositoryProvider feederRepositoryProvider

    def setup(){
        userFeedsTimeLineLoader = new UserFeedsTimeLineLoader()
        userFeedRepository = Mock(UserFeedRepository)
        feederRepositoryProvider = Mock(FeederRepositoryProvider)
        feederRepositoryProvider.getUserFeedRepository() >> userFeedRepository
        userFeedsTimeLineLoader.feederRepositoryProvider = feederRepositoryProvider
    }

    def "Load feeds time line of a user"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)

        def firstFeedModel = getFirstFeedModel()
        def secondFeedModel = getSecondFeedModel()

        def foundUserFeeds = [new UserRelatedFeedModel(firstFeedModel, new FeedToUserAssociation()),
                              new UserRelatedFeedModel(secondFeedModel, new FeedToUserAssociation())]

        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result instanceof FeedModel
        result.feedEntryModels[0] == firstFeedModel.feedEntryModels[0]
        result.feedEntryModels[1] == secondFeedModel.feedEntryModels[0]
        result.feedEntryModels[2] == firstFeedModel.feedEntryModels[2]
        result.feedEntryModels[3] == secondFeedModel.feedEntryModels[1]
        result.feedEntryModels[4] == firstFeedModel.feedEntryModels[1]
        result.feedEntryModels[5] == secondFeedModel.feedEntryModels[2]
        result.feedEntryModels[6] == secondFeedModel.feedEntryModels[3]
        1 * userFeedRepository.loadUserFeeds(userId) >> foundUserFeeds
    }

    def "User does not have any feeds, return empty feed model"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)


        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result != null
        1 * userFeedRepository.loadUserFeeds(userId) >> []
    }

    def "Make sure feed entry color is set"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)
        def highlightingColor1 = '#h32f234'
        def highlightingColor2 = '#ku9807b'

        def firstFeedModel = getFirstFeedModel()
        def secondFeedModel = getSecondFeedModel()
        def foundUserFeeds = [new UserRelatedFeedModel(firstFeedModel,
                                                       new FeedToUserAssociation(feedUiHighlightingColor: highlightingColor1)),
                              new UserRelatedFeedModel(secondFeedModel,
                                                       new FeedToUserAssociation(feedUiHighlightingColor: highlightingColor2))]

        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result instanceof UserRelatedFeedModel
        result.feedEntriesUiHighlightingColor.findAll {it.key.startsWith('f1')}.any {it.value == highlightingColor1}
        result.feedEntriesUiHighlightingColor.findAll {it.key.startsWith('f2')}.any {it.value == highlightingColor2}
        1 * userFeedRepository.loadUserFeeds(userId) >> foundUserFeeds
    }

    def "Color is not set, so no reason to populate this color into feed entries"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)

        def firstFeedModel = getFirstFeedModel()
        def foundUserFeeds = [new UserRelatedFeedModel(firstFeedModel,
                new FeedToUserAssociation(feedUiHighlightingColor: null))]

        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result instanceof UserRelatedFeedModel
        result.feedEntriesUiHighlightingColor.size() == 0
        1 * userFeedRepository.loadUserFeeds(userId) >> foundUserFeeds
    }

    def "Set read status of feed entries"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)

        def firstFeedModel = getFirstFeedModel()
        def secondFeedModel = getSecondFeedModel()
        def foundUserFeeds = [new UserRelatedFeedModel(firstFeedModel,
                new FeedToUserAssociation(feedEntriesReadStatus: ['f1e1': true])),
                new UserRelatedFeedModel(secondFeedModel,
                        new FeedToUserAssociation(feedEntriesReadStatus: ['f1e2': true]))]

        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result instanceof UserRelatedFeedModel
        result.feedEntriesReadStatus.findAll {it.key.startsWith('f1e1')}.any {it.value == true}
        result.feedEntriesReadStatus.findAll {it.key.startsWith('f1e2')}.any {it.value == true}
        result.feedEntriesReadStatus.findAll {it.key.startsWith('f2e3')}.size() == 0
        1 * userFeedRepository.loadUserFeeds(userId) >> foundUserFeeds
    }

    def "In case feeds do not have a publish date"() {
        given:
        def userId = '1233'
        def userRelatedFeedLoad = new UserRelatedLoadFeed(userId: userId)


        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: 'asd'))
        feedModel.addFeedEntry(new FeedEntryModel(id: 'asd2'))

        def foundUserFeeds = [new UserRelatedFeedModel(feedModel, new FeedToUserAssociation())]

        when:
        def result = userFeedsTimeLineLoader.loadFeed(userRelatedFeedLoad)

        then:
        result instanceof FeedModel
        result.feedEntryModels[0] == feedModel.feedEntryModels[0]
        result.feedEntryModels[1] == feedModel.feedEntryModels[1]
        1 * userFeedRepository.loadUserFeeds(userId) >> foundUserFeeds
    }

    private FeedModel getSecondFeedModel() {
        def secondFeedModel = new FeedModel()
        secondFeedModel.addFeedEntry(new FeedEntryModel(id: 'f2e1', updated: new Date(2012, 11, 13, 12, 22)))
        secondFeedModel.addFeedEntry(new FeedEntryModel(id: 'f2e2', updated: new Date(2012, 11, 12, 14, 22)))
        secondFeedModel.addFeedEntry(new FeedEntryModel(id: 'f2e3', updated: new Date(2012, 11, 12, 12, 22)))
        secondFeedModel.addFeedEntry(new FeedEntryModel(id: 'f2e4', updated: new Date(2012, 11, 11, 10, 22)))
        return secondFeedModel;
    }

    private FeedModel getFirstFeedModel() {
        def firstFeedModel = new FeedModel()
        firstFeedModel.addFeedEntry(new FeedEntryModel(id: 'f1e1', updated: new Date(2012, 12, 12, 11, 22)))
        firstFeedModel.addFeedEntry(new FeedEntryModel(id: 'f1e2', updated: new Date(2012, 11, 12, 12, 22)))
        firstFeedModel.addFeedEntry(new FeedEntryModel(id: 'f1e3', updated: new Date(2012, 11, 12, 16, 22)))
        return firstFeedModel
    }
}

