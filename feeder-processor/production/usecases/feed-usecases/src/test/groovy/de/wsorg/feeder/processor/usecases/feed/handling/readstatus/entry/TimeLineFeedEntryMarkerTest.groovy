package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.12.12
 * Time: 16:28
 */
class TimeLineFeedEntryMarkerTest extends Specification {
    TimeLineFeedEntryMarker timeLineFeedEntryMarker
    FeedEntryReadMarker singleFeedEntryMarker

    def setup(){
        timeLineFeedEntryMarker = new TimeLineFeedEntryMarker()
        singleFeedEntryMarker = Mock(FeedEntryReadMarker)
        timeLineFeedEntryMarker.singleFeedEntryMarker = singleFeedEntryMarker
    }

    def "Mark time line feed entries with appropriate read status"() {
        given:
        def userId = '123'
        def timeLineFeedUrl = 'http://asdasd'
        def feedUrl1= 'http://asda1'
        def feedUrl2= 'http://asda2'
        def entriesToMark = ["http://asda1@http://asd#asd":true,
                             "http://asda1@http://asd#asd2":true,
                             "http://asda2@http://asd2#asd":false]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                        feedUrl: timeLineFeedUrl,
                                                                        feedEntryUidWithReadabilityStatus: entriesToMark)

        when:
        timeLineFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        1 * singleFeedEntryMarker.markFeedEntries({MarkFeedEntryReadStatusHandling it -> it.userId == userId &&
                                                                                            it.feedUrl == feedUrl1 &&
                                                                                            it.feedEntryUidWithReadabilityStatus.get('http://asd#asd') == true &&
                                                                                            it.feedEntryUidWithReadabilityStatus.get('http://asd#asd2') == true})
        1 * singleFeedEntryMarker.markFeedEntries({MarkFeedEntryReadStatusHandling it -> it.userId == userId &&
                                                                                            it.feedUrl == feedUrl2 &&
                                                                                            it.feedEntryUidWithReadabilityStatus.get('http://asd2#asd') == false})
    }

    def "FeedEntryId does not match the needed format"() {
        given:
        def userId = '123'
        def timeLineFeedUrl = 'http://asdasd'
        def entriesToMark = ["http://asda1http://asd#asd":true]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                        feedUrl: timeLineFeedUrl,
                                                                        feedEntryUidWithReadabilityStatus: entriesToMark)

        when:
        timeLineFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided feed entry of the time line does not contain the apropriate separator (@)'
    }
}
