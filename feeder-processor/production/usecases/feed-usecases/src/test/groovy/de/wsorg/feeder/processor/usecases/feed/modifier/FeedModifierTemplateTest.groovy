package de.wsorg.feeder.processor.usecases.feed.modifier;


import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.updater.repository.FeedRepository
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.08.12
 */
public class FeedModifierTemplateTest extends Specification {

    TestFeedModifier feedModifierTemplate
    FeedDataAccess feedDataAccess
    FeedToUserAssociationStorage feedToUserAssociationStorage
    FeedRepository feedLoader
    FeederRepositoryProvider feederRepositoryProvider

    private static FeedModel feedModelPassedToManipulate
    private static FeedModel feedModelFinallyManipulated = new FeedModel()

    def setup(){
        feedModifierTemplate = new TestFeedModifier()
        feedDataAccess = Mock(FeedDataAccess)
        feedLoader = Mock(FeedRepository)
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        feederRepositoryProvider = Mock(FeederRepositoryProvider)

        feederRepositoryProvider.getFeedRepository() >> feedLoader

        feedModifierTemplate.feedToUserAssociationStorage = feedToUserAssociationStorage
        feedModifierTemplate.feedDataAccess = feedDataAccess
        feedModifierTemplate.feederRepositoryProvider = feederRepositoryProvider

        feedModelPassedToManipulate = null
    }

    def "Modify a feed containing in a database"() {
        given:
        def feedUrl = 'http://bla:sd'
        def titleToModify = 'newTitle'
        def userId='12345'

        and: 'The datasource will provide us this feed'
        FeedModel feed = new FeedModel(title: 'test title',
                                       description: 'test descr')

        and:
        feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.OWNER) >> true

        when:
        feedModifierTemplate.adjustFeed(feedUrl, userId, titleToModify);

        then:
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> feed
        feedModelPassedToManipulate == feed
        1 * feedDataAccess.save(FeedModifierTemplateTest.feedModelFinallyManipulated)
    }

    def "If feed is not in db then load it and perform the change"() {
        given:
        def feedUrl = 'http://blaFeed'
        def somethingToModify = "title"
        def userId='12345'

        and:
        feedDataAccess.findByFeedUrl(feedUrl) >> null
        feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.OWNER) >> true

        and:
        def feedToBeFoundFromLoader = new FeedModel()

        when:
        feedModifierTemplate.adjustFeed(feedUrl, userId, somethingToModify);

        then:
        1 * feedLoader.loadFeed(feedUrl) >> feedToBeFoundFromLoader
        feedToBeFoundFromLoader == feedModelPassedToManipulate
        1 * feedDataAccess.save(feedToBeFoundFromLoader)
    }

    def "Feed is not in DB and could not be loaded by loader"() {
        given:
        def feedUrl = 'http://blaFeed'
        def somethingToModify = "title"
        def userId='12345'

        and:
        feedDataAccess.findByFeedUrl(feedUrl) >> null
        feedLoader.loadFeed(feedUrl) >> null
        feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.OWNER) >> true

        when:
        feedModifierTemplate.adjustFeed(feedUrl, userId, somethingToModify);

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided feed url is not in DB and can not be loaded from an external source!"
    }

    def "If no userId is provided, throw an exception"() {
        given:
        def feedUrl = 'http://blaFeed'
        def somethingToModify = "title"
        def userId=''


        when:
        feedModifierTemplate.adjustFeed(feedUrl, userId, somethingToModify);

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "Feed modification can not be performed. No userId is provided."
    }

    def "Make sure the user owns this feed before he is able to manipulate it"() {
        given:
        def feedUrl = 'http://blaFeed'
        def somethingToModify = "title"
        def userId='12345'

        and:
        feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.OWNER) >> false

        when:
        def result = feedModifierTemplate.adjustFeed(feedUrl, userId, somethingToModify)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == "The provided user (${userId}) has no ownership over this feedUrl: ${feedUrl}"
    }
    
    private static class TestFeedModifier extends FeedModifierTemplate<String> {

        @Override
        FeedModel manipulateFeed(final FeedModel feedModel, final String manipulatingData) {
            FeedModifierTemplateTest.feedModelPassedToManipulate = feedModel
            return feedModelFinallyManipulated
        }
    }
}
