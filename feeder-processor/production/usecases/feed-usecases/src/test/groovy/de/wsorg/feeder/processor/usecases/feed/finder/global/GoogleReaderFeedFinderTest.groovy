package de.wsorg.feeder.processor.usecases.feed.finder.global

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import de.wsorg.feeder.processor.usecases.feed.finder.global.source.GoogleReaderFeedFinder
import de.wsorg.feeder.processor.usecases.util.wrapper.JavaUrlWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.12.12
 */
class GoogleReaderFeedFinderTest extends Specification {
    GoogleReaderFeedFinder googleReaderFeedFinder
    JavaUrlWrapper javaUrlWrapper

    def setup(){
        googleReaderFeedFinder = new GoogleReaderFeedFinder()
        javaUrlWrapper = Mock(JavaUrlWrapper)
        googleReaderFeedFinder.javaUrlWrapper = javaUrlWrapper
    }

    def "Find a feed using google ajax api"() {
        given:
        def GlobalSearchQuery query = new GlobalSearchQuery(searchTerm: 'bla')

        and:
        def expectedContent = '{"responseData": {"query":"englishrussia","entries":[{"url":"http://englishrussia.com/feed/","title":"English Russia » Daily entertainment news from Russia. In English!","contentSnippet":"buying seroquel now online allergic seroquel conjunctivitis buy lexapro","link":"http://www.englishrussia.com/"}]}, "responseDetails": null, "responseStatus": 200}'
        def connection = Mock(HttpURLConnection)
        connection.getInputStream() >> new ByteArrayInputStream(expectedContent.getBytes('UTF-8'))

        when:
        List<FeedModel> result = googleReaderFeedFinder.findFeeds(query)

        then:
        result != null
        result.size() == 1
        result[0].feedUrl == 'http://englishrussia.com/feed/'
        result[0].title == "English Russia » Daily entertainment news from Russia. In English!"
        result[0].description == "buying seroquel now online allergic seroquel conjunctivitis buy lexapro"
        1 * javaUrlWrapper.getUrlConnection({it == """https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=${query.searchTerm}"""}) >> connection
    }

    def "Normalize search string"() {
        given:
        def GlobalSearchQuery query = new GlobalSearchQuery(searchTerm: 'bla böa')

        and:
        def expectedContent = '{"responseData": {"query":"englishrussia","entries":[{"url":"http://englishrussia.com/feed/","title":"English Russia » Daily entertainment news from Russia. In English!","contentSnippet":"buying seroquel now online allergic seroquel conjunctivitis buy lexapro","link":"http://www.englishrussia.com/"}]}, "responseDetails": null, "responseStatus": 200}'
        def connection = Mock(HttpURLConnection)
        connection.getInputStream() >> new ByteArrayInputStream(expectedContent.getBytes('UTF-8'))

        when:
        googleReaderFeedFinder.findFeeds(query)

        then:
        1 * javaUrlWrapper.getUrlConnection({it == """https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=${query.searchTerm.replace(' ', '%20')}"""}) >> connection
    }

    def "Handle exception during request to google reader api"() {
        given:
        def GlobalSearchQuery query = new GlobalSearchQuery(searchTerm: 'bla böa')

        and:
        javaUrlWrapper.getUrlConnection(_) >> {throw new IOException()}

        when:
        googleReaderFeedFinder.findFeeds(query)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'Could not perform request to GoogleReader.'
    }

    def "Handle exception during parsing of the JSon-String"() {
        given:
        def GlobalSearchQuery query = new GlobalSearchQuery(searchTerm: 'bla')

        and:
        def expectedContent = 'invalid json'
        def connection = Mock(HttpURLConnection)
        connection.getInputStream() >> new ByteArrayInputStream(expectedContent.getBytes('UTF-8'))

        and:
        javaUrlWrapper.getUrlConnection(_) >> connection

        when:
        googleReaderFeedFinder.findFeeds(query)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'Error during parsing of response from google reader.'
    }
}
