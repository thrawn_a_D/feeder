package de.wsorg.feeder.processor.usecases.feed.loader.modification

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.03.13
 */
class ShowOnlyUnreadEntriesModificatorTest extends Specification {
    ShowOnlyUnreadEntriesModificator showOnlyUnreadEntriesModificator

    def setup(){
        showOnlyUnreadEntriesModificator = new ShowOnlyUnreadEntriesModificator()
    }

    def "Filter entries by their read status and show only unread elements"() {
        given:
        def feedEntry1 = new FeedEntryModel()
        def feedEntry2 = new FeedEntryModel()

        feedEntry1.id = 'fe1'
        feedEntry2.id = 'fe2'

        and:
        def feedAssociationData = new FeedToUserAssociation()
        feedAssociationData.feedEntriesReadStatus.put(feedEntry1.id, true);

        and:
        def feedModel = new UserRelatedFeedModel(new FeedModel(), feedAssociationData)

        feedModel.addFeedEntry(feedEntry1)
        feedModel.addFeedEntry(feedEntry2)

        and:
        def loadFeedCommand = new UserRelatedLoadFeed(showOnlyUnreadEntries: true)

        when:
        UserRelatedFeedModel result = showOnlyUnreadEntriesModificator.modifyFeed(feedModel, loadFeedCommand)

        then:
        result.feedEntries[0].id == 'fe2'
        result.feedToUserAssociation.feedEntriesReadStatus.size() == 0
    }

    def "Provide a load request not related to a user"() {
        given:
        def feedModel = new FeedModel(id: '1', description: 'bla')
        def loadRequest = new LoadFeed()


        when:
        def result = showOnlyUnreadEntriesModificator.modifyFeed(feedModel, loadRequest)

        then:
        result == feedModel
    }
}
