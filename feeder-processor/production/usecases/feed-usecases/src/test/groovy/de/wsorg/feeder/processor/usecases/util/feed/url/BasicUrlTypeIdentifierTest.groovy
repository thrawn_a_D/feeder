package de.wsorg.feeder.processor.usecases.util.feed.url

import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.12.12
 * Time: 15:41
 */
class BasicUrlTypeIdentifierTest extends Specification {
    BasicUrlTypeIdentifier timeLineUrlIdentifier

    def setup(){
        timeLineUrlIdentifier = new BasicUrlTypeIdentifier()
    }

    def "Determinate internal time lime feed"() {
        given:
        def internalFeedUrl = 'http://feeder.net/timeline/12343'

        when:
        def result = timeLineUrlIdentifier.determinateUrlType(internalFeedUrl)

        then:
        result == UrlType.INTERNAL_TIME_LINE
    }

    def "Determinate external feed"() {
        given:
        def externalFeed = 'http://bla.com/blub'

        when:
        def result = timeLineUrlIdentifier.determinateUrlType(externalFeed)

        then:
        result == UrlType.NORMAL
    }

    def "Determinate normal internal feeds"() {
        given:
        def internalFeedUrl = 'http://feeder.net/12343'

        when:
        def result = timeLineUrlIdentifier.determinateUrlType(internalFeedUrl)

        then:
        result == UrlType.NORMAL_INTERNAL
    }
}
