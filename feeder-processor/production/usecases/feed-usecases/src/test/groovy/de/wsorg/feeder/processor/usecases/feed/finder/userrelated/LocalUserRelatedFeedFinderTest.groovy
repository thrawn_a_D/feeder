package de.wsorg.feeder.processor.usecases.feed.finder.userrelated

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.user.UserRelatedSearchQuery
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.10.12
 */
class LocalUserRelatedFeedFinderTest extends Specification {
    LocalUserRelatedFeedFinder localUserRelatedFeedFinder
    UsersFeedsStorage usersFeedsStorage
    UseCaseStorageProvider useCaseStorageProvider

    def setup(){
        localUserRelatedFeedFinder = new LocalUserRelatedFeedFinder()
        usersFeedsStorage = Mock(UsersFeedsStorage)
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getUsersFeedsStorage() >> usersFeedsStorage
        localUserRelatedFeedFinder.useCaseStorageProvider = useCaseStorageProvider
    }

    def "Find user feeds independent of restrictions"() {
        given:
        UserRelatedSearchQuery searchQuery = new UserRelatedSearchQuery(userId: '12345', searchTerm: 'blub')

        and:
        def expectedUserFeeds = [new UserRelatedFeedModel(new FeedModel(feedUrl: '1'), new FeedToUserAssociation()),
                                 new UserRelatedFeedModel(new FeedModel(feedUrl: '2'), new FeedToUserAssociation())]

        when:
        def result = localUserRelatedFeedFinder.findFeeds(searchQuery);

        then:
        result == expectedUserFeeds
        1 * usersFeedsStorage.getUserFeeds(searchQuery.userId, searchQuery.searchTerm) >> expectedUserFeeds
    }

    def "Do not search for feeds with title if no title text is set"() {
        given:
        UserRelatedSearchQuery searchQuery = new UserRelatedSearchQuery(userId: '12345')

        and:
        def expectedUserFeeds = [new UserRelatedFeedModel(new FeedModel(feedUrl: '1'), new FeedToUserAssociation()),
                new UserRelatedFeedModel(new FeedModel(feedUrl: '2'), new FeedToUserAssociation())]

        when:
        def result = localUserRelatedFeedFinder.findFeeds(searchQuery);

        then:
        result == expectedUserFeeds
        1 * usersFeedsStorage.getUserFeeds(searchQuery.userId) >> expectedUserFeeds
    }
}
