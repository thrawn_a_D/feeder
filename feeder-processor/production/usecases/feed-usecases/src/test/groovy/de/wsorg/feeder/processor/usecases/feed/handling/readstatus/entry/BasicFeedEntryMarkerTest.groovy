package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.11.12
 */
class BasicFeedEntryMarkerTest extends Specification {
    BasicFeedEntryMarker basicFeedEntryMarker
    FeedToUserAssociationStorage feedToUserAssociationStorage
    UseCaseStorageProvider useCaseStorageProvider

    def setup(){
        basicFeedEntryMarker = new BasicFeedEntryMarker()
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getFeedToUserAssociationStorage() >> feedToUserAssociationStorage
        basicFeedEntryMarker.useCaseStorageProvider = useCaseStorageProvider
    }

    def "Adjust feed association domain with feed entry read status"() {
        given:
        def userId = '1235'
        def feedUrl = 'http://kjhkjh'
        def feedUids = ['uzg807g':true]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedUids)
        and:
        def relatedFeedToUserAssociation = new FeedToUserAssociation(userId: userId,
                                                                     feedUrl: feedUrl)

        when:
        basicFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> true
        1 * feedToUserAssociationStorage.getUserFeedAssociation(feedUrl, userId) >> relatedFeedToUserAssociation
        1 * feedToUserAssociationStorage.save({association -> association.feedEntriesReadStatus == feedUids})
    }

    def "Consolidate feed read status using an already existing markage and the provided one"() {
        given:
        def userId = '1235'
        def feedUrl = 'http://kjhkjh'
        def feedUids = ['uzg807g':true, 'ljbiu7':false]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedUids)

        and:
        def relatedFeedToUserAssociation = new FeedToUserAssociation(userId: userId,
                                                                     feedUrl: feedUrl,
                                                                     feedEntriesReadStatus: ['kuhgo':true])

        when:
        basicFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> true
        1 * feedToUserAssociationStorage.getUserFeedAssociation(feedUrl, userId) >> relatedFeedToUserAssociation
        1 * feedToUserAssociationStorage.save({FeedToUserAssociation association ->
                                                  association.feedEntriesReadStatus.size() == 2
                                                  association.feedEntriesReadStatus['uzg807g'] == true &&
                                                  association.feedEntriesReadStatus['kuhgo'] == true})
    }

    def "Remove a feed entry which already has been marked as read in the past if current status is back to false"() {
        given:
        def userId = '1235'
        def feedUrl = 'http://kjhkjh'
        def feedUids = ['ljbiu7':false]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedUids)

        and:
        def relatedFeedToUserAssociation = new FeedToUserAssociation(userId: userId,
                                                    feedUrl: feedUrl,
                                                    feedEntriesReadStatus: ['ljbiu7':true])

        when:
        basicFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> true
        1 * feedToUserAssociationStorage.getUserFeedAssociation(feedUrl, userId) >> relatedFeedToUserAssociation
        1 * feedToUserAssociationStorage.save({FeedToUserAssociation association ->
                                                    association.feedEntriesReadStatus.size() == 0})
    }

    def "Make sure no duplicates are set"() {
        given:
        def userId = '1235'
        def feedUrl = 'http://kjhkjh'
        def feedUids = ['ljbiu7':true]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedUids)

        and:
        def relatedFeedToUserAssociation = new FeedToUserAssociation(userId: userId,
                                                                     feedUrl: feedUrl,
                                                                     feedEntriesReadStatus: ['ljbiu7':true])

        when:
        basicFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        1 * feedToUserAssociationStorage.isAssociationGiven(feedUrl, userId, AssociationType.SUBSCRIBER) >> true
        1 * feedToUserAssociationStorage.getUserFeedAssociation(feedUrl, userId) >> relatedFeedToUserAssociation
        1 * feedToUserAssociationStorage.save({FeedToUserAssociation association ->
            association.feedEntriesReadStatus.size() == 1})
    }

    def "There is no association for this user and feed, throw exception!"() {
        given:
        def userId = '1235'
        def feedUrl = 'http://kjhkjh'
        def feedUids = ['uzg807g':true]
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedUids)

        when:
        basicFeedEntryMarker.markFeedEntries(feedMarkHandleModel)

        then:
        def ex = thrown(UseCaseProcessException)
        ex.message == "Could not mark feed entries as read, as no subscription to this feed is currently given (userId: ${userId}, feedUrl:${feedUrl})."
    }
}
