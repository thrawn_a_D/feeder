package de.wsorg.feeder.processor.usecases.feed.loader.modification

import de.wsorg.feeder.processor.production.domain.feeder.PagingRange
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.LoadFeed
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.12.12
 */
class FeedEntryPageSizeModificatorTest extends Specification {
    FeedEntryPageSizeModificator feedEntryPageSizeModificator

    def setup(){
        feedEntryPageSizeModificator = new FeedEntryPageSizeModificator()
    }

    def "If page size starts with 1 and ends with Integer.MAX then do no page cutting (Default)"() {
        given:
        def loadFeed = new LoadFeed()
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel())
        feedModel.addFeedEntry(new FeedEntryModel())
        feedModel.addFeedEntry(new FeedEntryModel())

        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result != null
        result.feedEntryModels.size() == 3
    }

    def "Cut the feed using pages from 1 to 3 out of 5"() {
        given:
        PagingRange pagingSettings = new PagingRange(endIndex: 3)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '2'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '3'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '4'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '5'))

        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result != null
        result.feedEntryModels.size() == 2
        result.feedEntryModels[0].id == '1'
        result.feedEntryModels[1].id == '2'
    }

    def "Cut the feed using pages from 5 to 9 out of 10"() {
        given:
        PagingRange pagingSettings = new PagingRange(startIndex: 5, endIndex: 9)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '2'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '3'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '4'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '5'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '6'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '7'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '8'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '9'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '10'))

        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result != null
        result.feedEntryModels.size() == 4
    }

    def "Wanted starting range outside of bound"() {
        given:
        PagingRange pagingSettings = new PagingRange(startIndex: -1)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))


        when:
        feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.getMessage() == 'The provided page range is invalid! Starting page is under 1.'
    }

    def "Wanted end range outside of possible bound"() {
        given:
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '2'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '3'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '4'))
        PagingRange pagingSettings = new PagingRange(endIndex: feedModel.feedEntryModels.size() + 5)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)


        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result.feedEntryModels.size() == feedModel.feedEntryModels.size()
    }

    def "Page range is null"() {
        given:
        def loadFeed = new LoadFeed(feedEntryPagingRange: null)
        def feedModel = new FeedModel()

        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result == feedModel
    }

    def "End page is not lower than start page"() {
        given:
        PagingRange pagingSettings = new PagingRange(startIndex: 3, endIndex: 1)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '2'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '3'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '4'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '5'))

        when:
        feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        thrown(IllegalArgumentException)
    }

    def "Tell if there is still paging possible if there are entries left outside"() {
        given:
        PagingRange pagingSettings = new PagingRange(startIndex: 1, endIndex: 3)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()
        feedModel.addFeedEntry(new FeedEntryModel(id: '1'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '2'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '3'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '4'))
        feedModel.addFeedEntry(new FeedEntryModel(id: '5'))

        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result != null
        result.getFeedEntriesCollection().additionalPagingPossible == true
    }

    def "Provide an empty list and try to cut a range based on that"() {
        given:
        PagingRange pagingSettings = new PagingRange(startIndex: 1, endIndex: 3)
        def loadFeed = new LoadFeed(feedEntryPagingRange: pagingSettings)
        def feedModel = new FeedModel()


        when:
        def result = feedEntryPageSizeModificator.modifyFeed(feedModel, loadFeed)

        then:
        result == feedModel
    }
}
