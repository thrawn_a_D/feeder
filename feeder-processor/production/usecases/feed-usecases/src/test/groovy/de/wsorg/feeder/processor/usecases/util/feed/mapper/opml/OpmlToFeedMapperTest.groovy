package de.wsorg.feeder.processor.usecases.util.feed.mapper.opml

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.usecases.domain.generated.opml.Body
import de.wsorg.feeder.processor.usecases.domain.generated.opml.OPML
import de.wsorg.feeder.processor.usecases.domain.generated.opml.Outline
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.07.12
 */
class OpmlToFeedMapperTest extends Specification {
    OpmlToFeedMapper opmlToFeedMapper

    def setup(){
        opmlToFeedMapper = new OpmlToFeedMapper()
    }

    def "Test opml outline is empty"(){
        given:
        def opml = Mock(OPML)
        def body = Mock(Body)
        def outerOutlineList = new ArrayList<FeedModel>();
        outerOutlineList.size() >> 0
        body.outline >> outerOutlineList
        opml.body >> body

        when:
        List<FeedModel> results = opmlToFeedMapper.getFeedByOpml(opml)

        then:
        results != null
        results.size() == 0
    }

    def "Test opml outline is null"(){
        given:
        def opml = Mock(OPML)
        def body = Mock(Body)
        body.outline >> null
        opml.body >> body

        when:
        List<FeedModel> results = opmlToFeedMapper.getFeedByOpml(opml)

        then:
        results != null
        results.size() == 0
    }

    def "Map opml to feed with the necessary fields"(){
        given: "OPML test data"
        def opml = Mock(OPML)
        Outline innerOutline = getFinalOutlineStub(opml)

        and: "Prepare the test values"
        def titleValue = 'Title'
        def testDescriptionValue = 'Test description'
        def testCreatedValue = '12/02/1997'
        def feedUrl = 'http://bla.blub.com/lalala.xml'

        innerOutline.text >> titleValue
        innerOutline.description >> testDescriptionValue
        innerOutline.created >> testCreatedValue
        innerOutline.xmlUrl >> feedUrl

        when:
        List<FeedModel> feedList = opmlToFeedMapper.getFeedByOpml(opml)

        then:
        feedList != null
        feedList[0].title == titleValue
        feedList[0].description == testDescriptionValue
        feedList[0].updated.format('dd/MM/yyyy') == testCreatedValue
        feedList[0].feedUrl == feedUrl
    }

    def "Parse date is null"(){
        given: "OPML test data"
        def opml = Mock(OPML)
        Outline innerOutline = getFinalOutlineStub(opml)

        and: "Prepare the test values"
        def titleValue = 'Title'
        def testCreatedValue = null

        innerOutline.text >> titleValue
        innerOutline.created >> testCreatedValue

        when:
        List<FeedModel> feedList = opmlToFeedMapper.getFeedByOpml(opml);

        then:
        feedList != null
        feedList[0].title == titleValue
    }

    def "Parse date with an invalid date"(){
        given: "OPML test data"
        def opml = Mock(OPML)
        Outline innerOutline = getFinalOutlineStub(opml)

        and: "Prepare the test values"
        def titleValue = 'Title'
        def testCreatedValue = '12#02#19'

        innerOutline.text >> titleValue
        innerOutline.created >> testCreatedValue

        when:
        List<FeedModel> feedList = opmlToFeedMapper.getFeedByOpml(opml);

        then:
        def ex = thrown(RuntimeException)
        ex.message.contains('Unparseable date: "12#02#19"')
    }

    private Outline getFinalOutlineStub(final OPML opml) {
        def body = Mock(Body)
        def outerOutlineList = new ArrayList<Outline>();
        def innerOutlineList = new ArrayList<Outline>();
        def outerOutline = Mock(Outline)
        def innerOutline = Mock(Outline)

        outerOutlineList.add(outerOutline);
        outerOutline.outline >> innerOutlineList
        innerOutlineList.add(innerOutline);
        body.outline >> outerOutlineList
        opml.body >> body
        return innerOutline
    }
}
