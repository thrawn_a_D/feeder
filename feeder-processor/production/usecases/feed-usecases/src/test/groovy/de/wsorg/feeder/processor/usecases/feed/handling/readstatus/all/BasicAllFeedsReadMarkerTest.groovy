package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.all

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.handling.all.MarkAllFeedsReadStatusHandling
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.06.13
 */
class BasicAllFeedsReadMarkerTest extends Specification {
    BasicAllFeedsReadMarker basicAllFeedsReadMarker
    UseCaseStorageProvider useCaseStorageProvider
    FeedToUserAssociationStorage feedToUserStorage
    FeedDataAccess feedDataAccess

    def setup(){
        basicAllFeedsReadMarker = new BasicAllFeedsReadMarker()
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        basicAllFeedsReadMarker.useCaseStorageProvider = useCaseStorageProvider

        feedToUserStorage = Mock(FeedToUserAssociationStorage)
        useCaseStorageProvider.getFeedToUserAssociationStorage() >> feedToUserStorage

        feedDataAccess = Mock(FeedDataAccess)
        useCaseStorageProvider.getFeedDataAccess() >> feedDataAccess
    }

    def "Mark all feeds related to provided setting"() {
        given:
        def readingStatus = new MarkAllFeedsReadStatusHandling()
        readingStatus.feedsReadStatus = true
        readingStatus.category = 'category'
        readingStatus.userId = 'asde2fwr332f'

        and:
        def association = new FeedToUserAssociation(feedUrl: 'asdasd')

        and:
        def feed = new FeedModel()
        def feedEntry = new FeedEntryModel(id: '7r87f765fr76f6')
        feed.addFeedEntry(feedEntry)

        when:
        basicAllFeedsReadMarker.markAllFeeds(readingStatus)

        then:
        1 * feedToUserStorage.getUserFeeds(readingStatus.userId) >> [association]
        1 * feedDataAccess.findByFeedUrl(association.feedUrl) >> feed
        1 * feedToUserStorage.save({FeedToUserAssociation it -> it.feedEntriesReadStatus[feedEntry.id] == readingStatus.feedsReadStatus})
    }
}
