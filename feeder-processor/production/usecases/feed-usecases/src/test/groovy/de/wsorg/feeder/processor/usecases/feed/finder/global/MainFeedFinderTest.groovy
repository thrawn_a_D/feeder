package de.wsorg.feeder.processor.usecases.feed.finder.global;


import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.09.12
 */
public class MainFeedFinderTest extends Specification {

    MainFeedFinder mainFeedFinder
    GlobalFeedFinder externalFinder
    GlobalFeedFinder localFeedFinder

    def setup(){
        mainFeedFinder = new MainFeedFinder()
        externalFinder = Mock(GlobalFeedFinder)
        localFeedFinder = Mock(GlobalFeedFinder)
        mainFeedFinder.externalFeedFinder = externalFinder
        mainFeedFinder.localFeedFinder = localFeedFinder
    }

    def "Find feeds using both external and internal sources"() {
        given:
        GlobalSearchQuery searchQuery = new GlobalSearchQuery(searchTerm: 'my search')

        and:
        def expectedFoundExternalFeeds = [new FeedModel(feedUrl: '1'), new FeedModel(feedUrl: '2')]
        def expectedFoundLocalFeeds = [new FeedModel(feedUrl: '3'), new FeedModel(feedUrl: '4')]

        when:
        def result = mainFeedFinder.findFeeds(searchQuery)

        then:
        result.size() == 4
        1 * externalFinder.findFeeds(searchQuery) >> expectedFoundExternalFeeds
        1 * localFeedFinder.findFeeds(searchQuery) >> expectedFoundLocalFeeds
    }

    def "Make sure only distinct results are provided"() {
        given:
        GlobalSearchQuery searchQuery = new GlobalSearchQuery(searchTerm: 'my search')

        and:
        def expectedFoundExternalFeeds = [new FeedModel(feedUrl: '1'), new FeedModel(feedUrl: '2')]
        def expectedFoundLocalFeeds = [new FeedModel(feedUrl: '1'), new FeedModel(feedUrl: '4')]

        when:
        def result = mainFeedFinder.findFeeds(searchQuery)

        then:
        result.size() == 3
        result.find {it.feedUrl == '1'}
        result.find {it.feedUrl == '2'}
        result.find {it.feedUrl == '4'}
        1 * externalFinder.findFeeds(searchQuery) >> expectedFoundExternalFeeds
        1 * localFeedFinder.findFeeds(searchQuery) >> expectedFoundLocalFeeds
    }

    def "Prefer external feeds over internal but enrich them with local metadata"() {
        given:
        GlobalSearchQuery searchQuery = new GlobalSearchQuery(searchTerm: 'my search')

        and:
        def expectedFoundExternalFeeds = [new FeedModel(feedUrl: '1'), new FeedModel(feedUrl: '2')]
        def expectedFoundLocalFeeds = [new UserRelatedFeedModel(new FeedModel(feedUrl: '1'), new FeedToUserAssociation()), new FeedModel(feedUrl: '4')]

        when:
        def result = mainFeedFinder.findFeeds(searchQuery)

        then:
        result.any {it instanceof UserRelatedFeedModel && it.feedUrl == '1'}
        1 * externalFinder.findFeeds(searchQuery) >> expectedFoundExternalFeeds
        1 * localFeedFinder.findFeeds(searchQuery) >> expectedFoundLocalFeeds
    }

    def "Make sure local feed finder is still executed when the external throws an exception"() {
        given:
        GlobalSearchQuery searchQuery = new GlobalSearchQuery(searchTerm: 'my search')

        and:
        externalFinder.findFeeds(_) >> {throw new RuntimeException()}

        and:
        def expectedFoundLocalFeeds = [new UserRelatedFeedModel(new FeedModel(feedUrl: '1'), new FeedToUserAssociation()), new FeedModel(feedUrl: '4')]

        when:
        def result= mainFeedFinder.findFeeds(searchQuery)

        then:
        result == expectedFoundLocalFeeds
        1 * localFeedFinder.findFeeds(searchQuery) >> expectedFoundLocalFeeds
    }
}
