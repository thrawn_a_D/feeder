package de.wsorg.feeder.processor.usecases.feed.loader.user

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.production.domain.feeder.load.UserRelatedLoadFeed
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.updater.repository.FeedRepository
import de.wsorg.feeder.processor.usecases.util.repository.FeederRepositoryProvider
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.01.13
 */
class CommonUserFeedLoaderTest extends Specification {
    CommonUserFeedLoader commonUserFeedLoader
    FeederRepositoryProvider feederRepositoryProvider
    FeedRepository feedRepository
    FeedToUserAssociationStorage feedToUserAssociationStorage
    UseCaseStorageProvider useCaseStorageProvider

    def setup(){
        commonUserFeedLoader = new CommonUserFeedLoader()
        feederRepositoryProvider = Mock(FeederRepositoryProvider)
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        feedRepository = Mock(FeedRepository)

        feederRepositoryProvider.getFeedRepository() >> feedRepository
        commonUserFeedLoader.feederRepositoryProvider = feederRepositoryProvider

        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getFeedToUserAssociationStorage() >> feedToUserAssociationStorage
        commonUserFeedLoader.useCaseStorageProvider = useCaseStorageProvider
    }

    def "Provide user Id and enrich the found feed with data"() {
        given:
        def feedUrl = 'http://feedUrl'
        def userId = '1234'
        def loadFeed = new UserRelatedLoadFeed(feedUrl: feedUrl, userId: userId)

        and:
        def feedToReceive = new FeedModel()

        when:
        def result = commonUserFeedLoader.loadFeed(loadFeed)

        then:
        result instanceof UserRelatedFeedModel
        1 * feedRepository.loadFeed(feedUrl) >> feedToReceive
        1 * feedToUserAssociationStorage.getUserFeedAssociation(feedUrl, userId) >> new FeedToUserAssociation()
    }

    def "Nothing found by external and internal feed"() {
        given:
        def feedUrl = 'http://feedUrl'
        def loadFeed = new UserRelatedLoadFeed(feedUrl: feedUrl)


        when:
        def result = commonUserFeedLoader.loadFeed(loadFeed)

        then:
        result == null
    }
}
