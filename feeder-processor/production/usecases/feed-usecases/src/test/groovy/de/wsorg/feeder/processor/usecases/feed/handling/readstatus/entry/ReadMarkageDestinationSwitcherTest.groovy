package de.wsorg.feeder.processor.usecases.feed.handling.readstatus.entry

import de.wsorg.feeder.processor.production.domain.feeder.handling.single.MarkFeedEntryReadStatusHandling
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlType
import de.wsorg.feeder.processor.usecases.util.feed.url.UrlTypeIdentifier
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.12.12
 */
class ReadMarkageDestinationSwitcherTest extends Specification {
    ReadMarkageDestinationSwitcher readMarkageDestinationSwitcher
    UrlTypeIdentifier urlTypeIdentifier
    FeedEntryReadMarker normalFeedEntryMarker
    FeedEntryReadMarker timeLineFeedEntryMarker

    def setup(){
        readMarkageDestinationSwitcher = new ReadMarkageDestinationSwitcher()
        urlTypeIdentifier = Mock(UrlTypeIdentifier)
        normalFeedEntryMarker = Mock(FeedEntryReadMarker)
        timeLineFeedEntryMarker = Mock(FeedEntryReadMarker)
        readMarkageDestinationSwitcher.urlTypeIdentifier = urlTypeIdentifier
        readMarkageDestinationSwitcher.normalFeedEntryMarker = normalFeedEntryMarker
        readMarkageDestinationSwitcher.timeLineFeedEntryMarker = timeLineFeedEntryMarker
    }

    def "Determinate which handler to be used to mark timeline entries"() {
        given:
        def feedUrl = 'http://asd'
        def userId = 'asd'
        def feedEntries = null
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedEntries)

        and:
        urlTypeIdentifier.determinateUrlType(feedUrl) >> UrlType.INTERNAL_TIME_LINE

        when:
        readMarkageDestinationSwitcher.markFeedEntries(feedMarkHandleModel)

        then:
        1 * timeLineFeedEntryMarker.markFeedEntries(feedMarkHandleModel)
    }

    def "Determinate marker handler for normal urls"() {
        given:
        def feedUrl = 'http://asd'
        def userId = 'asd'
        def feedEntries = null
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedEntries)

        and:
        urlTypeIdentifier.determinateUrlType(feedUrl) >> UrlType.NORMAL

        when:
        readMarkageDestinationSwitcher.markFeedEntries(feedMarkHandleModel)

        then:
        1 * normalFeedEntryMarker.markFeedEntries(feedMarkHandleModel)
    }

    def "Determinate marker handler for normal internal urls"() {
        given:
        def feedUrl = 'http://asd'
        def userId = 'asd'
        def feedEntries = null
        def feedMarkHandleModel = new MarkFeedEntryReadStatusHandling(userId: userId,
                                                                       feedUrl: feedUrl,
                                                                       feedEntryUidWithReadabilityStatus: feedEntries)

        and:
        urlTypeIdentifier.determinateUrlType(feedUrl) >> UrlType.NORMAL_INTERNAL

        when:
        readMarkageDestinationSwitcher.markFeedEntries(feedMarkHandleModel)

        then:
        1 * normalFeedEntryMarker.markFeedEntries(feedMarkHandleModel)
    }
}
