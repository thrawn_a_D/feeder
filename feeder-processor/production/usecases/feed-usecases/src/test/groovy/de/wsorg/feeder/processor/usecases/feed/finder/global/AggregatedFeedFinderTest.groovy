package de.wsorg.feeder.processor.usecases.feed.finder.global

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.07.12
 */
class AggregatedFeedFinderTest extends Specification {

    AggregatedFeedFinder allKnownSourcesFinder;

    def setup(){
        allKnownSourcesFinder = new AggregatedFeedFinder();
    }

    def "Find a feed using several sources"(){
        given: "Some test data"
        def usedTestQuery =new GlobalSearchQuery(searchTerm: 'test query to use')

        and: "Use mocks for feed finders"
        def firstFinder = Mock(GlobalFeedFinder)
        def secondFinder = Mock(GlobalFeedFinder)
        allKnownSourcesFinder.sourcesToUseForSearch = [firstFinder, secondFinder]

        when:
        allKnownSourcesFinder.findFeeds(usedTestQuery)

        then:
        1 * firstFinder.findFeeds(usedTestQuery) >> [new FeedModel()]
        1 * secondFinder.findFeeds(usedTestQuery) >> [new FeedModel()]
    }

    def "Check that all results are merged into one"(){
        given: "Some test data"
        def usedTestQuery =new GlobalSearchQuery(searchTerm: 'test query to use')

        and: "Use mocks for feed finders"
        def firstFinder = Mock(GlobalFeedFinder)
        def secondFinder = Mock(GlobalFeedFinder)
        def foundFeedsFromFirstFinder = [new FeedModel(), new FeedModel()]
        def foundFeedsFromSecondFinder = [new FeedModel()]

        allKnownSourcesFinder.sourcesToUseForSearch = [firstFinder, secondFinder]

        when:
        def result = allKnownSourcesFinder.findFeeds(usedTestQuery);

        then:
        1 * firstFinder.findFeeds(usedTestQuery) >> foundFeedsFromFirstFinder
        1 * secondFinder.findFeeds(usedTestQuery) >> foundFeedsFromSecondFinder
        result != null
        result.size() == 3

    }
}
