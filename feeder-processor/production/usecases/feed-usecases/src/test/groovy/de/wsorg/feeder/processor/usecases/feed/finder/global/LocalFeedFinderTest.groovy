package de.wsorg.feeder.processor.usecases.feed.finder.global;


import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.search.global.GlobalSearchQuery
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.usecases.util.storage.UseCaseStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.09.12
 */
public class LocalFeedFinderTest extends Specification {
    LocalFeedFinder localFeedFinder
    FeedDataAccess feedDataAccess;
    UseCaseStorageProvider useCaseStorageProvider

    def setup(){
        localFeedFinder = new LocalFeedFinder()
        feedDataAccess=Mock(FeedDataAccess)
        useCaseStorageProvider = Mock(UseCaseStorageProvider)
        useCaseStorageProvider.getFeedDataAccess() >> feedDataAccess
        localFeedFinder.useCaseStorageProvider = useCaseStorageProvider
    }

    def "Find local feeds"() {
        given:
        def searchQuery = new GlobalSearchQuery(searchTerm: 'nla')

        and:
        def expectedSearchResult = [new FeedModel()]

        when:
        def result = localFeedFinder.findFeeds(searchQuery)

        then:
        result == expectedSearchResult
        1 * feedDataAccess.findByTitleOrDescription(searchQuery.searchTerm) >> expectedSearchResult
    }
}
