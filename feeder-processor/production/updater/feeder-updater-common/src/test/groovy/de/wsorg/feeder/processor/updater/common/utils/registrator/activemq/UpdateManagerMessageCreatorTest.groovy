package de.wsorg.feeder.processor.updater.common.utils.registrator.activemq

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import spock.lang.Specification

import javax.jms.MapMessage
import javax.jms.Session

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.03.13
 */
class UpdateManagerMessageCreatorTest extends Specification {
    def "Create a message creator and send a message"() {
        given:
        def feedUrl = 'kjbkj'
        def action = UpdateManagerAction.CANCEL_UPDATE_OF_FEED

        and:
        def session = Mock(Session)
        def message = Mock(MapMessage)


        when:
        def messageCreator = new UpdateManagerMessageCreator(feedUrl, action)
        def result = messageCreator.createMessage(session)

        then:
        result == message
        1 * message.setString('update-manager-action', action.name())
        1 * message.setString('feedUrl', feedUrl)
        1 * session.createMapMessage() >> message
    }
}
