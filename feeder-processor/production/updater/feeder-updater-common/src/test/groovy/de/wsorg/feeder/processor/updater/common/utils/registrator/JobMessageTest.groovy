package de.wsorg.feeder.processor.updater.common.utils.registrator

import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.QueueMessageFieldNames
import spock.lang.Specification

import javax.jms.MapMessage
import javax.jms.ObjectMessage
import javax.jms.TextMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.04.13
 */
class JobMessageTest extends Specification {
    def "Get job message for map message"() {
        given:
        def feedUrl = 'kjhkjh'
        def action = UpdateManagerAction.REGISTER_FEED_FOR_UPDATE
        def message = Mock(MapMessage)
        message.getString(QueueMessageFieldNames.FEED_URL.columnName) >> feedUrl
        message.getString(QueueMessageFieldNames.UPDATE_MANAGER_ACTION.columnName) >> action

        when:
        def result = JobMessage.getJobMessage(message)

        then:
        result instanceof UpdaterJobMessage
    }

    def "Get job message for a text message"() {
        given:
        def text = Mock(TextMessage)

        when:
        def result = JobMessage.getJobMessage(text)

        then:
        result instanceof StringJobMessage
    }

    def "Provide something invalid"() {
        given:
        def bla = Mock(ObjectMessage)

        when:
        def result = JobMessage.getJobMessage(bla)

        then:
        !result
    }
}
