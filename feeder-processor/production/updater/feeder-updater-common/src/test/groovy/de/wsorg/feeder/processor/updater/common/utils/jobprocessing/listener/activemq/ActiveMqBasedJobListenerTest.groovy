package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.activemq

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.QueueMessageFieldNames
import spock.lang.Specification

import javax.jms.MapMessage
import javax.jms.ObjectMessage
import javax.jms.TextMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.03.13
 */
class ActiveMqBasedJobListenerTest extends Specification {
    ActiveMqBasedJobListener activeMqBasedJobListener

    def setup(){
        activeMqBasedJobListener = new ActiveMqBasedJobListener()
    }

    def "Register a job executor"() {
        given:
        def jobExecutor = Mock(UpdaterMessageJobExecutor)

        when:
        activeMqBasedJobListener.setJobExecutors([jobExecutor])

        then:
        activeMqBasedJobListener.jobExecutors.contains(jobExecutor)
    }

    def "If there is an incomming message, parse the content and delegate to the executors"() {
        given:
        def feedUrl = 'kjhkjh'
        def action = UpdateManagerAction.REGISTER_FEED_FOR_UPDATE
        def message = Mock(MapMessage)
        message.getString(QueueMessageFieldNames.FEED_URL.columnName) >> feedUrl
        message.getString(QueueMessageFieldNames.UPDATE_MANAGER_ACTION.columnName) >> action

        and:
        def jobExecutor = Mock(UpdaterMessageJobExecutor)
        activeMqBasedJobListener.jobExecutors.add(jobExecutor)

        when:
        activeMqBasedJobListener.onMessage(message)

        then:
        1 * jobExecutor.execute({UpdaterJobMessage it -> it.identifier == feedUrl && it.action == action})
    }

    def "Error while retrieving column data"() {
        given:
        def message = Mock(ObjectMessage)

        and:
        def jobExecutor = Mock(UpdaterMessageJobExecutor)
        activeMqBasedJobListener.jobExecutors.add(jobExecutor)

        when:
        activeMqBasedJobListener.onMessage(message)

        then:
        0 * jobExecutor.execute(_)
    }
}
