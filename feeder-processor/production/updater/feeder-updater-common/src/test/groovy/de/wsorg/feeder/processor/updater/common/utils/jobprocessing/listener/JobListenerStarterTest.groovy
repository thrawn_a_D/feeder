package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 29.04.12
 */
class JobListenerStarterTest extends Specification {
    JobListenerStarter jobListenerStarter

    def setup(){
        jobListenerStarter = new JobListenerStarter()
    }

    def "Start listener if appropriate config value is true"() {
        given:
        def propertyValue = 'true'
        jobListenerStarter.propertyValue = propertyValue

        and:
        def listener1 = Mock(JobListener)
        def listener2 = Mock(JobListener)

        and:
        jobListenerStarter.jobListeners = [listener1, listener2]

        when:
        jobListenerStarter.conditionalListenerStart()

        then:
        1 * listener1.startListener()
        1 * listener2.startListener()
    }

    def "No valid boolean value provided"() {
        given:
        def propertyValue = 'asdasd'
        jobListenerStarter.propertyValue = propertyValue

        and:
        def listener1 = Mock(JobListener)
        def listener2 = Mock(JobListener)

        and:
        jobListenerStarter.jobListeners = [listener1, listener2]

        when:
        jobListenerStarter.conditionalListenerStart()

        then:
        0 * listener1.startListener()
        0 * listener2.startListener()
    }

    def "Listener start is disabled"() {
        given:
        def propertyValue = 'false'
        jobListenerStarter.propertyValue = propertyValue

        and:
        def listener1 = Mock(JobListener)
        def listener2 = Mock(JobListener)

        and:
        jobListenerStarter.jobListeners = [listener1, listener2]

        when:
        jobListenerStarter.conditionalListenerStart()

        then:
        0 * listener1.startListener()
        0 * listener2.startListener()
    }
}
