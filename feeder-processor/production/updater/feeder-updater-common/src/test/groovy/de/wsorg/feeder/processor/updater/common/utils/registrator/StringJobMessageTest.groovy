package de.wsorg.feeder.processor.updater.common.utils.registrator

import spock.lang.Specification

import javax.jms.TextMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.04.13
 */
class StringJobMessageTest extends Specification {
    def "Get job message"() {
        given:
        def text = 'asdads'
        def message = Mock(TextMessage)
        message.getText() >> text

        when:
        def result = new StringJobMessage(message)

        then:
        result.identifier == text
        result.content == text
    }
}
