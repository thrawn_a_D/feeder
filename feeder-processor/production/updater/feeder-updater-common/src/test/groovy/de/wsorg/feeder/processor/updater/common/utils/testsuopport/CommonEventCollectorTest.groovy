package de.wsorg.feeder.processor.updater.common.utils.testsuopport

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class CommonEventCollectorTest extends Specification {
    CommonEventCollector commonEventCollector

    def setup(){
        commonEventCollector = new CommonEventCollector()
    }

    def "Inform a registered listener when a single event occurred"() {
        given:
        def someEventName = 'some_event_name'
        def listener = Mock(EventListener)

        and:
        commonEventCollector.setEventListener(listener, [someEventName] as Set)

        when:
        commonEventCollector.eventOccurred(someEventName)

        then:
        1 * listener.interestedEventsOccurred()
    }

    def "Listener is only called on a proper event"() {
        given:
        def someEventName = 'some_event_name'
        def listener = Mock(EventListener)

        and:
        commonEventCollector.setEventListener(listener, [someEventName] as Set)

        when:
        commonEventCollector.eventOccurred('bla')

        then:
        0 * listener.interestedEventsOccurred()
    }

    def "All wanted events must occur before triggering an vent"() {
        given:
        def someEventName1 = 'some_event_name1'
        def someEventName2 = 'some_event_name2'
        def listener = Mock(EventListener)

        and:
        commonEventCollector.setEventListener(listener, [someEventName1, someEventName2] as Set)

        when:
        commonEventCollector.eventOccurred(someEventName1)
        commonEventCollector.eventOccurred(someEventName2)

        then:
        1 * listener.interestedEventsOccurred()
    }

    def "Not all occurred events are wanted: dont trigger listener"() {
        given:
        def someEventName1 = 'some_event_name1'
        def someEventName2 = 'some_event_name2'
        def listener = Mock(EventListener)

        and:
        commonEventCollector.setEventListener(listener, [someEventName1, someEventName2] as Set)

        when:
        commonEventCollector.eventOccurred(someEventName1)
        commonEventCollector.eventOccurred('12asd')

        then:
        0 * listener.interestedEventsOccurred()
    }

    def "If no one is subscribed, do not collect events"() {
        when:
        commonEventCollector.eventOccurred('12asd')

        then:
        !commonEventCollector.eventListener
    }

    def "Exception event occurred, delegate to listener"() {
        given:
        def exception = Mock(Exception)
        def listener = Mock(EventListener)

        and:
        commonEventCollector.setEventListener(listener, [] as Set)

        when:
        commonEventCollector.exceptionEventOccurred(exception)

        then:
        1 * listener.exceptionOccurred(exception)
    }

    def "If no listener registered, no exception can be delegated"() {
        given:
        def exception = Mock(Exception)

        when:
        commonEventCollector.exceptionEventOccurred(exception)

        then:
        true
    }
}
