package de.wsorg.feeder.processor.updater.common.utils.registrator.activemq

import spock.lang.Specification

import javax.jms.Session
import javax.jms.TextMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.04.13
 */
class TextMessageCreatorTest extends Specification {
    def "Create a message for json messages"() {
        given:
        def content = 'asdasd'

        and:
        def session = Mock(Session)
        def message = Mock(TextMessage)


        when:
        def messageCreator = new TextMessageCreator(content)
        def result = messageCreator.createMessage(session)

        then:
        result == message
        1 * session.createTextMessage(content) >> message
    }
}
