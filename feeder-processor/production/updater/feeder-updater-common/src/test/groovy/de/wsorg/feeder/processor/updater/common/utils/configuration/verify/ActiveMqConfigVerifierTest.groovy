package de.wsorg.feeder.processor.updater.common.utils.configuration.verify

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.03.13
 */
class ActiveMqConfigVerifierTest extends Specification {
    ActiveMqConfigVerifier ActiveMqConfigVerifierVerifier
    def location = ['asddd'] as String[]

    def setup(){
        ActiveMqConfigVerifierVerifier = new ActiveMqConfigVerifier()
    }

    def "Verify all valid properties"() {
        given:
        def properties = new Properties()
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_URL, 'http://kljasd')
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT, '80')

        when:
        def result = ActiveMqConfigVerifierVerifier.verifyConfig(location, properties)

        then:
        result.isConfigErroneous() == false
        result.configLocation == location
        result.propertyErrors.size() == 0
    }

    def "Active mq url not set"() {
        given:
        def properties = new Properties()
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_URL, '')
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT, '80')

        when:
        def result = ActiveMqConfigVerifierVerifier.verifyConfig(location, properties)

        then:
        result.isConfigErroneous() == true
        result.configLocation == location
        result.propertyErrors[ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_URL][0] == 'The property is not set.'
    }

    def "Active mq port not set"() {
        given:
        def properties = new Properties()
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_URL, 'http://kljasd')
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT, '')

        when:
        def result = ActiveMqConfigVerifierVerifier.verifyConfig(location, properties)

        then:
        result.isConfigErroneous() == true
        result.configLocation == location
        result.propertyErrors[ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT][0] == 'The property is not set.'
    }

    def "Active mq port is valid number"() {
        given:
        def properties = new Properties()
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_URL, 'http://kljasd')
        properties.put(ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT, 'asd')

        when:
        def result = ActiveMqConfigVerifierVerifier.verifyConfig(location, properties)

        then:
        result.isConfigErroneous() == true
        result.configLocation == location
        result.propertyErrors[ActiveMqConfigVerifier.REPOSITORY_ACTIVE_MQ_PORT][0] == 'Mq port has to be a number'
    }
}
