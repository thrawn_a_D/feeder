package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.filebased

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.03.12
 */
class FileBasedJobListenerTest extends Specification {
    FileBasedJobListener registrationFileBasedJobListener
    FileBasedPersistence fileBasedPersistence

    def setup(){
        registrationFileBasedJobListener = new FileBasedJobListener()
        fileBasedPersistence = Mock(FileBasedPersistence)
        registrationFileBasedJobListener.fileBasedPersistence = fileBasedPersistence
    }

    def "Register a job executor"() {
        given:
        def jobExecutor = Mock(UpdaterMessageJobExecutor)

        when:
        registrationFileBasedJobListener.setJobExecutors([jobExecutor])

        then:
        registrationFileBasedJobListener.jobExecutors.contains(jobExecutor)
    }

    def "Look for new jobs and execute a job if there is one incoming"() {
        given:
        def feedUrl = 'kjhkjlkj'
        def registrationObject = new UpdaterJobMessage(identifier: feedUrl)

        and:
        def storageFolder = 'kjbkh'
        registrationFileBasedJobListener.setSourceFolderToListen(storageFolder)

        and:
        def executor = Mock(UpdaterMessageJobExecutor)
        registrationFileBasedJobListener.jobExecutors.add(executor)

        when:
        registrationFileBasedJobListener.startListener()
        Thread.sleep(10); //Give the thread a chance to complete

        then:
        (1.._) * executor.execute(registrationObject)
        (1.._) * fileBasedPersistence.getFolderContent(storageFolder) >> [registrationObject]
    }

    def "Remove file after job execution has been delegated"() {
        given:
        def feedUrl = 'kjhkjlkj'
        def registrationObject = new UpdaterJobMessage(identifier: feedUrl)

        and:
        def storageFolder = 'kjbkh'
        registrationFileBasedJobListener.setSourceFolderToListen(storageFolder)

        and:
        def executor = Mock(UpdaterMessageJobExecutor)
        registrationFileBasedJobListener.jobExecutors.add(executor)

        when:
        registrationFileBasedJobListener.startListener()
        Thread.sleep(10); //Give the thread a chance to complete

        then:
        (1.._) * executor.execute(registrationObject)
        (1.._) * fileBasedPersistence.remove(storageFolder, feedUrl)
        (1.._) * fileBasedPersistence.getFolderContent(storageFolder) >> [registrationObject]
    }
}
