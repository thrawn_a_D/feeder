package de.wsorg.feeder.processor.updater.common.utils.registrator

import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.QueueMessageFieldNames
import spock.lang.Specification

import javax.jms.MapMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.04.13
 */
class UpdaterJobMessageTest extends Specification {
    def "Provide a map message to initialize"() {
        given:
        def feedUrl = 'kjhkjh'
        def action = UpdateManagerAction.REGISTER_FEED_FOR_UPDATE
        def message = Mock(MapMessage)
        message.getString(QueueMessageFieldNames.FEED_URL.columnName) >> feedUrl
        message.getString(QueueMessageFieldNames.UPDATE_MANAGER_ACTION.columnName) >> action

        when:
        def result = new UpdaterJobMessage(message)

        then:
        result.identifier == feedUrl
        result.action == action
    }
}
