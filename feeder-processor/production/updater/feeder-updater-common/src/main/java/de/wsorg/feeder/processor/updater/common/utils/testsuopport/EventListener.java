package de.wsorg.feeder.processor.updater.common.utils.testsuopport;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public interface EventListener {
    void interestedEventsOccurred();
    void exceptionOccurred(final Exception exception);
}
