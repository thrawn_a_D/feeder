package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UpdaterMessageJobExecutor extends JobExecutor<UpdaterJobMessage> {
}
