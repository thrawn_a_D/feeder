package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.activemq;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.JobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.JobListener;
import de.wsorg.feeder.processor.updater.common.utils.registrator.JobMessage;
import lombok.extern.slf4j.Slf4j;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class ActiveMqBasedJobListener implements JobListener<JobExecutor>, MessageListener {
    private List<JobExecutor> jobExecutors = new ArrayList<>();

    @Override
    public void startListener() {
        // Will get started through spring
    }

    @Override
    public void stopListener() {
        // Will get stopped through spring
    }

    @Override
    public void setJobExecutors(final List<JobExecutor> jobExecutors) {
        this.jobExecutors = jobExecutors;
    }

    @Override
    public void onMessage(final Message message) {
        ActiveMqBasedJobListener.log.debug("Got one message to process!");
        JobMessage updaterJobMessage = JobMessage.getJobMessage(message);

        if (updaterJobMessage != null) {
            for (JobExecutor jobExecutor : jobExecutors) {
                ActiveMqBasedJobListener.log.debug("Delegate execution of a message");
                jobExecutor.execute(updaterJobMessage);
                ActiveMqBasedJobListener.log.debug("Processing finished.");
            }
        }
    }
}
