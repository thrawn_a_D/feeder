package de.wsorg.feeder.processor.updater.common.utils.storage;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UpdaterStorageProvider {
    FeedDataAccess getFeedDataAccess();
    FeedToUserAssociationStorage getFeedToUserAssociationStorage();
    FeedLoadStatisticsDataAccess getFeedLoadStatisticsDataAccess();
}
