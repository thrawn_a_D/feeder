package de.wsorg.feeder.processor.updater.common.utils.registrator;

import lombok.extern.slf4j.Slf4j;

import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.io.Serializable;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public abstract class JobMessage implements Serializable {
    public abstract String getIdentifier();
    protected abstract void init(final Message jmsMessage);

    public static JobMessage getJobMessage(final Message jmsMessage) {
        if (jmsMessage instanceof MapMessage) {
            return new UpdaterJobMessage(jmsMessage);
        } else if(jmsMessage instanceof TextMessage) {
            return new StringJobMessage(jmsMessage);
        } else {
            log.error("Message type could not be determined {}", jmsMessage);
            return null;
        }
    }
}
