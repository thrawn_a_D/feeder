package de.wsorg.feeder.processor.updater.common.utils.registrator;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;

import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.QueueMessageFieldNames;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
@Slf4j
public class UpdaterJobMessage extends JobMessage {
    private String identifier;
    private UpdateManagerAction action;

    public UpdaterJobMessage() {
    }

    public UpdaterJobMessage(final Message jmsMessage) {
        init(jmsMessage);
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }

    @Override
    protected void init(final Message jmsMessage) {
        try {
            MapMessage mapMessage = (MapMessage) jmsMessage;
            final String feedUrl = mapMessage.getString(QueueMessageFieldNames.FEED_URL.getColumnName());
            final String action = mapMessage.getString(QueueMessageFieldNames.UPDATE_MANAGER_ACTION.getColumnName());

            setIdentifier(feedUrl);
            setAction(UpdateManagerAction.valueOf(action));
        } catch (JMSException e) {
            log.error("Incoming message does not contain expected columns:" + e.getMessage());
        }
    }
}
