package de.wsorg.feeder.processor.updater.common.utils.storage;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederStorageProxy implements UpdaterStorageProvider {

    public FeedDataAccess getFeedDataAccess() {
        return FeedStorageFactory.getFeedDAO();
    }

    public FeedToUserAssociationStorage getFeedToUserAssociationStorage() {
        return FeedStorageFactory.getFeedToUserAssociationDAO();
    }

    @Override
    public FeedLoadStatisticsDataAccess getFeedLoadStatisticsDataAccess() {
        return FeedStorageFactory.getFeedLoadStatisticsDataAccess();
    }

    public void setStorageConfig(final StorageConfig storageConfig) {
        final boolean useFileSystemAsStorage = storageConfig.isUseFileSystemAsStorage();
        FeedStorageFactory.setEnableFileBasedStorage(useFileSystemAsStorage);

        FeedStorageFactory.setClientConfig(storageConfig);
    }
}
