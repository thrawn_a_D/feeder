package de.wsorg.feeder.processor.updater.common.utils.testsuopport;

import java.util.Set;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public interface EventCollector {
    void eventOccurred(final String eventTitle);
    void exceptionEventOccurred(final Exception exception);
    void setEventListener(EventListener eventListener, Set<String> interestedInEvents);
}
