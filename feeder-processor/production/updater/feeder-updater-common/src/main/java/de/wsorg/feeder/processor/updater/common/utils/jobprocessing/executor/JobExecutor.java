package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor;

import de.wsorg.feeder.processor.updater.common.utils.registrator.JobMessage;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface JobExecutor<T extends JobMessage> {
    void execute(final T message);
}
