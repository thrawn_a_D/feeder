package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JobListenerStarter {
    @Setter
    private List<JobListener> jobListeners = new ArrayList<>();
    @Setter
    private String propertyValue;

    public void conditionalListenerStart() {
        if(Boolean.valueOf(propertyValue)) {
            startListeners();
        }
    }

    private void startListeners() {
        for (JobListener jobListener : jobListeners) {
            jobListener.startListener();
        }
    }
}
