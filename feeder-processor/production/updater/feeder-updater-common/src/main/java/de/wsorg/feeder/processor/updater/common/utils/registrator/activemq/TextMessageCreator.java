package de.wsorg.feeder.processor.updater.common.utils.registrator.activemq;

import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class TextMessageCreator implements MessageCreator {
    private final String content;

    public TextMessageCreator(final String content) {
        this.content = content;
    }


    @Override
    public Message createMessage(final Session session) throws JMSException {
        return session.createTextMessage(content);
    }
}
