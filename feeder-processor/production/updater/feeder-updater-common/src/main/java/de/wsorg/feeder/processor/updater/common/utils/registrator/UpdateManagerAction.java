package de.wsorg.feeder.processor.updater.common.utils.registrator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum UpdateManagerAction {
    REGISTER_FEED_FOR_UPDATE,
    CANCEL_UPDATE_OF_FEED,
    GET_SUBSCRIBED_FEEDS
}
