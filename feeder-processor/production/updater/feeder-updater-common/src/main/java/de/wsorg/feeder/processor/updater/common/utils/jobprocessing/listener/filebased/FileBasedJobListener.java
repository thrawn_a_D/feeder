package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.filebased;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.JobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener.JobListener;
import de.wsorg.feeder.processor.updater.common.utils.registrator.JobMessage;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class FileBasedJobListener implements JobListener<JobExecutor> {
    private List<JobExecutor> jobExecutors = new ArrayList<>();

    @Inject
    private FileBasedPersistence<JobMessage> fileBasedPersistence;
    private String folderPath;
    private boolean listenerRunning = true;

    @Override
    public void startListener() {
        listenerRunning = true;

        log.debug("Starting file based message listener. Listening on folder:{}", folderPath);

        Thread listenerThread = new Thread(new Runnable() {
            public void run() {
            while (listenerRunning) {
                try {
                    List<JobMessage> folderContent = fileBasedPersistence.getFolderContent(folderPath);

                    for (JobMessage jobMessage : folderContent) {
                        log.debug("Start processing file based messages on queue {}", folderPath);

                        for (JobExecutor jobExecutor : jobExecutors) {
                            fileBasedPersistence.remove(folderPath, jobMessage.getIdentifier());
                            jobExecutor.execute(jobMessage);
                        }
                    }

                    Thread.sleep(500);
                } catch (InterruptedException ignored) {
                }
            }
            }
        });

        listenerThread.start();
    }

    @Override
    public void stopListener() {
        log.debug("Stopping file based message listener. Listening on folder:{}", folderPath);
        listenerRunning = false;
    }

    @Override
    public void setJobExecutors(final List<JobExecutor> jobExecutors) {
        this.jobExecutors = jobExecutors;
    }

    public void setSourceFolderToListen(final String folderPath) {
        this.folderPath = folderPath;
    }
}
