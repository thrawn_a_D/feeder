package de.wsorg.feeder.processor.updater.common.utils.jobprocessing.listener;


import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.JobExecutor;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface JobListener<T extends JobExecutor> {
    void startListener();
    void stopListener();
    void setJobExecutors(final List<T> jobExecutors);
}
