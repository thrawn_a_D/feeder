package de.wsorg.feeder.processor.updater.common.utils.registrator.activemq;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UpdateManagerMessageCreator implements MessageCreator {
    private final String feedUrl;
    private final UpdateManagerAction action;

    public UpdateManagerMessageCreator(final String feedUrl, final UpdateManagerAction action) {
        this.feedUrl = feedUrl;
        this.action = action;
    }
    public UpdateManagerMessageCreator(final List<String> feedUrl, final UpdateManagerAction action) {
        this.feedUrl = feedUrl.toString();
        this.action = action;
    }

    @Override
    public Message createMessage(final Session session) throws JMSException {
        MapMessage mapMessage = session.createMapMessage();
        mapMessage.setString(QueueMessageFieldNames.UPDATE_MANAGER_ACTION.getColumnName(), action.name());
        mapMessage.setString(QueueMessageFieldNames.FEED_URL.getColumnName(), feedUrl);

        return mapMessage;
    }
}
