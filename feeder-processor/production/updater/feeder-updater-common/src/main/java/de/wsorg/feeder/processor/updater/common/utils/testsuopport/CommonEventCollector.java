package de.wsorg.feeder.processor.updater.common.utils.testsuopport;

import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
@Slf4j
public class CommonEventCollector implements EventCollector {
    private EventListener eventListener;
    private Set<String> interestedInEvents;
    private Set<String> occurredEventsSoFar = new HashSet<>();

    @Override
    public void eventOccurred(final String eventTitle) {
        if (interestedInEvents != null && interestedInEvents.contains(eventTitle)) {
            occurredEventsSoFar.add(eventTitle);

            CommonEventCollector.log.debug("Wanted event has occurred: {}", eventTitle);
            if(occurredEventsSoFar.size() == interestedInEvents.size()) {
                eventListener.interestedEventsOccurred();
            }
        }
    }

    @Override
    public void exceptionEventOccurred(final Exception exception) {
        if (eventListener != null) {
            eventListener.exceptionOccurred(exception);
        }
    }

    @Override
    public void setEventListener(final EventListener eventListener, final Set<String> interestedInEvents) {
        this.eventListener = eventListener;
        this.interestedInEvents = interestedInEvents;
        log.debug("Registering event listener. Wanted events are: {}", interestedInEvents);
    }
}
