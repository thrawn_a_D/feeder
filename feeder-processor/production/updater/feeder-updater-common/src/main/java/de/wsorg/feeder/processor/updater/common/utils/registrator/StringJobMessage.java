package de.wsorg.feeder.processor.updater.common.utils.registrator;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
@Data
public class StringJobMessage extends JobMessage {
    private String content;

    public StringJobMessage(final String content) {
        this.content = content;
    }

    public StringJobMessage(final Message jmsMessage) {
        init(jmsMessage);
    }

    @Override
    public String getIdentifier() {
        return content;
    }

    @Override
    public void init(final Message jmsMessage) {
        try {
            TextMessage textMessage = (TextMessage) jmsMessage;
            content = textMessage.getText();
        } catch (JMSException e) {
            log.error("A problem occurred while transforming a jms text message:", e);
        }
    }
}
