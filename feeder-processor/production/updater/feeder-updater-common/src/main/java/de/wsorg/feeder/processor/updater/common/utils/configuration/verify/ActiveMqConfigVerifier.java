package de.wsorg.feeder.processor.updater.common.utils.configuration.verify;

import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ActiveMqConfigVerifier implements ConfigVerifier {
    public static final String REPOSITORY_ACTIVE_MQ_URL = "repository.activeMQ.url";
    public static final String REPOSITORY_ACTIVE_MQ_PORT = "repository.activeMQ.port";

    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult verificationResult = new ConfigVerificationResult(configurationFilePath);


        verifyPropertyNotEmpty(verificationResult, configProperties, REPOSITORY_ACTIVE_MQ_URL);
        verifyPropertyNotEmpty(verificationResult, configProperties, REPOSITORY_ACTIVE_MQ_PORT);
        verifyPortNumber(configProperties, verificationResult, REPOSITORY_ACTIVE_MQ_PORT);

        return verificationResult;
    }

    private void verifyPortNumber(final Properties configProperties, final ConfigVerificationResult verificationResult, final String mqPortPropertyName) {
        if(!verificationResult.getPropertyErrors().containsKey(mqPortPropertyName)) {
            String mqPort = configProperties.getProperty(mqPortPropertyName);

            try{
                Integer.parseInt(mqPort);
            } catch (NumberFormatException ex) {
                verificationResult.addVerificationError(mqPortPropertyName, "Mq port has to be a number");
            }
        }
    }

    private void verifyPropertyNotEmpty(final ConfigVerificationResult verificationResult, final Properties configProperties, final String propertyName) {
        final String propertyValue = configProperties.getProperty(propertyName);
        if(StringUtils.isBlank(propertyValue)) {
            verificationResult.addVerificationError(propertyName, "The property is not set.");
        }
    }
}
