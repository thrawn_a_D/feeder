package de.wsorg.feeder.processor.updater.common.utils.registrator.activemq;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum QueueMessageFieldNames {
    UPDATE_MANAGER_ACTION("update-manager-action"),
    FEED_URL("feedUrl");

    private String columnName;

    private QueueMessageFieldNames(final String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
