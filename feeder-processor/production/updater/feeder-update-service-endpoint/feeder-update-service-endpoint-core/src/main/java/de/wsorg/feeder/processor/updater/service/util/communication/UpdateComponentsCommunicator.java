package de.wsorg.feeder.processor.updater.service.util.communication;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public interface UpdateComponentsCommunicator {
    void sendSubscribedFeedListToComponents(final List<String> subscribedFeedUrls);
    void sendFeedUpdateToUpdaterModules(final String feedUpdateAsJson);
}
