package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.impl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.NotificationCallback;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.xml.sax.InputSource;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscriber;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscription;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.SubscriptionHandler;

/**
 * Basic {@link SubscriptionHandler} implementation. Incoming requests will be
 * checked and forwarded to the appropriate processing methods.
 * 
 * @author Benjamin Erb
 * 
 */
public class SubscriptionHandlerImpl extends AbstractHandler implements SubscriptionHandler
{
	private final Subscriber subscriber;
    private final NotificationCallback defaultNotificationCallback;

    /**
	 * Creates a new {@link SubscriptionHandler}.
     * @param subscriber
     * @param defaultNotificationCallback
     */
	public SubscriptionHandlerImpl(Subscriber subscriber, final NotificationCallback defaultNotificationCallback)
	{
		this.subscriber = subscriber;
        this.defaultNotificationCallback = defaultNotificationCallback;
    }

	@Override
	public void handleNotify(HttpServletRequest request, HttpServletResponse response, final Subscription subscription) throws IOException, ServletException
	{
		try
		{
			InputSource source = new InputSource(request.getInputStream());
			SyndFeedInput feedInput = new SyndFeedInput();
			feedInput.setPreserveWireFeed(true);
			final SyndFeed feed;
			feed = feedInput.build(source);
            feed.setUri(subscription.getFeedTopicUri().toString());
			subscriber.executeCallback(new Runnable()
			{
				@Override
				public void run()
				{
					if(subscription.getNotificationCallback() != null)
					{
						subscription.getNotificationCallback().handle(feed);
					} else {
                        defaultNotificationCallback.handle(feed);
                    }
				}
			});
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			response.setStatus(200);
		}

	}

	@Override
	public void handleVerify(HttpServletRequest request, HttpServletResponse response, Subscription subscription)
	{
		if(request.getParameter("hub.mode") != null && request.getParameter("hub.topic") != null && request.getParameter("hub.challenge") != null && request.getParameter("hub.verify_token") != null)
		{
			URI feedTopicUri = URI.create(request.getParameter("hub.topic"));
			if(request.getParameter("hub.mode").equals("subscribe"))
			{
				if(subscriber.verifySubscribeIntent(feedTopicUri, request.getParameter("hub.verify_token")))
				{
					response.setStatus(200);
					response.setContentType("text/plain");
					try
					{
						response.getWriter().write(request.getParameter("hub.challenge"));
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					response.setStatus(404);
				}
			}
			else if(request.getParameter("hub.mode").equals("unsubscribe"))
			{
				if(subscriber.verifyUnsubscribeIntent(feedTopicUri, request.getParameter("hub.verify_token")))
				{
					response.setStatus(200);
					response.setContentType("text/plain");
					try
					{
						response.getWriter().write(request.getParameter("hub.challenge"));
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					response.setStatus(404);
				}
			}

		}
		else
		{
			response.setStatus(400);
		}

	}

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		Subscription subscription = subscriber.getSubscriptionById(target.substring(1));

        if(null == subscription) {
            try {
                subscription = new SubscriptionImpl(target.substring(1), new URI(request.getRequestURI()), subscriber);
            } catch (URISyntaxException e) {
                throw new RuntimeException(e);
            }
        }

        if(request.getMethod().equals("GET"))
        {
            handleVerify(request, response, subscription);
        }
        else if(request.getMethod().equals("POST"))
        {
            handleNotify(request, response, subscription);
        }
        else
        {
            response.setStatus(405);
        }
        baseRequest.setHandled(true);
	}

}
