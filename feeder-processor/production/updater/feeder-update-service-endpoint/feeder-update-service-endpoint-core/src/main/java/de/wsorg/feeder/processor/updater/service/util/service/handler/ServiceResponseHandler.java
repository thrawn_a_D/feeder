package de.wsorg.feeder.processor.updater.service.util.service.handler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ServiceResponseHandler {
    void onSuccess(java.lang.Object o);
    void onError(java.lang.String s);
}
