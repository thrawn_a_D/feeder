package de.wsorg.feeder.processor.updater.service.util.service;

import java.net.URL;
import java.util.List;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedsUpdateService {
    void subscribe(List<URL> urls, ServiceResponseHandler onResponseHandler);
    void unsubscribe(List<URL> urls, ServiceResponseHandler onResponseHandler);
    List<URL> getSubscriptionList(ServiceResponseHandler onResponseHandler);
    void setMessageHandler(ServiceNotificationHandler messageHandler);
}
