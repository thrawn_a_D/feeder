package de.wsorg.feeder.processor.updater.service.util.communication.filebased;

import javax.inject.Inject;
import java.util.List;

import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage;
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedCommunicator implements UpdateComponentsCommunicator {

    public static final String GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER = "updaterService_getAllSubscriptionsResponseQueue";
    public static final String FEED_UPDATE_RESPONSE_FOLDER = "updaterService_feedUpdateQueue";

    @Inject
    private FileBasedPersistence fileBasedPersistence;

    @Override
    public void sendSubscribedFeedListToComponents(final List<String> subscribedFeedUrls) {
        final String feedUrls = subscribedFeedUrls.toString();
        final StringJobMessage stringJobMessage = new StringJobMessage(feedUrls);
        fileBasedPersistence.save(GET_SUBSCRIBED_FEEDS_RESPONSE_FOLDER, feedUrls, stringJobMessage);
    }

    @Override
    public void sendFeedUpdateToUpdaterModules(final String feedUpdateAsJson) {
        final StringJobMessage stringJobMessage = new StringJobMessage(feedUpdateAsJson);
        fileBasedPersistence.save(FEED_UPDATE_RESPONSE_FOLDER, feedUpdateAsJson, stringJobMessage);
    }
}
