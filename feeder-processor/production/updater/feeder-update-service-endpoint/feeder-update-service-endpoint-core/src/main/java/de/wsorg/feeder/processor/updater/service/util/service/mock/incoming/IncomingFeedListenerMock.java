package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface IncomingFeedListenerMock {
    public void startListener();
    public void setNotificationHandler(final ServiceNotificationHandler handler);
}
