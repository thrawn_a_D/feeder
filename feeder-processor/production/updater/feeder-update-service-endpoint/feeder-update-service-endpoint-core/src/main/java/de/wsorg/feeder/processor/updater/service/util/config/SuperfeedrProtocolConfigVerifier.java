package de.wsorg.feeder.processor.updater.service.util.config;

import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateServiceFactory;
import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SuperfeedrProtocolConfigVerifier implements ConfigVerifier {

    public static final String SUPERFEEDR_PROTOCOL = "superfeedr.protocol";
    public static final String SUPERFEEDR_SUBSCRIBER_HOST = "superfeedr.subscriber.host";
    public static final String SUPERFEEDR_SUBSCRIBER_PORT = "superfeedr.subscriber.port";
    public static final String SUPERFEEDR_HUB_HOST = "superfeedr.hub.host";

    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult configVerificationResult = new ConfigVerificationResult(configurationFilePath);

        verifySuperfeedrProtocol(configProperties, configVerificationResult);

        return configVerificationResult;
    }

    private void verifySuperfeedrProtocol(final Properties configProperties, final ConfigVerificationResult configVerificationResult) {
        boolean protocolValueSet = verifyProperty(configVerificationResult,
                configProperties,
                FeedsUpdateServiceFactory.SUPERFEEDR_PROTOCOL,
                "Protocol for superfeedr is not set.");

        if(protocolValueSet) {
            final String protocol = configProperties.getProperty(FeedsUpdateServiceFactory.SUPERFEEDR_PROTOCOL);
            verifyProtocolIsInRange(configVerificationResult, protocol);
            if("pubsub".equals(protocol)) {
                verifyReceiverHost(configProperties, configVerificationResult);
                verifyReceiverPort(configProperties, configVerificationResult);
                verifyHubHost(configProperties, configVerificationResult);
            }
        }
    }

    private void verifyHubHost(final Properties configProperties, final ConfigVerificationResult configVerificationResult) {
        final String hubHost = configProperties.getProperty(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST);
        final String errorMessage = "When using pubsub as protocol you need to provide a valid hub uri.";
        if(StringUtils.isBlank(hubHost)) {
            configVerificationResult.addVerificationError(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, errorMessage);
        } else {
            try {
                new URI(hubHost);
            } catch (URISyntaxException e) {
                configVerificationResult.addVerificationError(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, errorMessage);
            }
        }
    }

    private void verifyReceiverPort(final Properties configProperties, final ConfigVerificationResult configVerificationResult) {
        final String receiverPort = configProperties.getProperty(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT);
        if(StringUtils.isBlank(receiverPort)) {
            final String errorMessage = "When using pubsub as protocol you need to provide port of receiver.";
            configVerificationResult.addVerificationError(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT, errorMessage);
        }
    }

    private void verifyReceiverHost(final Properties configProperties, final ConfigVerificationResult configVerificationResult) {
        final String receiverHost = configProperties.getProperty(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST);
        if(StringUtils.isBlank(receiverHost)) {
            final String errorMessage = "When using pubsub as protocol you need to provide hostname of receiver.";
            configVerificationResult.addVerificationError(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST, errorMessage);
        }
    }

    private void verifyProtocolIsInRange(final ConfigVerificationResult configVerificationResult, final String protocol) {
        if(!"xmpp".equals(protocol) && !"pubsub".equals(protocol) && !"mocked".equals(protocol)) {
            addInvalidProtocolError(configVerificationResult);
        }
    }

    private void addInvalidProtocolError(final ConfigVerificationResult configVerificationResult) {
        final String errorMessage = "Protocol for superfeedr is not set to a valid value. Possible range is: {xmpp|pubsub|mocked}";
        configVerificationResult.addVerificationError(FeedsUpdateServiceFactory.SUPERFEEDR_PROTOCOL, errorMessage);
    }

    private boolean verifyProperty(final ConfigVerificationResult configVerificationResult, final Properties configProperties, final String propertyName, final String errorText) {
        boolean valid=false;
        final String propertyValue = configProperties.getProperty(propertyName);
        if(StringUtils.isBlank(propertyValue)) {
            configVerificationResult.addVerificationError(propertyName, errorText);
        } else {
            valid = true;
        }

        return valid;
    }
}
