package de.wsorg.feeder.processor.updater.service.util.communication.activemq;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.TextMessageCreator;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator;
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ActiveMqCommunicator implements UpdateComponentsCommunicator {

    @Inject
    private ApplicationContext applicationContext;
    @Inject
    @Named("destinationForGetSubscribedFeedsResponse")
    private ActiveMQQueue getSubscribedFeedsResponseQueue;
    @Inject
    @Named("destinationForFeedUpdateResponse")
    private ActiveMQQueue feedUpdateResponseQueue;

    @Override
    public void sendSubscribedFeedListToComponents(final List<String> subscribedFeedUrls) {
        putMessageIntoQueue(subscribedFeedUrls, UpdateManagerAction.GET_SUBSCRIBED_FEEDS, getSubscribedFeedsResponseQueue);
    }

    @Override
    public void sendFeedUpdateToUpdaterModules(final String feedUpdateAsJson) {
        TextMessageCreator messageCreator = new TextMessageCreator(feedUpdateAsJson);
        final JmsTemplate jmsTemplate = getJmsTemplate();
        jmsTemplate.send(feedUpdateResponseQueue, messageCreator);
    }

    private void putMessageIntoQueue(final List<String> feedUrls,
                                     final UpdateManagerAction action,
                                     final ActiveMQQueue queue) {
        UpdateManagerMessageCreator messageCreator = new UpdateManagerMessageCreator(feedUrls, action);
        final JmsTemplate jmsTemplate = getJmsTemplate();
        jmsTemplate.send(queue, messageCreator);
    }

    private JmsTemplate getJmsTemplate(){
        return applicationContext.getBean(JmsTemplate.class);
    }
}
