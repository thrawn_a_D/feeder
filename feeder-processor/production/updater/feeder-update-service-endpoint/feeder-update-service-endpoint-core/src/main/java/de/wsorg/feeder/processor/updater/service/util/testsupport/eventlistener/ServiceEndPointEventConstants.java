package de.wsorg.feeder.processor.updater.service.util.testsupport.eventlistener;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public class ServiceEndPointEventConstants {
    public static final String SUBSCRIBED_FEED_LIST_PUBLISHED_TO_COMPONENTS = "SUBSCRIBED_FEED_LIST_PUBLISHED_TO_COMPONENTS";
    public static final String SEND_UPDATED_FEED_TO_UPDATER_MODULES = "SEND_UPDATED_FEED_TO_UPDATER_MODULES";
    public static final String SUBSCRIBE_FEED_TO_SUPERFEEDR = "SUBSCRIBE_FEED_TO_SUPERFEEDR";
    public static final String UNSUBSCRIBE_FEED_FROM_SUPERFEEDR = "UNSUBSCRIBE_FEED_FROM_SUPERFEEDR";
}
