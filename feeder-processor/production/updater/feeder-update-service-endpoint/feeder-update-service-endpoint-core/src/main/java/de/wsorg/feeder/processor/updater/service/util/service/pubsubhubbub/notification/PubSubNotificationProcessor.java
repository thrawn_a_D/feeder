package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification;

import com.google.gson.Gson;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndLink;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator;
import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.util.Date;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class PubSubNotificationProcessor implements SuperfeedrNotificationProcessor<SyndFeed> {
    @Inject
    private UpdateComponentsCommunicator updateComponentsCommunicator;

    @Override
    public void processNotification(final SyndFeed notification) {
        FeedModel feedModel = new FeedModel();

        feedModel.setFeedUrl(notification.getUri());
        feedModel.setTitle(notification.getTitle());
        feedModel.setUpdated(new Date());
        feedModel.setDescription(notification.getDescription());

        for (Object syndEntry: notification.getEntries()) {
            SyndEntry entry = (SyndEntry) syndEntry;

            FeedEntryModel newEntryModel = new FeedEntryModel();
            newEntryModel.setTitle(entry.getTitle());

            if(entry.getDescription() != null)
                newEntryModel.setDescription(entry.getDescription().getValue());

            newEntryModel.setUpdated(entry.getUpdatedDate());
            newEntryModel.setFeedUrl(notification.getUri());
            newEntryModel.setId(((SyndEntry) syndEntry).getUri());

            addEntryLinks(entry, newEntryModel);

            feedModel.addFeedEntry(newEntryModel);
        }

        sendFeedModel(feedModel);
    }

    private void addEntryLinks(final SyndEntry entry, final FeedEntryModel newEntryModel) {
        for (Object link : entry.getLinks()) {
            SyndLink syndLink  = (SyndLink) link;
            Link newLink = new Link();
            newLink.setHref(syndLink.getHref());
            newLink.setRelation(LinkRelation.valueOfCode(syndLink.getRel()));
            newLink.setLinkType(new LinkType(syndLink.getType()));
            newEntryModel.addLink(newLink);
        }
    }

    private void sendFeedModel(final FeedModel feedModel) {
        Gson gson = new Gson();
        String feedAsJson = gson.toJson(feedModel, FeedModel.class);
        updateComponentsCommunicator.sendFeedUpdateToUpdaterModules(feedAsJson);
    }
}
