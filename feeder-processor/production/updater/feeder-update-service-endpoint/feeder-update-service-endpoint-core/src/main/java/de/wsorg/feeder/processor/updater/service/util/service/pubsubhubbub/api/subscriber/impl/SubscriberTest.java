package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.impl;

import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.NotificationCallback;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscription;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SubscriberTest {
    public static void main(String[] args) throws URISyntaxException {
        SubscriberImpl subscriber = new SubscriberImpl("wsds.i234.me",
                                                          8885,
                                                          new URI("http://superfeedr.com/hubbub"),
                                                          "Thrawn_a_d",
                                                          "x8uWZx4kam", new NotificationCallback() {
            @Override
            public void handle(final SyndFeed feed) {
                System.out.println(feed.getTitle());
            }
        });

//        Subscription subscription = subscriber.subscribe(new URI("http://www.spiegel.de/schlagzeilen/index.rss"));
//        Subscription subscription = subscriber.subscribe(new URI("http://superfeedr.com/dummy.xml"));
//        subscription.setNotificationCallback(new NotificationCallback() {
//            @Override
//            public void handle(final SyndFeed feed) {
//                System.err.println(feed.getTitle());
//            }
//        });

//        while(true);

        Subscription subscription = subscriber.calculateSubscriptionOfUri(new URI("http://www.spiegel.de/schlagzeilen/index.rss"));
        subscriber.unsubscribe(subscription);
    }
}
