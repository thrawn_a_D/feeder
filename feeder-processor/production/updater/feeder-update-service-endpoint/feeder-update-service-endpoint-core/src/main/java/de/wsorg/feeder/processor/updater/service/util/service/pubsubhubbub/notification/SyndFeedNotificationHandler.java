package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification;

import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SyndFeedNotificationHandler implements ServiceNotificationHandler<SyndFeed> {
    @Inject
    @Named("pubSubNotificationProcessor")
    private SuperfeedrNotificationProcessor<SyndFeed> superfeedrNotificationProcessor;
    @Override
    public void onNotification(final SyndFeed superfeedrEventExtension) {
        superfeedrNotificationProcessor.processNotification(superfeedrEventExtension);
    }
}
