package de.wsorg.feeder.processor.updater.service.util.testsupport;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeederUpdateServiceContextFactory {

    private static ClassPathXmlApplicationContext applicationContext;

    public static void initContext(){
        final String[] configLocations = {"classpath:spring/update-service-base-context.xml"};
        applicationContext = new ClassPathXmlApplicationContext(configLocations);
    }
}
