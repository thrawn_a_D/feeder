package de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper;

import org.superfeedr.OnResponseHandler;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;


public class XmppResposeHandlingWrapper implements OnResponseHandler {
    private final ServiceResponseHandler responseHandler;

    public XmppResposeHandlingWrapper(final ServiceResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
    }

    @Override
    public void onSuccess(final Object o) {
        responseHandler.onSuccess(o);
    }

    @Override
    public void onError(final String s) {
        responseHandler.onError(s);
    }
}
