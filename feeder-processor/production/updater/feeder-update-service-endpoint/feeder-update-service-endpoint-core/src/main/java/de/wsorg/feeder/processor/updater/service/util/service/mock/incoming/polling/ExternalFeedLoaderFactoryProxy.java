package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.polling;

import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoaderFactory;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ExternalFeedLoaderFactoryProxy {
    public ExternalFeedLoader getExternalFeedLoader() {
        return ExternalFeedLoaderFactory.getFeedLoader();
    }
}
