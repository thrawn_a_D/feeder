package de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper;

import org.superfeedr.OnNotificationHandler;
import org.superfeedr.extension.notification.SuperfeedrEventExtension;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class XmppNotificationHandlerWrapper implements OnNotificationHandler {
    private final ServiceNotificationHandler messageHandler;

    public XmppNotificationHandlerWrapper(final ServiceNotificationHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public void onNotification(final SuperfeedrEventExtension superfeedrEventExtension) {
        messageHandler.onNotification(superfeedrEventExtension);
    }
}
