package de.wsorg.feeder.processor.updater.service.executor.superfeedr;

import javax.inject.Inject;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.StringMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator;
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class GetSubscribedFeedsExecutor implements StringMessageJobExecutor {
    @Inject
    private FeedsUpdateService feedsUpdateService;
    @Inject
    private ServiceResponseHandler defaultResponseHandler;
    @Inject
    private UpdateComponentsCommunicator updateComponentsCommunicator;

    @Override
    public void execute(final StringJobMessage message) {
        log.debug("Getting subscribed feed on superfeedr using xmpp as protocol.");

        List<URL> subscribedFeeds = feedsUpdateService.getSubscriptionList(defaultResponseHandler);
        log.debug("Got {} subscribed feeds on superfeedr.", subscribedFeeds.size());

        if (subscribedFeeds.size() > 0) {
            List<String> subscribedFeedsAsString = new ArrayList<>();
            for (URL subscribedFeed : subscribedFeeds) {
                subscribedFeedsAsString.add(subscribedFeed.toExternalForm());
            }

            log.debug("Sending notification message with a list of subscribed feeds.");
            updateComponentsCommunicator.sendSubscribedFeedListToComponents(subscribedFeedsAsString);
        } else {
            log.debug("Currently there are no subscriptions on superfeedr.");
        }
    }
}
