package de.wsorg.feeder.processor.updater.service.util.service.xmpp;

import javax.inject.Inject;
import java.net.URL;
import java.util.List;

import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppNotificationHandlerWrapper;
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppResposeHandlingWrapper;
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppSuperfeedrWrapper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class SuperfeedrXmppBased implements FeedsUpdateService {

    @Inject
    private XmppSuperfeedrWrapper xmppSuperfeedrWrapper;

    @Override
    public void subscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        xmppSuperfeedrWrapper.subscribe(urls, getXmppResponseHandler(onResponseHandler));
    }

    @Override
    public void unsubscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        xmppSuperfeedrWrapper.unsubscribe(urls, getXmppResponseHandler(onResponseHandler));
    }

    @Override
    public List<URL> getSubscriptionList(final ServiceResponseHandler onResponseHandler) {
        return xmppSuperfeedrWrapper.getSubscriptionList(getXmppResponseHandler(onResponseHandler));
    }

    @Override
    public void setMessageHandler(final ServiceNotificationHandler messageHandler) {
        xmppSuperfeedrWrapper.setMessageHandler(new XmppNotificationHandlerWrapper(messageHandler));
    }

    private XmppResposeHandlingWrapper getXmppResponseHandler(final ServiceResponseHandler onResponseHandler) {
        return new XmppResposeHandlingWrapper(onResponseHandler);
    }
}
