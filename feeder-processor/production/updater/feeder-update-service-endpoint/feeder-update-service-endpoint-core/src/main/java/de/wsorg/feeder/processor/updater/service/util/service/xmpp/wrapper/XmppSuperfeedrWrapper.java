package de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper;

import java.net.URL;
import java.util.List;

import org.jivesoftware.smack.XMPPException;
import org.springframework.beans.factory.annotation.Value;
import org.superfeedr.OnNotificationHandler;
import org.superfeedr.OnResponseHandler;
import org.superfeedr.Superfeedr;

import de.wsorg.feeder.processor.updater.service.util.config.SuperfeedrConfigVerifier;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class XmppSuperfeedrWrapper {

    private Superfeedr superfeedr;

    @Value("${" + SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME + "}")
    private String superfeedrUserName;
    @Value("${" + SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD + "}")
    private String superfeedrPassword;
    @Value("${" + SuperfeedrConfigVerifier.SUPERFEEDR_HOST + "}")
    private String superfeedrHost;

    public void subscribe(List<URL> urls, OnResponseHandler onResponseHandler) {
        getSuperfeedr().subscribe(urls, onResponseHandler);
    }

    public void unsubscribe(final List<URL> urls, final OnResponseHandler onResponseHandler) {
        getSuperfeedr().unsubscribe(urls, onResponseHandler);
    }

    public List<URL> getSubscriptionList(final OnResponseHandler onResponseHandler) {
        return getSuperfeedr().getSubscriptionList(onResponseHandler);
    }

    public void setMessageHandler(final OnNotificationHandler messageHandler) {
        getSuperfeedr().addOnNotificationHandler(messageHandler);
    }

    private Superfeedr getSuperfeedr(){
        if(superfeedr == null) {
            try {
                superfeedr = new Superfeedr(superfeedrUserName, superfeedrPassword, superfeedrHost);
            } catch (XMPPException e) {
                XmppSuperfeedrWrapper.log.error("Error instantiating superfeedr: {}", e);
                throw new RuntimeException(e);
            }
        }

        return superfeedr;
    }
}
