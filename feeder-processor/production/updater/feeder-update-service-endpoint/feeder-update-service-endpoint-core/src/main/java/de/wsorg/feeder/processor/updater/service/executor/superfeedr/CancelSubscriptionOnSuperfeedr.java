package de.wsorg.feeder.processor.updater.service.executor.superfeedr;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class CancelSubscriptionOnSuperfeedr implements UpdaterMessageJobExecutor {
    @Inject
    private FeedsUpdateService feedsUpdateService;
    @Inject
    private ServiceResponseHandler responseHandler;

    @Override
    public void execute(final UpdaterJobMessage message) {
        CancelSubscriptionOnSuperfeedr.log.debug("Cancel update subscription on superfeedr: {}", message.getIdentifier());
        List<URL> feedUrlToCancel = getUrlListToUnsubscribe(message);
        feedsUpdateService.unsubscribe(feedUrlToCancel, responseHandler);
    }

    private List<URL> getUrlListToUnsubscribe(final UpdaterJobMessage message) {
        List<URL> feedUrlToCancel = new ArrayList<>();
        try {
            feedUrlToCancel.add(new URL(message.getIdentifier()));
            return feedUrlToCancel;
        } catch (MalformedURLException e) {
            log.error("Error while canceling feed with url ({}). Invalid url format.", message.getIdentifier());
            return null;
        }
    }
}
