package de.wsorg.feeder.processor.updater.service.util.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class FeedsUpdateServiceFactory {
    public static final String SUPERFEEDR_PROTOCOL = "superfeedr.protocol";

    @Value("${" + SUPERFEEDR_PROTOCOL + "}")
    private String serviceProtocol;
    private FeedsUpdateService superfeedrXmppBased;
    private FeedsUpdateService superfeedrPubSubBased;
    private FeedsUpdateService superfeedrMock;

    private FeedsUpdateService usedServiceClass;

    public FeedsUpdateService getFeedsUpdateService() {

        if (usedServiceClass == null) {
            usedServiceClass = getAppropriateServiceBasedOnConfig();
        }

        return usedServiceClass;
    }

    private FeedsUpdateService getAppropriateServiceBasedOnConfig() {
        if (StringUtils.equalsIgnoreCase("true", serviceProtocol)) {
            FeedsUpdateServiceFactory.log.debug("Superfeedr service is mocked by configuration!");
            return superfeedrMock;
        } else if (StringUtils.equalsIgnoreCase("xmpp", serviceProtocol)) {
            FeedsUpdateServiceFactory.log.debug("Xmpp protocol is used!");
            return superfeedrXmppBased;
        } else if (StringUtils.equalsIgnoreCase("pubsub", serviceProtocol)) {
            FeedsUpdateServiceFactory.log.debug("Pubsubhubpub protocol is used!");
            return superfeedrPubSubBased;
        } else if (StringUtils.isEmpty(serviceProtocol)) {
            FeedsUpdateServiceFactory.log.debug("No mocking of Superfeedr is desired!");
            return superfeedrXmppBased;
        } else  {
            final String message = "Some invalid value provided in " + SUPERFEEDR_PROTOCOL + " property. " +
                    "This leads to the superfeedr been mocked right now. " +
                    "Please provide an adequate boolean value.";
            FeedsUpdateServiceFactory.log.debug(message);
            return superfeedrMock;
        }
    }

    public void setSuperfeedrXmppBased(final FeedsUpdateService superfeedrXmppBased) {
        this.superfeedrXmppBased = superfeedrXmppBased;
    }

    public void setSuperfeedrPubSubBased(final FeedsUpdateService superfeedrPubSubBased) {
        this.superfeedrPubSubBased = superfeedrPubSubBased;
    }

    public void setSuperfeedrMock(final FeedsUpdateService superfeedrMock) {
        this.superfeedrMock = superfeedrMock;
    }
}
