package de.wsorg.feeder.processor.updater.service.util.service.handler.notification;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import org.springframework.beans.factory.annotation.Value;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class NotificationServiceTerminator {
    private static final String PROTOCOL_XMPP = "xmpp";
    private static final String PROTOCOL_PUBSUB = "pubsub";

    public static final String SUPERFEEDR_PROTOCOL = "superfeedr.protocol";

    @Value("${" + SUPERFEEDR_PROTOCOL + "}")
    private String serviceProtocol;

    private ServiceNotificationHandler xmppNotificationHandler;
    private ServiceNotificationHandler syndFeedNotificationHandler;

    public ServiceNotificationHandler determineNotificationHandler() {
        if (PROTOCOL_XMPP.equals(serviceProtocol)) {
            return xmppNotificationHandler;
        } else if (PROTOCOL_PUBSUB.equals(serviceProtocol)) {
            return syndFeedNotificationHandler;
        } else {
            return xmppNotificationHandler;
        }
    }

    public void setSyndFeedNotificationHandler(final ServiceNotificationHandler syndFeedNotificationHandler) {
        this.syndFeedNotificationHandler = syndFeedNotificationHandler;
    }

    public void setXmppNotificationHandler(final ServiceNotificationHandler xmppNotificationHandler) {
        this.xmppNotificationHandler = xmppNotificationHandler;
    }
}
