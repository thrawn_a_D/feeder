package de.wsorg.feeder.processor.updater.service.executor.superfeedr;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
@Named
@Slf4j
public class SubscribeToFeedBySuperfeedr implements UpdaterMessageJobExecutor {
    @Inject
    private FeedsUpdateService feedsUpdateService;
    @Inject
    private ServiceResponseHandler defaultResponseHandler;

    @Override
    public void execute(final UpdaterJobMessage message) {
        final List<URL> url = getUrlsToSubscribeTo(message);

        log.debug("Subscribing feed ({}) on superfeedr.", message.getIdentifier());
        feedsUpdateService.subscribe(url, defaultResponseHandler);
    }

    private List<URL> getUrlsToSubscribeTo(final UpdaterJobMessage message) {
        List<URL> url = new ArrayList<URL>();
        try {
            final URL feedUrl = new URL(message.getIdentifier());

            url.add(feedUrl);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("The provided feed url is not a valid URL");
        }
        return url;
    }
}
