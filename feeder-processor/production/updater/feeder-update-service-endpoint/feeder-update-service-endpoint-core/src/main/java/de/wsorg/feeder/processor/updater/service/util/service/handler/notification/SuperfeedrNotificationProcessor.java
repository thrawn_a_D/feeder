package de.wsorg.feeder.processor.updater.service.util.service.handler.notification;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface SuperfeedrNotificationProcessor<T> {
    void processNotification(final T notification);
}
