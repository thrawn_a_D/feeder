package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub;

import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.updater.service.util.config.SuperfeedrConfigVerifier;
import de.wsorg.feeder.processor.updater.service.util.config.SuperfeedrProtocolConfigVerifier;
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.NotificationCallback;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscriber;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscription;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.impl.SubscriberImpl;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification.wrapper.PubSubNotificationWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class SuperfeedrPubSubBased implements FeedsUpdateService {
    private Subscriber subscriber;

    @Value("${" + SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST + "}")
    private String subscriberHost;
    @Value("${" + SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT + "}")
    private String subscriberPort;
    @Value("${" + SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST + "}")
    private String masterHubHost;
    @Value("${" + SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME + "}")
    private String superfeedrUserName;
    @Value("${" + SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD + "}")
    private String superfeedrPassword;

    @Inject
    private ServiceNotificationHandler<SyndFeed> notificationHandler;

    private NotificationCallback notificationCallback;

    @Override
    public void subscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        try {
            for (URL url : urls) {
                try {
                    Subscription subscription = this.getSubscriber().subscribe(url.toURI());
                    subscription.setNotificationCallback(getNotificationCallback());
                    onResponseHandler.onSuccess(subscription);
                } catch (URISyntaxException e) {
                    log.error("Provided url to subscribe to is not valid: {}", url.toString());
                }
            }
        } catch (Exception e) {
            onResponseHandler.onError(e.getMessage());
        }
    }

    @Override
    public void unsubscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        try {
            for (URL url : urls) {
                try {
                    Subscription subscription = subscriber.calculateSubscriptionOfUri(url.toURI());
                    subscriber.unsubscribe(subscription);
                    onResponseHandler.onSuccess(subscription);
                } catch (URISyntaxException e) {
                    log.error("Provided url to unsubscribe to is not valid: {}", url.toString());
                }
            }
        } catch (Exception e) {
            onResponseHandler.onError(e.getMessage());
        }
    }

    @Override
    public List<URL> getSubscriptionList(final ServiceResponseHandler onResponseHandler) {
        List<URL> result = new ArrayList<>();
        List<Subscription> subscriptions = subscriber.getSubscriptions();
        for (Subscription subscription : subscriptions) {
            try {
                result.add(subscription.getFeedTopicUri().toURL());
            } catch (MalformedURLException e) {
                log.error("One of subscribed feed urls seems to be invalid???: {}", subscription.getFeedTopicUri().toString());
            }
        }
        return result;
    }

    @Override
    public void setMessageHandler(final ServiceNotificationHandler messageHandler) {
        this.notificationHandler = messageHandler;
    }

    public Subscriber getSubscriber() {
        if(subscriber == null) {
            try {
                subscriber = new SubscriberImpl(subscriberHost,
                                                Integer.valueOf(subscriberPort),
                                                new URI(masterHubHost),
                                                superfeedrUserName,
                                                superfeedrPassword,
                                                getNotificationCallback());
            } catch (URISyntaxException e) {
                log.error("Error creating instance of pubsub api client. Might be an invalid hub uri: {}", masterHubHost);
            }
        }

        return subscriber;
    }

    public void stopSubscriberService() {
        subscriber.stopServer();
    }

    private NotificationCallback getNotificationCallback() {
        if(notificationCallback == null) {
            notificationCallback = new PubSubNotificationWrapper(notificationHandler);
        }

        return notificationCallback;
    }
}
