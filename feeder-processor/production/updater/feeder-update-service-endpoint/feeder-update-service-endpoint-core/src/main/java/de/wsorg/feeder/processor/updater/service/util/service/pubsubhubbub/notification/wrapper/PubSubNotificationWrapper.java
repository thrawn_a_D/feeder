package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification.wrapper;

import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.NotificationCallback;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class PubSubNotificationWrapper implements NotificationCallback {

    private final ServiceNotificationHandler<SyndFeed> notificationHandler;

    public PubSubNotificationWrapper(final ServiceNotificationHandler<SyndFeed> notificationHandler) {
        this.notificationHandler = notificationHandler;
    }

    @Override
    public void handle(final SyndFeed feed) {
        notificationHandler.onNotification(feed);
    }
}
