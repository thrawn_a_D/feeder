package de.wsorg.feeder.processor.updater.service.util.service.mock;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler;
import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.IncomingFeedListenerMock;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class SuperfeedrMock implements FeedsUpdateService {
    private static final String UPDATER_FEED_SUBSCRIPTION_CANCELED_FOLDER = "updaterServiceFeedSubscriptionCanceled";
    private static final String UPDATER_FEED_SUBSCRIPTION_FOLDER = "updaterServiceFeedSubscription";
    @Inject
    private FileBasedPersistence<String> fileBasedPersistence;
    @Inject
    private IncomingFeedListenerMock incomingFeedListenerMock;

    @Override
    public void subscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        for (URL url : urls) {
            fileBasedPersistence.save(UPDATER_FEED_SUBSCRIPTION_FOLDER, url.toExternalForm(), url.toExternalForm());
        }
    }

    @Override
    public void unsubscribe(final List<URL> urls, final ServiceResponseHandler onResponseHandler) {
        for (URL url : urls) {
            fileBasedPersistence.save(UPDATER_FEED_SUBSCRIPTION_CANCELED_FOLDER, url.toExternalForm(), url.toExternalForm());
            fileBasedPersistence.remove(UPDATER_FEED_SUBSCRIPTION_FOLDER, url.toExternalForm());
        }
    }

    @Override
    public List<URL> getSubscriptionList(final ServiceResponseHandler onResponseHandler) {
        List<String> feedUrls = fileBasedPersistence.getFolderContent(UPDATER_FEED_SUBSCRIPTION_FOLDER);

        return mapFeedUrlStringToURL(feedUrls);
    }

    @Override
    public void setMessageHandler(final ServiceNotificationHandler messageHandler) {
        incomingFeedListenerMock.setNotificationHandler(messageHandler);
        incomingFeedListenerMock.startListener();
    }

    private List<URL> mapFeedUrlStringToURL(final List<String> feedUrls) {
        List<URL> fetchResult = new ArrayList<>();
        for (String feedUrl : feedUrls) {
            try {
                fetchResult.add(new URL(feedUrl));
            } catch (MalformedURLException e) {
                SuperfeedrMock.log.error("Looks like you've provided an invalid url during test:{}", feedUrl);
            }
        }

        return fetchResult;
    }
}
