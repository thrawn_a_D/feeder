package de.wsorg.feeder.processor.updater.service.util.config;

import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.FeedListenerDeterminator;
import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SuperfeedrConfigVerifier implements ConfigVerifier {

    public static final String SUPERFEEDR_USERNAME = "superfeedr.username";
    public static final String SUPERFEEDR_PASSWORD = "superfeedr.password";
    public static final String SUPERFEEDR_HOST = "superfeedr.host";

    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult configVerificationResult = new ConfigVerificationResult(configurationFilePath);

        verifyProperty(configVerificationResult,
                configProperties,
                SUPERFEEDR_USERNAME,
                "Username for superfeedr is not set.");

        verifyProperty(configVerificationResult,
                configProperties,
                SUPERFEEDR_PASSWORD,
                "Password for superfeedr is not set.");

        verifyProperty(configVerificationResult,
                configProperties,
                SUPERFEEDR_HOST,
                "Host for superfeedr is not set.");

        final String pollingEnabled = configProperties.getProperty(FeedListenerDeterminator.UPDATE_FEEDS_USING_LOCAL_POLLING);
        verifyBooleanValue(configVerificationResult, FeedListenerDeterminator.UPDATE_FEEDS_USING_LOCAL_POLLING, pollingEnabled);

        return configVerificationResult;
    }

    private void verifyBooleanValue(final ConfigVerificationResult configVerificationResult, final String propertyName, final String proeprtyValue) {
        if(StringUtils.isNotBlank(proeprtyValue)) {
            if(!proeprtyValue.toUpperCase().contains("FALSE") && !proeprtyValue.toUpperCase().contains("TRUE")) {
                configVerificationResult.addVerificationError(propertyName, "Please provide a valid boolean value");
            }
        }
    }

    private boolean verifyProperty(final ConfigVerificationResult configVerificationResult, final Properties configProperties, final String propertyName, final String errorText) {
        boolean valid=false;
        final String propertyValue = configProperties.getProperty(propertyName);
        if(StringUtils.isBlank(propertyValue)) {
            configVerificationResult.addVerificationError(propertyName, errorText);
        } else {
            valid = true;
        }

        return valid;
    }
}
