package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.polling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.superfeedr.extension.notification.EntryExtension;
import org.superfeedr.extension.notification.HttpExtension;
import org.superfeedr.extension.notification.ItemExtension;
import org.superfeedr.extension.notification.ItemsExtension;
import org.superfeedr.extension.notification.StatusExtension;
import org.superfeedr.extension.notification.SuperfeedrEventExtension;

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.IncomingFeedListenerMock;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class IncomingFeedsByPollingManager implements IncomingFeedListenerMock {
    public static final String UPDATE_FEEDS_USING_LOCAL_POLLING = "updateFeedsUsingLocalPolling";
    @Inject
    private UpdaterStorageProvider storageProvider;
    @Inject
    private ExternalFeedLoaderFactoryProxy externalFeedLoaderFactoryProxy;
    @Inject
    private ServiceNotificationHandler<SuperfeedrEventExtension> notificationHandler;

    @Value("${" + UPDATE_FEEDS_USING_LOCAL_POLLING + ":false}")
    private String updateFeedsUsingLocalPolling;

    @Override
    @Scheduled(fixedRate = 60000 * 5)
    public void startListener() {
        if(Boolean.valueOf(updateFeedsUsingLocalPolling)) {
            List<FeedLoadStatistic> allAvailableLoadStats = storageProvider.getFeedLoadStatisticsDataAccess().findLoadStatisticsOlderThen(new Date());

            for (FeedLoadStatistic allAvailableLoadStat : allAvailableLoadStats) {
                try {
                    FeedModel externalFeed = externalFeedLoaderFactoryProxy.getExternalFeedLoader().loadFeed(allAvailableLoadStat.getFeedUrl());
                    FeedModel localFeed = storageProvider.getFeedDataAccess().findByFeedUrl(externalFeed.getFeedUrl());

                    List<ItemExtension> itemsList = calculateDiffOnEntries(externalFeed, localFeed);

                    if (itemsList.size() > 0) {
                        SuperfeedrEventExtension incomingFeedEntry = createSuperfeedrEvent(externalFeed, itemsList);

                        notificationHandler.onNotification(incomingFeedEntry);
                    } else {
                        log.debug("Polling for feed finished. Nothing changed.");
                    }

                } catch (Exception e) {
                    log.error("An error occurred while updating feed using polling.", e);
                }
            }
        }
    }

    private SuperfeedrEventExtension createSuperfeedrEvent(final FeedModel externalFeed, final List<ItemExtension> itemsList) {
        StatusExtension statusExtension = new StatusExtension(externalFeed.getFeedUrl(), new Date(), new HttpExtension("200", ""));
        ItemsExtension itemsExtension = new ItemsExtension("", itemsList);
        return new SuperfeedrEventExtension(statusExtension, itemsExtension);
    }

    private List<ItemExtension> calculateDiffOnEntries(final FeedModel externalFeed, final FeedModel localFeed) {
        List<ItemExtension> itemsList = new ArrayList<>();
        for (FeedEntryModel feedEntryModel : externalFeed.getFeedEntryModels()) {
            if(!localFeed.containsFeedEntry(feedEntryModel.getId())) {

                makeSureUpdateDateIsSet(feedEntryModel);

                Link linkToUse = getLinkToAdd(feedEntryModel);

                EntryExtension newEntry = new EntryExtension(feedEntryModel.getId(),
                                                             linkToUse.getHref(),
                                                             linkToUse.getLinkType() != null ? linkToUse.getLinkType().getLinkType() : "",
                                                             new Date(),
                                                             feedEntryModel.getUpdated(),
                                                             feedEntryModel.getTitle(),
                                                             feedEntryModel.getDescription(),
                                                             "");
                itemsList.add(new ItemExtension(newEntry));
            }
        }
        return itemsList;
    }

    private Link getLinkToAdd(final FeedEntryModel feedEntryModel) {
        Link linkToUse = new Link();
        for (Link link : feedEntryModel.getLinks()) {
            if(link.getLinkType().isHtml())
                linkToUse = link;
        }
        return linkToUse;
    }

    private void makeSureUpdateDateIsSet(final FeedEntryModel feedEntryModel) {
        if(feedEntryModel.getUpdated() == null)
            feedEntryModel.setUpdated(new Date());
    }

    @Override
    public void setNotificationHandler(final ServiceNotificationHandler handler) {
        this.notificationHandler = handler;
    }
}
