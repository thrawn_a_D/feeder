package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.impl;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.codec.binary.Base64;

import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.NotificationCallback;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscriber;
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscription;
import org.apache.commons.lang.StringUtils;

/**
 * Basic {@link Subscription} implementation. Generates a unique subscription
 * and a random verify token using a MD5 digest.
 * 
 * @author Benjamin Erb
 * 
 */
public class SubscriptionImpl implements Subscription {
	private final URI feedUri;
	private final URI hubUri;
	private final Subscriber subscriber;
	private final String id;
	private final String verifyToken;
	private NotificationCallback notificationCallback;

	/**
	 * Creates a new subscription.
	 * @param feedUri 
	 * @param hubUri
	 * @param subscriber
	 */
	public SubscriptionImpl(URI feedUri, URI hubUri, Subscriber subscriber)
	{
		this.feedUri = feedUri;
		this.hubUri = hubUri;
		this.subscriber = subscriber;

        this.id = StringUtils.removeEnd(Base64.encodeBase64String(feedUri.toString().getBytes()), "=");
        this.verifyToken = this.id;
	}

    public SubscriptionImpl(final String base64EncodedTopicUrl, URI hubUri, Subscriber subscriber) {
        try {
            this.feedUri = new URI(new String(Base64.decodeBase64(base64EncodedTopicUrl)));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        this.hubUri = hubUri;
        this.subscriber = subscriber;

        id = base64EncodedTopicUrl;
        this.verifyToken = base64EncodedTopicUrl;
    }

	@Override
	public URI getFeedTopicUri()
	{
		return feedUri;
	}

	@Override
	public String getInternalId()
	{
		return id;
	}

	@Override
	public Subscriber getSubscriber()
	{
		return subscriber;
	}

	@Override
	public String getVerifyToken()
	{
		return verifyToken;
	}

	@Override
	public NotificationCallback getNotificationCallback()
	{
		return notificationCallback;
	}

	@Override
	public void setNotificationCallback(NotificationCallback callback)
	{
		this.notificationCallback = callback;
	}

	@Override
	public URI getHubUri()
	{
		return hubUri;
	}

}
