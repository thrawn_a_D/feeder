package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming;

import org.springframework.beans.factory.annotation.Value;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedListenerDeterminator {
    public static final String UPDATE_FEEDS_USING_LOCAL_POLLING = "updateFeedsUsingLocalPolling";

    @Value("${" + UPDATE_FEEDS_USING_LOCAL_POLLING + ":false}")
    private String updateFeedsUsingLocalPolling;
    private IncomingFeedListenerMock fileBasedFeedListener;
    private IncomingFeedListenerMock pollingBasedFeedUpdater;

    public IncomingFeedListenerMock getIncomingFeedListenerMock() {
        if(Boolean.valueOf(updateFeedsUsingLocalPolling)) {
            return pollingBasedFeedUpdater;
        } else {
            return fileBasedFeedListener;
        }
    }

    public void setFileBasedFeedListener(final IncomingFeedListenerMock fileBasedFeedListener) {
        this.fileBasedFeedListener = fileBasedFeedListener;
    }

    public void setPollingBasedFeedUpdater(final IncomingFeedListenerMock pollingBasedFeedUpdater) {
        this.pollingBasedFeedUpdater = pollingBasedFeedUpdater;
    }
}
