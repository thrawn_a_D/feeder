package de.wsorg.feeder.processor.updater.service.util.service.handler;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ServiceNotificationHandler<T> {
    void onNotification(T superfeedrEventExtension);
}
