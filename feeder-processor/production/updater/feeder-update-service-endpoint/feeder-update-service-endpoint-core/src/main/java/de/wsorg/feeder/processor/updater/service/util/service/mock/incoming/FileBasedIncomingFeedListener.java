package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.scheduling.annotation.Scheduled;
import org.superfeedr.extension.notification.EntryExtension;
import org.superfeedr.extension.notification.HttpExtension;
import org.superfeedr.extension.notification.ItemExtension;
import org.superfeedr.extension.notification.ItemsExtension;
import org.superfeedr.extension.notification.StatusExtension;
import org.superfeedr.extension.notification.SuperfeedrEventExtension;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedIncomingFeedListener implements IncomingFeedListenerMock {
    private static final String INCOMING_SUBSCRIPTIONS_FOLDER = "externalServiceIncomingSubscriptions";
    @Inject
    private FileBasedPersistence<Map<String, Object>> fileBasedPersistence;
    private ServiceNotificationHandler<SuperfeedrEventExtension> notificationHandler;

    @Override
    @Scheduled(fixedDelay = 1000)
    public void startListener() {
        processNotification();
    }

    private void processNotification() {
        if (notificationHandler != null) {
            final List<Map<String, Object>> incomingSubscriptions = fileBasedPersistence.getFolderContent(INCOMING_SUBSCRIPTIONS_FOLDER);

            for (Map<String, Object> incomingSubscription : incomingSubscriptions) {
                String feedUrl = (String) incomingSubscription.get("FEED_URL");
                String entryId = (String) incomingSubscription.get("ENTRY_ID");
                String entryLink = (String) incomingSubscription.get("ENTRY_LINK");
                String entryLinkType = (String) incomingSubscription.get("ENTRY_LINK_TYPE");
                Date entryPublished = (Date) incomingSubscription.get("ENTRY_PUBLISHED");
                Date entryUpdated = (Date) incomingSubscription.get("ENTRY_UPDATED");
                String entryTitle = (String) incomingSubscription.get("ENTRY_TITLE");
                String entrySummary = (String) incomingSubscription.get("ENTRY_SUMMARY");
                String entryContent = (String) incomingSubscription.get("ENTRY_CONTENT");

                StatusExtension statusExtension = new StatusExtension(feedUrl, new Date(), new HttpExtension("200", ""));
                EntryExtension newEntry = new EntryExtension(entryId, entryLink, entryLinkType, entryPublished, entryUpdated, entryTitle, entrySummary, entryContent);
                List<ItemExtension> itemsList = new ArrayList<>();
                itemsList.add(new ItemExtension(newEntry));
                ItemsExtension itemsExtension = new ItemsExtension("", itemsList);
                SuperfeedrEventExtension incomingFeedEntry = new SuperfeedrEventExtension(statusExtension, itemsExtension);

                notificationHandler.onNotification(incomingFeedEntry);

            }
        }
    }

    @Override
    public void setNotificationHandler(final ServiceNotificationHandler handler) {
        this.notificationHandler = handler;
    }
}
