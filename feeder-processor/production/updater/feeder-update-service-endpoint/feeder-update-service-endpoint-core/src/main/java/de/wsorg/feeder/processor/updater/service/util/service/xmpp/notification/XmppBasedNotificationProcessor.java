package de.wsorg.feeder.processor.updater.service.util.service.xmpp.notification;

import javax.inject.Inject;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.superfeedr.extension.notification.ItemExtension;
import org.superfeedr.extension.notification.SuperfeedrEventExtension;

import com.google.gson.Gson;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator;
import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class XmppBasedNotificationProcessor implements SuperfeedrNotificationProcessor<SuperfeedrEventExtension> {
    @Inject
    private UpdateComponentsCommunicator updateComponentsCommunicator;

    @Override
    @Async
    public void processNotification(final SuperfeedrEventExtension notification) {
        XmppBasedNotificationProcessor.log.debug("Got feed entry, delegate to updating.");

        String mappedUpdate = mapFeedEntryToModel(notification);
        updateComponentsCommunicator.sendFeedUpdateToUpdaterModules(mappedUpdate);
    }

    private String mapFeedEntryToModel(final SuperfeedrEventExtension notification) {
        final FeedModel feedModel = new FeedModel();

        feedModel.setFeedUrl(notification.getStatus().getFeedURL());

        final Iterator<ItemExtension> items = notification.getItems().getItems();
        while (items.hasNext()) {
            final ItemExtension incomingFeedElement = items.next();

            final FeedEntryModel newFeedEntryModel = new FeedEntryModel();
            newFeedEntryModel.setId(incomingFeedElement.getEntry().getId());
            newFeedEntryModel.setFeedUrl(notification.getStatus().getFeedURL());
            newFeedEntryModel.setDescription(incomingFeedElement.getEntry().getSummary());
            newFeedEntryModel.setTitle(incomingFeedElement.getEntry().getTitle());
            newFeedEntryModel.setUpdated(incomingFeedElement.getEntry().getUpdated());

            mapLink(incomingFeedElement, newFeedEntryModel);

            feedModel.addFeedEntry(newFeedEntryModel);
        }

        Gson gson = new Gson();
        return gson.toJson(feedModel);
    }

    private void mapLink(final ItemExtension incomingFeedElement, final FeedEntryModel newFeedEntryModel) {
        if(StringUtils.isNotBlank(incomingFeedElement.getEntry().getLink())) {
            Link link = new Link();
            link.setHref(incomingFeedElement.getEntry().getLink());
            link.setLinkType(new LinkType("text/html"));

            newFeedEntryModel.addLink(link);
        }
    }
}
