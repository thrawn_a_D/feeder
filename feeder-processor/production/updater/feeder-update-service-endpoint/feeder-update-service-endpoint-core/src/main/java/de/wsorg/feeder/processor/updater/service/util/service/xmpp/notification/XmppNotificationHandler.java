package de.wsorg.feeder.processor.updater.service.util.service.xmpp.notification;

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler;
import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor;
import org.superfeedr.extension.notification.SuperfeedrEventExtension;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class XmppNotificationHandler implements ServiceNotificationHandler<SuperfeedrEventExtension> {
    @Inject
    @Named("xmppBasedNotificationProcessor")
    private SuperfeedrNotificationProcessor<SuperfeedrEventExtension> superfeedrNotificationProcessor;

    @Override
    public void onNotification(final SuperfeedrEventExtension superfeedrEventExtension) {
        superfeedrNotificationProcessor.processNotification(superfeedrEventExtension);
    }
}
