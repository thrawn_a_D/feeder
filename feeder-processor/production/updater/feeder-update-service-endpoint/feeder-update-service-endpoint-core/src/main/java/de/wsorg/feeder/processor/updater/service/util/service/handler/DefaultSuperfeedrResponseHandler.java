package de.wsorg.feeder.processor.updater.service.util.service.handler;

import javax.inject.Named;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Slf4j
public class DefaultSuperfeedrResponseHandler implements ServiceResponseHandler {
    @Override
    public void onSuccess(final Object o) {
        log.debug("Superfeedr call was successful");
    }

    @Override
    public void onError(final String s) {
        log.error("Some error occurred while calling superfeedr: " + s);
    }
}
