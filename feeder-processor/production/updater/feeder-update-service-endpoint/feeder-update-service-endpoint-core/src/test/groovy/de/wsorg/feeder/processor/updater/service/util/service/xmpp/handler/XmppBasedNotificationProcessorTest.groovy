package de.wsorg.feeder.processor.updater.service.util.service.xmpp.handler

import com.google.gson.Gson
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.notification.XmppBasedNotificationProcessor
import org.superfeedr.extension.notification.EntryExtension
import org.superfeedr.extension.notification.ItemExtension
import org.superfeedr.extension.notification.ItemsExtension
import org.superfeedr.extension.notification.StatusExtension
import org.superfeedr.extension.notification.SuperfeedrEventExtension
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class XmppBasedNotificationProcessorTest extends Specification {

    XmppBasedNotificationProcessor incomingFeedToUpdaterComponentDelegator
    UpdateComponentsCommunicator updateComponentsCommunicator

    def setup(){
        incomingFeedToUpdaterComponentDelegator = new XmppBasedNotificationProcessor()
        updateComponentsCommunicator = Mock(UpdateComponentsCommunicator)
        incomingFeedToUpdaterComponentDelegator.updateComponentsCommunicator = updateComponentsCommunicator
    }

    def "Delegate feed to process"() {
        given:
        def feedUrl = 'http://feedUrl'
        def context = 'context'
        def updated = new Date()
        def title = 'title'
        def summary = 'summary'
        def id = 'id'
        def link = 'link'
        def linkType = 'text/html'
        def published = new Date()

        and:
        def expectedFeedModel = new FeedModel(feedUrl: feedUrl)
        expectedFeedModel.addFeedEntry(new FeedEntryModel(feedUrl: feedUrl,
                                                          description: summary,
                                                          id: id,
                                                          updated: updated,
                                                          title: title))

        and:
        def messageToHandle = Mock(SuperfeedrEventExtension)
        def feedStatus = Mock(StatusExtension)
        def itemsExtension = Mock(ItemsExtension)
        def feedItem = Mock(ItemExtension)
        def entryExtension = Mock(EntryExtension)
        def iterator = Mock(Iterator)

        and:
        entryExtension.getContent() >> context
        entryExtension.getId() >> id
        entryExtension.getLink() >> link
        entryExtension.getLinkType() >> linkType
        entryExtension.getPublished() >> published
        entryExtension.getSummary() >> summary
        entryExtension.getTitle() >> title
        entryExtension.getUpdated() >> updated

        and:
        feedStatus.getFeedURL() >> feedUrl
        iterator.hasNext() >>> [true, false]
        iterator.next() >> feedItem
        feedItem.getEntry() >> entryExtension
        itemsExtension.getItems() >> iterator
        messageToHandle.getStatus() >> feedStatus
        messageToHandle.getItems() >> itemsExtension


        when:
        incomingFeedToUpdaterComponentDelegator.processNotification(messageToHandle)

        then:
        1 * updateComponentsCommunicator.sendFeedUpdateToUpdaterModules({checkJson(it, expectedFeedModel, link)})
    }


    def checkJson(jsonString, FeedModel expectedFeedModel, def link) {
        Gson gson = new Gson();
        FeedModel feedModel = gson.fromJson(jsonString, FeedModel)
        feedModel.feedUrl == expectedFeedModel.feedUrl &&
        feedModel.feedEntries[0].feedUrl == feedModel.feedUrl &&
        feedModel.feedEntries[0].description == feedModel.feedEntries[0].description &&
        feedModel.feedEntries[0].id == feedModel.feedEntries[0].id &&
        feedModel.feedEntries[0].title == feedModel.feedEntries[0].title &&
        feedModel.feedEntries[0].updated == feedModel.feedEntries[0].updated &&
        feedModel.feedEntries[0].links.size() > 0 &&
        feedModel.feedEntries[0].links[0].href == link &&
        feedModel.feedEntries[0].links[0].linkType.linkType == 'text/html'
    }
}
