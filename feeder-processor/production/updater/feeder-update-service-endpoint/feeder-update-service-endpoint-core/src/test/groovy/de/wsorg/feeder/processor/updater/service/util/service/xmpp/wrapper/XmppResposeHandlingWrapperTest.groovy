package de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.05.13
 */
class XmppResposeHandlingWrapperTest extends Specification {
    def "Delegate success response"() {
        given:
        ServiceResponseHandler responseHandler = Mock(ServiceResponseHandler)

        and:
        def responseHandlerWrapper = new XmppResposeHandlingWrapper(responseHandler)

        and:
        def message = 'success'

        when:
        responseHandlerWrapper.onSuccess(message)

        then:
        1 * responseHandler.onSuccess(message)
    }

    def "Delegate error response"() {
        given:
        ServiceResponseHandler responseHandler = Mock(ServiceResponseHandler)

        and:
        def responseHandlerWrapper = new XmppResposeHandlingWrapper(responseHandler)

        and:
        def message = 'error'

        when:
        responseHandlerWrapper.onError(message)

        then:
        1 * responseHandler.onError(message)
    }

}
