package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification.wrapper

import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.05.13
 */
class PubSubNotificationWrapperTest extends Specification {
    PubSubNotificationWrapper pubSubNotificationHandler
    ServiceNotificationHandler serviceNotificationHandler

    def setup(){
        serviceNotificationHandler = Mock(ServiceNotificationHandler)

        pubSubNotificationHandler = new PubSubNotificationWrapper(serviceNotificationHandler)
    }

    def "Delegate call"() {
        given:
        def feed = Mock(SyndFeed)

        when:
        pubSubNotificationHandler.handle(feed)

        then:
        1 * serviceNotificationHandler.onNotification(feed)
    }
}
