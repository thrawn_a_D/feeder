package de.wsorg.feeder.processor.updater.service.executor.superfeedr

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.03.13
 */
class CancelSubscriptionOnSuperfeedrTest extends Specification {
    CancelSubscriptionOnSuperfeedr cancelSubscriptionOnSuperfeedr
    ServiceResponseHandler responseHandler
    FeedsUpdateService feedsUpdateService

    def setup(){
        cancelSubscriptionOnSuperfeedr = new CancelSubscriptionOnSuperfeedr()
        responseHandler = Mock(ServiceResponseHandler)
        feedsUpdateService = Mock(FeedsUpdateService)


        cancelSubscriptionOnSuperfeedr.feedsUpdateService = feedsUpdateService
        cancelSubscriptionOnSuperfeedr.responseHandler = responseHandler
    }

    def "Cancel the subscription"() {
        given:
        def jobMesage = new UpdaterJobMessage(identifier: 'http://bla.blub')

        when:
        cancelSubscriptionOnSuperfeedr.execute(jobMesage)

        then:
        1 * feedsUpdateService.unsubscribe({List<URL> urls -> urls[0].toString().contains(jobMesage.identifier)}, responseHandler)
    }
}
