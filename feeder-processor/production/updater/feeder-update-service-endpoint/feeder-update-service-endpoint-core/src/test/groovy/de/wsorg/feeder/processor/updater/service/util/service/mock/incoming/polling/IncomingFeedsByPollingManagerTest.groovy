package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.polling;


import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader
import org.superfeedr.extension.notification.SuperfeedrEventExtension
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.03.13
 */
public class IncomingFeedsByPollingManagerTest extends Specification {
    IncomingFeedsByPollingManager incomingFeedsByPollingManager
    UpdaterStorageProvider storageProvider
    FeedDataAccess feedDataAccess
    FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess
    ExternalFeedLoaderFactoryProxy externalFeedLoaderFactoryProxy
    ExternalFeedLoader externalFeedLoader
    ServiceNotificationHandler notificationHandler

    def setup(){
        incomingFeedsByPollingManager = new IncomingFeedsByPollingManager()

        storageProvider = Mock(UpdaterStorageProvider)
        feedDataAccess = Mock(FeedDataAccess)
        feedLoadStatisticsDataAccess = Mock(FeedLoadStatisticsDataAccess)
        externalFeedLoaderFactoryProxy = Mock(ExternalFeedLoaderFactoryProxy)
        externalFeedLoader = Mock(ExternalFeedLoader)
        notificationHandler = Mock(ServiceNotificationHandler)

        storageProvider.getFeedDataAccess() >> feedDataAccess
        storageProvider.getFeedLoadStatisticsDataAccess() >> feedLoadStatisticsDataAccess
        externalFeedLoaderFactoryProxy.getExternalFeedLoader() >> externalFeedLoader

        incomingFeedsByPollingManager.storageProvider = storageProvider
        incomingFeedsByPollingManager.externalFeedLoaderFactoryProxy = externalFeedLoaderFactoryProxy
        incomingFeedsByPollingManager.notificationHandler = notificationHandler
    }

    def "Calculate delta of update feed and publish result to receiver component"() {
        given:
        def feedUrl = 'http://asdasd.de'
        def feedLoadStat = new FeedLoadStatistic(feedUrl: feedUrl)
        def loadedExternalFeed = new FeedModel(feedUrl: feedUrl)
        def currentFeedInStorage = new FeedModel(feedUrl: feedUrl)

        and:
        def feedEntry1 = new FeedEntryModel(id: 1)
        def feedEntry2 = new FeedEntryModel(id: 2)

        def entryId = '3'
        def entryUpdated = new Date()
        def entryTitle = 'lkklhlh'
        def entrySummary = 'lkjh89jzi'
        def feedEntry3 = new FeedEntryModel(id: entryId,
                title: entryTitle,
                updated: entryUpdated,
                description: entrySummary)

        and:
        currentFeedInStorage.addFeedEntry(feedEntry1)
        currentFeedInStorage.addFeedEntry(feedEntry2)
        loadedExternalFeed.addFeedEntry(feedEntry3)

        and:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = "true"

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen({it.date == new Date().date}) >> [feedLoadStat]
        1 * externalFeedLoader.loadFeed(feedUrl) >> loadedExternalFeed
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> currentFeedInStorage
        1 * notificationHandler.onNotification({SuperfeedrEventExtension it -> it.status.feedURL == feedUrl &&
                it.items.items.next().entry.id == entryId &&
                it.items.items.next().entry.updated == entryUpdated &&
                it.items.items.next().entry.title == entryTitle &&
                it.items.items.next().entry.summary == entrySummary &&
                it.items.items.size() == 1})
    }

    def "Run polling only if particular property is on"() {
        given:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = "false"

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        0 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen(_)
        0 * externalFeedLoader.loadFeed(_)
        0 * notificationHandler.onNotification(_)
    }

    def "Run polling only if particular property is on 2"() {
        given:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = ""

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        0 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen(_)
        0 * externalFeedLoader.loadFeed(_)
        0 * notificationHandler.onNotification(_)
    }

    def "Assign notification handler"() {
        given:
        ServiceNotificationHandler onNotificationHandler = Mock(ServiceNotificationHandler)


        when:
        incomingFeedsByPollingManager.setNotificationHandler(onNotificationHandler)

        then:
        incomingFeedsByPollingManager.notificationHandler == onNotificationHandler
    }

    def "Delegate feed update only if there is a diff available to update"() {
        given:
        def feedUrl = 'http://asdasd.de'
        def feedLoadStat = new FeedLoadStatistic(feedUrl: feedUrl)
        def loadedExternalFeed = new FeedModel(feedUrl: feedUrl)
        def currentFeedInStorage = new FeedModel(feedUrl: feedUrl)

        and:
        def feedEntry = new FeedEntryModel(id: 1)

        and:
        currentFeedInStorage.addFeedEntry(feedEntry)
        loadedExternalFeed.addFeedEntry(feedEntry)

        and:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = "true"

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen({it.date == new Date().date}) >> [feedLoadStat]
        1 * externalFeedLoader.loadFeed(feedUrl) >> loadedExternalFeed
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> currentFeedInStorage
        0 * notificationHandler.onNotification(_)
    }

    def "If feed entry has no update date, set current"() {
        given:
        def feedUrl = 'http://asdasd.de'
        def feedLoadStat = new FeedLoadStatistic(feedUrl: feedUrl)
        def loadedExternalFeed = new FeedModel(feedUrl: feedUrl)
        def currentFeedInStorage = new FeedModel(feedUrl: feedUrl)

        and:
        def feedEntry = new FeedEntryModel(updated: null)

        and:
        loadedExternalFeed.addFeedEntry(feedEntry)

        and:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = "true"

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen({it.date == new Date().date}) >> [feedLoadStat]
        1 * externalFeedLoader.loadFeed(feedUrl) >> loadedExternalFeed
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> currentFeedInStorage
        1 * notificationHandler.onNotification({SuperfeedrEventExtension it ->
                it.items.items.next().entry.updated != null &&
                it.items.items.next().entry.updated.date > 0})
    }

    def "Map link to entry"() {
        given:
        def feedUrl = 'http://asdasd.de'
        def feedLoadStat = new FeedLoadStatistic(feedUrl: feedUrl)
        def loadedExternalFeed = new FeedModel(feedUrl: feedUrl)
        def currentFeedInStorage = new FeedModel(feedUrl: feedUrl)

        and:
        def entryModel = new FeedEntryModel()
        def link1 = new Link()
        link1.href = 'sdfkjhsdf'
        link1.linkType = new LinkType('text/html')

        def link2 = new Link()
        link2.href = 'dfgtrfdsdf'
        link2.linkType = new LinkType('application/pdf')

        entryModel.setLinks([link2, link1])
        loadedExternalFeed.addFeedEntry(entryModel)

        and:
        incomingFeedsByPollingManager.updateFeedsUsingLocalPolling = "true"

        when:
        incomingFeedsByPollingManager.startListener()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen(_) >> [feedLoadStat]
        1 * externalFeedLoader.loadFeed(feedUrl) >> loadedExternalFeed
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> currentFeedInStorage
        1 * notificationHandler.onNotification({SuperfeedrEventExtension it ->
            it.items.items.next().entry.link == link1.href &&
            it.items.items.next().entry.linkType == link1.linkType.linkType})
    }
}
