package de.wsorg.feeder.processor.updater.service.util.config

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.05.13
 */
class SuperfeedrProtocolConfigVerifierTest extends Specification {
    SuperfeedrProtocolConfigVerifier superfeedrProtocolConfigVerifier
    def configLocation = [''] as String[]

    def setup(){
        superfeedrProtocolConfigVerifier = new SuperfeedrProtocolConfigVerifier()
    }


    def "Verify all mandatory properties are valid"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'xmpp')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Superfeedr protocol is not set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, '')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL] == ['Protocol for superfeedr is not set.']
        result.isConfigErroneous()
    }

    def "Verify Superfeedr protocol is not set to valid value"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'asdasd')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL] == ['Protocol for superfeedr is not set to a valid value. Possible range is: {xmpp|pubsub|mocked}']
        result.isConfigErroneous()
    }

    def "Verify Superfeedr protocol is set to valid value, xmpp"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'xmpp')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Verify Superfeedr protocol is set to valid value, pubsub"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'pubsub')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST, 'http://sadsdf.cd')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT, '8888')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, 'http://asdkjhaskjd.dd')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Verify Superfeedr protocol is set to valid value, mocked"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'mocked')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "If protocol is set to pubsub, make sure receiver host is set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'pubsub')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT, '8888')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, 'http://asdkjhaskjd.dd')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST] == ['When using pubsub as protocol you need to provide hostname of receiver.']
    }

    def "If protocol is set to pubsub, make sure receiver port is set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'pubsub')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST, 'http://sadsdf.cd')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, 'http://asdkjhaskjd.dd')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT] == ['When using pubsub as protocol you need to provide port of receiver.']
    }

    def "If protocol is set to pubsub, verify master hub is set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'pubsub')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST, 'http://sadsdf.cd')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT, '8888')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST] == ['When using pubsub as protocol you need to provide a valid hub uri.']
    }

    def "If protocol is set to pubsub, verify master hub is set and is valid"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_PROTOCOL, 'pubsub')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_HOST, 'http://sadsdf.cd')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_SUBSCRIBER_PORT, '8888')
        properties.put(SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST, 'as das dasd')

        when:
        def result = superfeedrProtocolConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[SuperfeedrProtocolConfigVerifier.SUPERFEEDR_HUB_HOST] == ['When using pubsub as protocol you need to provide a valid hub uri.']
    }
}
