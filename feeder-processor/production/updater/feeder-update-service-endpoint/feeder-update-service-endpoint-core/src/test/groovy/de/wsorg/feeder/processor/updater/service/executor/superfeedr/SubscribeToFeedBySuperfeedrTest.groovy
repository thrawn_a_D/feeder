package de.wsorg.feeder.processor.updater.service.executor.superfeedr

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.03.12
 */
class SubscribeToFeedBySuperfeedrTest extends Specification {
    SubscribeToFeedBySuperfeedr subscribeToFeedJobExecutor
    FeedsUpdateService feedsUpdateService
    ServiceResponseHandler defaultResponseHandler

    def setup(){
        subscribeToFeedJobExecutor = new SubscribeToFeedBySuperfeedr()
        defaultResponseHandler = Mock(ServiceResponseHandler)
        feedsUpdateService = Mock(FeedsUpdateService)


        subscribeToFeedJobExecutor.feedsUpdateService = feedsUpdateService
        subscribeToFeedJobExecutor.defaultResponseHandler = defaultResponseHandler
    }

    def "Subscribe to feed"() {
        given:
        def updateRequest = new UpdaterJobMessage(identifier: 'http://feeder.de')

        when:
        subscribeToFeedJobExecutor.execute(updateRequest)

        then:
        1 * feedsUpdateService.subscribe({List<URL> urls -> urls[0].toString().contains(updateRequest.identifier)}, defaultResponseHandler)
    }

    def "Provide an invalid feedUrl and expect an exception"() {
        given:
        def updateRequest = new UpdaterJobMessage(identifier: 'sdaasd')

        when:
        subscribeToFeedJobExecutor.execute(updateRequest)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided feed url is not a valid URL'
    }
}