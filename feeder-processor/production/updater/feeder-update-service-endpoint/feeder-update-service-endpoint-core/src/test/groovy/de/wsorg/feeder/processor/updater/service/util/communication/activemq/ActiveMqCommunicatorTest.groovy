package de.wsorg.feeder.processor.updater.service.util.communication.activemq

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.TextMessageCreator
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator
import org.apache.activemq.command.ActiveMQQueue
import org.springframework.context.ApplicationContext
import org.springframework.jms.core.JmsTemplate
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class ActiveMqCommunicatorTest extends Specification {
    ActiveMqCommunicator activeMqCommunicator
    ApplicationContext applicationContext
    JmsTemplate jmsTemplate
    ActiveMQQueue getSubscribedFeedsResponse
    ActiveMQQueue feedUpdateResponseQueue

    def setup(){
        activeMqCommunicator = new ActiveMqCommunicator()

        jmsTemplate = Mock(JmsTemplate)
        applicationContext = Mock(ApplicationContext)
        getSubscribedFeedsResponse = Mock(ActiveMQQueue)
        feedUpdateResponseQueue = Mock(ActiveMQQueue)

        applicationContext.getBean(JmsTemplate) >> jmsTemplate

        activeMqCommunicator.applicationContext = applicationContext
        activeMqCommunicator.getSubscribedFeedsResponseQueue = getSubscribedFeedsResponse
        activeMqCommunicator.feedUpdateResponseQueue = feedUpdateResponseQueue
    }

    def "Register a feed in the queue"() {
        given:
        def feedUrls = ['kjhkjh']

        when:
        activeMqCommunicator.sendSubscribedFeedListToComponents(feedUrls)

        then:
        1 * jmsTemplate.send(getSubscribedFeedsResponse,
                {UpdateManagerMessageCreator it ->
                    it.feedUrl == feedUrls.toString() &&
                            it.action == UpdateManagerAction.GET_SUBSCRIBED_FEEDS})
    }

    def "Send update info to other modules"() {
        given:
        def jsonContent = '{json}'

        when:
        activeMqCommunicator.sendFeedUpdateToUpdaterModules(jsonContent)

        then:
        1 * jmsTemplate.send(feedUpdateResponseQueue, {TextMessageCreator it -> it.content == jsonContent})
    }
}
