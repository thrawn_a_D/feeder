package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscriber
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.api.subscriber.Subscription
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.05.13
 */
class SuperfeedrPubSubBasedTest extends Specification {
    SuperfeedrPubSubBased superfeedrPubSubBased
    Subscriber subscriber

    ServiceResponseHandler serviceResponseHandler
    ServiceNotificationHandler notificationHandler

    def setup(){
        superfeedrPubSubBased = new SuperfeedrPubSubBased()

        subscriber = Mock(Subscriber)
        serviceResponseHandler = Mock(ServiceResponseHandler)
        notificationHandler = Mock(ServiceNotificationHandler)

        superfeedrPubSubBased.subscriber = subscriber
        superfeedrPubSubBased.setMessageHandler(notificationHandler)
    }

    def "Subscribe to some feeds"() {
        given:
        def url1 = new URL('http://bla1.blub')
        def url2 = new URL('http://bla2.blub')
        def urls = [url1, url2]

        and:
        def subscription1 = Mock(Subscription)
        def subscription2 = Mock(Subscription)

        when:
        superfeedrPubSubBased.subscribe(urls, serviceResponseHandler)

        then:
        1 * subscriber.subscribe(url1.toURI()) >> subscription1
        1 * subscriber.subscribe(url2.toURI()) >> subscription2
        1 * subscription1.setNotificationCallback({it.notificationHandler == notificationHandler})
        1 * subscription2.setNotificationCallback({it.notificationHandler == notificationHandler})
        1 * serviceResponseHandler.onSuccess(subscription1)
        1 * serviceResponseHandler.onSuccess(subscription2)
    }

    def "Subscribe to feed, in case of error delegate"() {
        given:
        def url1 = new URL('http://bla1.blub')
        def urls = [url1]

        and:
        def errorMessage = 'sdsdffsdsdf'
        subscriber.subscribe(url1.toURI()) >> {throw new RuntimeException(errorMessage)}

        when:
        superfeedrPubSubBased.subscribe(urls, serviceResponseHandler)

        then:
        1 * serviceResponseHandler.onError(errorMessage)
    }

    def "Unsubscribe from some feeds"() {
        given:
        def url1 = new URL('http://bla1.blub')
        def url2 = new URL('http://bla2.blub')
        def urls = [url1, url2]

        and:
        def subscription1 = Mock(Subscription)
        def subscription2 = Mock(Subscription)

        when:
        superfeedrPubSubBased.unsubscribe(urls, serviceResponseHandler)

        then:
        1 * subscriber.calculateSubscriptionOfUri(url1.toURI()) >> subscription1
        1 * subscriber.calculateSubscriptionOfUri(url2.toURI()) >> subscription2
        1 * subscriber.unsubscribe(subscription1)
        1 * subscriber.unsubscribe(subscription2)
        1 * serviceResponseHandler.onSuccess(subscription1)
        1 * serviceResponseHandler.onSuccess(subscription2)
    }

    def "Unsubscribe from feed, in case of error delegate"() {
        given:
        def url1 = new URL('http://bla1.blub')
        def urls = [url1]

        and:
        def errorMessage = 'sdsdffsdsdf'
        subscriber.calculateSubscriptionOfUri(url1.toURI()) >> {throw new RuntimeException(errorMessage)}

        when:
        superfeedrPubSubBased.unsubscribe(urls, serviceResponseHandler)

        then:
        1 * serviceResponseHandler.onError(errorMessage)
    }

    def "Get subscription list"() {
        given:
        def feedTopicUri = 'http:/lkbkj.de'
        def subscription1 = Mock(Subscription)
        subscription1.getFeedTopicUri() >> new URI(feedTopicUri)
        def subscriptions = [subscription1]

        when:
        def result = superfeedrPubSubBased.getSubscriptionList(serviceResponseHandler)

        then:
        result[0].toExternalForm() == feedTopicUri
        1 * subscriber.getSubscriptions() >> subscriptions
    }

    def "Stop subscriber service"() {
        when:
        superfeedrPubSubBased.stopSubscriberService()

        then:
        1 * subscriber.stopServer()
    }
}
