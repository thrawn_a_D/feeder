package de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import org.superfeedr.extension.notification.SuperfeedrEventExtension
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.05.13
 */
class XmppNotificationHandlerWrapperTest extends Specification {
    XmppNotificationHandlerWrapper xmppNotificationHandlerWrapper
    ServiceNotificationHandler serviceNotificationHandler

    def setup(){
        serviceNotificationHandler = Mock(ServiceNotificationHandler)

        xmppNotificationHandlerWrapper = new XmppNotificationHandlerWrapper(serviceNotificationHandler)
    }

    def "Delegate response"() {
        given:
        def superfeedrEventExtension = Mock(SuperfeedrEventExtension)

        when:
        xmppNotificationHandlerWrapper.onNotification(superfeedrEventExtension)

        then:
        1 * serviceNotificationHandler.onNotification(superfeedrEventExtension)
    }
}
