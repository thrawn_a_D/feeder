package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification

import com.google.gson.Gson
import com.sun.syndication.feed.synd.SyndContent
import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndFeed
import com.sun.syndication.feed.synd.SyndLink
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.05.13
 */
class PubSubNotificationProcessorTest extends Specification {
    PubSubNotificationProcessor pubSubNotificationProcessor
    UpdateComponentsCommunicator updateComponentsCommunicator

    def setup(){
        pubSubNotificationProcessor = new PubSubNotificationProcessor()
        updateComponentsCommunicator = Mock(UpdateComponentsCommunicator)
        pubSubNotificationProcessor.updateComponentsCommunicator = updateComponentsCommunicator
    }

    def "Delegate updating to other component"() {
        given:
        def feed = Mock(SyndFeed)
        def feedEntry1 = Mock(SyndEntry)

        and:
        feed.getUri() >> 'feedUrl'
        feed.getTitle() >> 'title'
        feed.getDescription() >> 'descr'
        feed.getEntries() >> [feedEntry1]

        and:
        def entryDescription = 'entry description'
        def entryDescriptionContent = Mock(SyndContent)
        entryDescriptionContent.getValue() >> entryDescription

        def entryLink = Mock(SyndLink)
        entryLink.getRel() >> LinkRelation.ABOUT.linkRfcCode
        entryLink.getHref() >> 'href'
        entryLink.getType() >> 'asdasd'

        feedEntry1.getTitle() >> 'entry title'
        feedEntry1.getUpdatedDate() >> new Date()
        feedEntry1.getDescription() >> entryDescriptionContent
        feedEntry1.getUri() >> 'uriFeedEntry'
        feedEntry1.getLinks() >> [entryLink]

        when:
        pubSubNotificationProcessor.processNotification(feed)

        then:
        1 * updateComponentsCommunicator.sendFeedUpdateToUpdaterModules({checkJson(it, feed)})
    }

    def checkJson(jsonString, SyndFeed expectedFeedModel) {
        Gson gson = new Gson();
        FeedModel feedModel = gson.fromJson(jsonString, FeedModel)
        feedModel.feedUrl == expectedFeedModel.uri &&
        feedModel.title == expectedFeedModel.title &&
        feedModel.description == expectedFeedModel.description &&
        feedModel.updated.date > 0 &&
        feedModel.feedEntries[0].feedUrl == expectedFeedModel.uri &&
        feedModel.feedEntries[0].description == expectedFeedModel.entries[0].description.value &&
        feedModel.feedEntries[0].id == expectedFeedModel.entries[0].uri &&
        feedModel.feedEntries[0].title == expectedFeedModel.entries[0].title &&
        feedModel.feedEntries[0].updated.date > 0 &&
        feedModel.feedEntries[0].links[0].href == expectedFeedModel.entries[0].links[0].href &&
        feedModel.feedEntries[0].links[0].linkType.linkType == expectedFeedModel.entries[0].links[0].type &&
        feedModel.feedEntries[0].links[0].relation.linkRfcCode == expectedFeedModel.entries[0].links[0].rel
    }
}
