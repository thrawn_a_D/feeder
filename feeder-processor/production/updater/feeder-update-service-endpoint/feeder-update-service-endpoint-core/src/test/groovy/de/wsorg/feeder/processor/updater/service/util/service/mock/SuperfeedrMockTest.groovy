package de.wsorg.feeder.processor.updater.service.util.service.mock

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.IncomingFeedListenerMock
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.03.13
 */
class SuperfeedrMockTest extends Specification {
    SuperfeedrMock superfeedrMock
    FileBasedPersistence fileBasedPersistence
    ServiceResponseHandler onResponseHandler
    IncomingFeedListenerMock incomingFeedListenerMock

    def setup(){
        superfeedrMock = new SuperfeedrMock()
        fileBasedPersistence = Mock(FileBasedPersistence)
        onResponseHandler = Mock(ServiceResponseHandler)
        incomingFeedListenerMock = Mock(IncomingFeedListenerMock)
        superfeedrMock.fileBasedPersistence = fileBasedPersistence
        superfeedrMock.incomingFeedListenerMock = incomingFeedListenerMock
    }

    def "Subscribe to a feed by writing the intend to file"() {
        given:
        def feedUrl = 'http://bla.de'

        when:
        superfeedrMock.subscribe([new URL(feedUrl)], onResponseHandler)

        then:
        1 * fileBasedPersistence.save('updaterServiceFeedSubscription', feedUrl, feedUrl)
    }

    def "Cancel a subscription by writing a file entry"() {
        given:
        def feedUrl = 'http://bla.de'

        when:
        superfeedrMock.unsubscribe([new URL(feedUrl)], onResponseHandler)

        then:
        1 * fileBasedPersistence.save('updaterServiceFeedSubscriptionCanceled', feedUrl, feedUrl)
        1 * fileBasedPersistence.remove('updaterServiceFeedSubscription', feedUrl)
    }

    def "Get content of folder for the external subscription information"() {
        given:
        def feedUrl = 'http://kjnkjhkjn.de'

        when:
        def result = superfeedrMock.getSubscriptionList(onResponseHandler)

        then:
        result[0].toExternalForm() == feedUrl
        1 * fileBasedPersistence.getFolderContent('updaterServiceFeedSubscription') >> [feedUrl]
    }

    def "Register a notification handler and start listener"() {
        given:
        def notificationHandler = Mock(ServiceNotificationHandler)

        when:
        superfeedrMock.setMessageHandler(notificationHandler)

        then:
        1 * incomingFeedListenerMock.setNotificationHandler(notificationHandler)
        1 * incomingFeedListenerMock.startListener()
    }
}
