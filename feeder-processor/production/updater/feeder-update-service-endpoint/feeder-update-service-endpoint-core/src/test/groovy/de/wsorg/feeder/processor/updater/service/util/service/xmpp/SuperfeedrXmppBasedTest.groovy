package de.wsorg.feeder.processor.updater.service.util.service.xmpp

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppNotificationHandlerWrapper
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppResposeHandlingWrapper
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.wrapper.XmppSuperfeedrWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.05.13
 */
class SuperfeedrXmppBasedTest extends Specification {

    SuperfeedrXmppBased superfeedrXmppBased
    XmppSuperfeedrWrapper xmppSuperfeedrWrapper
    ServiceResponseHandler serviceResponseHandler

    def setup(){
        superfeedrXmppBased = new SuperfeedrXmppBased()

        xmppSuperfeedrWrapper = Mock(XmppSuperfeedrWrapper)
        serviceResponseHandler = Mock(ServiceResponseHandler)

        superfeedrXmppBased.xmppSuperfeedrWrapper = xmppSuperfeedrWrapper
    }

    def "Subscribe to a feed"() {
        given:
        def urls = [new URL('http://bla.blub')]

        when:
        superfeedrXmppBased.subscribe(urls, serviceResponseHandler)

        then:
        1 * xmppSuperfeedrWrapper.subscribe(urls, {it instanceof XmppResposeHandlingWrapper})
    }

    def "Unsubscribe from a feed"() {
        given:
        def urls = [new URL('http://bla.blub')]

        when:
        superfeedrXmppBased.unsubscribe(urls, serviceResponseHandler)

        then:
        1 * xmppSuperfeedrWrapper.unsubscribe(urls, {it instanceof XmppResposeHandlingWrapper})
    }

    def "Get subscription list"() {
        when:
        superfeedrXmppBased.getSubscriptionList(serviceResponseHandler)

        then:
        1 * xmppSuperfeedrWrapper.getSubscriptionList({it instanceof XmppResposeHandlingWrapper})
    }

    def "Set message handler"() {
        given:
        def messageHandler = Mock(ServiceNotificationHandler)

        when:
        superfeedrXmppBased.setMessageHandler(messageHandler)

        then:
        1 * xmppSuperfeedrWrapper.setMessageHandler({it instanceof XmppNotificationHandlerWrapper})
    }
}
