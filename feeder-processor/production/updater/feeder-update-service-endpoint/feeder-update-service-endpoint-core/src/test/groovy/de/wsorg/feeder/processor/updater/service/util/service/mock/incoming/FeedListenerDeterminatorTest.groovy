package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming;


import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.03.13
 */
public class FeedListenerDeterminatorTest extends Specification {
    FeedListenerDeterminator feedListenerDeterminator
    IncomingFeedListenerMock fileBasedListener;
    IncomingFeedListenerMock pollingBasedFeedUpdater;

    def setup(){
        feedListenerDeterminator = new FeedListenerDeterminator()

        fileBasedListener = Mock(IncomingFeedListenerMock)
        pollingBasedFeedUpdater = Mock(IncomingFeedListenerMock)

        feedListenerDeterminator.fileBasedFeedListener = fileBasedListener
        feedListenerDeterminator.pollingBasedFeedUpdater = pollingBasedFeedUpdater
    }

    def "Get a listener when the property is set to true"() {
        given:
        feedListenerDeterminator.updateFeedsUsingLocalPolling = 'true'

        when:
        def result = feedListenerDeterminator.getIncomingFeedListenerMock()

        then:
        result == pollingBasedFeedUpdater
    }

    def "Get a listener when the property is set to false"() {
        given:
        feedListenerDeterminator.updateFeedsUsingLocalPolling = 'false'

        when:
        def result = feedListenerDeterminator.getIncomingFeedListenerMock()

        then:
        result == fileBasedListener
    }
}
