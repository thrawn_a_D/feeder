package de.wsorg.feeder.processor.updater.service.util.testsupport.eventlistener

import de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventCollector
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.Signature
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class AopEventMapperTest extends Specification {
    AopEventMapper aopEventMapper
    EventCollector eventCollector

    def joinPoint = Mock(JoinPoint)
    def signature = Mock(Signature)

    def setup(){
        aopEventMapper = new AopEventMapper()
        eventCollector = Mock(EventCollector)
        aopEventMapper.eventCollector = eventCollector

        joinPoint.getSignature() >> signature
    }

    def "Map sendSubscribedFeedListToComponents to an event"() {
        given:
        def methodName = 'sendSubscribedFeedListToComponents'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(ServiceEndPointEventConstants.SUBSCRIBED_FEED_LIST_PUBLISHED_TO_COMPONENTS)
    }

    def "Map sendFeedUpdateToUpdaterModules to an event"() {
        given:
        def methodName = 'sendFeedUpdateToUpdaterModules'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(ServiceEndPointEventConstants.SEND_UPDATED_FEED_TO_UPDATER_MODULES)
    }

    def "Map subscribe to an event"() {
        given:
        def methodName = 'subscribe'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(ServiceEndPointEventConstants.SUBSCRIBE_FEED_TO_SUPERFEEDR)
    }

    def "Map unsubscribe to an event"() {
        given:
        def methodName = 'unsubscribe'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(ServiceEndPointEventConstants.UNSUBSCRIBE_FEED_FROM_SUPERFEEDR)
    }

    def "Exception is occurred"() {
        given:
        def exception = Mock(Exception)

        when:
        aopEventMapper.exceptionEventOccurred(exception)

        then:
        1 * eventCollector.exceptionEventOccurred(exception)
    }
}