package de.wsorg.feeder.processor.updater.service.util.service.handler.notification

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.05.13
 */
class NotificationServiceTerminatorTest extends Specification {
    ServiceNotificationHandler xmppNotificationHandler
    ServiceNotificationHandler syndFeedNotificationHandler

    NotificationServiceTerminator notificationServiceTerminator

    def setup(){
        notificationServiceTerminator = new NotificationServiceTerminator()

        xmppNotificationHandler = Mock(ServiceNotificationHandler)
        syndFeedNotificationHandler = Mock(ServiceNotificationHandler)

        notificationServiceTerminator.xmppNotificationHandler = xmppNotificationHandler
        notificationServiceTerminator.syndFeedNotificationHandler = syndFeedNotificationHandler
    }

    def "Determine service handler when protocol is set to xmpp"() {
        given:
        def protocol = 'xmpp'

        and:
        notificationServiceTerminator.serviceProtocol = protocol

        when:
        def result = notificationServiceTerminator.determineNotificationHandler()

        then:
        result == xmppNotificationHandler
    }

    def "Determine service handler when protocol is set to pubsub"() {
        given:
        def protocol = 'pubsub'

        and:
        notificationServiceTerminator.serviceProtocol = protocol

        when:
        def result = notificationServiceTerminator.determineNotificationHandler()

        then:
        result == syndFeedNotificationHandler
    }

    def "Determine service handler when protocol is set to something unspecific"() {
        given:
        def protocol = 'asdasdasd'

        and:
        notificationServiceTerminator.serviceProtocol = protocol

        when:
        def result = notificationServiceTerminator.determineNotificationHandler()

        then:
        result == xmppNotificationHandler
    }
}
