package de.wsorg.feeder.processor.updater.service.util.service

import de.wsorg.feeder.processor.updater.service.util.service.mock.SuperfeedrMock
import de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.SuperfeedrPubSubBased
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.SuperfeedrXmppBased
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.03.13
 */
class FeedsUpdateServiceFactoryTest extends Specification {

    FeedsUpdateServiceFactory feedsUpdateServiceFactory
    SuperfeedrXmppBased xmppSuperfeedrWrapper
    SuperfeedrPubSubBased superfeedrPubSubBased
    SuperfeedrMock superfeedrMock

    def setup(){
        feedsUpdateServiceFactory = new FeedsUpdateServiceFactory()

        xmppSuperfeedrWrapper = Mock(SuperfeedrXmppBased)
        superfeedrPubSubBased = Mock(SuperfeedrPubSubBased)

        superfeedrMock = Mock(SuperfeedrMock)
        feedsUpdateServiceFactory.superfeedrXmppBased = xmppSuperfeedrWrapper
        feedsUpdateServiceFactory.superfeedrPubSubBased = superfeedrPubSubBased
        feedsUpdateServiceFactory.superfeedrMock = superfeedrMock
    }

    def "Provide a pubsub instance when no protocol is defined"() {
        when:
        def result = feedsUpdateServiceFactory.getFeedsUpdateService()

        then:
        result == xmppSuperfeedrWrapper
    }

    def "Provide xmpp based service when property defines xmpp"() {
        given:
        def serviceProtocolProperty = 'xmpp'

        and:
        feedsUpdateServiceFactory.serviceProtocol = serviceProtocolProperty

        when:
        def result = feedsUpdateServiceFactory.getFeedsUpdateService()

        then:
        result == xmppSuperfeedrWrapper
    }

    def "Provide pubsub based service when property defines pubsub"() {
        given:
        def serviceProtocolProperty = 'pubsub'

        and:
        feedsUpdateServiceFactory.serviceProtocol = serviceProtocolProperty

        when:
        def result = feedsUpdateServiceFactory.getFeedsUpdateService()

        then:
        result == superfeedrPubSubBased
    }

    def "Service supposed to be mocked, provide a mocked instance"() {
        given:
        def serviceProtocolProperty = 'mocked'

        and:
        feedsUpdateServiceFactory.serviceProtocol = serviceProtocolProperty

        when:
        def result = feedsUpdateServiceFactory.getFeedsUpdateService()

        then:
        result == superfeedrMock
    }

    def "Provide an invalid setting for mocking property, provide a mock"() {
        given:
        def serviceProtocolProperty = 'asddasddasd'

        and:
        feedsUpdateServiceFactory.serviceProtocol = serviceProtocolProperty

        when:
        def result = feedsUpdateServiceFactory.getFeedsUpdateService()

        then:
        result == superfeedrMock
    }
}
