package de.wsorg.feeder.processor.updater.service.util.service.pubsubhubbub.notification;


import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.05.13
 */
public class SyndFeedNotificationHandlerTest extends Specification {
    SyndFeedNotificationHandler syndFeedNotificationHandler
    SuperfeedrNotificationProcessor superfeedrNotificationProcessor

    def setup(){
        syndFeedNotificationHandler = new SyndFeedNotificationHandler()

        superfeedrNotificationProcessor = Mock(SuperfeedrNotificationProcessor)

        syndFeedNotificationHandler.superfeedrNotificationProcessor = superfeedrNotificationProcessor
    }

    def "Delegate processing"() {
        given:
        def syndFeed = Mock(SyndFeed)

        when:
        syndFeedNotificationHandler.onNotification(syndFeed)

        then:
        1 * superfeedrNotificationProcessor.processNotification(syndFeed)
    }
}
