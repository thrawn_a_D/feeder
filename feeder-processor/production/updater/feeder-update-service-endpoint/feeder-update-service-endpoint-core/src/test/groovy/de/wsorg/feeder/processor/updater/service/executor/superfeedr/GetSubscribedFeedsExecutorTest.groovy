package de.wsorg.feeder.processor.updater.service.executor.superfeedr

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.service.util.communication.UpdateComponentsCommunicator
import de.wsorg.feeder.processor.updater.service.util.service.FeedsUpdateService
import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceResponseHandler
import spock.lang.Specification
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class GetSubscribedFeedsExecutorTest extends Specification {

    GetSubscribedFeedsExecutor getSubscribedFeedsExecutor
    FeedsUpdateService feedsUpdateService
    ServiceResponseHandler defaultResponseHandler
    UpdateComponentsCommunicator updateComponentsCommunicator

    def setup(){
        getSubscribedFeedsExecutor = new GetSubscribedFeedsExecutor()
        defaultResponseHandler = Mock(ServiceResponseHandler)
        feedsUpdateService = Mock(FeedsUpdateService)
        updateComponentsCommunicator = Mock(UpdateComponentsCommunicator)


        getSubscribedFeedsExecutor.feedsUpdateService = feedsUpdateService
        getSubscribedFeedsExecutor.defaultResponseHandler = defaultResponseHandler
        getSubscribedFeedsExecutor.updateComponentsCommunicator = updateComponentsCommunicator
    }

    def "Get a list of subscriptions and post them into the queue as response"() {
        given:
        def updaterMessage = new StringJobMessage('')

        and:
        def feedUrl = "http://fgeed.ad"
        def expectedUrls = [new URL(feedUrl)]

        when:
        getSubscribedFeedsExecutor.execute(updaterMessage)

        then:
        1 * feedsUpdateService.getSubscriptionList(defaultResponseHandler) >> expectedUrls
        1 * updateComponentsCommunicator.sendSubscribedFeedListToComponents([feedUrl])
    }

    def "If no subscriptions are on superfeedr, do not post a message to update components"() {
        given:
        def updaterMessage = new StringJobMessage('')

        when:
        getSubscribedFeedsExecutor.execute(updaterMessage)

        then:
        1 * feedsUpdateService.getSubscriptionList(defaultResponseHandler) >> []
        0 * updateComponentsCommunicator.sendSubscribedFeedListToComponents(_)
    }
}
