package de.wsorg.feeder.processor.updater.service.util.config

import de.wsorg.feeder.processor.updater.service.util.service.mock.incoming.FeedListenerDeterminator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.03.13
 */
class SuperfeedrConfigVerifierTest extends Specification {
    SuperfeedrConfigVerifier feederUpdaterConfigVerifier
    def configLocation = [''] as String[]

    def setup(){
        feederUpdaterConfigVerifier = new SuperfeedrConfigVerifier()
    }

    def "Verify all mandatory properties are valid"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_HOST, 'bla.com')

        when:
        def result = feederUpdaterConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Username to superfeedr is not set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME, '')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_HOST, 'bla.com')

        when:
        def result = feederUpdaterConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME] == ['Username for superfeedr is not set.']
        result.isConfigErroneous()
    }

    def "Password to superfeedr is not set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD, '')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_HOST, 'bla.com')

        when:
        def result = feederUpdaterConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD] == ['Password for superfeedr is not set.']
        result.isConfigErroneous()
    }

    def "Host to superfeedr is not set"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_HOST, '')

        when:
        def result = feederUpdaterConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[SuperfeedrConfigVerifier.SUPERFEEDR_HOST] == ['Host for superfeedr is not set.']
        result.isConfigErroneous()
    }

    def "Verify optional property update feed polling"() {
        given:
        def properties = new Properties()
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_USERNAME, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_PASSWORD, 'asd')
        properties.put(SuperfeedrConfigVerifier.SUPERFEEDR_HOST, 'asd')
        properties.put(FeedListenerDeterminator.UPDATE_FEEDS_USING_LOCAL_POLLING, 'kjf')

        when:
        def result = feederUpdaterConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors[FeedListenerDeterminator.UPDATE_FEEDS_USING_LOCAL_POLLING] == ['Please provide a valid boolean value']
        result.isConfigErroneous()
    }
}
