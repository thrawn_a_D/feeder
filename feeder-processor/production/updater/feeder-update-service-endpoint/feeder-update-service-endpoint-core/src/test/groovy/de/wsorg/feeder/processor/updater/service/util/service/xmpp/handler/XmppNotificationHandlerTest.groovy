package de.wsorg.feeder.processor.updater.service.util.service.xmpp.handler

import de.wsorg.feeder.processor.updater.service.util.service.handler.notification.SuperfeedrNotificationProcessor
import de.wsorg.feeder.processor.updater.service.util.service.xmpp.notification.XmppNotificationHandler
import org.superfeedr.extension.notification.SuperfeedrEventExtension
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class XmppNotificationHandlerTest extends Specification {
    XmppNotificationHandler superfeedrNotificationHandler
    SuperfeedrNotificationProcessor superfeedrNotificationProcessor

    def setup(){
        superfeedrNotificationHandler = new XmppNotificationHandler()

        superfeedrNotificationProcessor = Mock(SuperfeedrNotificationProcessor)

        superfeedrNotificationHandler.superfeedrNotificationProcessor = superfeedrNotificationProcessor
    }


    def "Receive a message and delegate the processing"() {
        given:
        def messageToHandle = Mock(SuperfeedrEventExtension)

        when:
        superfeedrNotificationHandler.onNotification(messageToHandle)

        then:
        1 * superfeedrNotificationProcessor.processNotification(messageToHandle)
    }
}