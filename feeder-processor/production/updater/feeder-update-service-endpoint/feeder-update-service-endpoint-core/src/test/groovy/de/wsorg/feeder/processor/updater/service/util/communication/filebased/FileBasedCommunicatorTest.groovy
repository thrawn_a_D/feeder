package de.wsorg.feeder.processor.updater.service.util.communication.filebased

import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class FileBasedCommunicatorTest extends Specification {
    FileBasedCommunicator fileBasedCommunicator
    FileBasedPersistence fileBasedPersistence

    def setup(){
        fileBasedCommunicator = new FileBasedCommunicator()

        fileBasedPersistence = Mock(FileBasedPersistence)

        fileBasedCommunicator.fileBasedPersistence = fileBasedPersistence
    }

    def "Store feed urls to storage"() {
        given:
        def feedUrls = ['http://asfasd.de', 'http://sdkj.de']

        when:
        fileBasedCommunicator.sendSubscribedFeedListToComponents(feedUrls)

        then:
        1 * fileBasedPersistence.save('updaterService_getAllSubscriptionsResponseQueue',
                feedUrls.toString(),
                {StringJobMessage it -> it.identifier == feedUrls.toString() && it.content == feedUrls.toString()})
    }

    def "Write json feed data to storage"() {
        given:
        def jsonData = '{json}'

        when:
        fileBasedCommunicator.sendFeedUpdateToUpdaterModules(jsonData)

        then:
        1 * fileBasedPersistence.save('updaterService_feedUpdateQueue', jsonData,
                {StringJobMessage it -> it.identifier == jsonData && it.content == jsonData})
    }
}
