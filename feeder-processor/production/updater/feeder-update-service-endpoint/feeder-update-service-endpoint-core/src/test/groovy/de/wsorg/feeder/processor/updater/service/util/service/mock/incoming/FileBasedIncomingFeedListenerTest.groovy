package de.wsorg.feeder.processor.updater.service.util.service.mock.incoming

import de.wsorg.feeder.processor.updater.service.util.service.handler.ServiceNotificationHandler
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import org.superfeedr.extension.notification.SuperfeedrEventExtension
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.03.13
 */
class FileBasedIncomingFeedListenerTest extends Specification {
    FileBasedIncomingFeedListener fileBasedIncomingFeedListener
    FileBasedPersistence fileBasedPersistence

    def setup(){
        fileBasedIncomingFeedListener = new FileBasedIncomingFeedListener()
        fileBasedPersistence = Mock(FileBasedPersistence)
        fileBasedIncomingFeedListener.fileBasedPersistence = fileBasedPersistence
    }

    def "Start a listener and wait for incoming feeds"() {
        given:
        def notificationHandler = Mock(ServiceNotificationHandler)

        def feedUrl = 'http://myfeed.de/feed'
        def entryId = 'http://myfeed.de/feed#1'
        def entryLink = 'http://myfeed.de/feed#link'
        def entryLinkType = 'http/html'
        def entryPublished = new Date()
        def entryUpdated = new Date()
        def entryTitle = 'title'
        def entrySummary = 'summary'
        def entryContent = 'kjkuuk'

        def mapOfValues = ['FEED_URL':feedUrl,
                'ENTRY_ID':entryId,
                'ENTRY_LINK':entryLink,
                'ENTRY_LINK_TYPE':entryLinkType,
                'ENTRY_PUBLISHED':entryPublished,
                'ENTRY_UPDATED':entryUpdated,
                'ENTRY_TITLE':entryTitle,
                'ENTRY_SUMMARY':entrySummary,
                'ENTRY_CONTENT':entryContent]

        when:
        fileBasedIncomingFeedListener.setNotificationHandler(notificationHandler)
        fileBasedIncomingFeedListener.startListener()
        sleep 500

        then:
        1 * fileBasedPersistence.getFolderContent('externalServiceIncomingSubscriptions') >> [mapOfValues]
        1 * notificationHandler.onNotification({SuperfeedrEventExtension it -> it.status.feedURL == feedUrl &&
                it.items.items.next().entry.id == entryId &&
                it.items.items.next().entry.link == entryLink &&
                it.items.items.next().entry.linkType == entryLinkType &&
                it.items.items.next().entry.published == entryPublished &&
                it.items.items.next().entry.updated == entryUpdated &&
                it.items.items.next().entry.title == entryTitle &&
                it.items.items.next().entry.summary == entrySummary &&
                it.items.items.next().entry.content == entryContent})
    }

    def "No handler provided, no start is allowed"() {
        when:
        fileBasedIncomingFeedListener.startListener()

        then:
        0 * fileBasedPersistence.getFolderContent(_)
    }
}
