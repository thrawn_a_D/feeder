package de.wsorg.feeder.processor.updater.service.util.testsupport

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.04.13
 */
class FeederUpdateServiceContextFactoryTest extends Specification {
    def "Init context"() {
        when:
        FeederUpdateServiceContextFactory.initContext()

        then:
        true
    }
}
