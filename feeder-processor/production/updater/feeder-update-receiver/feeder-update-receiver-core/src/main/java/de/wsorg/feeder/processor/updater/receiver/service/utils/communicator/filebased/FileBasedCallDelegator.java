package de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.filebased;

import javax.inject.Inject;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.ServiceComponentCommunicator;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedCallDelegator implements ServiceComponentCommunicator {

    public static String CANCEL_UPDATING_MESSAGE_FOLDER = "updateService_canceledSubscriptionQueue";

    @Inject
    private FileBasedPersistence fileBasedPersistence;

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        writeMessageToFile(feedUrl, CANCEL_UPDATING_MESSAGE_FOLDER);
    }

    private void writeMessageToFile(final String feedUrl,
                                    final String storageFolderName) {
        final UpdaterJobMessage jobMessage = new UpdaterJobMessage();
        jobMessage.setIdentifier(feedUrl);
        fileBasedPersistence.save(storageFolderName, feedUrl, jobMessage);
    }
}
