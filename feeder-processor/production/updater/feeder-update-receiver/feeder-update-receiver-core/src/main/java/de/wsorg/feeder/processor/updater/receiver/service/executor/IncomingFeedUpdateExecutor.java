package de.wsorg.feeder.processor.updater.receiver.service.executor;

import javax.inject.Inject;

import com.google.gson.Gson;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.StringMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;
import de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.ServiceComponentCommunicator;
import de.wsorg.feeder.processor.updater.receiver.service.utils.feed.FeedUpdater;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
@Slf4j
public class IncomingFeedUpdateExecutor implements StringMessageJobExecutor {
    @Inject
    private ServiceComponentCommunicator componentCommunicator;
    @Inject
    private UpdaterStorageProvider updaterStorageProvider;
    @Inject
    private FeedUpdater feedUpdater;

    @Override
    public void execute(final StringJobMessage message) {
        log.debug("Got feed update to be processed.");

        try {
            FeedModel feedModel = encodeJsonModel(message);
            FeedModel availableFeedModel = updaterStorageProvider.getFeedDataAccess().findByFeedUrl(feedModel.getFeedUrl());

            if (availableFeedModel != null) {
                feedUpdater.updateIncomingFeed(feedModel, availableFeedModel);
            } else {
                IncomingFeedUpdateExecutor.log.error("Was trying to apply an incoming feed update but failed. No local feed found in storage to update: {}", feedModel.getFeedUrl());
                IncomingFeedUpdateExecutor.log.debug("Delegate feed for subscription cancellation: {}", feedModel.getFeedUrl());
                componentCommunicator.cancelFeedUpdating(feedModel.getFeedUrl());
            }
        } catch (Exception e) {
            IncomingFeedUpdateExecutor.log.error("Error while updating feed! Provided json was: {}", message, e);
        }
    }

    private FeedModel encodeJsonModel(final StringJobMessage message) {
        Gson gson = new Gson();
        log.debug("Start decoding model data from json.");
        FeedModel feedModel = gson.fromJson(message.getContent(), FeedModel.class);
        log.debug("Feed encoding successfully.");
        return feedModel;
    }
}
