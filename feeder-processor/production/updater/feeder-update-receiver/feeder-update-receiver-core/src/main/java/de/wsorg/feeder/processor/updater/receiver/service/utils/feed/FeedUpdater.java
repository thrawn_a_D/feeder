package de.wsorg.feeder.processor.updater.receiver.service.utils.feed;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public interface FeedUpdater {
    void updateIncomingFeed(final FeedModel incomingFeedModel, final FeedModel availableFeedModel);
}
