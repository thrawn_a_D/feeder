package de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.activemq;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator;
import de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.ServiceComponentCommunicator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ActiveMQFeedRegistrator implements ServiceComponentCommunicator {

    @Inject
    private ApplicationContext applicationContext;
    @Inject
    @Named("destinationForUpdaterServiceCanceledSubscription")
    private ActiveMQQueue subscriptionCanceledQueue;

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        putMessageIntoQueue(feedUrl, UpdateManagerAction.CANCEL_UPDATE_OF_FEED, subscriptionCanceledQueue);
    }

    private void putMessageIntoQueue(final String feedUrl,
                                       final UpdateManagerAction action,
                                       final ActiveMQQueue queue) {
        UpdateManagerMessageCreator messageCreator = new UpdateManagerMessageCreator(feedUrl, action);
        final JmsTemplate jmsTemplate = getJmsTemplate();
        jmsTemplate.send(queue, messageCreator);
    }

    private JmsTemplate getJmsTemplate(){
        return applicationContext.getBean(JmsTemplate.class);
    }
}
