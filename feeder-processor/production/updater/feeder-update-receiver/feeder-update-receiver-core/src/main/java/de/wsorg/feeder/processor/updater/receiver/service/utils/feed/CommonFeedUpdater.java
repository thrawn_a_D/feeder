package de.wsorg.feeder.processor.updater.receiver.service.utils.feed;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;

import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
@Slf4j
@Named
public class CommonFeedUpdater implements FeedUpdater {
    @Inject
    private UpdaterStorageProvider updaterStorageProvider;

    @Override
    public void updateIncomingFeed(final FeedModel incomingFeedModel, final FeedModel availableFeedModel) {
        for (FeedEntryModel feedEntryModel : incomingFeedModel.getFeedEntries()) {
            if (!availableFeedModel.containsFeedEntry(feedEntryModel.getId())) {
                CommonFeedUpdater.log.debug("Adding new feed entry {} to feed: {}", feedEntryModel.getId(), feedEntryModel.getFeedUrl());
                availableFeedModel.addFeedEntryOnTop(feedEntryModel);
            }
        }

        updaterStorageProvider.getFeedDataAccess().save(availableFeedModel);
        CommonFeedUpdater.log.debug("Updating of feed {} finished", incomingFeedModel.getFeedUrl());
    }
}
