package de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport.eventlistener;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public class UpdateReceiverEventConstants {
    public static final String SAVE_UPDATED_FEED = "SAVE_UPDATED_FEED";
    public static final String UNSUBSCRIBE_FEED_FROM_SUPERFEEDR = "UNSUBSCRIBE_FEED_FROM_SUPERFEEDR";
}
