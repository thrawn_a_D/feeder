package de.wsorg.feeder.processor.updater.receiver.service.utils.communicator;

import org.springframework.beans.factory.annotation.Value;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ComponentCommunicatorDeterminator {
    private ServiceComponentCommunicator fileBasedComponentCommunicator;
    private ServiceComponentCommunicator activeMqBasedComponentCommunicator;

    @Value("${useFileSystemAsStorage}")
    private String useFileSystemAsStorage;

    public ServiceComponentCommunicator getComponentCommunicator() {
        if (Boolean.valueOf(useFileSystemAsStorage)) {
            return  fileBasedComponentCommunicator;
        } else {
            return activeMqBasedComponentCommunicator;
        }
    }

    public void setFileBasedComponentCommunicator(final ServiceComponentCommunicator fileBasedComponentCommunicator) {
        this.fileBasedComponentCommunicator = fileBasedComponentCommunicator;
    }

    public void setActiveMqBasedComponentCommunicator(final ServiceComponentCommunicator activeMqBasedComponentCommunicator) {
        this.activeMqBasedComponentCommunicator = activeMqBasedComponentCommunicator;
    }
}
