package de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeederListenerContextFactory {
    private static ClassPathXmlApplicationContext applicationContext;

    public static ApplicationContext initContext(){
        final String[] configLocations = {"classpath:spring/update-receiver-base-context.xml"};
        applicationContext = new ClassPathXmlApplicationContext(configLocations);
        return applicationContext;
    }
}
