package de.wsorg.feeder.processor.updater.receiver.service;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ExternalUpdateReceiver {
    void setupListener();
}
