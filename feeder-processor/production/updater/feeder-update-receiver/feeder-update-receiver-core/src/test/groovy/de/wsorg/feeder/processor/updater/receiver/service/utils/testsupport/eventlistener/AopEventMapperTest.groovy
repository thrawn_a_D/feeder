package de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport.eventlistener

import de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventCollector
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.Signature
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class AopEventMapperTest extends Specification {
    AopEventMapper aopEventMapper
    EventCollector eventCollector

    def joinPoint = Mock(JoinPoint)
    def signature = Mock(Signature)

    def setup(){
        aopEventMapper = new AopEventMapper()
        eventCollector = Mock(EventCollector)
        aopEventMapper.eventCollector = eventCollector

        joinPoint.getSignature() >> signature
    }

    def "Map cancelFeedUpdating to an event"() {
        given:
        def methodName = 'cancelFeedUpdating'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(UpdateReceiverEventConstants.UNSUBSCRIBE_FEED_FROM_SUPERFEEDR)
    }

    def "Map updateIncomingFeed to an event"() {
        given:
        def methodName = 'updateIncomingFeed'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(UpdateReceiverEventConstants.SAVE_UPDATED_FEED)
    }

    def "Exception is occurred"() {
        given:
        def exception = Mock(Exception)

        when:
        aopEventMapper.exceptionEventOccurred(exception)

        then:
        1 * eventCollector.exceptionEventOccurred(exception)
    }
}