package de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.filebased

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.05.13
 */
class FileBasedCallDelegatorTest extends Specification {
    FileBasedCallDelegator feedRegistrator
    FileBasedPersistence fileBasedPersistence

    def setup(){
        feedRegistrator = new FileBasedCallDelegator()

        fileBasedPersistence = Mock(FileBasedPersistence)

        feedRegistrator.fileBasedPersistence = fileBasedPersistence
    }

    def "Cancel updating of a feed by sending a message by file"() {
        given:
        def feedUrl = 'kjbkgu'

        when:
        feedRegistrator.cancelFeedUpdating(feedUrl)

        then:
        1 * fileBasedPersistence.save('updateService_canceledSubscriptionQueue', feedUrl, {UpdaterJobMessage it -> it.identifier == feedUrl})
    }
}
