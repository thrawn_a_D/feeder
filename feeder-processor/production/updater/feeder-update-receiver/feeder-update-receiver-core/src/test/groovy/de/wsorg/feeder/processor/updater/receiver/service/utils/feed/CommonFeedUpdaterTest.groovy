package de.wsorg.feeder.processor.updater.receiver.service.utils.feed

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class CommonFeedUpdaterTest extends Specification {

    CommonFeedUpdater commonFeedUpdater
    UpdaterStorageProvider updaterStorageProvider
    FeedDataAccess feedDataAccess

    def setup(){
        commonFeedUpdater = new CommonFeedUpdater()
        updaterStorageProvider = Mock(UpdaterStorageProvider)
        feedDataAccess = Mock(FeedDataAccess)

        commonFeedUpdater.updaterStorageProvider = updaterStorageProvider

        updaterStorageProvider.getFeedDataAccess() >> feedDataAccess
    }

    def "Check if feed entry id not exist on feed add it to entry and store on db"() {
        given:
        def incomingFeedModel = new FeedModel()
        def availableFeedModel = new FeedModel()

        and:
        def feedUrl = 'blablub'
        def entryId = '123'
        incomingFeedModel.addFeedEntry(new FeedEntryModel(id: entryId, feedUrl: feedUrl))

        when:
        commonFeedUpdater.updateIncomingFeed(incomingFeedModel, availableFeedModel)

        then:
        1 * feedDataAccess.save({FeedModel it-> it.feedEntries[0].feedUrl == feedUrl &&
                                                it.feedEntries[0].id == entryId &&
                                                it.feedEntries.size() == 1})
    }

    def "Available feed already contains an entry, then skip it"() {
        given:
        def incomingFeedModel = new FeedModel()
        def availableFeedModel = new FeedModel()

        and:
        def feedUrl = 'blablub'
        def entryId = '123'
        availableFeedModel.addFeedEntry(new FeedEntryModel(id: entryId, feedUrl: feedUrl))
        incomingFeedModel.addFeedEntry(new FeedEntryModel(id: entryId, feedUrl: feedUrl))

        when:
        commonFeedUpdater.updateIncomingFeed(incomingFeedModel, availableFeedModel)

        then:
        1 * feedDataAccess.save({FeedModel it-> it.feedEntries.size() == 1})
    }
}
