package de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.activemq

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator
import org.apache.activemq.command.ActiveMQQueue
import org.springframework.context.ApplicationContext
import org.springframework.jms.core.JmsTemplate
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 21.05.13
 */
class ActiveMQFeedRegistratorTest extends Specification {
    ActiveMQFeedRegistrator activeMQFeedRegistrator
    ApplicationContext applicationContext
    JmsTemplate jmsTemplate
    ActiveMQQueue subscriptionCanceledQueue

    def setup(){
        activeMQFeedRegistrator = new ActiveMQFeedRegistrator()

        jmsTemplate = Mock(JmsTemplate)
        applicationContext = Mock(ApplicationContext)
        subscriptionCanceledQueue = Mock(ActiveMQQueue)

        applicationContext.getBean(JmsTemplate) >> jmsTemplate

        activeMQFeedRegistrator.applicationContext = applicationContext
        activeMQFeedRegistrator.subscriptionCanceledQueue = subscriptionCanceledQueue
    }

    def "Cancel feed update"() {
        given:
        def feedUrl = 'kjhkjh'

        when:
        activeMQFeedRegistrator.cancelFeedUpdating(feedUrl)

        then:
        1 * jmsTemplate.send(subscriptionCanceledQueue,
                {UpdateManagerMessageCreator it ->
                    it.feedUrl == feedUrl  &&
                            it.action == UpdateManagerAction.CANCEL_UPDATE_OF_FEED
                })
    }
}
