package de.wsorg.feeder.processor.updater.receiver.service.executor

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import de.wsorg.feeder.processor.updater.receiver.service.utils.communicator.ServiceComponentCommunicator
import de.wsorg.feeder.processor.updater.receiver.service.utils.feed.FeedUpdater
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class IncomingFeedUpdateExecutorTest extends Specification {
    IncomingFeedUpdateExecutor incomingFeedUpdateProcessor
    UpdaterStorageProvider updaterStorageProvider
    FeedDataAccess feedDataAccess
    ServiceComponentCommunicator componentCommunicator
    FeedUpdater feedUpdater

    def setup(){
        incomingFeedUpdateProcessor = new IncomingFeedUpdateExecutor()

        updaterStorageProvider = Mock(UpdaterStorageProvider)
        feedDataAccess = Mock(FeedDataAccess)
        componentCommunicator = Mock(ServiceComponentCommunicator)
        feedUpdater = Mock(FeedUpdater)

        updaterStorageProvider.getFeedDataAccess() >> feedDataAccess

        incomingFeedUpdateProcessor.updaterStorageProvider = updaterStorageProvider
        incomingFeedUpdateProcessor.componentCommunicator = componentCommunicator
        incomingFeedUpdateProcessor.feedUpdater = feedUpdater
    }

    def "Provide a text message to process, receive json data and store feed entry data in db"() {
        given:
        def feedUrl = 'http://feedUrl'
        def description = 'kjhkjb'
        def title = 'title'
        def id = 'id'

        def feedModelAsJson = """{"feedEntriesCollection":{"additionalPagingPossible":false},
"feedEntries":[{
"title":"${title}",
"id":"${id}",
"updated":"Apr 9, 2013 9:29:44 AM",
"feedUrl":"${feedUrl}",
"description":"${description}"}],
"feedUrl":"http://feedUrl"}"""

        and:
        def modelWrapped = new StringJobMessage(feedModelAsJson)
        
        and:
        def alreadyAvailableFeed = new FeedModel()
        alreadyAvailableFeed.addFeedEntry(new FeedEntryModel(id: '123', feedUrl: 'blablub'))

        when:
        incomingFeedUpdateProcessor.execute(modelWrapped)

        then:
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> alreadyAvailableFeed
        1 * feedUpdater.updateIncomingFeed({FeedModel it-> it.feedEntries[0].feedUrl == feedUrl &&
                                                it.feedEntries[0].description == description &&
                                                it.feedEntries[0].title == title &&
                                                it.feedEntries[0].id == id &&
                                                it.feedEntries[0].updated}, alreadyAvailableFeed)
    }

    def "No feed found in storage, skip updating, delegate subscription canceling"() {
        given:
        def feedUrl = 'http://feedUrl'
        def description = 'kjhkjb'
        def title = 'title'
        def id = 'id'

        def feedModelAsJson = """{"feedEntriesCollection":{"additionalPagingPossible":false},
"feedEntries":[{
"title":"${title}",
"id":"${id}",
"updated":"Apr 9, 2013 9:29:44 AM",
"feedUrl":"${feedUrl}",
"description":"${description}"}],
"feedUrl":"http://feedUrl"}"""

        and:
        def modelWrapped = new StringJobMessage(feedModelAsJson)

        when:
        incomingFeedUpdateProcessor.execute(modelWrapped)

        then:
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> null
        0 * feedDataAccess.save(_)
        1 * componentCommunicator.cancelFeedUpdating(feedUrl)
    }

    def "Provide an invalid json string"() {
        given:
        def invalidJson = 'lkhkjh'

        and:
        def modelWrapped = new StringJobMessage(invalidJson)

        when:
        incomingFeedUpdateProcessor.execute(modelWrapped)

        then:
        0 * feedDataAccess.findByFeedUrl(_)
        0 * feedDataAccess.save(_)
    }
}
