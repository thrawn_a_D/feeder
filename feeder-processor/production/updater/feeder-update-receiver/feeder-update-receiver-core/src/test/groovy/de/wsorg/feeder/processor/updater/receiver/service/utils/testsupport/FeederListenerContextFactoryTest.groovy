package de.wsorg.feeder.processor.updater.receiver.service.utils.testsupport

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 24.03.13
 */
class FeederListenerContextFactoryTest extends Specification {
    def "Initialize context"() {
        when:
        FeederListenerContextFactory.initContext()

        then:
        true
    }
}
