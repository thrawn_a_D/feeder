package de.wsorg.feeder.processor.updater.repository;

import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory;
import de.wsorg.feeder.processor.updater.repository.utils.config.RepositoryConfig;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeedRepositoryFactory {
    private static ClassPathXmlApplicationContext applicationContext;
    private static boolean configProvided=false;
    private static boolean useFileSystemAsStorage=false;

    public static FeedRepository getFeedRepository() {
        validateState();
        return getApplicationContext().getBean(FeedRepository.class);
    }

    public static UserFeedRepository getUserFeedRepository() {
        validateState();
        return getApplicationContext().getBean(UserFeedRepository.class);
    }

    private static ClassPathXmlApplicationContext getApplicationContext(){
        if (applicationContext == null) {
            final String[] configLocations;
            final String loadConfiguration="classpath:spring/feed-repository-load-context.xml";

            if(useFileSystemAsStorage) {
                final String jmsConfiguration="classpath:spring/feed-repository-filebased-context.xml";
                configLocations = new String[]{loadConfiguration, jmsConfiguration};

                FeedStorageFactory.enableFileBasedStorage = true;
            } else {
                final String jmsConfiguration="classpath:spring/feed-repository-jms-context.xml";
                configLocations = new String[]{loadConfiguration, jmsConfiguration};
            }

            applicationContext = new ClassPathXmlApplicationContext(configLocations);
        }

        return applicationContext;
    }

    private static void validateState() {
        if(!configProvided)
            throw new IllegalStateException("Please provide a valid storage configuration first");
    }

    public static void setUseFileSystemAsStorage(final boolean useFileSystemAsStorage) {
        FeedRepositoryFactory.useFileSystemAsStorage = useFileSystemAsStorage;
    }

    public static void setClientConfig(final RepositoryConfig repositoryConfig) {
        if (!configProvided) {
            if(!getApplicationContext().containsBean("repositoryConfig")) {
                getApplicationContext().getBeanFactory().registerSingleton("repositoryConfig", repositoryConfig);
            }

            if(!getApplicationContext().containsBean("storageConfig")) {
                getApplicationContext().getBeanFactory().registerSingleton("storageConfig", repositoryConfig.getStorageConfig());
            }

            configProvided = true;
        }
    }
}
