package de.wsorg.feeder.processor.updater.repository.utils.registrator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedRegistrator {
    void registerFeed(final String feedUrl);
    void cancelFeedUpdating(final String feedUrl);
}
