package de.wsorg.feeder.processor.updater.repository.utils.storage;

import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedStorageFactory;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedStorageProxy {
    @Inject
    private ApplicationContext applicationContext;

    private FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess;
    private FeedDataAccess feedDataAccess;
    private UsersFeedsStorage usersFeedsStorage;

    public FeedLoadStatisticsDataAccess getFeedLoadStatisticsDataAccess() {
        if (feedLoadStatisticsDataAccess == null) {
            final StorageConfig storageConfig = applicationContext.getBean(StorageConfig.class);

            FeedStorageFactory.setClientConfig(storageConfig);
            feedLoadStatisticsDataAccess = FeedStorageFactory.getFeedLoadStatisticsDataAccess();
        }

        return feedLoadStatisticsDataAccess;
    }

    public FeedDataAccess getFeedDataAccess() {
        if (feedDataAccess == null) {
            final StorageConfig storageConfig = applicationContext.getBean(StorageConfig.class);

            FeedStorageFactory.setClientConfig(storageConfig);
            feedDataAccess = FeedStorageFactory.getFeedDAO();
        }

        return feedDataAccess;
    }

    public UsersFeedsStorage getUsersFeedsStorage() {
        if (usersFeedsStorage == null) {
            final StorageConfig storageConfig = applicationContext.getBean(StorageConfig.class);

            FeedStorageFactory.setClientConfig(storageConfig);
            usersFeedsStorage = FeedStorageFactory.getUserFeedsDAO();
        }

        return usersFeedsStorage;
    }
}
