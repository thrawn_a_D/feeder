package de.wsorg.feeder.processor.updater.repository.utils.loader;

import javax.inject.Named;

import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoaderFactory;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedLoaderProxy {
    public ExternalFeedLoader getFeedLoader() {
        return ExternalFeedLoaderFactory.getFeedLoader();
    }
}
