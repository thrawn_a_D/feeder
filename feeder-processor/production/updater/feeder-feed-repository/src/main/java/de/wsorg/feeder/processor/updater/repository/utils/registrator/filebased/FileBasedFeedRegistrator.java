package de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased;

import javax.inject.Inject;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.repository.utils.registrator.FeedRegistrator;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedFeedRegistrator implements FeedRegistrator {

    public static String UPDATE_MESSAGE_FOLDER = "updateManager_incomingUpdateSubscriptionQueue";
    public static String CANCEL_UPDATING_MESSAGE_FOLDER = "updateManager_subscriptionCanceledQueue";

    @Inject
    private FileBasedPersistence<UpdaterJobMessage> fileBasedPersistence;

    @Override
    public void registerFeed(final String feedUrl) {
        writeMessageToFile(feedUrl, UpdateManagerAction.REGISTER_FEED_FOR_UPDATE, UPDATE_MESSAGE_FOLDER);
    }

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        writeMessageToFile(feedUrl, UpdateManagerAction.CANCEL_UPDATE_OF_FEED, CANCEL_UPDATING_MESSAGE_FOLDER);
    }

    private void writeMessageToFile(final String feedUrl,
                                    final UpdateManagerAction registerFeedForUpdate,
                                    final String storageFolderName) {
        final UpdaterJobMessage message = new UpdaterJobMessage();

        message.setIdentifier(feedUrl);
        message.setAction(registerFeedForUpdate);

        fileBasedPersistence.save(storageFolderName, feedUrl, message);
    }
}
