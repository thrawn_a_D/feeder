package de.wsorg.feeder.processor.updater.repository;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.processor.updater.repository.utils.loader.FeedLoaderProxy;
import de.wsorg.feeder.processor.updater.repository.utils.registrator.FeedRegistrator;
import de.wsorg.feeder.processor.updater.repository.utils.storage.FeedStorageProxy;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Slf4j
public class SimpleFeedRepository implements FeedRepository {

    @Inject
    private FeedStorageProxy feedStorageProxy;
    @Inject
    private FeedLoaderProxy feedLoaderProxy;
    @Inject
    private FeedRegistrator feedRegistrator;

    @Override
    public FeedModel loadFeed(final String feedUrl) {
        log.debug("Loading feed {}", feedUrl);
        final FeedModel loadedFeed;
        final FeedLoadStatistic feedLoadStatistic = getFeedLoadStatisticsDataAccess().getFeedLoadStatistics(feedUrl);

        if (feedLoadStatistic != null) {
            log.debug("Feed {} already has an update subscription going. Get data from local storage.", feedUrl);
            feedLoadStatistic.setLastLoaded(new Date());
            getFeedLoadStatisticsDataAccess().updateFeedLoadStatistics(feedLoadStatistic);
            loadedFeed = getFeedDataAccess().findByFeedUrl(feedUrl);
        } else {
            log.debug("Feed {} has no update subscription yet. Register feed for update.", feedUrl);
            loadedFeed = feedLoaderProxy.getFeedLoader().loadFeed(feedUrl);
            feedRegistrator.registerFeed(feedUrl);
            getFeedLoadStatisticsDataAccess().addNewFeedLoadStatistic(feedUrl);
            getFeedDataAccess().save(loadedFeed);
        }


        return loadedFeed;
    }

    @Override
    public void informThatUserCanceledSubscription(final String feedUrl) {
        this.feedRegistrator.cancelFeedUpdating(feedUrl);
    }

    private FeedLoadStatisticsDataAccess getFeedLoadStatisticsDataAccess() {
        return feedStorageProxy.getFeedLoadStatisticsDataAccess();
    }

    private FeedDataAccess getFeedDataAccess() {
        return feedStorageProxy.getFeedDataAccess();
    }
}
