package de.wsorg.feeder.processor.updater.repository;

import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserFeedRepository {
    List<UserRelatedFeedModel> loadUserFeeds(final String userId);
    List<UserRelatedFeedModel> loadUserFeeds(final String userId, final String titleQuery);
}
