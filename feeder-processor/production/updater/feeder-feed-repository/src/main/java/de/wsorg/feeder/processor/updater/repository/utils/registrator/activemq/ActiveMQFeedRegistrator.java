package de.wsorg.feeder.processor.updater.repository.utils.registrator.activemq;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator;
import de.wsorg.feeder.processor.updater.repository.utils.registrator.FeedRegistrator;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ActiveMQFeedRegistrator implements FeedRegistrator {

    @Inject
    private ApplicationContext applicationContext;
    @Inject
    @Named("destinationForIncomingSubscription")
    private ActiveMQQueue incomingSubscriptionQueue;
    @Inject
    @Named("destinationForCanceledSubscription")
    private ActiveMQQueue subscriptionCanceledQueue;

    @Override
    public void registerFeed(final String feedUrl) {
        putMessageIntoQueue(feedUrl, UpdateManagerAction.REGISTER_FEED_FOR_UPDATE, incomingSubscriptionQueue);
    }

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        putMessageIntoQueue(feedUrl, UpdateManagerAction.CANCEL_UPDATE_OF_FEED, subscriptionCanceledQueue);
    }

    private void putMessageIntoQueue(final String feedUrl,
                                       final UpdateManagerAction action,
                                       final ActiveMQQueue queue) {
        UpdateManagerMessageCreator messageCreator = new UpdateManagerMessageCreator(feedUrl, action);
        final JmsTemplate jmsTemplate = getJmsTemplate();
        jmsTemplate.send(queue, messageCreator);
    }

    private JmsTemplate getJmsTemplate(){
        return applicationContext.getBean(JmsTemplate.class);
    }
}
