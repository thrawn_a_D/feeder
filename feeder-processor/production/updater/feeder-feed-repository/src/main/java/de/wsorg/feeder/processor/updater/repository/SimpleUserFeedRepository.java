package de.wsorg.feeder.processor.updater.repository;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;
import de.wsorg.feeder.processor.updater.repository.utils.storage.FeedStorageProxy;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class SimpleUserFeedRepository implements UserFeedRepository{

    @Inject
    private FeedStorageProxy feedStorageProxy;
    @Inject
    private FeedRepository feedRepository;

    @Override
    public List<UserRelatedFeedModel> loadUserFeeds(final String userId) {
        final List<UserRelatedFeedModel> currentUserFeedsList = getUsersFeedsStorage().getUserFeeds(userId);
        return updateUserFeeds(currentUserFeedsList);
    }

    @Override
    public List<UserRelatedFeedModel> loadUserFeeds(final String userId, final String titleQuery) {
        final List<UserRelatedFeedModel> currentUserFeedsList = getUsersFeedsStorage().getUserFeeds(userId, titleQuery);
        return updateUserFeeds(currentUserFeedsList);
    }

    private List<UserRelatedFeedModel> updateUserFeeds(final List<UserRelatedFeedModel> currentUserFeedsList) {
        final List<UserRelatedFeedModel> upToDateUserFeeds = new ArrayList<>();
        for (final UserRelatedFeedModel userFeed : currentUserFeedsList) {
            final FeedModel refreshedUserFeed = feedRepository.loadFeed(userFeed.getFeedUrl());
            upToDateUserFeeds.add(new UserRelatedFeedModel(refreshedUserFeed, userFeed.getFeedToUserAssociation()));
        }

        return upToDateUserFeeds;
    }

    private UsersFeedsStorage getUsersFeedsStorage() {
        return feedStorageProxy.getUsersFeedsStorage();
    }
}
