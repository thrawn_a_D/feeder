package de.wsorg.feeder.processor.updater.repository.utils.config;

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;
import lombok.Data;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class RepositoryConfig {
    public static final String REPOSITORY_ACTIVE_MQ_URL = "repository.activeMQ.url";
    public static final String REPOSITORY_ACTIVE_MQ_PORT = "repository.activeMQ.port";

    private StorageConfig storageConfig;
    private String activeMQUrl;
    private String activeMQPort;

    public RepositoryConfig(){
    }

    public RepositoryConfig(final Properties propertiesFile, final StorageConfig storageConfig) {
        activeMQUrl = propertiesFile.getProperty(REPOSITORY_ACTIVE_MQ_URL);
        activeMQPort = propertiesFile.getProperty(REPOSITORY_ACTIVE_MQ_PORT);
        this.storageConfig = storageConfig;
    }
}
