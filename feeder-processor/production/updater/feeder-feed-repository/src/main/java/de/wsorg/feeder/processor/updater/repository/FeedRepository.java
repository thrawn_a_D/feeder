package de.wsorg.feeder.processor.updater.repository;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedRepository {
    FeedModel loadFeed(final String feedUrl);
    void informThatUserCanceledSubscription(final String feedUrl);
}
