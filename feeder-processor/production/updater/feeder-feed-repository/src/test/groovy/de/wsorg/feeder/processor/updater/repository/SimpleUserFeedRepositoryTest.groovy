package de.wsorg.feeder.processor.updater.repository;


import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage
import de.wsorg.feeder.processor.updater.repository.utils.storage.FeedStorageProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 02.04.13
 */
public class SimpleUserFeedRepositoryTest extends Specification {
    SimpleUserFeedRepository simpleUserFeedRepository
    FeedStorageProxy feedStorageProxy
    UsersFeedsStorage usersFeedsStorage
    FeedRepository feedRepository


    def setup(){
        simpleUserFeedRepository = new SimpleUserFeedRepository()

        usersFeedsStorage = Mock(UsersFeedsStorage)
        feedStorageProxy = Mock(FeedStorageProxy)
        feedRepository = Mock(FeedRepository)

        feedStorageProxy.getUsersFeedsStorage() >> usersFeedsStorage

        simpleUserFeedRepository.feedStorageProxy = feedStorageProxy
        simpleUserFeedRepository.feedRepository = feedRepository
    }

    def "Load user feeds and register them for update"() {
        given:
        def userId = 'lkjlkn'

        and:
        def feed1 = new FeedModel(feedUrl: 'feedUrl1')
        def feed2 = new FeedModel(feedUrl: 'feedUrl2')
        def userFeed1 = new UserRelatedFeedModel(feed1, null)
        def userFeed2 = new UserRelatedFeedModel(feed2, null)
        def userRelatedFeeds = [userFeed1, userFeed2]

        and:
        def refreshedFeedModel1 = new FeedModel(feedUrl: feed1.feedUrl)
        def refreshedFeedModel2 = new FeedModel(feedUrl: feed2.feedUrl)

        when:
        def result = simpleUserFeedRepository.loadUserFeeds(userId)

        then:
        1 * usersFeedsStorage.getUserFeeds(userId) >> userRelatedFeeds
        1 * feedRepository.loadFeed(feed1.feedUrl) >> refreshedFeedModel1
        1 * feedRepository.loadFeed(feed2.feedUrl) >> refreshedFeedModel2
        result == [refreshedFeedModel1, refreshedFeedModel2]
    }

    def "Load feeds related to a title"() {
        given:
        def userId = 'lkjlkn'
        def title = 'sdfsdf'

        and:
        def feed1 = new FeedModel(feedUrl: 'feedUrl1')
        def feed2 = new FeedModel(feedUrl: 'feedUrl2')
        def userFeed1 = new UserRelatedFeedModel(feed1, null)
        def userFeed2 = new UserRelatedFeedModel(feed2, null)
        def userRelatedFeeds = [userFeed1, userFeed2]

        and:
        def refreshedFeedModel1 = new FeedModel(feedUrl: feed1.feedUrl)
        def refreshedFeedModel2 = new FeedModel(feedUrl: feed2.feedUrl)

        when:
        def result = simpleUserFeedRepository.loadUserFeeds(userId, title)

        then:
        1 * usersFeedsStorage.getUserFeeds(userId, title) >> userRelatedFeeds
        1 * feedRepository.loadFeed(feed1.feedUrl) >> refreshedFeedModel1
        1 * feedRepository.loadFeed(feed2.feedUrl) >> refreshedFeedModel2
        result == [refreshedFeedModel1, refreshedFeedModel2]
    }
}
