package de.wsorg.feeder.processor.updater.repository

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess
import de.wsorg.feeder.processor.updater.repository.utils.loader.FeedLoaderProxy
import de.wsorg.feeder.processor.updater.repository.utils.registrator.FeedRegistrator
import de.wsorg.feeder.processor.updater.repository.utils.storage.FeedStorageProxy
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.03.13
 */
class SimpleFeedRepositoryTest extends Specification {

    SimpleFeedRepository simpleFeedRepository
    FeedStorageProxy feedStorageProxy
    FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess
    FeedLoaderProxy feedLoaderProxy
    FeedDataAccess feedDataAccess
    ExternalFeedLoader externalFeedLoader
    FeedRegistrator feedRegistrator

    def setup(){
        simpleFeedRepository = new SimpleFeedRepository()

        feedStorageProxy = Mock(FeedStorageProxy)
        feedLoadStatisticsDataAccess = Mock(FeedLoadStatisticsDataAccess)
        externalFeedLoader = Mock(ExternalFeedLoader)
        feedDataAccess = Mock(FeedDataAccess)
        feedRegistrator = Mock(FeedRegistrator)
        feedLoaderProxy = Mock(FeedLoaderProxy)

        simpleFeedRepository.feedStorageProxy = feedStorageProxy
        simpleFeedRepository.feedLoaderProxy = feedLoaderProxy
        simpleFeedRepository.feedRegistrator = feedRegistrator

        feedLoaderProxy.getFeedLoader() >> externalFeedLoader
        feedStorageProxy.getFeedLoadStatisticsDataAccess() >> feedLoadStatisticsDataAccess
        feedStorageProxy.getFeedDataAccess() >> feedDataAccess
    }

    def "Load a feed with no internal feed available"() {
        given:
        def feedUrl = 'http://kdsjfkjbsfd.de/kajwd'

        and:
        def feedModel = new FeedModel()

        and:
        def feedLoadStatistic = new FeedLoadStatistic(lastLoaded: new Date(1989, 12, 12))

        when:
        def result = simpleFeedRepository.loadFeed(feedUrl)

        then:
        result == feedModel
        1 * feedLoadStatisticsDataAccess.getFeedLoadStatistics(feedUrl) >> feedLoadStatistic
        1 * feedLoadStatisticsDataAccess.updateFeedLoadStatistics({it.lastLoaded.date == new Date().date})
        1 * feedDataAccess.findByFeedUrl(feedUrl) >> feedModel
    }

    def "If feed load statistic is null load the feed external"() {
        given:
        def feedUrl = 'http://kdsjfkjbsfd.de/kajwd'

        and:
        def feedModel = new FeedModel()

        when:
        def result = simpleFeedRepository.loadFeed(feedUrl)

        then:
        result == feedModel
        1 * feedLoadStatisticsDataAccess.getFeedLoadStatistics(feedUrl) >> null
        1 * externalFeedLoader.loadFeed(feedUrl) >> feedModel
        1 * feedLoadStatisticsDataAccess.addNewFeedLoadStatistic(feedUrl)
        1 * feedRegistrator.registerFeed(feedUrl)
        1 * feedDataAccess.save(feedModel)
    }

    def "Cancel a subscription"() {
        given:
        def feedUrl = 'kjhuh'

        when:
        simpleFeedRepository.informThatUserCanceledSubscription(feedUrl)

        then:
        1 * feedRegistrator.cancelFeedUpdating(feedUrl)
    }
}
