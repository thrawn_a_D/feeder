package de.wsorg.feeder.processor.updater.repository

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.updater.repository.utils.config.RepositoryConfig
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.03.12
 */
class FeedRepositoryFactoryTest extends Specification {
    @Shared
    RepositoryConfig config


    def setup(){
        config = new RepositoryConfig()

        def storageConfig = new StorageConfig()
        storageConfig.couchbaseConfig = new CouchbaseConfig()
        storageConfig.elasticSearchConfig = new ElasticSearchConfig()

        config.storageConfig = storageConfig

        FeedRepositoryFactory.configProvided = false
    }

    def "Get feed repository object"() {
        given:
        FeedRepositoryFactory.setClientConfig(config)

        when:
        def result = FeedRepositoryFactory.getFeedRepository()

        then:
        result
    }

    def "Check if configuration was set properly when getting a feed repository"() {

        when:
        FeedRepositoryFactory.getFeedRepository()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Get user feed repository object"() {
        given:
        FeedRepositoryFactory.setClientConfig(config)

        when:
        def result = FeedRepositoryFactory.getUserFeedRepository()

        then:
        result
    }

    def "Check if configuration was set properly when getting a user feed repository"() {

        when:
        FeedRepositoryFactory.getUserFeedRepository()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }
}
