package de.wsorg.feeder.processor.updater.repository.utils.registrator.filebased

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.03.13
 */
class FileBasedFeedRegistratorTest extends Specification {
    FileBasedFeedRegistrator feedRegistrator
    FileBasedPersistence fileBasedPersistence

    def setup(){
        feedRegistrator = new FileBasedFeedRegistrator()

        fileBasedPersistence = Mock(FileBasedPersistence)

        feedRegistrator.fileBasedPersistence = fileBasedPersistence
    }

    def "Send message to update a feed using file system"() {
        given:
        def feedUrl = 'kjbkgu'

        when:
        feedRegistrator.registerFeed(feedUrl)

        then:
        1 * fileBasedPersistence.save('updateManager_incomingUpdateSubscriptionQueue', feedUrl, {UpdaterJobMessage it ->
                                                                              it.identifier == feedUrl &&
                                                                              it.action == UpdateManagerAction.REGISTER_FEED_FOR_UPDATE})
    }

    def "Cancel updating of a feed by sending a message by file"() {
        given:
        def feedUrl = 'kjbkgu'

        when:
        feedRegistrator.cancelFeedUpdating(feedUrl)

        then:
        1 * fileBasedPersistence.save('updateManager_subscriptionCanceledQueue', feedUrl, {UpdaterJobMessage it ->
                                                                            it.identifier == feedUrl &&
                                                                            it.action == UpdateManagerAction.CANCEL_UPDATE_OF_FEED})
    }
}
