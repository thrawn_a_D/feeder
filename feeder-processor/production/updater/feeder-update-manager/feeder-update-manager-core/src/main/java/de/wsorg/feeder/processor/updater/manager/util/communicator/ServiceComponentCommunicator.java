package de.wsorg.feeder.processor.updater.manager.util.communicator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ServiceComponentCommunicator {
    void registerFeed(final String feedUrl);
    void cancelFeedUpdating(final String feedUrl);
    void requestSubscriptionList();
}
