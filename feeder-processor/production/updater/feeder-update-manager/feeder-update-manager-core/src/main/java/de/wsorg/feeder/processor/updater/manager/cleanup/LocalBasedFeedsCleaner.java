package de.wsorg.feeder.processor.updater.manager.cleanup;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import javax.inject.Inject;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class LocalBasedFeedsCleaner implements SubscribedFeedsCleaner {
    public static final String UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES = "updater.removeSubscriptionsNotLoadedSinceMinutes";
    private static final String UPDATER_LOCAL_CLEANUP_TIME_AS_CRON_FORMAT = "updater.localCleanupTimeAsCronFormat";

    @Inject
    private UpdaterStorageProvider storageProvider;
    @Inject
    private ServiceComponentCommunicator serviceComponentCommunicator;
    @Value("${" + UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES + ":180}")
    private String timeIntervalForValidFeedSubscriptions;

    @Override
    @Async
    @Scheduled(cron = "${" + UPDATER_LOCAL_CLEANUP_TIME_AS_CRON_FORMAT + ":0 0 */3 * * *}")
    public void removeUnusedFeeds() {
        log.debug("Start cleanup process. Based on local unused feed subscriptions");
        final Date feedLoadedOlderThen = calculateDateForRemovalOfUnusedFeedSubscriptions();
        final List<FeedLoadStatistic> feedsToCancelSubscription = storageProvider.getFeedLoadStatisticsDataAccess().findLoadStatisticsOlderThen(feedLoadedOlderThen);

        final List<URL> feedUrlsToCancelSubscription = getUrlsOfFeedsToCancel(feedsToCancelSubscription);

        for (URL url : feedUrlsToCancelSubscription) {
            log.debug("Found unused feed. Cancel update subscription for: {}", url.toExternalForm());
            serviceComponentCommunicator.cancelFeedUpdating(url.toExternalForm());
        }

        removeNowUnnecessaryData(feedsToCancelSubscription);
        LocalBasedFeedsCleaner.log.debug("Cleanup process finished!");
    }

    private Date calculateDateForRemovalOfUnusedFeedSubscriptions() {
        final Calendar calendar = Calendar.getInstance();
        final int minutesToSubstract = -Integer.parseInt(timeIntervalForValidFeedSubscriptions);
        calendar.add(Calendar.MINUTE, minutesToSubstract);
        return calendar.getTime();
    }

    private void removeNowUnnecessaryData(final List<FeedLoadStatistic> feedsToCancelSubscription) {
        for (FeedLoadStatistic feedLoadStatistic : feedsToCancelSubscription) {
            LocalBasedFeedsCleaner.log.debug("Remove local load statistics data for feed: {}", feedLoadStatistic.getFeedUrl());
            storageProvider.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedLoadStatistic.getFeedUrl());
        }
    }

    private List<URL> getUrlsOfFeedsToCancel(final List<FeedLoadStatistic> feedsToCancelSubscription) {
        final List<URL> feedUrlsToCancelSubscription = new ArrayList<>();
        for (FeedLoadStatistic feedLoadStatistic : feedsToCancelSubscription) {
            try {
                feedUrlsToCancelSubscription.add(new URL(feedLoadStatistic.getFeedUrl()));
            } catch (MalformedURLException e) {
                LocalBasedFeedsCleaner.log.error("Canceling of feed with url ({}) not possible. URL is invalid.", feedLoadStatistic.getFeedUrl());
            }
        }
        return feedUrlsToCancelSubscription;
    }
}
