package de.wsorg.feeder.processor.updater.manager.util.testsupport.eventlistener;

import javax.inject.Inject;

import org.aspectj.lang.JoinPoint;

import de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventCollector;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public class AopEventMapper {
    @Inject
    private EventCollector eventCollector;

    public void eventOccurred(JoinPoint processingJoinPoint) {
        switch (processingJoinPoint.getSignature().getName()) {
            case "removeUnusedFeeds":
                eventCollector.eventOccurred(UpdateManagerEventConstants.CLEANUP_SUBSCRIBED_FEEDS);
                break;
            case "execute":
                if(processingJoinPoint.getSourceLocation()
                                      .getWithinType()
                                      .getName().contains("SubscribedFeedsBasedCleanupExecutor")) {
                    eventCollector.eventOccurred(UpdateManagerEventConstants.SUPERFEEDER_BASED_CLEANUP_SUBSCRIBED_FEEDS);
                } else if(processingJoinPoint.getSourceLocation()
                        .getWithinType()
                        .getName().contains("CancelSubscription")) {
                    eventCollector.eventOccurred(UpdateManagerEventConstants.CANCEL_SUBSCRIPTION);
                }
                break;
        }
    }

    public void exceptionEventOccurred(Exception exception) {
        eventCollector.exceptionEventOccurred(exception);
    }
}
