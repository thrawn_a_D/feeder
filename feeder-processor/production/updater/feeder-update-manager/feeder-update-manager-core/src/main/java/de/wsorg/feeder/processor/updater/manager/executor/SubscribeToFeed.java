package de.wsorg.feeder.processor.updater.manager.executor;

import javax.inject.Inject;
import javax.inject.Named;

import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Slf4j
public class SubscribeToFeed implements UpdaterMessageJobExecutor {
    @Inject
    private ServiceComponentCommunicator serviceComponentCommunicator;

    @Override
    public void execute(final UpdaterJobMessage message) {
        SubscribeToFeed.log.debug("Start subscribing feed for updates: {}", message.getIdentifier());
        serviceComponentCommunicator.registerFeed(message.getIdentifier());
    }
}
