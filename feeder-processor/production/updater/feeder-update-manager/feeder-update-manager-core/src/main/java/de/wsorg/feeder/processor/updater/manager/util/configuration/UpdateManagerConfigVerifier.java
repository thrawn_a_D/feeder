package de.wsorg.feeder.processor.updater.manager.util.configuration;

import de.wsorg.feeder.processor.updater.manager.cleanup.LocalBasedFeedsCleaner;
import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UpdateManagerConfigVerifier implements ConfigVerifier {
    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult verificationResult = new ConfigVerificationResult(configurationFilePath);

        verifyMinutes(configProperties, verificationResult);

        return verificationResult;
    }

    private void verifyMinutes(final Properties configProperties, final ConfigVerificationResult verificationResult) {
        final String notLoadedSinceMinutes = LocalBasedFeedsCleaner.UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES;
        String loadedSinceMinutes = configProperties.getProperty(notLoadedSinceMinutes);

        if(StringUtils.isNotBlank(loadedSinceMinutes)) {
            try {
                Integer.parseInt(loadedSinceMinutes);
            } catch (NumberFormatException e) {
                verificationResult.addVerificationError(notLoadedSinceMinutes, "Please provide a valid number.");
            }
        }
    }
}
