package de.wsorg.feeder.processor.updater.manager.util.communicator.activemq;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.TextMessageCreator;
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ActiveMQFeedRegistrator implements ServiceComponentCommunicator {

    @Inject
    private ApplicationContext applicationContext;
    @Inject
    @Named("destinationForupdaterServiceIncomingUpdateSubscriptions")
    private ActiveMQQueue incomingSubscriptionQueue;
    @Inject
    @Named("destinationForUpdaterServiceCanceledSubscription")
    private ActiveMQQueue subscriptionCanceledQueue;
    @Inject
    @Named("destinationForUpdaterServiceGetAllSubscriptions")
    private ActiveMQQueue requestSubscribedFeedsQueue;

    @Override
    public void registerFeed(final String feedUrl) {
        putMessageIntoQueue(feedUrl, UpdateManagerAction.REGISTER_FEED_FOR_UPDATE, incomingSubscriptionQueue);
    }

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        putMessageIntoQueue(feedUrl, UpdateManagerAction.CANCEL_UPDATE_OF_FEED, subscriptionCanceledQueue);
    }

    @Override
    public void requestSubscriptionList() {
        TextMessageCreator textMessageCreator = new TextMessageCreator(StringUtils.EMPTY);
        getJmsTemplate().send(requestSubscribedFeedsQueue, textMessageCreator);
    }

    private void putMessageIntoQueue(final String feedUrl,
                                       final UpdateManagerAction action,
                                       final ActiveMQQueue queue) {
        UpdateManagerMessageCreator messageCreator = new UpdateManagerMessageCreator(feedUrl, action);
        final JmsTemplate jmsTemplate = getJmsTemplate();
        jmsTemplate.send(queue, messageCreator);
    }

    private JmsTemplate getJmsTemplate(){
        return applicationContext.getBean(JmsTemplate.class);
    }
}
