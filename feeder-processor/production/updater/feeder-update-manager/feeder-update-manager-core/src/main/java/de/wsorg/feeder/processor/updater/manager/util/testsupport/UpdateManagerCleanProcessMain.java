package de.wsorg.feeder.processor.updater.manager.util.testsupport;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UpdateManagerCleanProcessMain {

    private static ClassPathXmlApplicationContext applicationContext;

    public static void initContext() {
        final String[] configLocations = {"classpath:spring/update-manager-base-context.xml"};
        applicationContext = new ClassPathXmlApplicationContext(configLocations);
    }
}
