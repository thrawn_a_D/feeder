package de.wsorg.feeder.processor.updater.manager.util.communicator.filebased;

import javax.inject.Inject;

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import de.wsorg.feeder.utils.wrapper.UUIDWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FileBasedCallDelegator implements ServiceComponentCommunicator {

    public static String UPDATE_MESSAGE_FOLDER = "updateService_incomingUpdateSubscriptionQueue";
    public static String CANCEL_UPDATING_MESSAGE_FOLDER = "updateService_canceledSubscriptionQueue";
    public static String GET_ALL_SUBSCRIBED_FEEDS_FOLDER = "updateService_getAllSubscriptionsQueue";

    @Inject
    private FileBasedPersistence fileBasedPersistence;

    private UUIDWrapper uuidWrapper = new UUIDWrapper();

    @Override
    public void registerFeed(final String feedUrl) {
        writeMessageToFile(feedUrl, UPDATE_MESSAGE_FOLDER);
    }

    @Override
    public void cancelFeedUpdating(final String feedUrl) {
        writeMessageToFile(feedUrl, CANCEL_UPDATING_MESSAGE_FOLDER);
    }

    @Override
    public void requestSubscriptionList() {
        final String uuid = uuidWrapper.randomUUID();
        final UpdaterJobMessage jobMessage = new UpdaterJobMessage();

        jobMessage.setIdentifier(uuid);

        fileBasedPersistence.save(GET_ALL_SUBSCRIBED_FEEDS_FOLDER, uuid, jobMessage);
    }

    private void writeMessageToFile(final String feedUrl,
                                    final String storageFolderName) {
        final UpdaterJobMessage jobMessage = new UpdaterJobMessage();
        jobMessage.setIdentifier(feedUrl);
        fileBasedPersistence.save(storageFolderName, feedUrl, jobMessage);
    }
}
