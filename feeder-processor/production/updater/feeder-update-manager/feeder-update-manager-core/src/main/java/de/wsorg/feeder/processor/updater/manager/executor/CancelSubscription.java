package de.wsorg.feeder.processor.updater.manager.executor;

import javax.inject.Inject;
import java.util.List;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.UpdaterMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class CancelSubscription implements UpdaterMessageJobExecutor {
    @Inject
    private UpdaterStorageProvider storageProvider;
    @Inject
    private ServiceComponentCommunicator serviceComponentCommunicator;

    @Override
    public void execute(final UpdaterJobMessage message) {
        log.debug("Subscription cancellation event received. Checking if cancelling is possible.");

        final List<FeedToUserAssociation> leftAssociationsOfFeed = storageProvider.getFeedToUserAssociationStorage().getAssociationsOfFeed(message.getIdentifier());
        if (isNoOneSubscribedToThatFeed(leftAssociationsOfFeed)) {
            CancelSubscription.log.debug("Feed is no longer used by any other user, so remove feed data and unsubscribe from update subscription. {}", message.getIdentifier());

            storageProvider.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(message.getIdentifier());
            storageProvider.getFeedDataAccess().deleteFeed(message.getIdentifier());
            serviceComponentCommunicator.cancelFeedUpdating(message.getIdentifier());

            CancelSubscription.log.debug("Unsubscribing from feed done: {}",message.getIdentifier());
        } else {
            CancelSubscription.log.debug("Cancellation is not possible as the feed is still used by some other user. {}", message.getIdentifier());
        }
    }

    private boolean isNoOneSubscribedToThatFeed(final List<FeedToUserAssociation> leftAssociationsOfFeed) {
        return leftAssociationsOfFeed.isEmpty();
    }
}
