package de.wsorg.feeder.processor.updater.manager.executor;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.updater.common.utils.jobprocessing.executor.StringMessageJobExecutor;
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage;
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider;
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class SubscribedFeedsBasedCleanupExecutor implements StringMessageJobExecutor {
    @Inject
    private UpdaterStorageProvider storageProvider;
    @Inject
    private ServiceComponentCommunicator serviceComponentCommunicator;

    @Override
    public void execute(final StringJobMessage message) {

        log.debug("Received message with feedUrls to potentially be cleaned from update subscription.");
        log.debug("Check feeds to potentially remove from subscription: {}", message.getContent());

        List<String> feedUrlList = getFeedUrlList(message);

        for (String feedUrl : feedUrlList) {
            List<FeedToUserAssociation> localAssociationForSubscription = storageProvider.getFeedToUserAssociationStorage().getAssociationsOfFeed(feedUrl);

            if(isFeedNotUsed(localAssociationForSubscription)) {
                storageProvider.getFeedLoadStatisticsDataAccess().removeFeedLoadStatistic(feedUrl);
                storageProvider.getFeedDataAccess().deleteFeed(feedUrl);
                serviceComponentCommunicator.cancelFeedUpdating(feedUrl);
                log.debug("Feed removed from update subscription: {}", feedUrl);
            }
        }
    }

    private List<String> getFeedUrlList(final StringJobMessage message) {
        List<String> result = new ArrayList<>();

        String feedUrls = StringUtils.substringBetween(message.getContent(), "[", "]");
        if (feedUrls.contains(",")) {
            String[] feedUrlsArray = feedUrls.split(", ");
            result = Arrays.asList(feedUrlsArray);
        } else {
            result.add(feedUrls);
        }

        return result;
    }

    private boolean isFeedNotUsed(final List<FeedToUserAssociation> localAssociationForSubscription) {
        return localAssociationForSubscription == null || localAssociationForSubscription.size() <= 0;
    }
}
