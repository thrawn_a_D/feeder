package de.wsorg.feeder.processor.updater.manager.util.testsupport.eventlistener;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
public class UpdateManagerEventConstants {
    public static final String CLEANUP_SUBSCRIBED_FEEDS = "CLEANUP_SUBSCRIBED_FEEDS";
    public static final String SUPERFEEDER_BASED_CLEANUP_SUBSCRIBED_FEEDS = "SUPERFEEDER_BASED_CLEANUP_SUBSCRIBED_FEEDS";
    public static final String CANCEL_SUBSCRIPTION = "CANCEL_SUBSCRIPTION";
}
