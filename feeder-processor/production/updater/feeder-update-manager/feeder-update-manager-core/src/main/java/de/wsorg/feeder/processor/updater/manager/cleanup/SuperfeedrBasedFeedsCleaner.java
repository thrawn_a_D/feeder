package de.wsorg.feeder.processor.updater.manager.cleanup;

import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

import javax.inject.Inject;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class SuperfeedrBasedFeedsCleaner implements SubscribedFeedsCleaner {
    public static final String UPDATER_SUPERFEEDR_CLEANUP_TIME_AS_CRON_FORMAT = "updater.superfeedrCleanupTimeAsCronFormat";
    @Inject
    private ServiceComponentCommunicator serviceComponentCommunicator;

    @Override
    @Async
    @Scheduled(cron = "${" + UPDATER_SUPERFEEDR_CLEANUP_TIME_AS_CRON_FORMAT + ":0 0 0 * * *}")
    public void removeUnusedFeeds() {
        SuperfeedrBasedFeedsCleaner.log.debug("Trigger cleanup based on subscribed feeds on source.");
        serviceComponentCommunicator.requestSubscriptionList();
    }
}
