package de.wsorg.feeder.processor.updater.manager.executor

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 15.03.12
 */
class SubscribeToFeedTest extends Specification {
    SubscribeToFeed subscribeToFeedJobExecutor
    ServiceComponentCommunicator serviceComponentCommunicator

    def setup(){
        subscribeToFeedJobExecutor = new SubscribeToFeed()
        serviceComponentCommunicator = Mock(ServiceComponentCommunicator)

        subscribeToFeedJobExecutor.serviceComponentCommunicator = serviceComponentCommunicator
    }

    def "Subscribe to feed"() {
        given:
        def updateRequest = new UpdaterJobMessage(identifier: 'http://feeder.de')

        when:
        subscribeToFeedJobExecutor.execute(updateRequest)

        then:
        1 * serviceComponentCommunicator.registerFeed(updateRequest.identifier)
    }
}
