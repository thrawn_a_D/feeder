package de.wsorg.feeder.processor.updater.manager.executor

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.updater.common.utils.registrator.StringJobMessage
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.04.13
 */
class SubscribedFeedsBasedCleanupExecutorTest extends Specification {
    SubscribedFeedsBasedCleanupExecutor subscribedFeedsBasedCleanupExecutor
    FeedToUserAssociationStorage feedToUserAssociationStorage
    FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess
    ServiceComponentCommunicator serviceComponentCommunicator
    FeedDataAccess feedDataAccess
    UpdaterStorageProvider storageProvider

    def setup(){
        subscribedFeedsBasedCleanupExecutor = new SubscribedFeedsBasedCleanupExecutor()
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        feedDataAccess = Mock(FeedDataAccess)
        feedLoadStatisticsDataAccess = Mock(FeedLoadStatisticsDataAccess)
        storageProvider = Mock(UpdaterStorageProvider)
        serviceComponentCommunicator = Mock(ServiceComponentCommunicator)

        storageProvider.getFeedDataAccess() >> feedDataAccess
        storageProvider.getFeedLoadStatisticsDataAccess() >> feedLoadStatisticsDataAccess
        storageProvider.getFeedToUserAssociationStorage() >> feedToUserAssociationStorage

        subscribedFeedsBasedCleanupExecutor.storageProvider = storageProvider
        subscribedFeedsBasedCleanupExecutor.serviceComponentCommunicator = serviceComponentCommunicator
    }

    def "Remove all feeds which are subscribed at the superfeedr but no one has association to locally"() {
        given:
        def feedUrl1 = 'http://kjbkjh1.de'
        def feedUrl2 = 'http://kjbkjh2.de'

        and:
        def message = new StringJobMessage([feedUrl1, feedUrl2].toString())

        when:
        subscribedFeedsBasedCleanupExecutor.execute(message)

        then:
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(feedUrl1) >> [new FeedToUserAssociation()]
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(feedUrl2)
        1 * serviceComponentCommunicator.cancelFeedUpdating(feedUrl2)
    }

    def "Remove local data when unsubscribing from feed"() {
        given:
        def feedUrl = 'http://kjbkjh1.de'

        and:
        def message = new StringJobMessage([feedUrl].toString())

        when:
        subscribedFeedsBasedCleanupExecutor.execute(message)

        then:
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(feedUrl) >> []
        1 * feedLoadStatisticsDataAccess.removeFeedLoadStatistic(feedUrl)
        1 * feedDataAccess.deleteFeed(feedUrl)
        1 * serviceComponentCommunicator.cancelFeedUpdating(feedUrl)
    }
}
