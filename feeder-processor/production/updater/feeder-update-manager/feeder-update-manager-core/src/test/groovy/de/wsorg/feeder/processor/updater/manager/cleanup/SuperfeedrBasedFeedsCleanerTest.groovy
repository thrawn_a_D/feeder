package de.wsorg.feeder.processor.updater.manager.cleanup

import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.03.13
 */
class SuperfeedrBasedFeedsCleanerTest extends Specification {
    SuperfeedrBasedFeedsCleaner supereedrBasedFeedsCleaner
    ServiceComponentCommunicator serviceComponentCommunicator

    def setup(){
        supereedrBasedFeedsCleaner = new SuperfeedrBasedFeedsCleaner()
        serviceComponentCommunicator = Mock(ServiceComponentCommunicator)

        supereedrBasedFeedsCleaner.serviceComponentCommunicator = serviceComponentCommunicator
    }

    def "Initiate cleanup by requesting for all subscriptions"() {
        when:
        supereedrBasedFeedsCleaner.removeUnusedFeeds()

        then:
        1 * serviceComponentCommunicator.requestSubscriptionList()
    }
}
