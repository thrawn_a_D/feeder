package de.wsorg.feeder.processor.updater.manager.util.communicator

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.04.13
 */
class ComponentCommunicatorDeterminatorTest extends Specification {

    ComponentCommunicatorDeterminator communicatorDeterminator
    ServiceComponentCommunicator activeMqCommunicator
    ServiceComponentCommunicator fileBasedCommunicator

    def setup(){
        communicatorDeterminator = new ComponentCommunicatorDeterminator()

        activeMqCommunicator = Mock(ServiceComponentCommunicator)
        fileBasedCommunicator = Mock(ServiceComponentCommunicator)

        communicatorDeterminator.activeMqBasedComponentCommunicator = activeMqCommunicator
        communicatorDeterminator.fileBasedComponentCommunicator = fileBasedCommunicator
    }



    def "Determine communicator if file based is chosen"() {
        given:
        communicatorDeterminator.useFileSystemAsStorage = "true"

        when:
        def result = communicatorDeterminator.getComponentCommunicator()

        then:
        result == fileBasedCommunicator
    }

    def "Determine communicator if active mq based is chosen"() {
        given:
        communicatorDeterminator.useFileSystemAsStorage = "false"

        when:
        def result = communicatorDeterminator.getComponentCommunicator()

        then:
        result == activeMqCommunicator
    }

    def "Determine communicator if nothing is chosen"() {
        given:
        communicatorDeterminator.useFileSystemAsStorage = ""

        when:
        def result = communicatorDeterminator.getComponentCommunicator()

        then:
        result == activeMqCommunicator
    }

    def "Determine communicator if invalid is chosen"() {
        given:
        communicatorDeterminator.useFileSystemAsStorage = "asdasd"

        when:
        def result = communicatorDeterminator.getComponentCommunicator()

        then:
        result == activeMqCommunicator
    }
}
