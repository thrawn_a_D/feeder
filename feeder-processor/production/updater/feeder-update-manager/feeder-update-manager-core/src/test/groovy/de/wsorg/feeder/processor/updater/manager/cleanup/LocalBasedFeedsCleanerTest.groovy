package de.wsorg.feeder.processor.updater.manager.cleanup

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.03.13
 */
class LocalBasedFeedsCleanerTest extends Specification {
    LocalBasedFeedsCleaner complexSubscribedFeedsCleaner
    FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess
    FeedDataAccess feedDataAccess
    UpdaterStorageProvider storageProvider
    ServiceComponentCommunicator serviceComponentCommunicator

    def timeIntervalForValidFeedSubscriptions='60'

    def setup(){
        complexSubscribedFeedsCleaner = new LocalBasedFeedsCleaner()

        feedLoadStatisticsDataAccess = Mock(FeedLoadStatisticsDataAccess)
        feedDataAccess = Mock(FeedDataAccess)
        storageProvider = Mock(UpdaterStorageProvider)
        serviceComponentCommunicator = Mock(ServiceComponentCommunicator)

        storageProvider.getFeedDataAccess() >> feedDataAccess
        storageProvider.getFeedLoadStatisticsDataAccess() >> feedLoadStatisticsDataAccess

        complexSubscribedFeedsCleaner.storageProvider = storageProvider
        complexSubscribedFeedsCleaner.timeIntervalForValidFeedSubscriptions = timeIntervalForValidFeedSubscriptions
        complexSubscribedFeedsCleaner.serviceComponentCommunicator = serviceComponentCommunicator
    }

    def "Unsubscribe unused feeds"() {
        given:
        def loadStat1 = new FeedLoadStatistic(feedUrl: 'http://bla1.de')
        def loadStat2 = new FeedLoadStatistic(feedUrl: 'http://bla2.de')
        def feedsOlderThen = [loadStat1, loadStat2]

        when:
        complexSubscribedFeedsCleaner.removeUnusedFeeds()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen({it.hours == new Date().hours -1}) >> feedsOlderThen
        1 * serviceComponentCommunicator.cancelFeedUpdating(loadStat1.feedUrl)
        1 * serviceComponentCommunicator.cancelFeedUpdating(loadStat2.feedUrl)
    }

    def "Remove unused feeds and load statistics"() {
        given:
        def loadStat1 = new FeedLoadStatistic(feedUrl: 'http://bla1.de')
        def loadStat2 = new FeedLoadStatistic(feedUrl: 'http://bla2.de')
        def feedsOlderThen = [loadStat1, loadStat2]

        when:
        complexSubscribedFeedsCleaner.removeUnusedFeeds()

        then:
        1 * feedLoadStatisticsDataAccess.findLoadStatisticsOlderThen({it.hours == new Date().hours -1}) >> feedsOlderThen
        1 * feedLoadStatisticsDataAccess.removeFeedLoadStatistic(loadStat1.feedUrl)
        1 * feedLoadStatisticsDataAccess.removeFeedLoadStatistic(loadStat2.feedUrl)
    }
}
