package de.wsorg.feeder.processor.updater.manager.executor

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.processor.updater.common.utils.storage.UpdaterStorageProvider
import de.wsorg.feeder.processor.updater.manager.util.communicator.ServiceComponentCommunicator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.03.13
 */
class CancelSubscriptionTest extends Specification {
    CancelSubscription cancelSubscriptionOnSuperfeedr
    FeedToUserAssociationStorage feedToUserAssociationStorage
    FeedLoadStatisticsDataAccess feedLoadStatisticsDataAccess
    FeedDataAccess feedDataAccess
    ServiceComponentCommunicator serviceComponentCommunicator
    UpdaterStorageProvider storageProvider

    def setup(){
        cancelSubscriptionOnSuperfeedr = new CancelSubscription()
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        feedDataAccess = Mock(FeedDataAccess)
        feedLoadStatisticsDataAccess = Mock(FeedLoadStatisticsDataAccess)
        serviceComponentCommunicator = Mock(ServiceComponentCommunicator)
        storageProvider = Mock(UpdaterStorageProvider)

        storageProvider.getFeedDataAccess() >> feedDataAccess
        storageProvider.getFeedLoadStatisticsDataAccess() >> feedLoadStatisticsDataAccess
        storageProvider.getFeedToUserAssociationStorage() >> feedToUserAssociationStorage


        cancelSubscriptionOnSuperfeedr.storageProvider = storageProvider
        cancelSubscriptionOnSuperfeedr.serviceComponentCommunicator = serviceComponentCommunicator
    }

    def "Check if canceled feed has more associations, if not cancel the subscription"() {
        given:
        def jobMesage = new UpdaterJobMessage(identifier: 'http://bla.blub')

        when:
        cancelSubscriptionOnSuperfeedr.execute(jobMesage)

        then:
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(jobMesage.identifier) >> []
        1 * serviceComponentCommunicator.cancelFeedUpdating(jobMesage.identifier)
    }

    def "If there are still subscriptions to rely on that feed, DO NOT unsubscribe"() {
        given:
        def jobMesage = new UpdaterJobMessage(identifier: 'http://bla.blub')

        when:
        cancelSubscriptionOnSuperfeedr.execute(jobMesage)

        then:
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(jobMesage.identifier) >> [new FeedToUserAssociation()]
        0 * serviceComponentCommunicator.cancelFeedUpdating(_, _)
    }

    def "Remove local data if feed is no longer needed"() {
        given:
        def jobMesage = new UpdaterJobMessage(identifier: 'http://bla.blub')

        when:
        cancelSubscriptionOnSuperfeedr.execute(jobMesage)

        then:
        1 * feedToUserAssociationStorage.getAssociationsOfFeed(jobMesage.identifier) >> []
        1 * feedLoadStatisticsDataAccess.removeFeedLoadStatistic(jobMesage.identifier)
        1 * feedDataAccess.deleteFeed(jobMesage.identifier)
    }
}
