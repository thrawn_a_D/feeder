package de.wsorg.feeder.processor.updater.manager.util.testsupport.eventlistener

import de.wsorg.feeder.processor.updater.common.utils.testsuopport.EventCollector
import de.wsorg.feeder.processor.updater.manager.executor.SubscribedFeedsBasedCleanupExecutor
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.Signature
import org.aspectj.lang.reflect.SourceLocation
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 */
class AopEventMapperTest extends Specification {
    AopEventMapper aopEventMapper
    EventCollector eventCollector

    def joinPoint = Mock(JoinPoint)
    def signature = Mock(Signature)

    def setup(){
        aopEventMapper = new AopEventMapper()
        eventCollector = Mock(EventCollector)
        aopEventMapper.eventCollector = eventCollector

        joinPoint.getSignature() >> signature
    }

    def "Map removeUnusedFeeds to an event"() {
        given:
        def methodName = 'removeUnusedFeeds'
        signature.getName() >> methodName

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(UpdateManagerEventConstants.CLEANUP_SUBSCRIBED_FEEDS)
    }

    def "Map superfeedr based cleanup to an event"() {
        given:
        def methodName = 'execute'
        def sourceLocation = Mock(SourceLocation)
        def withinType = SubscribedFeedsBasedCleanupExecutor

        and:
        signature.getName() >> methodName
        joinPoint.getSourceLocation() >> sourceLocation
        sourceLocation.getWithinType() >> withinType

        when:
        aopEventMapper.eventOccurred(joinPoint)

        then:
        1 * eventCollector.eventOccurred(UpdateManagerEventConstants.SUPERFEEDER_BASED_CLEANUP_SUBSCRIBED_FEEDS)
    }

    def "Exception is occurred"() {
        given:
        def exception = Mock(Exception)

        when:
        aopEventMapper.exceptionEventOccurred(exception)

        then:
        1 * eventCollector.exceptionEventOccurred(exception)
    }
}