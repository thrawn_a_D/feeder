package de.wsorg.feeder.processor.updater.manager.util.spring

import org.springframework.context.support.ClassPathXmlApplicationContext
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.03.13
 */
class ContextLoadingTest extends Specification {
    def "Load the spring context to test it's functionality"() {
        when:
        def appContext = new ClassPathXmlApplicationContext(["classpath:spring/update-manager-base-context.xml"] as String[])

        then:
        true
    }
}
