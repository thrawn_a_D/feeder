package de.wsorg.feeder.processor.updater.manager.util.configuration

import de.wsorg.feeder.processor.updater.manager.cleanup.LocalBasedFeedsCleaner
import de.wsorg.feeder.processor.updater.manager.cleanup.SuperfeedrBasedFeedsCleaner
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.03.13
 */
class UpdateManagerConfigVerifierTest extends Specification {
    UpdateManagerConfigVerifier updateManagerConfigVerifier
    def configLocation = [''] as String[]

    def setup(){
        updateManagerConfigVerifier = new UpdateManagerConfigVerifier()
    }

    def "Verify config with only valid entries"() {
        given:
        def properties = new Properties()
        properties.put(LocalBasedFeedsCleaner.UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES, '60')
        properties.put(LocalBasedFeedsCleaner.UPDATER_LOCAL_CLEANUP_TIME_AS_CRON_FORMAT, '0 0 */3 * * *')
        properties.put(SuperfeedrBasedFeedsCleaner.UPDATER_SUPERFEEDR_CLEANUP_TIME_AS_CRON_FORMAT, '0 0 0 * * *')

        when:
        def result = updateManagerConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Do not show errors if optional fields are not set"() {
        given:
        def properties = new Properties()
        properties.put(LocalBasedFeedsCleaner.UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES, '')
        properties.put(LocalBasedFeedsCleaner.UPDATER_LOCAL_CLEANUP_TIME_AS_CRON_FORMAT, '')
        properties.put(SuperfeedrBasedFeedsCleaner.UPDATER_SUPERFEEDR_CLEANUP_TIME_AS_CRON_FORMAT, '')

        when:
        def result = updateManagerConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Verify not loaded since minutes value"() {
        given:
        def properties = new Properties()
        properties.put(LocalBasedFeedsCleaner.UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES, 'sa')
        properties.put(LocalBasedFeedsCleaner.UPDATER_LOCAL_CLEANUP_TIME_AS_CRON_FORMAT, '0 0 */3 * * *')
        properties.put(SuperfeedrBasedFeedsCleaner.UPDATER_SUPERFEEDR_CLEANUP_TIME_AS_CRON_FORMAT, '0 0 0 * * *')

        when:
        def result = updateManagerConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[LocalBasedFeedsCleaner.UPDATER_REMOVE_SUBSCRIPTIONS_NOT_LOADED_SINCE_MINUTES] == ['Please provide a valid number.']
    }
}
