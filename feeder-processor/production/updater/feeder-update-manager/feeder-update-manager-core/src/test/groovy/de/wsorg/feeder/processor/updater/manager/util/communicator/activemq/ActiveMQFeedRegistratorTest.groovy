package de.wsorg.feeder.processor.updater.manager.util.communicator.activemq

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdateManagerAction
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.TextMessageCreator
import de.wsorg.feeder.processor.updater.common.utils.registrator.activemq.UpdateManagerMessageCreator
import org.apache.activemq.command.ActiveMQQueue
import org.springframework.context.ApplicationContext
import org.springframework.jms.core.JmsTemplate
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.03.13
 */
class ActiveMQFeedRegistratorTest extends Specification {
    ActiveMQFeedRegistrator activeMQFeedRegistrator
    ApplicationContext applicationContext
    JmsTemplate jmsTemplate
    ActiveMQQueue incomingSubscriptionQueue
    ActiveMQQueue subscriptionCanceledQueue
    ActiveMQQueue requestSubscribedFeedsQueue

    def setup(){
        activeMQFeedRegistrator = new ActiveMQFeedRegistrator()

        jmsTemplate = Mock(JmsTemplate)
        applicationContext = Mock(ApplicationContext)
        incomingSubscriptionQueue = Mock(ActiveMQQueue)
        subscriptionCanceledQueue = Mock(ActiveMQQueue)
        requestSubscribedFeedsQueue = Mock(ActiveMQQueue)

        applicationContext.getBean(JmsTemplate) >> jmsTemplate

        activeMQFeedRegistrator.applicationContext = applicationContext
        activeMQFeedRegistrator.incomingSubscriptionQueue = incomingSubscriptionQueue
        activeMQFeedRegistrator.subscriptionCanceledQueue = subscriptionCanceledQueue
        activeMQFeedRegistrator.requestSubscribedFeedsQueue = requestSubscribedFeedsQueue
    }

    def "Register a feed in the queue"() {
        given:
        def feedUrl = 'kjhkjh'

        when:
        activeMQFeedRegistrator.registerFeed(feedUrl)

        then:
        1 * jmsTemplate.send(incomingSubscriptionQueue,
                             {UpdateManagerMessageCreator it ->
                              it.feedUrl == feedUrl &&
                              it.action == UpdateManagerAction.REGISTER_FEED_FOR_UPDATE})
    }

    def "Cancel feed update"() {
        given:
        def feedUrl = 'kjhkjh'

        when:
        activeMQFeedRegistrator.cancelFeedUpdating(feedUrl)

        then:
        1 * jmsTemplate.send(subscriptionCanceledQueue,
                             {UpdateManagerMessageCreator it ->
                              it.feedUrl == feedUrl  &&
                              it.action == UpdateManagerAction.CANCEL_UPDATE_OF_FEED
        })
    }

    def "Get a list of subscriptions"() {
        when:
        activeMQFeedRegistrator.requestSubscriptionList()

        then:
        1 * jmsTemplate.send(requestSubscribedFeedsQueue, {it instanceof TextMessageCreator})
    }
}
