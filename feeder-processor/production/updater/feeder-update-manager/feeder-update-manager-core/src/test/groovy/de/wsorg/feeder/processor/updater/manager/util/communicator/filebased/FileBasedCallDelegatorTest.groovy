package de.wsorg.feeder.processor.updater.manager.util.communicator.filebased

import de.wsorg.feeder.processor.updater.common.utils.registrator.UpdaterJobMessage
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import de.wsorg.feeder.utils.wrapper.UUIDWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.03.13
 */
class FileBasedCallDelegatorTest extends Specification {
    FileBasedCallDelegator feedRegistrator
    FileBasedPersistence fileBasedPersistence
    UUIDWrapper uuidWrapper

    def setup(){
        feedRegistrator = new FileBasedCallDelegator()

        fileBasedPersistence = Mock(FileBasedPersistence)
        uuidWrapper = Mock(UUIDWrapper)

        feedRegistrator.fileBasedPersistence = fileBasedPersistence
        feedRegistrator.uuidWrapper = uuidWrapper
    }

    def "Send message to update a feed using file system"() {
        given:
        def feedUrl = 'kjbkgu'

        when:
        feedRegistrator.registerFeed(feedUrl)

        then:
        1 * fileBasedPersistence.save('updateService_incomingUpdateSubscriptionQueue', feedUrl, {UpdaterJobMessage it -> it.identifier == feedUrl})
    }

    def "Cancel updating of a feed by sending a message by file"() {
        given:
        def feedUrl = 'kjbkgu'

        when:
        feedRegistrator.cancelFeedUpdating(feedUrl)

        then:
        1 * fileBasedPersistence.save('updateService_canceledSubscriptionQueue', feedUrl, {UpdaterJobMessage it -> it.identifier == feedUrl})
    }

    def "Request feed update"() {
        given:
        def actualUUID = 'lijooihi8'

        and:
        uuidWrapper.randomUUID() >> actualUUID

        when:
        feedRegistrator.requestSubscriptionList()

        then:
        1 * fileBasedPersistence.save('updateService_getAllSubscriptionsQueue', actualUUID, {UpdaterJobMessage it -> it.identifier == actualUUID})
    }
}
