package de.wsorg.feeder.processor.production.domain.exception;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UseCaseProcessException extends RuntimeException {
    public UseCaseProcessException() {
    }

    public UseCaseProcessException(final String message) {
        super(message);
    }
}
