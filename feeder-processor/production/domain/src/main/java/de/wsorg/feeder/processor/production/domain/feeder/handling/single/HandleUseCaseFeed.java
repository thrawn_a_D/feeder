package de.wsorg.feeder.processor.production.domain.feeder.handling.single;

import de.wsorg.feeder.processor.production.domain.feeder.handling.HandleUseCase;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public abstract class HandleUseCaseFeed extends HandleUseCase {
    protected String feedUrl;
}
