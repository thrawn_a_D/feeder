package de.wsorg.feeder.processor.production.domain.feeder.search.user;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum UserSearchRestriction {
    UNREAD_ENTRIES
}
