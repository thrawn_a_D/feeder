package de.wsorg.feeder.processor.production.domain.authentication;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface AuthorizedRequest {
    String getUserName();
    String getPassword();
    String getUserId();
    void setUserName(final String userName);
    void setPassword(final String password);
    void setUserId(final String userId);
}
