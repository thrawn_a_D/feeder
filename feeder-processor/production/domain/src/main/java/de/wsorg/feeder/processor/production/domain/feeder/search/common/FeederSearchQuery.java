package de.wsorg.feeder.processor.production.domain.feeder.search.common;

import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeederSearchQuery {
    protected String searchTerm;
}
