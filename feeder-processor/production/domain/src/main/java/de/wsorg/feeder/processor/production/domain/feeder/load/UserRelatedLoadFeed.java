package de.wsorg.feeder.processor.production.domain.feeder.load;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class UserRelatedLoadFeed extends LoadFeed implements AuthorizedRequest {
    protected String userName;
    protected String password;

    protected String userId;

    protected boolean showOnlyUnreadEntries=false;
}
