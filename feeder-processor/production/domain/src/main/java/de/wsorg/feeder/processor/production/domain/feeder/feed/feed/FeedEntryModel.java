package de.wsorg.feeder.processor.production.domain.feeder.feed.feed;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeedEntryModel extends StandardFeedEntry implements Serializable {

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final FeedEntryModel that = (FeedEntryModel) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (updated != null ? !isDateEquals(that) : that.updated != null)
            return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    private boolean isDateEquals(final FeedEntryModel feedEntryModel) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        return dateFormat.format(updated).equals(dateFormat.format(feedEntryModel.getUpdated()));
    }
}
