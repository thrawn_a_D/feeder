package de.wsorg.feeder.processor.production.domain.feeder;

import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 *
 */
@Data
public class PagingRange implements Serializable {
    private int startIndex=1;
    private int endIndex=Integer.MAX_VALUE;
}
