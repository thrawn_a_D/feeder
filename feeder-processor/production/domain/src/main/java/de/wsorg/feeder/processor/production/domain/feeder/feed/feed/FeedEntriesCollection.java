package de.wsorg.feeder.processor.production.domain.feeder.feed.feed;

import lombok.Data;

import java.io.Serializable;

/**
 *
 *
 *
 */
@Data
public class FeedEntriesCollection implements Serializable {
    protected boolean additionalPagingPossible;
}
