package de.wsorg.feeder.processor.production.domain.user;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class FeederUseCaseUser implements Serializable {
    private String userId;
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private String eMail;

    public FeederUseCaseUser(){}

    public FeederUseCaseUser(final String id,
                             final String nickName,
                             final String firstName,
                             final String lastName,
                             final String password,
                             final String eMail) {
        this.userId = id;
        this.userName = nickName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.eMail = eMail;
    }
}
