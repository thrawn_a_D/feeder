package de.wsorg.feeder.processor.production.domain.feeder.search.global;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * <p/>
 *
 *
 */
@Data
public class GlobalSearchQueryEnrichedWithUserData extends GlobalSearchQuery implements AuthorizedRequest {
    protected String userName;
    protected String password;
    protected String userId;
}
