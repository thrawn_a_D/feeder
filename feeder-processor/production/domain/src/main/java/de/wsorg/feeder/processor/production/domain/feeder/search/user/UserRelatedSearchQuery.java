package de.wsorg.feeder.processor.production.domain.feeder.search.user;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.processor.production.domain.feeder.search.common.FeederSearchQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class UserRelatedSearchQuery extends FeederSearchQuery implements AuthorizedRequest {
    protected String userName;
    protected String password;
    protected String userId;

    private Map<UserSearchRestriction, Object> searchRestrictions = new HashMap<UserSearchRestriction, Object>();

    public void addRestriction(final UserSearchRestriction restrictionType, final Object restrictionValue){
        searchRestrictions.put(restrictionType, restrictionValue);
    }

    public void addRestriction(final String restrictionType, final Object restrictionValue){
        UserSearchRestriction restriction = UserSearchRestriction.valueOf(restrictionType);
        searchRestrictions.put(restriction, restrictionValue);
    }
}
