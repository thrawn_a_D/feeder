package de.wsorg.feeder.processor.production.domain.feeder.search.global;

import de.wsorg.feeder.processor.production.domain.feeder.search.common.FeederSearchQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class GlobalSearchQuery extends FeederSearchQuery {
    private Map<GlobalSearchRestriction, Object> searchRestrictions = new HashMap<GlobalSearchRestriction, Object>();

    public void addRestriction(final GlobalSearchRestriction restrictionType, final Object restrictionValue){
        searchRestrictions.put(restrictionType, restrictionValue);
    }

    public void addRestriction(final String restrictionType, final Object restrictionValue){
        GlobalSearchRestriction restriction = GlobalSearchRestriction.valueOf(restrictionType);
        searchRestrictions.put(restriction, restrictionValue);
    }

    @Override
    public String toString(){
        return "SearchTerm: " + this.searchTerm +
               " SearchRestrictions: " + searchRestrictions;
    }
}
