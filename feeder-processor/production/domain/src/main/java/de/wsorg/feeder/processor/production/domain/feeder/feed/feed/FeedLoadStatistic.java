package de.wsorg.feeder.processor.production.domain.feeder.feed.feed;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeedLoadStatistic implements Serializable {
    @Getter
    private Date lastLoaded = new Date();
    @Getter
    private long lastLoadedAsLong = lastLoaded.getTime();
    @Getter @Setter
    private String feedUrl;

    public void setLastLoaded(final Date lastLoaded) {
        this.lastLoaded = lastLoaded;
        lastLoadedAsLong = lastLoaded.getTime();
    }
}
