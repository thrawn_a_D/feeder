package de.wsorg.feeder.processor.production.domain.feeder.feed.feed;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeedModel extends StandardFeed<FeedEntryModel> implements Serializable {

    protected FeedEntriesCollection feedEntriesCollection = new FeedEntriesCollection();

    public FeedModel() {
    }

    public FeedModel(final FeedModel feedModel) {
        applyFeedModel(feedModel);
    }

    public void addFeedEntry(final FeedEntryModel entryModel) {
        this.feedEntries.add(entryModel);
    }

    public void addFeedEntryOnTop(final FeedEntryModel entryModel) {
        this.feedEntries.add(0, entryModel);
    }

    public List<FeedEntryModel> getFeedEntryModels(){
        return feedEntries;
    }

    public void setFeedEntryModels(final List<FeedEntryModel> feedEntryModelList) {
        this.feedEntries = feedEntryModelList;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        final FeedModel feedModel = (FeedModel) o;

        if (feedUrl != null ? !feedUrl.equals(feedModel.feedUrl) : feedModel.feedUrl != null) return false;

        return true;
    }

    private boolean isDateEquals(final FeedModel feedModel) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        return dateFormat.format(updated).equals(dateFormat.format(feedModel.getUpdated()));
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (feedUrl != null ? feedUrl.hashCode() : 0);
        result = 31 * result + (logoUrl != null ? logoUrl.hashCode() : 0);
        result = 31 * result + (feedEntries != null ? feedEntries.hashCode() : 0);
        return result;
    }

    public boolean containsFeedEntry(String feedEntryId) {
        for (FeedEntryModel feedEntry : feedEntries) {
            if(feedEntry.getId().equals(feedEntryId))
                return true;
        }

        return false;
    }

    public void applyFeedModel(final FeedModel feedModel) {
        this.setDescription(feedModel.getDescription());
        this.feedEntriesCollection = feedModel.getFeedEntriesCollection();
        this.feedEntries = feedModel.feedEntries;
        this.setFeedUrl(feedModel.getFeedUrl());
        this.setLogoUrl(feedModel.getLogoUrl());
        this.setUpdated(feedModel.getUpdated());
        this.setTitle(feedModel.getTitle());
        this.setId(feedModel.getId());
    }
}
