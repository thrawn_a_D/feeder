package de.wsorg.feeder.processor.production.domain.feeder.load;

import de.wsorg.feeder.processor.production.domain.feeder.PagingRange;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class LoadFeed {
    protected String feedUrl;
    protected PagingRange feedEntryPagingRange = new PagingRange();
}
