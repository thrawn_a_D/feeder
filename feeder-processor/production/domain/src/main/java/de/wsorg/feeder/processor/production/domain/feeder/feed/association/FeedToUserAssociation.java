package de.wsorg.feeder.processor.production.domain.feeder.feed.association;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeedToUserAssociation implements Serializable {
    private String userId;
    private String feedUrl;
    private AssociationType associationType;
    /**
     * Map of feed entries which have a relative feed read status.
     * KEY: UID of a feed entry
     * VALUE: true of feed entry has bean read, otherwise false
     */
    private Map<String, Boolean> feedEntriesReadStatus = new HashMap<>();

    private String feedUiHighlightingColor;

    /**
     * Map of feed entries which have a relative feed entry coloring.
     * By default the FeedToUserAssociation#feedUiHighlightingColor
     * is also the feed entry color. In some cases entries can have
     * individual color (for instance in users TimeLine feed).
     */
    private Map<String, String> feedEntriesUiHighlightingColor = new HashMap<>();
}
