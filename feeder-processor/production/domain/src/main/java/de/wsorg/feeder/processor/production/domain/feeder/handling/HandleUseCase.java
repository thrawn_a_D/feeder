package de.wsorg.feeder.processor.production.domain.feeder.handling;

import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public abstract class HandleUseCase {
    protected String userId;
    protected String userName;
    protected String password;
}
