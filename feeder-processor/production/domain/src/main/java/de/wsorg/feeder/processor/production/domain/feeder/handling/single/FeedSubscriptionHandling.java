package de.wsorg.feeder.processor.production.domain.feeder.handling.single;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class FeedSubscriptionHandling extends HandleUseCaseFeed implements AuthorizedRequest {
}
