package de.wsorg.feeder.processor.production.domain.feeder.handling.all;

import de.wsorg.feeder.processor.production.domain.authentication.AuthorizedRequest;
import de.wsorg.feeder.processor.production.domain.feeder.handling.HandleUseCase;
import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class MarkAllFeedsReadStatusHandling extends HandleUseCase implements AuthorizedRequest {
    private boolean feedsReadStatus;
    private String category;
}
