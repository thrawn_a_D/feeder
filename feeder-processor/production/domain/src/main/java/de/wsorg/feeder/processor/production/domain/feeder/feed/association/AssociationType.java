package de.wsorg.feeder.processor.production.domain.feeder.feed.association;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum AssociationType {
    SUBSCRIBER,
    OWNER
}
