package de.wsorg.feeder.processor.production.domain.feeder.feed.user;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import lombok.Data;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class UserRelatedFeedModel extends FeedModel {
    private final FeedToUserAssociation feedToUserAssociation;

    public UserRelatedFeedModel(final FeedModel result,
                                final FeedToUserAssociation feedToUserAssociation) {
        super(result);
        this.feedToUserAssociation = feedToUserAssociation;
    }

    public UserRelatedFeedModel() {
        super(new FeedModel());
        this.feedToUserAssociation = new FeedToUserAssociation();
    }

    public AssociationType getUserToFeedAssociationType() {
        return feedToUserAssociation.getAssociationType();
    }

    public Map<String, Boolean> getFeedEntriesReadStatus() {
        return feedToUserAssociation.getFeedEntriesReadStatus();
    }

    public String getFeedUiHighlightingColor() {
        return feedToUserAssociation.getFeedUiHighlightingColor();
    }

    public Map<String, String> getFeedEntriesUiHighlightingColor() {
        return feedToUserAssociation.getFeedEntriesUiHighlightingColor();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!super.equals(o)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (feedToUserAssociation.getAssociationType() != null ? feedToUserAssociation.getAssociationType().hashCode() : 0);
        result = 31 * result + (feedToUserAssociation.getFeedEntriesReadStatus() != null ? feedToUserAssociation.getFeedEntriesReadStatus().hashCode() : 0);
        return result;
    }
}
