package de.wsorg.feeder.processor.production.domain.feeder.feed

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntriesCollection
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 22.01.13
 * Time: 09:45
 */
class FeedModelTest extends Specification {
    def "Apply already existing model"() {
        given:
        def model = new FeedModel()
        model.id = 'id'
        model.feedEntries = [new FeedEntryModel()]
        model.feedUrl = 'url'
        model.updated = new Date()
        model.logoUrl = 'logo'
        model.title = 'title'
        model.feedEntriesCollection = new FeedEntriesCollection()
        model.feedEntryModels = [new FeedEntryModel()]
        model.description = 'desc'

        and:
        def newModel = new FeedModel()

        when:
        newModel.applyFeedModel(model)

        then:
        model.id == newModel.id
        model.feedEntries == newModel.feedEntries
        model.feedUrl == newModel.feedUrl
        model.updated == newModel.updated
        model.logoUrl == newModel.logoUrl
        model.title == newModel.title
        model.feedEntriesCollection == newModel.feedEntriesCollection
        model.feedEntryModels == newModel.feedEntryModels
        model.description == newModel.description
    }

    def "Feed entry already exists"() {
        given:
        def entryId = '1'
        def feedModel = new FeedModel()
        def existingEntry = new FeedEntryModel(id: entryId)
        feedModel.addFeedEntry(existingEntry)

        when:
        def result = feedModel.containsFeedEntry(entryId)

        then:
        result
    }

    def "Feed entry does not exist"() {
        given:
        def entryId = '1'
        def feedModel = new FeedModel()
        def existingEntry = new FeedEntryModel(id: entryId)
        feedModel.addFeedEntry(existingEntry)

        when:
        def result = feedModel.containsFeedEntry('45')

        then:
        !result
    }

    def "Add entry at first place"() {
        given:
        def entryId = '1'
        def feedModel = new FeedModel()
        def existingEntry = new FeedEntryModel(id: entryId)
        feedModel.addFeedEntry(existingEntry)

        and:
        def entryToAdd = new FeedEntryModel(id: 'new')

        when:
        feedModel.addFeedEntryOnTop(entryToAdd)

        then:
        feedModel.getFeedEntries()[0].id == entryToAdd.id
    }
}