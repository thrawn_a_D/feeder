package de.wsorg.feeder.processor.storage.feed;

import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UsersFeedsStorage {
    List<UserRelatedFeedModel> getUserFeeds(final String userId);
    List<UserRelatedFeedModel> getUserFeeds(final String userId, final String titleQuery);
}
