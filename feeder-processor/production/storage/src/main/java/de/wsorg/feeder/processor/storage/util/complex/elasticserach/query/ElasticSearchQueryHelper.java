package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query;

import de.wsorg.feeder.processor.storage.util.complex.elasticserach.ElasticSearchConnectionManager;
import org.elasticsearch.action.ListenableActionFuture;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ElasticSearchQueryHelper<T> {
    @Inject
    private ElasticSearchConnectionManager elasticSearchConnectionManager;
    @Inject
    private QueryResultTransformer<T> queryResultTransformer;

    public List<T> processQuery(final QueryBuilder queryToProcess, final String indexName, final Class<T> modelType){
        final Client client = elasticSearchConnectionManager.getClient();

        final ListenableActionFuture<SearchResponse> feedModel = client.prepareSearch(indexName).setQuery(queryToProcess).execute();
        return queryResultTransformer.transformResultToModel(feedModel.actionGet(), modelType);
    }
}
