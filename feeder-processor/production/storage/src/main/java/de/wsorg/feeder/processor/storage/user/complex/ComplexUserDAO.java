package de.wsorg.feeder.processor.storage.user.complex;

import javax.inject.Inject;
import javax.inject.Named;

import com.couchbase.client.CouchbaseClient;
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.storage.user.UserDataAccess;
import de.wsorg.feeder.processor.storage.util.TimestampProvider;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager;
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ComplexUserDAO implements UserDataAccess {
    private static final String OBJECT_TYPE_NAME = "feederUseCaseUser";
    private static final int DEFAULT_OBJECT_EXPIRATION = 0;
    @Inject
    private FeederJsonTransformer<FeederUseCaseUser> jsonTransformer;
    @Inject
    private CouchbaseConnectionManager couchbaseConnectionManager;
    @Inject
    private StorageConfigProvider storageConfigProvider;

    private TimestampProvider timeStampProvider = new TimestampProvider();

    @Override
    public FeederUseCaseUser save(final FeederUseCaseUser feederUseCaseUser) {
        final String bucketName = storageConfigProvider.getCouchbaseConfig().getFeederUserBucketName();
        final CouchbaseClient couchbaseConnection = couchbaseConnectionManager.getConnection(bucketName);
        final String userIdAsTimestamp = timeStampProvider.getTimestampAsString();
        feederUseCaseUser.setUserId(userIdAsTimestamp);
        final String objectAsJson = jsonTransformer.getJsonForObject(feederUseCaseUser, OBJECT_TYPE_NAME);
        couchbaseConnection.add(feederUseCaseUser.getUserName(), DEFAULT_OBJECT_EXPIRATION, objectAsJson);
        return feederUseCaseUser;
    }

    @Override
    public FeederUseCaseUser findByNickName(final String nickName) {
        final String bucketName = storageConfigProvider.getCouchbaseConfig().getFeederUserBucketName();
        final CouchbaseClient couchbaseConnection = couchbaseConnectionManager.getConnection(bucketName);
        final String userDataAsJson = (String) couchbaseConnection.get(nickName);
        if (userDataAsJson != null) {
            return jsonTransformer.getObjectFromJson(userDataAsJson, FeederUseCaseUser.class);
        } else {
            return null;
        }
    }
}
