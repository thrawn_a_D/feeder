package de.wsorg.feeder.processor.storage.feed.complex;

import com.couchbase.client.CouchbaseClient;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.TermQueryBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedToUserAssociationDAO implements FeedToUserAssociationStorage {
    private static final int DEFAULT_OBJECT_EXPIRATION = 0;

    private static final String OBJECT_TYPE_NAME = "feedAssociationModel";

    @Inject
    private FeederJsonTransformer<FeedToUserAssociation> jsonTransformer;
    @Inject
    private ElasticSearchQueryHelper<FeedToUserAssociation> elasticSearchQueryHelper;
    @Inject
    private CouchbaseConnectionManager couchbaseConnectionManager;
    @Inject
    private StorageConfigProvider storageConfigProvider;
    @Inject
    private TermQueryWrapper termQueryWrapper;

    @Override
    public void save(final FeedToUserAssociation feedToUserAssociation) {
        validateInput(feedToUserAssociation);

        final String objectAsJson = jsonTransformer.getJsonForObject(feedToUserAssociation, OBJECT_TYPE_NAME);
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());

        final String storingKey = getModelStoreKey(feedToUserAssociation.getFeedUrl(), feedToUserAssociation.getUserId());

        couchbaseClient.set(storingKey, DEFAULT_OBJECT_EXPIRATION, objectAsJson);
    }

    @Override
    public List<FeedToUserAssociation> getUserFeeds(final String userId) {
        final TermQueryBuilder matchUserId = termQueryWrapper.getTermQueryBuilder("userId", userId);
        String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsAssociationIndexName();
        return elasticSearchQueryHelper.processQuery(matchUserId, indexName, FeedToUserAssociation.class);
    }

    @Override
    public List<FeedToUserAssociation> getAssociationsOfFeed(final String feedUrl) {
        final TermQueryBuilder matchFeedUrl = termQueryWrapper.getTermQueryBuilder("feedUrl", feedUrl);
        String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsAssociationIndexName();
        return elasticSearchQueryHelper.processQuery(matchFeedUrl, indexName, FeedToUserAssociation.class);
    }

    @Override
    public FeedToUserAssociation getUserFeedAssociation(final String feedUrl, final String userId) {
        final String storeKey = getModelStoreKey(feedUrl, userId);
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        final String modelAsJson = (String) couchbaseClient.get(storeKey);
        if (StringUtils.isNotBlank(modelAsJson)) {
            return jsonTransformer.getObjectFromJson(modelAsJson, FeedToUserAssociation.class);
        } else {
            return null;
        }
    }

    @Override
    public boolean isAssociationGiven(final String feedUrl, final String userId, final AssociationType associationType) {
        final FeedToUserAssociation feedToUserAssociation = this.getUserFeedAssociation(feedUrl, userId);
        return feedToUserAssociation != null && feedToUserAssociation.getAssociationType() == associationType;
    }

    @Override
    public void removeAssociation(final String feedUrl, final String userId) {
        final String storeKey = getModelStoreKey(feedUrl, userId);
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());

        couchbaseClient.delete(storeKey);
    }

    private String getModelStoreKey(final String feedUrl, final String userId) {
        return userId + '_' + feedUrl;
    }

    private void validateInput(final FeedToUserAssociation feedToUserAssociation) {
        if(StringUtils.isBlank(feedToUserAssociation.getUserId()) ||
                StringUtils.isBlank(feedToUserAssociation.getFeedUrl())) {
            throw new IllegalArgumentException("The provided feed model to be stored does not have a feed url or/and userId set.");
        }
    }

    private String getCouchbaseBucketName() {
        return storageConfigProvider.getCouchbaseConfig().getFeedAssociationBucketName();
    }
}
