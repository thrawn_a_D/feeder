package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class StringQueryWrapper {
    public QueryStringQueryBuilder getQueryStringBuilder(final String queryString) {
        return QueryBuilders.queryString(queryString);
    }
}
