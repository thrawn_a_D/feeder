package de.wsorg.feeder.processor.storage.util.complex.json;

import com.google.gson.Gson;

import javax.inject.Named;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CommonJsonTransformer<T> implements FeederJsonTransformer<T> {

    private Gson gsonTransformer = new Gson();

    @Override
    public String getJsonForObject(final T objectToSerialize, final String objectType) {
        String transformationResult = gsonTransformer.toJson(objectToSerialize);

        transformationResult = addObjectTypeToJsonString(objectType, transformationResult);

        return transformationResult;
    }

    private String addObjectTypeToJsonString(final String objectType, String transformationResult) {
        transformationResult = transformationResult.replaceFirst("\\{", "{\"type\":\"" + objectType + "\",");
        return transformationResult;
    }

    @Override
    public T getObjectFromJson(final String objectAsJson, final Class<T> objectType) {
        return gsonTransformer.fromJson(objectAsJson, objectType);
    }

    @Override
    public T getObjectFromMap(final Map objectAsMap, final Class<T> objectType) {
        final String asJson = gsonTransformer.toJson(objectAsMap);
        return gsonTransformer.fromJson(asJson, objectType);
    }
}
