package de.wsorg.feeder.processor.storage.feed;

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class FeedStorageFactory {
    private static ClassPathXmlApplicationContext applicationContext;
    public static boolean enableFileBasedStorage=false;

    private static boolean configProvided=false;

    public static FeedDataAccess getFeedDAO() {
        validateState();
        return getApplicationContext().getBean(FeedDataAccess.class);
    }

    public static UsersFeedsStorage getUserFeedsDAO() {
        validateState();
        return getApplicationContext().getBean(UsersFeedsStorage.class);
    }

    public static FeedToUserAssociationStorage getFeedToUserAssociationDAO() {
        validateState();
        return getApplicationContext().getBean(FeedToUserAssociationStorage.class);
    }

    public static FeedLoadStatisticsDataAccess getFeedLoadStatisticsDataAccess() {
        validateState();
        return getApplicationContext().getBean(FeedLoadStatisticsDataAccess.class);
    }

    private static ClassPathXmlApplicationContext getApplicationContext(){
        if (applicationContext == null) {
            String storageConfiguration="classpath:spring/feed/feed-couchbase-storage-context.xml";
            if (enableFileBasedStorage) {
                storageConfiguration = "classpath:spring/feed/feed-filebased-storage-context.xml";
            }
            applicationContext = new ClassPathXmlApplicationContext(new String[] {storageConfiguration});
        }

        return applicationContext;
    }

    private static void validateState() {
        if(!configProvided)
            throw new IllegalStateException("Please provide a valid storage configuration first");
    }

    public static void setClientConfig(final StorageConfig storageConfig) {
        if(!getApplicationContext().containsBean("couchbaseConfig")) {
            getApplicationContext().getBeanFactory().registerSingleton("couchbaseConfig", storageConfig.getCouchbaseConfig());
        }
        if(!getApplicationContext().containsBean("elasticSearchConfig")) {
            getApplicationContext().getBeanFactory().registerSingleton("elasticSearchConfig", storageConfig.getElasticSearchConfig());
        }

        configProvided = true;
    }

    public static void setEnableFileBasedStorage(final boolean enableFileBasedStorage) {
        FeedStorageFactory.enableFileBasedStorage = enableFileBasedStorage;
    }
}
