package de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class TransportClientWrapper {
    public TransportClient getTransportClient(final Settings settings) {
        return new TransportClient(settings);
    }
}
