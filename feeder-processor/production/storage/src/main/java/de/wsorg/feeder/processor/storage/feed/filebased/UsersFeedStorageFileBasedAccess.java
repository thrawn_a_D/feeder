package de.wsorg.feeder.processor.storage.feed.filebased;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UsersFeedStorageFileBasedAccess implements UsersFeedsStorage {

    @Inject
    private FeedDataAccess feedDataAccess;
    @Inject
    private FeedToUserAssociationStorage feedToUserAssociationStorage;

    @Override
    public List<UserRelatedFeedModel> getUserFeeds(final String userId) {
        List<UserRelatedFeedModel> foundUserFeeds = new ArrayList<>();

        List<FeedToUserAssociation> userFeedAssociations = feedToUserAssociationStorage.getUserFeeds(userId);
        for (FeedToUserAssociation userFeedAssociation : userFeedAssociations) {
            FeedModel userFeed = feedDataAccess.findByFeedUrl(userFeedAssociation.getFeedUrl());
            foundUserFeeds.add(new UserRelatedFeedModel(userFeed, userFeedAssociation));
        }
        return foundUserFeeds;
    }

    @Override
    public List<UserRelatedFeedModel> getUserFeeds(final String userId, final String titleQuery) {
        List<UserRelatedFeedModel> foundUserFeeds = new ArrayList<>();

        List<FeedToUserAssociation> userFeedAssociations = feedToUserAssociationStorage.getUserFeeds(userId);
        for (FeedToUserAssociation userFeedAssociation : userFeedAssociations) {
            FeedModel userFeed = feedDataAccess.findByFeedUrlAndTitleOrDescription(userFeedAssociation.getFeedUrl(), titleQuery);
            if(userFeed != null)
                foundUserFeeds.add(new UserRelatedFeedModel(userFeed, userFeedAssociation));
        }
        return foundUserFeeds;
    }
}
