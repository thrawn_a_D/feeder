package de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper;

import org.elasticsearch.common.settings.ImmutableSettings;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ImmutableSettingsWrapper {
    public ImmutableSettings.Builder getSettingsBuilder() {
        return ImmutableSettings.settingsBuilder();
    }
}
