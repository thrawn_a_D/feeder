package de.wsorg.feeder.processor.storage.util.complex.json;

import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeederJsonTransformer<T> {
    String getJsonForObject(final T objectToSerialize, final String objectType);
    T getObjectFromJson(final String objectAsJson, Class<T> objectType);
    T getObjectFromMap(final Map objectAsMap, Class<T> objectType);
}
