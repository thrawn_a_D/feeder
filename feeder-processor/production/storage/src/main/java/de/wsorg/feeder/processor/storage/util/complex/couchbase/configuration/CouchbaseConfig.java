package de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration;

import de.wsorg.feeder.processor.storage.util.complex.config.common.Host;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class CouchbaseConfig {
    public static final String COUCHBASE_BUCKET_NAME_FEEDS = "couchbase.bucket.name.feeds";
    public static final String COUCHBASE_BUCKET_NAME_ASSOCIATIONS = "couchbase.bucket.name.associations";
    public static final String COUCHBASE_BUCKET_NAME_USER = "couchbase.bucket.name.user";
    public static final String COUCHBASE_BUCKET_NAME_LOAD_STATISTICS = "couchbase.bucket.name.loadStatistics";
    public static final String COUCHBASE_BUCKET_PASSWORD = "couchbase.bucket.password";
    public static final String COUCHBASE_HOSTS = "couchbase.hosts";

    private List<Host> couchbaseHosts = new ArrayList<>();
    private String feedsBucketName;
    private String feedAssociationBucketName;
    private String feederUserBucketName;
    private String loadStatisticsBucketName;
    private String couchbasePassword;

    public CouchbaseConfig() {
    }

    public CouchbaseConfig(final Properties storageProperties) {
        feedsBucketName = getPropertyValue(storageProperties, COUCHBASE_BUCKET_NAME_FEEDS);
        feedAssociationBucketName = getPropertyValue(storageProperties, COUCHBASE_BUCKET_NAME_ASSOCIATIONS);
        feederUserBucketName = getPropertyValue(storageProperties, COUCHBASE_BUCKET_NAME_USER);
        loadStatisticsBucketName = getPropertyValue(storageProperties, COUCHBASE_BUCKET_NAME_LOAD_STATISTICS);
        couchbasePassword = getPropertyValue(storageProperties, COUCHBASE_BUCKET_PASSWORD);
        addSearchHosts(storageProperties);
    }

    private void addSearchHosts(final Properties storageProperties) {
        String searchHostsString = storageProperties.getProperty(COUCHBASE_HOSTS);
        if (StringUtils.isNotBlank(searchHostsString)) {
            String[] hostsArray = searchHostsString.split(";");
            for (String host : hostsArray) {
                String[] hostPort = host.split(":");
                this.addCouchbaseHost(hostPort[0], hostPort[1]);
            }
        }
    }

    public void addCouchbaseHost(final String host, final String port) {
        Host couchbaseHost = new Host();
        couchbaseHost.setHost(host);
        couchbaseHost.setPort(port);
        couchbaseHosts.add(couchbaseHost);
    }

    private String getPropertyValue(final Properties storageProperties, final String propertyName) {
        return storageProperties.getProperty(propertyName);
    }

    public List<URI> getCouchbaseHostUris() {
        List<URI> result = new ArrayList<>();
        for (Host couchbaseHost : couchbaseHosts) {
            result.add(URI.create("http://"+couchbaseHost.getHost()+":"+couchbaseHost.getPort()+"/pools"));
        }

        return result;
    }
}
