package de.wsorg.feeder.processor.storage.feed.filebased;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedFileBasedStorage implements FeedDataAccess {

    private static final String STORAGE_FOLDER_FOR_FEEDS = "feeds" + File.separator;
    @Inject
    private FileBasedPersistence<FeedModel> fileBasedPersistence;

    @Override
    public FeedModel save(final FeedModel feedModel) {
        fileBasedPersistence.save(STORAGE_FOLDER_FOR_FEEDS, feedModel.getFeedUrl(), feedModel);
        return feedModel;
    }

    @Override
    public FeedModel findByFeedUrl(final String feedUrl) {
        return fileBasedPersistence.getModelByFilePath(STORAGE_FOLDER_FOR_FEEDS, feedUrl);
    }

    @Override
    public List<FeedModel> findByTitleOrDescription(final String textContainingInTitleOrDescription) {
        List<FeedModel> foundResult = new ArrayList<>();

        if(StringUtils.isNotBlank(textContainingInTitleOrDescription)){
            List<FeedModel> localFeeds = this.findAllLocalFeeds();

            for (FeedModel localFeed : localFeeds) {
                if(feedMatchesQuery(textContainingInTitleOrDescription, localFeed))
                    foundResult.add(localFeed);
            }
        }

        return foundResult;
    }

    @Override
    public FeedModel findByFeedUrlAndTitleOrDescription(final String feedUrl, final String textContainingInTitleOrDescription) {
        final FeedModel feedWithUrl = this.findByFeedUrl(feedUrl);
        if(feedMatchesQuery(textContainingInTitleOrDescription, feedWithUrl))
            return feedWithUrl;
        else
            return null;
    }

    private boolean feedMatchesQuery(final String textContainingInTitleOrDescription, final FeedModel localFeed) {
        return StringUtils.containsIgnoreCase(localFeed.getTitle(), textContainingInTitleOrDescription) ||
           StringUtils.containsIgnoreCase(localFeed.getDescription(), textContainingInTitleOrDescription);
    }

    @Override
    public List<FeedModel> findAllLocalFeeds() {
        return fileBasedPersistence.getFolderContent(STORAGE_FOLDER_FOR_FEEDS);
    }

    @Override
    public void deleteFeed(final String feedUrl) {
        fileBasedPersistence.remove(STORAGE_FOLDER_FOR_FEEDS, feedUrl);
    }
}
