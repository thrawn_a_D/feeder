package de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration;

import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ElasticSearchConfigVerifier implements ConfigVerifier {
    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult result = new ConfigVerificationResult(configurationFilePath);

        final String clusterName = configProperties.getProperty(ElasticSearchConfig.ELASTIC_SEARCH_CLUSTER_NAME);
        final String searchIndexFeeds = configProperties.getProperty(ElasticSearchConfig.ELASTIC_SEARCH_INDEX_FEEDS);
        final String searchIndexAssociation = configProperties.getProperty(ElasticSearchConfig.ELASTIC_SEARCH_INDEX_ASSOCIATION);
        final String searchIndexLoadStats = configProperties.getProperty(ElasticSearchConfig.ELASTIC_SEARCH_INDEX_LOAD_STATS);
        final String searchHosts = configProperties.getProperty(ElasticSearchConfig.ELASTIC_SEARCH_HOSTS);

        verifyProperty(result, ElasticSearchConfig.ELASTIC_SEARCH_CLUSTER_NAME, clusterName);
        verifyProperty(result, ElasticSearchConfig.ELASTIC_SEARCH_INDEX_FEEDS, searchIndexFeeds);
        verifyProperty(result, ElasticSearchConfig.ELASTIC_SEARCH_INDEX_ASSOCIATION, searchIndexAssociation);
        verifyProperty(result, ElasticSearchConfig.ELASTIC_SEARCH_INDEX_LOAD_STATS, searchIndexLoadStats);
        verifyProperty(result, ElasticSearchConfig.ELASTIC_SEARCH_HOSTS, searchHosts);

        validateHostsProperty(result, searchHosts);

        return result;
    }

    private void validateHostsProperty(final ConfigVerificationResult result, final String couchbaseHostsProperty) {
        if(!result.getPropertyErrors().containsKey(ElasticSearchConfig.ELASTIC_SEARCH_HOSTS) &&
                (!couchbaseHostsProperty.contains(";") || !couchbaseHostsProperty.contains(":"))) {
            result.addVerificationError(ElasticSearchConfig.ELASTIC_SEARCH_HOSTS, "Hosts must be separated by a ; and port should be separated by : (e.g. host:port;)");
        }
    }

    private void verifyProperty(final ConfigVerificationResult result, final String validatingPropertyName, final String validatingPropertyValue) {
        if(StringUtils.isBlank(validatingPropertyValue)) {
            result.addVerificationError(validatingPropertyName, "Property does not contain a valid value.");
        }
    }
}
