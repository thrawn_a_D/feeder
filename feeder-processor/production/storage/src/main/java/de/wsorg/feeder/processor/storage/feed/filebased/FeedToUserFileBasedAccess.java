package de.wsorg.feeder.processor.storage.feed.filebased;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import org.apache.commons.lang.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedToUserFileBasedAccess implements FeedToUserAssociationStorage {

    private static final String STORAGE_FOLDER_FOR_ASSOCIATIONS = "associations" + File.separator;

    @Inject
    private FileBasedPersistence<FeedToUserAssociation> fileBasedPersistence;

    @Override
    public void save(final FeedToUserAssociation feedToUserAssociation) {

        validateInput(feedToUserAssociation);

        String identifier = getIdentifierOfAssociation(feedToUserAssociation.getFeedUrl(), feedToUserAssociation.getUserId());
        fileBasedPersistence.save(STORAGE_FOLDER_FOR_ASSOCIATIONS, identifier, feedToUserAssociation);
    }

    private void validateInput(final FeedToUserAssociation feedToUserAssociation) {
        if(StringUtils.isBlank(feedToUserAssociation.getUserId()))
            throw new IllegalArgumentException("The provided association has no set userId");
        if(StringUtils.isBlank(feedToUserAssociation.getFeedUrl()))
            throw new IllegalArgumentException("The provided association has no set feedUrl");
    }

    @Override
    public List<FeedToUserAssociation> getUserFeeds(final String userId) {
        List<FeedToUserAssociation> foundUserAssociations = new ArrayList<>();
        List<FeedToUserAssociation> allAssociations = this.fileBasedPersistence.getFolderContent(STORAGE_FOLDER_FOR_ASSOCIATIONS);

        if(allAssociations != null) {
            for (FeedToUserAssociation currentAssocation : allAssociations) {
                if(currentAssocation.getUserId().equals(userId))
                    foundUserAssociations.add(currentAssocation);
            }
        }

        return foundUserAssociations;
    }

    @Override
    public List<FeedToUserAssociation> getAssociationsOfFeed(final String feedUrl) {
        List<FeedToUserAssociation> foundFeedAssociations = new ArrayList<>();
        List<FeedToUserAssociation> allAssociations = this.fileBasedPersistence.getFolderContent(STORAGE_FOLDER_FOR_ASSOCIATIONS);

        if(allAssociations != null) {
            for (FeedToUserAssociation currentAssocation : allAssociations) {
                if(currentAssocation.getFeedUrl().equals(feedUrl))
                    foundFeedAssociations.add(currentAssocation);
            }
        }

        return foundFeedAssociations;
    }

    @Override
    public FeedToUserAssociation getUserFeedAssociation(final String feedUrl, final String userId) {
        List<FeedToUserAssociation> userFeeds = getUserFeeds(userId);

        for (FeedToUserAssociation userFeed : userFeeds) {
            if(userFeed.getFeedUrl().equals(feedUrl))
                return userFeed;
        }

        return null;
    }

    @Override
    public boolean isAssociationGiven(final String feedUrl, final String userId, final AssociationType associationType) {
        List<FeedToUserAssociation> userAssociations = getUserFeeds(userId);
        for (FeedToUserAssociation userAssociation : userAssociations) {
            if(userAssociation.getFeedUrl().equals(feedUrl) && userAssociation.getAssociationType() == associationType)
                return true;
        }

        return false;
    }

    @Override
    public void removeAssociation(final String feedUrl, final String userId) {
        FeedToUserAssociation userAssociation = getUserFeedAssociation(feedUrl, userId);
        if(userAssociation != null){
            String identifier = getIdentifierOfAssociation(feedUrl, userId);
            fileBasedPersistence.remove(STORAGE_FOLDER_FOR_ASSOCIATIONS, identifier);
        }
    }

    private String getIdentifierOfAssociation(final String feedUrl, final String userId) {
        return userId + "_" + feedUrl;
    }
}
