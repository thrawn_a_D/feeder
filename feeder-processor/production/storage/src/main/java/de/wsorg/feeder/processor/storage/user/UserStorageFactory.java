package de.wsorg.feeder.processor.storage.user;

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class UserStorageFactory {
    private static ClassPathXmlApplicationContext applicationContext;
    private static boolean enableFileBasedStorage=false;

    private static boolean configProvided=false;

    public static UserDataAccess getUserDAO() {
        validateState();
        return getApplicationContext().getBean(UserDataAccess.class);
    }

    private static ClassPathXmlApplicationContext getApplicationContext(){
        if (applicationContext == null) {
            String storageConfiguration="classpath:spring/user/user-couchbase-storage-context.xml";
            if (enableFileBasedStorage) {
                storageConfiguration = "classpath:spring/user/user-filebased-storage-context.xml";
            }
            applicationContext = new ClassPathXmlApplicationContext(new String[] {storageConfiguration});
        }

        return applicationContext;
    }

    private static void validateState() {
        if(!configProvided)
            throw new IllegalStateException("Please provide a valid storage configuration first");
    }

    public static void setClientConfig(final StorageConfig storageConfig) {
        if(!getApplicationContext().containsBean("couchbaseConfig")) {
            getApplicationContext().getBeanFactory().registerSingleton("couchbaseConfig", storageConfig.getCouchbaseConfig());
        }
        if(!getApplicationContext().containsBean("elasticSearchConfig")) {
            getApplicationContext().getBeanFactory().registerSingleton("elasticSearchConfig", storageConfig.getElasticSearchConfig());
        }

        configProvided = true;
    }

    public static void setEnableFileBasedStorage(final boolean enableFileBasedStorage) {
        UserStorageFactory.enableFileBasedStorage = enableFileBasedStorage;
    }
}
