package de.wsorg.feeder.processor.storage.feed;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;

import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedLoadStatisticsDataAccess {
    FeedLoadStatistic getFeedLoadStatistics(final String feedUrl);
    void updateFeedLoadStatistics(final FeedLoadStatistic feedLoadStatistic);
    void addNewFeedLoadStatistic(final String feedUrl);
    List<FeedLoadStatistic> findLoadStatisticsOlderThen(final Date feedLoadedOlderThen);
    void removeFeedLoadStatistic(final String feedUrl);
}
