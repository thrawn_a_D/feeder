package de.wsorg.feeder.processor.storage.feed;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedDataAccess {
    FeedModel save(final FeedModel feedModel);
    FeedModel findByFeedUrl(final String feedUrl);
    List<FeedModel> findByTitleOrDescription(final String textContainingInTitleOrDescription);
    FeedModel findByFeedUrlAndTitleOrDescription(final String feedUrl, final String textContainingInTitleOrDescription);
    List<FeedModel> findAllLocalFeeds();
    void deleteFeed(final String feedUrl);
}
