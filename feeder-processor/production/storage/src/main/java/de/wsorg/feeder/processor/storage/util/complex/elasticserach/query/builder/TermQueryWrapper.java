package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class TermQueryWrapper {
    public TermQueryBuilder getTermQueryBuilder(final String field, final Object value) {
        return QueryBuilders.termQuery(field, value);
    }
}
