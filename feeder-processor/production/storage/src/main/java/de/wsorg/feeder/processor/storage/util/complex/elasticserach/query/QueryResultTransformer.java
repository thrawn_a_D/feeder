package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query;

import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class QueryResultTransformer<T> {
    @Inject
    private FeederJsonTransformer<T> jsonTransformer;

    public List<T> transformResultToModel(final SearchResponse searchResponse, final Class<T> objectType) {
        List<T> result = new ArrayList<>();

        if (searchResponse.getHits().getTotalHits() > 0) {
            for (SearchHit searchHit : searchResponse.getHits().getHits()) {
                Map document = (Map) searchHit.getSource().get("doc");
                T modelObject = jsonTransformer.getObjectFromMap(document, objectType);
                result.add(modelObject);
            }
        }
        return result;
    }
}
