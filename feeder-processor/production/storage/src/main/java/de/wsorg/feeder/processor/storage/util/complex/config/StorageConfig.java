package de.wsorg.feeder.processor.storage.util.complex.config;

import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class StorageConfig {
    private static final String FILE_SYSTEM_AS_STORAGE = "useFileSystemAsStorage";

    private CouchbaseConfig couchbaseConfig;
    private ElasticSearchConfig elasticSearchConfig;
    private boolean useFileSystemAsStorage;

    public StorageConfig() {
    }

    public StorageConfig(final Properties storageProperties) {
        this.couchbaseConfig = new CouchbaseConfig(storageProperties);
        this.elasticSearchConfig = new ElasticSearchConfig(storageProperties);

        setUseFileSystemAsStorageProperty(storageProperties);
    }

    private void setUseFileSystemAsStorageProperty(final Properties storageProperties) {
        final String useFileSystemPropertyValue = storageProperties.getProperty(FILE_SYSTEM_AS_STORAGE);
        if(StringUtils.isNotBlank(useFileSystemPropertyValue)) {
            useFileSystemAsStorage = Boolean.parseBoolean(useFileSystemPropertyValue);
        }
    }
}
