package de.wsorg.feeder.processor.storage.util.complex.config;

import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class StorageConfigProvider {
    @Inject
    private ApplicationContext applicationContext;

    public CouchbaseConfig getCouchbaseConfig() {
        return applicationContext.getBean(CouchbaseConfig.class);
    }

    public ElasticSearchConfig getElasticSearchConfig() {
        return applicationContext.getBean(ElasticSearchConfig.class);
    }
}
