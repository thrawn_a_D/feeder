package de.wsorg.feeder.processor.storage.feed.complex;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.View;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.query.CouchbaseQueryHelper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.BoolQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.MultimatchQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ComplexFeedDAO implements FeedDataAccess {
    private static final String OBJECT_TYPE_NAME = "feedModel";
    private static final int DEFAULT_OBJECT_EXPIRATION = 0;
    @Inject
    private FeederJsonTransformer<FeedModel> jsonTransformer;
    @Inject
    private CouchbaseConnectionManager couchbaseConnectionManager;
    @Inject
    private CouchbaseQueryHelper<FeedModel> couchbaseQueryHelper;
    @Inject
    private ElasticSearchQueryHelper<FeedModel> elasticSearchQueryHelper;
    @Inject
    private StorageConfigProvider storageConfigProvider;
    @Inject
    private MultimatchQueryWrapper multimatchQueryWrapper;
    @Inject
    private BoolQueryWrapper boolQueryWrapper;
    @Inject
    private TermQueryWrapper termQueryWrapper;

    @Override
    public FeedModel save(final FeedModel feedModel) {
        validateKeyIsSet(feedModel);

        final String objectAsJson = jsonTransformer.getJsonForObject(feedModel, OBJECT_TYPE_NAME);
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        couchbaseClient.set(feedModel.getFeedUrl(), DEFAULT_OBJECT_EXPIRATION, objectAsJson);
        return feedModel;
    }

    @Override
    public FeedModel findByFeedUrl(final String feedUrl) {
        final CouchbaseClient connection = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        final String objectAsJson = (String) connection.get(feedUrl);
        return jsonTransformer.getObjectFromJson(objectAsJson, FeedModel.class);
    }

    @Override
    public List<FeedModel> findByTitleOrDescription(final String textContainingInTitleOrDescription) {

        QueryBuilder multivalueBuilder = getContainsTitleOrDescriptionQuery(textContainingInTitleOrDescription);
        String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsIndexName();

        return elasticSearchQueryHelper.processQuery(multivalueBuilder, indexName, FeedModel.class);
    }

    @Override
    public FeedModel findByFeedUrlAndTitleOrDescription(final String feedUrl, final String textContainingInTitleOrDescription) {
        final TermQueryBuilder matchFeedUrl = termQueryWrapper.getTermQueryBuilder("_id", feedUrl);
        final QueryBuilder containsTitleOrDescriptionQuery = getContainsTitleOrDescriptionQuery(textContainingInTitleOrDescription);
        QueryBuilder searchQuery = boolQueryWrapper.getBoolQueryBuilder().must(matchFeedUrl).must(containsTitleOrDescriptionQuery);

        String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsIndexName();

        final List<FeedModel> feedModels = elasticSearchQueryHelper.processQuery(searchQuery, indexName, FeedModel.class);

        if(feedModels.size() > 0) {
            return feedModels.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<FeedModel> findAllLocalFeeds() {
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());

        View view = couchbaseClient.getView("dev_feed_title_or_description", "feed_title_or_description");
        Query query = new Query();
        query.setIncludeDocs(true);

        return couchbaseQueryHelper.findDataInViewRelatedToQuery(couchbaseClient, view, query, FeedModel.class);
    }

    @Override
    public void deleteFeed(final String feedUrl) {
        final CouchbaseClient connection = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        connection.delete(feedUrl);
    }

    private String getCouchbaseBucketName() {
        return storageConfigProvider.getCouchbaseConfig().getFeedsBucketName();
    }

    private QueryBuilder getContainsTitleOrDescriptionQuery(final String textContainingInTitleOrDescription) {
        return multimatchQueryWrapper.getMultiMatchQuery(textContainingInTitleOrDescription, "doc.title", "description");
    }

    private void validateKeyIsSet(final FeedModel feedModel) {
        if(StringUtils.isBlank(feedModel.getFeedUrl())) {
            throw new IllegalArgumentException("The provided feed model to be stored does not have a feed url set.");
        }
    }
}
