package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class BoolQueryWrapper {
    public BoolQueryBuilder getBoolQueryBuilder() {
        return QueryBuilders.boolQuery();
    }
}
