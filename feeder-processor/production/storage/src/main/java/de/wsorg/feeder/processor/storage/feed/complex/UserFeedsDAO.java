package de.wsorg.feeder.processor.storage.feed.complex;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.user.UserRelatedFeedModel;
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage;
import de.wsorg.feeder.processor.storage.feed.UsersFeedsStorage;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.BoolQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.StringQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.TermQueryBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserFeedsDAO implements UsersFeedsStorage {
    @Inject
    private FeedToUserAssociationStorage userAssociationDAO;
    @Inject
    private BoolQueryWrapper boolQueryWrapper;
    @Inject
    private TermQueryWrapper termQueryWrapper;
    @Inject
    private StringQueryWrapper stringQueryWrapper;
    @Inject
    private StorageConfigProvider storageConfigProvider;
    @Inject
    private ElasticSearchQueryHelper<FeedModel> elasticSearchQueryHelper;

    @Override
    public List<UserRelatedFeedModel> getUserFeeds(final String userId) {
        final Map<String, FeedToUserAssociation> matchFeedUrlToAssociation = new HashMap<>();

        final List<FeedToUserAssociation> associationsOfUser = userAssociationDAO.getUserFeeds(userId);

        if(associationsOfUser != null && associationsOfUser.size() > 0) {
            final BoolQueryBuilder boolQueryBuilder = prepareQeuryForFeeds(matchFeedUrlToAssociation, associationsOfUser);

            final List<FeedModel> feedModels = getFeedsOfUser(boolQueryBuilder);

            return mapFeedsToUserRelatedFeedModel(matchFeedUrlToAssociation, feedModels);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<UserRelatedFeedModel> getUserFeeds(final String userId, final String titleQuery) {
        final Map<String, FeedToUserAssociation> matchFeedUrlToAssociation = new HashMap<>();

        final List<FeedToUserAssociation> associationsOfUser = userAssociationDAO.getUserFeeds(userId);

        if(associationsOfUser != null && associationsOfUser.size() > 0) {
            final BoolQueryBuilder boolQueryBuilder = prepareQeuryForFeeds(matchFeedUrlToAssociation, associationsOfUser);

            QueryStringQueryBuilder titleStringQuery = stringQueryWrapper.getQueryStringBuilder(titleQuery);
            titleStringQuery.field("doc.title");
            boolQueryBuilder.must(titleStringQuery);

            final List<FeedModel> feedModels = getFeedsOfUser(boolQueryBuilder);

            return mapFeedsToUserRelatedFeedModel(matchFeedUrlToAssociation, feedModels);
        } else {
            return new ArrayList<>();
        }
    }

    private List<UserRelatedFeedModel> mapFeedsToUserRelatedFeedModel(final Map<String, FeedToUserAssociation> matchFeedUrlToAssociation, final List<FeedModel> feedModels) {
        final List<UserRelatedFeedModel> result = new ArrayList<>();

        for (FeedModel feedModel : feedModels) {
            FeedToUserAssociation association = matchFeedUrlToAssociation.get(feedModel.getFeedUrl());
            UserRelatedFeedModel userRelatedFeedModel = new UserRelatedFeedModel(feedModel, association);
            result.add(userRelatedFeedModel);
        }

        return result;
    }

    private List<FeedModel> getFeedsOfUser(final BoolQueryBuilder boolQueryBuilder) {
        final String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsIndexName();

        return elasticSearchQueryHelper.processQuery(boolQueryBuilder, indexName, FeedModel.class);
    }

    private BoolQueryBuilder prepareQeuryForFeeds(final Map<String, FeedToUserAssociation> matchFeedUrlToAssociation, final List<FeedToUserAssociation> associationsOfUser) {
        final BoolQueryBuilder boolQueryBuilder = boolQueryWrapper.getBoolQueryBuilder();

        for (FeedToUserAssociation feedToUserAssociation : associationsOfUser) {
            final String feedUrl = feedToUserAssociation.getFeedUrl();
            matchFeedUrlToAssociation.put(feedUrl, feedToUserAssociation);
            final TermQueryBuilder matchQuery = termQueryWrapper.getTermQueryBuilder("_id", feedUrl);
            boolQueryBuilder.should(matchQuery);
        }
        return boolQueryBuilder;
    }
}
