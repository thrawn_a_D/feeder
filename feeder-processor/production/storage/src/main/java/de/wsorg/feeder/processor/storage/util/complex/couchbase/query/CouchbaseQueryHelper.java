package de.wsorg.feeder.processor.storage.util.complex.couchbase.query;

import com.couchbase.client.CouchbaseClient;
import com.couchbase.client.protocol.views.Query;
import com.couchbase.client.protocol.views.View;
import com.couchbase.client.protocol.views.ViewResponse;
import com.couchbase.client.protocol.views.ViewRow;
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CouchbaseQueryHelper<T> {
    @Inject
    private FeederJsonTransformer<T> jsonTransformer;

    public List<T> findDataInViewRelatedToQuery(final CouchbaseClient couchbaseClient,
                                                 final View view,
                                                 final Query myQuery,
                                                 final Class<T> modelType) {
        List<T> result = new ArrayList<>();
        ViewResponse queryResults = couchbaseClient.query(view, myQuery);
        Iterator<ViewRow> foundEntries = queryResults.iterator();

        while (foundEntries.hasNext()) {
            ViewRow next = foundEntries.next();
            result.add(jsonTransformer.getObjectFromJson((String) next.getDocument(), modelType));
        }
        return result;
    }
}
