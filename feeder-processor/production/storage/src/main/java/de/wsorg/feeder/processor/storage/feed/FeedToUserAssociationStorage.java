package de.wsorg.feeder.processor.storage.feed;

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedToUserAssociationStorage {
    void save(final FeedToUserAssociation feedToUserAssociation);
    List<FeedToUserAssociation> getUserFeeds(final String userId);
    List<FeedToUserAssociation> getAssociationsOfFeed(final String feedUrl);
    FeedToUserAssociation getUserFeedAssociation(final String feedUrl, final String userId);
    boolean isAssociationGiven(final String feedUrl, final String userId, final AssociationType associationType);
    void removeAssociation(final String feedUrl, final String userId);
}
