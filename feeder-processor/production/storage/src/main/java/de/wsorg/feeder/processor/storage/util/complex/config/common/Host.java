package de.wsorg.feeder.processor.storage.util.complex.config.common;

import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class Host {
    private String host;
    private String port;
}
