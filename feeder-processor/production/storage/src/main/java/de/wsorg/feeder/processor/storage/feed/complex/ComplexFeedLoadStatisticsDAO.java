package de.wsorg.feeder.processor.storage.feed.complex;

import com.couchbase.client.CouchbaseClient;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.RangeQueryWrapper;
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer;
import org.elasticsearch.index.query.RangeQueryBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ComplexFeedLoadStatisticsDAO implements FeedLoadStatisticsDataAccess {
    private static final String OBJECT_TYPE_NAME = "feedLoadStatistic";
    private static final int DEFAULT_OBJECT_EXPIRATION = 0;
    @Inject
    private FeederJsonTransformer<FeedLoadStatistic> jsonTransformer;
    @Inject
    private CouchbaseConnectionManager couchbaseConnectionManager;
    @Inject
    private StorageConfigProvider storageConfigProvider;
    @Inject
    private ElasticSearchQueryHelper<FeedLoadStatistic> elasticSearchQueryHelper;
    @Inject
    private RangeQueryWrapper rangeQueryWrapper;


    @Override
    public FeedLoadStatistic getFeedLoadStatistics(final String feedUrl) {
        final CouchbaseClient connection = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        final String objectAsJson = (String) connection.get(feedUrl);
        return jsonTransformer.getObjectFromJson(objectAsJson, FeedLoadStatistic.class);
    }

    @Override
    public void updateFeedLoadStatistics(final FeedLoadStatistic feedLoadStatistic) {
        final String objectAsJson = jsonTransformer.getJsonForObject(feedLoadStatistic, OBJECT_TYPE_NAME);
        final CouchbaseClient couchbaseClient = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        couchbaseClient.set(feedLoadStatistic.getFeedUrl(), DEFAULT_OBJECT_EXPIRATION, objectAsJson);
    }

    @Override
    public void addNewFeedLoadStatistic(final String feedUrl) {
        final FeedLoadStatistic newFeedLoadStatistic = new FeedLoadStatistic();
        newFeedLoadStatistic.setFeedUrl(feedUrl);
        updateFeedLoadStatistics(newFeedLoadStatistic);
    }

    @Override
    public List<FeedLoadStatistic> findLoadStatisticsOlderThen(final Date feedLoadedOlderThen) {
        RangeQueryBuilder loadUpdatedToCondition = rangeQueryWrapper.getRangeQueryBuilder("lastLoadedAsLong").to(feedLoadedOlderThen.getTime());

        String indexName = storageConfigProvider.getElasticSearchConfig().getFeedsLoadStatisticsIndexName();

        return elasticSearchQueryHelper.processQuery(loadUpdatedToCondition, indexName, FeedLoadStatistic.class);
    }

    @Override
    public void removeFeedLoadStatistic(final String feedUrl) {
        final CouchbaseClient connection = couchbaseConnectionManager.getConnection(getCouchbaseBucketName());
        connection.delete(feedUrl);
    }

    private String getCouchbaseBucketName() {
        return storageConfigProvider.getCouchbaseConfig().getLoadStatisticsBucketName();
    }
}
