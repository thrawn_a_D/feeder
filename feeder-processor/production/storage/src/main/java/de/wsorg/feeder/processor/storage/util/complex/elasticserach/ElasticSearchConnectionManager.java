package de.wsorg.feeder.processor.storage.util.complex.elasticserach;

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper.ImmutableSettingsWrapper;
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper.TransportClientWrapper;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ElasticSearchConnectionManager {
    @Inject
    private ImmutableSettingsWrapper immutableSettingsWrapper;
    @Inject
    private TransportClientWrapper transportClientWrapper;
    @Inject
    private StorageConfigProvider storageConfigProvider;

    private TransportClient transportClient;

    public Client getClient() {
        if (this.transportClient == null) {
            final String clusterName = storageConfigProvider.getElasticSearchConfig().getClusterName();
            final ImmutableSettings.Builder settingsBuilder = immutableSettingsWrapper.getSettingsBuilder();
            final Settings clientSettings = settingsBuilder.put("cluster.name", clusterName).build();

            this.transportClient = transportClientWrapper.getTransportClient(clientSettings);
            this.transportClient.addTransportAddresses(storageConfigProvider.getElasticSearchConfig().getElasticSearchHostsAddresses());
        }
        return  this.transportClient;
    }
}
