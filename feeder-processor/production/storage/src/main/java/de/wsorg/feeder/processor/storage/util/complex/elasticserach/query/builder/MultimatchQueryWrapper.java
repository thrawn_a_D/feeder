package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class MultimatchQueryWrapper {
    public QueryBuilder getMultiMatchQuery(final String searchString, final String ... columns) {
        return QueryBuilders.multiMatchQuery(searchString, columns);
    }
}
