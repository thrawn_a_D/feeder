package de.wsorg.feeder.processor.storage.util.complex.couchbase.wrapper;

import com.couchbase.client.CouchbaseClient;

import javax.inject.Named;
import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CouchbaseClientWrapper {
    public CouchbaseClient getCouchbaseClient(List<URI> baseList, String bucketName, String pwd) throws IOException {
        return new CouchbaseClient(baseList, bucketName, pwd);
    }
}
