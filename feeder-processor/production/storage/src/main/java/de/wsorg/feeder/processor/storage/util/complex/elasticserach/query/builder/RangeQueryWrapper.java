package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class RangeQueryWrapper {
    public RangeQueryBuilder getRangeQueryBuilder(final String columnName) {
        return QueryBuilders.rangeQuery(columnName);
    }

}
