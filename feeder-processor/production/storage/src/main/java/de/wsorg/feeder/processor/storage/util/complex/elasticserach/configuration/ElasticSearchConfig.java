package de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration;

import de.wsorg.feeder.processor.storage.util.complex.config.common.Host;
import lombok.Data;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class ElasticSearchConfig {
    public static final String ELASTIC_SEARCH_CLUSTER_NAME = "elasticSearch.clusterName";
    public static final String ELASTIC_SEARCH_INDEX_FEEDS = "elasticSearch.index.feeds";
    public static final String ELASTIC_SEARCH_INDEX_ASSOCIATION = "elasticSearch.index.association";
    public static final String ELASTIC_SEARCH_INDEX_LOAD_STATS = "elasticSearch.index.loadStats";
    public static final String ELASTIC_SEARCH_HOSTS = "elasticSearch.hosts";
    private String clusterName;
    private String feedsIndexName;
    private String feedsAssociationIndexName;
    private String feedsLoadStatisticsIndexName;
    private List<Host> elasticSearchHosts = new ArrayList<>();

    public ElasticSearchConfig() {
    }

    public ElasticSearchConfig(final Properties storageProperties) {
        clusterName = getPropertyValue(storageProperties, ELASTIC_SEARCH_CLUSTER_NAME);
        feedsIndexName = getPropertyValue(storageProperties, ELASTIC_SEARCH_INDEX_FEEDS);
        feedsAssociationIndexName = getPropertyValue(storageProperties, ELASTIC_SEARCH_INDEX_ASSOCIATION);
        feedsLoadStatisticsIndexName = getPropertyValue(storageProperties, ELASTIC_SEARCH_INDEX_LOAD_STATS);
        loadSearchHosts(storageProperties);
    }

    private void loadSearchHosts(final Properties storageProperties) {
        String searchHostsString = storageProperties.getProperty(ELASTIC_SEARCH_HOSTS);
        if (StringUtils.isNotBlank(searchHostsString)) {
            String[] hostsArray = searchHostsString.split(";");
            for (String host : hostsArray) {
                String[] hostPort = host.split(":");
                this.addElasticSearchHost(hostPort[0], hostPort[1]);
            }
        }
    }

    private String getPropertyValue(final Properties storageProperties, final String propertyName) {
        return storageProperties.getProperty(propertyName);
    }

    public void addElasticSearchHost(final String host, final String port) {
        Host elasticSearchHost = new Host();
        elasticSearchHost.setHost(host);
        elasticSearchHost.setPort(port);
        elasticSearchHosts.add(elasticSearchHost);
    }

    public InetSocketTransportAddress[] getElasticSearchHostsAddresses() {
        InetSocketTransportAddress[] hosts = new InetSocketTransportAddress[elasticSearchHosts.size()];

        int counter=0;
        for (Host elasticSearchHost : elasticSearchHosts) {
            final int port = Integer.parseInt(elasticSearchHost.getPort());
            final String host = elasticSearchHost.getHost();
            hosts[counter] = new InetSocketTransportAddress(host, port);
            counter++;
        }

        return hosts;
    }
}
