package de.wsorg.feeder.processor.storage.user.filebased;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;
import de.wsorg.feeder.processor.storage.user.UserDataAccess;
import de.wsorg.feeder.processor.storage.util.TimestampProvider;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserFileBasedStorage implements UserDataAccess {
    private static final String STORAGE_FOLDER_NAME = "users" + File.separator;

    @Inject
    private FileBasedPersistence<FeederUseCaseUser> fileBasedPersistence;
    private TimestampProvider timeStampProvider = new TimestampProvider();

    @Override
    public FeederUseCaseUser save(final FeederUseCaseUser feederUseCaseUser) {

        if(isUserNew(feederUseCaseUser))
            setNewUserId(feederUseCaseUser);

        fileBasedPersistence.save(STORAGE_FOLDER_NAME, feederUseCaseUser.getUserId(), feederUseCaseUser);

        return feederUseCaseUser;
    }

    @Override
    public FeederUseCaseUser findByNickName(final String nickName) {
        FeederUseCaseUser useCaseUserWithNickName = null;

        List<FeederUseCaseUser> existingUseCaseUsers = fileBasedPersistence.getFolderContent(STORAGE_FOLDER_NAME);
        if(areThereLocalUsers(existingUseCaseUsers)) {
            useCaseUserWithNickName = lookForUserWithNickName(nickName, existingUseCaseUsers);
        }

        return useCaseUserWithNickName;
    }

    private FeederUseCaseUser lookForUserWithNickName(final String nickName, final List<FeederUseCaseUser> existingUseCaseUsers) {
        FeederUseCaseUser useCaseUserWithNickName = null;
        for (FeederUseCaseUser currentUseCaseUser : existingUseCaseUsers) {
            if(isTheUserWeLookFor(nickName, currentUseCaseUser)) {
                useCaseUserWithNickName = currentUseCaseUser;
                break;
            }
        }
        return useCaseUserWithNickName;
    }

    private boolean areThereLocalUsers(final List<FeederUseCaseUser> existingUseCaseUsers) {
        return existingUseCaseUsers != null;
    }

    private boolean isTheUserWeLookFor(final String nickName, final FeederUseCaseUser currentUseCaseUser) {
        return nickName.equals(currentUseCaseUser.getUserName());
    }

    private void setNewUserId(final FeederUseCaseUser feederUseCaseUser) {
        feederUseCaseUser.setUserId(timeStampProvider.getTimestampAsString());
    }

    private boolean isUserNew(final FeederUseCaseUser feederUseCaseUser) {
        return StringUtils.isBlank(feederUseCaseUser.getUserId());
    }
}
