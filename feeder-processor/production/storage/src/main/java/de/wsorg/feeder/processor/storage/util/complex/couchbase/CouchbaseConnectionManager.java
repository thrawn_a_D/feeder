package de.wsorg.feeder.processor.storage.util.complex.couchbase;

import com.couchbase.client.CouchbaseClient;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig;
import de.wsorg.feeder.processor.storage.util.complex.couchbase.wrapper.CouchbaseClientWrapper;
import org.springframework.context.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CouchbaseConnectionManager {
    @Inject
    private ApplicationContext applicationContext;
    @Inject
    private CouchbaseClientWrapper couchbaseClientWrapper;

    private CouchbaseConfig couchbaseConfig;

    private Map<String, CouchbaseClient> bucketClientMap = new HashMap<>();

    public CouchbaseClient getConnection(final String bucketName) {
        CouchbaseClient client = bucketClientMap.get(bucketName);
        if(client == null) {
            try {
                client = couchbaseClientWrapper.getCouchbaseClient(getCouchbaseConfig().getCouchbaseHostUris(),
                                                                            bucketName,
                                                                            getCouchbaseConfig().getCouchbasePassword());
                bucketClientMap.put(bucketName, client);
            } catch (IOException e) {
                throw new RuntimeException("Error while connecting to couchbase.");
            }
        }

        return client;
    }

    public void disconnect(final String bucketName) {
        final CouchbaseClient couchbaseClient = bucketClientMap.get(bucketName);
        if (couchbaseClient != null) {
            couchbaseClient.shutdown(3, TimeUnit.SECONDS);
        }
    }


    private CouchbaseConfig getCouchbaseConfig() {
        if(couchbaseConfig == null) {
            couchbaseConfig = applicationContext.getBean(CouchbaseConfig.class);

            if(couchbaseConfig == null)
                throw new IllegalStateException("Please provide a couchbase configueartion before connecting to database");
        }

        return couchbaseConfig;
    }
}
