package de.wsorg.feeder.processor.storage.util;

import java.util.Calendar;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class TimestampProvider {
    public String getTimestampAsString(){
        return String.valueOf(Calendar.getInstance().getTimeInMillis());
    }
}
