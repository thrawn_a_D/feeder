package de.wsorg.feeder.processor.storage.feed.filebased;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic;
import de.wsorg.feeder.processor.storage.feed.FeedLoadStatisticsDataAccess;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedLoadStatisticsFileBasedAccess implements FeedLoadStatisticsDataAccess {

    private static final String STORAGE_FOLDER_FOR_LOAD_STATISTICS = "feedLoadStatistics" + File.separator;
    @Inject
    private FileBasedPersistence<FeedLoadStatistic> fileBasedPersistence;

    @Override
    public FeedLoadStatistic getFeedLoadStatistics(final String feedUrl) {
        return fileBasedPersistence.getModelByFilePath(STORAGE_FOLDER_FOR_LOAD_STATISTICS, feedUrl);
    }

    @Override
    public void updateFeedLoadStatistics(final FeedLoadStatistic feedLoadStatistics) {
        storeLoadStatistic(feedLoadStatistics);
    }

    @Override
    public void addNewFeedLoadStatistic(final String feedUrl) {
        final FeedLoadStatistic newFeedLoadStatistic = new FeedLoadStatistic();
        newFeedLoadStatistic.setFeedUrl(feedUrl);
        storeLoadStatistic(newFeedLoadStatistic);
    }

    @Override
    public List<FeedLoadStatistic> findLoadStatisticsOlderThen(final Date feedLoadedOlderThen) {
        List<FeedLoadStatistic> result = new ArrayList<>();
        List<FeedLoadStatistic> allLoadStatisticsInStorage = fileBasedPersistence.getFolderContent(STORAGE_FOLDER_FOR_LOAD_STATISTICS);

        for (FeedLoadStatistic feedLoadStatistics : allLoadStatisticsInStorage) {

            Date lastLoadedDate = getLastLoadedDate(feedLoadStatistics);

            if(lastLoadedDate.before(feedLoadedOlderThen)) {
                result.add(feedLoadStatistics);
            }
        }

        return result;
    }

    /**
     * This is pure madness but it seems that the date is not serialized/deserialized properly
     */
    private Date getLastLoadedDate(final FeedLoadStatistic feedLoadStatistics) {
        SimpleDateFormat formater = new SimpleDateFormat();
        String parsedDateToString = formater.format(feedLoadStatistics.getLastLoaded());
        Date lastLoadedDate = feedLoadStatistics.getLastLoaded();
        try {
            lastLoadedDate = formater.parse(parsedDateToString);
        } catch (ParseException ignored) {}
        return lastLoadedDate;
    }

    @Override
    public void removeFeedLoadStatistic(final String feedUrl) {
        fileBasedPersistence.remove(STORAGE_FOLDER_FOR_LOAD_STATISTICS, feedUrl);
    }

    private void storeLoadStatistic(final FeedLoadStatistic feedLoadStatistics) {
        fileBasedPersistence.save(STORAGE_FOLDER_FOR_LOAD_STATISTICS, feedLoadStatistics.getFeedUrl(), feedLoadStatistics);
    }
}
