package de.wsorg.feeder.processor.storage.user;

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface UserDataAccess {
    FeederUseCaseUser save(final FeederUseCaseUser feederUseCaseUser);
    FeederUseCaseUser findByNickName(final String nickName);
}
