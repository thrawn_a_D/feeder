package de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration;

import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import org.apache.commons.lang.StringUtils;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class CouchbaseConfigVerifier implements ConfigVerifier {
    @Override
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties) {
        ConfigVerificationResult result = new ConfigVerificationResult(configurationFilePath);

        final String bucketNameForFeedsProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_BUCKET_NAME_FEEDS);
        final String bucketNameForAssociationsProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_BUCKET_NAME_ASSOCIATIONS);
        final String bucketNameForUserProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_BUCKET_NAME_USER);
        final String bucketNameForLoadStatsProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_BUCKET_NAME_LOAD_STATISTICS);
        final String passwordProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_BUCKET_PASSWORD);
        final String couchbaseHostsProperty = configProperties.getProperty(CouchbaseConfig.COUCHBASE_HOSTS);

        verifyProperty(result, CouchbaseConfig.COUCHBASE_BUCKET_NAME_FEEDS, bucketNameForFeedsProperty);
        verifyProperty(result, CouchbaseConfig.COUCHBASE_BUCKET_NAME_ASSOCIATIONS, bucketNameForAssociationsProperty);
        verifyProperty(result, CouchbaseConfig.COUCHBASE_BUCKET_NAME_USER, bucketNameForUserProperty);
        verifyProperty(result, CouchbaseConfig.COUCHBASE_BUCKET_NAME_LOAD_STATISTICS, bucketNameForLoadStatsProperty);
        verifyProperty(result, CouchbaseConfig.COUCHBASE_BUCKET_PASSWORD, passwordProperty);
        verifyProperty(result, CouchbaseConfig.COUCHBASE_HOSTS, couchbaseHostsProperty);

        validateHostsProperty(result, couchbaseHostsProperty);

        return result;
    }

    private void validateHostsProperty(final ConfigVerificationResult result, final String couchbaseHostsProperty) {
        if(!result.getPropertyErrors().containsKey(CouchbaseConfig.COUCHBASE_HOSTS) &&
          (!couchbaseHostsProperty.contains(";") || !couchbaseHostsProperty.contains(":"))) {
            result.addVerificationError(CouchbaseConfig.COUCHBASE_HOSTS, "Hosts must be separated by a ; and port should be separated by : (e.g. host:port;)");
        }
    }

    private void verifyProperty(final ConfigVerificationResult result, final String validatingPropertyName, final String validatingPropertyValue) {
        if(StringUtils.isBlank(validatingPropertyValue)) {
            result.addVerificationError(validatingPropertyName, "Property does not contain a valid value.");
        }
    }


}
