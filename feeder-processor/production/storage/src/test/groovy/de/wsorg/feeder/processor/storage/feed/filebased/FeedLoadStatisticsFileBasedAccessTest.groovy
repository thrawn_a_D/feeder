package de.wsorg.feeder.processor.storage.feed.filebased

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.03.12
 */
class FeedLoadStatisticsFileBasedAccessTest extends Specification {
    private static final String STORAGE_FOLDER_NAME = 'feedLoadStatistics' + File.separator

    FeedLoadStatisticsFileBasedAccess feedLoadStatisticsFileBasedAccess
    FileBasedPersistence<FeedLoadStatistic> fileBasedPersistence

    def setup(){
        feedLoadStatisticsFileBasedAccess = new FeedLoadStatisticsFileBasedAccess()
        fileBasedPersistence = Mock(FileBasedPersistence)
        feedLoadStatisticsFileBasedAccess.fileBasedPersistence = fileBasedPersistence
    }

    def "Store feed load statistics to file"() {
        given:
        def feedLoadStats = new FeedLoadStatistic(feedUrl: 'bla')

        when:
        feedLoadStatisticsFileBasedAccess.updateFeedLoadStatistics(feedLoadStats)

        then:
        1 * fileBasedPersistence.save(STORAGE_FOLDER_NAME, feedLoadStats.feedUrl, feedLoadStats)
    }

    def "Add a new Load statistic"() {
        given:
        def feedUrl = 'feedUrl'

        when:
        feedLoadStatisticsFileBasedAccess.addNewFeedLoadStatistic(feedUrl)

        then:
        1 * fileBasedPersistence.save(STORAGE_FOLDER_NAME, feedUrl, {it.feedUrl == feedUrl && it.lastLoaded.date == new Date().date})
    }

    def "Retrieve feed load statistics from file"() {
        given:
        def feedUrl = 'feed'

        and:
        def expectedStatistics = new FeedLoadStatistic()

        when:
        def result = feedLoadStatisticsFileBasedAccess.getFeedLoadStatistics(feedUrl)

        then:
        result == expectedStatistics
        1 * fileBasedPersistence.getModelByFilePath(STORAGE_FOLDER_NAME, feedUrl) >> expectedStatistics
    }

    def "Remove a feed statistic"() {
        given:
        def feedUrl = 'feedUrl'

        when:
        feedLoadStatisticsFileBasedAccess.removeFeedLoadStatistic(feedUrl)

        then:
        1 * fileBasedPersistence.remove(STORAGE_FOLDER_NAME, feedUrl)
    }

    def "Find all statistics older then"() {
        given:
        def dateToLookFor = new Date(2013, 01, 10)

        and:
        def feedLoadStat1 = new FeedLoadStatistic(lastLoaded: new Date(2012, 1, 2))
        def feedLoadStat2 = new FeedLoadStatistic(lastLoaded: new Date(2014, 11, 2))
        def feedLoadStat3 = new FeedLoadStatistic(lastLoaded: new Date(2012, 3, 2))

        when:
        def result = feedLoadStatisticsFileBasedAccess.findLoadStatisticsOlderThen(dateToLookFor)

        then:
        result.contains(feedLoadStat1)
        result.contains(feedLoadStat3)
        1 * fileBasedPersistence.getFolderContent(STORAGE_FOLDER_NAME) >> [feedLoadStat1, feedLoadStat2, feedLoadStat3]
    }
}
