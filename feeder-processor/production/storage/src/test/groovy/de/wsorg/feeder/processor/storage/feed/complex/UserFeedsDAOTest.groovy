package de.wsorg.feeder.processor.storage.feed.complex

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.BoolQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.StringQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper
import org.elasticsearch.index.query.BoolQueryBuilder
import org.elasticsearch.index.query.QueryStringQueryBuilder
import org.elasticsearch.index.query.TermQueryBuilder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 14.02.13
 */
class UserFeedsDAOTest extends Specification {
    private static final String indexName = 'feeder-feed'
    UserFeedsDAO userFeedsDAO
    FeedToUserAssociationStorage userAssociationDAO
    BoolQueryWrapper boolQueryWrapper
    TermQueryWrapper termQueryWrapper
    StorageConfigProvider storageConfigProvider
    ElasticSearchQueryHelper elasticSearchQueryHelper
    StringQueryWrapper stringQueryWrapper

    ElasticSearchConfig elasticSearchConfig

    def setup(){
        userFeedsDAO = new UserFeedsDAO()

        userAssociationDAO = Mock(FeedToUserAssociationStorage)
        boolQueryWrapper = Mock(BoolQueryWrapper)
        termQueryWrapper = Mock(TermQueryWrapper)
        storageConfigProvider = Mock(StorageConfigProvider)
        elasticSearchQueryHelper = Mock(ElasticSearchQueryHelper)
        stringQueryWrapper = Mock(StringQueryWrapper)

        userFeedsDAO.userAssociationDAO = userAssociationDAO
        userFeedsDAO.boolQueryWrapper = boolQueryWrapper
        userFeedsDAO.termQueryWrapper = termQueryWrapper
        userFeedsDAO.storageConfigProvider = storageConfigProvider
        userFeedsDAO.elasticSearchQueryHelper = elasticSearchQueryHelper
        userFeedsDAO.stringQueryWrapper = stringQueryWrapper

        elasticSearchConfig = Mock(ElasticSearchConfig)

        storageConfigProvider.getElasticSearchConfig() >> elasticSearchConfig
        elasticSearchConfig.getFeedsIndexName() >> indexName
    }



    def "Get user feeds"() {
        given:
        def userId = 'kugjf'

        and:
        def feedUrl1 = 'http://asddas'
        def feedUrl2 = 'http://vd343ff'
        def association1 = new FeedToUserAssociation(feedUrl: feedUrl1, associationType: AssociationType.OWNER)
        def association2 = new FeedToUserAssociation(feedUrl: feedUrl2, associationType: AssociationType.SUBSCRIBER)

        and:
        def expectedUserAssociation = [association1, association2]

        and:
        def boolQueryBuilder = Mock(BoolQueryBuilder)
        def termQueryBuilder1 = Mock(TermQueryBuilder)
        def termQueryBuilder2 = Mock(TermQueryBuilder)

        and:
        def expectedFeedResult = [new FeedModel(feedUrl: feedUrl1), new FeedModel(feedUrl: feedUrl2)]

        when:
        def result = userFeedsDAO.getUserFeeds(userId)

        then:
        result
        result[0].feedUrl == feedUrl1
        result[0].feedToUserAssociation.associationType == AssociationType.OWNER
        1 * userAssociationDAO.getUserFeeds(userId) >> expectedUserAssociation
        1 * boolQueryWrapper.getBoolQueryBuilder() >> boolQueryBuilder
        1 * termQueryWrapper.getTermQueryBuilder('_id', feedUrl1) >> termQueryBuilder1
        1 * termQueryWrapper.getTermQueryBuilder('_id', feedUrl2) >> termQueryBuilder2
        1 * boolQueryBuilder.should(termQueryBuilder1) >> boolQueryBuilder
        1 * boolQueryBuilder.should(termQueryBuilder2) >> boolQueryBuilder
        1 * elasticSearchQueryHelper.processQuery(boolQueryBuilder, indexName, FeedModel) >> expectedFeedResult
    }

    def "No associations found for user feeds"() {
        given:
        def userId = 'kugjf'

        and:
        def expectedUserAssociation = []

        when:
        def result = userFeedsDAO.getUserFeeds(userId)

        then:
        result.empty
        1 * userAssociationDAO.getUserFeeds(userId) >> expectedUserAssociation
    }

    def "Get user feeds contianing title"() {
        given:

        def userId = 'kugjf'
        def query = 'kubj'

        and:
        def feedUrl1 = 'http://asddas'
        def feedUrl2 = 'http://vd343ff'
        def association1 = new FeedToUserAssociation(feedUrl: feedUrl1, associationType: AssociationType.OWNER)
        def association2 = new FeedToUserAssociation(feedUrl: feedUrl2, associationType: AssociationType.SUBSCRIBER)

        and:
        def expectedUserAssociation = [association1, association2]

        and:
        def boolQueryBuilder = Mock(BoolQueryBuilder)
        def termQueryBuilder1 = Mock(TermQueryBuilder)
        def termQueryBuilder2 = Mock(TermQueryBuilder)
        def stringQueryBuilder = Mock(QueryStringQueryBuilder)

        and:
        def expectedFeedResult = [new FeedModel(feedUrl: feedUrl1), new FeedModel(feedUrl: feedUrl2)]

        when:
        def result = userFeedsDAO.getUserFeeds(userId, query)

        then:
        result
        result[0].feedUrl == feedUrl1
        result[0].feedToUserAssociation.associationType == AssociationType.OWNER
        1 * userAssociationDAO.getUserFeeds(userId) >> expectedUserAssociation
        1 * boolQueryWrapper.getBoolQueryBuilder() >> boolQueryBuilder
        1 * termQueryWrapper.getTermQueryBuilder('_id', feedUrl1) >> termQueryBuilder1
        1 * termQueryWrapper.getTermQueryBuilder('_id', feedUrl2) >> termQueryBuilder2
        1 * stringQueryWrapper.getQueryStringBuilder(query) >> stringQueryBuilder
        1 * stringQueryBuilder.field('doc.title')
        1 * boolQueryBuilder.should(termQueryBuilder1) >> boolQueryBuilder
        1 * boolQueryBuilder.should(termQueryBuilder2) >> boolQueryBuilder
        1 * boolQueryBuilder.must(stringQueryBuilder) >> boolQueryBuilder
        1 * elasticSearchQueryHelper.processQuery(boolQueryBuilder, indexName, FeedModel) >> expectedFeedResult
    }
}
