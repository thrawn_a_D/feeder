package de.wsorg.feeder.processor.storage.user.complex

import com.couchbase.client.CouchbaseClient
import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.storage.util.TimestampProvider
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 18.02.13
 */
class ComplexUserDAOTest extends Specification {
    ComplexUserDAO complexUserDAO
    FeederJsonTransformer<FeederUseCaseUser> jsonTransformer
    CouchbaseConnectionManager couchbaseConnectionManager
    StorageConfigProvider storageConfigProvider
    TimestampProvider timestampProvider;

    def DEFAULT_BUCKET_NAME = 'asdasd'

    def setup(){
        complexUserDAO = new ComplexUserDAO()

        storageConfigProvider = Mock(StorageConfigProvider)
        couchbaseConnectionManager = Mock(CouchbaseConnectionManager)
        jsonTransformer = Mock(FeederJsonTransformer)
        timestampProvider = Mock(TimestampProvider)

        complexUserDAO.storageConfigProvider = storageConfigProvider
        complexUserDAO.couchbaseConnectionManager = couchbaseConnectionManager
        complexUserDAO.jsonTransformer = jsonTransformer
        complexUserDAO.timeStampProvider = timestampProvider

        storageConfigProvider.getCouchbaseConfig() >> Mock(CouchbaseConfig)
        storageConfigProvider.couchbaseConfig.getFeederUserBucketName() >> DEFAULT_BUCKET_NAME
    }

    def "Store user"() {
        given:
        def userData = new FeederUseCaseUser()
        userData.userName = 'asdads'
        userData.EMail = 'asddasd@asd.com'

        and:
        def someTimestamptForNewUser = "123523423234"

        and:
        def couchbaseClient = Mock(CouchbaseClient)
        def objectAsJson = 'asdoiausdj09'

        when:
        complexUserDAO.save(userData)

        then:
        1 * timestampProvider.getTimestampAsString() >> someTimestamptForNewUser
        1 * jsonTransformer.getJsonForObject({it -> it.userId == someTimestamptForNewUser}, "feederUseCaseUser") >> objectAsJson
        1 * couchbaseConnectionManager.getConnection(DEFAULT_BUCKET_NAME) >> couchbaseClient
        1 * couchbaseClient.add(userData.getUserName(), 0, objectAsJson)
    }

    def "Get user from storage by nickName"() {
        given:
        def userData = new FeederUseCaseUser()
        userData.userName = 'asdads'
        userData.EMail = 'asddasd@asd.com'

        and:
        def couchbaseClient = Mock(CouchbaseClient)
        def userAsJson = 'lkjnkjn'

        when:
        def result = complexUserDAO.findByNickName(userData.getUserName())

        then:
        result == userData
        1 * couchbaseConnectionManager.getConnection(DEFAULT_BUCKET_NAME) >> couchbaseClient
        1 * couchbaseClient.get(userData.getUserName()) >> userAsJson
        1 * jsonTransformer.getObjectFromJson(userAsJson, FeederUseCaseUser) >> userData
    }

    def "No user found, return null"() {
        given:
        def nickName = 'asd'
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        def result = complexUserDAO.findByNickName(nickName)

        then:
        result == null
        1 * couchbaseConnectionManager.getConnection(DEFAULT_BUCKET_NAME) >> couchbaseClient
        1 * couchbaseClient.get(nickName) >> null
        0 * jsonTransformer.getObjectFromJson(_, _)
    }
}
