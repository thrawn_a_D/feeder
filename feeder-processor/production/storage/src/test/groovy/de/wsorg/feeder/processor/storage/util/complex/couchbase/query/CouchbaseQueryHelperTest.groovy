package de.wsorg.feeder.processor.storage.util.complex.couchbase.query

import com.couchbase.client.CouchbaseClient
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.View
import com.couchbase.client.protocol.views.ViewResponse
import com.couchbase.client.protocol.views.ViewRow
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.02.13
 */
class CouchbaseQueryHelperTest extends Specification {
    CouchbaseQueryHelper queryHelper
    FeederJsonTransformer<FeedModel> jsonTransformer

    def setup(){
        queryHelper = new CouchbaseQueryHelper()
        jsonTransformer = Mock(FeederJsonTransformer)
        queryHelper.jsonTransformer = jsonTransformer
    }

    def "Process query"() {
        given:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def view = Mock(View)
        def query = Mock(Query)
        def response = Mock(ViewResponse)
        def responseIterator = Mock(Iterator)
        def responseEntry = Mock(ViewRow)
        def objectAsJson = 'asdjnadskjn'

        and:
        def expectedModel = new FeedModel()

        when:
        def result = queryHelper.findDataInViewRelatedToQuery(couchbaseClient, view, query, FeedModel)

        then:
        result
        result[0] == expectedModel
        1 * couchbaseClient.query(view, query) >> response
        1 * response.iterator() >> responseIterator
        2 * responseIterator.hasNext() >>> [true, false]
        1 * responseIterator.next() >> responseEntry
        1 * responseEntry.getDocument() >> objectAsJson
        1 * jsonTransformer.getObjectFromJson(objectAsJson, FeedModel) >> expectedModel
    }
}
