package de.wsorg.feeder.processor.storage.feed.complex

import com.couchbase.client.CouchbaseClient
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedLoadStatistic
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.RangeQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import org.elasticsearch.index.query.RangeQueryBuilder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 08.03.12
 */
class ComplexFeedLoadStatisticsDAOTest extends Specification {
    ComplexFeedLoadStatisticsDAO complexFeedLoadStatisticsDAO
    FeederJsonTransformer feederJsonTransformer
    CouchbaseConnectionManager couchbaseConnectionManager
    StorageConfigProvider storageConfigProvider
    ElasticSearchQueryHelper elasticSearchQueryHelper
    RangeQueryWrapper rangeQueryWrapper

    CouchbaseConfig couchbaseConfig
    ElasticSearchConfig elasticSearchConfig

    def bucketName = 'feeder-load-statistics'
    def indexName = 'feeder-load-statistics'

    def setup(){
        complexFeedLoadStatisticsDAO = new ComplexFeedLoadStatisticsDAO()

        feederJsonTransformer = Mock(FeederJsonTransformer)
        couchbaseConnectionManager = Mock(CouchbaseConnectionManager)
        storageConfigProvider = Mock(StorageConfigProvider)
        elasticSearchQueryHelper = Mock(ElasticSearchQueryHelper)
        rangeQueryWrapper = Mock(RangeQueryWrapper)

        couchbaseConfig = Mock(CouchbaseConfig)
        elasticSearchConfig = Mock(ElasticSearchConfig)

        complexFeedLoadStatisticsDAO.jsonTransformer = feederJsonTransformer
        complexFeedLoadStatisticsDAO.couchbaseConnectionManager = couchbaseConnectionManager
        complexFeedLoadStatisticsDAO.storageConfigProvider = storageConfigProvider
        complexFeedLoadStatisticsDAO.elasticSearchQueryHelper = elasticSearchQueryHelper
        complexFeedLoadStatisticsDAO.rangeQueryWrapper = rangeQueryWrapper

        storageConfigProvider.getCouchbaseConfig() >> couchbaseConfig
        couchbaseConfig.getLoadStatisticsBucketName() >> bucketName

        elasticSearchConfig.getFeedsLoadStatisticsIndexName() >> indexName
        storageConfigProvider.getElasticSearchConfig() >> elasticSearchConfig
    }

    def "Update a load statistic in couchbase"() {
        given:
        def loadStatistic = new FeedLoadStatistic()

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        complexFeedLoadStatisticsDAO.updateFeedLoadStatistics(loadStatistic)

        then:
        1 * feederJsonTransformer.getJsonForObject(loadStatistic, 'feedLoadStatistic') >> modelAsJson
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.set(loadStatistic.feedUrl, 0, modelAsJson)
    }

    def "Load a feed statistic"() {
        given:
        def feedUrl = 'lkjbbiubkub'

        and:
        def loadStatistic = new FeedLoadStatistic()

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        def result = complexFeedLoadStatisticsDAO.getFeedLoadStatistics(feedUrl)

        then:
        result
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(feedUrl) >> modelAsJson
        1 * feederJsonTransformer.getObjectFromJson(modelAsJson, FeedLoadStatistic) >> loadStatistic
    }

    def "Add new feed load entry"() {
        given:
        def feedUrl = 'kjnknkjn'

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        complexFeedLoadStatisticsDAO.addNewFeedLoadStatistic(feedUrl)

        then:
        1 * feederJsonTransformer.getJsonForObject({it.feedUrl == feedUrl && it.lastLoaded.date == new Date().date}, 'feedLoadStatistic') >> modelAsJson
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.set(feedUrl, 0, modelAsJson)
    }

    def "Remove feed statistic"() {
        given:
        def feedUrl = 'kjnknkjn'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        complexFeedLoadStatisticsDAO.removeFeedLoadStatistic(feedUrl)

        then:
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.delete(feedUrl)
    }

    def "Find all feed stats which are older then"() {
        given:
        def dateToLookFor = new Date()

        and:
        def rangeQuery = Mock(RangeQueryBuilder)

        and:
        def expectedResult = [new FeedLoadStatistic()]

        when:
        def result = complexFeedLoadStatisticsDAO.findLoadStatisticsOlderThen(dateToLookFor)

        then:
        result == expectedResult
        1 * rangeQueryWrapper.getRangeQueryBuilder('lastLoadedAsLong') >> rangeQuery
        1 * rangeQuery.to(dateToLookFor.getTime()) >> rangeQuery
        1 * elasticSearchQueryHelper.processQuery(rangeQuery, indexName, FeedLoadStatistic) >> expectedResult
    }
}
