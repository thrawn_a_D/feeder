package de.wsorg.feeder.processor.storage.util.complex.couchbase

import com.couchbase.client.CouchbaseClient
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.wrapper.CouchbaseClientWrapper
import org.springframework.context.ApplicationContext
import spock.lang.Specification

import java.util.concurrent.TimeUnit

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.02.13
 */
class CouchbaseConnectionManagerTest extends Specification {
    CouchbaseConnectionManager connection
    CouchbaseClientWrapper couchbaseClientWrapper
    CouchbaseClient couchbaseClient
    ApplicationContext applicationContext

    def setup(){
        connection = new CouchbaseConnectionManager()
        couchbaseClientWrapper = Mock(CouchbaseClientWrapper)
        applicationContext = Mock(ApplicationContext)
        couchbaseClient = Mock(CouchbaseClient)
        connection.couchbaseClientWrapper = couchbaseClientWrapper
        connection.applicationContext = applicationContext
    }

    def "Open connection"() {
        given:
        def config = new CouchbaseConfig()
        config.addCouchbaseHost('123.123.123.213', '8081')
        config.feedsBucketName = 'feed'
        config.couchbasePassword = 'passWd'

        connection.couchbaseConfig = config

        when:
        def result = connection.getConnection(config.feedsBucketName)

        then:
        1 * couchbaseClientWrapper.getCouchbaseClient(config.getCouchbaseHostUris(),
                                                         config.getFeedsBucketName(),
                                                         config.getCouchbasePassword()) >> couchbaseClient
        result != null;
    }

    def "Store connection depending on bucket name"() {
        given:
        def bucketName1 = 'feed1'
        def bucketName2 = 'feed2'

        and:
        def config = new CouchbaseConfig()
        config.feedsBucketName = bucketName1

        connection.couchbaseConfig = config

        and:
        def couchbaseClient1 = Mock(CouchbaseClient)
        def couchbaseClient2 = Mock(CouchbaseClient)

        and:
        couchbaseClientWrapper.getCouchbaseClient(_,bucketName1,_) >> couchbaseClient1
        couchbaseClientWrapper.getCouchbaseClient(_,bucketName2,_) >> couchbaseClient2

        when:
        def result1 = connection.getConnection(bucketName1)
        def result2 = connection.getConnection(bucketName2)

        then:
        result1 == couchbaseClient1;
        result2 == couchbaseClient2;

    }

    def "Check that configuration is provided before initializing connection"() {
        when:
        connection.getConnection('')

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a couchbase configueartion before connecting to database'
        1 * applicationContext.getBean(CouchbaseConfig) >> null
    }

    def "Disconnect from database"() {
        given:
        def bucketName = 'buck'
        connection.bucketClientMap = [buck: couchbaseClient]

        when:
        connection.disconnect(bucketName)

        then:
        1 * couchbaseClient.shutdown(3, TimeUnit.SECONDS)
    }

    def "Connection is null and disconnect"() {
        when:
        connection.disconnect('buck')

        then:
        true
    }
}
