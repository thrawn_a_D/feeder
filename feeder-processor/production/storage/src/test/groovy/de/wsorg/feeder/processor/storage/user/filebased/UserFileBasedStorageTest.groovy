package de.wsorg.feeder.processor.storage.user.filebased

import de.wsorg.feeder.processor.production.domain.user.FeederUseCaseUser
import de.wsorg.feeder.processor.storage.util.TimestampProvider
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 21.09.12
 */
class UserFileBasedStorageTest extends Specification {
    private static final String STORAGE_FOLDER_NAME = "users" + File.separator

    UserFileBasedStorage userFileBasedStorage
    TimestampProvider timestampProvider;
    FileBasedPersistence<FeederUseCaseUser> fileBasedPersistence

    def setup(){
        userFileBasedStorage = new UserFileBasedStorage()
        fileBasedPersistence = Mock(FileBasedPersistence)
        timestampProvider = Mock(TimestampProvider)
        userFileBasedStorage.fileBasedPersistence = fileBasedPersistence
        userFileBasedStorage.timeStampProvider = timestampProvider
    }

    def "Persist a given user without an existing id"() {
        given:
        def userToPersist = new FeederUseCaseUser()
        def someTimestamptForNewUser = "123523423234"

        when:
        def persistedUser = userFileBasedStorage.save(userToPersist)

        then:
        persistedUser.userId == someTimestamptForNewUser
        1 * timestampProvider.getTimestampAsString() >> someTimestamptForNewUser
        1 * fileBasedPersistence.save(STORAGE_FOLDER_NAME, {it == someTimestamptForNewUser}, userToPersist)
    }

    def "Persist a user with a given user id"() {
        def userToPersist = new FeederUseCaseUser(userId: "123523423234")

        when:
        def persistedUser = userFileBasedStorage.save(userToPersist)

        then:
        persistedUser.userId != null
        1 * fileBasedPersistence.save(STORAGE_FOLDER_NAME, {it == userToPersist.userId}, userToPersist)
    }

    def "Find a user by a nickName"() {
        given:
        def nickName = 'nick'
        def userToFind = new FeederUseCaseUser(userName: nickName)
        def userWithADifferentNickName = new FeederUseCaseUser(userName: 'asd')

        when:
        def foundUser = userFileBasedStorage.findByNickName(nickName)

        then:
        foundUser == userToFind
        1 * fileBasedPersistence.getFolderContent(STORAGE_FOLDER_NAME) >> [userToFind, userWithADifferentNickName]
    }

    def "Look for user with nickname but no users exist in local storage"() {
        given:
        def nickName = 'nick'

        when:
        def result = userFileBasedStorage.findByNickName(nickName)

        then:
        result == null
    }
}
