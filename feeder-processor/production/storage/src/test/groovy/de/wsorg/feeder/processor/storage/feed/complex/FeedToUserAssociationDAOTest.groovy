package de.wsorg.feeder.processor.storage.feed.complex

import com.couchbase.client.CouchbaseClient
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import org.elasticsearch.index.query.TermQueryBuilder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 13.02.13
 */
class FeedToUserAssociationDAOTest extends Specification {

    def bucketName = 'feeder-feeds'

    FeedToUserAssociationDAO feedToUserAssociationDAO
    FeederJsonTransformer<FeedToUserAssociation> jsonTransformer
    CouchbaseConnectionManager couchbaseConnectionManager
    ElasticSearchQueryHelper elasticSearchQueryHelper
    StorageConfigProvider storageConfigProvider

    TermQueryWrapper termQueryWrapper

    ElasticSearchConfig elasticSearchConfig
    CouchbaseConfig couchbaseConfig

    def setup(){
        feedToUserAssociationDAO = new FeedToUserAssociationDAO()

        jsonTransformer = Mock(FeederJsonTransformer)
        couchbaseConnectionManager = Mock(CouchbaseConnectionManager)
        elasticSearchQueryHelper = Mock(ElasticSearchQueryHelper)
        storageConfigProvider = Mock(StorageConfigProvider)

        couchbaseConfig = Mock(CouchbaseConfig)
        elasticSearchConfig = Mock(ElasticSearchConfig)
        termQueryWrapper = Mock(TermQueryWrapper)

        feedToUserAssociationDAO.jsonTransformer = jsonTransformer
        feedToUserAssociationDAO.couchbaseConnectionManager = couchbaseConnectionManager
        feedToUserAssociationDAO.elasticSearchQueryHelper = elasticSearchQueryHelper
        feedToUserAssociationDAO.storageConfigProvider = storageConfigProvider

        storageConfigProvider.getElasticSearchConfig() >> elasticSearchConfig
        storageConfigProvider.getCouchbaseConfig() >> couchbaseConfig

        feedToUserAssociationDAO.termQueryWrapper = termQueryWrapper


        couchbaseConfig.getFeedAssociationBucketName() >> bucketName
    }


    def "Store a feed model in the database"() {
        given:
        def model = new FeedToUserAssociation()
        model.feedUrl = 'http:/feed'
        model.userId = '213e2'

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)
        def expectedStoringKey = model.userId + '_' + model.feedUrl

        when:
        feedToUserAssociationDAO.save(model)

        then:
        1 * jsonTransformer.getJsonForObject(model, 'feedAssociationModel') >> modelAsJson
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.set(expectedStoringKey, 0, modelAsJson)
    }

    def "Feed url and userId needs to be set before storing an object"() {
        given:
        def model = new FeedToUserAssociation()

        when:
        feedToUserAssociationDAO.save(model)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided feed model to be stored does not have a feed url or/and userId set.'
    }

    def "Find all entries of a user by his userId"() {
        given:
        def indexName = 'feeder-feed-assoziation'
        elasticSearchConfig.getFeedsAssociationIndexName() >> indexName
        def userId = 'sdw3d32'

        and:
        def termQueryBuilder = Mock(TermQueryBuilder)

        when:
        feedToUserAssociationDAO.getUserFeeds(userId)

        then:
        1 * termQueryWrapper.getTermQueryBuilder('userId', userId) >> termQueryBuilder
        1 * elasticSearchQueryHelper.processQuery(termQueryBuilder, indexName, FeedToUserAssociation)
    }

    def "Find all associations related to a feed url"() {
        given:
        def feedUrl = 'khlkjhlkh'

        and:
        def termQueryBuilder = Mock(TermQueryBuilder)

        and:
        def indexName = 'feeder-feed-assoziation'
        elasticSearchConfig.getFeedsAssociationIndexName() >> indexName

        when:
        feedToUserAssociationDAO.getAssociationsOfFeed(feedUrl)

        then:
        1 * termQueryWrapper.getTermQueryBuilder('feedUrl', feedUrl) >> termQueryBuilder
        1 * elasticSearchQueryHelper.processQuery(termQueryBuilder, indexName, FeedToUserAssociation)
    }

    def "Get feed to user association by the key (userId, and feedUrl)"() {
        given:
        def feedUrl = 'http://lkin'
        def userId = 'ioj087uzv67'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def expectedStoringKey = userId + '_' + feedUrl

        and:
        def modelAsJSonString = 'kljniubnkbasd'
        def expectedFeedModel = new FeedToUserAssociation()


        when:
        def result = feedToUserAssociationDAO.getUserFeedAssociation(feedUrl, userId)

        then:
        result == expectedFeedModel
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(expectedStoringKey) >> modelAsJSonString
        1 * jsonTransformer.getObjectFromJson(modelAsJSonString, FeedToUserAssociation) >> expectedFeedModel
    }

    def "feed to user association by the key not found"() {
        given:
        def feedUrl = 'http://lkin'
        def userId = 'ioj087uzv67'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def expectedStoringKey = userId + '_' + feedUrl

        when:
        def result = feedToUserAssociationDAO.getUserFeedAssociation(feedUrl, userId)

        then:
        result == null
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(expectedStoringKey) >> null
        0 * jsonTransformer.getObjectFromJson(_, _)
    }

    def "Get association by feedUrl, userId and association type"() {
        given:
        def feedUrl = 'http://lkin'
        def userId = 'ioj087uzv67'
        def associationType = AssociationType.OWNER

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def expectedStoringKey = userId + '_' + feedUrl

        and:
        def modelAsJSonString = 'kljniubnkbasd'
        def expectedFeedModel = new FeedToUserAssociation()
        expectedFeedModel.associationType = associationType

        when:
        def result = feedToUserAssociationDAO.isAssociationGiven(feedUrl, userId, associationType)

        then:
        result == true
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(expectedStoringKey) >> modelAsJSonString
        1 * jsonTransformer.getObjectFromJson(modelAsJSonString, FeedToUserAssociation) >> expectedFeedModel
    }

    def "No feed assiciation found, make association type return false"() {
        given:
        def feedUrl = 'http://lkin'
        def userId = 'ioj087uzv67'
        def associationType = AssociationType.OWNER

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def expectedStoringKey = userId + '_' + feedUrl

        when:
        def result = feedToUserAssociationDAO.isAssociationGiven(feedUrl, userId, associationType)

        then:
        result == false
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(expectedStoringKey) >> null
    }

    def "Remove association from db"() {
        given:
        def feedUrl = 'http://lkin'
        def userId = 'ioj087uzv67'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def expectedStoringKey = userId + '_' + feedUrl

        when:
        feedToUserAssociationDAO.removeAssociation(feedUrl, userId)

        then:
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.delete(expectedStoringKey)
    }
}
