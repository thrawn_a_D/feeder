package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.ElasticSearchConnectionManager
import org.elasticsearch.action.ListenableActionFuture
import org.elasticsearch.action.search.SearchRequestBuilder
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.Client
import org.elasticsearch.index.query.QueryBuilder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.02.13
 */
class ElasticSearchQueryHelperTest extends Specification {
    ElasticSearchQueryHelper elasticSearchQueryHelper
    ElasticSearchConnectionManager elasticSearchConnectionManager
    QueryResultTransformer queryResultTransformer

    def setup(){
        elasticSearchQueryHelper = new ElasticSearchQueryHelper()
        elasticSearchConnectionManager = Mock(ElasticSearchConnectionManager)
        queryResultTransformer = Mock(QueryResultTransformer)
        elasticSearchQueryHelper.elasticSearchConnectionManager = elasticSearchConnectionManager
        elasticSearchQueryHelper.queryResultTransformer = queryResultTransformer
    }



    def "Process query"() {
        given:
        def client = Mock(Client)
        def indexName = 'feeder-feed'

        elasticSearchConnectionManager.getClient() >> client

        and:
        def searchRequestBuilder = Mock(SearchRequestBuilder)
        def listenableActionFuture = Mock(ListenableActionFuture)
        def searchResponse = Mock(SearchResponse)

        and:
        def queryToProcess = Mock(QueryBuilder)

        and:
        def expectedResult = [Mock(FeedModel)]

        when:
        elasticSearchQueryHelper.processQuery(queryToProcess, indexName, FeedModel)

        then:
        1 * client.prepareSearch(indexName) >> searchRequestBuilder
        1 * searchRequestBuilder.setQuery(_) >> searchRequestBuilder
        1 * searchRequestBuilder.execute() >> listenableActionFuture
        1 * listenableActionFuture.actionGet() >> searchResponse
        1 * queryResultTransformer.transformResultToModel(searchResponse, FeedModel) >> expectedResult
    }
}
