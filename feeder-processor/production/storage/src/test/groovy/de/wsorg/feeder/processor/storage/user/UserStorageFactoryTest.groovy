package de.wsorg.feeder.processor.storage.user

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.02.13
 */
class UserStorageFactoryTest extends Specification {
    @Shared
    StorageConfig config

    def setup(){
        config = new StorageConfig()
        config.couchbaseConfig = new CouchbaseConfig()
        config.elasticSearchConfig = new ElasticSearchConfig()
        UserStorageFactory.configProvided = false
    }

    def "Get user dao"() {
        given:
        UserStorageFactory.setClientConfig(config)

        when:
        def result = UserStorageFactory.getUserDAO()

        then:
        result != null
    }

    def "Check if configuration was set properly when getting a feed dao"() {

        when:
        UserStorageFactory.getUserDAO()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Check that config was inserted into app context"() {
        when:
        UserStorageFactory.setClientConfig(config)

        then:
        def usedCouchbaseConfigConfig = UserStorageFactory.applicationContext.getBean(CouchbaseConfig)
        def usedElasticSearchConfigConfig = UserStorageFactory.applicationContext.getBean(ElasticSearchConfig)
        usedCouchbaseConfigConfig == config.couchbaseConfig
        usedElasticSearchConfigConfig == config.elasticSearchConfig
        UserStorageFactory.configProvided
    }
}
