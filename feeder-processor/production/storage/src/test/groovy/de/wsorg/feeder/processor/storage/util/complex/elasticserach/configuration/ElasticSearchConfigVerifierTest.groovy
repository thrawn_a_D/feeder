package de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.03.13
 */
class ElasticSearchConfigVerifierTest extends Specification {
    ElasticSearchConfigVerifier elasticSearchConfigVerifier
    def configLocation = ['asd', 'bla'] as String[]

    def setup(){
        elasticSearchConfigVerifier = new ElasticSearchConfigVerifier()
    }

    def "Validate configuration when providing properties"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'bla'
        properties.getProperty("elasticSearch.index.feeds") >> 'bla'
        properties.getProperty("elasticSearch.index.association") >> 'bla'
        properties.getProperty("elasticSearch.index.loadStats") >> 'bla'
        properties.getProperty("elasticSearch.hosts") >> 'bla:2020;'

        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Do not provide clusterName"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> ''
        properties.getProperty("elasticSearch.index.feeds") >> 'bla'
        properties.getProperty("elasticSearch.index.association") >> 'bla'
        properties.getProperty("elasticSearch.index.loadStats") >> 'bla'
        properties.getProperty("elasticSearch.hosts") >> 'bla:2020;'

        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_CLUSTER_NAME] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide index name for feeds"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'asd'
        properties.getProperty("elasticSearch.index.feeds") >> ''
        properties.getProperty("elasticSearch.index.association") >> 'bla'
        properties.getProperty("elasticSearch.index.loadStats") >> 'bla'
        properties.getProperty("elasticSearch.hosts") >> 'bla:2020;'

        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_INDEX_FEEDS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide index name for association"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'asd'
        properties.getProperty("elasticSearch.index.feeds") >> 'asd'
        properties.getProperty("elasticSearch.index.association") >> ''
        properties.getProperty("elasticSearch.index.loadStats") >> 'bla'
        properties.getProperty("elasticSearch.hosts") >> 'bla:2020;'

        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_INDEX_ASSOCIATION] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide index name for loadStats"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'asd'
        properties.getProperty("elasticSearch.index.feeds") >> 'asd'
        properties.getProperty("elasticSearch.index.association") >> 'asd'
        properties.getProperty("elasticSearch.index.loadStats") >> ''
        properties.getProperty("elasticSearch.hosts") >> 'bla:2020;'

        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_INDEX_LOAD_STATS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide couchbase hosts"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'asd'
        properties.getProperty("elasticSearch.index.feeds") >> 'asd'
        properties.getProperty("elasticSearch.index.association") >> 'asd'
        properties.getProperty("elasticSearch.index.loadStats") >> 'sad'
        properties.getProperty("elasticSearch.hosts") >> ''


        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_HOSTS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Verify host is properly set"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("elasticSearch.clusterName") >> 'asd'
        properties.getProperty("elasticSearch.index.feeds") >> 'asd'
        properties.getProperty("elasticSearch.index.association") >> 'asd'
        properties.getProperty("elasticSearch.index.loadStats") >> 'asd'
        properties.getProperty("elasticSearch.hosts") >> 'bla2020'


        when:
        def result = elasticSearchConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[ElasticSearchConfig.ELASTIC_SEARCH_HOSTS] == ['Hosts must be separated by a ; and port should be separated by : (e.g. host:port;)']
        result.isConfigErroneous() == true
    }
}
