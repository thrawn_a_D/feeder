package de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.03.13
 */
class CouchbaseConfigVerifierTest extends Specification {

    CouchbaseConfigVerifier couchbaseConfigVerifier
    def configLocation = ['asd', 'bla'] as String[]

    def setup(){
        couchbaseConfigVerifier = new CouchbaseConfigVerifier()
    }



    def "Validate configuration when providing all valid properties"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'bla'
        properties.getProperty("couchbase.bucket.name.associations") >> 'bla'
        properties.getProperty("couchbase.bucket.name.user") >> 'bla'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'bla'
        properties.getProperty("couchbase.bucket.password") >> 'bla'
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 0
        result.isConfigErroneous() == false
    }

    def "Do not provide bucket name for feeds"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> ''
        properties.getProperty("couchbase.bucket.name.associations") >> 'bla'
        properties.getProperty("couchbase.bucket.name.user") >> 'bla'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'bla'
        properties.getProperty("couchbase.bucket.password") >> 'bla'
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_BUCKET_NAME_FEEDS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide bucket name for associations"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> ''
        properties.getProperty("couchbase.bucket.name.user") >> 'bla'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'bla'
        properties.getProperty("couchbase.bucket.password") >> 'bla'
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_BUCKET_NAME_ASSOCIATIONS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide bucket name for user"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> 'asd'
        properties.getProperty("couchbase.bucket.name.user") >> ''
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'bla'
        properties.getProperty("couchbase.bucket.password") >> 'bla'
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_BUCKET_NAME_USER] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide bucket name for loadStatistics"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> 'asd'
        properties.getProperty("couchbase.bucket.name.user") >> 'asd'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> ''
        properties.getProperty("couchbase.bucket.password") >> 'bla'
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_BUCKET_NAME_LOAD_STATISTICS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide bucket name for password"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> 'asd'
        properties.getProperty("couchbase.bucket.name.user") >> 'asd'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'asd'
        properties.getProperty("couchbase.bucket.password") >> ''
        properties.getProperty("couchbase.hosts") >> 'asd:8080;'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_BUCKET_PASSWORD] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Do not provide couchbase hosts"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> 'asd'
        properties.getProperty("couchbase.bucket.name.user") >> 'asd'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'asd'
        properties.getProperty("couchbase.bucket.password") >> 'asd'
        properties.getProperty("couchbase.hosts") >> ''

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_HOSTS] == ['Property does not contain a valid value.']
        result.isConfigErroneous() == true
    }

    def "Verify host is properly set"() {
        given:
        def properties = Mock(Properties)
        properties.getProperty("couchbase.bucket.name.feeds") >> 'asdasd'
        properties.getProperty("couchbase.bucket.name.associations") >> 'asd'
        properties.getProperty("couchbase.bucket.name.user") >> 'asd'
        properties.getProperty("couchbase.bucket.name.loadStatistics") >> 'asd'
        properties.getProperty("couchbase.bucket.password") >> 'asd'
        properties.getProperty("couchbase.hosts") >> 'asd98'

        when:
        def result = couchbaseConfigVerifier.verifyConfig(configLocation, properties)

        then:
        result.configLocation == configLocation
        result.propertyErrors.size() == 1
        result.propertyErrors[CouchbaseConfig.COUCHBASE_HOSTS] == ['Hosts must be separated by a ; and port should be separated by : (e.g. host:port;)']
        result.isConfigErroneous() == true
    }
}
