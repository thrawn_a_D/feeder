package de.wsorg.feeder.processor.storage.util.complex.elasticserach

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper.ImmutableSettingsWrapper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.wrapper.TransportClientWrapper
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.ImmutableSettings
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.InetSocketTransportAddress
import org.elasticsearch.common.transport.TransportAddress
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.02.13
 */
class ElasticSearchConnectionManagerTest extends Specification {
    ElasticSearchConnectionManager elasticSearchConnectionManager
    ImmutableSettingsWrapper immutableSettingsWrapper
    TransportClientWrapper transportClientWrapper
    StorageConfigProvider storageConfigProvider
    ElasticSearchConfig elasticSearchConfig

    def setup(){
        elasticSearchConnectionManager = new ElasticSearchConnectionManager()
        immutableSettingsWrapper = Mock(ImmutableSettingsWrapper)
        transportClientWrapper = Mock(TransportClientWrapper)
        storageConfigProvider = Mock(StorageConfigProvider)
        elasticSearchConnectionManager.immutableSettingsWrapper = immutableSettingsWrapper
        elasticSearchConnectionManager.transportClientWrapper = transportClientWrapper
        elasticSearchConnectionManager.storageConfigProvider = storageConfigProvider

        elasticSearchConfig = Mock(ElasticSearchConfig)
        storageConfigProvider.getElasticSearchConfig() >> elasticSearchConfig
    }

    def "Get connection"() {
        given:
        def clusterName = 'elasticSearch'
        def indexName = 'indexName'

        def settingsBuilder = Mock(ImmutableSettings.Builder)
        def transportClient = Mock(TransportClient)

        and:
        def settings = Mock(Settings)
        TransportAddress[] transportAddresses = [Mock(InetSocketTransportAddress)]
        elasticSearchConfig.getElasticSearchHostsAddresses() >> transportAddresses

        when:
        def result = elasticSearchConnectionManager.getClient()

        then:
        result == transportClient
        1 * elasticSearchConfig.getClusterName() >> clusterName
        1 * immutableSettingsWrapper.getSettingsBuilder() >> settingsBuilder
        1 * settingsBuilder.put('cluster.name', clusterName) >> settingsBuilder
        1 * settingsBuilder.build() >> settings
        1 * transportClientWrapper.getTransportClient(settings) >> transportClient
        1 * transportClient.addTransportAddresses(transportAddresses) >> transportClient
    }

    def "Recycle client connection on next get call"() {
        given:
        def client = Mock(TransportClient)
        elasticSearchConnectionManager.transportClient = client

        when:
        def result = elasticSearchConnectionManager.getClient()

        then:
        result == client
    }
}
