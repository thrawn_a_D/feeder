package de.wsorg.feeder.processor.storage.util.complex.config

import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import org.springframework.context.ApplicationContext
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 13.02.13
 */
class StorageConfigProviderTest extends Specification {
    StorageConfigProvider storageConfigProvider
    ApplicationContext applicationContext

    def setup(){
        storageConfigProvider = new StorageConfigProvider()
        applicationContext = Mock(ApplicationContext)
        storageConfigProvider.applicationContext = applicationContext
    }

    def "Provide config for couchbase"() {
        given:
        def config = Mock(CouchbaseConfig)

        when:
        def result = storageConfigProvider.getCouchbaseConfig()

        then:
        result == config
        1 * applicationContext.getBean(CouchbaseConfig) >> config
    }

    def "Provide config for elasticSearch"() {
        given:
        def config = Mock(ElasticSearchConfig)

        when:
        def result = storageConfigProvider.getElasticSearchConfig()

        then:
        result == config
        1 * applicationContext.getBean(ElasticSearchConfig) >> config
    }
}
