package de.wsorg.feeder.processor.storage.feed

import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import spock.lang.Shared
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.02.13
 */
class FeedStorageFactoryTest extends Specification {
    @Shared
    StorageConfig config

    def setup(){
        config = new StorageConfig()
        config.couchbaseConfig = new CouchbaseConfig()
        config.elasticSearchConfig = new ElasticSearchConfig()
        FeedStorageFactory.configProvided = false
    }

    def "Get feed dao"() {
        given:
        FeedStorageFactory.setClientConfig(config)

        when:
        def result = FeedStorageFactory.getFeedDAO()

        then:
        result != null
    }

    def "Check if configuration was set properly when getting a feed dao"() {

        when:
        FeedStorageFactory.getFeedDAO()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Get user feeds dao"() {
        given:
        FeedStorageFactory.setClientConfig(config)

        when:
        def result = FeedStorageFactory.getUserFeedsDAO()

        then:
        result != null
    }

    def "Check if configuration was set properly when getting a user feeds dao"() {

        when:
        FeedStorageFactory.getUserFeedsDAO()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Get feed to user association dao"() {
        given:
        FeedStorageFactory.setClientConfig(config)

        when:
        def result = FeedStorageFactory.getFeedToUserAssociationDAO()

        then:
        result != null
    }

    def "Check if configuration was set properly when getting a user association dao"() {
        when:
        FeedStorageFactory.getFeedToUserAssociationDAO()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Get feed load statistics dao"() {
        given:
        FeedStorageFactory.setClientConfig(config)

        when:
        def result = FeedStorageFactory.getFeedLoadStatisticsDataAccess()

        then:
        result != null
    }

    def "Check if configuration was set properly when getting a feed load statistics dao"() {
        when:
        FeedStorageFactory.getFeedLoadStatisticsDataAccess()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Please provide a valid storage configuration first'
    }

    def "Check that config was inserted into app context"() {
        when:
        FeedStorageFactory.setClientConfig(config)

        then:
        def usedCouchbaseConfigConfig = FeedStorageFactory.applicationContext.getBean(CouchbaseConfig)
        def usedElasticSearchConfigConfig = FeedStorageFactory.applicationContext.getBean(ElasticSearchConfig)
        usedCouchbaseConfigConfig == config.couchbaseConfig
        usedElasticSearchConfigConfig == config.elasticSearchConfig
        FeedStorageFactory.configProvided
    }
}
