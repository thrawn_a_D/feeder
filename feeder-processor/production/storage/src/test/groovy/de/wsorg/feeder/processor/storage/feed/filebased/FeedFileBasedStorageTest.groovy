package de.wsorg.feeder.processor.storage.feed.filebased;


import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.08.12
 */
public class FeedFileBasedStorageTest extends Specification {

    private static final String SYSTEM_TMP_FOLDER=System.getProperty("java.io.tmpdir") + File.separator + "feederStorage"
    private static final String EXPECTED_FEED_STORAGE_FOLDER = "feeds" + File.separator

    private final fileNameToUse = "asdkjasdkjn"
    private final expectFilePath = SYSTEM_TMP_FOLDER + File.separator + fileNameToUse;

    FeedFileBasedStorage feedFileBasedStorage
    FileBasedPersistence<FeedModel> fileBasedPersistence

    def setup(){
        feedFileBasedStorage = new FeedFileBasedStorage()
        fileBasedPersistence = Mock(FileBasedPersistence)
        feedFileBasedStorage.fileBasedPersistence = fileBasedPersistence

        removeTetsFile()
    }

    def "Store a feed model in file"() {
        given:
        FeedModel givenFeedToSave = new FeedModel(feedUrl: 'someTestUrl')
        def feedStorageFolder = EXPECTED_FEED_STORAGE_FOLDER

        when:
        feedFileBasedStorage.save(givenFeedToSave)

        then:
        1 * fileBasedPersistence.save(feedStorageFolder, givenFeedToSave.feedUrl, givenFeedToSave)
    }

    def "Read object from file"() {
        given:
        def expectedModel = new FeedModel()

        when:
        def loadedFeedModel = feedFileBasedStorage.findByFeedUrl(fileNameToUse)

        then:
        loadedFeedModel == expectedModel
        1 * fileBasedPersistence.getModelByFilePath(EXPECTED_FEED_STORAGE_FOLDER, fileNameToUse) >> expectedModel
    }

    def "Find all local feeds"() {
        when:
        def result = feedFileBasedStorage.findAllLocalFeeds()

        then:
        result.size() > 0
        1 * fileBasedPersistence.getFolderContent(EXPECTED_FEED_STORAGE_FOLDER) >> [new FeedModel()]
    }

    def "look for feeds where query appears in title"() {
        given:
        def searchQuery='bla'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some bla text', description: 'lalala')
        def feedToNatExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some blub text', description: 'blub')
        def contentOnStorage = [feedToExpectAsResult, feedToNatExpectAsResult]

        when:
        def result = feedFileBasedStorage.findByTitleOrDescription(searchQuery)

        then:
        result.size() == 1
        1 * fileBasedPersistence.getFolderContent(EXPECTED_FEED_STORAGE_FOLDER) >> contentOnStorage
    }

    def "look for feeds where query appears in description"() {
        given:
        def searchQuery='bla'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some sss text', description: 'blaub')
        def feedToNatExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some blub text', description: 'dsa')
        def contentOnStorage = [feedToExpectAsResult, feedToNatExpectAsResult]

        when:
        def result = feedFileBasedStorage.findByTitleOrDescription(searchQuery)

        then:
        result.size() == 1
        1 * fileBasedPersistence.getFolderContent(EXPECTED_FEED_STORAGE_FOLDER) >> contentOnStorage
    }

    def "If no query is provided no feeds should be found"() {
        given:
        def searchQuery=''

        when:
        def result = feedFileBasedStorage.findByTitleOrDescription(searchQuery)

        then:
        result.size() == 0
    }

    def "Ignore case sensitivity while searching for description"() {
        given:
        def searchQuery='blAub'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some sss text', description: 'blaub')
        def contentOnStorage = [feedToExpectAsResult]

        when:
        def result = feedFileBasedStorage.findByTitleOrDescription(searchQuery)

        then:
        result.size() == 1
        result[0].description==feedToExpectAsResult.description
        1 * fileBasedPersistence.getFolderContent(EXPECTED_FEED_STORAGE_FOLDER) >> contentOnStorage
    }

    def "Ignore case sensitivity while searching for title"() {
        given:
        def searchQuery='DsS'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some dss text', description: 'blaub')
        def contentOnStorage = [feedToExpectAsResult]

        when:
        def result = feedFileBasedStorage.findByTitleOrDescription(searchQuery)

        then:
        result.size() == 1
        result[0].title==feedToExpectAsResult.title
        1 * fileBasedPersistence.getFolderContent(EXPECTED_FEED_STORAGE_FOLDER) >> contentOnStorage
    }

    def "Find feed of url and containing text in title or description"() {
        given:
        def text = 'sss'
        def feedUrl = 'http://asd'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some sss text', description: 'blaub')
        def contentOnStorage = feedToExpectAsResult

        when:
        def result = feedFileBasedStorage.findByFeedUrlAndTitleOrDescription(feedUrl, text)

        then:
        result == feedToExpectAsResult
        1 * fileBasedPersistence.getModelByFilePath(EXPECTED_FEED_STORAGE_FOLDER, feedUrl) >> contentOnStorage
    }

    def "Find feed of url but NOT containing text in title or description"() {
        given:
        def text = 'sss'
        def feedUrl = 'http://asd'

        and:
        def feedToExpectAsResult = new FeedModel(feedUrl: 'http://asd', title: 'some text', description: 'blaub')
        def contentOnStorage = feedToExpectAsResult

        when:
        def result = feedFileBasedStorage.findByFeedUrlAndTitleOrDescription(feedUrl, text)

        then:
        result == null
        1 * fileBasedPersistence.getModelByFilePath(EXPECTED_FEED_STORAGE_FOLDER, feedUrl) >> contentOnStorage
    }

    def "Remove feed"() {
        given:
        def feedUrl = 'lihilh'

        when:
        feedFileBasedStorage.deleteFeed(feedUrl)

        then:
        1 * fileBasedPersistence.remove(EXPECTED_FEED_STORAGE_FOLDER, feedUrl)
    }

    private void removeTetsFile() {
        def ourTestFile = new File(expectFilePath)
        if (ourTestFile.exists())
            ourTestFile.delete();
    }
}
