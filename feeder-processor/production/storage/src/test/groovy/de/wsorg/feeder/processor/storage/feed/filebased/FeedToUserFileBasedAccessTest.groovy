package de.wsorg.feeder.processor.storage.feed.filebased

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.AssociationType
import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.09.12
 */
class FeedToUserFileBasedAccessTest extends Specification {
    FeedToUserFileBasedAccess feedToUserFileBasedAccess
    FileBasedPersistence fileBasedPersistence

    def setup(){
        feedToUserFileBasedAccess = new FeedToUserFileBasedAccess()
        fileBasedPersistence = Mock(FileBasedPersistence)
        feedToUserFileBasedAccess.fileBasedPersistence = fileBasedPersistence
    }

    def "Check save functionality"() {
        given:
        FeedToUserAssociation feedToUserAssociation = new FeedToUserAssociation()
        feedToUserAssociation.userId = 'asd123'
        feedToUserAssociation.feedUrl = 'http:/asdkjh'

        and:
        def expectedIdentifier = feedToUserAssociation.userId + "_" + feedToUserAssociation.feedUrl

        when:
        feedToUserFileBasedAccess.save(feedToUserAssociation)

        then:
        1 * fileBasedPersistence.save(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS,
                                         expectedIdentifier,
                                         feedToUserAssociation)
    }

    def "Check that userId is set before saving can proceed"() {
        given:
        FeedToUserAssociation feedToUserAssociation = new FeedToUserAssociation()
        feedToUserAssociation.feedUrl = 'http:/asdkjh'

        when:
        feedToUserFileBasedAccess.save(feedToUserAssociation)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided association has no set userId"
    }

    def "Check that feedUrl is set before saving can proceed"() {
        given:
        FeedToUserAssociation feedToUserAssociation = new FeedToUserAssociation()
        feedToUserAssociation.userId = '123wer123'

        when:
        feedToUserFileBasedAccess.save(feedToUserAssociation)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided association has no set feedUrl"
    }

    def "Find feeds related to a user"() {
        given:
        def userId = 'userId123'

        and:
        def expectedFeedToUSerAssociation = new FeedToUserAssociation(userId: userId)
        def expectedFeedToUSerAssociation2 = new FeedToUserAssociation(userId: '43')

        when:
        def result = feedToUserFileBasedAccess.getUserFeeds(userId)

        then:
        result.size() > 0
        result[0].userId == userId
        1 * fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> [expectedFeedToUSerAssociation, expectedFeedToUSerAssociation2]
    }

    def "Find associations related to a feedUrl"() {
        given:
        def feedUrl = 'lkjnlkjb'

        and:
        def expectedFeedToUSerAssociation = new FeedToUserAssociation(feedUrl: feedUrl)
        def expectedFeedToUSerAssociation2 = new FeedToUserAssociation(feedUrl: 'sdfsdf')

        when:
        def result = feedToUserFileBasedAccess.getAssociationsOfFeed(feedUrl)

        then:
        result.size() > 0
        result[0].feedUrl == feedUrl
        1 * fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> [expectedFeedToUSerAssociation, expectedFeedToUSerAssociation2]
    }

    def "Feed association for user and feed url is given as wanted"() {
        given:
        def userId='12345'
        def feedUrl='http://bla'
        def associationType = AssociationType.OWNER

        and:
        def expectedFeedToUSerAssociation = new FeedToUserAssociation(userId: userId, associationType: AssociationType.OWNER, feedUrl: feedUrl)
        def expectedFeedToUSerAssociation2 = new FeedToUserAssociation(userId: userId, associationType: AssociationType.SUBSCRIBER, feedUrl: 'http://lo')
        def expectedFeedToUSerAssociation3 = new FeedToUserAssociation(userId: '43')
        def userFeeds=[expectedFeedToUSerAssociation, expectedFeedToUSerAssociation2, expectedFeedToUSerAssociation3]

        when:
        def associationGiven = feedToUserFileBasedAccess.isAssociationGiven(feedUrl, userId, associationType)

        then:
        associationGiven
        1 * fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> userFeeds
    }

    def "No feeds found with a given association of user found"() {
        given:
        def userId='12345'
        def feedUrl='http://bla'
        def associationType = AssociationType.SUBSCRIBER

        and:
        def expectedFeedToUSerAssociation = new FeedToUserAssociation(userId: userId, associationType: AssociationType.OWNER, feedUrl: feedUrl)
        def expectedFeedToUSerAssociation2 = new FeedToUserAssociation(userId: userId, associationType: AssociationType.OWNER, feedUrl: 'http://lo')
        def expectedFeedToUSerAssociation3 = new FeedToUserAssociation(userId: '43')
        def userFeeds=[expectedFeedToUSerAssociation, expectedFeedToUSerAssociation2, expectedFeedToUSerAssociation3]

        when:
        def associationGiven = feedToUserFileBasedAccess.isAssociationGiven(feedUrl, userId, associationType)

        then:
        associationGiven == false
        1 * fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> userFeeds
    }

    def "Get feed to user association based on a feedUrl and a user id"() {
        given:
        def userId='12345'
        def feedUrl='http://bla'

        and:
        def association = new FeedToUserAssociation(feedUrl: feedUrl, userId: userId)
        def userFeeds=[association]

        and:
        fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> userFeeds

        when:
        def result = feedToUserFileBasedAccess.getUserFeedAssociation(feedUrl, userId)

        then:
        association == result
    }

    def "Remove association from storage"() {
        given:
        def userId='12345'
        def feedUrl='http://bla'

        and:
        FeedToUserAssociation feedToUserAssociation = new FeedToUserAssociation()
        feedToUserAssociation.feedUrl = feedUrl
        feedToUserAssociation.userId = userId

        when:
        feedToUserFileBasedAccess.removeAssociation(feedUrl, userId)

        then:
        1 * fileBasedPersistence.getFolderContent(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS) >> [feedToUserAssociation]
        1 * fileBasedPersistence.remove(FeedToUserFileBasedAccess.STORAGE_FOLDER_FOR_ASSOCIATIONS, userId + '_' + feedUrl)
    }
}
