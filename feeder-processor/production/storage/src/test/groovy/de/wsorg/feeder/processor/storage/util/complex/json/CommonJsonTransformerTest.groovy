package de.wsorg.feeder.processor.storage.util.complex.json

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntriesCollection
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.02.13
 */
class CommonJsonTransformerTest extends Specification {
    CommonJsonTransformer<FeedModel> commonJsonTransformer

    def setup(){
        commonJsonTransformer = new CommonJsonTransformer()
    }


    def "Transform a feed model into json and set object type"() {
        given:
        def modelToTransform = new FeedModel()
        modelToTransform.title = 'title'
        modelToTransform.feedEntries = [new FeedEntryModel()]
        modelToTransform.feedEntriesCollection = new FeedEntriesCollection()

        when:
        def result = commonJsonTransformer.getJsonForObject(modelToTransform, 'feedModel')

        then:
        result
        result.contains(""""title":"${modelToTransform.title}\"""")
        result.contains(""""feedEntriesCollection":{"additionalPagingPossible":false}""")
    }

    def "Make sure a field representing the object type is added to json"() {
        given:
        def modelToTransform = new FeedModel()
        modelToTransform.title = 'title'

        and:
        def objectType = 'feedModel'

        when:
        def result = commonJsonTransformer.getJsonForObject(modelToTransform, objectType)

        then:
        result
        result.contains(""""type":"${objectType}\"""")
    }

    def "Transform json string back to object"() {
        given:
        def jsonString = """{"type":"feedModel","feedEntriesCollection":{"additionalPagingPossible":false},"feedEntries":[{}],"title":"title"}"""

        when:
        def result = commonJsonTransformer.getObjectFromJson(jsonString, FeedModel)

        then:
        result
    }

    def "Transform map into an object"() {
        given:
        def map = [feedUrl:'http://asd', title:'tsdfs']

        when:
        FeedModel result = commonJsonTransformer.getObjectFromMap(map, FeedModel)

        then:
        result
        result.feedUrl == map.feedUrl
        result.title == map.title
    }
}
