package de.wsorg.feeder.processor.storage.feed.filebased

import de.wsorg.feeder.processor.production.domain.feeder.feed.association.FeedToUserAssociation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.feed.FeedDataAccess
import de.wsorg.feeder.processor.storage.feed.FeedToUserAssociationStorage
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.12.12
 */
class UsersFeedStorageFileBasedAccessTest extends Specification {
    UsersFeedStorageFileBasedAccess usersFeedStorageFileBasedAccess
    FeedDataAccess feedDataAccess
    FeedToUserAssociationStorage feedToUserAssociationStorage

    def setup(){
        usersFeedStorageFileBasedAccess = new UsersFeedStorageFileBasedAccess()
        feedDataAccess = Mock(FeedDataAccess)
        feedToUserAssociationStorage = Mock(FeedToUserAssociationStorage)
        usersFeedStorageFileBasedAccess.feedDataAccess = feedDataAccess
        usersFeedStorageFileBasedAccess.feedToUserAssociationStorage = feedToUserAssociationStorage
    }

    def "Find users feeds by a userId"() {
        given:
        def userId = "123"

        and:
        def providedFeedToUser = [new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf1'),
                                  new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf2')]

        and:
        def expectedFeedModel = new FeedModel()

        when:
        def result = usersFeedStorageFileBasedAccess.getUserFeeds(userId)

        then:
        result.size() > 0
        1 * feedToUserAssociationStorage.getUserFeeds(userId) >> providedFeedToUser
        2 * feedDataAccess.findByFeedUrl({it == providedFeedToUser[0].feedUrl || it == providedFeedToUser[1].feedUrl}) >> expectedFeedModel
    }

    def "User has no feeds, retreive empty list"() {
        given:
        def userId = "123"

        when:
        def result = usersFeedStorageFileBasedAccess.getUserFeeds(userId)

        then:
        result.size() == 0
        1 * feedToUserAssociationStorage.getUserFeeds(userId) >> []
    }

    def "Find users feeds by userId and title text"() {
        given:
        def userId = "123"
        def title = 'title'

        and:
        def providedFeedToUser = [new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf1'),
                new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf2')]

        and:
        def expectedFeedModel = new FeedModel()

        when:
        def result = usersFeedStorageFileBasedAccess.getUserFeeds(userId, title)

        then:
        result.size() > 0
        1 * feedToUserAssociationStorage.getUserFeeds(userId) >> providedFeedToUser
        2 * feedDataAccess.findByFeedUrlAndTitleOrDescription({it == providedFeedToUser[0].feedUrl || it == providedFeedToUser[1].feedUrl}, title) >> expectedFeedModel
    }

    def "No feed found which matches the title or description text"() {
        given:
        def userId = "123"
        def title = 'title'

        and:
        def providedFeedToUser = [new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf1'),
                new FeedToUserAssociation(userId: userId, feedUrl: 'http://sdf2')]

        when:
        def result = usersFeedStorageFileBasedAccess.getUserFeeds(userId, title)

        then:
        result.size() == 0
        1 * feedToUserAssociationStorage.getUserFeeds(userId) >> providedFeedToUser
        2 * feedDataAccess.findByFeedUrlAndTitleOrDescription({it == providedFeedToUser[0].feedUrl || it == providedFeedToUser[1].feedUrl}, title) >> null
    }
}
