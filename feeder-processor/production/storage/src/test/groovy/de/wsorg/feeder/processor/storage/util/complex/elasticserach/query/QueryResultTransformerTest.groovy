package de.wsorg.feeder.processor.storage.util.complex.elasticserach.query

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.search.SearchHit
import org.elasticsearch.search.SearchHits
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 11.02.13
 */
class QueryResultTransformerTest extends Specification {
    QueryResultTransformer queryResultTransformer
    FeederJsonTransformer jsonTransformer

    def setup(){
        queryResultTransformer = new QueryResultTransformer()
        jsonTransformer = Mock(FeederJsonTransformer)
        queryResultTransformer.jsonTransformer = jsonTransformer
    }

    def "Transform search result to model"() {
        given:
        def searchResult = Mock(SearchResponse)
        def searchHits = Mock(SearchHits)
        def searchHit = Mock(SearchHit)
        searchResult.getHits() >> searchHits
        searchHits.getHits() >> [searchHit].toArray()
        searchHit.getSource() >> Mock(Map)
        searchHits.totalHits >> 1

        and:
        def modelAsHashMap = Mock(Map)

        and:
        def expectedModel = Mock(FeedModel)

        when:
        def result = queryResultTransformer.transformResultToModel(searchResult, FeedModel)

        then:
        result
        result[0] == expectedModel
        1 * searchHit.getSource().get("doc") >> modelAsHashMap
        1 * jsonTransformer.getObjectFromMap(modelAsHashMap, FeedModel) >> expectedModel
    }

    def "Result was empty"() {
        given:
        def searchResult = Mock(SearchResponse)
        def searchHits = Mock(SearchHits)
        searchResult.getHits() >> searchHits
        searchHits.totalHits >> 0

        when:
        def result = queryResultTransformer.transformResultToModel(searchResult, FeedModel)

        then:
        result.empty
    }
}
