package de.wsorg.feeder.processor.storage.feed.complex

import com.couchbase.client.CouchbaseClient
import com.couchbase.client.protocol.views.Query
import com.couchbase.client.protocol.views.View
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.storage.util.complex.config.StorageConfigProvider
import de.wsorg.feeder.processor.storage.util.complex.couchbase.CouchbaseConnectionManager
import de.wsorg.feeder.processor.storage.util.complex.couchbase.configuration.CouchbaseConfig
import de.wsorg.feeder.processor.storage.util.complex.couchbase.query.CouchbaseQueryHelper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.configuration.ElasticSearchConfig
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.ElasticSearchQueryHelper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.BoolQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.MultimatchQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.elasticserach.query.builder.TermQueryWrapper
import de.wsorg.feeder.processor.storage.util.complex.json.FeederJsonTransformer
import org.elasticsearch.index.query.BoolQueryBuilder
import org.elasticsearch.index.query.QueryBuilder
import org.elasticsearch.index.query.TermQueryBuilder
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.02.13
 */
class ComplexFeedDAOTest extends Specification {
    ComplexFeedDAO couchbaseFeedDAO
    FeederJsonTransformer<FeedModel> jsonTransformer
    CouchbaseConnectionManager couchbaseConnectionManager
    CouchbaseQueryHelper queryHelper
    ElasticSearchQueryHelper elasticSearchQueryHelper
    StorageConfigProvider storageConfigProvider

    MultimatchQueryWrapper multimatchQueryBuilder
    BoolQueryWrapper boolQueryBuilderWraper
    TermQueryWrapper termQueryWrapper

    ElasticSearchConfig elasticSearchConfig
    CouchbaseConfig couchbaseConfig

    def bucketName = 'feeder-feeds'

    def setup(){
        couchbaseFeedDAO = new ComplexFeedDAO()
        jsonTransformer = Mock(FeederJsonTransformer)
        couchbaseConnectionManager = Mock(CouchbaseConnectionManager)
        elasticSearchQueryHelper = Mock(ElasticSearchQueryHelper)
        storageConfigProvider = Mock(StorageConfigProvider)
        queryHelper = Mock(CouchbaseQueryHelper)
        couchbaseConfig = Mock(CouchbaseConfig)

        multimatchQueryBuilder = Mock(MultimatchQueryWrapper)
        boolQueryBuilderWraper = Mock(BoolQueryWrapper)
        termQueryWrapper = Mock(TermQueryWrapper)

        elasticSearchConfig = Mock(ElasticSearchConfig)

        couchbaseFeedDAO.jsonTransformer = jsonTransformer
        couchbaseFeedDAO.couchbaseConnectionManager = couchbaseConnectionManager
        couchbaseFeedDAO.couchbaseQueryHelper = queryHelper
        couchbaseFeedDAO.elasticSearchQueryHelper = elasticSearchQueryHelper
        couchbaseFeedDAO.storageConfigProvider = storageConfigProvider

        couchbaseFeedDAO.multimatchQueryWrapper = multimatchQueryBuilder
        couchbaseFeedDAO.boolQueryWrapper = boolQueryBuilderWraper
        couchbaseFeedDAO.termQueryWrapper = termQueryWrapper

        storageConfigProvider.getElasticSearchConfig() >> elasticSearchConfig
        storageConfigProvider.getCouchbaseConfig() >> couchbaseConfig

        couchbaseConfig.getFeedsBucketName() >> bucketName
    }

    def "Store a feed model in the database"() {
        given:
        def model = new FeedModel()
        model.feedUrl = 'http:/feed'

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        def result = couchbaseFeedDAO.save(model)

        then:
        result
        1 * jsonTransformer.getJsonForObject(model, 'feedModel') >> modelAsJson
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.set(model.feedUrl, 0, modelAsJson)
    }

    def "Feed url needs to be set before storing an object"() {
        given:
        def model = new FeedModel()

        when:
        couchbaseFeedDAO.save(model)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided feed model to be stored does not have a feed url set.'
    }

    def "Get a feed identified by a feedUrl"() {
        given:
        def feedUrl = 'http:/feed'
        def model = new FeedModel()
        model.feedUrl = feedUrl

        and:
        def modelAsJson = 'asdin98hjoi'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        def result = couchbaseFeedDAO.findByFeedUrl(feedUrl)

        then:
        result
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.get(feedUrl) >> modelAsJson
        1 * jsonTransformer.getObjectFromJson(modelAsJson, FeedModel) >> model
    }

    def "Query for all stored feed model data"() {
        given:
        def couchbaseClient = Mock(CouchbaseClient)

        and:
        def view = Mock(View)
        def expectedList = [new FeedModel()]

        and:
        def expectedModel = new FeedModel()

        when:
        def result = couchbaseFeedDAO.findAllLocalFeeds()

        then:
        result
        result[0] == expectedModel
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.getView("dev_feed_title_or_description", "feed_title_or_description") >> view
        1 * queryHelper.findDataInViewRelatedToQuery(couchbaseClient, view, {Query it-> it != null}, FeedModel) >> expectedList
    }

    def "Find an entry by title or id"() {
        given:
        def indexName = 'feeder-feed'
        elasticSearchConfig.getFeedsIndexName() >> indexName
        def query = 'query'

        and:
        def queryBuilder = Mock(QueryBuilder)

        when:
        couchbaseFeedDAO.findByTitleOrDescription(query)

        then:
        1 * multimatchQueryBuilder.getMultiMatchQuery(query, 'doc.title', 'description') >> queryBuilder
        1 * elasticSearchQueryHelper.processQuery(queryBuilder, indexName, FeedModel)
    }

    def "Find by title query and description"() {
        given:
        def indexName = 'feeder-feed'
        elasticSearchConfig.getFeedsIndexName() >> indexName
        def query = 'query'
        def feedUrl = 'http://ewf'

        and:
        def multiQueryBuilder = Mock(QueryBuilder)
        def boolQueryBuilder = Mock(BoolQueryBuilder)
        def termQueryBuilder = Mock(TermQueryBuilder)

        and:
        def expectedResult = [new FeedModel()]

        when:
        couchbaseFeedDAO.findByFeedUrlAndTitleOrDescription(feedUrl, query)

        then:
        1 * multimatchQueryBuilder.getMultiMatchQuery(query, 'doc.title', 'description') >> multiQueryBuilder
        1 * boolQueryBuilderWraper.getBoolQueryBuilder() >> boolQueryBuilder
        1 * termQueryWrapper.getTermQueryBuilder('_id', feedUrl) >> termQueryBuilder
        1 * boolQueryBuilder.must(termQueryBuilder) >> boolQueryBuilder
        1 * boolQueryBuilder.must(multiQueryBuilder) >> boolQueryBuilder
        1 * elasticSearchQueryHelper.processQuery(boolQueryBuilder, indexName, FeedModel) >> expectedResult
    }

    def "Remove feed from storage"() {
        given:
        def feedUrl = 'kjkih'

        and:
        def couchbaseClient = Mock(CouchbaseClient)

        when:
        couchbaseFeedDAO.deleteFeed(feedUrl)

        then:
        1 * couchbaseConnectionManager.getConnection(bucketName) >> couchbaseClient
        1 * couchbaseClient.delete(feedUrl)
    }
}
