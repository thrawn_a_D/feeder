package de.wsorg.feeder.processor.storage.util.complex.config

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.02.13
 */
class StorageConfigTest extends Specification {
    def "Use filesystem as storage"() {
        given:
        def configProperties = new Properties()
        configProperties.put("useFileSystemAsStorage", "true")
        configProperties.put("couchbase.bucket.name.feeds", "asd")
        configProperties.put("couchbase.bucket.name.associations", "asd")
        configProperties.put("couchbase.bucket.name.user", "asd")
        configProperties.put("couchbase.bucket.name.loadStatistics", "asd")
        configProperties.put("couchbase.bucket.password", "asd")
        configProperties.put("elasticSearch.clusterName", "asd")
        configProperties.put("elasticSearch.index.feeds", "asd")
        configProperties.put("elasticSearch.index.association", "asd")
        configProperties.put("elasticSearch.index.loadStats", "asd")

        when:
        def configuration = new StorageConfig(configProperties)

        then:
        configuration.useFileSystemAsStorage == true
    }
}
