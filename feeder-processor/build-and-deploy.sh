#!/bin/bash

executeAsRoot() {
     sudo $*
}

buildProject() {
     echo "Start building project"
     mvn clean install -DskipTests=true -P unitTestsOnly > /dev/null
     echo "Build finished."
}

stopTomcat() {
     echo "Stopping tomcat"
     executeAsRoot /etc/init.d/tomcat7 stop
}

cleanupWebappsFolder() {
     echo "Removing old webapp artefacts"
     executeAsRoot rm -rf /var/lib/tomcat7/webapps/user-*
     executeAsRoot rm -rf /var/lib/tomcat7/webapps/update-*
     executeAsRoot rm -rf /var/lib/tomcat7/webapps/feeder-*
}

copyWarToWebApps() {
     echo "Deploying webapps"
     executeAsRoot cp production/updater/feeder-update-receiver/feeder-update-receiver-webapp/target/update-receiver.war /var/lib/tomcat7/webapps/
     executeAsRoot cp production/updater/feeder-update-manager/feeder-update-manager-webapp/target/update-manager.war /var/lib/tomcat7/webapps/
     executeAsRoot cp production/updater/feeder-update-service-endpoint/feeder-update-service-endpoint-webapp/target/update-service-endpoint.war /var/lib/tomcat7/webapps/
     executeAsRoot cp production/execution/tomcat/user-handling-tomcat-execution/target/user-handling.war /var/lib/tomcat7/webapps/
     executeAsRoot cp production/execution/tomcat/feeder-handling-tomcat-execution/target/feeder-handling.war /var/lib/tomcat7/webapps/
}

startTomcat() {
     echo "Starting tomcat"
     executeAsRoot su root -c "export JRE_HOME='/usr/lib/jvm/java-7-oracle/' /etc/init.d/tomcat7 start"
}

buildProject()
stopTomcat()
cleanupWebappsFolder()
copyWarToWebApps()
startTomcat()
