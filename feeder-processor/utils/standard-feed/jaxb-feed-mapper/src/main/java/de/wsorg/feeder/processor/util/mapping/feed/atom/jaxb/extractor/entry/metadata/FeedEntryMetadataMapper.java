package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedEntryMetadataMapper<T extends StandardFeedEntry> {
    void mapFeedEntryMetadata(final T newEntryFeeder, final QName qName, final String value);
}
