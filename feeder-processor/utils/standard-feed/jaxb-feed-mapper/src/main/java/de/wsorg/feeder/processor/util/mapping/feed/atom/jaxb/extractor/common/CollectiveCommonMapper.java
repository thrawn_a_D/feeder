package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class CollectiveCommonMapper {

    @Resource(name = "commonAtomValueMappers")
    private List<CommonAtomValueMapper> commonAtomValueMappers;

    public void mapElementToCommonAtomItem(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        for (CommonAtomValueMapper commonAtomValueMapper : commonAtomValueMappers) {
            commonAtomValueMapper.mapValueToFeed(mappedFeedResult, currentJaxbElement);
        }
    }
}
