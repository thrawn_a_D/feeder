package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface CommonAtomValueMapper {
    void mapValueToFeed(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement);
}
