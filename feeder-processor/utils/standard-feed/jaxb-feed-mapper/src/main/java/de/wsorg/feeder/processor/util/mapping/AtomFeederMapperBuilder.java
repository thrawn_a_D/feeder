package de.wsorg.feeder.processor.util.mapping;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomFeederMapperBuilder {
    private List<FeedMetadataMapper> feedMetadataMappers;
    private List<FeedEntryMetadataMapper> feedEntryMetadataMappers;
    private DomainBuilderFactory domainBuilderFactory;

    public AtomFeederMapperBuilder feedMetadataMappers(final List<FeedMetadataMapper> feedMetadataMappers) {
        this.feedMetadataMappers = feedMetadataMappers;
        return this;
    }

    public AtomFeederMapperBuilder feedEntryMetadataMappers(final List<FeedEntryMetadataMapper> feedEntryMetadataMappers) {
        this.feedEntryMetadataMappers = feedEntryMetadataMappers;
        return this;
    }

    public AtomFeederMapperBuilder domainBuilderFactory(final DomainBuilderFactory domainBuilderFactory) {
        this.domainBuilderFactory = domainBuilderFactory;
        return this;
    }

    public XmlToDomainMapper<StandardFeed> build(){
        ClassPathXmlApplicationContext applicationContext;
        applicationContext = new ClassPathXmlApplicationContext("classpath:spring/feederMappingContext.xml");

        adjsutFeedMetadataMapper(applicationContext);
        adjustFeedEntryMetadataMapper(applicationContext);
        adjustDomainBuilder(applicationContext);

        return applicationContext.getBean("responseAtomXmlMapper", XmlToDomainMapper.class);
    }

    private void adjustDomainBuilder(final ClassPathXmlApplicationContext applicationContext) {
        if(this.domainBuilderFactory != null){
            applicationContext.getBeanFactory().registerSingleton("atomDomainObjectBuilder", this.domainBuilderFactory);
        }
    }

    private void adjustFeedEntryMetadataMapper(final ClassPathXmlApplicationContext applicationContext) {
        if (this.feedEntryMetadataMappers != null && this.feedEntryMetadataMappers.size() > 0) {
            List<FeedEntryMetadataMapper> feedMetadataMappers = applicationContext.getBean("feedEntryMetadataMappers", List.class);
            feedMetadataMappers.addAll(this.feedEntryMetadataMappers);
        }
    }

    private void adjsutFeedMetadataMapper(final ClassPathXmlApplicationContext applicationContext) {
        if (this.feedMetadataMappers != null && this.feedMetadataMappers.size() > 0) {
            List<FeedMetadataMapper> feedMetadataMappers = applicationContext.getBean("feedMetadataMappers", List.class);
            feedMetadataMappers.addAll(this.feedMetadataMappers);
        }
    }
}
