package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedMetadataMapper<T extends StandardFeed> {
    void mapFeedMetadata(final T StandardAtom, final QName qName, final String value);
}
