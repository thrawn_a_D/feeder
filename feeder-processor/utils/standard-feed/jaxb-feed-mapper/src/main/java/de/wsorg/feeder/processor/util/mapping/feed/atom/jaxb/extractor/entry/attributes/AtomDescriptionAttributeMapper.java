package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper;

import javax.inject.Inject;
import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomDescriptionAttributeMapper implements AtomEntryAttributeMapper {
    private static final String DESCRIPTION_TAG = "summary";

    @Inject
    private JaxbToFeedMappingHelper jaxbToFeedMappingHelper;

    @Override
    public void mapAtomEntryValue(final StandardFeedEntry newEntryFeeder, final JAXBElement currentJaxbElement) {
        if (jaxbToFeedMappingHelper.isElement(currentJaxbElement, DESCRIPTION_TAG)) {
            final String description = jaxbToFeedMappingHelper.getText(currentJaxbElement);
            newEntryFeeder.setDescription(description);
        }
    }
}
