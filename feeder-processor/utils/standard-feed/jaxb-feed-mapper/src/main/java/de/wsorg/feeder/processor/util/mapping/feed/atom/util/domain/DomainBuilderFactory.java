package de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface DomainBuilderFactory<T extends StandardFeed, F extends StandardFeedEntry> {
    T getAtomModelObject();
    F getAtomEntryModelObject();
}
