package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.AbstractAtomValueMapper;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomTitleValueMapper extends AbstractAtomValueMapper {

    private static final String TITLE_TAG = "title";

    @Override
    protected String getIdentifyingElementName() {
        return TITLE_TAG;
    }

    @Override
    public void performActualAction(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        mappedFeedResult.setTitle(jaxbToFeedMappingHelper.getText(currentJaxbElement));
    }
}
