package de.wsorg.feeder.processor.util.mapping.feed.generic;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface JaxbPojoToFeederPojoMapper<T, F> {
    T mapJaxbPojoToFeederPojo(final F objectToMap);
}
