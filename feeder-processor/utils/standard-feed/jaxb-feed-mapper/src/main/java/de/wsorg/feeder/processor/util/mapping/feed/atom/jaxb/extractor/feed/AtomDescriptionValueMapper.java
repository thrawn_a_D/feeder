package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.AbstractAtomValueMapper;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomDescriptionValueMapper extends AbstractAtomValueMapper {

    private static final String SUBTITLE_TAG = "subtitle";

    @Override
    protected String getIdentifyingElementName() {
        return SUBTITLE_TAG;
    }

    @Override
    public void performActualAction(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        mappedFeedResult.setDescription(jaxbToFeedMappingHelper.getText(currentJaxbElement));
    }
}
