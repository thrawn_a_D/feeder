package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface AtomEntryAttributeMapper {
    void mapAtomEntryValue(final StandardFeedEntry newEntryFeeder, final JAXBElement currentJaxbElement);
}
