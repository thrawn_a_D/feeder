package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.CollectiveCommonMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.AtomEntryMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.FeedType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named("jaxbAtomToFeederApiAtom")
@Slf4j
public class JaxbAtomToFeederApiAtom implements JaxbPojoToFeederPojoMapper<StandardFeed, JAXBElement> {
    private static final String ENTRY_TAG = "entry";

    @Inject
    private JaxbToFeedMappingHelper jaxbToFeedMappingHelper;

    @Inject
    private CollectiveCommonMapper collectiveCommonMapper;

    @Resource(name = "feedMetadataMappers")
    private List<FeedMetadataMapper> feedMetadataMappers;

    @Inject
    private ApplicationContext applicationContext;

    private DomainBuilderFactory<StandardFeed, StandardFeedEntry> domainBuilderFactory;

    @Inject
    private AtomEntryMapper atomEntryMapper;

    public StandardFeed mapJaxbPojoToFeederPojo(final JAXBElement providedJaxbElement){
        StandardFeed mappedFeedResult = getDomainBuilderFactory().getAtomModelObject();

        if(providedJaxbElement.getValue() instanceof FeedType) {
            FeedType jaxbAtom  = (FeedType) providedJaxbElement.getValue();

            mapAtomAttributes(mappedFeedResult, jaxbAtom);
            processFeedMetadata(mappedFeedResult, jaxbAtom);
        } else {
            throw  new IllegalArgumentException("The provided JAXBElement does not contain a FeedType");
        }

        return mappedFeedResult;
    }

    private void mapAtomAttributes(final StandardFeed mappedFeedResult, final FeedType jaxbAtom) {
        for(Object currentElement : jaxbAtom.getAuthorOrCategoryOrContributor()) {
            JAXBElement currentJaxbElement = getJaxbElement(currentElement);

            if (currentJaxbElement != null) {
                collectiveCommonMapper.mapElementToCommonAtomItem(mappedFeedResult, currentJaxbElement);

                if (jaxbToFeedMappingHelper.isElement(currentJaxbElement, ENTRY_TAG )) {
                    final StandardFeedEntry newEntryFeeder = atomEntryMapper.processFeedEntry(currentJaxbElement);
                    mappedFeedResult.addEntry(newEntryFeeder);
                }
            }
        }
    }

    private JAXBElement getJaxbElement(final Object currentElement) {
        JAXBElement currentJaxbElement = null;
        try {
            currentJaxbElement = (JAXBElement) currentElement;
        } catch (Exception e) {
            log.debug("An invalid element was found in the atom feed: {}", currentElement.toString());
        }
        return currentJaxbElement;
    }

    private void processFeedMetadata(final StandardFeed mappedFeedResult, final FeedType jaxbAtom) {
        for (QName qSetName : jaxbAtom.getOtherAttributes().keySet()) {
            final String value = jaxbAtom.getOtherAttributes().get(qSetName);
            for (FeedMetadataMapper feedMetadataMapper : feedMetadataMappers) {
                feedMetadataMapper.mapFeedMetadata(mappedFeedResult, qSetName, value);
            }
        }
    }

    private DomainBuilderFactory<StandardFeed, StandardFeedEntry> getDomainBuilderFactory(){
        if (this.domainBuilderFactory == null) {
            this.domainBuilderFactory = applicationContext.getBean("atomDomainObjectBuilder", DomainBuilderFactory.class);
        }

        return domainBuilderFactory;
    }
}
