package de.wsorg.feeder.processor.util.mapping.feed.generic;


import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller;

import javax.xml.bind.JAXBException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class XmlToDomainMapperTemplate<T, F> implements XmlToDomainMapper<T> {
    protected JaxbUnmarshaller jaxbUnmarshaller;

    protected abstract String getGeneratedJaxbClassesPackage();
    protected abstract JaxbPojoToFeederPojoMapper<T, F> getJaxbPojoToFeederPojoMapper();

    @Override
    public T getResult(final String responseXml) {
        F feed = unmarshall(responseXml);

        return getJaxbPojoToFeederPojoMapper().mapJaxbPojoToFeederPojo(feed);
    }

    private JaxbUnmarshaller getJaxbUnmarshaller(){
        if(jaxbUnmarshaller == null) {
            jaxbUnmarshaller = UtilsFactory.getJaxbUnmarshaller();
        }

        jaxbUnmarshaller.setJaxbGeneratedClassesPackage(getGeneratedJaxbClassesPackage());
        return jaxbUnmarshaller;
    }

    private F unmarshall(final String responseXml) {
        try {
            return  (F) getJaxbUnmarshaller().unmarshal(responseXml);
        } catch (JAXBException e) {
            throw new IllegalArgumentException("The provided xml is not a valid jaxb object");
        }
    }
}
