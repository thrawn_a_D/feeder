package de.wsorg.feeder.processor.util.mapping.feed.atom;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapperTemplate;

import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named(value = "responseAtomXmlMapper")
public class AtomXmlToDomainMapper extends XmlToDomainMapperTemplate<StandardFeed, JAXBElement> {
    private static final String JAXB_GENERATED_ATOM_PACKAGE = "de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom";
    @Inject
    private JaxbPojoToFeederPojoMapper<StandardFeed, JAXBElement> jaxbAtomToFeederApiAtom;


    @Override
    protected String getGeneratedJaxbClassesPackage() {
        return JAXB_GENERATED_ATOM_PACKAGE;
    }

    @Override
    protected JaxbPojoToFeederPojoMapper<StandardFeed, JAXBElement> getJaxbPojoToFeederPojoMapper() {
        return jaxbAtomToFeederApiAtom;
    }
}
