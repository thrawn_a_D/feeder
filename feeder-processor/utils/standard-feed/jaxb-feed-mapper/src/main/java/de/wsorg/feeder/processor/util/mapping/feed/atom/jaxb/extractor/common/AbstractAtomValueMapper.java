package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper;

import javax.inject.Inject;
import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class AbstractAtomValueMapper implements CommonAtomValueMapper {
    protected abstract void performActualAction(final CommonFeedData mappedFeedResult,
                                                final JAXBElement currentJaxbElement);

    @Inject
    protected JaxbToFeedMappingHelper jaxbToFeedMappingHelper;

    protected abstract String getIdentifyingElementName();

    @Override
    public void mapValueToFeed(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        if(jaxbToFeedMappingHelper.isElement(currentJaxbElement, getIdentifyingElementName())){
            performActualAction(mappedFeedResult, currentJaxbElement);
        }
    }
}
