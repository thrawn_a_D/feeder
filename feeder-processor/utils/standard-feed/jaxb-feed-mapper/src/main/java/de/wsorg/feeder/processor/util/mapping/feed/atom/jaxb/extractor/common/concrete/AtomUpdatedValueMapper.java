package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.AbstractAtomValueMapper;

import javax.xml.bind.JAXBElement;
import java.util.Date;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomUpdatedValueMapper extends AbstractAtomValueMapper {

    private static final String UPDATED_TAG = "updated";

    @Override
    protected String getIdentifyingElementName() {
        return UPDATED_TAG;
    }

    @Override
    public void performActualAction(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        final Date date = jaxbToFeedMappingHelper.getDate(currentJaxbElement);
        if (date != null) {
            mappedFeedResult.setUpdated(date);
        }
    }
}
