package de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper;

import javax.inject.Named;
import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class JaxbLinkToFeederLinkMappingHelper {

    public Link getLink(final JAXBElement jaxbElement) {
        Link link = new Link();
        LinkType linkType = (LinkType) jaxbElement.getValue();

        setHref(link, linkType);
        setRelationType(link, linkType);
        setLinkType(link, linkType);

        return link;
    }

    private void setRelationType(final Link link, final LinkType linkType) {
        LinkRelation relation = LinkRelation.valueOfCode(linkType.getRel());
        link.setRelation(relation);
    }

    private void setHref(final Link link, final LinkType linkType) {
        String href = linkType.getHref() == null ? StringUtils.EMPTY : linkType.getHref();
        link.setHref(href);
    }

    private void setLinkType(final Link link, final LinkType linkType) {
        String type = linkType.getType() == null ? StringUtils.EMPTY : linkType.getType();
        de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType actualLinkType = new de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType(type);
        link.setLinkType(actualLinkType);
    }
}
