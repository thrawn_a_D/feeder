package de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomDomainObjectBuilder implements DomainBuilderFactory<StandardFeed, StandardFeedEntry> {
    @Override
    public StandardFeed getAtomModelObject() {
        return new StandardFeed();
    }

    @Override
    public StandardFeedEntry getAtomEntryModelObject() {
        return new StandardFeedEntry();
    }
}
