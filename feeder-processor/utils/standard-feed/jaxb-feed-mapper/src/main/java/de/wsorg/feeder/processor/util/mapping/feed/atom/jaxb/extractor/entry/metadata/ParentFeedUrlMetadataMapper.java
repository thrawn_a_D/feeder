package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

import javax.xml.namespace.QName;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ParentFeedUrlMetadataMapper implements FeedEntryMetadataMapper {
    private static final String METADATA_PARENT_FEED_URL="PARENT_FEED_URL";

    @Override
    public void mapFeedEntryMetadata(final StandardFeedEntry newEntryFeeder, final QName qName, final String value) {
        if(qName.getLocalPart().equals(METADATA_PARENT_FEED_URL))
            newEntryFeeder.setFeedUrl(value);
    }
}
