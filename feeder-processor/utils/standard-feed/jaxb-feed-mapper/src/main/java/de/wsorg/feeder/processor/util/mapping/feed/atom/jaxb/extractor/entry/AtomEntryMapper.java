package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface AtomEntryMapper {
    public StandardFeedEntry processFeedEntry(final JAXBElement currentJaxbElement);
}
