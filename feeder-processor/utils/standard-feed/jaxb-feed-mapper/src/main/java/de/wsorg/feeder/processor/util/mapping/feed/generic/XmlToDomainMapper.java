package de.wsorg.feeder.processor.util.mapping.feed.generic;

/**
 *
 *
 *
 */
public interface XmlToDomainMapper<T> {
    T getResult(final String responseXml);
}
