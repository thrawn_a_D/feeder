package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes;

import javax.inject.Inject;
import javax.xml.bind.JAXBElement;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbLinkToFeederLinkMappingHelper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomLinkAttributeMapper implements AtomEntryAttributeMapper {
    private static final String LINK_TAG = "link";

    @Inject
    private JaxbLinkToFeederLinkMappingHelper jaxbLinkToFeederLinkMappingHelper;
    @Inject
    private JaxbToFeedMappingHelper jaxbToFeedMappingHelper;

    @Override
    public void mapAtomEntryValue(final StandardFeedEntry newEntryFeeder, final JAXBElement currentJaxbElement) {
        if (jaxbToFeedMappingHelper.isElement(currentJaxbElement, LINK_TAG)) {
            final Link link = jaxbLinkToFeederLinkMappingHelper.getLink(currentJaxbElement);
            newEntryFeeder.addLink(link);
        }
    }
}
