package de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper;

import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.DateTimeType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.IdType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LogoType;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.TextType;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class JaxbToFeedMappingHelper {

    public String getText(final JAXBElement jaxbElement) {
        TextType title = (TextType) jaxbElement.getValue();
        if (title.getContent().size() > 0) {
            return (String) title.getContent().get(0);
        } else {
            return StringUtils.EMPTY;
        }
    }

    public String getId(final JAXBElement jaxbElement) {
        IdType id = (IdType) jaxbElement.getValue();
        return id.getValue() == null ? StringUtils.EMPTY : id.getValue();
    }

    public Date getDate(final JAXBElement jaxbElement){
        DateTimeType updated = (DateTimeType) jaxbElement.getValue();
        if (updated.getValue() != null) {
            return updated.getValue().toGregorianCalendar().getTime();
        } else {
            return null;
        }
    }

    public String getLogo(final JAXBElement jaxbElement) {
        LogoType logo = (LogoType) jaxbElement.getValue();
        return logo.getBase() == null ? StringUtils.EMPTY : logo.getBase();
    }

    public boolean isElement(final JAXBElement currentJaxbElement, final String name) {
        return currentJaxbElement.getName().getLocalPart().equals(name);
    }
}
