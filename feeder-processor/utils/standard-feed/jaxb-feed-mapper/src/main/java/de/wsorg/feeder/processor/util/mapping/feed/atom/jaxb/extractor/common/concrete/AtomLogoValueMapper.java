package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete;

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.AbstractAtomValueMapper;

import javax.xml.bind.JAXBElement;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class AtomLogoValueMapper extends AbstractAtomValueMapper {

    private static final String LOGO_TAG = "logo";

    @Override
    protected String getIdentifyingElementName() {
        return LOGO_TAG;
    }

    @Override
    public void performActualAction(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
        mappedFeedResult.setLogoUrl(jaxbToFeedMappingHelper.getLogo(currentJaxbElement));
    }
}