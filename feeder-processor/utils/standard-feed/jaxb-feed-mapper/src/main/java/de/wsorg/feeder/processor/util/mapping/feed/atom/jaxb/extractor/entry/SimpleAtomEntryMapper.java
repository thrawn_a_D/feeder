package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.CollectiveCommonMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes.AtomEntryAttributeMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.EntryType;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class SimpleAtomEntryMapper implements AtomEntryMapper{
    @Inject
    private CollectiveCommonMapper collectiveCommonMapper;
    @Inject
    private JaxbToFeedMappingHelper jaxbToFeedMappingHelper;

    @Resource(name = "atomEntryAttributeMappers")
    private List<AtomEntryAttributeMapper> atomEntryAttributeMappers;

    @Resource(name = "feedEntryMetadataMappers")
    private List<FeedEntryMetadataMapper> feedEntryMetadataMappers;

    @Inject
    private ApplicationContext applicationContext;
    private DomainBuilderFactory domainBuilderFactory;

    @Override
    public StandardFeedEntry processFeedEntry(final JAXBElement currentJaxbElement) {
        StandardFeedEntry newEntryFeeder = getDomainBuilderFactory().getAtomEntryModelObject();

        EntryType feedEntry = (EntryType) currentJaxbElement.getValue();

        for(Object feedEntryElement : feedEntry.getAuthorOrCategoryOrContent()) {
            JAXBElement currentEntryElement = (JAXBElement) feedEntryElement;

            collectiveCommonMapper.mapElementToCommonAtomItem(newEntryFeeder, currentEntryElement);

            mapEntryAttributes(newEntryFeeder, currentEntryElement);
        }

        mapFeedEntryMetadata(newEntryFeeder, feedEntry);

        return newEntryFeeder;
    }

    private void mapFeedEntryMetadata(final StandardFeedEntry newEntryFeeder, final EntryType feedEntry) {
        if (feedEntry.getOtherAttributes() != null) {
            for (QName qSetName : feedEntry.getOtherAttributes().keySet()) {
                final String value = feedEntry.getOtherAttributes().get(qSetName);
                for (FeedEntryMetadataMapper feedEntryMetadataMapper : feedEntryMetadataMappers) {
                    feedEntryMetadataMapper.mapFeedEntryMetadata(newEntryFeeder, qSetName, value);
                }
            }
        }
    }

    private void mapEntryAttributes(final StandardFeedEntry newEntryFeeder, final JAXBElement currentEntryElement) {
        for (AtomEntryAttributeMapper atomEntryAttributeMapper : atomEntryAttributeMappers) {
            atomEntryAttributeMapper.mapAtomEntryValue(newEntryFeeder, currentEntryElement);
        }
    }

    private DomainBuilderFactory<StandardFeed, StandardFeedEntry> getDomainBuilderFactory(){
        if (this.domainBuilderFactory == null) {
            this.domainBuilderFactory = applicationContext.getBean("atomDomainObjectBuilder", DomainBuilderFactory.class);
        }

        return domainBuilderFactory;
    }
}
