package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class AbstractAtomValueMapperTest extends Specification {

    def IDENTIFYING_STRING = 'bla'
    def actualActionDone = false
    def usedCommonFeed=null
    def usedJaxbElement=null

    TestClass abstractAtomValueMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        abstractAtomValueMapper = new TestClass()
        abstractAtomValueMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
        actualActionDone = false
        usedCommonFeed=null
        usedJaxbElement=null
    }

    def "Call the actual method when the jaxb element matches the right identifying string"() {
        given:
        def jaxbElement = new JAXBElement(new QName(IDENTIFYING_STRING), String, '')

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, IDENTIFYING_STRING) >> true

        when:
        abstractAtomValueMapper.mapValueToFeed(null, jaxbElement)

        then:
        actualActionDone == true
    }

    def "Provided elements truly were used to call the actual methods"() {
        given:
        def jaxbElement = new JAXBElement(new QName(IDENTIFYING_STRING), String, '')

        and:
        def commonElement = new StandardFeed()

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, IDENTIFYING_STRING) >> true

        when:
        abstractAtomValueMapper.mapValueToFeed(commonElement, jaxbElement)

        then:
        usedCommonFeed == commonElement
        usedJaxbElement == jaxbElement
    }

    class TestClass extends AbstractAtomValueMapper {

        @Override
        protected void performActualAction(final CommonFeedData mappedFeedResult, final JAXBElement currentJaxbElement) {
            actualActionDone = true
            usedCommonFeed = mappedFeedResult
            usedJaxbElement = currentJaxbElement
        }

        @Override
        protected String getIdentifyingElementName() {
            return IDENTIFYING_STRING;
        }
    }
}
