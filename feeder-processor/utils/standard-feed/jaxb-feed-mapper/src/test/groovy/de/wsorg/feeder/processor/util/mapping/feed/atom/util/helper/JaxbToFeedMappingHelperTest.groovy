package de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper

import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.DateTimeType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.IdType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LogoType
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.TextType
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class JaxbToFeedMappingHelperTest extends Specification {
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        jaxbToFeedMappingHelper = new JaxbToFeedMappingHelper()
    }

    def "Get text out of a jaxb element"() {
        given:
        def textValue = 'asd'
        TextType text = new TextType(content: [textValue])
        JAXBElement jaxbElement = new JAXBElement(new QName(''), TextType, text)

        when:
        String result = jaxbToFeedMappingHelper.getText(jaxbElement)

        then:
        result == textValue
    }

    def "Get empty value out of an element with no text"() {
        given:
        TextType text = new TextType()
        JAXBElement jaxbElement = new JAXBElement(new QName(''), TextType, text)

        when:
        String result = jaxbToFeedMappingHelper.getText(jaxbElement)

        then:
        result == ''
    }

    def "Get an id value out of JaxbElement"() {
        given:
        def idValue = 'asd'
        IdType id = new IdType(value: idValue)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), IdType, id)

        when:
        String result = jaxbToFeedMappingHelper.getId(jaxbElement)

        then:
        result == idValue
    }

    def "Nothing is set as Id, provide emtpy field"() {
        given:
        IdType id = new IdType()
        JAXBElement jaxbElement = new JAXBElement(new QName(''), IdType, id)

        when:
        String result = jaxbToFeedMappingHelper.getId(jaxbElement)

        then:
        result == ''
    }

    def "Get Date value out of jaxbElement"() {
        given:
        def calendar= new XMLGregorianCalendarImpl("2012-08-07T20:40:12")
        DateTimeType date = new DateTimeType(value: calendar)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), DateTimeType, date)

        when:
        Date result = jaxbToFeedMappingHelper.getDate(jaxbElement)

        then:
        result.format("yyyy/MM/dd HH:mm:ss").toString().contains("2012/08/07 20:40:12")
    }

    def "Date value is null"() {
        given:
        DateTimeType date = new DateTimeType(value: null)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), DateTimeType, date)

        when:
        Date result = jaxbToFeedMappingHelper.getDate(jaxbElement)

        then:
        result == null
    }


    def "Get logo value out of a JaxbElement"() {
        given:
        def logoValue = 'asd'
        LogoType logoType = new LogoType(base: logoValue)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LogoType, logoType)

        when:
        String result = jaxbToFeedMappingHelper.getLogo(jaxbElement)

        then:
        result == logoValue
    }

    def "Empty logo element is provided, return empty string"() {
        given:
        LogoType logoType = new LogoType()
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LogoType, logoType)

        when:
        String result = jaxbToFeedMappingHelper.getLogo(jaxbElement)

        then:
        result == ''
    }

    def "Check if jaxb element can be identified"() {
        given:
        def identifier = 'identifier'
        JAXBElement jaxbElement = new JAXBElement(new QName(identifier), String, 'asd')

        when:
        def result = jaxbToFeedMappingHelper.isElement(jaxbElement, identifier)

        then:
        result == true
    }

    def "Jaxb element can not be identified"() {
        given:
        def identifier = 'identifier'
        JAXBElement jaxbElement = new JAXBElement(new QName('asaa'), String, 'asd')

        when:
        def result = jaxbToFeedMappingHelper.isElement(jaxbElement, identifier)

        then:
        !result
    }
}
