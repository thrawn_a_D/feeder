package de.wsorg.feeder.processor.util.mapping.feed.generic;


import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.bind.JAXBException
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 10.08.12
 */
public class ResponseToResultMapperTemplateTest extends Specification {

    private static String DEFAULT_JAXB_PACKAGE_NAME = "de.wsorg.jaxb.bla"
    private static JaxbPojoToFeederPojoMapper jaxbPojoToFeederPojoMapper

    XmlToDomainMapperTemplate responseToResultMapperTemplate
    JaxbUnmarshaller unmarshaller

    def setup(){
        responseToResultMapperTemplate = new ConcreteMapper()
        unmarshaller = Mock(JaxbUnmarshaller)
        jaxbPojoToFeederPojoMapper = Mock(JaxbPojoToFeederPojoMapper)

        responseToResultMapperTemplate.jaxbUnmarshaller = unmarshaller
    }

    def "Process atom xml and get a result domain"() {
        given:
        def atomToProcess = getSampleAtomXml()

        and:
        JAXBElement expectedUnmarshalledResult = new JAXBElement(new QName(""), String, "");
        StandardFeed expectedResult = new StandardFeed();

        when:
        def result = responseToResultMapperTemplate.getResult(atomToProcess)

        then:
        result != null
        result == expectedResult
        1 * unmarshaller.unmarshal(atomToProcess) >> expectedUnmarshalledResult
        1 * jaxbPojoToFeederPojoMapper.mapJaxbPojoToFeederPojo(expectedUnmarshalledResult) >> expectedResult
    }

    def "Handle unmarshall exception"() {
        given:
        def atomToProcess = getSampleAtomXml()

        and:
        unmarshaller.unmarshal(atomToProcess) >> {throw new JAXBException("blub")};

        when:
        responseToResultMapperTemplate.getResult(atomToProcess)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided xml is not a valid jaxb object"
    }

    def "Check unmarshaller uses the provided package"() {
        given:
        def atomToProcess = getSampleAtomXml()

        when:
        responseToResultMapperTemplate.getResult(atomToProcess)

        then:
        1 * unmarshaller.setJaxbGeneratedClassesPackage(DEFAULT_JAXB_PACKAGE_NAME)
    }

    private String getSampleAtomXml() {
        """<feed xmlns="http://www.w3.org/2005/Atom"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xml:lang="de">
    <title type="html">Golem.de</title>
    <id>http://www.golem.de/</id>
    <updated>2012-08-09T02:19:02+02:00</updated>
        <entry>
            <title type="html">Valve Software: Steam hat künftig auch Anwendungssoftware</title>
            <id>http://www.golem.de/news/valve-software-steam-hat-kuenftig-auch-anwendungssoftware-1208-93729-rss.html</id>
            <published>2012-08-08T20:40:00+02:00</published>
        </entry>
        <entry>
            <title type="html">Valve Software: Steam hat künftig auch Anwendungssoftware</title>
            <id>http://www.golem.de/news/valve-software-steam-hat-kuenftig-auch-anwendungssoftware-1208-93729-rss.html</id>
            <published>2012-08-08T20:40:00+02:00</published>
        </entry>
    </feed>"""
    }

    private static class ConcreteMapper extends XmlToDomainMapperTemplate {

        @Override
        protected String getGeneratedJaxbClassesPackage() {
            return DEFAULT_JAXB_PACKAGE_NAME;
        }

        @Override
        protected JaxbPojoToFeederPojoMapper getJaxbPojoToFeederPojoMapper() {
            return ResponseToResultMapperTemplateTest.jaxbPojoToFeederPojoMapper;
        }
    }
}
