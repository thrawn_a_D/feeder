package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 22.01.13
 */
class AtomDescriptionValueMapperTest extends Specification {
    AtomDescriptionValueMapper atomTitleValueExtractor
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        atomTitleValueExtractor = new AtomDescriptionValueMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomTitleValueExtractor.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
    }

    def "Extract title and set in in the provided common Object"() {
        given:
        def commonFeed = new StandardFeed()

        and:
        def expectedText = 'subtitle'
        and:
        def jaxbElement = Mock(JAXBElement)

        when:
        atomTitleValueExtractor.performActualAction(commonFeed, jaxbElement)

        then:
        commonFeed.description == expectedText
        1 * jaxbToFeedMappingHelper.getText(jaxbElement) >> expectedText
    }

    def "Make sure the right identifier is provided"() {
        when:
        def identifier = atomTitleValueExtractor.getIdentifyingElementName()

        then:
        identifier == "subtitle"
    }
}
