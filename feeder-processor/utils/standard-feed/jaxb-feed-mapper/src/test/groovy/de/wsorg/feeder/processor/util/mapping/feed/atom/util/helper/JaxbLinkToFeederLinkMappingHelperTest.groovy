package de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 03.05.13
 */
class JaxbLinkToFeederLinkMappingHelperTest extends Specification {
    JaxbLinkToFeederLinkMappingHelper jaxbLinkToFeederLinkMappingHelper

    def setup(){
        jaxbLinkToFeederLinkMappingHelper = new JaxbLinkToFeederLinkMappingHelper()
    }


    def "Get a link value out of JaxbElement"() {
        given:
        def linkValue = 'asd'
        LinkType link = new LinkType(href: linkValue)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result.href == linkValue
    }

    def "Nothing is set as link, provide emtpy field"() {
        given:
        LinkType link = new LinkType()
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result
        result.href == ''
    }

    def "Set link relation type if available"() {
        given:
        def linkValue = 'asd'
        def linkRelation = LinkRelation.ICON

        and:
        LinkType link = new LinkType(href: linkValue, rel: linkRelation.getLinkRfcCode())
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result.relation == linkRelation
    }

    def "Nothing set as relation type, use default value"() {
        given:
        def linkRelation = LinkRelation.UNKNOWN

        and:
        LinkType link = new LinkType()
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result.relation == linkRelation
    }

    def "Set link type"() {
        given:
        def linkValue = 'asd'
        def linkType = 'image/jpeg'
        LinkType link = new LinkType(href: linkValue, type: linkType)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result.linkType.isImage()
    }

    def "No link type provided"() {
        given:
        def linkValue = 'asd'
        LinkType link = new LinkType(href: linkValue)
        JAXBElement jaxbElement = new JAXBElement(new QName(''), LinkType, link)

        when:
        Link result = jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement)

        then:
        result.linkType != null
    }
}
