package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class AtomIdValueMapperTest extends Specification {
    AtomIdValueMapper atomIdValueMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        atomIdValueMapper = new AtomIdValueMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomIdValueMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
    }

    def "Extract title and set in in the provided common Object"() {
        given:
        def commonFeed = new StandardFeed()

        and:
        def expectedId = 'id'
        and:
        def jaxbElement = Mock(JAXBElement)

        when:
        atomIdValueMapper.performActualAction(commonFeed, jaxbElement)

        then:
        commonFeed.id == expectedId
        1 * jaxbToFeedMappingHelper.getId(jaxbElement) >> expectedId
    }

    def "Make sure the right identifier is provided"() {
        when:
        def identifier = atomIdValueMapper.getIdentifyingElementName()

        then:
        identifier == "id"
    }
}
