package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.CollectiveCommonMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes.AtomEntryAttributeMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.EntryType
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class SimpleAtomEntryMapperTest extends Specification {
    SimpleAtomEntryMapper simpleAtomEntryMapper
    CollectiveCommonMapper collectiveCommonMapper
    List<AtomEntryAttributeMapper> atomEntryAttributeMappers
    List<FeedEntryMetadataMapper> feedEntryMetadataMappers
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper
    DomainBuilderFactory domainBuilderFactory;

    def setup(){
        simpleAtomEntryMapper = new SimpleAtomEntryMapper()
        collectiveCommonMapper = Mock(CollectiveCommonMapper)
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        domainBuilderFactory = Mock(DomainBuilderFactory)
        atomEntryAttributeMappers = [Mock(AtomEntryAttributeMapper)]
        feedEntryMetadataMappers = [Mock(FeedEntryMetadataMapper)]
        simpleAtomEntryMapper.collectiveCommonMapper = collectiveCommonMapper
        simpleAtomEntryMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
        simpleAtomEntryMapper.atomEntryAttributeMappers = atomEntryAttributeMappers
        simpleAtomEntryMapper.feedEntryMetadataMappers = feedEntryMetadataMappers
        simpleAtomEntryMapper.domainBuilderFactory = domainBuilderFactory
        domainBuilderFactory.getAtomEntryModelObject() >> new StandardFeedEntry()
    }

    def "Map an entry based on the provided JAXBElement"() {
        given:
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.name >> new QName("entry")
        EntryType entryType = Mock(EntryType)
        jaxbElement.value >> entryType
        def someElement = Mock(JAXBElement)
        entryType.authorOrCategoryOrContent >> [someElement]

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, jaxbElement.name.localPart) >> true

        when:
        simpleAtomEntryMapper.processFeedEntry(jaxbElement)

        then:
        entryType.authorOrCategoryOrContent.size() * collectiveCommonMapper.mapElementToCommonAtomItem({it instanceof StandardFeedEntry}, someElement)
        entryType.authorOrCategoryOrContent.size() * atomEntryAttributeMappers[0].mapAtomEntryValue({it instanceof StandardFeedEntry}, someElement)
    }

    def "Map feed entry metadata"() {
        given:
        def metadataMap = new HashMap<QName, String>()
        def qName = new QName('')
        def valueToMap = 'some value'

        and:
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.name >> new QName("entry")
        EntryType entryType = Mock(EntryType)
        jaxbElement.value >> entryType
        def someElement = Mock(JAXBElement)
        entryType.authorOrCategoryOrContent >> [someElement]
        entryType.otherAttributes >> metadataMap
        metadataMap.put(qName, valueToMap)

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, jaxbElement.name.localPart) >> true

        when:
        simpleAtomEntryMapper.processFeedEntry(jaxbElement)

        then:
        entryType.otherAttributes.size() * feedEntryMetadataMappers[0].mapFeedEntryMetadata({it instanceof StandardFeedEntry}, qName, valueToMap)
    }
}
