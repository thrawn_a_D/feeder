package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class AtomLogoValueMapperTest extends Specification {
    AtomLogoValueMapper atomLogoValueMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        atomLogoValueMapper = new AtomLogoValueMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomLogoValueMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
    }

    def "Extract title and set in in the provided common Object"() {
        given:
        def commonFeed = new StandardFeed()

        and:
        def expectedLogo = 'http://logoUrl'
        and:
        def jaxbElement = Mock(JAXBElement)

        when:
        atomLogoValueMapper.performActualAction(commonFeed, jaxbElement)

        then:
        commonFeed.logoUrl == expectedLogo
        1 * jaxbToFeedMappingHelper.getLogo(jaxbElement) >> expectedLogo
    }

    def "Make sure the right identifier is provided"() {
        when:
        def identifier = atomLogoValueMapper.getIdentifyingElementName()

        then:
        identifier == "logo"
    }
}
