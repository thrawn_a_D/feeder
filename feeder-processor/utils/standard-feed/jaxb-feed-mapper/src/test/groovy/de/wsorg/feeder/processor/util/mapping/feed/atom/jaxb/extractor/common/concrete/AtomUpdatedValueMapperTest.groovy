package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.concrete

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class AtomUpdatedValueMapperTest extends Specification {
    AtomUpdatedValueMapper atomUpdatedValueMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        atomUpdatedValueMapper = new AtomUpdatedValueMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomUpdatedValueMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
    }

    def "Extract title and set in in the provided common Object"() {
        given:
        def commonFeed = new StandardFeed()

        and:
        def expectedDate = new Date()
        and:
        def jaxbElement = Mock(JAXBElement)

        when:
        atomUpdatedValueMapper.performActualAction(commonFeed, jaxbElement)

        then:
        commonFeed.updated == expectedDate
        1 * jaxbToFeedMappingHelper.getDate(jaxbElement) >> expectedDate
    }

    def "Make sure the right identifier is provided"() {
        when:
        def identifier = atomUpdatedValueMapper.getIdentifyingElementName()

        then:
        identifier == "updated"
    }

    def "Provided date is null"() {
        given:
        def commonFeed = Mock(StandardFeed)

        and:
        def jaxbElement = Mock(JAXBElement)

        when:
        atomUpdatedValueMapper.performActualAction(commonFeed, jaxbElement)

        then:
        0 * commonFeed.setUpdated(_)
        1 * jaxbToFeedMappingHelper.getDate(jaxbElement) >> null
    }
}
