package de.wsorg.feeder.processor.util.mapping

import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata.FeedEntryMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.12.12
 */
class AtomFeederMapperBuilderTest extends Specification {

    AtomFeederMapperBuilder atomFeederMapperBuilder

    def setup(){
        atomFeederMapperBuilder = new AtomFeederMapperBuilder()
    }

    def "Build a mapper without any adjustments"() {
        when:
        def result = atomFeederMapperBuilder.build()

        then:
        result != null
        result.jaxbAtomToFeederApiAtom.feedMetadataMappers.size() == 0
    }

    def "Build a model using only metadata mappers"() {
        given:
        def metadataMappers = [Mock(FeedMetadataMapper)]

        when:
        def result = atomFeederMapperBuilder.feedMetadataMappers(metadataMappers).build()

        then:
        result != null
        result.jaxbAtomToFeederApiAtom.feedMetadataMappers.size() == 1
        result.jaxbAtomToFeederApiAtom.feedMetadataMappers[0] == metadataMappers[0]
    }

    def "Build a model using only entry metadata mappers"() {
        given:
        def metadataMappers = [Mock(FeedEntryMetadataMapper)]

        when:
        def result = atomFeederMapperBuilder.feedEntryMetadataMappers(metadataMappers).build()

        then:
        result != null
        result.jaxbAtomToFeederApiAtom.atomEntryMapper.feedEntryMetadataMappers[0] == metadataMappers[0]
        result.jaxbAtomToFeederApiAtom.atomEntryMapper.feedEntryMetadataMappers.size() == 1
    }

    def "Build a mapper using only the domain builder"() {
        given:
        def domainBuilder = Mock(DomainBuilderFactory)

        when:
        def result = atomFeederMapperBuilder.domainBuilderFactory(domainBuilder).build()

        then:
        result != null
        result.jaxbAtomToFeederApiAtom.domainBuilderFactory == domainBuilder
    }

    def "No domain builder is set use the default one"() {
        when:
        def result = atomFeederMapperBuilder.build()

        then:
        result != null
        result.jaxbAtomToFeederApiAtom.domainBuilderFactory != null
    }
}
