package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb;


import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common.CollectiveCommonMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.AtomEntryMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.feed.metadata.FeedMetadataMapper
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.FeedType
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.08.12
 */
public class JaxbAtomToFeederApiAtomTest extends Specification {

    JaxbAtomToFeederApiAtom jaxbAtomToFeederApiAtom

    CollectiveCommonMapper collectiveCommonMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper
    List<FeedMetadataMapper> feedMetadataMappers = new ArrayList<FeedMetadataMapper>()
    AtomEntryMapper atomEntryMapper
    DomainBuilderFactory domainBuilderFactory

    def setup(){
        jaxbAtomToFeederApiAtom = new JaxbAtomToFeederApiAtom()

        collectiveCommonMapper = Mock(CollectiveCommonMapper)
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomEntryMapper = Mock(AtomEntryMapper)
        domainBuilderFactory = Mock(DomainBuilderFactory)

        jaxbAtomToFeederApiAtom.collectiveCommonMapper = collectiveCommonMapper
        jaxbAtomToFeederApiAtom.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
        jaxbAtomToFeederApiAtom.feedMetadataMappers = feedMetadataMappers
        jaxbAtomToFeederApiAtom.atomEntryMapper = atomEntryMapper
        jaxbAtomToFeederApiAtom.domainBuilderFactory = domainBuilderFactory

        domainBuilderFactory.getAtomModelObject() >> new StandardFeed()
    }

    def "Process a JaxbElement to map to an Atom model"() {
        given:
        def feedTypeToMap = Mock(FeedType)
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.value >> feedTypeToMap

        and:
        feedTypeToMap.otherAttributes >> new HashMap<QName, String>()

        and:
        def feedTitleElement = Mock(JAXBElement)
        def feedEntryElement = Mock(JAXBElement)
        feedTypeToMap.authorOrCategoryOrContributor >> [feedTitleElement, feedEntryElement]

        and:
        def calculatedAtomEntry = Mock(StandardFeedEntry)

        when:
        def result = jaxbAtomToFeederApiAtom.mapJaxbPojoToFeederPojo(jaxbElement)

        then:
        result != null
        1 * domainBuilderFactory.getAtomModelObject() >> new StandardFeed()
        1 * collectiveCommonMapper.mapElementToCommonAtomItem({it instanceof StandardFeed}, feedTitleElement)
        1 * collectiveCommonMapper.mapElementToCommonAtomItem({it instanceof StandardFeed}, feedEntryElement)
        1 * jaxbToFeedMappingHelper.isElement(feedTitleElement, "entry") >> false
        1 * jaxbToFeedMappingHelper.isElement(feedEntryElement, "entry") >> true
        1 * atomEntryMapper.processFeedEntry(feedEntryElement) >> calculatedAtomEntry
    }

    def "Process metadata of the feed"() {
        given:
        def feedTypeToMap = Mock(FeedType)
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.value >> feedTypeToMap

        and:
        def metadataName = new QName('')
        def metadataValue = 'asdd'
        feedTypeToMap.otherAttributes >> new HashMap<QName, String>()
        feedTypeToMap.otherAttributes.put(metadataName, metadataValue)

        and:
        def metadataMapper1 = Mock(FeedMetadataMapper)
        def metadataMapper2 = Mock(FeedMetadataMapper)
        feedMetadataMappers << metadataMapper1
        feedMetadataMappers << metadataMapper2

        and:
        feedTypeToMap.authorOrCategoryOrContributor >> []

        when:
        jaxbAtomToFeederApiAtom.mapJaxbPojoToFeederPojo(jaxbElement)

        then:
        1 * metadataMapper1.mapFeedMetadata({it != null}, metadataName, metadataValue)
        1 * metadataMapper2.mapFeedMetadata({it != null}, metadataName, metadataValue)
    }

    def "Provide an element which is not of jaxb element type"() {
        given:
        def feedTypeToMap = Mock(FeedType)
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.value >> feedTypeToMap

        and:
        feedTypeToMap.otherAttributes >> new HashMap<QName, String>()

        and:
        def feedTitleElement = Mock(JAXBElement)
        def feedEntryElement = Mock(JAXBElement)
        def someUnknownEntry = 'asd'
        feedTypeToMap.authorOrCategoryOrContributor >> [feedTitleElement, someUnknownEntry, feedEntryElement]

        and:
        def calculatedAtomEntry = Mock(StandardFeedEntry)

        when:
        def result = jaxbAtomToFeederApiAtom.mapJaxbPojoToFeederPojo(jaxbElement)

        then:
        result != null
        1 * domainBuilderFactory.getAtomModelObject() >> new StandardFeed()
        1 * collectiveCommonMapper.mapElementToCommonAtomItem({it instanceof StandardFeed}, feedTitleElement)
        1 * collectiveCommonMapper.mapElementToCommonAtomItem({it instanceof StandardFeed}, feedEntryElement)
        1 * jaxbToFeedMappingHelper.isElement(feedTitleElement, "entry") >> false
        1 * jaxbToFeedMappingHelper.isElement(feedEntryElement, "entry") >> true
        1 * atomEntryMapper.processFeedEntry(feedEntryElement) >> calculatedAtomEntry
    }

    def "Get an error if a provided jaxbElement does not match the needed type"() {
        given:
        def jaxbElement = Mock(JAXBElement)


        when:
        jaxbAtomToFeederApiAtom.mapJaxbPojoToFeederPojo(jaxbElement)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'The provided JAXBElement does not contain a FeedType'
    }
}
