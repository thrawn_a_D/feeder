package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.metadata

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import spock.lang.Specification

import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 17.12.12
 */
class ParentFeedUrlMetadataMapperTest extends Specification {

    ParentFeedUrlMetadataMapper parentFeedUrlMetadataMapper

    def setup(){
        parentFeedUrlMetadataMapper = new ParentFeedUrlMetadataMapper()
    }

    def "Provide a description and expect the value to be mapped to the feed entry"() {
        given:
        def feedEntry = new StandardFeedEntry()
        def qname = new QName('PARENT_FEED_URL')
        def feedUrl = 'http://feedUrl'

        when:
        parentFeedUrlMetadataMapper.mapFeedEntryMetadata(feedEntry, qname, feedUrl)

        then:
        feedEntry.feedUrl == feedUrl
    }

    def "Entry does not match the identifier, do nothing"() {
        given:
        def feedEntry = new StandardFeedEntry()
        def qname = new QName('asd')
        def feedUrl = 'http://feedUrl'
        when:
        parentFeedUrlMetadataMapper.mapFeedEntryMetadata(feedEntry, qname, feedUrl)

        then:
        !feedEntry.feedUrl
    }
}
