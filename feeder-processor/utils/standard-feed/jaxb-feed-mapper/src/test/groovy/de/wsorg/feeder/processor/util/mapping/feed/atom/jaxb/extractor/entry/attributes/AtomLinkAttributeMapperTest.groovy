package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbLinkToFeederLinkMappingHelper
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.LinkType
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 03.05.12
 */
class AtomLinkAttributeMapperTest extends Specification {
    AtomLinkAttributeMapper atomLinkAttributeMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper
    JaxbLinkToFeederLinkMappingHelper jaxbLinkToFeederLinkMappingHelper

    def setup(){
        atomLinkAttributeMapper = new AtomLinkAttributeMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        jaxbLinkToFeederLinkMappingHelper = Mock(JaxbLinkToFeederLinkMappingHelper)
        atomLinkAttributeMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
        atomLinkAttributeMapper.jaxbLinkToFeederLinkMappingHelper = jaxbLinkToFeederLinkMappingHelper
    }

    def "Map description JaxbElement to an atom entry"() {
        given:
        def link = new Link()
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.value >> new LinkType(href: link)

        and:
        def entryToBeModified = new StandardFeedEntry()

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, 'link') >> true

        and:
        jaxbLinkToFeederLinkMappingHelper.getLink(jaxbElement) >> link

        when:
        atomLinkAttributeMapper.mapAtomEntryValue(entryToBeModified, jaxbElement)

        then:
        entryToBeModified.links[0] == link
    }

    def "Tag does not match to jaxbElement"() {
        given:
        def jaxbElement = Mock(JAXBElement)

        and:
        def entryToBeModified = new StandardFeedEntry()

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, 'das') >> false

        when:
        atomLinkAttributeMapper.mapAtomEntryValue(entryToBeModified, jaxbElement)

        then:
        0 * jaxbToFeedMappingHelper.getText(jaxbElement)
        entryToBeModified.links.size() == 0
    }
}
