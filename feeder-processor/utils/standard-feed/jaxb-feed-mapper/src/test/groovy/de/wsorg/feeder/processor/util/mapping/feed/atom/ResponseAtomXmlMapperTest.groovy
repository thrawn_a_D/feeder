package de.wsorg.feeder.processor.util.mapping.feed.atom;


import de.wsorg.feeder.processor.util.mapping.feed.generic.JaxbPojoToFeederPojoMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.08.12
 */
public class ResponseAtomXmlMapperTest extends Specification {

    AtomXmlToDomainMapper responseAtomXmlMapper
    JaxbPojoToFeederPojoMapper jaxbAtomToFeederApiAtom

    def setup(){
        responseAtomXmlMapper = new AtomXmlToDomainMapper()
        jaxbAtomToFeederApiAtom = Mock(JaxbPojoToFeederPojoMapper)
        responseAtomXmlMapper.jaxbAtomToFeederApiAtom = jaxbAtomToFeederApiAtom;
    }

    def "Check default package is provided correctly"() {
        when:
        def jaxbPackage = responseAtomXmlMapper.generatedJaxbClassesPackage

        then:
        jaxbPackage == "de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom"
    }

    def "Check the jaxb mapper is correct"() {
        when:
        def jaxbMapper = responseAtomXmlMapper.jaxbPojoToFeederPojoMapper

        then:
        jaxbMapper == jaxbAtomToFeederApiAtom
    }
}
