package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.common

import de.wsorg.feeder.processor.domain.standard.atom.CommonFeedData
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class CollectiveCommonMapperTest extends Specification {
    CollectiveCommonMapper collectiveCommonMapper

    def setup(){
        collectiveCommonMapper = new CollectiveCommonMapper()
    }

    def "Manipulate data using a list of manipulators"() {
        given:
        final CommonFeedData mappedFeedResult = new StandardFeed()
        final JAXBElement currentJaxbElement = Mock(JAXBElement)

        and:
        def mapper1 = Mock(CommonAtomValueMapper)
        def mapper2 = Mock(CommonAtomValueMapper)
        def listOfMappers = [mapper1, mapper2]

        collectiveCommonMapper.commonAtomValueMappers = listOfMappers

        when:
        collectiveCommonMapper.mapElementToCommonAtomItem(mappedFeedResult, currentJaxbElement)

        then:
        1 * mapper1.mapValueToFeed(mappedFeedResult, currentJaxbElement)
        1 * mapper2.mapValueToFeed(mappedFeedResult, currentJaxbElement)
    }
}
