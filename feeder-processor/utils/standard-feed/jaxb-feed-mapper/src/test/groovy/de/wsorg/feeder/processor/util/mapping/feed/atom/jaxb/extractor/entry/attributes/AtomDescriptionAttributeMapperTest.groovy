package de.wsorg.feeder.processor.util.mapping.feed.atom.jaxb.extractor.entry.attributes

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.helper.JaxbToFeedMappingHelper
import de.wsorg.feeder.processor.util.mapping.generated.domain.feed.atom.TextType
import spock.lang.Specification

import javax.xml.bind.JAXBElement

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.12.12
 */
class AtomDescriptionAttributeMapperTest extends Specification {
    AtomDescriptionAttributeMapper atomDescriptionAttributeMapper
    JaxbToFeedMappingHelper jaxbToFeedMappingHelper

    def setup(){
        atomDescriptionAttributeMapper = new AtomDescriptionAttributeMapper()
        jaxbToFeedMappingHelper = Mock(JaxbToFeedMappingHelper)
        atomDescriptionAttributeMapper.jaxbToFeedMappingHelper = jaxbToFeedMappingHelper
    }

    def "Map description JaxbElement to an atom entry"() {
        given:
        def description = 'desc'
        def jaxbElement = Mock(JAXBElement)
        jaxbElement.value >> new TextType(content: [description])

        and:
        def entryToBeModified = new StandardFeedEntry()

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, 'summary') >> true

        and:
        jaxbToFeedMappingHelper.getText(jaxbElement) >> description

        when:
        atomDescriptionAttributeMapper.mapAtomEntryValue(entryToBeModified, jaxbElement)

        then:
        entryToBeModified.description == description
    }

    def "Tag does not match to jaxbElement"() {
        given:
        def jaxbElement = Mock(JAXBElement)

        and:
        def entryToBeModified = new StandardFeedEntry()

        and:
        jaxbToFeedMappingHelper.isElement(jaxbElement, 'das') >> false

        when:
        atomDescriptionAttributeMapper.mapAtomEntryValue(entryToBeModified, jaxbElement)

        then:
        0 * jaxbToFeedMappingHelper.getText(jaxbElement)
        entryToBeModified.description == null
    }
}
