package de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.12.12
 */
class AtomDomainObjectBuilderTest extends Specification {
    def "Build a standard domain object"() {
        when:
        def model = new AtomDomainObjectBuilder().getAtomModelObject()

        then:
        model != null
        model instanceof StandardFeed
    }

    def "Build atom entry model"() {
        when:
        def entryModel = new AtomDomainObjectBuilder().getAtomEntryModelObject()

        then:
        entryModel != null
        entryModel instanceof StandardFeedEntry
    }
}
