package de.wsorg.feeder.processor.domain.standard.atom;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class StandardFeed<T extends StandardFeedEntry> extends CommonFeedData {
    protected List<T> feedEntries = new ArrayList<T>();

    public void addEntry(final T entryFeeder){
        feedEntries.add(entryFeeder);
    }
}
