package de.wsorg.feeder.processor.domain.standard.atom.attributes.link;

import java.io.Serializable;

import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class Link implements Serializable {
    private String href;
    private LinkRelation relation;
    private LinkType linkType;
}
