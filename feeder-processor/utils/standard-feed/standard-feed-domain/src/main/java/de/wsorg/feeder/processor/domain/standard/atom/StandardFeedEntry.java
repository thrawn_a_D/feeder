package de.wsorg.feeder.processor.domain.standard.atom;

import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class StandardFeedEntry extends CommonFeedData {
    protected List<Link> links = new ArrayList<>();

    public void addLink(final Link link) {
        links.add(link);
    }
}
