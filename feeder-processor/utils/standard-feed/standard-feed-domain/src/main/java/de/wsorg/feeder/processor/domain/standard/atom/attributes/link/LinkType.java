package de.wsorg.feeder.processor.domain.standard.atom.attributes.link;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;

import lombok.Data;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class LinkType implements Serializable {
    private String linkType;

    public LinkType() {
    }

    public LinkType(String linkType) {
        this.linkType = linkType;
    }

    public boolean isImage() {
        return StringUtils.contains(linkType, "image/");
    }

    public boolean isHtml() {
        return StringUtils.contains(linkType, "text/html");
    }

    public boolean isAudio() {
        return StringUtils.contains(linkType, "audio/");
    }

    public boolean isAtom() {
        return isApplication() && StringUtils.contains(linkType, "atom");
    }

    public boolean isApplication() {
        return StringUtils.contains(linkType, "application/");
    }
}
