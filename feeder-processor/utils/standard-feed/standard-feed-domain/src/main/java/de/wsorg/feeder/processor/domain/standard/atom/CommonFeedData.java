package de.wsorg.feeder.processor.domain.standard.atom;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@EqualsAndHashCode(callSuper = false)
@Data
public class CommonFeedData implements Serializable {
    protected String title;
    protected String id;
    protected Date updated;
    protected String logoUrl;

    protected String feedUrl;
    protected String description;


    /**
     * This is necessary as rss feeds do not appose to have an id.
     * So in some cases the id could be null and is further mode not reliable.
     */
    public String getId() {
        if (StringUtils.isBlank(id)) {
            return feedUrl;
        } else {
            return id;
        }
    }
}
