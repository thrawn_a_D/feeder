package de.wsorg.feeder.processor.domain.standard.atom.attributes.link;

import org.apache.commons.lang.StringUtils;

import lombok.Getter;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum LinkRelation {
    ABOUT("about"),
    ALTERNATE("alternate"),
    APPENDIX("appendix"),
    ARCHIVES("archives"),
    AUTHOR("author"),
    BOOKMARK("bookmark"),
    CANONICAL("canonical"),
    CHAPTER("chapter"),
    COLLECTION("collection"),
    CONTENTS("contents"),
    COPYRIGHT("copyright"),
    CREATE("create-form"),
    CURRENT("current"),
    DESCRIBEDBY("describedby"),
    DESCRIBES("describes"),
    DISCLOSURE("disclosure"),
    DUPLICATE("duplicate"),
    EDIT("edit"),
    EDIT_FROM("edit-form"),
    EDIT_MEDIA("edit-media"),
    ENCLOSURE("enclosure"),
    FIRST("first"),
    GLOSSARY("glossary"),
    HELP("help"),
    HOSTS("hosts"),
    HUB("hub"),
    ICON("icon"),
    INDEX("index"),
    ITEM("item"),
    LAST("last"),
    LATEST("latest-version"),
    LICENSE("license"),
    LRDD("lrdd"),
    MONITOR("monitor"),
    MONITOR_GROUP("monitor-group"),
    NEXT("next"),
    NEXT_ARCHIVE("next-archive"),
    NOFOLLOW("nofollow"),
    NOREFERRER("noreferrer"),
    PAYMENT("payment"),
    PREDECESSOR("predecessor-version"),
    PREFETCH("prefetch"),
    PREV("prev"),
    PREVIEW("preview"),
    PREVIOUS("previous"),
    PREV_ARCHIVE("prev-archive"),
    PRIVACY("privacy-policy"),
    PROFILE("profile"),
    RELATED("related"),
    REPLIES("replies"),
    SEARCH("search"),
    SECTION("section"),
    SELF("self"),
    SERVICE("service"),
    START("start"),
    STYLESHEET("stylesheet"),
    SUBSECTION("subsection"),
    SUCCESSOR("successor-version"),
    TAG("tag"),
    TERMS("terms-of-service"),
    TYPE("type"),
    UP("up"),
    VERSION("version-history"),
    VIA("via"),
    WORKING_COPY("working-copy"),
    WORKING_COPY_OF("working-copy-of"),
    UNKNOWN("unknown");

    @Getter
    private String linkRfcCode;

    private LinkRelation(final String linkRfcCode) {
        this.linkRfcCode = linkRfcCode;
    }

    public static LinkRelation valueOfCode(final String linkRelationCode) {
        for (LinkRelation linkRelation : LinkRelation.values()) {
            if(linkRelation.linkRfcCode.equals(StringUtils.lowerCase(linkRelationCode)))
                return linkRelation;
        }

        return UNKNOWN;
    }
}
