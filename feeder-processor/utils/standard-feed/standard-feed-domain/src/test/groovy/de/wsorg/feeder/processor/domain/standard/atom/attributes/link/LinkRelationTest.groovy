package de.wsorg.feeder.processor.domain.standard.atom.attributes.link

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.05.13
 */
class LinkRelationTest extends Specification {
    def "Get relation type out of a code"() {
        given:
        def code = 'last'

        when:
        def result = LinkRelation.valueOfCode(code)

        then:
        result == LinkRelation.LAST
    }

    def "Return default value when nothing is found"() {
        given:
        def code = 'sadasdasasd'

        when:
        def result = LinkRelation.valueOfCode(code)

        then:
        result == LinkRelation.UNKNOWN
    }

    def "Return default value when providing null"() {
        given:
        def code = 'sadasdasasd'

        when:
        def result = LinkRelation.valueOfCode(code)

        then:
        result == LinkRelation.UNKNOWN
    }

    def "Make check case insensetive"() {
        given:
        def code = 'LAST'

        when:
        def result = LinkRelation.valueOfCode(code)

        then:
        result == LinkRelation.LAST
    }

    def "code is null"() {
        when:
        def result = LinkRelation.valueOfCode(null)

        then:
        result == LinkRelation.UNKNOWN
    }
}
