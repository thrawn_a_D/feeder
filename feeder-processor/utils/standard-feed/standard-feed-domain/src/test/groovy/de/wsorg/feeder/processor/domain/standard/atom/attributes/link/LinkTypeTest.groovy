package de.wsorg.feeder.processor.domain.standard.atom.attributes.link

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 06.05.13
 */
class LinkTypeTest extends Specification {
    def "Is linktype image"() {
        given:
        def type = 'image/png'

        def linkTypeObject = new LinkType(linkType: type)

        when:
        def result = linkTypeObject.isImage()

        then:
        result == true
    }

    def "Is linktype a link"() {
        given:
        def type = 'text/html'

        def linkTypeObject = new LinkType(linkType: type)

        when:
        def result = linkTypeObject.isHtml()

        then:
        result == true
    }

    def "Is linktpe an atom"() {
        given:
        def type = 'application/atom+xml'

        def linkTypeObject = new LinkType(linkType: type)

        when:
        def result = linkTypeObject.isAtom()

        then:
        result == true
    }

    def "Is linktpe application"() {
        given:
        def type = 'application/atom+xml'

        def linkTypeObject = new LinkType(linkType: type)

        when:
        def result = linkTypeObject.isApplication()

        then:
        result == true
    }

    def "Is linktpe audio"() {
        given:
        def type = 'audio/mpeg'

        def linkTypeObject = new LinkType(linkType: type)

        when:
        def result = linkTypeObject.isAudio()

        then:
        result == true
    }
}
