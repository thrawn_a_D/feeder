package de.wsorg.feeder.processor.domain.standard.atom

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.04.12
 */
class CommonFeedDataTest extends Specification {
    def "Make sure to use feed url in case id is null"() {
        given:
        def feedUrl = 'asdasd'
        def feedData = new CommonFeedData(feedUrl: feedUrl)

        when:
        def id = feedData.getId()

        then:
        id == feedUrl
    }

    def "If id is set, provide this as id"() {
        given:
        def feedUrl = 'asdasd'
        def id = 'kjhkjh'
        def feedData = new CommonFeedData(feedUrl: feedUrl, id: id)

        when:
        def result = feedData.getId()

        then:
        result == id
    }
}
