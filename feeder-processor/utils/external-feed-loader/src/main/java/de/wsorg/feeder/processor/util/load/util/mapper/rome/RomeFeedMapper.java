package de.wsorg.feeder.processor.util.load.util.mapper.rome;

import javax.annotation.Resource;
import javax.inject.Named;
import java.util.List;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.util.mapper.rome.head.HeadDataMapper;
import de.wsorg.feeder.processor.util.load.util.mapper.rome.item.ItemDataMapper;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
@Slf4j
public class RomeFeedMapper {
    @Resource(name = "romeHeadDataMappers")
    private List<HeadDataMapper> headDataMappers;
    @Resource(name = "romeItemDataMappers")
    private List<ItemDataMapper> itemDataMappers;

    public FeedModel mapFeed(final SyndFeed feed) {
        FeedModel feedModel = new FeedModel();

        mapHeaderData(feed, feedModel);
        mapEntriesData(feed, feedModel);

        return feedModel;
    }

    private void mapHeaderData(final SyndFeed feed, final FeedModel feedModel) {
        for (HeadDataMapper headDataMapper : headDataMappers) {
            headDataMapper.mapHeaderData(feed, feedModel);
        }
    }

    private void mapEntriesData(final SyndFeed feed, final FeedModel feedModel) {
        if (feed.getEntries() != null) {
            for (Object syndEntry : feed.getEntries()) {
                if (syndEntry instanceof SyndEntry) {
                    mapEntryData(feedModel, (SyndEntry) syndEntry);
                } else {
                    log.error("Error while mapping feed entry. Seems like entry is not of type SyndFeed but of type: {}", syndEntry);
                }
            }
        }
    }

    private void mapEntryData(final FeedModel feedModel, final SyndEntry syndEntry) {
        FeedEntryModel mappedFeedEntry = new FeedEntryModel();
        for (ItemDataMapper itemDataMapper : itemDataMappers) {
            itemDataMapper.mapFeedEntryData((SyndEntry) syndEntry, mappedFeedEntry);
        }
        feedModel.addFeedEntry(mappedFeedEntry);
    }
}
