package de.wsorg.feeder.processor.util.load.external.rome;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.util.mapper.rome.RomeFeedMapper;
import de.wsorg.feeder.processor.util.load.util.wrapper.XmlReaderWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class RomeFeedLoader implements ExternalFeedLoader {
    private SyndFeedInput syndFeedInput;

    @Inject
    private XmlReaderWrapper xmlReaderWrapper;
    @Inject
    private RomeFeedMapper romeFeedMapper;

    @Override
    public FeedModel loadFeed(final String feedIdentificator) {
        try {
            Reader feedReader = xmlReaderWrapper.getReader(new URL(feedIdentificator));
            SyndFeed syndFeed = getSyndFeedInput().build(feedReader);
            FeedModel mappedFeed = romeFeedMapper.mapFeed(syndFeed);
            setFeedUrl(feedIdentificator, mappedFeed);
            return mappedFeed;
        } catch (IOException e) {
            final String message = "Feed " + feedIdentificator + " could not be loaded. Maybe an invalid url?";
            throw new RuntimeException(message);
        } catch (FeedException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private void setFeedUrl(final String feedIdentificator, final FeedModel mappedFeed) {
        mappedFeed.setFeedUrl(feedIdentificator);

        for (FeedEntryModel feedEntryModel : mappedFeed.getFeedEntryModels()) {
            feedEntryModel.setFeedUrl(feedIdentificator);
        }
    }

    private SyndFeedInput getSyndFeedInput() {
        if(syndFeedInput == null) {
            syndFeedInput = new SyndFeedInput();
        }

        return syndFeedInput;
    }
}
