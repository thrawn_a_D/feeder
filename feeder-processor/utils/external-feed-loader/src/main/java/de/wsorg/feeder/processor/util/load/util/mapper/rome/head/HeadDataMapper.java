package de.wsorg.feeder.processor.util.load.util.mapper.rome.head;

import com.sun.syndication.feed.synd.SyndFeed;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface HeadDataMapper {
    FeedModel mapHeaderData(final SyndFeed syndFeed, final FeedModel destinationModel);
}
