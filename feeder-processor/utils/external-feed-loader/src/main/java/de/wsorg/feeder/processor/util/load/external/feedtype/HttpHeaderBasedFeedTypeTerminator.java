package de.wsorg.feeder.processor.util.load.external.feedtype;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;
import de.wsorg.feeder.processor.util.load.util.wrapper.JavaUrlWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class HttpHeaderBasedFeedTypeTerminator implements FeedTypeTerminator {
    final Logger LOGGER = LoggerFactory.getLogger(HttpHeaderBasedFeedTypeTerminator.class);

    @Inject
    private JavaUrlWrapper urlWrapper;

    @Override
    public ExternalFeedType determineFeedType(final String feedUrl) {
        String contentType = determineHtmlContentType(feedUrl);
        return determineFeedTypeByHttpHeader(feedUrl, contentType);
    }

    private ExternalFeedType determineFeedTypeByHttpHeader(final String feedUrl, final String contentType) {
        LOGGER.debug("HTML content type of:{} IS: {}", feedUrl, contentType);
        final String loggingMessage = "Feed content type is set to : {}";

        if(contentType.toLowerCase().contains("rss")){
            LOGGER.debug(loggingMessage, ExternalFeedType.RSS);
            return ExternalFeedType.RSS;
        } else if (contentType.toLowerCase().contains("atom")) {
            LOGGER.debug(loggingMessage, ExternalFeedType.ATOM);
            return ExternalFeedType.ATOM;
        } else {
            LOGGER.debug(loggingMessage, ExternalFeedType.UNKNOWN);
            return ExternalFeedType.UNKNOWN;
        }
    }

    private String determineHtmlContentType(final String feedUrl) {
        HttpURLConnection connection = null;
        connection = getUrlConnection(feedUrl);
        connection = establishConnection(feedUrl, connection);
        return connection.getContentType();
    }

    private HttpURLConnection establishConnection(final String feedUrl, final HttpURLConnection connection) {
        try {
            connection.connect();
        } catch (IOException e) {
            LOGGER.error("The provided url is invalid and can not be parsed : {}", feedUrl);
            throw new IllegalStateException("Could not establish a connection for ("+feedUrl+").");
        }

        return connection;
    }

    private HttpURLConnection getUrlConnection(final String feedUrl) {
        final HttpURLConnection connection;
        try {
            connection = urlWrapper.getUrlConnection(feedUrl);

            connection.setRequestMethod("HEAD");
        } catch (IOException e) {
            LOGGER.debug("The provided url is invalid and can not be parsed : {}", feedUrl);
            throw new UseCaseProcessException("The provided url is not valid ("+feedUrl+").");
        }
        return connection;
    }
}
