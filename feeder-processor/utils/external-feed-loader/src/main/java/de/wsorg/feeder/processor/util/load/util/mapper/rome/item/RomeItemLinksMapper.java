package de.wsorg.feeder.processor.util.load.util.mapper.rome.item;

import org.apache.commons.lang.StringUtils;

import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndLink;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class RomeItemLinksMapper implements ItemDataMapper {
    @Override
    public FeedEntryModel mapFeedEntryData(final SyndEntry entry, final FeedEntryModel destinationModel) {

        mapLinks(entry, destinationModel);
        mapEnclosures(entry, destinationModel);

        return destinationModel;
    }

    private void mapEnclosures(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getEnclosures() != null) {
            for (Object enclosure : entry.getEnclosures()) {
                mapSingleEnclosure(destinationModel, enclosure);
            }
        }
    }

    private void mapSingleEnclosure(final FeedEntryModel destinationModel, final Object enclosure) {
        if (enclosure instanceof SyndEnclosure) {
            final SyndEnclosure syndEnclosure = (SyndEnclosure) enclosure;
            final Link enclosureLink = new Link();

            enclosureLink.setLinkType(new LinkType(syndEnclosure.getType()));
            enclosureLink.setRelation(LinkRelation.ENCLOSURE);
            enclosureLink.setHref(syndEnclosure.getUrl());

            destinationModel.addLink(enclosureLink);
        }
    }

    private void mapLinks(final SyndEntry entry, final FeedEntryModel destinationModel) {
        mapListOfLinks(entry, destinationModel);
        mapSingleLinkAttribute(entry, destinationModel);
    }

    private void mapListOfLinks(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getLinks() != null) {
            for (Object link : entry.getLinks()) {
                mapSyndLink(destinationModel, link);
            }
        }
    }

    private void mapSingleLinkAttribute(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if(StringUtils.isNotBlank(entry.getLink())) {
            final Link link = new Link();
            link.setHref(entry.getLink());
            link.setLinkType(new LinkType("text/html"));
            link.setRelation(LinkRelation.RELATED);

            destinationModel.addLink(link);
        }
    }

    private void mapSyndLink(final FeedEntryModel destinationModel, final Object link) {
        if (link instanceof SyndLink) {
            SyndLink syndLink = (SyndLink) link;
            final Link feederLink = new Link();

            mapAttributesIfNotEmpty(destinationModel, syndLink, feederLink);
        }
    }

    private void mapAttributesIfNotEmpty(final FeedEntryModel destinationModel, final SyndLink syndLink, final Link feederLink) {
        if (StringUtils.isNotBlank(syndLink.getHref()) ||
            StringUtils.isNotBlank(syndLink.getRel()) ||
            StringUtils.isNotBlank(syndLink.getType()) ) {
            feederLink.setHref(syndLink.getHref());
            feederLink.setLinkType(new LinkType(syndLink.getType()));
            feederLink.setRelation(LinkRelation.valueOfCode(syndLink.getRel()));

            destinationModel.addLink(feederLink);
        }
    }
}
