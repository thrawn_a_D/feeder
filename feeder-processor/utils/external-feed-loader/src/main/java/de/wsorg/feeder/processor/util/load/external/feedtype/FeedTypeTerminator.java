package de.wsorg.feeder.processor.util.load.external.feedtype;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FeedTypeTerminator {
    ExternalFeedType determineFeedType(final String feedUrl);
}
