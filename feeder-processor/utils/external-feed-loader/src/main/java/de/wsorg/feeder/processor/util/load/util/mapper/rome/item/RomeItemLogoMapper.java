package de.wsorg.feeder.processor.util.load.util.mapper.rome.item;

import com.sun.syndication.feed.synd.SyndEnclosure;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndLink;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class RomeItemLogoMapper implements ItemDataMapper {
    @Override
    public FeedEntryModel mapFeedEntryData(final SyndEntry entry, final FeedEntryModel destinationModel) {
        boolean isLogoMapped = mapLogoUsingLinks(entry, destinationModel);

        if(!isLogoMapped) {
            mapLogoUsingEnclosures(entry, destinationModel);
        }

        return destinationModel;
    }

    private void mapLogoUsingEnclosures(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getEnclosures() != null) {
            for (Object enclosure : entry.getEnclosures()) {
                if (enclosure instanceof SyndEnclosure) {
                    mapIfEnclosureIsImage(destinationModel, (SyndEnclosure) enclosure);
                }
            }
        }
    }

    private void mapIfEnclosureIsImage(final FeedEntryModel destinationModel, final SyndEnclosure enclosure) {
        SyndEnclosure syndEnclosure = (SyndEnclosure) enclosure;
        if (isTypeImage(syndEnclosure.getType())) {
            destinationModel.setLogoUrl(syndEnclosure.getUrl());
        }
    }

    private boolean mapLogoUsingLinks(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getLinks() != null) {
            for (Object link : entry.getLinks()) {
                if (link instanceof SyndLink) {
                    SyndLink syndLink = (SyndLink) link;
                    mapIfLinkIsImage(destinationModel, syndLink);
                    return true;
                }
            }
        }
        return false;
    }

    private void mapIfLinkIsImage(final FeedEntryModel destinationModel, final SyndLink syndLink) {
        if (isTypeImage(syndLink.getType())) {
            destinationModel.setLogoUrl(syndLink.getHref());
        }
    }

    private boolean isTypeImage(final String type) {
        return new LinkType(type).isImage();
    }
}
