package de.wsorg.feeder.processor.util.load.util.mapper.rome.item;

import com.sun.syndication.feed.synd.SyndEntry;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class RomeItemUpdatedMapper implements ItemDataMapper {
    @Override
    public FeedEntryModel mapFeedEntryData(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getUpdatedDate() != null) {
            destinationModel.setUpdated(entry.getUpdatedDate());
        } else {
            destinationModel.setUpdated(entry.getPublishedDate());
        }
        return destinationModel;
    }
}
