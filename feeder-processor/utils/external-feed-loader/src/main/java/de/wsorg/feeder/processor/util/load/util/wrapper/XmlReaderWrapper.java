package de.wsorg.feeder.processor.util.load.util.wrapper;

import javax.inject.Named;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;

import com.sun.syndication.io.XmlReader;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class XmlReaderWrapper {
    public Reader getReader(final URL feedUrl) throws IOException {
        return new XmlReader(feedUrl);
    }
}
