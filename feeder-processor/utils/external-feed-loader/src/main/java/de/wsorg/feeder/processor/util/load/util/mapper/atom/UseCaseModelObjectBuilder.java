package de.wsorg.feeder.processor.util.load.util.mapper.atom;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.mapping.feed.atom.util.domain.DomainBuilderFactory;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UseCaseModelObjectBuilder implements DomainBuilderFactory<FeedModel, FeedEntryModel> {
    @Override
    public FeedModel getAtomModelObject() {
        return new FeedModel();
    }

    @Override
    public FeedEntryModel getAtomEntryModelObject() {
        return new FeedEntryModel();
    }
}
