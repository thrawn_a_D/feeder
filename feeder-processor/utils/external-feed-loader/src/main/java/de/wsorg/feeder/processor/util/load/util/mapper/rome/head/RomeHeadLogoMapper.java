package de.wsorg.feeder.processor.util.load.util.mapper.rome.head;

import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndLink;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import lombok.extern.slf4j.Slf4j;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class RomeHeadLogoMapper implements HeadDataMapper {
    @Override
    public FeedModel mapHeaderData(final SyndFeed syndFeed, final FeedModel destinationModel) {

        for (Object link : syndFeed.getLinks()) {
            if (link instanceof SyndLink) {
                SyndLink syndLink = (SyndLink) link;
                LinkType linkType = new LinkType(syndLink.getType());
                if(linkType.isImage()) {
                    destinationModel.setLogoUrl(syndLink.getHref());
                    break;
                }
            } else {
                log.error("Link of a synd feed does not seem to be of type SyndFeed but of {}", link);
            }
        }

        return destinationModel;
    }
}
