package de.wsorg.feeder.processor.util.load.external;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.external.atom.ExternalAtomFeedLoader;
import de.wsorg.feeder.processor.util.load.external.feedtype.ExternalFeedType;
import de.wsorg.feeder.processor.util.load.external.feedtype.FeedTypeTerminator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ExternalFeedLoaderSwitcher implements ExternalFeedLoader {
    final Logger LOGGER = LoggerFactory.getLogger(ExternalFeedLoaderSwitcher.class);

    @Inject
    private FeedTypeTerminator feedTypeTerminator;
    @Inject
    @Named("externalRssFeedLoader")
    private ExternalFeedLoader rssFeedLoader;
    @Inject
    @Named("externalAtomFeedLoader")
    private ExternalAtomFeedLoader atomFeedLoader;

    @Override
    public FeedModel loadFeed(final String feedIdentificator) {
        final ExternalFeedType feedType = feedTypeTerminator.determineFeedType(feedIdentificator);
        if(feedType == ExternalFeedType.RSS) {
            return useRssAsPrimaryLoader(feedIdentificator);
        }
        else if (feedType == ExternalFeedType.ATOM)
            return useAtomAsPrimaryLoader(feedIdentificator);
        else
            return useRssAsPrimaryLoader(feedIdentificator);
    }

    private FeedModel useAtomAsPrimaryLoader(final String feedIdentificator) {
        try {
            return atomFeedLoader.loadFeed(feedIdentificator);
        } catch (Exception e) {
            try {
                LOGGER.debug("Atom feed loading failed. Use rss feed loader as fallback");
                return rssFeedLoader.loadFeed(feedIdentificator);
            } catch (Exception e1) {
                throw new UseCaseProcessException("The url (" + feedIdentificator + ") seems not to be a valid feed.");
            }
        }
    }

    private FeedModel useRssAsPrimaryLoader(final String feedIdentificator) {
        try {
            return rssFeedLoader.loadFeed(feedIdentificator);
        } catch (Exception e) {
            try {
                LOGGER.debug("Rss feed loading failed. Use atom feed loader as fallback");
                return atomFeedLoader.loadFeed(feedIdentificator);
            } catch (Exception e1) {
                throw new UseCaseProcessException("The url (" + feedIdentificator + ") seems not to be a valid feed.");
            }
        }
    }
}
