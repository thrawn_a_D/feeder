package de.wsorg.feeder.processor.util.load;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ExternalFeedLoader {
    FeedModel loadFeed(final String feedIdentificator);
}
