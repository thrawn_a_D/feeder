package de.wsorg.feeder.processor.util.load.external.rss;

import javax.inject.Inject;
import javax.inject.Named;
import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss;
import de.wsorg.feeder.processor.util.load.util.mapper.rss.RssToFeedMapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ExternalRssFeedLoader implements ExternalFeedLoader {
    final Logger LOGGER = LoggerFactory.getLogger(ExternalRssFeedLoader.class);

    @Inject
    private RestTemplate restTemplate;

    @Inject
    private RssToFeedMapper rssToFeedMapper;

    @Override
    public FeedModel loadFeed(String feedUrl) {
        LOGGER.debug("Start loading of external rss {}", feedUrl);
        Rss rawRssResponse = getRawRssResponse(feedUrl);
        return mapRssResponseToFeedDomainClass(feedUrl, rawRssResponse);
    }

    private FeedModel mapRssResponseToFeedDomainClass(final String feedUrl, final Rss rawRssResponse) {
        LOGGER.debug("Start mapping of external rss to the feeder model");

        final FeedModel result;
        result = rssToFeedMapper.getFeedByRss(rawRssResponse);
        result.setFeedUrl(feedUrl);

        for (StandardFeedEntry standardFeedEntry : result.getFeedEntries()) {
            standardFeedEntry.setFeedUrl(feedUrl);
        }

        return result;
    }

    private Rss getRawRssResponse(final String feedUrl) {
        URI feedUri = getUriFromString(feedUrl);
        return restTemplate.getForObject(feedUri, Rss.class);
    }

    private URI getUriFromString(String feedUrl) {
        try {
            URI feedUri;
            feedUri = new URI(feedUrl);
            return feedUri;
        } catch (URISyntaxException e) {
            LOGGER.debug("Error while loading feed with url {}. Exception is:", feedUrl, e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
