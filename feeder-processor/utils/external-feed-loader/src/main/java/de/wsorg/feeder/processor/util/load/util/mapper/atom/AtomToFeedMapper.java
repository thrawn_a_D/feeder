package de.wsorg.feeder.processor.util.load.util.mapper.atom;

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed;
import de.wsorg.feeder.processor.domain.standard.atom.StandardFeedEntry;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.mapping.AtomFeederMapperBuilder;
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class AtomToFeedMapper {

    private XmlToDomainMapper<StandardFeed> atomXmlToDomainMapper;

    @Inject
    private UseCaseModelObjectBuilder useCaseModelObjectBuilder;

    public FeedModel mapAtomToFeed(final String rawAtomXml, final String feedUrl) {
        final FeedModel resultAsAtom = (FeedModel) getXmlToDomainMapper().getResult(rawAtomXml);

        resultAsAtom.setFeedUrl(feedUrl);

        for (StandardFeedEntry standardAtomEntry : resultAsAtom.getFeedEntries()) {
            standardAtomEntry.setFeedUrl(feedUrl);
        }

        return resultAsAtom;
    }

    private XmlToDomainMapper getXmlToDomainMapper(){
        if(this.atomXmlToDomainMapper == null) {
            AtomFeederMapperBuilder atomFeederMapperBuilder = new AtomFeederMapperBuilder();
            atomFeederMapperBuilder.domainBuilderFactory(useCaseModelObjectBuilder);
            this.atomXmlToDomainMapper = atomFeederMapperBuilder.build();
        }

        return this.atomXmlToDomainMapper;
    }
}
