package de.wsorg.feeder.processor.util.load;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class ExternalFeedLoaderFactory {

    private static ApplicationContext applicationContext;

    public static ExternalFeedLoader getFeedLoader() {
        getApplicationContext();

        return applicationContext.getBean("romeFeedLoader", ExternalFeedLoader.class);
    }

    private static void getApplicationContext() {
        if (applicationContext == null) {
            final String[] configLocations = {"classpath:spring/feed-utils-loader-http-context.xml"};
            applicationContext = new ClassPathXmlApplicationContext(configLocations);
        }
    }
}
