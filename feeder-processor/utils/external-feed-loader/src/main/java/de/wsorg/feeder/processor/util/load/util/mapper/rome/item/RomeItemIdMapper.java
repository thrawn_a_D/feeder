package de.wsorg.feeder.processor.util.load.util.mapper.rome.item;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndLink;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.utils.wrapper.DigestUtilsWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class RomeItemIdMapper implements ItemDataMapper {
    private DigestUtilsWrapper digestUtilsWrapper = new DigestUtilsWrapper();

    @Override
    public FeedEntryModel mapFeedEntryData(final SyndEntry entry, final FeedEntryModel destinationModel) {
        boolean uuidMappedUsingLinks = mapIdUsingLinks(entry, destinationModel);

        if(!uuidMappedUsingLinks)
            setRandomUUIdAsId(entry, destinationModel);

        return destinationModel;
    }

    private void setRandomUUIdAsId(final SyndEntry entry, final FeedEntryModel destinationModel) {
        destinationModel.setId(digestUtilsWrapper.md5Hex(entry.getTitle()));
    }

    private boolean mapIdUsingLinks(final SyndEntry entry, final FeedEntryModel destinationModel) {
        if (entry.getLinks() != null) {
            for (Object link : entry.getLinks()) {
                if (link instanceof SyndLink) {
                    boolean success = mapSyndLink(destinationModel, (SyndLink) link);
                    if(success) return success;
                }
            }
        }
        return false;
    }

    private boolean mapSyndLink(final FeedEntryModel destinationModel, final SyndLink link) {
        SyndLink syndLink = (SyndLink) link;
        if (new LinkType(syndLink.getType()).isHtml()) {
            final String hrefAsMd5 = digestUtilsWrapper.md5Hex(syndLink.getHref());
            destinationModel.setId(hrefAsMd5);
            return true;
        }
        return false;
    }
}
