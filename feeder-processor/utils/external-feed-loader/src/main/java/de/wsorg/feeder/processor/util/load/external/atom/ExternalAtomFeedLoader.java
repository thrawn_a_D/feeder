package de.wsorg.feeder.processor.util.load.external.atom;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader;
import de.wsorg.feeder.processor.util.load.util.mapper.atom.AtomToFeedMapper;
import de.wsorg.feeder.processor.util.load.util.wrapper.JavaUrlWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class ExternalAtomFeedLoader implements ExternalFeedLoader {
    final Logger LOGGER = LoggerFactory.getLogger(ExternalAtomFeedLoader.class);

    @Inject
    private JavaUrlWrapper javaUrlWrapper;

    @Inject
    private AtomToFeedMapper atomToFeedMapper;

    @Override
    public FeedModel loadFeed(final String feedIdentificator) {
        LOGGER.debug("Loading atom feed:{}", feedIdentificator);
        HttpURLConnection connection = getURLConnection(feedIdentificator);

        String encoding = connection.getContentEncoding();
        encoding = encoding == null ? "UTF-8" : encoding;
        InputStream responseBodyStream = getResponseBodyStream(feedIdentificator, connection);
        String responseBodyAsString = getResponseAsString(feedIdentificator, encoding, responseBodyStream);
        return atomToFeedMapper.mapAtomToFeed(responseBodyAsString, feedIdentificator);
    }

    private String getResponseAsString(final String feedIdentificator, final String encoding, final InputStream responseBodyStream) {
        String responseBodyAsString="";
        try {
            responseBodyAsString = IOUtils.toString(responseBodyStream, encoding);
        } catch (IOException e) {
            LOGGER.debug("Error reading atom response from {} : {}", feedIdentificator, e.getMessage());
            throw new RuntimeException("Error while reading response from :" + feedIdentificator);
        }
        return responseBodyAsString;
    }

    private InputStream getResponseBodyStream(final String feedIdentificator, final HttpURLConnection connection) {
        InputStream responseBodyStream=null;
        try {
            responseBodyStream = connection.getInputStream();
        } catch (IOException e) {
            LOGGER.debug("Error reading atom response from {} : {}", feedIdentificator, e.getMessage());
            throw new RuntimeException("Error while reading the response from : " + feedIdentificator);
        }
        return responseBodyStream;
    }

    private HttpURLConnection getURLConnection(final String feedIdentificator) {
        HttpURLConnection connection;
        try {
            connection = javaUrlWrapper.getUrlConnection(feedIdentificator);
        } catch (IOException e) {
            String message = "Error while creating url connection for :" + feedIdentificator;
            LOGGER.debug(message);
            throw new IllegalArgumentException(message);
        }
        return connection;
    }

}
