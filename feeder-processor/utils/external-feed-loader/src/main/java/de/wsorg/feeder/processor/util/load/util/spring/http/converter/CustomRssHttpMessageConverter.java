package de.wsorg.feeder.processor.util.load.util.spring.http.converter;

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss;
import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import javax.xml.bind.JAXBException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *
 */
public class CustomRssHttpMessageConverter extends org.springframework.http.converter.AbstractHttpMessageConverter<Rss> {

    private static final String JAXB_GENERATED_CLASSES_PACKAGE = "de.wsorg.feeder.processor.usecases.domain.generated.rss";
    JaxbUnmarshaller jaxbUnmarshaller;

    public CustomRssHttpMessageConverter() {
        setSupportedMediaTypes();
    }

    private void setSupportedMediaTypes() {
        List<MediaType> mediaTypes = new ArrayList<>();
        mediaTypes.add(MediaType.ALL);
        setSupportedMediaTypes(mediaTypes);
    }

    @Override
    protected boolean supports(final Class<?> aClass) {
        return aClass == Rss.class;
    }

    @Override
    protected Rss readInternal(final Class<? extends Rss> aClass, final HttpInputMessage httpInputMessage) throws IOException, HttpMessageNotReadableException {
        Rss result = new Rss();
        StringBuilder rssAsStringContent = getRssAsString(httpInputMessage);

        result = parseRssContent(rssAsStringContent);

        return result;
    }

    private Rss parseRssContent(final StringBuilder rssAsStringContent) {
        final Rss result;
        try {
            result = (Rss) getJaxbUnmarshaller().unmarshal(rssAsStringContent.toString());
        } catch (JAXBException e) {
            throw new UseCaseProcessException("The http response does not provide a valid content which could be parsed by jaxb");
        }
        return result;
    }

    private StringBuilder getRssAsString(final HttpInputMessage httpInputMessage) throws IOException {
        validateMessageBody(httpInputMessage);

        BufferedReader streamReader = new BufferedReader(new InputStreamReader(httpInputMessage.getBody()));

        StringBuilder rssAsStringContent = new StringBuilder();

        String line;
        while ((line = streamReader.readLine()) != null) {
            rssAsStringContent.append(line);
        }
        return rssAsStringContent;
    }

    private void validateMessageBody(final HttpInputMessage httpInputMessage) throws IOException {
        if(httpInputMessage.getBody() == null)
            throw new UseCaseProcessException("The rss response does not a valid body");
    }

    @Override
    protected void writeInternal(final Rss rss, final HttpOutputMessage httpOutputMessage) throws IOException, HttpMessageNotWritableException {
        throw new NotImplementedException();
    }

    private JaxbUnmarshaller getJaxbUnmarshaller(){
        if(jaxbUnmarshaller == null) {
            jaxbUnmarshaller = UtilsFactory.getJaxbUnmarshaller();
            jaxbUnmarshaller.setJaxbGeneratedClassesPackage(JAXB_GENERATED_CLASSES_PACKAGE);
        }

        return jaxbUnmarshaller;
    }
}
