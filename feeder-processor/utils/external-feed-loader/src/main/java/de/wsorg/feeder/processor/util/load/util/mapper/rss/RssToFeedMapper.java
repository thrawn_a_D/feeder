package de.wsorg.feeder.processor.util.load.util.mapper.rss;

import javax.inject.Named;
import javax.xml.bind.JAXBElement;

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation;
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel;
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Enclosure;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Guid;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Image;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss;
import de.wsorg.feeder.processor.util.load.domain.generated.rss.RssItem;
import de.wsorg.feeder.utils.date.DateTimeParser;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class RssToFeedMapper {

    public FeedModel getFeedByRss(Rss rssData) {
        FeedModel resultingFeedModel = new FeedModel();

        resultingFeedModel = getFeedHeader(rssData, resultingFeedModel);
        resultingFeedModel = getFeedEntries(rssData, resultingFeedModel);

        return resultingFeedModel;
    }

    private FeedModel getFeedEntries(final Rss rssData, final FeedModel resultingFeedModel) {
        if(rssData.getChannel().getItem() != null) {
            for (RssItem rssItem : rssData.getChannel().getItem()) {
                FeedEntryModel entryModel = new FeedEntryModel();

                entryModel = iterateThroughEntryFields(rssItem, entryModel);
                resultingFeedModel.addFeedEntry(entryModel);
            }
        }

        return resultingFeedModel;
    }

    private FeedEntryModel iterateThroughEntryFields(final RssItem rssItem, FeedEntryModel entryModel) {
        for (Object element : rssItem.getTitleOrDescriptionOrLink()) {
            if(element instanceof JAXBElement) {
                JAXBElement<?> jaxBElement = (JAXBElement<?>) element;
                entryModel = fillFeedEntryWithValues(entryModel, jaxBElement);
            }
        }
        return entryModel;
    }

    private FeedEntryModel fillFeedEntryWithValues(final FeedEntryModel entryModel, final JAXBElement<?> jaxBElement) {
        if(jaxBElement.getName().getLocalPart().equals("title")){
            entryModel.setTitle((String) jaxBElement.getValue());
        } else if(jaxBElement.getName().getLocalPart().equals("description")){
            entryModel.setDescription((String) jaxBElement.getValue());
        } else if(jaxBElement.getName().getLocalPart().equals("pubDate")){
            entryModel.setUpdated(DateTimeParser.parse((String) jaxBElement.getValue()));
        } else if(jaxBElement.getName().getLocalPart().equals("guid")){
            Guid guid = (Guid) jaxBElement.getValue();
            entryModel.setId(guid.getValue());
        } else if(jaxBElement.getName().getLocalPart().equals("enclosure")){
            Enclosure enclosure = (Enclosure) jaxBElement.getValue();
            Link linkType = new Link();
            linkType.setHref(enclosure.getUrl());
            linkType.setLinkType(new LinkType(enclosure.getType()));
            linkType.setRelation(LinkRelation.UNKNOWN);
            entryModel.addLink(linkType);
        } else if(jaxBElement.getName().getLocalPart().equals("link")){
            Link linkType = new Link();
            linkType.setHref((String) jaxBElement.getValue());
            linkType.setLinkType(new LinkType("text/html"));
            linkType.setRelation(LinkRelation.UNKNOWN);
            entryModel.addLink(linkType);
        }

        return entryModel;
    }

    private FeedModel getFeedHeader(final Rss rssData, FeedModel resultingFeedModel) {
        for (Object element : rssData.getChannel().getTitleOrLinkOrDescription()) {
            if (element instanceof JAXBElement) {
                JAXBElement<?> jaxBElement = (JAXBElement<?>) element;
                resultingFeedModel = fillFeedWithValues(resultingFeedModel, jaxBElement);
            }
        }
        return resultingFeedModel;
    }

    private FeedModel fillFeedWithValues(final FeedModel resultingFeedModel, final JAXBElement<?> jaxBElement) {
        if (jaxBElement.getName().getLocalPart().equals("title")) {
            resultingFeedModel.setTitle((String) jaxBElement.getValue());
        } else if (jaxBElement.getName().getLocalPart().equals("description")) {
            resultingFeedModel.setDescription((String) jaxBElement.getValue());
        } else if(jaxBElement.getName().getLocalPart().equals("pubDate")){
            resultingFeedModel.setUpdated(DateTimeParser.parse((String) jaxBElement.getValue()));
        } else if(jaxBElement.getName().getLocalPart().equals("image")){
            Image image = (Image) jaxBElement.getValue();
            resultingFeedModel.setLogoUrl(image.getUrl());
        }

        return resultingFeedModel;
    }
}
