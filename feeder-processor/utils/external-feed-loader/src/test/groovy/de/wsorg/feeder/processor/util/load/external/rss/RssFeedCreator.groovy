package de.wsorg.feeder.processor.util.load.external.rss

import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Enclosure
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Guid
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Image
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss
import de.wsorg.feeder.processor.util.load.domain.generated.rss.RssChannel
import de.wsorg.feeder.processor.util.load.domain.generated.rss.RssItem
import de.wsorg.feeder.utils.date.DateTimeParser
import spock.lang.Specification

import javax.xml.bind.JAXBElement
import javax.xml.namespace.QName

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.07.12
 */
class RssFeedCreator extends Specification {

    Rss getRssChannelWithoutEntries(final FeedModel expectedTestData) {
        Rss rawRss = new Rss();
        RssChannel rssChannel = Mock(RssChannel)
        JAXBElement title = getElementForTestData('title', expectedTestData.title)
        JAXBElement description = getElementForTestData('description', expectedTestData.description)
        JAXBElement publishedDate = getElementForTestData('pubDate', DateTimeParser.parse(expectedTestData.updated))
        JAXBElement imageUrl = getImageElementForTestData('image', expectedTestData.logoUrl)
        rssChannel.getTitleOrLinkOrDescription() >> [title, description, publishedDate, imageUrl]
        rawRss.setChannel(rssChannel)
        rawRss
    }

    JAXBElement getImageElementForTestData(String paramName, String imageUrl) {
        Image image = new Image();
        image.setUrl(imageUrl);
        return getElementForTestData(paramName, image);
    }

    Rss getRssChannelWithFeedEntries(final FeedModel testFeedWithFeedEntries) {
        Rss rawRssData = getRssChannelWithoutEntries(testFeedWithFeedEntries);

        ArrayList<RssItem> listOfRawRssItems = getRawRssEntries(testFeedWithFeedEntries)
        rawRssData.getChannel().getItem() >> listOfRawRssItems
        return rawRssData;
    }

    private ArrayList<RssItem> getRawRssEntries(FeedModel testFeedWithFeedEntries) {
        def listOfRawRssItems = new ArrayList<RssItem>();
        for (FeedEntryModel feedEntry : testFeedWithFeedEntries.feedEntryModels) {

            def elementList = []

            RssItem rssItem = Mock(RssItem);
            JAXBElement title = getElementForTestData('title', feedEntry.title)
            JAXBElement description = getElementForTestData('description', feedEntry.description)
            JAXBElement uid = setGuid(feedEntry)
            JAXBElement publishedDate = getElementForTestData('pubDate', DateTimeParser.parse(feedEntry.updated))

            elementList = [title, description, publishedDate, uid]

            for (Link link : feedEntry.getLinks()) {
                if(link.linkType.html) {
                    JAXBElement textLink = getElementForTestData('link', link.href)
                    elementList.add(textLink)
                } else if(link.linkType.audio || link.linkType.image) {
                    Enclosure enclosure = new Enclosure()
                    enclosure.setUrl(link.href)
                    enclosure.setType(link.linkType.linkType)
                    JAXBElement enclosureElement = getElementForTestData("enclosure", enclosure);
                    elementList.add(enclosureElement)
                }
            }

            rssItem.getTitleOrDescriptionOrLink() >> elementList
            listOfRawRssItems.add(rssItem);
        }
        listOfRawRssItems
    }

    private JAXBElement setGuid(FeedEntryModel feedEntry) {
        def guid = new Guid()
        guid.setValue(feedEntry.id)
        JAXBElement uid = getElementForTestData('guid', guid)
        uid
    }

    private JAXBElement getElementForTestData(String paramName, Object testTitleValue) {
        JAXBElement jaxBTitle = new JAXBElement(new QName(paramName), String, testTitleValue)
        jaxBTitle
    }
}
