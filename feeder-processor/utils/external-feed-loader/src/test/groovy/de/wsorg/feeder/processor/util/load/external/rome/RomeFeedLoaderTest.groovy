package de.wsorg.feeder.processor.util.load.external.rome

import com.sun.syndication.feed.synd.SyndFeed
import com.sun.syndication.io.FeedException
import com.sun.syndication.io.SyndFeedInput
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.util.mapper.rome.RomeFeedMapper
import de.wsorg.feeder.processor.util.load.util.wrapper.XmlReaderWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeFeedLoaderTest extends Specification {
    RomeFeedLoader romeFeedLoader
    SyndFeedInput syndFeedInput
    XmlReaderWrapper xmlReaderWrapper
    RomeFeedMapper romeFeedMapper

    def setup(){
        romeFeedLoader = new RomeFeedLoader()

        syndFeedInput = Mock(SyndFeedInput)
        xmlReaderWrapper = Mock(XmlReaderWrapper)
        romeFeedMapper = Mock(RomeFeedMapper)

        romeFeedLoader.syndFeedInput = syndFeedInput
        romeFeedLoader.xmlReaderWrapper = xmlReaderWrapper
        romeFeedLoader.romeFeedMapper = romeFeedMapper
    }

    def "Load a feed using rome"() {
        given:
        def feedUrl = 'http://sdfkjbd.de'

        and:
        def reader = Mock(Reader)

        and:
        def loadedSyndFeed = Mock(SyndFeed)

        and:
        def mappedFeed = new FeedModel()

        when:
        def result = romeFeedLoader.loadFeed(feedUrl)

        then:
        result
        result == mappedFeed
        result.feedUrl == feedUrl
        1 * xmlReaderWrapper.getReader({URL it -> it.toExternalForm() == feedUrl}) >> reader
        1 * syndFeedInput.build(reader) >> loadedSyndFeed
        1 * romeFeedMapper.mapFeed(loadedSyndFeed) >> mappedFeed
    }

    def "Provide invalid url and receive an exception"() {
        given:
        def feedUrl = 'ä#l.,ölk'

        when:
        romeFeedLoader.loadFeed(feedUrl)

        then:
        def ex = thrown(RuntimeException)
        ex.message == "Feed ${feedUrl} could not be loaded. Maybe an invalid url?"
    }

    def "Error while building feed"() {
        given:
        def feedUrl = 'http://sdfkjbd.de'

        and:
        def reader = Mock(Reader)

        and:
        xmlReaderWrapper.getReader({URL it -> it.toExternalForm() == feedUrl}) >> reader

        def errorMessage = "blub"
        syndFeedInput.build(reader) >> {throw new FeedException(errorMessage)}

        when:
        romeFeedLoader.loadFeed(feedUrl)

        then:
        def ex = thrown(RuntimeException)
        ex.message == errorMessage
    }

    def "Set feed url on entries and feed header"() {
        given:
        def feedUrl = 'http://sdfkjbd.de'

        and:
        def reader = Mock(Reader)

        and:
        def loadedSyndFeed = Mock(SyndFeed)

        and:
        def mappedFeed = new FeedModel()
        def entryModel = new FeedEntryModel()
        mappedFeed.addFeedEntry(entryModel)

        and:
        xmlReaderWrapper.getReader({URL it -> it.toExternalForm() == feedUrl}) >> reader
        syndFeedInput.build(reader) >> loadedSyndFeed
        romeFeedMapper.mapFeed(loadedSyndFeed) >> mappedFeed

        when:
        def result = romeFeedLoader.loadFeed(feedUrl)

        then:
        result
        result == mappedFeed
        result.feedUrl == feedUrl
        result.feedEntries[0].feedUrl == feedUrl
    }
}
