package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndLink
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.utils.wrapper.DigestUtilsWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemIdMapperTest extends Specification {
    RomeItemIdMapper romeItemIdMapper
    DigestUtilsWrapper digestUtilsWrapper

    def setup(){
        romeItemIdMapper = new RomeItemIdMapper()
        digestUtilsWrapper = Mock(DigestUtilsWrapper)
        romeItemIdMapper.digestUtilsWrapper = digestUtilsWrapper
    }

    def "Map id of an entry"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def link = Mock(SyndLink)
        link.getType() >> 'text/html'
        link.getHref() >> 'kjgkugkbkuh'
        link.getRel() >> LinkRelation.ABOUT.name()
        syndEntry.getLinks() >> [link]

        and:
        def hrefAsMd5 = 'asdaskjnasd'

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemIdMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        1 * digestUtilsWrapper.md5Hex(link.getHref()) >> hrefAsMd5
        result.getId() == hrefAsMd5
    }

    def "Make sure links are set"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemIdMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getId()
    }

    def "Make sure links are of type SyndLink"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        syndEntry.getLinks() >> ['asd']

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemIdMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getId()
    }

    def "Link must be of type html"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def link = Mock(SyndLink)
        link.getType() >> 'image/jpg'
        link.getHref() >> 'kjgkugkbkuh'
        link.getRel() >> LinkRelation.ABOUT.name()
        syndEntry.getLinks() >> [link]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemIdMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getId()
    }

    def "If no html links are available, use a generated uuid"() {
        given:
        def title = 'asdasd'
        def syndEntry = Mock(SyndEntry)
        syndEntry.getTitle() >> title

        and:
        def uuid = 'löknkljnkljb'

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemIdMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        1 * digestUtilsWrapper.md5Hex(title) >> uuid
        result.getId() == uuid
    }
}
