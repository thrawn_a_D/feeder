package de.wsorg.feeder.processor.util.load.util.mapper.rome.head

import com.sun.syndication.feed.synd.SyndFeed
import com.sun.syndication.feed.synd.SyndLink
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import org.apache.commons.lang.StringUtils
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeHeadLogoMapperTest extends Specification {
    RomeHeadLogoMapper romeHeadLogoMapper

    def setup(){
        romeHeadLogoMapper = new RomeHeadLogoMapper()
    }

    def "Map logo value"() {
        given:
        def syndFeed = Mock(SyndFeed)
        def link = Mock(SyndLink)
        link.getType() >> 'image/jpg'
        link.getHref() >> 'href'
        syndFeed.getLinks() >> [link]

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeadLogoMapper.mapHeaderData(syndFeed, feederModel)

        then:
        result.logoUrl == link.getHref()
    }

    def "Link is not of type image"() {
        given:
        def syndFeed = Mock(SyndFeed)
        def link = Mock(SyndLink)
        link.getType() >> 'application/pdf'
        link.getHref() >> 'href'
        syndFeed.getLinks() >> [link]

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeadLogoMapper.mapHeaderData(syndFeed, feederModel)

        then:
        StringUtils.isBlank(result.logoUrl)
    }

    def "Link is not of appropriate type"() {
        given:
        def syndFeed = Mock(SyndFeed)
        def link = "yssadfsd"
        syndFeed.getLinks() >> [link]

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeadLogoMapper.mapHeaderData(syndFeed, feederModel)

        then:
        StringUtils.isBlank(result.logoUrl)
    }
}
