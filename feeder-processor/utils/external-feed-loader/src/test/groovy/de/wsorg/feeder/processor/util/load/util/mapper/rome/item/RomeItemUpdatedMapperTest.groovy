package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndEntry
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemUpdatedMapperTest extends Specification {
    RomeItemUpdatedMapper romeItemUpdatedMapper

    def setup(){
        romeItemUpdatedMapper = new RomeItemUpdatedMapper()
    }

    def "Map id of an entry"() {
        given:
        def syndEntry = Mock(SyndEntry)
        syndEntry.getUpdatedDate() >> new Date()

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemUpdatedMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getUpdated() == syndEntry.getUpdatedDate()
    }

    def "If no updated field is set, look for the published value"() {
        given:
        def syndEntry = Mock(SyndEntry)
        syndEntry.getPublishedDate() >> new Date()

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemUpdatedMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getUpdated() == syndEntry.getPublishedDate()
    }
}
