package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndContent
import com.sun.syndication.feed.synd.SyndEntry
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemDescriptionMapperTest extends Specification {
    RomeItemDescriptionMapper romeItemDescriptionMapper

    def setup(){
        romeItemDescriptionMapper = new RomeItemDescriptionMapper()
    }

    def "Map description of an entry"() {
        given:
        def syndEntry = Mock(SyndEntry)
        def description = Mock(SyndContent)
        description.getValue() >> 'asdlknasd'
        syndEntry.getDescription() >> description

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemDescriptionMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getDescription() == description.getValue()
    }

    def "Description is not set"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemDescriptionMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getDescription()
    }
}
