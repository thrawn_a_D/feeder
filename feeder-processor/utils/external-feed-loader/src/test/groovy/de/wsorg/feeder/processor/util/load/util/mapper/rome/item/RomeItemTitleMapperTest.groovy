package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndEntry
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemTitleMapperTest extends Specification {
    RomeItemTitleMapper romeItemTitleMapper

    def setup(){
        romeItemTitleMapper = new RomeItemTitleMapper()
    }

    def "Map title of an entry"() {
        given:
        def syndEntry = Mock(SyndEntry)
        syndEntry.getTitle() >> 'sfdkjsdf'

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemTitleMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getTitle() == syndEntry.getTitle()
    }
}
