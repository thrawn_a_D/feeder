package de.wsorg.feeder.processor.util.load.external.atom

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.util.mapper.atom.AtomToFeedMapper
import de.wsorg.feeder.processor.util.load.util.wrapper.JavaUrlWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.12.12
 */
class ExternalAtomFeedLoaderTest extends Specification {
    ExternalAtomFeedLoader externalAtomFeedLoader
    JavaUrlWrapper urlWrapper
    AtomToFeedMapper atomToFeedMapper

    def setup(){
        externalAtomFeedLoader = new ExternalAtomFeedLoader()
        urlWrapper = Mock(JavaUrlWrapper)
        atomToFeedMapper = Mock(AtomToFeedMapper)
        externalAtomFeedLoader.javaUrlWrapper = urlWrapper
        externalAtomFeedLoader.atomToFeedMapper = atomToFeedMapper
    }

    def "Process url and retreive atom data"() {
        given:
        def feedUrl = 'http://asd'

        and:
        def rawXmlString = '<xml></xml>'
        def mappedFeedData = Mock(FeedModel)

        and:
        def urlConnection = Mock(HttpURLConnection)
        def inputStream = new ByteArrayInputStream(rawXmlString.bytes)
        def responseEncoding = 'UTF-8'

        when:
        def result = externalAtomFeedLoader.loadFeed(feedUrl)

        then:
        result == mappedFeedData
        1 * urlWrapper.getUrlConnection(feedUrl) >> urlConnection
        1 * urlConnection.getInputStream() >> inputStream
        1 * urlConnection.getContentEncoding() >> responseEncoding
        1 * atomToFeedMapper.mapAtomToFeed(rawXmlString, feedUrl) >> mappedFeedData
    }

    def "Error while receiving the response"() {
        given:
        def invalidUrl = 'asdasd'

        and:
        def connection = Mock(HttpURLConnection)

        and:
        urlWrapper.getUrlConnection(invalidUrl) >> connection
        connection.getInputStream() >> {throw new IOException()}

        when:
        externalAtomFeedLoader.loadFeed(invalidUrl)

        then:
        def ex = thrown(RuntimeException)
        ex.message == 'Error while reading the response from : ' + invalidUrl
    }

    def "Handle error when url is invalid"() {
        given:
        def invalidUrl = 'asdasd'

        and:
        urlWrapper.getUrlConnection(invalidUrl) >> {throw new IOException()}

        when:
        externalAtomFeedLoader.loadFeed(invalidUrl)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == 'Error while creating url connection for :' + invalidUrl
    }
}
