package de.wsorg.feeder.processor.util.load.external

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.ExternalFeedLoader
import de.wsorg.feeder.processor.util.load.external.atom.ExternalAtomFeedLoader
import de.wsorg.feeder.processor.util.load.external.feedtype.ExternalFeedType
import de.wsorg.feeder.processor.util.load.external.feedtype.FeedTypeTerminator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.12.12
 */
class ExternalFeedLoaderSwitcherTest extends Specification {
    ExternalFeedLoaderSwitcher externalFeedLoaderSwitcher
    FeedTypeTerminator feedTypeTerminator
    ExternalFeedLoader rssFeedLoader
    ExternalAtomFeedLoader atomFeedLoader

    def setup(){
        feedTypeTerminator = Mock(FeedTypeTerminator)
        rssFeedLoader = Mock(ExternalFeedLoader)
        atomFeedLoader = Mock(ExternalAtomFeedLoader)
        externalFeedLoaderSwitcher = new ExternalFeedLoaderSwitcher()
        externalFeedLoaderSwitcher.feedTypeTerminator = feedTypeTerminator
        externalFeedLoaderSwitcher.rssFeedLoader = rssFeedLoader
        externalFeedLoaderSwitcher.atomFeedLoader = atomFeedLoader
    }

    def "Choose an rss loader based on feed type determination"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.RSS

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * rssFeedLoader.loadFeed(feedUrl) >> loadedFeedType
        0 * atomFeedLoader.loadFeed(feedUrl)
    }

    def "Choose an atom loader based on feed type"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.ATOM

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * atomFeedLoader.loadFeed(feedUrl) >> loadedFeedType
        0 * rssFeedLoader.loadFeed(feedUrl)
    }

    def "If no type can be determinate choose rss loader as default"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.UNKNOWN

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * rssFeedLoader.loadFeed(feedUrl) >> loadedFeedType
        0 * atomFeedLoader.loadFeed(feedUrl)
    }

    def "If default rss loader can not perform operation use atom as fallback"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.RSS

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * rssFeedLoader.loadFeed(feedUrl) >> {throw new RuntimeException()}
        1 * atomFeedLoader.loadFeed(feedUrl) >> loadedFeedType
    }

    def "If default atom loader can not perform operation use rss as fallback"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.ATOM

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * atomFeedLoader.loadFeed(feedUrl) >> {throw new RuntimeException()}
        1 * rssFeedLoader.loadFeed(feedUrl) >> loadedFeedType
    }

    def "If default loader can not perform operation use rss as fallback"() {
        given:
        def feedUrl = 'bka'

        and:
        def feedType = ExternalFeedType.UNKNOWN

        and:
        def loadedFeedType = new FeedModel()

        when:
        def result = externalFeedLoaderSwitcher.loadFeed(feedUrl)

        then:
        result == loadedFeedType
        1 * feedTypeTerminator.determineFeedType(feedUrl) >> feedType
        1 * rssFeedLoader.loadFeed(feedUrl) >> {throw new RuntimeException()}
        1 * atomFeedLoader.loadFeed(feedUrl) >> loadedFeedType
    }
}
