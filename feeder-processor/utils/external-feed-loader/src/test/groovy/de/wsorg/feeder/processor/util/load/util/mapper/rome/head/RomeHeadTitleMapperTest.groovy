package de.wsorg.feeder.processor.util.load.util.mapper.rome.head

import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeHeadTitleMapperTest extends Specification {
    RomeHeadTitleMapper romeHeaTitleMapper

    def setup(){
        romeHeaTitleMapper = new RomeHeadTitleMapper()
    }

    def "Map title value"() {
        given:
        def syndFeed = Mock(SyndFeed)
        syndFeed.getTitle() >> 'title'

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeaTitleMapper.mapHeaderData(syndFeed, feederModel)

        then:
        result.title == syndFeed.getTitle()
    }
}
