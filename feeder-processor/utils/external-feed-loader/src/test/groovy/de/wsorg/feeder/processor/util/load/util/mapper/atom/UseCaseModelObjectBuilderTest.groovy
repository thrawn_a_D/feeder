package de.wsorg.feeder.processor.util.load.util.mapper.atom

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 01.01.13
 */
class UseCaseModelObjectBuilderTest extends Specification {

    def "Build feed object"() {
        when:
        def result = new UseCaseModelObjectBuilder().getAtomModelObject()

        then:
        result != null
        result instanceof FeedModel
    }

    def "Build feed entry object"() {
        when:
        def result = new UseCaseModelObjectBuilder().getAtomEntryModelObject()

        then:
        result != null
        result instanceof FeedEntryModel
    }
}
