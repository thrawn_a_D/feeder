package de.wsorg.feeder.processor.util.load.external.rss

import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss
import de.wsorg.feeder.processor.util.load.util.mapper.rss.RssToFeedMapper
import org.springframework.context.ApplicationContext
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.07.12
 */
class ExternalRssFeedLoaderTest extends Specification {

    ExternalRssFeedLoader externalRssFeedLoader;
    RestTemplate restTemplate
    RssToFeedMapper rssToFeedMapper
    ApplicationContext applicationContext;

    def setup(){
        externalRssFeedLoader = new ExternalRssFeedLoader();
        mockExternalClasses(externalRssFeedLoader)
    }

    def "Load an external rss feed using a valid feed url"() {
        given: "Test feed url"
        def feedUrl = 'http://my-feed.net/lalala_feed.xml'

        and: "A Rss stub to pass between mocks"
        def rssFeedStub = Mock(Rss)

        and: "A FeedModel to retreive as result"
        def feedModel = new FeedModel();
        feedModel.feedEntryModels << new FeedEntryModel()
        feedModel.feedEntryModels << new FeedEntryModel()

        when:
        def feedResult = externalRssFeedLoader.loadFeed(feedUrl)

        then:
        1 * restTemplate.getForObject({URI feedUri -> feedUri.toString() == feedUrl}, Rss.class) >> rssFeedStub
        1 * rssToFeedMapper.getFeedByRss(rssFeedStub) >> feedModel
        feedResult != null
        feedResult.feedUrl == feedUrl
        feedResult.feedEntryModels.every {it.feedUrl == feedUrl}
    }

    def "Test invalid provided url"() {
        given:
        def anInvalidFeedUrl = "ht%§sddd"

        when:
        externalRssFeedLoader.loadFeed(anInvalidFeedUrl)

        then:
        def ex = thrown(RuntimeException)
        ex.cause instanceof URISyntaxException
    }

    private void mockExternalClasses(ExternalRssFeedLoader externalRssFeedLoader) {
        restTemplate = Mock(RestTemplate)
        rssToFeedMapper = Mock(RssToFeedMapper)
        externalRssFeedLoader.rssToFeedMapper = rssToFeedMapper
        externalRssFeedLoader.restTemplate = restTemplate
    }
}
