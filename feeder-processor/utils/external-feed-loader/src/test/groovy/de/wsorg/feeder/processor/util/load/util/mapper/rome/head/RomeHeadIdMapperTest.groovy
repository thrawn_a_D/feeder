package de.wsorg.feeder.processor.util.load.util.mapper.rome.head

import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeHeadIdMapperTest extends Specification {
    RomeHeadIdMapper romeHeadIdMapper

    def setup(){
        romeHeadIdMapper = new RomeHeadIdMapper()
    }

    def "Map id value"() {
        given:
        def syndFeed = Mock(SyndFeed)
        syndFeed.getUri() >> 'asdasd'

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeadIdMapper.mapHeaderData(syndFeed, feederModel)

        then:
        result.id == syndFeed.getUri()
    }
}
