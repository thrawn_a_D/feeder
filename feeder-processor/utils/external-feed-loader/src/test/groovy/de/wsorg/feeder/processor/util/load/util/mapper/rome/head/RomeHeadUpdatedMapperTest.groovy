package de.wsorg.feeder.processor.util.load.util.mapper.rome.head

import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeHeadUpdatedMapperTest extends Specification {
    RomeHeadUpdatedMapper romeHeadUpdatedMapper

    def setup(){
        romeHeadUpdatedMapper = new RomeHeadUpdatedMapper()
    }

    def "Map updated value"() {
        given:
        def syndFeed = Mock(SyndFeed)
        syndFeed.getPublishedDate() >> new Date()

        and:
        def feederModel = new FeedModel()

        when:
        def result = romeHeadUpdatedMapper.mapHeaderData(syndFeed, feederModel)

        then:
        result.updated == syndFeed.getPublishedDate()
    }
}
