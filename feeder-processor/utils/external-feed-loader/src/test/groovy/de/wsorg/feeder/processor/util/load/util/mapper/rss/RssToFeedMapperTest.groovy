package de.wsorg.feeder.processor.util.load.util.mapper.rss;


import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss
import de.wsorg.feeder.processor.util.load.external.rss.RssFeedCreator
import de.wsorg.feeder.utils.date.DateTimeParser

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 05.07.12
 */
public class RssToFeedMapperTest extends RssFeedCreator {

    RssToFeedMapper rssToFeedMapper;

    def setup(){
        rssToFeedMapper = new RssToFeedMapper();
    }

    def "Parse raw rss header into feed data structure without the feed entries"(){
        given: "Some test data"
        FeedModel expectedTestData = getTestFeedWithoutEntires()

        and: "The raw test rss test stub"
        Rss rawRss = getRssChannelWithoutEntries(expectedTestData)

        when:
        FeedModel resultedFeed = rssToFeedMapper.getFeedByRss(rawRss);

        then:
        resultedFeed != null
        resultedFeed.title == expectedTestData.title
        resultedFeed.description == expectedTestData.description
        resultedFeed.logoUrl == expectedTestData.logoUrl
        DateTimeParser.parse(resultedFeed.updated) == DateTimeParser.parse(expectedTestData.updated)
    }

    def "Parse some feed entries"(){
        given:
        FeedModel testFeedWithFeedEntries = getTestFeedWithEntries()
        Rss rawRssData = getRssChannelWithFeedEntries(testFeedWithFeedEntries)

        when:
        FeedModel resultedFeed = rssToFeedMapper.getFeedByRss(rawRssData);

        then:
        resultedFeed != null
        resultedFeed.feedEntryModels != null
        resultedFeed.feedEntryModels.size() > 0

        resultedFeed.feedEntryModels[0].title == testFeedWithFeedEntries.feedEntryModels[0].title
        resultedFeed.feedEntryModels[0].description == testFeedWithFeedEntries.feedEntryModels[0].description
        resultedFeed.feedEntryModels[0].updated.date == testFeedWithFeedEntries.feedEntryModels[0].updated.date
        resultedFeed.feedEntryModels[0].id == testFeedWithFeedEntries.feedEntryModels[0].id

        resultedFeed.feedEntryModels[1].title == testFeedWithFeedEntries.feedEntryModels[1].title
        resultedFeed.feedEntryModels[1].description == testFeedWithFeedEntries.feedEntryModels[1].description
        resultedFeed.feedEntryModels[1].updated.date == testFeedWithFeedEntries.feedEntryModels[1].updated.date
        resultedFeed.feedEntryModels[1].id == testFeedWithFeedEntries.feedEntryModels[1].id

        resultedFeed.feedEntryModels[1].links[0].href == testFeedWithFeedEntries.feedEntryModels[1].links[0].href
        resultedFeed.feedEntryModels[1].links[0].linkType.linkType == testFeedWithFeedEntries.feedEntryModels[1].links[0].linkType.linkType
        resultedFeed.feedEntryModels[1].links[1].href == testFeedWithFeedEntries.feedEntryModels[1].links[1].href
        resultedFeed.feedEntryModels[1].links[1].linkType.linkType == testFeedWithFeedEntries.feedEntryModels[1].links[1].linkType.linkType
    }

    private FeedModel getTestFeedWithoutEntires() {
        FeedModel expectedTestData = new FeedModel();
        expectedTestData.title = 'Expected title'
        expectedTestData.description = 'This is a test description'
        expectedTestData.logoUrl = 'http://image.de/image.jpg'
        expectedTestData.updated = new Date();
        expectedTestData
    }

    private FeedModel getTestFeedWithEntries() {
        final FeedModel testStub = getTestFeedWithoutEntires();

        FeedEntryModel firstFeedEntry = getFeedEntry('my entry test title',
                                                'my entry test description');
        FeedEntryModel secondFeedEntry = getFeedEntry('my entry test title 2',
                                                 'my entry test description 2');

        testStub.feedEntryModels = [firstFeedEntry, secondFeedEntry]
        return testStub;
    }

    private FeedEntryModel getFeedEntry(final String title, final String description) {
        FeedEntryModel feedEntry = new FeedEntryModel();
        feedEntry.title = title
        feedEntry.description = description
        feedEntry.updated = new Date();
        feedEntry.id = 'http://entry#hash'

        def linkType1 = new LinkType(linkType: "audio/mpeg")
        def linkType2 = new LinkType(linkType: "text/html")
        def link1 = new Link(href: 'href1', linkType: linkType1)
        def link2 = new Link(href: 'href2', linkType: linkType2)
        feedEntry.links = [link1, link2]

        return feedEntry;
    }
}
