package de.wsorg.feeder.processor.util.load.util.mapper.atom

import de.wsorg.feeder.processor.domain.standard.atom.StandardFeed
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.mapping.feed.generic.XmlToDomainMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 30.12.12
 */
class AtomToFeedMapperTest extends Specification {
    AtomToFeedMapper atomToFeedMapper
    XmlToDomainMapper<StandardFeed> atomXmlToDomainMapper

    def setup(){
        atomToFeedMapper = new AtomToFeedMapper()
        atomXmlToDomainMapper = Mock(XmlToDomainMapper)
        atomToFeedMapper.atomXmlToDomainMapper = atomXmlToDomainMapper
    }

    def "Use atom mapper to map the response to a feeder domain"() {
        given:
        def xmlString = '<xml></xml>'
        def feedUrl = 'http://asdkj'

        and:
        def atom = new FeedModel()
        atom.title = 'bla'
        atom.updated = new Date()
        atom.logoUrl = 'http://asdasd'

        and:
        def atomEntry = new FeedEntryModel()
        atomEntry.description = 'desc'
        atomEntry.id = 'logo'
        atomEntry.title = 'asdasd'
        atomEntry.updated = new Date()
        atom.feedEntries.add(atomEntry)

        when:
        def result = atomToFeedMapper.mapAtomToFeed(xmlString, feedUrl)

        then:
        result.title == atom.title
        result.updated == atom.updated
        result.feedUrl == feedUrl
        result.feedEntryModels[0].feedUrl == feedUrl
        result.feedEntryModels[0].title == atomEntry.title
        result.feedEntryModels[0].id == atomEntry.id
        result.feedEntryModels[0].updated == atomEntry.updated
        result.feedEntryModels[0].description == atomEntry.description

        1 * atomXmlToDomainMapper.getResult(xmlString) >> atom
    }
}
