package de.wsorg.feeder.processor.util.load.util.spring.http.converter

import de.wsorg.feeder.processor.util.load.domain.generated.rss.Rss
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import org.apache.commons.lang.NotImplementedException
import org.springframework.http.HttpInputMessage
import org.springframework.http.MediaType
import spock.lang.Specification

import javax.xml.bind.JAXBException

/**
 * @author wschneider
 * Date: 22.11.12
 * Time: 09:32
 */
class CustomRssHttpMessageConverterTest extends Specification {
    CustomRssHttpMessageConverter customRssHttpMessageConverter
    JaxbUnmarshaller jaxbUnmarshaller

    def setup(){
        customRssHttpMessageConverter = new CustomRssHttpMessageConverter()
        jaxbUnmarshaller  = Mock(JaxbUnmarshaller)
        customRssHttpMessageConverter.jaxbUnmarshaller = jaxbUnmarshaller
    }


    def "Make sure converter support our jaxb rss"() {
        given:
        Class<Rss> classType = Rss;

        when:
        def result = customRssHttpMessageConverter.supports(classType)

        then:
        result == true
    }

    def "Make sure converter don support any other type except RSS"() {
        given:
        Class<String> classType = String;



        when:
        def result = customRssHttpMessageConverter.supports(classType)

        then:
        result == false
    }

    def "Make sure write method throws a not implemented exception"() {
        when:
        customRssHttpMessageConverter.writeInternal(null, null);

        then:
        thrown(NotImplementedException)
    }

    def "Convert a provided http response to something useful"() {
        given:
        def ourRssAsString ="<rss>bla</rss>"
        InputStream rssAsStream = new ByteArrayInputStream(ourRssAsString.getBytes("UTF-8"));

        and:
        HttpInputMessage httpOutputMessage = Mock(HttpInputMessage)
        httpOutputMessage.getBody() >> rssAsStream


        and:
        Rss expectedResult = Mock(Rss)

        when:
        def result = customRssHttpMessageConverter.readInternal(Rss.class, httpOutputMessage)

        then:
        result == expectedResult
        1 * jaxbUnmarshaller.unmarshal(ourRssAsString) >> expectedResult
    }

    def "Make sure an exception is thrown when xml is not valid"() {
        given:
        def ourRssAsString ="<rsrss>"
        InputStream rssAsStream = new ByteArrayInputStream(ourRssAsString.getBytes("UTF-8"));

        and:
        HttpInputMessage httpOutputMessage = Mock(HttpInputMessage)
        httpOutputMessage.getBody() >> rssAsStream


        and:
        jaxbUnmarshaller.unmarshal(_) >> {throw new JAXBException("")}

        when:
        customRssHttpMessageConverter.readInternal(Rss.class, httpOutputMessage)

        then:
        def ex = thrown(RuntimeException)
        ex.message == "The http response does not provide a valid content which could be parsed by jaxb"
    }

    def "Response has a null body content: trow runtime exception"() {
        given:
        HttpInputMessage httpOutputMessage = Mock(HttpInputMessage)

        when:
        customRssHttpMessageConverter.readInternal(Rss.class, httpOutputMessage)

        then:
        def ex = thrown(RuntimeException)
        !(ex instanceof NullPointerException)
        ex.message == "The rss response does not a valid body"
    }

    def "Make sure the converter supports all media types"() {
        when:
        def mediaTypes = customRssHttpMessageConverter.getSupportedMediaTypes()

        then:
        mediaTypes.contains(MediaType.ALL)
    }
}
