package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndEnclosure
import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndLink
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemLogoMapperTest extends Specification {
    RomeItemLogoMapper romeItemLogoMapper

    def setup(){
        romeItemLogoMapper = new RomeItemLogoMapper()
    }

    def "Map logo of an entry, using links"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def syndLink = Mock(SyndLink)
        syndLink.getType() >> 'image/jpeg'
        syndLink.getHref() >> 'asdasdasd'
        syndEntry.getLinks() >> [syndLink]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getLogoUrl() == syndLink.getHref()
    }

    def "Map logo of an entry, using links which have the wrong type"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def syndLink = Mock(SyndLink)
        syndLink.getType() >> 'application/pdf'
        syndLink.getHref() >> 'asdasdasd'
        syndEntry.getLinks() >> [syndLink]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getLogoUrl()
    }

    def "Link is not of type SyndLink"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        syndEntry.getLinks() >> ['asd']

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getLogoUrl()
    }

    def "If links do not contain images, use enclosure"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def syndEnclosure = Mock(SyndEnclosure)
        syndEnclosure.getType() >> 'image/jpeg'
        syndEnclosure.getUrl() >> 'asdasdasd'
        syndEntry.getEnclosures() >> [syndEnclosure]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.getLogoUrl() == syndEnclosure.getUrl()
    }

    def "Map logo of an entry, using enclosures which have the wrong type"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def syndEnclosure = Mock(SyndEnclosure)
        syndEnclosure.getType() >> 'application/pdf'
        syndEnclosure.getUrl() >> 'asdasdasd'
        syndEntry.getEnclosures() >> [syndEnclosure]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getLogoUrl()
    }

    def "Enclosures is not of type Enclosure"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        syndEntry.getEnclosures() >> ['asd']

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLogoMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        !result.getLogoUrl()
    }
}
