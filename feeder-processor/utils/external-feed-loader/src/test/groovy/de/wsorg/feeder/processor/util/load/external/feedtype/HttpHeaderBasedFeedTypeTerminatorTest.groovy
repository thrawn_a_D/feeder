package de.wsorg.feeder.processor.util.load.external.feedtype

import de.wsorg.feeder.processor.production.domain.exception.UseCaseProcessException
import de.wsorg.feeder.processor.util.load.util.wrapper.JavaUrlWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 23.12.12
 */
class HttpHeaderBasedFeedTypeTerminatorTest extends Specification {
    HttpHeaderBasedFeedTypeTerminator headerBasedFeedTypeTerminator
    JavaUrlWrapper javaUrlWrapper
    HttpURLConnection urlConnection

    def setup(){
        javaUrlWrapper = Mock(JavaUrlWrapper)
        headerBasedFeedTypeTerminator = new HttpHeaderBasedFeedTypeTerminator()
        headerBasedFeedTypeTerminator.urlWrapper = javaUrlWrapper
        urlConnection = Mock(HttpURLConnection)
    }

    def "Determinate type of feed using HTTP-ContentType header"() {
        given:
        def feedUrl='http://cre.fm/feed'

        and:
        javaUrlWrapper.getUrlConnection(feedUrl)>>urlConnection

        and:
        def expectedContentType = 'application/rss+xml'

        when:
        def type = headerBasedFeedTypeTerminator.determineFeedType(feedUrl)

        then:
        type == ExternalFeedType.RSS
        1 * urlConnection.setRequestMethod('HEAD')
        1 * urlConnection.connect()
        1 * urlConnection.getContentType() >> expectedContentType
    }

    def "Determine atom feed type"() {
        given:
        def feedUrl='http://cre.fm/feed?type=atom'

        and:
        javaUrlWrapper.getUrlConnection(feedUrl)>>urlConnection

        and:
        def expectedContentType = 'application/atom+xml'

        when:
        def type = headerBasedFeedTypeTerminator.determineFeedType(feedUrl)

        then:
        type == ExternalFeedType.ATOM
        1 * urlConnection.setRequestMethod('HEAD')
        1 * urlConnection.connect()
        1 * urlConnection.getContentType() >> expectedContentType
    }

    def "Type can not be determined"() {
        given:
        def feedUrl='http://cre.fm/feed'

        and:
        javaUrlWrapper.getUrlConnection(feedUrl)>>urlConnection

        and:
        def expectedContentType = 'text/xml'

        when:
        def type = headerBasedFeedTypeTerminator.determineFeedType(feedUrl)

        then:
        type == ExternalFeedType.UNKNOWN
        1 * urlConnection.setRequestMethod('HEAD')
        1 * urlConnection.connect()
        1 * urlConnection.getContentType() >> expectedContentType
    }

    def "Malformed url"() {
        given:
        def feedUrl='http://fm/feed'

        and:
        javaUrlWrapper.getUrlConnection(feedUrl)>>{throw new IOException()}

        when:
        headerBasedFeedTypeTerminator.determineFeedType(feedUrl)

        then:
        def ex = thrown(UseCaseProcessException)
        ex.message == "The provided url is not valid (${feedUrl})."
    }

    def "Connection to url source can not be established"() {
        given:
        def feedUrl='http://fm/feed'

        and:
        def connection = Mock(HttpURLConnection)

        and:
        javaUrlWrapper.getUrlConnection(feedUrl)>> connection

        and:
        connection.connect() >> {throw new IOException()}

        when:
        headerBasedFeedTypeTerminator.determineFeedType(feedUrl)

        then:
        def ex = thrown(IllegalStateException)
        ex.message == "Could not establish a connection for (${feedUrl})."
    }
}
