package de.wsorg.feeder.processor.util.load;

import spock.lang.Specification;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.03.13
 */
public class ExternalFeedLoaderFactoryTest extends Specification {

    def "Get an instance of a feed loader"() {
        when:
        def result = ExternalFeedLoaderFactory.getFeedLoader()

        then:
        result
    }
}
