package de.wsorg.feeder.processor.util.load.util.mapper.rome.item

import com.sun.syndication.feed.synd.SyndEnclosure
import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndLink
import de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkRelation
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 28.05.13
 */
class RomeItemLinksMapperTest extends Specification {
    RomeItemLinksMapper romeItemLinksMapper

    def setup(){
        romeItemLinksMapper = new RomeItemLinksMapper()
    }

    def "Map links of an entry"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def link = Mock(SyndLink)
        link.getType() >> 'image/jpeg'
        link.getHref() >> 'kjgkugkbkuh'
        link.getRel() >> LinkRelation.ABOUT.name()
        syndEntry.getLinks() >> [link]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links[0].href == link.getHref()
        result.links[0].linkType.linkType == link.getType()
        result.links[0].relation.name() == link.getRel()
    }

    def "Entry is not of type SyndLink"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        syndEntry.getLinks() >> ['asd']

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links.size() == 0
    }

    def "All SyndLink attributes are null"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def link = Mock(SyndLink)
        syndEntry.getLinks() >> [link]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links.size() == 0
    }

    def "Map occurring enclosures as links"() {
        given: 
        def syndEntry = Mock(SyndEntry)
        
        and:
        def syndEnclosure = Mock(SyndEnclosure)
        syndEnclosure.getUrl() >> 'url'
        syndEnclosure.getType() >> 'application/html'
        syndEnclosure.getLength() >> 987987
        syndEntry.getEnclosures() >> [syndEnclosure]
             
        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links[0].href == syndEnclosure.getUrl()
        result.links[0].linkType.linkType == syndEnclosure.getType()
        result.links[0].relation == LinkRelation.ENCLOSURE
    }

    def "Enclosure is not of type SyndEnclosure"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        def syndEnclosure = 'asdsad'
        syndEntry.getEnclosures() >> [syndEnclosure]

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links.size() == 0
    }

    def "Extract single link field"() {
        given:
        def syndEntry = Mock(SyndEntry)

        and:
        syndEntry.getLink() >> 'gsdfkjsfdkjb'

        and:
        def feederEntry = new FeedEntryModel()

        when:
        def result = romeItemLinksMapper.mapFeedEntryData(syndEntry, feederEntry)

        then:
        result.links[0].href == syndEntry.getLink()
        result.links[0].linkType.linkType == 'text/html'
        result.links[0].relation == LinkRelation.RELATED
    }
}
