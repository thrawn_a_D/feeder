package de.wsorg.feeder.processor.util.load.util.mapper.rome

import com.sun.syndication.feed.synd.SyndEntry
import com.sun.syndication.feed.synd.SyndFeed
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedEntryModel
import de.wsorg.feeder.processor.production.domain.feeder.feed.feed.FeedModel
import de.wsorg.feeder.processor.util.load.util.mapper.rome.head.HeadDataMapper
import de.wsorg.feeder.processor.util.load.util.mapper.rome.item.ItemDataMapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 27.05.13
 */
class RomeFeedMapperTest extends Specification {
    RomeFeedMapper romeFeedMapper
    HeadDataMapper headDataMapper
    ItemDataMapper itemDataMapper

    def setup(){
        romeFeedMapper = new RomeFeedMapper()

        headDataMapper = Mock(HeadDataMapper)
        itemDataMapper = Mock(ItemDataMapper)

        romeFeedMapper.headDataMappers = [headDataMapper]
        romeFeedMapper.itemDataMappers = [itemDataMapper]
    }

    def "Delegate mapping of header data"() {
        given:
        def feed = Mock(SyndFeed)

        when:
        def result = romeFeedMapper.mapFeed(feed)

        then:
        result
        1 * headDataMapper.mapHeaderData(feed, {it instanceof FeedModel})
    }

    def "Delegate mapping of entry data"() {
        given:
        def feed = Mock(SyndFeed)
        def entry = Mock(SyndEntry)

        and:
        feed.getEntries() >> [entry]

        when:
        def result = romeFeedMapper.mapFeed(feed)

        then:
        result.feedEntries.size() == 1
        1 * itemDataMapper.mapFeedEntryData(entry, {it instanceof FeedEntryModel})
    }

    def "Entry is not of type SyndEntry"() {
        given:
        def feed = Mock(SyndFeed)
        def entry = "asdasasd"

        and:
        feed.getEntries() >> [entry]

        when:
        def result = romeFeedMapper.mapFeed(feed)

        then:
        result.feedEntries.size() == 0
        0 * itemDataMapper.mapFeedEntryData(entry, {it instanceof FeedEntryModel})
    }
}
