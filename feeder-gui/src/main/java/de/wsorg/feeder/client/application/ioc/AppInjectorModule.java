package de.wsorg.feeder.client.application.ioc;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.inject.Singleton;
import com.gwtplatform.mvp.client.RootPresenter;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.proxy.ParameterTokenFormatter;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.TokenFormatter;
import de.wsorg.feeder.client.event.readstatus.all.AllEntriesReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.all.SimpleAllEntriesReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.multyentry.MultiEntryReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.multyentry.SimpleMultiEntryReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.singleentry.SimpleSingleReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.singleentry.SingleReadStatusHandler;
import de.wsorg.feeder.client.event.subscribtion.SimpleSubscriptionHandler;
import de.wsorg.feeder.client.event.subscribtion.SubscriptionHandler;
import de.wsorg.feeder.client.place.AppPlaceManager;
import de.wsorg.feeder.client.place.DefaultPlace;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.MainPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.login.LoginPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.newaccount.NewAccountPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.user.UserSettingsOverviewPresenter;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsExtendedPresenter;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsPresenter;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.presenter.getfeeds.FoundFeedListPresenter;
import de.wsorg.feeder.client.presenter.myfeeds.MyFeedsListPresenter;
import de.wsorg.feeder.client.presenter.timeline.MyFeedTimeLinePresenter;
import de.wsorg.feeder.client.request.FeedRequestManager;
import de.wsorg.feeder.client.security.LogInGateKeeper;
import de.wsorg.feeder.client.security.session.ClientSessionStore;
import de.wsorg.feeder.client.security.session.SimpleClientSessionStore;
import de.wsorg.feeder.client.security.session.user.SimpleUserSession;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.ui.MainPageViewImpl;
import de.wsorg.feeder.client.ui.authentication.login.LoginPageViewImpl;
import de.wsorg.feeder.client.ui.authentication.newaccount.NewAccountPageViewImpl;
import de.wsorg.feeder.client.ui.authentication.user.UserSettingsOverviewViewImpl;
import de.wsorg.feeder.client.ui.common.feeddetails.FeedDetailsExtendedViewImpl;
import de.wsorg.feeder.client.ui.common.feeddetails.FeedDetailsViewImpl;
import de.wsorg.feeder.client.ui.common.feedlist.MainFeedListContainerViewImpl;
import de.wsorg.feeder.client.ui.getfeeds.FoundFeedListViewImpl;
import de.wsorg.feeder.client.ui.myfeeds.MyFeedListViewImpl;
import de.wsorg.feeder.client.ui.timeline.MyFeedTimeLineViewImpl;
import de.wsorg.feeder.shared.ApplicationRequestFactory;

public class AppInjectorModule extends AbstractPresenterModule {

    @Override
    protected void configure() {
        bind(EventBus.class).to(SimpleEventBus.class).in(Singleton.class);
        bind(PlaceManager.class).to(AppPlaceManager.class).in(Singleton.class);
        bind(ApplicationRequestFactory.class).in(Singleton.class);
        bind(LogInGateKeeper.class).in(Singleton.class);

        bind(SubscriptionHandler.class).to(SimpleSubscriptionHandler.class);
        bind(SingleReadStatusHandler.class).to(SimpleSingleReadStatusHandler.class);
        bind(MultiEntryReadStatusHandler.class).to(SimpleMultiEntryReadStatusHandler.class);
        bind(AllEntriesReadStatusHandler.class).to(SimpleAllEntriesReadStatusHandler.class);

                bind(TokenFormatter.class).to(ParameterTokenFormatter.class).in(Singleton.class);
        bind(RootPresenter.class).asEagerSingleton();
        
        bind(FeedRequestManager.class).asEagerSingleton();

        bindConstant().annotatedWith(DefaultPlace.class).to(PlaceNameToken.FOUND_FEED_LIST);

        //Presenters
        bindPresenter(MainPagePresenter.class,
                      MainPagePresenter.MainPageView.class,
                      MainPageViewImpl.class,
                      MainPagePresenter.MainPageProxy.class);
		bindPresenter(MainFeedListContainerPresenter.class,
                      MainFeedListContainerPresenter.MainFeedListContainerView.class,
                      MainFeedListContainerViewImpl.class,
                      MainFeedListContainerPresenter.MainFeedListContainerProxy.class);
		bindPresenter(FeedDetailsPresenter.class,
                      FeedDetailsPresenter.FeedItemDetailsView.class,
                      FeedDetailsViewImpl.class,
                      FeedDetailsPresenter.FeedItemDetailsProxy.class);
		bindPresenter(FeedDetailsExtendedPresenter.class,
                      FeedDetailsExtendedPresenter.FeedDetailsExtendedView.class,
                      FeedDetailsExtendedViewImpl.class,
                      FeedDetailsExtendedPresenter.FeedDetailsExtendedProxy.class);
		bindPresenter(FoundFeedListPresenter.class,
                      FoundFeedListPresenter.FoundFeedListView.class,
                      FoundFeedListViewImpl.class,
				      FoundFeedListPresenter.FeedFoundListProxy.class);
		bindPresenter(MyFeedsListPresenter.class,
                      MyFeedsListPresenter.MyFeedListView.class,
                      MyFeedListViewImpl.class,
                      MyFeedsListPresenter.MyFeedsListProxy.class);
        bindPresenter(MyFeedTimeLinePresenter.class,
                      MyFeedTimeLinePresenter.MyFeedTimeLineView.class,
                      MyFeedTimeLineViewImpl.class,
                      MyFeedTimeLinePresenter.MyFeedTimeLineProxy.class);

        //AUTHENTICATION
        bindPresenter(LoginPagePresenter.class,
                      LoginPagePresenter.LoginPageView.class,
                      LoginPageViewImpl.class,
                      LoginPagePresenter.LoginPageProxy.class);
        bindPresenter(NewAccountPagePresenter.class,
                      NewAccountPagePresenter.NewAccountPageView.class,
                      NewAccountPageViewImpl.class,
                      NewAccountPagePresenter.NewAccountPageProxy.class);

        //USER-MANAGEMENT
        bindPresenter(UserSettingsOverviewPresenter.class,
                      UserSettingsOverviewPresenter.UserManagementOverviewView.class,
                      UserSettingsOverviewViewImpl.class,
                      UserSettingsOverviewPresenter.UserManagementOverviewProxy.class);


        //Security
        bind(ClientSessionStore.class).to(SimpleClientSessionStore.class).in(Singleton.class);
        bind(UserSession.class).to(SimpleUserSession.class).in(Singleton.class);
    }

}