package de.wsorg.feeder.client.presenter.common.feedlist;

import com.gwtplatform.mvp.client.View;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 *
 *
 *
 */
public interface FeedListView extends View {
    void setFeedToList(OpmlWrapper atomResult);
    void clear();
    void showNoResultsFoundMessage();
}
