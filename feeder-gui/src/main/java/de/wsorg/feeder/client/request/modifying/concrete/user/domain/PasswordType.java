package de.wsorg.feeder.client.request.modifying.concrete.user.domain;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public enum PasswordType {
    MD5,
    PLAIN_TEXT
}
