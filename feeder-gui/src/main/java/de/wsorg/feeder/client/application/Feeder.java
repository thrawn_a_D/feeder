package de.wsorg.feeder.client.application;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.gwtplatform.mvp.client.DelayedBindRegistry;

import de.wsorg.feeder.client.application.ioc.AppInjector;
import de.wsorg.feeder.client.application.ioc.DesktopInjectorWrapper;
import de.wsorg.feeder.client.application.ioc.InjectorWrapper;
import de.wsorg.feeder.client.resources.jsvascript.FeederJavaScriptInjector;
import de.wsorg.feeder.client.resources.jsvascript.MD5Script;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Feeder implements EntryPoint {

    // we are going to use our little injectorWrapper to get everything started
    // for us
    // We will use deferred binding so we can create all our different apps
    // (Desktop & Mobile)
    // see the Gwt_seminar.gwt.xml to see how we switch in MobileInjectorWrapper
    // when on a mobile device
    final private InjectorWrapper injectorWrapper = GWT.create(DesktopInjectorWrapper.class);

    /**
     * This is the entry point method.
     */
    @Override
    public void onModuleLoad() {
        AppInjector injector = injectorWrapper.getInjector();
        
        DelayedBindRegistry.bind(injector);

        loadJavaScriptLibraries();

        /*
         * use our injectorWrapper to get the appropriate injector and then to
         * use that to get our platform specific application
         */
        injector.getFeederApp().run();
    }

    private void loadJavaScriptLibraries() {
        MD5Script bundle = GWT.create(MD5Script.class);
        FeederJavaScriptInjector.inject(bundle.md5ScriptJS().getText());
    }
}
