package de.wsorg.feeder.client.event.readstatus.singleentry;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.presenter.MainPagePresenter;
import de.wsorg.feeder.client.request.modifying.concrete.feed.SingleReadStatusModifier;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatusProxy;

/**
 *
 *
 *
 */
public class SimpleSingleReadStatusHandler implements SingleReadStatusHandler {
    private final SingleReadStatusModifier singleReadStatusModifier;
    private final MainPagePresenter mainPagePresenter;

    @Inject
    public SimpleSingleReadStatusHandler(final SingleReadStatusModifier singleReadStatusModifier,
                                          final MainPagePresenter mainPagePresenter) {
        this.singleReadStatusModifier = singleReadStatusModifier;
        this.mainPagePresenter = mainPagePresenter;
    }

    @Override
    public void markFeedEntryAsRead(final String feedUrl, final String feedEntryId) {
        markFeed(feedUrl, feedEntryId, true);
    }

    @Override
    public void markFeedEntryAsUnread(final String feedUrl, final String feedEntryId) {
        markFeed(feedUrl, feedEntryId, false);
    }

    private void markFeed(final String feedUrl, final String feedEntryId, final boolean feedEntryRead) {
        final SingleFeedEntryReadStatusProxy singleReadStatusRequest = getChangeRequest(feedUrl, feedEntryId, feedEntryRead);

        Receiver<Void> receiver = getDefaultSubscriptionReceiver();

        singleReadStatusModifier.processRequest(singleReadStatusRequest, receiver, Void.class);
    }

    private SingleFeedEntryReadStatusProxy getChangeRequest(final String feedUrl, final String feedEntryId, final boolean feedEntryRead) {
        SingleFeedEntryReadStatusProxy readStatusProxy = singleReadStatusModifier.getNewDomainObjectForParam(SingleFeedEntryReadStatusProxy.class);

        readStatusProxy.setFeedUrl(feedUrl);
        readStatusProxy.setFeedEntryId(feedEntryId);
        readStatusProxy.setFeedEntryReadStatus(feedEntryRead);

        return readStatusProxy;
    }

    private Receiver<Void> getDefaultSubscriptionReceiver() {
        return new Receiver<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {

            }

            @Override
            public void onFailure(final ServerFailure error) {
                ShowErrorMessageEvent.fire(mainPagePresenter, error.getMessage());
            }
        };
    }
}
