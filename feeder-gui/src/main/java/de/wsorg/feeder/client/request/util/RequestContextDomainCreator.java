package de.wsorg.feeder.client.request.util;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.BaseProxy;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 *
 *
 *
 */
public class RequestContextDomainCreator {
    private RequestContext requestContext;
    private UserSession userSession;

    public <D extends BaseProxy> D getNewDomainObjectForParam(final Class<D> classType) {
        D model = requestContext.create(classType);

        if(model instanceof AuthenticatedRequest) {
            FeederUserProxy currentUser = userSession.getCurrentUser();

            if (currentUser != null) {
                AuthenticatedRequest authenticatedModel = (AuthenticatedRequest) model;
                authenticatedModel.setUserId(currentUser.getUserId());
                authenticatedModel.setUserName(currentUser.getUserName());
                authenticatedModel.setPassword(currentUser.getPassword());
            }
        }

        return model;
    }

    public void setRequestContext(final RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    @Inject
    public void setUserSession(final UserSession userSession) {
        this.userSession = userSession;
    }
}
