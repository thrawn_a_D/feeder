package de.wsorg.feeder.client.ui.common.feeddetails;

import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.datasource.AtomEntryDataSource;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsPresenter.FeedItemDetailsView;
import de.wsorg.feeder.client.ui.cell.FeedEntryDetail;
import de.wsorg.feeder.client.ui.widget.ShowMorePagerPanel;
import de.wsorg.feeder.client.ui.widget.common.button.FeederButton;
import de.wsorg.feeder.client.ui.widget.getfeeds.FeedInfo;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

public class FeedDetailsViewImpl extends ViewImpl implements FeedItemDetailsView {

	private static int PAGE_SIZE = 10;
	
	private Widget widget;

	@UiField SimplePanel pnlFeedInfo;
	@UiField ShowMorePagerPanel vpFeedEntries;

	private final FeedInfo feedInfo;

	/**
	 * The CellList.
	 */
	private CellList<AtomEntryProxy> cellList;

	private final PlaceManager placeManager;
    private final FeedEntryDetail cellItem;

    @Inject
	public FeedDetailsViewImpl(final Binder uiBinder,
                               final FeedInfo feedInfo,
			                   final PlaceManager placeManager,
                               final FeedEntryDetail cellItem) {
		super();
		this.placeManager = placeManager;
        this.cellItem = cellItem;
        this.widget = uiBinder.createAndBindUi(this);
		
		this.feedInfo = feedInfo;
		this.initializeCellList();
	}

    @Override
    public void setPagingTimeLineEvent(final ClickHandler scrollDown) {
        vpFeedEntries.addPagingEventHandler(scrollDown);
    }

    @Override
    public void disablePagingButton() {
        this.vpFeedEntries.disablePagingButton();
    }

    @Override
    public void enablePagingButton() {
        this.vpFeedEntries.enablePagingButton();
    }

    public interface Binder extends UiBinder<Widget, FeedDetailsViewImpl> {
	}

	@Override
	public Widget asWidget() {
		return widget;
	}

	@Override
	public void setFeedInfo(AtomWrapper atom) {
		this.feedInfo.clear();
		this.feedInfo.initFeedInfoUi(atom);
		this.pnlFeedInfo.setWidget(feedInfo);
		this.pnlFeedInfo.setVisible(true);
	}

    @Override
    public void setAtom(final AtomWrapper atom) {
        this.cellItem.setRelatedAtom(atom);
    }

    @Override
	public void showLoadingInfo() {
		Label loading = new Label("Loading content please wait");
		this.pnlFeedInfo.clear();		
		this.pnlFeedInfo.add(loading);
		this.pnlFeedInfo.setVisible(true);
	}

    @Override
    public void addSubscribeButton(final ClickHandler subscriptionEvent) {
        FeederButton btnSubscribeToFeed = new FeederButton();
        btnSubscribeToFeed.addClickHandler(subscriptionEvent);
        btnSubscribeToFeed.setText("Subscribe");
        this.feedInfo.addActionButton("btnSubscribe", btnSubscribeToFeed);
    }

    @Override
    public void addUnsubscribeButton(final ClickHandler subscriptionEvent) {
        FeederButton btnSubscribeToFeed = new FeederButton();
        btnSubscribeToFeed.addClickHandler(subscriptionEvent);
        btnSubscribeToFeed.setText("Unsubscribe");
        this.feedInfo.addActionButton("btnSubscribe", btnSubscribeToFeed);
    }

    @Override
	public void setFeedEntryList(List<AtomEntryProxy> atomEntry) {
		AtomEntryDataSource.get().setEntryList(atomEntry);
		this.cellList.setVisible(true);
	}

	private void initializeCellList() {
		cellList = new CellList<AtomEntryProxy>(cellItem,
				AtomEntryDataSource.FeedEntryProxy_KEY_PROVIDER);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);

		AtomEntryDataSource.get().addDataDisplay(cellList);

		this.vpFeedEntries.setDisplay(cellList);
	}

	@Override
	public void clear() {
		cellList.setVisible(false);
		cellList.setPageSize(PAGE_SIZE);
        this.feedInfo.clear();
        this.pnlFeedInfo.setVisible(false);
	}

    @Override
    public void refresh() {
        AtomEntryDataSource.get().refreshDisplays();
    }
}
