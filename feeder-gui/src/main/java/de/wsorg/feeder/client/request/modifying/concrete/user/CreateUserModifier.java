package de.wsorg.feeder.client.request.modifying.concrete.user;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.UserRequest;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class CreateUserModifier extends AbstractUserModifier {

    @Inject
    public CreateUserModifier(final ApplicationRequestFactory factory) {
        super(factory);
    }

    @Override
    protected void processActualCall(final FeederUserProxy userProxy,
                                     final UserRequest userRequest,
                                     final Receiver<Void> responseReceiver) {
        userRequest.create(userProxy).fire(responseReceiver);
    }
}
