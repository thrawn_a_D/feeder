package de.wsorg.feeder.client.ui.widget.common.table;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.TableElement;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import de.wsorg.feeder.client.utils.StringUtils;

import java.util.HashMap;

/**
 *
 *
 *
 */
public class FeederTable extends Composite {
    private static FeederTableUiBinder uiBinder = GWT.create(FeederTableUiBinder.class);
    private Widget thisWidget;

    @UiField TableElement tblContent;
    @UiField HorizontalPanel pnlActions;

    private final StringUtils stringUtils;
    private SafeHtmlBuilder contentHtml;

    private HashMap<String, Widget> actionButtons = new HashMap<String, Widget>();

    interface FeederTableUiBinder extends UiBinder<Widget, FeederTable> {}

    public FeederTable(final StringUtils stringUtils) {
        this.stringUtils = stringUtils;
        this.contentHtml = new SafeHtmlBuilder();

        thisWidget = uiBinder.createAndBindUi(this);
        initWidget(thisWidget);
    }

    public void addRow(final String title, final String value) {
        addRow(title, value, "spec", false);
    }

    public void addActionButton(final String buttonId, final Widget button) {
        if(!actionButtons.containsKey(buttonId)) {
            actionButtons.put(buttonId, button);
            pnlActions.add(button);
        } else {
            pnlActions.clear();
            actionButtons.put(buttonId, button);

            for (String keys : actionButtons.keySet()) {
                Widget buttonToAdd = actionButtons.get(keys);
                pnlActions.add(buttonToAdd);
            }
        }
    }

    private void addRow(String title, String value, String rowStyleClass, boolean escape) {
        if (!stringUtils.isBlank(value)) {
            contentHtml.appendHtmlConstant("<tr>");
            contentHtml.appendHtmlConstant(" <th scope='row' width='100' class='"
                    + rowStyleClass + "'>" + title + "</th>");
            contentHtml.appendHtmlConstant(" <td class=\"tableitem_td\">");
            if (escape) {
                contentHtml.appendEscaped(value);
            } else {
                contentHtml.appendHtmlConstant(value);
            }
            contentHtml.appendHtmlConstant("</td>");
            contentHtml.appendHtmlConstant("</tr>");
            tblContent.setInnerHTML(contentHtml.toSafeHtml().asString());
        }
    }

    public void clear() {
        tblContent.setInnerHTML("");
        contentHtml = new SafeHtmlBuilder();
        pnlActions.clear();
    }
}
