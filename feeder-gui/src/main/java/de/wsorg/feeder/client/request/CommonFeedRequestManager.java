package de.wsorg.feeder.client.request;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.BaseProxy;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.util.RequestContextDomainCreator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class CommonFeedRequestManager<T, P> implements RequestManager<T, P> {
    protected RequestContextDomainCreator domainProxyCreator;

    protected abstract RequestContext getRequestContext();

    @Override
    public <D extends BaseProxy> D getNewDomainObjectForParam(final Class<D> classType) {
        domainProxyCreator.setRequestContext(getRequestContext());
        return domainProxyCreator.getNewDomainObjectForParam(classType);
    }

    @Inject
    public void setDomainProxyCreator(final RequestContextDomainCreator domainProxyCreator) {
        this.domainProxyCreator = domainProxyCreator;
    }
}
