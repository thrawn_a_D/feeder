package de.wsorg.feeder.client.presenter.common.feeddetails;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.FeedRequestManager;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;

public class FeedDetailsExtendedPresenter
		extends
		Presenter<FeedDetailsExtendedPresenter.FeedDetailsExtendedView, FeedDetailsExtendedPresenter.FeedDetailsExtendedProxy> {

	@ProxyCodeSplit
	@NameToken(PlaceNameToken.FEED_DETAILS_EXTENDED)
	public interface FeedDetailsExtendedProxy extends
			ProxyPlace<FeedDetailsExtendedPresenter>, Place {
	}

	public interface FeedDetailsExtendedView extends View {
		void initializeView(AtomEntryProxy atomEntry);
	}

	private final FeedDetailsExtendedView view;
	private final EventBus eventBus;
	private final FeedRequestManager requestManager;

	@Inject
	public FeedDetailsExtendedPresenter(final EventBus eventBus,
			final FeedDetailsExtendedView view,
			final FeedDetailsExtendedProxy proxy,
			final FeedRequestManager requestManager) {
		super(eventBus, view, proxy);
		this.eventBus = eventBus;
		this.view = view;
		this.requestManager = requestManager;
	}
	
	@Override
	public void prepareFromRequest(PlaceRequest placeRequest) {
		super.prepareFromRequest(placeRequest);
		String feedAddress = placeRequest.getParameter("feedAddress", "");
		String guid = placeRequest.getParameter("guid", "");
		//get the comments from param so that we can process the feed and build up
		//the comments async.
		String feedCommetsRss = placeRequest.getParameter("feedCommetsRss", "");
		
		initializeFeedEntryView(feedAddress, guid);
	}

	private void initializeFeedEntryView(String feedAddress, String guid) {
		Receiver<AtomEntryProxy> receiver = new Receiver<AtomEntryProxy>() {
			@Override
			public void onSuccess(AtomEntryProxy response) {
				if (response != null) {
					view.initializeView(response);
				} else {
					// no entries found,!
				}
			}

			@Override
			public void onFailure(ServerFailure error) {
				try {
					super.onFailure(error);
				} catch (Throwable t) {
					// there must be an
					// UnhandledExceptionHandler
					GWT.getUncaughtExceptionHandler().onUncaughtException(t);
				}
			}
		};
		
		this.requestManager.getFeedEntry(feedAddress, guid, receiver);
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this.eventBus,
				MainFeedListContainerPresenter.TYPE_SetFeedView, this);	}
}
