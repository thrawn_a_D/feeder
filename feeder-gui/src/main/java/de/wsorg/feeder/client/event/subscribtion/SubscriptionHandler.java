package de.wsorg.feeder.client.event.subscribtion;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 *
 *
 */
public interface SubscriptionHandler extends EventHandler {
    public void onSubscribeFeed(final String feedUrl);
    public void onCancelSubscription(final String feedUrl);
}
