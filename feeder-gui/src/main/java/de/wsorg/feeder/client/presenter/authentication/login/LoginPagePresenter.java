package de.wsorg.feeder.client.presenter.authentication.login;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.reading.concrete.user.UserLoginRequestManager;
import de.wsorg.feeder.client.security.event.UserLoggedInEvent;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;
import de.wsorg.feeder.shared.domain.user.login.UserLoginRequestProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class LoginPagePresenter extends Presenter<LoginPagePresenter.LoginPageView, LoginPagePresenter.LoginPageProxy> {

    private final EventBus eventBus;
    private final LoginPageView view;
    private final PlaceManager placeManager;
    private final UserLoginRequestManager userLoginRequestManager;
    private final UserSession userSession;

    @Inject
    public LoginPagePresenter(final EventBus eventBus,
                               final LoginPageView view,
                               final LoginPageProxy proxy,
                               final PlaceManager placeManager,
                               final StringUtils stringUtils,
                               final UserLoginRequestManager userLoginRequestManager,
                               final UserSession userSession) {
        super(eventBus, view, proxy);
        this.eventBus = eventBus;
        this.view = view;
        this.placeManager = placeManager;
        this.userLoginRequestManager = userLoginRequestManager;
        this.userSession = userSession;

        final LoginPagePresenter currentPresenter = this;

        registerLoginEventHandler(view, stringUtils, userLoginRequestManager, currentPresenter);
        registerNewAccountEventHandler(view, placeManager);
    }

    @ProxyCodeSplit
    @NameToken(PlaceNameToken.AUTHENTICATION_LOGIN_PAGE)
    public interface LoginPageProxy extends
            ProxyPlace<LoginPagePresenter>, Place {
    }

    public interface LoginPageView extends View {
        public void registerLoginClickHandler(final ClickHandler loginEvent);
        public void registerCreateNewAccountClickHandler(final ClickHandler newAccountEvent);
        public String getUserName();
        public String getPassword();
    }

    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this.eventBus,
                MainFeedListContainerPresenter.TYPE_SetFeedView, this);
    }

    private void registerNewAccountEventHandler(final LoginPageView view, final PlaceManager placeManager) {
        ClickHandler newAccountClickHandler = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                PlaceRequest request = new PlaceRequest(PlaceNameToken.AUTHENTICATION_NEW_ACCOUNT_PAGE);
                History.newItem(placeManager.buildHistoryToken(request));
            }
        };

        view.registerCreateNewAccountClickHandler(newAccountClickHandler);
    }

    private void registerLoginEventHandler(final LoginPageView view, final StringUtils stringUtils, final UserLoginRequestManager userLoginRequestManager, final LoginPagePresenter currentPresenter) {
        ClickHandler loginUser = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                if(stringUtils.isBlank(view.getUserName()))
                    ShowErrorMessageEvent.fire(currentPresenter, "User name is empty. Please provide a valid user name.");
                else if(stringUtils.isBlank(view.getPassword()))
                    ShowErrorMessageEvent.fire(currentPresenter, "Password is empty. Please provide a valid password.");
                else {
                    Receiver<FeederUserProxy> receiver = getDefaultCreationReceiver(currentPresenter);
                    UserLoginRequestProxy loginRequest = userLoginRequestManager.getNewDomainObjectForParam(UserLoginRequestProxy.class);
                    loginRequest.setNickName(view.getUserName());
                    loginRequest.setPassword(view.getPassword());
                    userLoginRequestManager.processRequest(loginRequest, receiver, FeederUserProxy.class);
                }
            }
        };
        view.registerLoginClickHandler(loginUser);
    }

    private Receiver<FeederUserProxy> getDefaultCreationReceiver(final LoginPagePresenter currentPresenter) {
        return new Receiver<FeederUserProxy>() {
            @Override
            public void onSuccess(final FeederUserProxy user) {
                userSession.registerUserInSession(user);
                UserLoggedInEvent.fire(currentPresenter, user);
                PlaceRequest request = new PlaceRequest(PlaceNameToken.MY_FEED_TIME_LINE);
                History.newItem(placeManager.buildHistoryToken(request));
            }

            @Override
            public void onFailure(final ServerFailure error) {
                ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
            }
        };
    }
}
