package de.wsorg.feeder.client.request.modifying.concrete.feed;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.modifying.AbstractFeedModifier;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedReadStatusRequest;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatusProxy;

/**
 *
 *
 *
 */
public class SingleReadStatusModifier extends AbstractFeedModifier<Void, SingleFeedEntryReadStatusProxy> {

    private ApplicationRequestFactory factory;
    private FeedReadStatusRequest requestContext;

    @Inject
    public SingleReadStatusModifier(final ApplicationRequestFactory factory) {
        this.factory = factory;
        this.requestContext = factory.feedReadStatusRequest();
    }

    @Override
    protected RequestContext getRequestContext() {
        return this.requestContext;
    }

    @Override
    protected void processActualModification(final SingleFeedEntryReadStatusProxy params, final Receiver<Void> responseReceiver) {
        requestContext.setFeedReadStatus(params).fire(responseReceiver);
        requestContext = factory.feedReadStatusRequest();
    }
}
