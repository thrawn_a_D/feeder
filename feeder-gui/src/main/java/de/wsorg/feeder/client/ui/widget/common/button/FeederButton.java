package de.wsorg.feeder.client.ui.widget.common.button;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederButton extends Composite {
    private static final ClickHandler DEFAULT_BUTTON_HANDLER = new DoNothingClickHandler();
    private static FeederButtonUiBinder uiBinder = GWT.create(FeederButtonUiBinder.class);
    private final Widget thisWidget;

    interface FeederButtonUiBinder extends UiBinder<Widget, FeederButton> {}

    @UiField Label lblButtonText;
    @UiField AnchorElement aButton;
    @UiField HTMLPanel pnlButton;

    public FeederButton() {
        thisWidget = uiBinder.createAndBindUi(this);
        initWidget(thisWidget);
    }

    public void setText(final String text) {
        this.lblButtonText.setText(text);
    }

    public void addClickHandler(final ClickHandler clickHandler) {
        pnlButton.addDomHandler(clickHandler, ClickEvent.getType());
    }

    private static class FeederClickEvent extends ClickEvent { }
    private static class DoNothingClickHandler implements ClickHandler {
        @Override
        public void onClick(final ClickEvent clickEvent) {

        }
    }
}
