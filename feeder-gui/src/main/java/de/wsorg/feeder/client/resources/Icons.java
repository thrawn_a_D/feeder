package de.wsorg.feeder.client.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface Icons extends ClientBundle {
    
    @Source("de/wsorg/feeder/client/icons/set1/19-gear.png")
    ImageResource Gear();
    @Source("de/wsorg/feeder/client/icons/set1/20-gear2.png")
    ImageResource Gear2();
    @Source("de/wsorg/feeder/client/icons/set1/53-house.png")
    ImageResource House();
    @Source("de/wsorg/feeder/client/icons/set1/33-cabinet.png")
    ImageResource Cabinet();
    @Source("de/wsorg/feeder/client/icons/set1/58-bookmark.png")
    ImageResource Bookmark();
    @Source("de/wsorg/feeder/client/icons/set1/63-runner.png")
    ImageResource Runner();
    @Source("de/wsorg/feeder/client/icons/set1/64-zap.png")
    ImageResource Zap();
    @Source("de/wsorg/feeder/client/icons/set1/61-brightness.png")
    ImageResource Brightness();
    @Source("de/wsorg/feeder/client/icons/set1/06-magnify.png")
    ImageResource Magnify();
    @Source("de/wsorg/feeder/client/icons/set1/164-glasses-2.png")
    ImageResource Glasses();
    @Source("de/wsorg/feeder/client/icons/set1/01-refresh_15x15.png")
    ImageResource Refresh();
    @Source("de/wsorg/feeder/client/icons/set1/40-inbox_15x15.png")
    ImageResource InBox();
    @Source("de/wsorg/feeder/client/icons/set1/106-sliders.png")
    ImageResource Settings();
    @Source("de/wsorg/feeder/client/icons/set1/04-squiggle.png")
    ImageResource Squiggle();
}
