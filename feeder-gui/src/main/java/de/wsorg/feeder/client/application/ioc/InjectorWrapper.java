package de.wsorg.feeder.client.application.ioc;

//A simple interface for our Desktop/Mobile InjectorWrappers to implement
public interface InjectorWrapper {
    AppInjector getInjector();
}
