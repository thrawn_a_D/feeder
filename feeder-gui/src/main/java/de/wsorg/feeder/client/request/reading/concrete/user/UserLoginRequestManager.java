package de.wsorg.feeder.client.request.reading.concrete.user;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.reading.AbstractFeedRequestManager;
import de.wsorg.feeder.client.request.wrapper.MD5JavaScriptWrapper;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.UserRequest;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;
import de.wsorg.feeder.shared.domain.user.login.UserLoginRequestProxy;

/**
 *
 *
 *
 */
public class UserLoginRequestManager extends AbstractFeedRequestManager<FeederUserProxy, UserLoginRequestProxy> {
    private final FeedUserAutoBeanFactory feedUserAutoBeanFactory;
    private UserRequest userRequest;
    private final ApplicationRequestFactory factory;
    protected MD5JavaScriptWrapper md5JavaScriptWrapper;


    public interface FeedUserAutoBeanFactory extends AutoBeanFactory {
        AutoBean<FeederUserProxy> userproxy(FeederUserProxy delegate);
    }

    @Inject
    public UserLoginRequestManager(final FeedUserAutoBeanFactory feedUserAutoBeanFactory,
                                   final ApplicationRequestFactory factory) {
        this.feedUserAutoBeanFactory = feedUserAutoBeanFactory;
        this.userRequest = factory.userRequest();
        this.factory = factory;
    }

    @Override
    protected void executeRequest(final UserLoginRequestProxy requestParams,
                                  final Receiver<FeederUserProxy> receiverOfRelatedCall) {

        final String userPassword = md5JavaScriptWrapper.hashPassword(requestParams.getPassword());
        requestParams.setPassword(userPassword);

        userRequest.loginUser(requestParams).fire(receiverOfRelatedCall);
        userRequest = factory.userRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final UserLoginRequestProxy requestParam) {
        return requestParam.getNickName();
    }

    @Override
    protected AutoBeanFactory getDomainAutoBean() {
        return feedUserAutoBeanFactory;
    }

    @Override
    protected boolean cacheResponse() {
        return false;
    }

    @Override
    protected RequestContext getRequestContext() {
        return userRequest;
    }

    @Inject
    public void setMd5JavaScriptWrapper(final MD5JavaScriptWrapper md5JavaScriptWrapper) {
        this.md5JavaScriptWrapper = md5JavaScriptWrapper;
    }
}
