package de.wsorg.feeder.client.ui.getfeeds;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.http.client.URL;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.getfeeds.FoundFeedListPresenter;
import de.wsorg.feeder.client.ui.widget.common.feedlist.FeedListViewImpl;
import de.wsorg.feeder.client.ui.widget.common.textbox.ValidatedTextBox;
import de.wsorg.feeder.client.ui.widget.common.textbox.validator.UrlValidator;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 *
 *
 *
 */
public class FoundFeedListViewImpl extends ViewImpl implements FoundFeedListPresenter.FoundFeedListView {

    private Widget widget;
    private final PlaceManager placeManager;

    @UiField Image imgFindFeeds;
    @UiField HorizontalPanel hpSearchOption;
    @UiField RadioButton rbRssQuery;
    @UiField RadioButton rbImportFromRss;
    @UiField RadioButton rbTwitterSearch;
    @UiField RadioButton rbYoutube;
    @UiField ValidatedTextBox tbQuery;

    @UiField FeedListViewImpl feedList;
    private UrlValidator urlValidator;

    public interface Binder extends UiBinder<Widget, FoundFeedListViewImpl> {
    }

    @Inject
    public FoundFeedListViewImpl(final Binder uiBinder,
                                  final PlaceManager placeManager,
                                  final FeedListViewImpl feedList) {
        super();
        this.placeManager = placeManager;
        this.feedList = feedList;
        widget = uiBinder.createAndBindUi(this);
        urlValidator = new UrlValidator();
    }

    @UiFactory
    public FeedListViewImpl makeFeedListViewImpl(){
        return this.feedList;
    }

    private void searchForQuery(){
        if(shouldWeSearchUsingAQuery()){
            searchUsingAQuery();
        } else if(shouldWeImportFromUrl()){
            importFromUrl();
        } else if(shouldWeSearchYouTube()) {
            searchYouTube();
        } else if(shouldWeSearchTwitter()) {
            searchTwitter();
        }
    }

    @UiHandler("imgFindFeeds")
    void onImgFindFeedsClick(ClickEvent event) {
        this.searchForQuery();
    }

    @UiHandler("tbQuery")
    void onTbQueryKeyUp(KeyUpEvent event) {
        if( event.getNativeKeyCode() == KeyCodes.KEY_ENTER){
            this.searchForQuery();
        }
    }

    @UiHandler({"rbRssQuery", "rbTwitterSearch", "rbYoutube"})
    void onHpSearchOptionClicked(ClickEvent event) {
        this.tbQuery.clearValidator();
        this.tbQuery.clearValidationErrors(true);
    }

    @UiHandler("rbImportFromRss")
    void onRbImportFromRss(ClickEvent event) {
        this.tbQuery.addValidator(urlValidator);
    }

    private Boolean shouldWeSearchUsingAQuery() {
        return this.rbRssQuery.getValue();
    }

    private void searchUsingAQuery() {
        PlaceRequest request = new PlaceRequest(PlaceNameToken.FOUND_FEED_LIST).with("query", this.tbQuery.getText());
        History.newItem(this.placeManager.buildHistoryToken(request));
    }

    private Boolean shouldWeImportFromUrl() {
        return this.rbImportFromRss.getValue();
    }

    private void importFromUrl() {
        PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS);
        request = request.with("feedAddress",this.tbQuery.getText());
        History.newItem(this.placeManager.buildHistoryToken(request));
    }

    private Boolean shouldWeSearchYouTube() {
        return this.rbYoutube.getValue();
    }

    @Override
    public Widget asWidget() {
        return this.widget;
    }

    private void searchYouTube() {
        PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS);
        final String youTubeQuery = this.tbQuery.getText();
        request = request.with("feedAddress","http://gdata.youtube.com/feeds/api/videos?alt=rss&orderby=updated&q=" + URL.encodeQueryString(youTubeQuery));
        History.newItem(this.placeManager.buildHistoryToken(request));
    }

    private Boolean shouldWeSearchTwitter() {
        return this.rbTwitterSearch.getValue();
    }

    private void searchTwitter() {
        PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS);
        final String text = this.tbQuery.getText().replace(" ", "");
        request = request.with("feedAddress","http://api.twitter.com/1/statuses/user_timeline.rss?screen_name=" + text);
        History.newItem(this.placeManager.buildHistoryToken(request));
    }


    @Override
    public void setFeedToList(final OpmlWrapper atomResult) {
        feedList.setFeedToList(atomResult);
    }

    @Override
    public void clear() {
        feedList.clear();
    }

    @Override
    public void showNoResultsFoundMessage() {
        feedList.showNoResultsFoundMessage();
    }
}
