package de.wsorg.feeder.client.ui;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.presenter.MainPagePresenter;
import de.wsorg.feeder.client.presenter.MainPagePresenter.MainPageView;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.ui.widget.MainMenu;
import de.wsorg.feeder.client.ui.widget.common.message.FeederMessagePopUp;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

public class MainPageViewImpl extends ViewImpl implements MainPageView {
    private final UserSession userSession;

    public interface MainPageViewUiBinder extends UiBinder<Widget, MainPageViewImpl> {}

    public Widget widget;
    
    @UiField FlowPanel mainContentPanel;
    @UiField FeederMessagePopUp msgPopUp;

    private MainMenu mainMenu;
    
    @Inject
    public MainPageViewImpl(final MainPageViewUiBinder uiBinder,
                            final UserSession userSession) {
        this.userSession = userSession;
        this.widget = uiBinder.createAndBindUi(this);
    }
    
    @UiFactory
    public MainMenu makeMainManu(){
        this.mainMenu = new MainMenu(userSession);
        return this.mainMenu;
    }

    @Override
    public Widget asWidget() {
      return this.widget;
    }

    @Override
    public void setInSlot(Object slot, Widget content) {
      if (slot == MainPagePresenter.TYPE_SetMainContent) {
        setMainContent(content);
      } else {
        super.setInSlot(slot, content);
      }
    }

    @Override
    public void showErrorMessage(final String errorMessage, final boolean stayVisible) {
        msgPopUp.showErrorMessage(errorMessage, stayVisible);
    }

    @Override
    public void showInfoMessage(final String infoMessage, final boolean stayVisible) {
        msgPopUp.showInfoMessage(infoMessage, stayVisible);
    }

    @Override
    public void enableMenuForUser(final FeederUserProxy user) {
        mainMenu.enableUserMenuForUser(user);
    }


    private void setMainContent(Widget content) {
      mainContentPanel.clear();
      
      if (content != null) {
        mainContentPanel.add(content);
      }
    }
  }