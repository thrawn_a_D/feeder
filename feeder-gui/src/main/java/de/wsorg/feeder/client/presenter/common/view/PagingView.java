package de.wsorg.feeder.client.presenter.common.view;

import com.google.gwt.event.dom.client.ClickHandler;
import com.gwtplatform.mvp.client.View;

/**
 *
 *
 *
 */
public interface PagingView extends View {
    void setPagingTimeLineEvent(ClickHandler scrollDown);
    void disablePagingButton();
    void enablePagingButton();
}
