package de.wsorg.feeder.client.presenter.timeline;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.event.readstatus.all.AllEntriesReadStatusHandler;
import de.wsorg.feeder.client.event.readstatus.multyentry.MultiEntryReadStatusHandler;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.presenter.common.view.PagingView;
import de.wsorg.feeder.client.request.reading.concrete.feed.TimeLineRequestManager;
import de.wsorg.feeder.client.security.LogInGateKeeper;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedForUserRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.load.PagingRangeProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class MyFeedTimeLinePresenter extends Presenter<MyFeedTimeLinePresenter.MyFeedTimeLineView, MyFeedTimeLinePresenter.MyFeedTimeLineProxy> {

    private final EventBus eventBus;
    private final MyFeedTimeLineView view;
    private final ApplicationRequestFactory factory;
    private final TimeLineRequestManager requestManager;
    private final MultiEntryReadStatusHandler multiEntryReadStatusHandler;

    private int startPageCount = 1;
    private int endPageCount = 10;

    public static final int PAGING_INCREMENT = 10;

    @Inject
    public MyFeedTimeLinePresenter(final EventBus eventBus,
                                   final MyFeedTimeLineView view,
                                   final MyFeedTimeLineProxy proxy,
                                   final ApplicationRequestFactory factory,
                                   final TimeLineRequestManager requestManager,
                                   final MultiEntryReadStatusHandler multiEntryReadStatusHandler,
                                   final AllEntriesReadStatusHandler allEntriesReadStatusHandler) {
        super(eventBus, view, proxy);
        this.eventBus = eventBus;
        this.view = view;
        this.factory = factory;
        this.requestManager = requestManager;
        this.multiEntryReadStatusHandler = multiEntryReadStatusHandler;

        setPagingEventHandler(view);
        setMultiReadHandler(multiEntryReadStatusHandler);
        setTimeLineRefreshHandler(view);
        setAllReadStatusHandler(view, allEntriesReadStatusHandler);
    }

    @ProxyCodeSplit
    @NameToken(PlaceNameToken.MY_FEED_TIME_LINE)
    @UseGatekeeper(LogInGateKeeper.class)
    public interface MyFeedTimeLineProxy extends
            ProxyPlace<MyFeedTimeLinePresenter>, Place {
    }

    public interface MyFeedTimeLineView extends PagingView {
        void clear();
        void showLoadingInfo();
        void showNoEntriesFound();
        void scrollToTop();
        void setAtom(final AtomWrapper atom);
        void setFeedEntryList(List<AtomEntryProxy> feedEntries);
        List<AtomEntryProxy> getFeedEntryList();
        void setMarkVisibleAsReadHandler(ClickHandler handler);
        void setMarkAllAsReadHandler(ClickHandler handler);
        void setTimeLineRefreshEvent(ClickHandler handler);
        boolean isShowOnlyUnreadEntries();
    }

    @Override
    public void prepareFromRequest(final PlaceRequest request) {
        super.prepareFromRequest(request);
        String feedEntryCountToShow = request.getParameter("showEntriesCount", "11");

        endPageCount = 10;

        view.clear();
        view.showLoadingInfo();

        loadTimeLine(feedEntryCountToShow);
    }

    private void loadTimeLine(final String feedEntryCountToShow) {
        Integer endItemCount = Integer.valueOf(feedEntryCountToShow);

        fireTimeLineLoadRequest(endItemCount, new DefaultFeedReceiver(this));
    }

    private void fireTimeLineLoadRequest(final Integer endItemCount, final Receiver<AtomWrapper> receiver) {
        fireTimeLineLoadRequest(endItemCount, receiver, view.isShowOnlyUnreadEntries());
    }

    private void fireTimeLineLoadRequest(final Integer endItemCount, final Receiver<AtomWrapper> receiver, final boolean showOnlyUnreadEntries) {
        LoadFeedForUserRequestProxy loadFeedProxy = getCallParameter(endItemCount, showOnlyUnreadEntries);
        this.requestManager.processRequest(loadFeedProxy, receiver, AtomWrapper.class);
    }

    private void setPagingEventHandler(final MyFeedTimeLineView view) {
        final DefaultFeedReceiver receiver = new DefaultFeedReceiver(this);
        ClickHandler showMoreTimeLineItems = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                endPageCount += PAGING_INCREMENT;
                fireTimeLineLoadRequest(endPageCount, receiver, false);
            }
        };

        view.setPagingTimeLineEvent(showMoreTimeLineItems);
    }

    private LoadFeedForUserRequestProxy getCallParameter(final Integer endItemCount, final boolean showOnlyUnreadEntries) {

        LoadFeedForUserRequestProxy loadFeedProxy = this.requestManager.getNewDomainObjectForParam(LoadFeedForUserRequestProxy.class);
        PagingRangeProxy pagingRangeProxy = this.requestManager.getNewDomainObjectForParam(PagingRangeProxy.class);
        pagingRangeProxy.setStartIndex(startPageCount);
        pagingRangeProxy.setEndIndex(endItemCount);
        loadFeedProxy.setFeedUrl("http://feeder.net/timeline");
        loadFeedProxy.setFeedEntryPagingRange(pagingRangeProxy);
        loadFeedProxy.setShowOnlyUnreadEntries(showOnlyUnreadEntries);
        return loadFeedProxy;
    }

    private void setTimeLineRefreshHandler(final MyFeedTimeLineView view) {
        ClickHandler refreshHandler = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                refreshTimeLineList(view);
            }
        };
        view.setTimeLineRefreshEvent(refreshHandler);
    }

    private void refreshTimeLineList(final MyFeedTimeLineView view) {
        loadTimeLine("11");
        view.scrollToTop();
    }

    private void setMultiReadHandler(final MultiEntryReadStatusHandler multiEntryReadStatusHandler) {
        ClickHandler onEntriesReadMark = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                List<AtomEntryProxy> feedEntryList = view.getFeedEntryList();
                List<AtomEntryProxy> editedEntryList = new ArrayList<AtomEntryProxy>();

                for (AtomEntryProxy atomEntryProxy : feedEntryList) {
                    AtomEntryProxy editProxy = factory.feedRequest().edit(atomEntryProxy);
                    editProxy.setEntryAlreadyRead(true);
                    editedEntryList.add(editProxy);
                }
                view.setFeedEntryList(editedEntryList);
            }
        };
        multiEntryReadStatusHandler.addHookEvent(onEntriesReadMark);
        this.view.setMarkVisibleAsReadHandler(multiEntryReadStatusHandler);
    }

    private void setAllReadStatusHandler(final MyFeedTimeLineView view,
                                         final AllEntriesReadStatusHandler allEntriesReadStatusHandler) {
        view.setMarkAllAsReadHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                allEntriesReadStatusHandler.markAllFeedsAsRead();
            }
        });
        allEntriesReadStatusHandler.setEventReceiver(new Receiver<Void>() {
            @Override
            public void onSuccess(final Void response) {
                refreshTimeLineList(view);
            }
        });
    }

    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this.eventBus,
                MainFeedListContainerPresenter.TYPE_SetFeedView, this);
    }

    private class DefaultFeedReceiver extends Receiver<AtomWrapper> {

        private final MyFeedTimeLinePresenter presenter;

        private DefaultFeedReceiver(final MyFeedTimeLinePresenter presenter) {
            this.presenter = presenter;
        }

        @Override
        public void onSuccess(AtomWrapper response) {
            if (response != null) {
                if (response.getFeedEntries() != null
                        && response.getFeedEntries().size() > 0) {
                    view.setAtom(response);
                    view.setFeedEntryList(response.getFeedEntries());
                    multiEntryReadStatusHandler.setFeedEntryList(response.getFeedEntries());

                    if(response.isAdditionalPagingPossible()) {
                        view.enablePagingButton();
                    } else {
                        view.disablePagingButton();
                    }
                } else {
                    view.showNoEntriesFound();
                }
            }
        }

        @Override
        public void onFailure(ServerFailure error) {
            ShowErrorMessageEvent.fire(presenter, error.getMessage());
        }
    }
}
