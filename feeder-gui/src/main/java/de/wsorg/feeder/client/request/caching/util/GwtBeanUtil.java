package de.wsorg.feeder.client.request.caching.util;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;

/**
 *
 *
 *
 */
public class GwtBeanUtil<T> {
    public AutoBean<T> getAutoBean(final T objectForAutoBean) {
        return AutoBeanUtils.getAutoBean(objectForAutoBean);
    }

    public String encodeAutoBeanToJson(final AutoBean<T> autoBeanToEncode) {
        return AutoBeanCodex.encode(autoBeanToEncode).getPayload();
    }

    public AutoBean<T> decodeJsonToAutoBean(final AutoBeanFactory beanFactory, final String jsonString, final Class<T> type) {
        return AutoBeanCodex.decode(beanFactory, type, jsonString);
    }
}
