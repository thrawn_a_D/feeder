package de.wsorg.feeder.client.ui.myfeeds;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.presenter.myfeeds.MyFeedsListPresenter;
import de.wsorg.feeder.client.ui.cell.FeedListItem;
import de.wsorg.feeder.client.ui.widget.common.feedlist.FeedListViewImpl;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 *
 *
 *
 */
public class MyFeedListViewImpl extends ViewImpl implements MyFeedsListPresenter.MyFeedListView {

    private Widget widget;
    private final PlaceManager placeManager;
    private final FeedListItem cellItem;

    FeedListViewImpl feedListViewImpl;

    public interface Binder extends UiBinder<Widget, MyFeedListViewImpl> {
    }

    @Inject
    public MyFeedListViewImpl(final Binder uiBinder,
                                 final PlaceManager placeManager,
                                 final FeedListItem cellItem) {
        super();
        this.placeManager = placeManager;
        this.cellItem = cellItem;
        widget = uiBinder.createAndBindUi(this);
    }



    @UiFactory
    public FeedListViewImpl makeFeedListViewImpl(){
        this.feedListViewImpl = new FeedListViewImpl(this.cellItem);
        return this.feedListViewImpl;
    }

    @Override
    public Widget asWidget() {
        return this.widget;
    }

    @Override
    public void setFeedToList(final OpmlWrapper atomResult) {
        feedListViewImpl.setFeedToList(atomResult);
    }

    @Override
    public void clear() {
        feedListViewImpl.clear();
    }

    @Override
    public void showNoResultsFoundMessage() {
        feedListViewImpl.showNoResultsFoundMessage();
    }
}
