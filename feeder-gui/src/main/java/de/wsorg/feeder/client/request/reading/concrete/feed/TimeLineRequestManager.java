package de.wsorg.feeder.client.request.reading.concrete.feed;

import com.google.inject.Inject;
import de.wsorg.feeder.client.request.reading.concrete.feed.load.LoadAtomForUserRequestManager;
import de.wsorg.feeder.shared.ApplicationRequestFactory;

/**
 *
 *
 *
 */
public class TimeLineRequestManager extends LoadAtomForUserRequestManager {

    @Inject
    public TimeLineRequestManager(final ApplicationRequestFactory factory,
                                  final AtomAutoBeanFactory atomAutoBeanFactory) {
        super(factory, atomAutoBeanFactory);
    }

    @Override
    protected boolean cacheResponse() {
        return false;
    }
}
