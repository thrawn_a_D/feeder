package de.wsorg.feeder.client.event.readstatus.singleentry;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 *
 *
 */
public interface SingleReadStatusHandler extends EventHandler {
    public void markFeedEntryAsRead(final String feedUrl, final String feedEntryId);
    public void markFeedEntryAsUnread(final String feedUrl, final String feedEntryId);
}
