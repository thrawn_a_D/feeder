package de.wsorg.feeder.client.request.modifying.concrete.user.domain;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ClientUserData {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    protected String userName;
    protected ClientUserPassword clientUserPassword;
    protected String eMail;
    protected String firstName;
    protected String lastName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public ClientUserPassword getPassword() {
        return clientUserPassword;
    }

    public void setPassword(final ClientUserPassword password) {
        this.clientUserPassword = password;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(final String eMail) {
        this.eMail = eMail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public void validateMandatoryFields() {
        if(userName.isEmpty())
            throw new IllegalStateException("User name is a mandatory field and has to be set.");
        if(clientUserPassword == null || clientUserPassword.getPasswordText().isEmpty())
            throw new IllegalStateException("Password is a mandatory field and has to be set.");
        if(eMail.isEmpty())
            throw new IllegalStateException("E-Mail is a mandatory field and has to be set.");
        if(!eMail.matches(EMAIL_PATTERN))
            throw new IllegalStateException("The provided E-Mail address is invalid. Please provide a correct mail address.");
    }
}
