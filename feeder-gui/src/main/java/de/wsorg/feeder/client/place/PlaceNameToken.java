package de.wsorg.feeder.client.place;

public class PlaceNameToken {
    /**
     * Use fields for anotations.
     */
    public static final String HOME_PAGE = "!homePage";
    public static final String MY_FEEDS = "!myFeeds";
    public static final String MY_FEED_TIME_LINE = "!myFeedTimeLine";
    public static final String FEED_DETAILS = "!feedItemDetails";
    public static final String FEED_DETAILS_EXTENDED = "!feedItemDetailsExtended";
    public static final String FOUND_FEED_LIST = "!findFeeds";

    public static final String AUTHENTICATION_LOGIN_PAGE = "!loginPage";
    public static final String AUTHENTICATION_NEW_ACCOUNT_PAGE = "!newAccountPage";

    public static final String USER_SETTINGS_OVERVIEW = "!userSettingsOverview";

    /**
     * Use method for UIBinder
     * @return
     */
    public static String getHomePage() {
        return HOME_PAGE;
    }
    public static String getMyFeedsPage() {
        return MY_FEEDS;
    }
    public static String getMyFeedTimeLine() {
        return MY_FEED_TIME_LINE;
    }
    public static String getFeedItemDetailsPage(){
    	return FEED_DETAILS;
    }
    public static String getFindFeedsPage(){
    	return FOUND_FEED_LIST;
    }
	public static String getFeedDetailsExtended() {
		return FEED_DETAILS_EXTENDED;
	}


    public static String getAuthenticationLoginPage() {
        return AUTHENTICATION_LOGIN_PAGE;
    }

    public static String getAuthenticationNewAccountPage() {
        return AUTHENTICATION_NEW_ACCOUNT_PAGE;
    }

    public static String getUserSettingsOverviewPage() {
        return USER_SETTINGS_OVERVIEW;
    }
}
