package de.wsorg.feeder.client.request.reading.concrete.feed.load;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.reading.AbstractFeedRequestManager;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedRequest;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.load.PagingRangeProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

/**
 *
 *
 *
 */
public class LoadAtomRequestManager<T extends LoadFeedRequestProxy> extends AbstractFeedRequestManager<AtomWrapper, T> {
    private final AtomAutoBeanFactory atomAutoBeanFactory;
    protected FeedRequest feedRequest;
    protected final ApplicationRequestFactory factory;


    public interface AtomAutoBeanFactory extends AutoBeanFactory {
        AutoBean<AtomWrapper> feedproxy(AtomWrapper delegate);
    }

    @Inject
    public LoadAtomRequestManager(final ApplicationRequestFactory factory, final AtomAutoBeanFactory atomAutoBeanFactory) {
        this.factory = factory;
        feedRequest = factory.feedRequest();
        this.atomAutoBeanFactory = atomAutoBeanFactory;
    }

    @Override
    protected void executeRequest(final T requestParams, final Receiver<AtomWrapper> receiverOfRelatedCall) {
        feedRequest.getFeed(requestParams).fire(receiverOfRelatedCall);
        feedRequest = factory.feedRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final T requestParam) {
        final String feedUrl = requestParam.getFeedUrl();
        final PagingRangeProxy feedEntryPagingRange = requestParam.getFeedEntryPagingRange();
        if (feedEntryPagingRange != null) {
            final int startIndex = feedEntryPagingRange.getStartIndex();
            final int endIndex = feedEntryPagingRange.getEndIndex();

            return feedUrl + "_" + startIndex + "_" + endIndex;
        } else {
            return feedUrl;
        }
    }

    @Override
    protected AutoBeanFactory getDomainAutoBean() {
        return atomAutoBeanFactory;
    }

    @Override
    protected boolean cacheResponse() {
        return true;
    }

    @Override
    protected RequestContext getRequestContext() {
        return feedRequest;
    }
}
