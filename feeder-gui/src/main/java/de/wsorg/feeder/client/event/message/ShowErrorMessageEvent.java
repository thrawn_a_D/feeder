package de.wsorg.feeder.client.event.message;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ShowErrorMessageEvent extends GwtEvent<ShowErrorMessageEvent.ShowErrorMessageHandler> {

    private boolean stayVisible;

    public interface ShowErrorMessageHandler extends EventHandler {
        void onShowMessage(ShowErrorMessageEvent event);
    }

    private static Type<ShowErrorMessageHandler> TYPE = new Type<ShowErrorMessageHandler>();

    public static void fire(HasHandlers source, String message, boolean stayVisible) {
        if (TYPE != null) {
            source.fireEvent(new ShowErrorMessageEvent(message, stayVisible));
        }
    }

    public static void fire(HasHandlers source, String message) {
        if (TYPE != null) {
            source.fireEvent(new ShowErrorMessageEvent(message, true));
        }
    }

    public static Type<ShowErrorMessageHandler> getType() {
        return TYPE;
    }

    private final String message;

    public ShowErrorMessageEvent(final String message, final boolean stayVisible) {
        this.message = message;
        this.stayVisible = stayVisible;
    }

    @Override
    public Type<ShowErrorMessageHandler> getAssociatedType() {
        return TYPE;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStayVisible() {
        return stayVisible;
    }

    @Override
    protected void dispatch(ShowErrorMessageHandler handler) {
        handler.onShowMessage(this);
    }
}
