package de.wsorg.feeder.client.application.ioc;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.inject.client.GinModules;
import com.gwtplatform.mvp.client.proxy.PlaceManager;

import de.wsorg.feeder.client.application.DesktopApp;

@GinModules(value = { AppInjectorModule.class })
public interface DesktopInjector extends AppInjector {
    // notice we are returning a "DesktopApp" which extends AcrintaSeminarApp
    // This will tell Gin to create a DesktopApp instead of a MobileApp
    DesktopApp getFeederApp();    

    EventBus getEventBus();
    PlaceManager getPlaceManager();
}