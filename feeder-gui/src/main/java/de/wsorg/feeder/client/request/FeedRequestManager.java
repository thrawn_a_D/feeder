package de.wsorg.feeder.client.request;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import de.wsorg.feeder.client.request.caching.ClientCache;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedRequest;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

import java.util.HashMap;


public class FeedRequestManager {
	
	private final ApplicationRequestFactory factory;
	private HashMap<String, String> cache = new HashMap<String, String>();
	private final ClientCache<AtomWrapper> atomBeanFactory;
	private final ClientCache<OpmlWrapper> opmlClientCache;
	
	@Inject
	public FeedRequestManager(final ApplicationRequestFactory factory,
                              final ClientCache<AtomWrapper> atomBeanFactory,
                              final ClientCache<OpmlWrapper> opmlClientCache){
		this.factory = factory;
        this.atomBeanFactory = atomBeanFactory;
        this.opmlClientCache = opmlClientCache;
	}
	
	public void findFeed(final String query, final Receiver<OpmlWrapper> receiver){
		final FeedRequest feedRequest = this.factory.feedRequest();
		GlobalSearchQueryProxy queryProxy = feedRequest.create(GlobalSearchQueryProxy.class);
		
		if(this.cache.containsKey(query)){
            OpmlWrapper result = opmlClientCache.getObjectFromCache(query, OpmlWrapper.class, null);
			receiver.onSuccess(result);
		} else {
		
			queryProxy.setSearchTerm(query);
			
			feedRequest.findGlobalFeed(queryProxy).fire(new Receiver<OpmlProxy>() {
				@Override
				public void onSuccess(OpmlProxy response) {
                    opmlClientCache.storeObjectInCache(query, response);
					cache.put(query, "1");
					receiver.onSuccess(response);
				}
	
				@Override
				public void onFailure(ServerFailure error) {
					receiver.onFailure(error);
				}
			});
		}
	}

	public void getFeed(final String feedAddress, final Receiver<AtomWrapper> receiver) {
		final FeedRequest feedRequest = this.factory.feedRequest();
		
		if(this.cache.containsKey(feedAddress)){
            AtomWrapper result = atomBeanFactory.getObjectFromCache(feedAddress, AtomWrapper.class, null);
			receiver.onSuccess(result);
		} else {
		
//			feedRequest.getFeed(feedAddress).with("feedEntries").fire(new Receiver<AtomProxy>() {
//				@Override
//				public void onSuccess(AtomProxy response) {
//                    atomBeanFactory.storeObjectInCache(feedAddress, response);
//					cache.put(feedAddress, "1");
//					receiver.onSuccess(response);
//				}
//
//				@Override
//				public void onFailure(ServerFailure error) {
//					receiver.onFailure(error);
//				}
//			});
		}
	}

	public void getFeedEntry(final String feedAddress, final String guid,
			final Receiver<AtomEntryProxy> receiver) {
		
		Receiver<AtomWrapper> innerReceiver = new Receiver<AtomWrapper>() {
			@Override
			public void onSuccess(AtomWrapper response) {
				if (response != null) {
					boolean found = false;
					for (AtomEntryProxy currentFeedEntry : response.getFeedEntries()) {
						if(currentFeedEntry.getId().equals(guid)){
							found = true;
							receiver.onSuccess(currentFeedEntry); 
						}
					}
					if(!found){
						receiver.onSuccess(null);
					}
				}
			}

			@Override
			public void onFailure(ServerFailure error) {
				try {
					super.onFailure(error);
				} catch (Throwable t) {
					// there must be an
					// UnhandledExceptionHandler
					GWT.getUncaughtExceptionHandler().onUncaughtException(t);
				}
			}
		};
		
		this.getFeed(feedAddress, innerReceiver);
	}
}
