package de.wsorg.feeder.client.ui.cell;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.dom.client.Element;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import de.wsorg.feeder.client.utils.StringUtils;

public abstract class AbstractFeederCell<T> extends AbstractCell<T> {

    private final StringUtils stringUtils;

	protected AbstractFeederCell(String[] acceptableEvents, final StringUtils stringUtils){
		super(acceptableEvents);
        this.stringUtils = stringUtils;
    }
	
	@Override
	public void render(com.google.gwt.cell.client.Cell.Context context,
			T value, SafeHtmlBuilder sb) {
		
	}
	protected void setTitle(final String title, final String feedUrl, final boolean entryRead, SafeHtmlBuilder sb) {
		if (!stringUtils.isBlank(title)) {
			sb.appendHtmlConstant("<tr>");
            addTitleHeader(entryRead, sb);
            sb.appendHtmlConstant("Title");
            sb.appendHtmlConstant("</th>");
            setTitleValue(entryRead, sb);
            String titleValue = title + "<div style=\"font-size:0.7em; margin-top:5px;\"><a href=\""+feedUrl+"\" style=\"color: #676767;\" target=\"_blank\">"+feedUrl+"</a></div>";
			sb.appendHtmlConstant(titleValue);
			sb.appendHtmlConstant("</th>");
			sb.appendHtmlConstant("</tr>");
		}
	}

    protected void setTitle(final String title, final String articleUrl, final String feedUrl, final boolean entryRead, SafeHtmlBuilder sb) {
        final String titleTag = "<a href=\"" + articleUrl + "\" target=\"_blank\" >" + title + "</a>";
        setTitle(titleTag, feedUrl, entryRead, sb);
    }

    private void setTitleValue(final boolean entryRead, final SafeHtmlBuilder sb) {
        String readStyleValueColumn = "";
        if(entryRead)
            readStyleValueColumn = "class='unreadEntryGradient'";
        sb.appendHtmlConstant("<th id='thTitleValue' scope='col' "+readStyleValueColumn+">");
    }

    private void addTitleHeader(final boolean entryRead, final SafeHtmlBuilder sb) {
        String readStyleTitleColumn = "nobg";
        if(entryRead)
            readStyleTitleColumn = "unreadSpec";
        sb.appendHtmlConstant("<th id='thTitleText' scope='col' width='50' abbr='Title' class='"+readStyleTitleColumn+"'>");
    }

    protected void setTwoColumnEntryInTable(final String title,
                                            final String value,
                                            final String rowStyleClass,
                                            final SafeHtmlBuilder sb,
                                            final boolean escape) {
		if (!stringUtils.isBlank(value)) {
            try {
                sb.appendHtmlConstant("<tr>");
                sb.appendHtmlConstant(" <th scope='row' width='50' class='" + rowStyleClass + "'>" + title + "</th>");
                sb.appendHtmlConstant(" <td class=\"tableitem_td\">");
                if (escape) {
                    sb.appendEscaped(value);
                } else {
                    sb.appendHtmlConstant(value);
                }
                sb.appendHtmlConstant("</td>");
                sb.appendHtmlConstant("</tr>");
            } catch (Exception e) {
                //Ignore these HTML errors if some occur
                e.printStackTrace();
            }
        }
	}

    protected boolean isButtonActive(final Element childElement) {
        return childElement.getClassName().contains("white_keep_active");
    }
}
