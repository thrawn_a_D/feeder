package de.wsorg.feeder.client.presenter.authentication.newaccount;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.event.message.ShowInfoMessageEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.modifying.concrete.user.CreateUserModifier;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData;


/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class NewAccountPagePresenter extends Presenter<NewAccountPagePresenter.NewAccountPageView, NewAccountPagePresenter.NewAccountPageProxy> {

    private final EventBus eventBus;
    private final NewAccountPageView view;
    private final CreateUserModifier createUserModifier;
    private final PlaceManager placeManager;

    @Inject
    public NewAccountPagePresenter(final EventBus eventBus,
                                    final NewAccountPageView view,
                                    final NewAccountPageProxy proxy,
                                    final CreateUserModifier createUserModifier,
                                    final PlaceManager placeManager) {
        super(eventBus, view, proxy);
        this.eventBus = eventBus;
        this.view = view;
        this.createUserModifier = createUserModifier;
        this.placeManager = placeManager;

        registerEventHandler();
    }

    @ProxyCodeSplit
    @NameToken(PlaceNameToken.AUTHENTICATION_NEW_ACCOUNT_PAGE)
    public interface NewAccountPageProxy extends
            ProxyPlace<NewAccountPagePresenter>, Place {

    }

    public interface NewAccountPageView extends View {
        public void registerCreateAccountClickHandler(final ClickHandler newAccountEvent);
        public void registerLoginClickHandler(final ClickHandler loginEvent);
        public ClientUserData getUserAccountData();
    }

    @Override
    public void prepareFromRequest(final PlaceRequest request) {
        super.prepareFromRequest(request);
    }

    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this.eventBus,
                MainFeedListContainerPresenter.TYPE_SetFeedView, this);
    }

    private void registerEventHandler() {
        final NewAccountPagePresenter currentPresenter = this;

        ClickHandler loginClickHandler = getLoginClickHandler();
        view.registerLoginClickHandler(loginClickHandler);

        ClickHandler newAccount = getNewAccountClickHandler(view, currentPresenter);
        view.registerCreateAccountClickHandler(newAccount);
    }

    private ClickHandler getLoginClickHandler() {
        return new ClickHandler() {
                @Override
                public void onClick(final ClickEvent clickEvent) {
                    PlaceRequest request = new PlaceRequest(PlaceNameToken.AUTHENTICATION_LOGIN_PAGE);
                    History.newItem(placeManager.buildHistoryToken(request));
                }
            };
    }

    private ClickHandler getNewAccountClickHandler(final NewAccountPageView view, final NewAccountPagePresenter currentPresenter) {
        return new ClickHandler() {
                @Override
                public void onClick(final ClickEvent clickEvent) {
                    try {
                        ClientUserData userData = view.getUserAccountData();
                        userData.validateMandatoryFields();
                        createUserModifier.processRequest(userData, getDefaultCreationReceiver(currentPresenter, view), Void.class);
                    } catch (Exception e) {
                        ShowErrorMessageEvent.fire(currentPresenter, e.getMessage());
                    }
                }
            };
    }

    private Receiver<Void> getDefaultCreationReceiver(final NewAccountPagePresenter currentPresenter,
                                                      final NewAccountPageView view) {
        return new Receiver<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {
                ShowInfoMessageEvent.fire(currentPresenter, "User successfully created! You're now ready to log in.");
            }

            @Override
            public void onFailure(final ServerFailure error) {
                ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
            }
        };
    }
}
