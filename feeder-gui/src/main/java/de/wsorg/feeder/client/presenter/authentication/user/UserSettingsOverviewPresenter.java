package de.wsorg.feeder.client.presenter.authentication.user;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.event.message.ShowInfoMessageEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.modifying.concrete.user.UpdateUserModifier;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserSettingsOverviewPresenter extends Presenter<UserSettingsOverviewPresenter.UserManagementOverviewView, UserSettingsOverviewPresenter.UserManagementOverviewProxy> {

    private final EventBus eventBus;
    private final UserManagementOverviewView view;
    private final UserSession userSession;
    private final UpdateUserModifier updateUserModifier;
    private final PlaceManager placeManager;

    @Inject
    public UserSettingsOverviewPresenter(final EventBus eventBus,
                                         final UserManagementOverviewView view,
                                         final UserManagementOverviewProxy proxy,
                                         final UserSession userSession,
                                         final UpdateUserModifier updateUserModifier,
                                         final PlaceManager placeManager) {
        super(eventBus, view, proxy);
        this.eventBus = eventBus;
        this.view = view;
        this.userSession = userSession;
        this.updateUserModifier = updateUserModifier;
        this.placeManager = placeManager;

        registerCancelHandler(view, placeManager);
        registerSaveHandler(view, updateUserModifier);
    }

    @Override
    protected void onReveal() {
        super.onReveal();

        view.setUserData(userSession.getCurrentUser());
    }

    private void registerSaveHandler(final UserManagementOverviewView view, final UpdateUserModifier updateUserModifier) {
        final UserSettingsOverviewPresenter currentPresenter = this;
        view.registerSaveClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                try {
                    ClientUserData userData = view.getUserAccountData();
                    userData.validateMandatoryFields();
                    updateUserModifier.processRequest(userData, getDefaultCreationReceiver(currentPresenter, view), Void.class);
                } catch (Exception e) {
                    ShowErrorMessageEvent.fire(currentPresenter, e.getMessage());
                }
            }
        });
    }

    @ProxyCodeSplit
    @NameToken(PlaceNameToken.USER_SETTINGS_OVERVIEW)
    public interface UserManagementOverviewProxy extends
            ProxyPlace<UserSettingsOverviewPresenter>, Place {
    }

    public interface UserManagementOverviewView extends View {
        void setUserData(final FeederUserProxy currentUser);
        void registerCancelClickHandler(final ClickHandler handleCancelEvent);
        void registerSaveClickHandler(final ClickHandler handleSaveEvent);
        public ClientUserData getUserAccountData();
    }

    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this.eventBus,
                MainFeedListContainerPresenter.TYPE_SetFeedView, this);
    }

    private void registerCancelHandler(final UserManagementOverviewView view, final PlaceManager placeManager) {
        view.registerCancelClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                PlaceRequest request = new PlaceRequest(PlaceNameToken.FOUND_FEED_LIST);
                History.newItem(placeManager.buildHistoryToken(request));
            }
        });
    }

    private Receiver<Void> getDefaultCreationReceiver(final UserSettingsOverviewPresenter currentPresenter,
                                                      final UserManagementOverviewView view) {
        return new Receiver<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {
                ShowInfoMessageEvent.fire(currentPresenter, "User successfully updated!");
            }

            @Override
            public void onFailure(final ServerFailure error) {
                ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
            }
        };
    }

}
