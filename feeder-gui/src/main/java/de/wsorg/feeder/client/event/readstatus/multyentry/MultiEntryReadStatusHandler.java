package de.wsorg.feeder.client.event.readstatus.multyentry;

import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;

/**
 *
 *
 *
 */
public interface MultiEntryReadStatusHandler extends ClickHandler {
    void setFeedEntryList(List<AtomEntryProxy> feedEntryList);
    void addHookEvent(ClickHandler eventHandler);
}
