package de.wsorg.feeder.client.ui.widget.getfeeds;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import de.wsorg.feeder.client.ui.widget.common.button.FeederButton;
import de.wsorg.feeder.client.ui.widget.common.table.FeederTable;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

public class FeedInfo extends Composite {
    private static FeederDetailsUiBinder uiBinder = GWT.create(FeederDetailsUiBinder.class);
    @UiField FeederTable ftInfo;
    @UiField Image imgFeedImage;
    @UiField Label lblFeedTitle;
    @UiField HorizontalPanel hpFeddDetailsContent;
    private StringUtils stringUtils;

    interface FeederDetailsUiBinder extends UiBinder<Widget, FeedInfo> {}

	private String feedName;
	private String feedImageUrl;


    @Inject
	public FeedInfo(final StringUtils stringUtils) {
		super();
        this.stringUtils = stringUtils;
        initWidget(uiBinder.createAndBindUi(this));
	}

    @UiFactory
    public FeederTable createFeederTemplate(){
        return new FeederTable(this.stringUtils);
    }
    
    public void initFeedInfoUi(AtomWrapper atom){
    	
    	String publishedDate = atom.getUpdated() != null ? atom
				.getUpdated().toString() : "";
    	
		this.feedName = atom.getTitle();
		this.feedImageUrl = atom.getLogoUrl();
		
		this.hpFeddDetailsContent.setVisible(true);

		this.ftInfo.addRow("Published", publishedDate);
		this.ftInfo.addRow("Url", atom.getId());

        if(!stringUtils.isBlank(atom.getDescription()))
		    this.ftInfo.addRow("Description", atom.getDescription());

    	this.addHeader(this.feedImageUrl, this.feedName);
    }

    public void addActionButton(final String buttonId, final FeederButton feederButton) {
        this.ftInfo.addActionButton(buttonId, feederButton);
    }
    
    private void addHeader(String imageUrl, String feedName){    	
    	if(imageUrl != null && !stringUtils.unsafeEquals(imageUrl, "")){
    		this.imgFeedImage.setUrl(imageUrl);    	
    	} else {
    		this.imgFeedImage.setVisible(false);
    	}
	
        this.lblFeedTitle.setText(feedName);
    }

	public void clear() {
		this.ftInfo.clear();
		this.imgFeedImage.setUrl("");
		this.lblFeedTitle.setText("");
		this.hpFeddDetailsContent.setVisible(false);
	}
}