package de.wsorg.feeder.client.ui.authentication.user;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.presenter.authentication.user.UserSettingsOverviewPresenter;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserPassword;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.PasswordType;
import de.wsorg.feeder.client.ui.widget.common.button.FeederButton;
import de.wsorg.feeder.client.ui.widget.common.textbox.ValidatedTextBox;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserSettingsOverviewViewImpl extends ViewImpl implements UserSettingsOverviewPresenter.UserManagementOverviewView {

    private Widget widget;

    @UiField TextBox tbUserName;
    @UiField PasswordTextBox tbPassword;
    @UiField ValidatedTextBox tbEmail;
    @UiField TextBox tbFirstName;
    @UiField TextBox tbLastName;
    @UiField FeederButton fbCancelView;
    @UiField FeederButton fbSaveAccount;
    private FeederUserProxy currentUser;

    public interface UserSettingsOverviewViewUiBinder extends UiBinder<Widget, UserSettingsOverviewViewImpl> {}

    @Inject
    public UserSettingsOverviewViewImpl(final UserSettingsOverviewViewUiBinder uiBinder) {
        this.widget = uiBinder.createAndBindUi(this);
    }

    @Override
    public Widget asWidget() {
        return this.widget;
    }

    @Override
    public void setUserData(final FeederUserProxy currentUser) {
        this.currentUser = currentUser;
        this.tbUserName.setText(currentUser.getUserName());
        this.tbFirstName.setText(currentUser.getFirstName());
        this.tbLastName.setText(currentUser.getLastName());
        this.tbPassword.setText(currentUser.getPassword());
        this.tbEmail.setText(currentUser.getEMail());
    }

    @Override
    public void registerCancelClickHandler(final ClickHandler handleCancelEvent) {
        fbCancelView.addClickHandler(handleCancelEvent);
    }

    @Override
    public void registerSaveClickHandler(final ClickHandler handleSaveEvent) {
        fbSaveAccount.addClickHandler(handleSaveEvent);
    }

    @Override
    public ClientUserData getUserAccountData() {
        ClientUserData userData = new ClientUserData();
        userData.setUserName(this.tbUserName.getText());

        userData.setPassword(getUserPassword());
        userData.seteMail(this.tbEmail.getText());
        userData.setFirstName(this.tbFirstName.getText());
        userData.setLastName(this.tbLastName.getText());
        return userData;
    }

    private ClientUserPassword getUserPassword() {
        PasswordType passwordType;
        if(currentUser.getPassword().equals(this.tbPassword.getText())) {
            passwordType = PasswordType.MD5;
        } else {
            passwordType = PasswordType.PLAIN_TEXT;
        }

        return new ClientUserPassword(this.tbPassword.getText(),
                                        passwordType);
    }
}
