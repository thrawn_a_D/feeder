package de.wsorg.feeder.client.security.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 *
 *
 *
 */
public class UserLoggedInEvent extends GwtEvent<UserLoggedInEvent.UserLoggedInHandler> {

    private FeederUserProxy feedUser;

    public interface UserLoggedInHandler extends EventHandler {
        void onShowMessage(UserLoggedInEvent event);
    }

    private static Type<UserLoggedInHandler> TYPE = new Type<UserLoggedInHandler>();

    public static void fire(HasHandlers source, FeederUserProxy feedUser) {
        if (TYPE != null) {
            source.fireEvent(new UserLoggedInEvent(feedUser));
        }
    }

    public static Type<UserLoggedInHandler> getType() {
        return TYPE;
    }

    public UserLoggedInEvent(final FeederUserProxy feedUser) {
        this.feedUser = feedUser;
    }

    @Override
    public Type<UserLoggedInHandler> getAssociatedType() {
        return TYPE;
    }

    public FeederUserProxy getUser() {
        return feedUser;
    }

    @Override
    protected void dispatch(UserLoggedInHandler handler) {
        handler.onShowMessage(this);
    }
}
