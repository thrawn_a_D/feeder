package de.wsorg.feeder.client.ui.widget.common.textbox.validator;

/**
 *
 *
 *
 */
public abstract class Validator {
    public String errorMessage;

    public abstract boolean validate(String value);

    public String getErrorMessage() {
        return errorMessage;
    }
}
