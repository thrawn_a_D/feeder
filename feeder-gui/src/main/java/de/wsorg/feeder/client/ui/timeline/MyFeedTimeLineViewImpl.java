package de.wsorg.feeder.client.ui.timeline;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.datasource.AtomEntryDataSource;
import de.wsorg.feeder.client.presenter.timeline.MyFeedTimeLinePresenter;
import de.wsorg.feeder.client.ui.cell.FeedEntryDetail;
import de.wsorg.feeder.client.ui.widget.ShowMorePagerPanel;
import de.wsorg.feeder.client.ui.widget.common.button.ImageButton;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

/**
 *
 *
 *
 */
public class MyFeedTimeLineViewImpl extends ViewImpl implements MyFeedTimeLinePresenter.MyFeedTimeLineView {

    private Widget widget;

    @UiField
    SimplePanel pnlFeedInfo;
    @UiField
    ShowMorePagerPanel vpFeedEntries;
    @UiField
    ImageButton btnMarkVisibleAsRead;
    @UiField
    ImageButton btnMarkAllAsRead;
    @UiField
    ImageButton btnRefreshTimeline;
    @UiField
    CheckBox cbShowOnlyUnreadEntries;

    private CellList<AtomEntryProxy> cellList;
    private final FeedEntryDetail cellItem;
    private List<AtomEntryProxy> feedEntryList = new ArrayList<AtomEntryProxy>();

    public interface MyFeedTimeLineViewUiBinder extends UiBinder<Widget, MyFeedTimeLineViewImpl> {}

    @Inject
    public MyFeedTimeLineViewImpl(final MyFeedTimeLineViewUiBinder uiBinder,
                                  final FeedEntryDetail cellItem) {
        this.cellItem = cellItem;
        this.widget = uiBinder.createAndBindUi(this);
        this.cbShowOnlyUnreadEntries.setValue(true);
        this.initializeCellList();
    }

    @Override
    public void showLoadingInfo() {
        Label loading = new Label("Loading content please wait");
        this.pnlFeedInfo.clear();
        this.pnlFeedInfo.add(loading);
        this.pnlFeedInfo.setVisible(true);
    }

    @Override
    public void showNoEntriesFound() {
        Label noItemsFound = new Label("No feed entries found");
        this.pnlFeedInfo.clear();
        this.pnlFeedInfo.add(noItemsFound);
        this.pnlFeedInfo.setVisible(true);
        this.vpFeedEntries.setVisible(false);
    }

    @Override
    public void setFeedEntryList(final List<AtomEntryProxy> feedEntries) {
        if (feedEntries.size() > 0) {
            AtomEntryDataSource.get().clear();
            AtomEntryDataSource.get().setEntryList(feedEntries);
            this.cellList.setVisible(true);
            this.pnlFeedInfo.setVisible(false);
            this.btnMarkVisibleAsRead.setIsActive(true);
            this.btnMarkAllAsRead.setIsActive(true);
            this.vpFeedEntries.setVisible(true);
            this.feedEntryList = feedEntries;
        } else {
            this.btnMarkVisibleAsRead.setIsActive(false);
            this.btnMarkAllAsRead.setIsActive(false);
        }
    }

    @Override
    public List<AtomEntryProxy> getFeedEntryList() {
        return this.feedEntryList;
    }

    @Override
    public void setMarkVisibleAsReadHandler(ClickHandler handler) {
        this.btnMarkVisibleAsRead.addClickHandler(handler);
    }

    @Override
    public void setMarkAllAsReadHandler(final ClickHandler handler) {
        this.btnMarkAllAsRead.addClickHandler(handler);
    }

    @Override
    public void setTimeLineRefreshEvent(final ClickHandler handler) {
        this.btnRefreshTimeline.addClickHandler(handler);
    }

    @Override
    public boolean isShowOnlyUnreadEntries() {
        return this.cbShowOnlyUnreadEntries.getValue();
    }

    @Override
    public void setPagingTimeLineEvent(final ClickHandler scrollDown) {
        this.vpFeedEntries.addPagingEventHandler(scrollDown);
    }

    @Override
    public void disablePagingButton() {
        this.vpFeedEntries.disablePagingButton();
    }

    @Override
    public void enablePagingButton() {
        this.vpFeedEntries.enablePagingButton();
    }

    @Override
    public void scrollToTop() {
        this.vpFeedEntries.scrollToTop();
    }

    @Override
    public void setAtom(final AtomWrapper atom) {
        this.cellItem.setRelatedAtom(atom);
    }

    @Override
    public Widget asWidget() {
        return widget;
    }

    private void initializeCellList() {
        cellList = new CellList<AtomEntryProxy>(cellItem,
                AtomEntryDataSource.FeedEntryProxy_KEY_PROVIDER);
        cellList.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.INCREASE_RANGE);
        cellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.BOUND_TO_SELECTION);

        AtomEntryDataSource.get().addDataDisplay(cellList);

        this.vpFeedEntries.setDisplay(cellList);
    }

    @Override
    public void clear() {
        cellList.setVisible(false);
        cellList.setPageSize(ShowMorePagerPanel.DEFAULT_INCREMENT);
        this.pnlFeedInfo.setVisible(false);
    }
}
