package de.wsorg.feeder.client.security.session;

/**
 *
 *
 *
 */
public interface ClientSessionStore {
    void putSessionValue(final String objectId, final String object);
    String getValueFromSession(final String objectId);
    void removeSessionValue(final String objectId);
}
