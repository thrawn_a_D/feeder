package de.wsorg.feeder.client.security.session;

import com.google.inject.Inject;
import de.wsorg.feeder.client.security.session.wrapper.GwtCookiesWrapper;

/**
 *
 *
 *
 */
public class SimpleClientSessionStore implements ClientSessionStore {
    final GwtCookiesWrapper cookieWrapper;

    @Inject
    public SimpleClientSessionStore(final GwtCookiesWrapper cookieWrapper) {
        this.cookieWrapper = cookieWrapper;
    }


    @Override
    public void putSessionValue(final String objectId, final String object) {
        cookieWrapper.addCookieValue(objectId, object);
    }

    @Override
    public String getValueFromSession(final String objectId) {
        return cookieWrapper.getCookieValue(objectId);
    }

    @Override
    public void removeSessionValue(final String objectId) {
        cookieWrapper.clearCookieValue(objectId);
    }
}
