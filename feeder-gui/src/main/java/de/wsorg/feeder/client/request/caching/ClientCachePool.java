package de.wsorg.feeder.client.request.caching;

import com.google.inject.Singleton;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *
 */
@Singleton
public class ClientCachePool {
    private List<ClientCache> clientCachePool = new ArrayList<ClientCache>();

    public void registerClientCache(final ClientCache clientCache){
        clientCachePool.add(clientCache);
    }

    public void clearWholeCache(){
        for (ClientCache clientCache : clientCachePool) {
            clientCache.clearCache();
        }
    }
}
