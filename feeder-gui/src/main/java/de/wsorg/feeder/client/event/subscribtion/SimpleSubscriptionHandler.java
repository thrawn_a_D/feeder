package de.wsorg.feeder.client.event.subscribtion;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.client.request.modifying.concrete.feed.SubscriptionModifier;
import de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion.FeedSubscriptionRequestProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SimpleSubscriptionHandler implements SubscriptionHandler {
    private final SubscriptionModifier subscriptionModifier;

    @Inject
    public SimpleSubscriptionHandler(final SubscriptionModifier subscriptionModifier) {
        this.subscriptionModifier = subscriptionModifier;
    }

    @Override
    public void onSubscribeFeed(final String feedUrl) {
        processSubscription(feedUrl, true);
    }

    @Override
    public void onCancelSubscription(final String feedUrl) {
        processSubscription(feedUrl, false);
    }

    private void processSubscription(final String feedUrl, final boolean subscribeToFeed) {
        Receiver<Void> receiver = getDefaultSubscriptionReceiver();
        final FeedSubscriptionRequestProxy subscriptionRequest = getSubscriptionRequest(feedUrl, subscribeToFeed);
        subscriptionModifier.processRequest(subscriptionRequest, receiver, Void.class);
    }

    private FeedSubscriptionRequestProxy getSubscriptionRequest(final String feedUrl, final boolean subscribeToFeed) {
        final FeedSubscriptionRequestProxy subscriptionRequest = subscriptionModifier.getNewDomainObjectForParam(FeedSubscriptionRequestProxy.class);
        subscriptionRequest.setFeedUrl(feedUrl);
        subscriptionRequest.setFavourFeed(subscribeToFeed);
        return subscriptionRequest;
    }

    private Receiver<Void> getDefaultSubscriptionReceiver() {
        return new Receiver<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {

            }
        };
    }
}
