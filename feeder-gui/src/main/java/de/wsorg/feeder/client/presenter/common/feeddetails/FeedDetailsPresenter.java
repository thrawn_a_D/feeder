package de.wsorg.feeder.client.presenter.common.feeddetails;

import java.util.List;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.event.subscribtion.SubscriptionHandler;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.presenter.common.view.PagingView;
import de.wsorg.feeder.client.request.reading.concrete.feed.load.LoadAtomForUserRequestManager;
import de.wsorg.feeder.client.request.reading.concrete.feed.load.LoadAtomRequestManager;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedForUserRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.load.PagingRangeProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

public class FeedDetailsPresenter
		extends
		Presenter<FeedDetailsPresenter.FeedItemDetailsView, FeedDetailsPresenter.FeedItemDetailsProxy> {

    private int startPageCount = 1;
    private int endPageCount = 10;

    public static final int PAGING_INCREMENT = 10;

    private String feedAddress;

    private AtomWrapper atom;

    @ProxyCodeSplit
	@NameToken(PlaceNameToken.FEED_DETAILS)
	public interface FeedItemDetailsProxy extends
			ProxyPlace<FeedDetailsPresenter>, Place {
	}

	public interface FeedItemDetailsView extends PagingView {
		void setFeedInfo(AtomWrapper atom);
        void setAtom(AtomWrapper atom);
		void setFeedEntryList(List<AtomEntryProxy> atomEntry);
		void showLoadingInfo();
        void addSubscribeButton(final ClickHandler subscriptionEvent);
        void addUnsubscribeButton(final ClickHandler subscriptionEvent);
		void clear();
        void refresh();
	}

	private final EventBus eventBus;
	private final FeedItemDetailsView view;
    private final UserSession userSession;
    private final LoadAtomRequestManager<LoadFeedRequestProxy> requestManager;
    private final LoadAtomForUserRequestManager requestManagerForUser;
    private final SubscriptionHandler subscriptionHandler;
    private final ApplicationRequestFactory factory;

    @Inject
	public FeedDetailsPresenter(final EventBus eventBus,
			final FeedItemDetailsView view,
            final FeedItemDetailsProxy proxy,
            final UserSession userSession,
			final LoadAtomRequestManager requestManager,
            final LoadAtomForUserRequestManager requestManagerForUser,
            final SubscriptionHandler subscriptionHandler,
            final ApplicationRequestFactory factory) {
		super(eventBus, view, proxy);
		this.eventBus = eventBus;
		this.view = view;
        this.userSession = userSession;
        this.requestManager = requestManager;
        this.requestManagerForUser = requestManagerForUser;
        this.subscriptionHandler = subscriptionHandler;
        this.factory = factory;

        this.setPagingEventHandler(view);
    }

	@Override
	public void prepareFromRequest(PlaceRequest placeRequest) {
		super.prepareFromRequest(placeRequest);
		view.clear();
		view.showLoadingInfo();

        String feedEntryCountToShow = placeRequest.getParameter("showEntriesCount", "11");
        final String incomingFeedAddress = placeRequest.getParameter("feedAddress", "");

        if(!incomingFeedAddress.equals(this.feedAddress)) {
            endPageCount = 10;
            feedAddress = incomingFeedAddress;
        }

        Integer endItemCount = Integer.valueOf(feedEntryCountToShow);

        executeLoadRequest(feedAddress, endItemCount);
    }

    private void executeLoadRequest(final String feedAddress, final Integer endItemCount) {
        Receiver<AtomWrapper> receiver = getLoadRequestReceiver();

        LoadFeedRequestProxy loadFeedProxy;
        PagingRangeProxy pagingRangeProxy;
        LoadAtomRequestManager requestManagerToBeUsed;
        if (userSession.getCurrentUser() == null) {
            requestManagerToBeUsed = requestManager;
            loadFeedProxy = this.requestManager.getNewDomainObjectForParam(LoadFeedRequestProxy.class);
            pagingRangeProxy = this.requestManager.getNewDomainObjectForParam(PagingRangeProxy.class);

        } else {
            requestManagerToBeUsed = requestManagerForUser;
            loadFeedProxy = this.requestManagerForUser.getNewDomainObjectForParam(LoadFeedForUserRequestProxy.class);
            pagingRangeProxy = this.requestManagerForUser.getNewDomainObjectForParam(PagingRangeProxy.class);
        }

        pagingRangeProxy.setStartIndex(startPageCount);
        pagingRangeProxy.setEndIndex(endItemCount);
        loadFeedProxy.setFeedUrl(feedAddress);
        loadFeedProxy.setFeedEntryPagingRange(pagingRangeProxy);
        requestManagerToBeUsed.processRequest(loadFeedProxy, receiver, AtomWrapper.class);
    }

    private Receiver<AtomWrapper> getLoadRequestReceiver() {
        final FeedDetailsPresenter currentPresenter = this;

        return new Receiver<AtomWrapper>() {

            @Override
            public void onSuccess(AtomWrapper response) {
                atom = response;
                if (response != null) {
                    view.setFeedInfo(response);
                    if (response.getFeedEntries() != null && response.getFeedEntries().size() > 0) {
                        view.setFeedEntryList(response.getFeedEntries());
                        addSubscriptionButton(response);

                        if(response.isAdditionalPagingPossible()) {
                            view.enablePagingButton();
                        } else {
                            view.disablePagingButton();
                        }
                    }
                }
            }

            @Override
            public void onFailure(ServerFailure error) {
                ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
            }
        };
    }

    private void setPagingEventHandler(final FeedItemDetailsView view) {
        ClickHandler showMoreTimeLineItems = new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                endPageCount += PAGING_INCREMENT;
                executeLoadRequest(feedAddress, endPageCount);
            }
        };

        view.setPagingTimeLineEvent(showMoreTimeLineItems);
    }

    private void addSubscriptionButton(final AtomWrapper response) {
        if(userSession.getCurrentUser() != null) {
            if(response.isFavoured()) {
                view.addUnsubscribeButton(getUnsubscribeHandler(response.getId()));
            } else {
                view.addSubscribeButton(getSubscribeHandler(response.getId()));
            }
        }
    }

    private ClickHandler getUnsubscribeHandler(final String feedUrl) {
        return new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                subscriptionHandler.onCancelSubscription(feedUrl);
                view.addSubscribeButton(getSubscribeHandler(feedUrl));
                AtomProxy editProxy = factory.feedRequest().edit((AtomProxy) atom);
                editProxy.setFavoured(false);
                view.setAtom(editProxy);
                view.refresh();
            }
        };
    }

    private ClickHandler getSubscribeHandler(final String feedUrl) {
        return new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                subscriptionHandler.onSubscribeFeed(feedUrl);
                view.addUnsubscribeButton(getUnsubscribeHandler(feedUrl));
                AtomProxy editProxy = factory.feedRequest().edit((AtomProxy) atom);
                editProxy.setFavoured(true);
                view.setAtom(editProxy);
                view.refresh();
            }
        };
    }

    @Override
	protected void revealInParent() {
		RevealContentEvent.fire(this.eventBus,
				MainFeedListContainerPresenter.TYPE_SetFeedView, this);
	}
}
