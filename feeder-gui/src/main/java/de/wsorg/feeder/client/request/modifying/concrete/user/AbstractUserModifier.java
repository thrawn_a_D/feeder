package de.wsorg.feeder.client.request.modifying.concrete.user;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.modifying.AbstractFeedModifier;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.PasswordType;
import de.wsorg.feeder.client.request.wrapper.MD5JavaScriptWrapper;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.UserRequest;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class AbstractUserModifier extends AbstractFeedModifier<Void, ClientUserData> {

    private ApplicationRequestFactory factory;
    protected UserRequest requestContext;
    protected MD5JavaScriptWrapper md5JavaScriptWrapper;

    @Inject
    public AbstractUserModifier(final ApplicationRequestFactory factory) {
        this.factory = factory;
        this.requestContext = factory.userRequest();
    }

    protected abstract void processActualCall(final FeederUserProxy userProxy,
                                              final UserRequest userRequest,
                                              final Receiver<Void> responseReceiver);

    @Override
    protected void processActualModification(final ClientUserData params, final Receiver<Void> responseReceiver) {
        final UserRequest userRequest = this.factory.userRequest();
        final FeederUserProxy userProxy = userRequest.create(FeederUserProxy.class);
        userProxy.setEMail(params.geteMail());
        userProxy.setUserName(params.getUserName());

        final String userPassword;
        if (params.getPassword().getPasswordType() == PasswordType.PLAIN_TEXT) {
            userPassword = md5JavaScriptWrapper.hashPassword(params.getPassword().getPasswordText());
        } else {
            userPassword = params.getPassword().getPasswordText();
        }

        userProxy.setPassword(userPassword);
        userProxy.setFirstName(params.getFirstName());
        userProxy.setLastName(params.getLastName());

        processActualCall(userProxy, userRequest, responseReceiver);
    }

    @Override
    protected RequestContext getRequestContext() {
        return this.requestContext;
    }

    @Inject
    public void setMd5JavaScriptWrapper(final MD5JavaScriptWrapper md5JavaScriptWrapper) {
        this.md5JavaScriptWrapper = md5JavaScriptWrapper;
    }
}
