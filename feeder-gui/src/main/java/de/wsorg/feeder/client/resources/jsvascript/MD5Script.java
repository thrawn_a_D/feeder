package de.wsorg.feeder.client.resources.jsvascript;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface MD5Script extends ClientBundle {
    @Source("de/wsorg/feeder/client/md5.js")
    TextResource md5ScriptJS();
}
