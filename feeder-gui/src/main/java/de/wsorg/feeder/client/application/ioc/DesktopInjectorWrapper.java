package de.wsorg.feeder.client.application.ioc;

import com.google.gwt.core.client.GWT;

public class DesktopInjectorWrapper implements InjectorWrapper {

    public AppInjector getInjector() {
        return GWT.create(DesktopInjector.class);
    }

}
