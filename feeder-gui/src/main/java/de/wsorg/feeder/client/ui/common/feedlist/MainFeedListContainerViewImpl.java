package de.wsorg.feeder.client.ui.common.feedlist;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.ui.widget.getfeeds.FeedInfo;

public class MainFeedListContainerViewImpl extends ViewImpl implements MainFeedListContainerPresenter.MainFeedListContainerView {

    public interface Binder extends UiBinder<Widget, MainFeedListContainerViewImpl> {
	}

	private final Widget widget;
	@UiField SimplePanel pnlFeedPanel;
	private FeedInfo feederDetails;
	private PlaceManager placeManager;

	@Inject
	public MainFeedListContainerViewImpl(final Binder uiBinder, final PlaceManager placeManager, final FeedInfo feedInfo) {
        this.widget = uiBinder.createAndBindUi(this);
		this.placeManager = placeManager;
        this.feederDetails = feedInfo;
	}

	@UiFactory
	public FeedInfo makeMainManu() {
		return this.feederDetails;
	}

	@Override
	public Widget asWidget() {
		return widget;
	}

	@Override
	public void resetVew() {
		this.pnlFeedPanel.clear();
		this.feederDetails.clear();
	}
	
    @Override
    public void setInSlot(Object slot, Widget content) {
    	super.setInSlot(slot, content);
      if (slot == MainFeedListContainerPresenter.TYPE_SetFeedView) {
    	  setItemDetails(content);
      } else {
        super.setInSlot(slot, content);
      }
    }

    private void setItemDetails(Widget content) {
      this.pnlFeedPanel.clear();
      
      if (content != null) {
        this.pnlFeedPanel.add(content);
      }
    }
}
