package de.wsorg.feeder.client.event.readstatus.all;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.client.request.modifying.concrete.feed.AllEntriesReadStatusModifier;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatusProxy;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
public class SimpleAllEntriesReadStatusHandler implements AllEntriesReadStatusHandler {

    private final AllEntriesReadStatusModifier allEntriesReadStatusModifier;
    private Receiver<Void> defaultEventReceiver;

    @Inject
    public SimpleAllEntriesReadStatusHandler(final AllEntriesReadStatusModifier allEntriesReadStatusModifier) {
        this.allEntriesReadStatusModifier = allEntriesReadStatusModifier;
    }

    @Override
    public void markAllFeedsAsRead() {
        setReadStatus(true);
    }

    @Override
    public void markAllFeedsAsUnread() {
        setReadStatus(false);
    }

    @Override
    public void setEventReceiver(final Receiver<Void> eventReceiver) {
        this.defaultEventReceiver = eventReceiver;
    }

    private void setReadStatus(final boolean allEntriesAreRead) {
        AllEntriesReadStatusProxy readStatusDomain = allEntriesReadStatusModifier.getNewDomainObjectForParam(AllEntriesReadStatusProxy.class);
        readStatusDomain.setReadStatus(allEntriesAreRead);
        allEntriesReadStatusModifier.processRequest(readStatusDomain, getDefaultSubscriptionReceiver(), Void.class);
    }

    private Receiver<Void> getDefaultSubscriptionReceiver() {
        if(defaultEventReceiver == null) {
            defaultEventReceiver = new Receiver<Void>() {
                @Override
                public void onSuccess(final Void aVoid) {

                }
            };
        }
        return defaultEventReceiver;
    }
}
