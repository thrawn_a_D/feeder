package de.wsorg.feeder.client.ui.widget.common.textbox.validator;

import com.google.gwt.regexp.shared.RegExp;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UrlValidator extends Validator {
    private RegExp urlValidator;
    private RegExp urlPlusTldValidator;

    @Override
    public boolean validate(final String value) {
        final boolean validUrl = isValidUrl(value, false);

        if(validUrl) {
            errorMessage = "";
        } else {
            errorMessage = "Provide a valid URL!";
        }

        return validUrl;
    }

    public boolean isValidUrl(String url, boolean topLevelDomainRequired) {
        if (urlValidator == null || urlPlusTldValidator == null) {
            urlValidator = RegExp.compile("^((ftp|http|https)://[\\w@.\\-\\_]+(:\\d{1,5})?(/[\\w#!:.?+=&%@!\\_\\-/]+)*){1}$");
            urlPlusTldValidator = RegExp.compile("^((ftp|http|https)://[\\w@.\\-\\_]+\\.[a-zA-Z]{2,}(:\\d{1,5})?(/[\\w#!:.?+=&%@!\\_\\-/]+)*){1}$");
        }
        return (topLevelDomainRequired ? urlPlusTldValidator : urlValidator).exec(url) != null;
    }
}
