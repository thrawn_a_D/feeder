package de.wsorg.feeder.client.security.session.wrapper;

import com.google.gwt.user.client.Cookies;

/**
 *
 *
 *
 */
public class GwtCookiesWrapper {
    public void addCookieValue(final String key, final String value){
        Cookies.setCookie(key, value);
    }

    public String getCookieValue(final String key) {
        return Cookies.getCookie(key);
    }

    public void clearCookieValue(final String key) {
        Cookies.removeCookie(key);
    }
}
