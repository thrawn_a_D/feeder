package de.wsorg.feeder.client.request.modifying.concrete.feed;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.modifying.AbstractFeedModifier;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedSubscriptionHandlerRequest;
import de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion.FeedSubscriptionRequestProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SubscriptionModifier extends AbstractFeedModifier<Void, FeedSubscriptionRequestProxy> {

    private ApplicationRequestFactory factory;
    private FeedSubscriptionHandlerRequest requestContext;

    @Inject
    public SubscriptionModifier(final ApplicationRequestFactory factory) {
        this.factory = factory;
        this.requestContext = factory.feedSubscriptionHandlerRequest();
    }

    @Override
    protected void processActualModification(final FeedSubscriptionRequestProxy params, final Receiver<Void> responseReceiver) {
        requestContext.subscribeToFeed(params).fire(responseReceiver);
        requestContext = factory.feedSubscriptionHandlerRequest();
    }

    @Override
    protected RequestContext getRequestContext() {
        return this.requestContext;
    }
}
