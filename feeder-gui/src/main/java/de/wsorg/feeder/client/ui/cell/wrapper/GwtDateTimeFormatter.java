package de.wsorg.feeder.client.ui.cell.wrapper;

import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GwtDateTimeFormatter {
    public DateTimeFormat getFormat(final DateTimeFormat.PredefinedFormat pattern){
        return DateTimeFormat.getFormat(pattern);
    }
}
