package de.wsorg.feeder.client.utils.wrapper;

import com.google.gwt.core.client.GWT;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GwtStaticClassWrapper {
    public boolean isScript() {
        return GWT.isScript();
    }
}
