package de.wsorg.feeder.client.request.reading.concrete.feed.find;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryForUserProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class GlobalFindForUserRequestManager extends GlobalFindRequestManager<GlobalSearchQueryForUserProxy> {

    private final ApplicationRequestFactory factory;

    @Inject
    public GlobalFindForUserRequestManager(final ApplicationRequestFactory factory,
                                            final OpmlAutoBeanFactory opmlAutoBeanFactory) {
        super(factory, opmlAutoBeanFactory);
        this.factory = factory;
    }

    @Override
    protected void executeRequest(final GlobalSearchQueryForUserProxy requestParams, final Receiver<OpmlWrapper> receiverOfRelatedCall) {
        feedRequest.findGlobalFeedWithUserData(requestParams).fire(receiverOfRelatedCall);
        feedRequest = factory.feedRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final GlobalSearchQueryForUserProxy requestParam) {
        return requestParam.getUserName() + "#@#" + requestParam.getSearchTerm();
    }
}
