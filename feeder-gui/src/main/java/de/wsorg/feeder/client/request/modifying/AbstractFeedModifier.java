package de.wsorg.feeder.client.request.modifying;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.CommonFeedRequestManager;
import de.wsorg.feeder.client.request.caching.ClientCachePool;
import de.wsorg.feeder.client.request.util.RequestContextDomainCreator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class AbstractFeedModifier<T, P> extends CommonFeedRequestManager<T, P> {

    private ClientCachePool clientCachePool;
    protected abstract RequestContext getRequestContext();

    protected abstract void processActualModification(final P params, final Receiver<T> responseReceiver);

    @Override
    public void processRequest(final P requestParams, final Receiver<T> responseReceiver, final Class<T> responseType) {
        processActualModification(requestParams, responseReceiver);
        clientCachePool.clearWholeCache();
    }

    @Inject
    public void setClientCachePool(final ClientCachePool clientCachePool) {
        this.clientCachePool = clientCachePool;
    }

    @Inject
    public void setDomainProxyCreator(final RequestContextDomainCreator domainProxyCreator) {
        this.domainProxyCreator = domainProxyCreator;
    }
}
