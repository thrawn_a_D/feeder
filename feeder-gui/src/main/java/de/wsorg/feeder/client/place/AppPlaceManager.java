package de.wsorg.feeder.client.place;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.PlaceManagerImpl;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.TokenFormatter;

public class AppPlaceManager extends PlaceManagerImpl {

	@Inject
	public AppPlaceManager(EventBus eventBus, TokenFormatter tokenFormatter) {
		super(eventBus, tokenFormatter);
	}

	@Override
	public void revealDefaultPlace() {
		revealPlace(new PlaceRequest(PlaceNameToken.FOUND_FEED_LIST));
	}

	public void revealItemDetailsViewPlace() {
		revealPlace(new PlaceRequest(PlaceNameToken.FEED_DETAILS));
	}
}
