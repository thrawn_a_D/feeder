package de.wsorg.feeder.client.request.reading.concrete.feed.find;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.reading.AbstractFeedRequestManager;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedRequest;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 *
 *
 *
 */
public class GlobalFindRequestManager<T extends GlobalSearchQueryProxy> extends AbstractFeedRequestManager<OpmlWrapper, T> {
    private final OpmlAutoBeanFactory opmlAutoBeanFactory;
    protected FeedRequest feedRequest;
    private final ApplicationRequestFactory factory;


    public interface OpmlAutoBeanFactory extends AutoBeanFactory {
        AutoBean<OpmlWrapper> feedproxy(OpmlWrapper delegate);
    }

    @Inject
    public GlobalFindRequestManager(final ApplicationRequestFactory factory, final OpmlAutoBeanFactory opmlAutoBeanFactory) {
        this.factory = factory;
        this.feedRequest = factory.feedRequest();
        this.opmlAutoBeanFactory = opmlAutoBeanFactory;
    }

    @Override
    protected void executeRequest(final T requestParams, final Receiver<OpmlWrapper> receiverOfRelatedCall) {
        feedRequest.findGlobalFeed(requestParams).fire(receiverOfRelatedCall);
        feedRequest = factory.feedRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final T requestParam) {
        return requestParam.getSearchTerm();
    }

    @Override
    protected AutoBeanFactory getDomainAutoBean() {
        return opmlAutoBeanFactory;
    }

    @Override
    protected boolean cacheResponse() {
        return true;
    }

    @Override
    protected RequestContext getRequestContext() {
        return feedRequest;
    }
}
