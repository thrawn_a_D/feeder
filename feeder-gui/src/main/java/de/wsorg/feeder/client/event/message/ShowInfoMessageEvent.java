package de.wsorg.feeder.client.event.message;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ShowInfoMessageEvent extends GwtEvent<ShowInfoMessageEvent.ShowInfoMessageHandler> {

    private boolean stayVisible;

    public interface ShowInfoMessageHandler extends EventHandler {
        void onShowMessage(ShowInfoMessageEvent event);
    }

    private static Type<ShowInfoMessageHandler> TYPE = new Type<ShowInfoMessageHandler>();

    public static void fire(HasHandlers source, String message, boolean stayVisible) {
        if (TYPE != null) {
            source.fireEvent(new ShowInfoMessageEvent(message, stayVisible));
        }
    }

    public static void fire(HasHandlers source, String message) {
        if (TYPE != null) {
            source.fireEvent(new ShowInfoMessageEvent(message, true));
        }
    }

    public static Type<ShowInfoMessageHandler> getType() {
        return TYPE;
    }

    private final String message;

    public ShowInfoMessageEvent(final String message, final boolean stayVisible) {
        this.message = message;
        this.stayVisible = stayVisible;
    }

    @Override
    public Type<ShowInfoMessageHandler> getAssociatedType() {
        return TYPE;
    }

    public String getMessage() {
        return message;
    }

    public boolean isStayVisible() {
        return stayVisible;
    }

    @Override
    protected void dispatch(ShowInfoMessageHandler handler) {
        handler.onShowMessage(this);
    }
}
