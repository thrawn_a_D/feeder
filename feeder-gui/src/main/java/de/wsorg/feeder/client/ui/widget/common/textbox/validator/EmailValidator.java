package de.wsorg.feeder.client.ui.widget.common.textbox.validator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class EmailValidator extends Validator {

    public boolean validate(String value) {
        if (value.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$")) {
            errorMessage = "";
            return true;
        } else {
            errorMessage = "Enter valid email Id";
            return false;
        }
    }
}

