package de.wsorg.feeder.client.ui.widget.common.textbox;

import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.TextBox;
import de.wsorg.feeder.client.ui.widget.common.textbox.validator.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class ValidatedTextBox extends TextBox {

    private static final String TEXTBOX_VALIDATION_ERROR_STYLE = "validation-error";
    private String errorMessage = "";
    private List<Validator> validators = new ArrayList<Validator>();

    public ValidatedTextBox() {
        this.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(final KeyUpEvent event) {
                validate();
            }
        });
    }

    public void addValidator(Validator validator) {
        validators.add(validator);
    }

    public boolean validate() {
        boolean validationResult = true;
        for (Validator validator : validators) {
            validationResult = validator.validate(getValue().trim());
            if (!validationResult) {
                errorMessage = validator.getErrorMessage();
                break;
            }
            errorMessage = validator.getErrorMessage();
        }
        clearValidationErrors(validationResult);
        return validationResult;
    }

    public void clearValidator() {
        validators.clear();
    }

    public void clearValidationErrors(boolean validationResult) {
        if (validationResult) {
            removeStyleName(TEXTBOX_VALIDATION_ERROR_STYLE);
            setTitle("");
        } else {
            addStyleName(TEXTBOX_VALIDATION_ERROR_STYLE);
            setTitle(errorMessage);
        }
    }

    @Override
    public void setValue(String s) {
        removeStyleDependentName(TEXTBOX_VALIDATION_ERROR_STYLE);
        super.setValue(s);
    }

    @Override
    public String getValue() {
        return super.getValue().trim();
    }
}
