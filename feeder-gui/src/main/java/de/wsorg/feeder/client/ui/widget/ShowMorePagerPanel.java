package de.wsorg.feeder.client.ui.widget;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasRows;
import com.google.inject.Inject;
import de.wsorg.feeder.client.ui.widget.common.button.ImageButton;

/**
 * A scrolling pager that automatically increases the range every time the
 * scroll bar reaches the bottom.
 */
public class ShowMorePagerPanel extends AbstractPager implements ResizeHandler {

    private static final Label NO_ENTRIES_FOUND_LABEL = new Label("No entries could be found!");

	/**
	 * The default increment size.
	 */
	public static final int DEFAULT_INCREMENT = 10;

	/**
	 * The last scroll position.
	 */
	private int lastScrollPos = 0;
    /**
	 * The scrollable panel.
	 */
	private final ScrollPanel scrollable = new ScrollPanel();
	private final VerticalPanel pnlPagerContent = new VerticalPanel();
	private ImageButton scrollButton = new ImageButton();
    private ClickHandler pagingHandler;

    /**
	 * Construct a new {@link ShowMorePagerPanel}.
	 */
    @Inject
	public ShowMorePagerPanel() {
        initWidget(pnlPagerContent);
		
		scrollButton.setText("Show more...");
		scrollButton.setWidth("100%");
        scrollButton.setVisible(false);

        setUpEntriesList();

        pnlPagerContent.add(scrollable);
		pnlPagerContent.add(scrollButton);

		this.scrollButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                pagingHandler.onClick(event);
                scrollDown();
            }
        });
	}

    private void setUpEntriesList() {
        scrollable.getElement().getStyle().setMarginBottom(3, Style.Unit.PX);
        Window.addResizeHandler(this);
        setSizeForEntriesList(Window.getClientHeight() - 150);
    }

    @Override
	public void setDisplay(HasRows display) {
		assert display instanceof Widget : "display must extend Widget";
		scrollable.setWidget((Widget) display);
		super.setDisplay(display);
	}

    public void addScrollHandler(ScrollHandler scrollHandler) {
        scrollable.addScrollHandler(scrollHandler);
    }

	@Override
	protected void onRangeOrRowCountChanged() {

	}

    public void scrollDown() {
		// If scrolling up, ignore the event.
		lastScrollPos = scrollable.getVerticalScrollPosition();

		HasRows display = getDisplay();
		if (display == null) {
			return;
		}
		int maxScrollTop = scrollable.getWidget().getOffsetHeight() - scrollable.getOffsetHeight();
		if (lastScrollPos >= maxScrollTop) {
			// We are near the end, so increase the page size.
			int newPageSize = Math.max(display.getVisibleRange().getLength()
                    + DEFAULT_INCREMENT, display.getRowCount());
			display.setVisibleRange(0, newPageSize);
		}
	}

    public void scrollToTop() {
        this.scrollable.scrollToTop();
    }

    public void showNoResultsFoundMessage() {
        scrollable.clear();
        scrollable.setWidget(NO_ENTRIES_FOUND_LABEL);
        this.scrollButton.setVisible(false);
    }

    public void addPagingEventHandler(final ClickHandler scrollDown) {
        this.pagingHandler = scrollDown;
    }

    public void disablePagingButton() {
        this.scrollButton.setVisible(false);
    }

    public void enablePagingButton() {
        this.scrollButton.setVisible(true);
    }

    @Override
    public void onResize(final ResizeEvent event) {
        setSizeForEntriesList(event.getHeight());
    }

    private void setSizeForEntriesList(final int height) {
        int scrollerHeight = height
                - scrollable.getAbsoluteTop()
                - scrollButton.getOffsetHeight() - 10;
        if (scrollerHeight < 1) scrollerHeight = 1;
        scrollable.setHeight(scrollerHeight + "px");
    }
}