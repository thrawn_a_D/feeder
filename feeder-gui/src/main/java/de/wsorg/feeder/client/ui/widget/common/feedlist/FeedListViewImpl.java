package de.wsorg.feeder.client.ui.widget.common.feedlist;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy.KeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.NoSelectionModel;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.datasource.OpmlDataSource;
import de.wsorg.feeder.client.resources.FeedCellListResources;
import de.wsorg.feeder.client.ui.cell.FeedListItem;
import de.wsorg.feeder.client.ui.widget.ShowMorePagerPanel;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;
import de.wsorg.feeder.shared.domain.feed.response.opml.OutlineProxy;

public class FeedListViewImpl extends Composite {

	private static int PAGE_SIZE = 10;

    @UiField ShowMorePagerPanel vpResultSet;

    private static FeedListViewBinder uiBinder = GWT.create(FeedListViewBinder.class);
	public interface FeedListViewBinder extends UiBinder<Widget, FeedListViewImpl> {
	}

	/**
	 * The CellList.
	 */
	private CellList<OutlineProxy> cellList;
	private FeedListItem cellItem;

	@Inject
	public FeedListViewImpl(final FeedListItem cellItem) {
		super();
        this.cellItem = cellItem;
        initWidget(uiBinder.createAndBindUi(this));

		this.initializeCellList();
	}

    public void setFeedToList(OpmlWrapper atomResult) {
        clear();
        OpmlDataSource.get().setEntryList(atomResult);
    }

	public void clear() {
		OpmlDataSource.get().clear();
		this.initializeCellList();
	}

	public void showNoResultsFoundMessage() {
		 this.vpResultSet.showNoResultsFoundMessage();
	}

    private void initializeCellList() {
		CellList.Resources resources = GWT.create(FeedCellListResources.class);
		cellList = new CellList<OutlineProxy>(cellItem, resources,
				OpmlDataSource.FeedProxy_KEY_PROVIDER);
		cellList.setPageSize(PAGE_SIZE);
		cellList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
		cellList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.DISABLED);
		// Add a selection model so we can select cells.
		NoSelectionModel<OutlineProxy> selectionModel = new NoSelectionModel<OutlineProxy>(
				OpmlDataSource.FeedProxy_KEY_PROVIDER);
		cellList.setSelectionModel(selectionModel);

		OpmlDataSource.get().addDataDisplay(cellList);

		this.vpResultSet.setDisplay(cellList);
	}
}
