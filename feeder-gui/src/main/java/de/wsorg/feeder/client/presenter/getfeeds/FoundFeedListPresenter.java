package de.wsorg.feeder.client.presenter.getfeeds;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.FeedListView;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.reading.concrete.feed.find.GlobalFindForUserRequestManager;
import de.wsorg.feeder.client.request.reading.concrete.feed.find.GlobalFindRequestManager;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryForUserProxy;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

public class FoundFeedListPresenter
		extends
		Presenter<FoundFeedListPresenter.FoundFeedListView, FoundFeedListPresenter.FeedFoundListProxy> {

	private final EventBus eventBus;
	private final FoundFeedListView view;
	private final GlobalFindRequestManager<GlobalSearchQueryProxy> globalFindRequestManager;
    private final GlobalFindForUserRequestManager globalFindRequestForUserManager;
    private final ApplicationRequestFactory factory;
    private final StringUtils stringUtils;
    private final UserSession userSession;

    public interface FoundFeedListView extends FeedListView {}

	@Inject
	public FoundFeedListPresenter(final EventBus eventBus,
                                  final FoundFeedListView view,
                                  final FeedFoundListProxy proxy,
                                  final GlobalFindRequestManager requestManager,
                                  final GlobalFindForUserRequestManager userRequestManager,
                                  final ApplicationRequestFactory factory,
                                  final StringUtils stringUtils,
                                  final UserSession userSession) {
		super(eventBus, view, proxy);
		this.eventBus = eventBus;
		this.view = view;
		this.globalFindRequestManager = requestManager;
        this.globalFindRequestForUserManager = userRequestManager;
        this.factory = factory;
        this.stringUtils = stringUtils;
        this.userSession = userSession;
    }

	@ProxyCodeSplit
	@NameToken(PlaceNameToken.FOUND_FEED_LIST)
	public interface FeedFoundListProxy extends
			ProxyPlace<FoundFeedListPresenter>, Place {
	}

	@Override
	public void prepareFromRequest(PlaceRequest placeRequest) {
		super.prepareFromRequest(placeRequest);
		this.view.clear();
		String query = placeRequest.getParameter("query", "");
		this.searchForQuery(query);
	}

    @Override
	protected void revealInParent() {
		RevealContentEvent.fire(this.eventBus,
				MainFeedListContainerPresenter.TYPE_SetFeedView, this);
	}
	
	private void searchForQuery(final String query){
        if(!stringUtils.isBlank(query)) {
            final FoundFeedListPresenter currentPresenter = this;

            Receiver<OpmlWrapper> receiver = new Receiver<OpmlWrapper>() {
                        @Override
                        public void onSuccess(OpmlWrapper response) {
                            if (response != null && response.getBody().getSearchResultList().size() > 0) {
                                view.setFeedToList(response);
                            } else {
                                view.showNoResultsFoundMessage();
                            }
                        }

                        @Override
                        public void onFailure(ServerFailure error) {
                            ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
                        }
                    };

            if(userSession.getCurrentUser() == null) {
                GlobalSearchQueryProxy queryProxy = this.globalFindRequestManager.getNewDomainObjectForParam(GlobalSearchQueryProxy.class);
                queryProxy.setSearchTerm(query);
                this.globalFindRequestManager.processRequest(queryProxy ,receiver, OpmlWrapper.class);
            } else {
                GlobalSearchQueryForUserProxy queryProxy = this.globalFindRequestForUserManager.getNewDomainObjectForParam(GlobalSearchQueryForUserProxy.class);
                queryProxy.setSearchTerm(query);
                this.globalFindRequestForUserManager.processRequest(queryProxy ,receiver, OpmlWrapper.class);
            }
        }
	}
}
