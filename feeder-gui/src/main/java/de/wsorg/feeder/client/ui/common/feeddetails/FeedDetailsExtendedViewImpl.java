package de.wsorg.feeder.client.ui.common.feeddetails;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsExtendedPresenter.FeedDetailsExtendedView;
import de.wsorg.feeder.client.ui.widget.FeedEntryPlayer;
import de.wsorg.feeder.client.ui.widget.ShowMorePagerPanel;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;

public class FeedDetailsExtendedViewImpl extends ViewImpl implements FeedDetailsExtendedView {

    private static final int PAGE_SIZE = 10;
	@UiField HTML pnlInfoDetils;
    @UiField Label lblFeedTitle;
    @UiField FeedEntryPlayer player;
    @UiField Label lblErrorWhileLoadMedia;
    @UiField ShowMorePagerPanel vpFeedCommentEntries;
	private Widget widget;
    private StringUtils stringUtils;

    @Inject
	public FeedDetailsExtendedViewImpl(final Binder uiBinder, final StringUtils stringUtils) {
		super();
        this.stringUtils = stringUtils;
        this.widget = uiBinder.createAndBindUi(this);
	}

	public interface Binder extends UiBinder<Widget, FeedDetailsExtendedViewImpl> {
	}

	@Override
	public Widget asWidget() {
		return widget;
	}

	@Override
	public void initializeView(AtomEntryProxy atomEntry) {
		
		this.lblErrorWhileLoadMedia.setVisible(false);
		
		SafeHtmlBuilder htmlBuilder = new SafeHtmlBuilder();

		htmlBuilder.appendHtmlConstant("<table id='cellItem' width='100%' cellspacing='0'>");
		this.setKeyValue("Published", atomEntry.getUpdated().toGMTString(), "altspec", htmlBuilder, true);
		this.setKeyValue("Description", atomEntry.getDescription(), "altspec", htmlBuilder, false);

		htmlBuilder.appendHtmlConstant("</table>");
    	
		this.pnlInfoDetils.setHTML(htmlBuilder.toSafeHtml());  
        this.lblFeedTitle.setText(atomEntry.getTitle());

    }
	
	private void showErrorMessageWhileLoadingMediaContent(){
		this.lblErrorWhileLoadMedia.setVisible(true);
	}
    
	protected void setKeyValue(String title, String value, String rowStyleClass,
			SafeHtmlBuilder sb, boolean escape) {
		if (!stringUtils.isBlank(value)) {
			sb.appendHtmlConstant("<tr>");
			sb.appendHtmlConstant(" <th scope='row' width='50' class='"
					+ rowStyleClass + "'>" + title + "</th>");
			sb.appendHtmlConstant(" <td class=\"tableitem_td\">");
			if (escape) {
				sb.appendEscaped(value);
			} else {
				sb.appendHtmlConstant(value);
			}
			sb.appendHtmlConstant("</td>");
			sb.appendHtmlConstant("</tr>");
		}
	}
}
