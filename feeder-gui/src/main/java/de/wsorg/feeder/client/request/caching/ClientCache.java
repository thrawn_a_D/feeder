package de.wsorg.feeder.client.request.caching;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import de.wsorg.feeder.client.request.caching.util.GwtBeanUtil;

import java.util.HashMap;

/**
 *
 *
 *
 */
@Singleton
public class ClientCache<T> {

    private HashMap<String, String> cachingMap = new HashMap<String, String>();
    private GwtBeanUtil<T> gwtBeanUtil;

    @Inject
    public ClientCache(final GwtBeanUtil<T> gwtBeanUtil, final ClientCachePool clientCachePool) {
        this.gwtBeanUtil = gwtBeanUtil;
        clientCachePool.registerClientCache(this);
    }

    public void storeObjectInCache(final String objectId, final T objectToStore) {
        AutoBean<T> autoBean = gwtBeanUtil.getAutoBean(objectToStore);
        final String objectAsJson=gwtBeanUtil.encodeAutoBeanToJson(autoBean);
        this.cachingMap.put(objectId, objectAsJson);
    }

    public T getObjectFromCache(final String objectId, final Class<T> wantedObjectType, final AutoBeanFactory clientBeanFactory) {
        AutoBean<T> autoBean = gwtBeanUtil.decodeJsonToAutoBean(clientBeanFactory, cachingMap.get(objectId), wantedObjectType);
        return autoBean.as();
    }

    public boolean hasEntryInCache(final String objectId) {
        return cachingMap.containsKey(objectId);
    }

    public void clearCache(){
        cachingMap.clear();
    }
}
