package de.wsorg.feeder.client.request.reading;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import de.wsorg.feeder.client.request.CommonFeedRequestManager;
import de.wsorg.feeder.client.request.caching.ClientCache;

/**
 *
 *
 *
 */
public abstract class AbstractFeedRequestManager<T, P> extends CommonFeedRequestManager<T, P> {

    private ClientCache<T> clientCache;

    protected abstract void executeRequest(final P requestParams, final Receiver<T> receiverOfRelatedCall);
    protected abstract String getUniqueIdOutOfRequestParam(P requestParam);
    protected abstract AutoBeanFactory getDomainAutoBean();
    protected abstract boolean cacheResponse();

    @Override
    public void processRequest(final P requestParams, final Receiver<T> responseReceiver, Class<T> responseType) {

        final String uniqueRequestId = getUniqueIdOutOfRequestParam(requestParams);

        if (clientCache.hasEntryInCache(uniqueRequestId)) {
            T cachedResult = clientCache.getObjectFromCache(uniqueRequestId, responseType, getDomainAutoBean());
            responseReceiver.onSuccess(cachedResult);
            performCall(requestParams, responseReceiver, uniqueRequestId);
        } else {
            performCall(requestParams, responseReceiver, uniqueRequestId);
        }
    }

    private void performCall(final P requestParams, final Receiver<T> responseReceiver, final String uniqueRequestId) {
        Receiver<T> cachingAndDelegatingReceiver = new Receiver<T>() {
            @Override
            public void onSuccess(T response) {
                cacheResponseIfWanted(response, uniqueRequestId);
                responseReceiver.onSuccess(response);
            }

            @Override
            public void onFailure(ServerFailure error) {
                responseReceiver.onFailure(error);
            }
        };
        executeRequest(requestParams, cachingAndDelegatingReceiver);
    }

    private void cacheResponseIfWanted(final T response, final String uniqueRequestId) {
        if(cacheResponse())
            clientCache.storeObjectInCache(uniqueRequestId, response);
    }

    @Inject
    public void setClientCache(final ClientCache<T> clientCache) {
        this.clientCache = clientCache;
    }

    public void clearCache() {
        this.clientCache.clearCache();
    }
}
