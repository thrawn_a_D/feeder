package de.wsorg.feeder.client.ui.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

public class MainMenu extends Composite {
    private static MainMenuUiBinder uiBinder = GWT.create(MainMenuUiBinder.class);

    interface MainMenuUiBinder extends UiBinder<Widget, MainMenu> {}

    @UiField HTMLPanel pnlTimelineMenu;
    @UiField HTMLPanel pnlMyFeedsMenu;

    @UiField HTMLPanel pnlLoginMenu;
    @UiField HTMLPanel pnlLogoutMenu;
    @UiField HTMLPanel pnlUserSettings;

    @UiField SpanElement spnUserInfo;

    @UiField UListElement ulMenuElements;

    @UiField Anchor aMenuLogout;

    @Inject
    public MainMenu(final UserSession userSession) {
        initWidget(uiBinder.createAndBindUi(this));

        adjusetMenuToUser(userSession);

        registerLogoutClickHandler(userSession);

        History.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(final ValueChangeEvent<String> event) {
                String destinationPlace = event.getValue();

                NodeList<Element> liElements = ulMenuElements.getElementsByTagName("li");
                for(int counter = 0; counter<liElements.getLength(); counter++){
                    Element liElement = liElements.getItem(counter);
                    resetMenuStyle(liElement);

                    markMenuIfMatchesDestination(destinationPlace, liElement);
                }
            }
        });
    }

    private void resetMenuStyle(final Element liElement) {
        if(liElement.getClassName().contains("current")) {
            final String className = liElement.getClassName().replace("current", "single-link");
            liElement.setClassName(className);
        }
    }

    private void markMenuIfMatchesDestination(final String destinationPlace, final Element liElement) {
        final NodeList<Element> linkElements = liElement.getElementsByTagName("a");
        for (int i = 0; i < linkElements.getLength(); i++) {
            AnchorElement href = (AnchorElement) linkElements.getItem(i);
            if(href.getHref().endsWith(destinationPlace)) {
                final String className = liElement.getClassName() + " current";
                liElement.setClassName(className);
            }
        }
    }

    private void registerLogoutClickHandler(final UserSession userSession) {
        aMenuLogout.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent clickEvent) {
                userSession.cancelUserSession();
                enableUserMenuForUser(null);
            }
        });
    }

    private void adjusetMenuToUser(final UserSession userSession) {
        FeederUserProxy currentUser = userSession.getCurrentUser();
        enableUserMenuForUser(currentUser);
    }

    public void enableUserMenuForUser(final FeederUserProxy user) {
        if(user != null) {
            pnlMyFeedsMenu.setVisible(true);
            pnlTimelineMenu.setVisible(true);
            pnlLogoutMenu.setVisible(true);
            pnlLoginMenu.setVisible(false);
            pnlUserSettings.setVisible(true);

            adjustLoginMenu(user);
        } else {
            pnlMyFeedsMenu.setVisible(false);
            pnlTimelineMenu.setVisible(false);
            pnlLogoutMenu.setVisible(false);
            pnlLoginMenu.setVisible(true);
            pnlUserSettings.setVisible(false);
        }
    }

    private void adjustLoginMenu(final FeederUserProxy user) {
        if(firstNameNotEmpty(user) &&
           lastNameNotEmpty(user)) {
            spnUserInfo.setInnerText(user.getFirstName() + " " + user.getLastName());
        } else {
            spnUserInfo.setInnerText(user.getEMail());
        }
    }

    private boolean lastNameNotEmpty(final FeederUserProxy user) {
        return stringEmpty(user.getLastName());
    }

    private boolean firstNameNotEmpty(final FeederUserProxy user) {
        return stringEmpty(user.getFirstName());
    }

    private boolean stringEmpty(final String valueToCheck) {
        return valueToCheck != null && valueToCheck.length() > 0;
    }
}