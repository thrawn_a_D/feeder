package de.wsorg.feeder.client.request.reading.concrete.feed;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import de.wsorg.feeder.client.request.reading.AbstractFeedRequestManager;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.FeedRequest;
import de.wsorg.feeder.shared.domain.feed.request.search.UserRelatedSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserFeedsRequestManager extends AbstractFeedRequestManager<OpmlWrapper, UserRelatedSearchQueryProxy> {
    private final ApplicationRequestFactory factory;
    private final UserOpmlAutoBeanFactory opmlAutoBeanFactory;
    private FeedRequest feedRequest;


    public interface UserOpmlAutoBeanFactory extends AutoBeanFactory {
        AutoBean<OpmlWrapper> feedproxy(OpmlWrapper delegate);
    }


    @Inject
    public UserFeedsRequestManager(final ApplicationRequestFactory factory,
                                   final UserOpmlAutoBeanFactory userFeedsAutoBeanFactory) {
        this.factory = factory;
        this.opmlAutoBeanFactory = userFeedsAutoBeanFactory;
        feedRequest = factory.feedRequest();
    }

    @Override
    protected void executeRequest(final UserRelatedSearchQueryProxy requestParams, final Receiver<OpmlWrapper> receiverOfRelatedCall) {
        feedRequest.findUserFeed(requestParams).fire(receiverOfRelatedCall);
        feedRequest = factory.feedRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final UserRelatedSearchQueryProxy requestParam) {
        return requestParam.getUserId() + "#@#" + requestParam.getSearchTerm();
    }

    @Override
    protected AutoBeanFactory getDomainAutoBean() {
        return opmlAutoBeanFactory;
    }

    @Override
    protected boolean cacheResponse() {
        return false;
    }

    @Override
    protected RequestContext getRequestContext() {
        return feedRequest;
    }
}
