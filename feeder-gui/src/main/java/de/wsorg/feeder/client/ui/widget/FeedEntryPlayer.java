package de.wsorg.feeder.client.ui.widget;

import com.bramosystems.oss.player.core.client.LoadException;
import com.bramosystems.oss.player.core.client.PluginNotFoundException;
import com.bramosystems.oss.player.core.client.PluginVersionException;
import com.bramosystems.oss.player.core.client.ui.FlashMediaPlayer;
import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.SimplePanel;

public class FeedEntryPlayer extends Composite {
    private static FeedEntryPlayerUiBinder uiBinder = GWT.create(FeedEntryPlayerUiBinder.class);    
    @UiField SimplePanel pnlPlayerField;
    interface FeedEntryPlayerUiBinder extends UiBinder<Widget, FeedEntryPlayer> {}
    
    private FlashMediaPlayer flashPlayer;
    
    public FeedEntryPlayer() {
		super();
	    initWidget(uiBinder.createAndBindUi(this));	 
	}
    
    public void loadMediaURL(String mediaURL) throws LoadException, PluginNotFoundException, PluginVersionException{
    	this.pnlPlayerField.clear();
		this.flashPlayer = new FlashMediaPlayer(mediaURL, false);
		this.pnlPlayerField.add(flashPlayer);
    }
}
