package de.wsorg.feeder.client.request;

import com.google.web.bindery.requestfactory.shared.BaseProxy;
import com.google.web.bindery.requestfactory.shared.Receiver;

/**
 *
 *
 *
 */
public interface RequestManager<T, P> {
    void processRequest(final P requestParams, final Receiver<T> responseReceiver, final Class<T> responseType);
    <D extends BaseProxy> D getNewDomainObjectForParam(final Class<D> classType);
}
