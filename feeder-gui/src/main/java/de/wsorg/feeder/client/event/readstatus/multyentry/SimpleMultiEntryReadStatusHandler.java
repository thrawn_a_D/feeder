package de.wsorg.feeder.client.event.readstatus.multyentry;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.client.request.modifying.concrete.feed.MultiReadStatusModifier;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatusProxy;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkageProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *
 */
public class SimpleMultiEntryReadStatusHandler implements MultiEntryReadStatusHandler {
    final MultiReadStatusModifier multiReadStatusModifier;
    private List<AtomEntryProxy> feedEntryList = new ArrayList<AtomEntryProxy>();
    private List<ClickHandler> hookedEvents = new ArrayList<ClickHandler>();

    @Inject
    public SimpleMultiEntryReadStatusHandler(final MultiReadStatusModifier multiReadStatusModifier) {
        this.multiReadStatusModifier = multiReadStatusModifier;
    }

    @Override
    public void onClick(final ClickEvent clickEvent) {
        if (this.feedEntryList != null && this.feedEntryList.size() > 0) {
            final FeedReadMarkageProxy markFeedEntries = multiReadStatusModifier.getNewDomainObjectForParam(FeedReadMarkageProxy.class);
            markFeedEntries.setFeedUrl("http://feeder.net/timeline");

            markFeedEntriesAsRead(markFeedEntries);

            multiReadStatusModifier.processRequest(markFeedEntries, getDefaultSubscriptionReceiver(), Void.class);

            informHookedEventHandlers();
        }
    }

    private void markFeedEntriesAsRead(final FeedReadMarkageProxy markFeedEntries) {
        markFeedEntries.setFeedEntriesReadMarkage(new ArrayList<FeedEntryReadStatusProxy>());
        for (AtomEntryProxy atomEntryProxy : feedEntryList) {
            if (!atomEntryProxy.isEntryAlreadyRead()) {
                String entryId = atomEntryProxy.getFeedUrl() + "@" + atomEntryProxy.getId();
                FeedEntryReadStatusProxy feedEntryReadStatusProxy = multiReadStatusModifier.getNewDomainObjectForParam(FeedEntryReadStatusProxy.class);
                feedEntryReadStatusProxy.setFeedEntryId(entryId);
                feedEntryReadStatusProxy.setFeedEntryReadStatus(true);
                markFeedEntries.getFeedEntriesReadMarkage().add(feedEntryReadStatusProxy);
            }
        }
    }

    private void informHookedEventHandlers() {
        for (ClickHandler hookedEvent : hookedEvents) {
            hookedEvent.onClick(null);
        }
    }

    @Override
    public void setFeedEntryList(final List<AtomEntryProxy> feedEntryList) {
        this.feedEntryList = feedEntryList;
    }

    @Override
    public void addHookEvent(final ClickHandler eventHandler) {
        this.hookedEvents.add(eventHandler);
    }

    private Receiver<Void> getDefaultSubscriptionReceiver() {
        return new Receiver<Void>() {
            @Override
            public void onSuccess(final Void aVoid) {

            }
        };
    }
}
