package de.wsorg.feeder.client.ui.cell;

import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import de.wsorg.feeder.client.event.readstatus.singleentry.SingleReadStatusHandler;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.ui.cell.wrapper.GwtDateTimeFormatter;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;
import de.wsorg.feeder.shared.domain.feed.response.atom.attributes.AtomEntryLinkProxy;

public class FeedEntryDetail extends AbstractFeederCell<AtomEntryProxy> {

	private final PlaceManager placeManager;
    private final StringUtils stringUtils;
    private final GwtDateTimeFormatter dateTimeFormatter;
    private final SingleReadStatusHandler singleReadStatusHandler;
    private final UserSession userSession;
    private AtomWrapper relatedAtom;

    @Inject
	public FeedEntryDetail(final PlaceManager placeManager,
                           final StringUtils stringUtils,
                           final GwtDateTimeFormatter dateTimeFormatter,
                           final SingleReadStatusHandler singleReadStatusHandler,
                           final UserSession userSession) {
		super(new String[] { "click", "keydown" }, stringUtils);
		this.placeManager = placeManager;
        this.stringUtils = stringUtils;
        this.dateTimeFormatter = dateTimeFormatter;
        this.singleReadStatusHandler = singleReadStatusHandler;
        this.userSession = userSession;
    }

	@Override
	public void onBrowserEvent(Context context, Element parent,
			AtomEntryProxy value, NativeEvent event,
			ValueUpdater<AtomEntryProxy> valueUpdater) {

		super.onBrowserEvent(context, parent, value, event, valueUpdater);

		if ("click".equals(event.getType())) {
			EventTarget eventTarget = event.getEventTarget();
			
	        if (parent.isOrHasChild(Element.as(eventTarget))) {
	            // use this to get the selected element!!
	            Element button = Element.as(eventTarget);
	 
	            // check if we really click on the image
	            if (button.getId().equalsIgnoreCase("BTNEXTENDEDVIEW")) {
					PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS_EXTENDED);
					request = request.with("guid",value.getId());
					History.newItem(this.placeManager.buildHistoryToken(request));
	            } else if (button.getId().equalsIgnoreCase("BTNSHOWPARENTFEED")) {
					PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS);
					request = request.with("feedAddress",value.getFeedUrl());
					History.newItem(this.placeManager.buildHistoryToken(request));
	            } else if (button.getId().equalsIgnoreCase("BTNMARKASREAD")) {
                    if (!isButtonActive(button)) {
                        markEntryAsRead(parent, value, button);
                    } else {
                        markEntryAsUnread(parent, value, button);
                    }
                } else {
                    if(relatedAtom.isFavoured())
                        markEntryAsRead(parent, value, null);
                }
			}
		}
	}

    private void markEntryAsUnread(final Element parent, final AtomEntryProxy value, final Element button) {
        NodeList<Element> tableElementList = parent.getElementsByTagName("table");

        final Element tableElement = tableElementList.getItem(0);
        Element thTitleValue = findThElement(parent, "thTitleValue");
        Element thTitleText = findThElement(parent, "thTitleText");
        button.replaceClassName("button white small white_keep_active", "button white small");
        tableElement.setClassName("unreadEntryShadow");
        thTitleValue.setClassName("unreadEntryGradient");
        thTitleText.replaceClassName("nobg", "unreadSpec");
        this.singleReadStatusHandler.markFeedEntryAsUnread(value.getFeedUrl(), value.getId());
    }

    private void markEntryAsRead(final Element parent, final AtomEntryProxy value, final Element button) {
        if (userSession.getCurrentUser() != null && !value.isEntryAlreadyRead()) {
            NodeList<Element> tableElementList = parent.getElementsByTagName("table");

            final Element tableElement = tableElementList.getItem(0);
            Element thTitleValue = findThElement(parent, "thTitleValue");
            Element thTitleText = findThElement(parent, "thTitleText");
            if (button != null) {
                button.replaceClassName("button white small", "button white small white_keep_active");
            }
            tableElement.replaceClassName("unreadEntryShadow", "NONE");
            thTitleValue.replaceClassName("unreadEntryGradient", "NONE");
            thTitleText.replaceClassName("unreadSpec", "nobg");
            this.singleReadStatusHandler.markFeedEntryAsRead(value.getFeedUrl(), value.getId());
        }
    }

    private Element findThElement(final Element parent, final String elementId) {
        Element thElementToLookFor = null;
        final NodeList<Element> thElementList = parent.getElementsByTagName("th");

        for (int i = 0; i < thElementList.getLength(); i++) {
            Element thElement = thElementList.getItem(i);
            if(thElement.getId().equals(elementId)) {
                thElementToLookFor = thElement;
                break;
            }
        }
        return thElementToLookFor;
    }

    @Override
	public void render(Context context, AtomEntryProxy value, SafeHtmlBuilder sb) {
		
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}

        String entryReadStyle = getEntryReadStyle(value);

		sb.appendHtmlConstant("<table id='cellItem' "+entryReadStyle+" width='100%' cellspacing='0'>");
        setUserFeedColor(value, sb);
        setTitle(value, sb);
        setUpdatedDate(value, sb);
        setDescription(value, sb);
        this.setTwoColumnEntryInTable("Actions", this.getActionBarHTML(value), "specalt", sb, false);
		sb.appendHtmlConstant("</table>");
	}

    private void setTitle(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        if (!setLinkedTitle(value, sb)) {
            this.setTitle(value.getTitle(), value.getFeedUrl(), !value.isEntryAlreadyRead(), sb);
        }
    }

    private boolean setLinkedTitle(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        if (value.getLinks() != null && value.getLinks().size() > 0) {
            for (AtomEntryLinkProxy atomEntryLinkProxy : value.getLinks()) {
                if(stringUtils.isBlank(atomEntryLinkProxy.getLinkType().getLinkType()) || atomEntryLinkProxy.getLinkType().isHtml()) {
                    this.setTitle(value.getTitle(), atomEntryLinkProxy.getHref(), value.getFeedUrl(), !value.isEntryAlreadyRead(), sb);
                    return true;
                }
            }
        }

        return false;
    }

    private void setDescription(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        if (!setImageDescriptionRow(value, sb)) {
            this.setTwoColumnEntryInTable("Description", value.getDescription(), "spec", sb, false);
        }
    }

    private boolean setImageDescriptionRow(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        boolean descriptionSet = false;
        if (value.getLinks() != null && value.getLinks().size() > 0) {
            for (AtomEntryLinkProxy atomEntryLinkProxy : value.getLinks()) {
                if(atomEntryLinkProxy.getLinkType().isImage()) {
                    String imgValue = "<img src=\"" + atomEntryLinkProxy.getHref() + "\" >";
                    this.setTwoColumnEntryInTable(imgValue, value.getDescription(), "spec", sb, false);
                    descriptionSet = true;
                }
            }
        }
        return descriptionSet;
    }

    private String getEntryReadStyle(final AtomEntryProxy value) {
        String entryReadStyle = "";
        if(!value.isEntryAlreadyRead())
            entryReadStyle = "class='unreadEntryShadow'";
        return entryReadStyle;
    }

    private int getRowSpanNumber(final AtomEntryProxy value){
        int rowCount = 3;
        if(value.getUpdated() != null)
            rowCount++;
        if(!stringUtils.isBlank(value.getDescription()))
            rowCount++;
        return rowCount;
    }

    private void setUserFeedColor(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        if(!stringUtils.isBlank(value.getFeedUiHighlightingColor())) {
            int rowSpanNumber = getRowSpanNumber(value);
            sb.appendHtmlConstant("<tr>");
            sb.appendHtmlConstant("<th rowspan='"+rowSpanNumber+"' class='coloring' style='background-color: "+value.getFeedUiHighlightingColor()+"'/>");
            sb.appendHtmlConstant("</tr>");
        }
    }

    private void setUpdatedDate(final AtomEntryProxy value, final SafeHtmlBuilder sb) {
        if(value.getUpdated() != null) {
            final String formattedDate = dateTimeFormatter.getFormat(DateTimeFormat.PredefinedFormat.DATE_TIME_MEDIUM).format(value.getUpdated());
            this.setTwoColumnEntryInTable("Updated", formattedDate, "spec", sb, false);
        }
    }

    private String getActionBarHTML(final AtomEntryProxy value){
		String actionBarHTML = "";

		actionBarHTML += "<img class='button white small' id='btnShowParentFeed' src='cellStyleItem/152-rolodex.png' height='10' width='10' style='margin-right:5px;' title='Show feed' alt='Show feed'/>";
        actionBarHTML = setExternalLinkButton(value, actionBarHTML);

        if(userSession.getCurrentUser() != null) {
            actionBarHTML = addMarkAsReadButton(value, actionBarHTML);
        }

		return actionBarHTML;
	}

    private String setExternalLinkButton(final AtomEntryProxy value, String actionBarHTML) {
        if (value.getLinks() != null) {
            for (AtomEntryLinkProxy atomEntryLinkProxy : value.getLinks()) {
                if(atomEntryLinkProxy.getLinkType() != null && atomEntryLinkProxy.getLinkType().isHtml()) {
                    actionBarHTML += "<a href=\"" + atomEntryLinkProxy.getHref() + "\" target=\"_blank\" ><img class='button white small' id='btnShowExternalLink' src='cellStyleItem/113-navigation.png' height='10' width='10' style='margin-right:5px;' title='Show article' alt='Show article'/></a>";
                    break;
                }
            }
        }
        return actionBarHTML;
    }

    private String addMarkAsReadButton(final AtomEntryProxy value, String actionBarHTML) {
        if (relatedAtom != null && relatedAtom.isFavoured()) {
            String readButtonActiveClass="";
            String text = "Mark as read/unread";
            if(value.isEntryAlreadyRead()) {
                readButtonActiveClass=" white_keep_active";
            }
            actionBarHTML += "<img class='button white small"+readButtonActiveClass+"' id='btnMarkAsRead' src='cellStyleItem/40-inbox.png' height='10' width='10' style='margin-right:5px;' title='"+text+"' alt='"+text+"'/>";
        }
        return actionBarHTML;
    }

    public void setRelatedAtom(final AtomWrapper atom) {
        this.relatedAtom = atom;
    }
}