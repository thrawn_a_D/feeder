package de.wsorg.feeder.client.utils;

import com.google.gwt.core.client.GWT;
import com.google.inject.Inject;
import de.wsorg.feeder.client.utils.wrapper.GwtStaticClassWrapper;

public class StringUtils {

    private GwtStaticClassWrapper gwtStaticClassWrapper;

	public boolean unsafeEquals(String a, String b) {
		if (gwtStaticClassWrapper.isScript()) {
			return a == b;
		} else {
			return a.equals(b);
		}
	}
	
	public boolean isBlank(String value){
        return value == null || this.unsafeEquals(value, "");
    }

    @Inject
    public void setGwtStaticClassWrapper(final GwtStaticClassWrapper gwtStaticClassWrapper) {
        this.gwtStaticClassWrapper = gwtStaticClassWrapper;
    }
}