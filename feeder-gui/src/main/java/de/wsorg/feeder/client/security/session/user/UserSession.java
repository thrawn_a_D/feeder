package de.wsorg.feeder.client.security.session.user;

import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 *
 *
 *
 */
public interface UserSession {
    void registerUserInSession(final FeederUserProxy feederUser);
    FeederUserProxy getCurrentUser();
    void cancelUserSession();
}
