package de.wsorg.feeder.client.presenter;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;
import com.gwtplatform.mvp.client.proxy.RevealRootContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.event.message.ShowInfoMessageEvent;
import de.wsorg.feeder.client.security.event.UserLoggedInEvent;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

public class MainPagePresenter extends Presenter<MainPagePresenter.MainPageView, MainPagePresenter.MainPageProxy> {
    /**
     * Child presenters can fire a RevealContentEvent with TYPE_SetMainContent to set themselves
     * as children of this presenter.
     */
    @ContentSlot
    public static final Type<RevealContentHandler<?>> TYPE_SetMainContent = new Type<RevealContentHandler<?>>();

    public interface MainPageView extends View {
        void showErrorMessage(final String errorMessage, final boolean stayVisible);
        void showInfoMessage(final String infoMessage, final boolean stayVisible);
        void enableMenuForUser(final FeederUserProxy user);
    }
      
    @ProxyStandard
    public interface MainPageProxy extends Proxy<MainPagePresenter> {}
    
    private final EventBus eventBus;
    private final MainPageView view;

    @Inject
    public MainPagePresenter(final EventBus eventBus,
                             final MainPageView view,
                             final MainPageProxy proxy) {
      super(eventBus, view, proxy);
      
      this.eventBus = eventBus;
        this.view = view;
    }

    @Override
    protected void onBind() {
        super.onBind();
        bindErrorMessageEventHandler();
        bindInfoMessageEventHandler();
        registerUserLoggedInEventHandler();
    }

    private void registerUserLoggedInEventHandler() {
        addRegisteredHandler(UserLoggedInEvent.getType(), new UserLoggedInEvent.UserLoggedInHandler() {
            @Override
            public void onShowMessage(final UserLoggedInEvent event) {
                view.enableMenuForUser(event.getUser());
            }
        } );
    }

    private void bindInfoMessageEventHandler() {
        addRegisteredHandler(ShowInfoMessageEvent.getType(), new ShowInfoMessageEvent.ShowInfoMessageHandler() {
            @Override
            public void onShowMessage(final ShowInfoMessageEvent event) {
                view.showInfoMessage(event.getMessage(), event.isStayVisible());
            }
        } );
    }

    private void bindErrorMessageEventHandler() {
        addRegisteredHandler(ShowErrorMessageEvent.getType(), new ShowErrorMessageEvent.ShowErrorMessageHandler() {
            @Override
            public void onShowMessage(final ShowErrorMessageEvent event) {
                view.showErrorMessage(event.getMessage(), event.isStayVisible());
            }
        } );
    }

    @Override
    protected void revealInParent() {
      RevealRootContentEvent.fire(this.eventBus, this);
    }
  }