package de.wsorg.feeder.client.security;

import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;
import de.wsorg.feeder.client.security.session.user.UserSession;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class LogInGateKeeper implements Gatekeeper {
    private UserSession userSession;

    @Inject
    public LogInGateKeeper(final UserSession userSession) {
        this.userSession = userSession;
    }

    @Override
    public boolean canReveal() {
        return isUserLoggedIn();
    }

    private boolean isUserLoggedIn() {
        return userSession.getCurrentUser() != null;
    }
}
