package de.wsorg.feeder.client.ui.authentication.login;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.presenter.authentication.login.LoginPagePresenter;
import de.wsorg.feeder.client.ui.widget.common.button.FeederButton;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class LoginPageViewImpl extends ViewImpl implements LoginPagePresenter.LoginPageView {

    public interface LoginPageViewUiBinder extends UiBinder<Widget, LoginPageViewImpl> {}

    public Widget widget;

    @UiField
    FeederButton fbLogin;
    @UiField
    TextBox tbUserName;
    @UiField
    TextBox tbPassword;


    @UiField
    FeederButton fbCreateNewAccount;

    @Inject
    public LoginPageViewImpl(final LoginPageViewUiBinder uiBinder) {
        this.widget = uiBinder.createAndBindUi(this);
    }

    @UiFactory
    public FeederButton makeFeederButton(){
        return new FeederButton();
    }

    @Override
    public Widget asWidget() {
      return this.widget;
    }

    @Override
    public void registerLoginClickHandler(final ClickHandler loginEvent) {
        this.fbLogin.addClickHandler(loginEvent);
    }

    @Override
    public void registerCreateNewAccountClickHandler(final ClickHandler newAccountEvent) {
        this.fbCreateNewAccount.addClickHandler(newAccountEvent);
    }

    @Override
    public String getUserName() {
        return tbUserName.getText();
    }

    @Override
    public String getPassword() {
        return tbPassword.getText();
    }
}
