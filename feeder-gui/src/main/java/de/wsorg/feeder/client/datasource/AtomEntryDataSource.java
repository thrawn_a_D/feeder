package de.wsorg.feeder.client.datasource;

import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy;

import java.util.List;


/**
 * The data source for AtomProxy information used in the sample.
 */
public class AtomEntryDataSource {

	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<AtomEntryProxy> FeedEntryProxy_KEY_PROVIDER = new ProvidesKey<AtomEntryProxy>() {
		public Object getKey(AtomEntryProxy item) {
			return item == null ? null : item.getTitle();
		}
	};

	/**
	 * The singleton instance of the database.
	 */
	private static AtomEntryDataSource instance;

	/**
	 * Get the singleton instance of the AtomProxy database.
	 * 
	 * @return the singleton instance
	 */
	public static AtomEntryDataSource get() {
		if (instance == null) {
			instance = new AtomEntryDataSource();
		}
		return instance;
	}

	/**
	 * The provider that holds the list of FeedProxys in the database.
	 */
	private ListDataProvider<AtomEntryProxy> dataProvider = new ListDataProvider<AtomEntryProxy>();

	/**
	 * Construct a new AtomProxy database.
	 */
	private AtomEntryDataSource() {

	}

	/**
	 * Add a new AtomProxy.
	 * 
	 * @param atomProxy
	 *            the AtomProxy to add.
	 */
	public void addFeedProxy(AtomEntryProxy atomProxy) {
		List<AtomEntryProxy> atomEntryProxies = dataProvider.getList();
		// Remove the AtomProxy first so we don't add a duplicate.
		atomEntryProxies.remove(atomProxy);
		atomEntryProxies.add(atomProxy);
	}
	
	/**
	 * Set a whole set of entries.
	 * @param atomProxies
	 */
	public void setEntryList(List<AtomEntryProxy> atomProxies){
		this.dataProvider.setList(atomProxies);
	}

	/**
	 * Add a display to the database. The current range of interest of the
	 * display will be populated with data.
	 * 
	 * @param display
	 *            a {@Link HasData}.
	 */
	public void addDataDisplay(HasData<AtomEntryProxy> display) {
		dataProvider.addDataDisplay(display);
	}

	public ListDataProvider<AtomEntryProxy> getDataProvider() {
		return dataProvider;
	}

	/**
	 * Refresh all displays.
	 */
	public void refreshDisplays() {
		dataProvider.refresh();
	}
	
	/**
	 * Clear current data-content.
	 */
	public void clear() {
		dataProvider.getList().clear();
	}
}