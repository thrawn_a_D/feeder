package de.wsorg.feeder.client.ui.authentication.newaccount;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiFactory;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import de.wsorg.feeder.client.presenter.authentication.newaccount.NewAccountPagePresenter;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserPassword;
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.PasswordType;
import de.wsorg.feeder.client.ui.widget.common.button.FeederButton;
import de.wsorg.feeder.client.ui.widget.common.textbox.ValidatedTextBox;
import de.wsorg.feeder.client.ui.widget.common.textbox.validator.EmailValidator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class NewAccountPageViewImpl extends ViewImpl implements NewAccountPagePresenter.NewAccountPageView{


    public interface NewAccountPageViewUiBinder extends UiBinder<Widget, NewAccountPageViewImpl> {}

    public Widget widget;

    @UiField FeederButton fbCreateAccount;
    @UiField FeederButton fbLoginView;
    @UiField TextBox tbUserName;
    @UiField PasswordTextBox tbPassword;
    @UiField ValidatedTextBox tbEmail;
    @UiField TextBox tbFirstName;
    @UiField TextBox tbLastName;

    @Inject
    public NewAccountPageViewImpl(final NewAccountPageViewUiBinder uiBinder) {
        this.widget = uiBinder.createAndBindUi(this);
        tbEmail.addValidator(new EmailValidator());
    }

    @UiFactory
    public FeederButton makeFeederButton(){
        return new FeederButton();
    }

    @Override
    public Widget asWidget() {
        return widget;
    }



    @Override
    public void registerCreateAccountClickHandler(final ClickHandler newAccountEvent) {
        fbCreateAccount.addClickHandler(newAccountEvent);
    }

    @Override
    public void registerLoginClickHandler(final ClickHandler loginEvent) {
        fbLoginView.addClickHandler(loginEvent);
    }

    @Override
    public ClientUserData getUserAccountData() {
        ClientUserData userData = new ClientUserData();
        userData.setUserName(this.tbUserName.getText());
        ClientUserPassword userPassword = new ClientUserPassword(this.tbPassword.getText(), PasswordType.PLAIN_TEXT);
        userData.setPassword(userPassword);
        userData.seteMail(this.tbEmail.getText());
        userData.setFirstName(this.tbFirstName.getText());
        userData.setLastName(this.tbLastName.getText());
        return userData;
    }
}
