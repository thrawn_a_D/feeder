package de.wsorg.feeder.client.resources;

import com.google.gwt.user.cellview.client.CellList;

public interface FeedCellListResources extends CellList.Resources {
	@Source({ "feederCellList.css" })
	ListStyle cellListStyle();
}
