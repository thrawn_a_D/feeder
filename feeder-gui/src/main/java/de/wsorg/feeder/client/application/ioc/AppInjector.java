package de.wsorg.feeder.client.application.ioc;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.inject.client.AsyncProvider;
import com.google.gwt.inject.client.Ginjector;
import com.google.inject.Provider;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.application.DesktopApp;
import de.wsorg.feeder.client.presenter.MainPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.login.LoginPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.newaccount.NewAccountPagePresenter;
import de.wsorg.feeder.client.presenter.authentication.user.UserSettingsOverviewPresenter;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsExtendedPresenter;
import de.wsorg.feeder.client.presenter.common.feeddetails.FeedDetailsPresenter;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.presenter.getfeeds.FoundFeedListPresenter;
import de.wsorg.feeder.client.presenter.myfeeds.MyFeedsListPresenter;
import de.wsorg.feeder.client.presenter.timeline.MyFeedTimeLinePresenter;
import de.wsorg.feeder.client.security.LogInGateKeeper;

public interface AppInjector extends Ginjector {
    DesktopApp getFeederApp();    

    Provider<MainPagePresenter> getMainPagePresenter();
    Provider<MainFeedListContainerPresenter> getMainFeedListContainerPresenter();
    AsyncProvider<FeedDetailsPresenter> getFeedItemDetailsPresenter();
    AsyncProvider<FoundFeedListPresenter> getFeedFoundListPresenter();
    AsyncProvider<MyFeedTimeLinePresenter> getMyFeedTimeLinePresenter();
    AsyncProvider<LoginPagePresenter> getLoginPagePresenter();
    AsyncProvider<NewAccountPagePresenter> getNewAccountPagePresenter();
    AsyncProvider<MyFeedsListPresenter> getMyFeedsListPresenter();
    AsyncProvider<FeedDetailsExtendedPresenter> getFeedDetailsExtendedPresenter();
    AsyncProvider<UserSettingsOverviewPresenter> getUserSettingsOverviewPresenter();
    EventBus getEventBus();
    PlaceManager getPlaceManager();
    LogInGateKeeper getLoggedInGatekeeper();
}