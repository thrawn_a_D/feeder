package de.wsorg.feeder.client.presenter.common.feedlist;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;
import de.wsorg.feeder.client.presenter.MainPagePresenter;

public class MainFeedListContainerPresenter
		extends
		Presenter<MainFeedListContainerPresenter.MainFeedListContainerView, MainFeedListContainerPresenter.MainFeedListContainerProxy> {

	@ContentSlot
    public static final Type<RevealContentHandler<?>> TYPE_SetFeedView = new Type<RevealContentHandler<?>>();

	public interface MainFeedListContainerView extends View {
        void resetVew();
    }

    @ProxyStandard
    public interface MainFeedListContainerProxy extends Proxy<MainFeedListContainerPresenter> {}

	@Inject
	public MainFeedListContainerPresenter(final EventBus eventBus, final MainFeedListContainerView view,
                                          final MainFeedListContainerProxy proxy) {
		super(eventBus, view, proxy);
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this,
				MainPagePresenter.TYPE_SetMainContent, this);
	}
}
