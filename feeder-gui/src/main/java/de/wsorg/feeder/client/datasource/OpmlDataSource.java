package de.wsorg.feeder.client.datasource;

import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;
import de.wsorg.feeder.shared.domain.feed.response.opml.OutlineProxy;

import java.util.List;


/**
 * The data source for AtomProxy information used in the sample.
 */
public class OpmlDataSource {

	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<OutlineProxy> FeedProxy_KEY_PROVIDER = new ProvidesKey<OutlineProxy>() {
		public Object getKey(OutlineProxy item) {
			return item == null ? null : item.getTitle();
		}
	};

	/**
	 * The singleton instance of the database.
	 */
	private static OpmlDataSource instance;

	/**
	 * Get the singleton instance of the AtomProxy database.
	 * 
	 * @return the singleton instance
	 */
	public static OpmlDataSource get() {
		if (instance == null) {
			instance = new OpmlDataSource();
		}
		return instance;
	}

	/**
	 * The provider that holds the list of FeedProxys in the database.
	 */
	private ListDataProvider<OutlineProxy> dataProvider = new ListDataProvider<OutlineProxy>();

	/**
	 * Construct a new AtomProxy database.
	 */
	private OpmlDataSource() {

	}

	/**
	 * Add a new atomProxy.
	 * 
	 * @param atomProxy
	 *            the atomProxy to add.
	 */
	public void addFeedProxy(OutlineProxy atomProxy) {
		List<OutlineProxy> atomProxies = dataProvider.getList();
		// Remove the atomProxy first so we don't add a duplicate.
		atomProxies.remove(atomProxy);
		atomProxies.add(atomProxy);
	}
	
	/**
	 * Set a whole set of entries.
	 * @param atomProxies
	 */
	public void setEntryList(OpmlWrapper atomProxies){
		this.dataProvider.setList(atomProxies.getBody().getSearchResultList());
	}

	/**
	 * Add a display to the database. The current range of interest of the
	 * display will be populated with data.
	 * 
	 * @param display
	 *            a {@Link HasData}.
	 */
	public void addDataDisplay(HasData<OutlineProxy> display) {
		dataProvider.addDataDisplay(display);
	}

	public ListDataProvider<OutlineProxy> getDataProvider() {
		return dataProvider;
	}

	/**
	 * Refresh all displays.
	 */
	public void refreshDisplays() {
		dataProvider.refresh();
	}
	
	/**
	 * Clear current data-content.
	 */
	public void clear() {
		dataProvider.getList().clear();
	}
}