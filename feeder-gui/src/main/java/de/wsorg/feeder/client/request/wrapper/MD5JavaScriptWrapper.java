package de.wsorg.feeder.client.request.wrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * <p/>
 *
 *
 */
public class MD5JavaScriptWrapper {
    public String hashPassword(final String password) {
        return md5Password(password);
    }

    native String md5Password(String password) /*-{
        return $wnd.MD5(password);
    }-*/;
}
