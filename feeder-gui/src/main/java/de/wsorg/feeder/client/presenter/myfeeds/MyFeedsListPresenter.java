package de.wsorg.feeder.client.presenter.myfeeds;

import com.google.gwt.event.shared.EventBus;
import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import com.google.web.bindery.requestfactory.shared.ServerFailure;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.annotations.UseGatekeeper;
import com.gwtplatform.mvp.client.proxy.Place;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import de.wsorg.feeder.client.event.message.ShowErrorMessageEvent;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.presenter.common.feedlist.FeedListView;
import de.wsorg.feeder.client.presenter.common.feedlist.MainFeedListContainerPresenter;
import de.wsorg.feeder.client.request.reading.concrete.feed.UserFeedsRequestManager;
import de.wsorg.feeder.client.security.LogInGateKeeper;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.search.UserRelatedSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class MyFeedsListPresenter extends
        Presenter<MyFeedsListPresenter.MyFeedListView, MyFeedsListPresenter.MyFeedsListProxy> {

    private final EventBus eventBus;
    private final MyFeedListView view;
    private final UserFeedsRequestManager userFeedsRequestManager;
    private final ApplicationRequestFactory factory;
    private final UserSession userSession;

    public interface MyFeedListView extends FeedListView {}

    @Inject
    public MyFeedsListPresenter(final EventBus eventBus,
                             final MyFeedListView view,
                             final MyFeedsListProxy proxy,
                             final UserFeedsRequestManager requestManager,
                             final ApplicationRequestFactory factory,
                             final UserSession userSession) {
        super(eventBus, view, proxy);
        this.eventBus = eventBus;
        this.view = view;
        this.userFeedsRequestManager = requestManager;
        this.factory = factory;
        this.userSession = userSession;
    }

    @ProxyCodeSplit
    @NameToken(PlaceNameToken.MY_FEEDS)
    @UseGatekeeper(LogInGateKeeper.class)
    public interface MyFeedsListProxy extends
            ProxyPlace<MyFeedsListPresenter>, Place {
    }

    @Override
    public void prepareFromRequest(PlaceRequest placeRequest) {
        final MyFeedsListPresenter currentPresenter = this;
        Receiver<OpmlWrapper> receiver = new Receiver<OpmlWrapper>() {
            @Override
            public void onSuccess(OpmlWrapper response) {
                if (response != null && response.getBody().getSearchResultList().size() > 0) {
                    view.setFeedToList(response);
                } else {
                    view.showNoResultsFoundMessage();
                }
            }

            @Override
            public void onFailure(ServerFailure error) {
                ShowErrorMessageEvent.fire(currentPresenter, error.getMessage());
            }
        };

        UserRelatedSearchQueryProxy request = this.userFeedsRequestManager.getNewDomainObjectForParam(UserRelatedSearchQueryProxy.class);
        this.userFeedsRequestManager.processRequest(request ,receiver, OpmlWrapper.class);
    }

    @Override
    protected void revealInParent() {
        RevealContentEvent.fire(this.eventBus,
                MainFeedListContainerPresenter.TYPE_SetFeedView, this);
    }
}
