package de.wsorg.feeder.client.ui.widget.common.message;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.ParagraphElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederMessagePopUp extends Composite {
    private static FeederMessagePopUpUiBinder uiBinder = GWT.create(FeederMessagePopUpUiBinder.class);

    interface FeederMessagePopUpUiBinder extends UiBinder<Widget, FeederMessagePopUp> {}

    final int ERROR_MESSAGE_VISIBLE_DELAY = 10000;

    @UiField
    ParagraphElement lblMessageText;
    @UiField
    HTMLPanel pnlMessageField;
    @UiField
    DivElement divMessageField;
    @UiField
    Anchor hrefMessageClose;

    public FeederMessagePopUp() {
        Widget thisWidget = uiBinder.createAndBindUi(this);
        initWidget(thisWidget);

        hrefMessageClose.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                pnlMessageField.setVisible(false);
            }
        });
    }

    public void showErrorMessage(final String errorMessage, final boolean stayVisible) {
        pnlMessageField.setVisible(true);
        lblMessageText.setInnerText(errorMessage);
        divMessageField.setClassName("error");

        if(!stayVisible) {
            getMessageRemovalTimer();
        }
    }

    public void showInfoMessage(final String infoMessage, final boolean stayVisible) {
        pnlMessageField.setVisible(true);
        lblMessageText.setInnerText(infoMessage);
        divMessageField.setClassName("message");
        if(!stayVisible) {
            getMessageRemovalTimer();
        }
    }

    private Timer getMessageRemovalTimer() {
        final Timer timer = new Timer() {
            @Override
            public void run() {
                pnlMessageField.setVisible(false);
                lblMessageText.setInnerText("");
            }
        };
        timer.schedule(ERROR_MESSAGE_VISIBLE_DELAY);
        return timer;
    }
}
