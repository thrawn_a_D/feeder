package de.wsorg.feeder.client.ui.cell;

import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.EventTarget;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.History;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import de.wsorg.feeder.client.event.subscribtion.SubscriptionHandler;
import de.wsorg.feeder.client.place.PlaceNameToken;
import de.wsorg.feeder.client.security.session.user.UserSession;
import de.wsorg.feeder.client.ui.cell.wrapper.GwtDomElement;
import de.wsorg.feeder.client.utils.StringUtils;
import de.wsorg.feeder.shared.domain.feed.response.opml.OutlineProxy;

public class FeedListItem extends AbstractFeederCell<OutlineProxy> {
	
	private final PlaceManager placeManager;
    private final StringUtils stringUtils;
    private final GwtDomElement gwtDomElement;
    private SubscriptionHandler subscriptionHandler;
    private final UserSession userSession;

    @Inject
    public FeedListItem(final PlaceManager placeManager,
                        final StringUtils stringUtils,
                        final GwtDomElement element,
                        final SubscriptionHandler subscriptionHandler,
                        final UserSession userSession) {
		super(new String[] { "click", "keydown" }, stringUtils);
		this.placeManager = placeManager;
        this.stringUtils = stringUtils;
        gwtDomElement = element;
        this.subscriptionHandler = subscriptionHandler;
        this.userSession = userSession;
    }

	@Override
	public void onBrowserEvent(Context context, Element parent,
                               OutlineProxy value, NativeEvent event,
			                   ValueUpdater<OutlineProxy> valueUpdater) {

		super.onBrowserEvent(context, parent, value, event, valueUpdater);

		if ("click".equals(event.getType())) {
			EventTarget eventTarget = event.getEventTarget();

            final Element childElement = gwtDomElement.as(eventTarget);
            if (parent.isOrHasChild(childElement)) {

	            // check if we really click on the image
	            if (childElement.getId().equalsIgnoreCase("BTNPREVIEW")) {
					PlaceRequest request = new PlaceRequest(PlaceNameToken.FEED_DETAILS);
					request = request.with("feedAddress",value.getXmlUrl());
					History.newItem(this.placeManager.buildHistoryToken(request));
	            } else if (childElement.getId().equalsIgnoreCase("BTNLOGINTOSUBSCRIBE")) {
                    PlaceRequest request = new PlaceRequest(PlaceNameToken.AUTHENTICATION_LOGIN_PAGE);
                    History.newItem(this.placeManager.buildHistoryToken(request));
                } else if (childElement.getId().equalsIgnoreCase("BTNSUBSCRIBE")) {

                    if (!isButtonActive(childElement)) {
                        childElement.replaceClassName("button white small", "button white small white_keep_active");
                        this.subscriptionHandler.onSubscribeFeed(value.getXmlUrl());
                    } else {
                        childElement.replaceClassName("button white small white_keep_active", "button white small");
                        this.subscriptionHandler.onCancelSubscription(value.getXmlUrl());
                    }
                }
			}
		}
	}

    @Override
	public void render(final Context context,
                       final OutlineProxy value,
			           final SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
		sb.appendHtmlConstant("<table id='cellItem' class='unreadEntryShadow' width='100%' cellspacing='0'>");
        setUserFeedColor(value, sb);
		this.setTitle(value.getTitle(), value.getXmlUrl(), false, sb);
		this.setTwoColumnEntryInTable("Description", value.getDescription(), "spec", sb, false);
		this.setTwoColumnEntryInTable("Actions", this.getActionBarHTML(value), "specalt", sb, false);
		sb.appendHtmlConstant("</table>");
	}
	
	private String getActionBarHTML(final OutlineProxy value){
		String actionBarHTML = "";


        actionBarHTML += "<img class='button white small' id='btnPreview' src='cellStyleItem/164-glasses-2.png' height='10' width='10' style='margin-right:5px;' alt='Preview' title='Preview'/>";

        if(userSession.getCurrentUser() != null) {
            actionBarHTML = addSubscriptionButton(value, actionBarHTML);
        } else {
            actionBarHTML = addDummySubscriptionButton(actionBarHTML);
        }

		return actionBarHTML;
	}

    private int getRowSpanNumber(final OutlineProxy value){
        int rowCount = 3;
        if(!stringUtils.isBlank(value.getDescription()))
            rowCount++;
        return rowCount;
    }

    private void setUserFeedColor(final OutlineProxy value, final SafeHtmlBuilder sb) {
        if(!stringUtils.isBlank(value.getFeedUiHighlightingColor())) {
            int rowSpanNumber = getRowSpanNumber(value);
            sb.appendHtmlConstant("<tr>");
            sb.appendHtmlConstant("<th rowspan='"+rowSpanNumber+"' class='coloring' style='background-color: "+value.getFeedUiHighlightingColor()+"'/>");
            sb.appendHtmlConstant("</tr>");
        }
    }

    private String addDummySubscriptionButton(String actionBarHTML) {
        String buttonText = "Login to subscribe to feed";
        actionBarHTML += "<img id='btnLoginToSubscribe' class='button white small' src='cellStyleItem/117-todo.png' height='10' width='10' style='margin-right:5px;' title='"+buttonText+"' alt='"+buttonText+"'/>";
        return actionBarHTML;
    }

    private String addSubscriptionButton(final OutlineProxy value, String actionBarHTML) {
        String subscriptionButtonActiveClass="";
        if(value.isFavoured()) {
            subscriptionButtonActiveClass="white_keep_active";
        }
        actionBarHTML += "<img id='btnSubscribe' class='button white small "+ subscriptionButtonActiveClass +" 'src='cellStyleItem/117-todo.png' height='10' width='10' style='margin-right:5px;' alt='Subscribe/Unsubscribe' title='Subscribe/Unsubscribe'/>";
        return actionBarHTML;
    }
}
