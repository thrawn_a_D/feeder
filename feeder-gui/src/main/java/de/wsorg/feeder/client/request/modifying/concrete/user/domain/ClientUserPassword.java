package de.wsorg.feeder.client.request.modifying.concrete.user.domain;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * 
 *
 *
 */
public class ClientUserPassword {
    private String passwordText;
    private PasswordType passwordType;


    public ClientUserPassword() {
    }

    public ClientUserPassword(final String passwordText, final PasswordType passwordType) {
        this.passwordText = passwordText;
        this.passwordType = passwordType;
    }

    public PasswordType getPasswordType() {
        return passwordType;
    }

    public void setPasswordType(final PasswordType passwordType) {
        this.passwordType = passwordType;
    }

    public String getPasswordText() {
        return passwordText;
    }

    public void setPasswordText(final String password) {
        this.passwordText = password;
    }
}
