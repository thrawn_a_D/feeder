package de.wsorg.feeder.client.security.session.user;

import com.google.inject.Inject;
import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import de.wsorg.feeder.client.request.caching.util.GwtBeanUtil;
import de.wsorg.feeder.client.security.session.ClientSessionStore;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;

/**
 *
 *
 *
 */
public class SimpleUserSession implements UserSession {

    private static final String STORE_OBJECT_ID = "loggedInUser";

    private GwtBeanUtil<FeederUserProxy> gwtBeanUtil;
    private final UserBeanFactory userBeanFactory;
    private final ClientSessionStore clientSessionStore;

    public interface UserBeanFactory extends AutoBeanFactory {
        AutoBean<FeederUserProxy> feedproxy(FeederUserProxy delegate);
    }

    @Inject
    public SimpleUserSession(final GwtBeanUtil gwtBeanUtil,
                             final SimpleUserSession.UserBeanFactory userBeanFactory,
                             final ClientSessionStore clientSessionStore) {
        this.gwtBeanUtil = gwtBeanUtil;
        this.userBeanFactory = userBeanFactory;
        this.clientSessionStore = clientSessionStore;
    }

    @Override
    public void registerUserInSession(final FeederUserProxy feederUser) {
        if(feederUser != null) {
            AutoBean<FeederUserProxy> autoBean = gwtBeanUtil.getAutoBean(feederUser);
            final String objectAsJson=gwtBeanUtil.encodeAutoBeanToJson(autoBean);
            clientSessionStore.putSessionValue(STORE_OBJECT_ID, objectAsJson);
        }
    }

    @Override
    public FeederUserProxy getCurrentUser() {
        String jsonUser = clientSessionStore.getValueFromSession(STORE_OBJECT_ID);
        if(jsonUser != null) {
            AutoBean<FeederUserProxy> autoBean = gwtBeanUtil.decodeJsonToAutoBean(this.userBeanFactory, jsonUser, FeederUserProxy.class);
            return autoBean.as();
        } else {
            return null;
        }
    }

    @Override
    public void cancelUserSession() {
        clientSessionStore.removeSessionValue(STORE_OBJECT_ID);
    }
}
