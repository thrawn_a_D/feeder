package de.wsorg.feeder.client.event.readstatus.all;

import com.google.gwt.event.shared.EventHandler;
import com.google.web.bindery.requestfactory.shared.Receiver;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
public interface AllEntriesReadStatusHandler extends EventHandler {
    void markAllFeedsAsRead();
    void markAllFeedsAsUnread();
    void setEventReceiver(final Receiver<Void> eventReceiver);
}
