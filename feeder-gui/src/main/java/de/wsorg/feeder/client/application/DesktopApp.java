package de.wsorg.feeder.client.application;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import de.wsorg.feeder.client.resources.jsvascript.FeederJavaScriptInjector;
import de.wsorg.feeder.client.resources.jsvascript.MD5Script;
import de.wsorg.feeder.shared.ApplicationRequestFactory;

public class DesktopApp extends FeederApp {
    private SimplePanel appWidget = new SimplePanel();
    private final EventBus eventBus;
    private final ApplicationRequestFactory requestFactory;
    private final PlaceManager placeManager;

    @Inject
    public DesktopApp(EventBus eventBus, PlaceManager placeManager,  ApplicationRequestFactory requestFactory) {
        this.eventBus = eventBus;
        this.placeManager = placeManager;
        this.requestFactory = requestFactory;
    }

    /**
     * This is the entry point method.
     */
    @Override
    public void run() {
        this.requestFactory.initialize(this.eventBus);
        
        RootPanel.get().add(appWidget);
        this.placeManager.revealCurrentPlace();
    }
}
