package de.wsorg.feeder.client.request.reading.concrete.feed.load;

import com.google.inject.Inject;
import com.google.web.bindery.requestfactory.shared.Receiver;
import de.wsorg.feeder.shared.ApplicationRequestFactory;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedForUserRequestProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper;

/**
 *
 *
 *
 */
public class LoadAtomForUserRequestManager extends LoadAtomRequestManager<LoadFeedForUserRequestProxy> {

    @Inject
    public LoadAtomForUserRequestManager(final ApplicationRequestFactory factory,
                                         final AtomAutoBeanFactory atomAutoBeanFactory) {
        super(factory, atomAutoBeanFactory);
    }


    @Override
    protected void executeRequest(final LoadFeedForUserRequestProxy requestParams, final Receiver<AtomWrapper> receiverOfRelatedCall) {
        feedRequest.getFeedForUser(requestParams).fire(receiverOfRelatedCall);
        feedRequest = factory.feedRequest();
    }

    @Override
    protected String getUniqueIdOutOfRequestParam(final LoadFeedForUserRequestProxy requestParam) {
        return requestParam.getUserId() + "#@#" + requestParam.getFeedUrl();
    }
}
