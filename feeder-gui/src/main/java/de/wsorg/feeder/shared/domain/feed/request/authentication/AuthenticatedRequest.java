package de.wsorg.feeder.shared.domain.feed.request.authentication;

/**
 *
 *
 *
 */
public interface AuthenticatedRequest {
    String getUserId();
    void setUserId(final String userId);
    String getUserName();
    void setUserName(final String nickName);
    String getPassword();
    void setPassword(final String password);
}
