package de.wsorg.feeder.shared.domain.feed.response.atom.attributes;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.domain.standard.atom.attributes.link.LinkType", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface AtomLinkTypeProxy extends ValueProxy {
    String getLinkType();
    void setLinkType(String linkType);

    boolean isImage();
    boolean isHtml();
    boolean isAtom();
    boolean isApplication();
}
