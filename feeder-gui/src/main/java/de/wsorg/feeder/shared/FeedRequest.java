package de.wsorg.feeder.shared;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedForUserRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy;
import de.wsorg.feeder.shared.domain.feed.request.search.UserRelatedSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryForUserProxy;
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomProxy;
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlProxy;

@ServiceName(value="de.wsorg.feeder.server.domain.dao.feeder.FeedDAO", locator="de.wsorg.feeder.server.domain.locator.ApplicationServiceLocator")
public interface FeedRequest extends RequestContext {
	Request<AtomProxy> getFeed(final LoadFeedRequestProxy feedLoadData);
	Request<AtomProxy> getFeedForUser(final LoadFeedForUserRequestProxy feedLoadData);
    Request<OpmlProxy> findGlobalFeed(final GlobalSearchQueryProxy query);
    Request<OpmlProxy> findGlobalFeedWithUserData(final GlobalSearchQueryForUserProxy query);
    Request<OpmlProxy> findUserFeed(final UserRelatedSearchQueryProxy userRelatedSearchQueryRequest);
}
