package de.wsorg.feeder.shared.domain.user.login;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface UserLoginRequestProxy extends ValueProxy {
    String getNickName();
    void setNickName(final String userName);

    String getPassword();
    void setPassword(final String password);
}
