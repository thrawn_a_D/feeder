package de.wsorg.feeder.shared;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatusProxy;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkageProxy;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatusProxy;

/**
 *
 *
 *
 */
@ServiceName(value="de.wsorg.feeder.server.domain.dao.feeder.ReadStatusDAO", locator="de.wsorg.feeder.server.domain.locator.ApplicationServiceLocator")
public interface FeedReadStatusRequest extends RequestContext {
    Request<Void> setFeedReadStatus(final SingleFeedEntryReadStatusProxy feedEntryReadStatus);
    Request<Void> setMultiFeedReadStatus(final FeedReadMarkageProxy feedReadMarkage);
    Request<Void> setAllEntriesReadStatus(final AllEntriesReadStatusProxy readStatusForAllEntries);
}
