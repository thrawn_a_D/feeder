package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

import java.util.List;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkage", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface FeedReadMarkageProxy extends ValueProxy, AuthenticatedRequest {
    String getFeedUrl();
    void setFeedUrl(final String feedUrl);

    String getUserId();
    void setUserId(final String userId);

    List<FeedEntryReadStatusProxy> getFeedEntriesReadMarkage();
    void setFeedEntriesReadMarkage(final List<FeedEntryReadStatusProxy> feedEntriesReadMarkageProxy);
}
