package de.wsorg.feeder.shared.domain.feed.request.search.global;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface GlobalSearchQueryForUserProxy extends GlobalSearchQueryProxy, AuthenticatedRequest {
}
