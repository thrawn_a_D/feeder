package de.wsorg.feeder.shared;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;
import de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion.FeedSubscriptionRequestProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@ServiceName(value="de.wsorg.feeder.server.domain.dao.feeder.SubscriptionDAO", locator="de.wsorg.feeder.server.domain.locator.ApplicationServiceLocator")
public interface FeedSubscriptionHandlerRequest extends RequestContext {
    Request<Void> subscribeToFeed(final FeedSubscriptionRequestProxy subscriptionRequestProxy);
}
