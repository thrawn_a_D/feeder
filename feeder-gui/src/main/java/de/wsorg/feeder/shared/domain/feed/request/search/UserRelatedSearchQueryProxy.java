package de.wsorg.feeder.shared.domain.feed.request.search;

import com.google.web.bindery.requestfactory.shared.EntityProxy;
import com.google.web.bindery.requestfactory.shared.ProxyForName;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface UserRelatedSearchQueryProxy extends EntityProxy, AuthenticatedRequest {
	String getSearchTerm();
	void setSearchTerm(String searchQuery);
}