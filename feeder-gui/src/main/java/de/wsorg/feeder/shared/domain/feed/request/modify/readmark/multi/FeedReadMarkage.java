package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 *
 */
public class FeedReadMarkage {
    private String feedUrl;
    private List<FeedEntryReadStatus> feedEntriesReadMarkage = new ArrayList<FeedEntryReadStatus>();
    private String userId;
    private String userName;
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(final String feedUrl) {
        this.feedUrl = feedUrl;
    }

    public List<FeedEntryReadStatus> getFeedEntriesReadMarkage() {
        return feedEntriesReadMarkage;
    }

    public void setFeedEntriesReadMarkage(final List<FeedEntryReadStatus> feedEntriesReadMarkage) {
        this.feedEntriesReadMarkage = feedEntriesReadMarkage;
    }
}
