package de.wsorg.feeder.shared.domain.feed.response.opml;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

import java.util.List;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.response.feed.opml.Body", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface BodyProxy extends ValueProxy {
    List<OutlineProxy> getSearchResultList();
}
