package de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface FeedSubscriptionRequestProxy extends ValueProxy, AuthenticatedRequest {
    boolean isFavourFeed();
    void setFavourFeed(final boolean favourFeed);

    String getFeedUrl();
    void setFeedUrl(final String feedUrl);
}
