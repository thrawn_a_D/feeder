package de.wsorg.feeder.shared.domain.feed.response.atom;

import java.util.Date;
import java.util.List;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import de.wsorg.feeder.shared.domain.feed.response.atom.attributes.AtomEntryLinkProxy;

@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtomEntry", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface AtomEntryProxy extends ValueProxy {
	String getId();

    void setId(String id);

	String getDescription();

	void setDescription(String description);

	String getTitle();

	public void setTitle(String title);

    Date getUpdated();

    void setUpdated(Date updateDate);

    String getLogoUrl();

    void setLogoUrl(String imageUrl);

    void setEntryAlreadyRead(boolean isEntryAlreadyRead);

    boolean isEntryAlreadyRead();

    void setFeedUiHighlightingColor(String feedUiHighlightingColor);

    String getFeedUiHighlightingColor();

    void setFeedUrl(final String feedUrl);

    String getFeedUrl();

    List<AtomEntryLinkProxy> getLinks();

    void setLinks(List<AtomEntryLinkProxy> link);
}
