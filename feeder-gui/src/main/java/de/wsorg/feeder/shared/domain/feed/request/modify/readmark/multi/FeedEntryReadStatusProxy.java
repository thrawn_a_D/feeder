package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatus", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface FeedEntryReadStatusProxy extends ValueProxy, AuthenticatedRequest {
    String getFeedEntryId();
    void setFeedEntryId(final String feedEntryId);

    void setFeedEntryReadStatus(final Boolean feedEntryReadStatus);
    Boolean getFeedEntryReadStatus();
}
