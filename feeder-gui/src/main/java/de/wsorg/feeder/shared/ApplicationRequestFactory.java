package de.wsorg.feeder.shared;

import com.google.web.bindery.requestfactory.shared.RequestFactory;

public interface ApplicationRequestFactory extends RequestFactory {
    FeedRequest feedRequest();
    FeedSubscriptionHandlerRequest feedSubscriptionHandlerRequest();
    FeedReadStatusRequest feedReadStatusRequest();
    UserRequest userRequest();
}
