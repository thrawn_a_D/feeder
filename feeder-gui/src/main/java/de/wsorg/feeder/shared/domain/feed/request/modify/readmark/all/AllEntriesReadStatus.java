package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
public class AllEntriesReadStatus {
    private boolean readStatus;
    private String userId;
    private String userName;
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Boolean getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(final Boolean readStatus) {
        this.readStatus = readStatus;
    }
}
