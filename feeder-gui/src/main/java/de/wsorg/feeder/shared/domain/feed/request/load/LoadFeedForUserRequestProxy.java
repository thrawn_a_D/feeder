package de.wsorg.feeder.shared.domain.feed.request.load;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface LoadFeedForUserRequestProxy extends LoadFeedRequestProxy, AuthenticatedRequest {
    boolean isShowOnlyUnreadEntries();
    void setShowOnlyUnreadEntries(boolean showOnlyUnreadEntries);
}
