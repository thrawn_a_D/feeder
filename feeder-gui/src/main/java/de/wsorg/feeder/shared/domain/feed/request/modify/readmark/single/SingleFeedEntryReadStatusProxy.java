package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatusProxy;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatus", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface SingleFeedEntryReadStatusProxy extends FeedEntryReadStatusProxy {
    public String getFeedUrl();
    public void setFeedUrl(final String feedUrl);
}
