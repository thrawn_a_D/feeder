package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;
import de.wsorg.feeder.shared.domain.feed.request.authentication.AuthenticatedRequest;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatus", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface AllEntriesReadStatusProxy extends ValueProxy, AuthenticatedRequest {
    void setReadStatus(final Boolean allEntriesAreRead);
    Boolean getReadStatus();
}
