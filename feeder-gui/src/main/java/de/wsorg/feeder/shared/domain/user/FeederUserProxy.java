package de.wsorg.feeder.shared.domain.user;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 * <p/>
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface FeederUserProxy extends ValueProxy {
    String getUserId();
    String getUserName();
    String getFirstName();
    String getLastName();
    String getPassword();
    String getEMail();

    void setUserId(final String id);
    void setUserName(final String nickName);
    void setFirstName(final String firstName);
    void setLastName(final String lastName);
    void setPassword(final String password);
    void setEMail(final String eMail);
}
