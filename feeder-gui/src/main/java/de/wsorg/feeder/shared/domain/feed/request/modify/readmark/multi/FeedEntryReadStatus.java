package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi;

/**
 *
 *
 *
 */
public class FeedEntryReadStatus {

    private String feedEntryId;
    private Boolean feedEntryReadStatus;
    private String userId;
    private String userName;
    private String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(final String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getFeedEntryId() {
        return feedEntryId;
    }

    public void setFeedEntryId(final String feedEntryId) {
        this.feedEntryId = feedEntryId;
    }

    public Boolean getFeedEntryReadStatus() {
        return feedEntryReadStatus;
    }

    public void setFeedEntryReadStatus(final Boolean feedEntryReadStatus) {
        this.feedEntryReadStatus = feedEntryReadStatus;
    }
}
