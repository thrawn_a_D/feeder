package de.wsorg.feeder.shared;

import com.google.web.bindery.requestfactory.shared.Request;
import com.google.web.bindery.requestfactory.shared.RequestContext;
import com.google.web.bindery.requestfactory.shared.ServiceName;
import de.wsorg.feeder.shared.domain.user.FeederUserProxy;
import de.wsorg.feeder.shared.domain.user.login.UserLoginRequestProxy;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@ServiceName(value="de.wsorg.feeder.server.domain.dao.user.UserDAO", locator="de.wsorg.feeder.server.domain.locator.ApplicationServiceLocator")
public interface UserRequest extends RequestContext {
    Request<Void> create(final FeederUserProxy userProxy) throws RuntimeException;
    Request<Void> update(final FeederUserProxy userProxy) throws RuntimeException;
    Request<FeederUserProxy> loginUser(final UserLoginRequestProxy credentials) throws RuntimeException;
}
