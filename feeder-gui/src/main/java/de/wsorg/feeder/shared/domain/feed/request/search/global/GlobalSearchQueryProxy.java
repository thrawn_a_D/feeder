package de.wsorg.feeder.shared.domain.feed.request.search.global;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface GlobalSearchQueryProxy extends ValueProxy {
    String getSearchTerm();
    void setSearchTerm(String searchQuery);
}