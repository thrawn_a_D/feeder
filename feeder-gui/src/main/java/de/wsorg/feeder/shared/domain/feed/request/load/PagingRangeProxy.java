package de.wsorg.feeder.shared.domain.feed.request.load;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "de.wsorg.feeder.processor.production.domain.feeder.PagingRange", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface PagingRangeProxy extends ValueProxy {
    int getStartIndex();
    void setStartIndex(final int startIndex);
    int getEndIndex();
    void setEndIndex(final int endIndex);
}
