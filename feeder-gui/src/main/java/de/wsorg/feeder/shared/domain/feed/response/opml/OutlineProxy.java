package de.wsorg.feeder.shared.domain.feed.response.opml;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 *
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.response.feed.opml.Outline", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface OutlineProxy extends ValueProxy {
    String getTitle();
    String getDescription();
    String getXmlUrl();
    String getFeedUiHighlightingColor();
    boolean isFavoured();
}
