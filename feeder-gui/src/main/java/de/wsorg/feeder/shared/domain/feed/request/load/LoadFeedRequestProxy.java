package de.wsorg.feeder.shared.domain.feed.request.load;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;


@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface LoadFeedRequestProxy extends ValueProxy {
    String getFeedUrl();
    void setFeedUrl(final String feedUrl);
    PagingRangeProxy getFeedEntryPagingRange();
    void setFeedEntryPagingRange(final PagingRangeProxy pagingRange);
}
