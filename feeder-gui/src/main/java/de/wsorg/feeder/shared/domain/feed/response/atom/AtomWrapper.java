package de.wsorg.feeder.shared.domain.feed.response.atom;

import java.util.Date;
import java.util.List;

public interface AtomWrapper {
	String getId();

    void setId(String id);

	String getTitle();

	public void setTitle(String title);

    String getDescription();

    void setDescription(String description);

	Date getUpdated();

	void setUpdated(Date updateDate);

	List<AtomEntryProxy> getFeedEntries();

	String getLogoUrl();

	void setLogoUrl(String imageUrl);

    boolean isAdditionalPagingPossible();

    void setAdditionalPagingPossible(boolean additionalPagingPossible);

    String getFeedUiHighlightingColor();

    void setFeedUiHighlightingColor(String feedUiHighlightingColor);

    boolean isFavoured();
    void setFavoured(final boolean isFavoured);
}
