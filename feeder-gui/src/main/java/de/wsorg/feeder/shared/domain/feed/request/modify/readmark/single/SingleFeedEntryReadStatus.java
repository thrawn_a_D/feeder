package de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single;

import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatus;

/**
 *
 *
 *
 */
public class SingleFeedEntryReadStatus extends FeedEntryReadStatus {
    private String feedUrl;

    public String getFeedUrl() {
        return feedUrl;
    }

    public void setFeedUrl(final String feedUrl) {
        this.feedUrl = feedUrl;
    }
}
