package de.wsorg.feeder.shared.domain.feed.response.atom;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

@ProxyForName(value = "de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface AtomProxy extends ValueProxy, AtomWrapper {
    
}
