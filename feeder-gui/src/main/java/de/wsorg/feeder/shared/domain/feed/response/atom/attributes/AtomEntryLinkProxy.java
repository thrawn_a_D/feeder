package de.wsorg.feeder.shared.domain.feed.response.atom.attributes;

import com.google.web.bindery.requestfactory.shared.ProxyForName;
import com.google.web.bindery.requestfactory.shared.ValueProxy;

/**
 * .
 *
 *
 * @version $Revision$
 *
 *
 */
@ProxyForName(value = "de.wsorg.feeder.processor.domain.standard.atom.attributes.link.Link", locator="de.wsorg.feeder.server.domain.locator.EntityLocator")
public interface AtomEntryLinkProxy extends ValueProxy {
    String getHref();
    void setHref(final String href);

    AtomLinkTypeProxy getLinkType();
    void setLinkType(AtomLinkTypeProxy linkType);
}
