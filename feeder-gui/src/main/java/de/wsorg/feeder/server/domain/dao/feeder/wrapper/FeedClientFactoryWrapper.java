package de.wsorg.feeder.server.domain.dao.feeder.wrapper;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor;
import de.wsorg.feeder.processor.api.client.feed.FeederClientFactory;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;

import javax.inject.Named;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class FeedClientFactoryWrapper {
    static {
        FeederClientConfiguration feedConfiguration = new SimpleFeederClientConfiguration();
        feedConfiguration.setFeederRestServiceUrl("http://localhost:8080/feeder-handling/");
        FeederClientFactory.setClientConfig(feedConfiguration);
    }

    public FeederRequestProcessor<LoadFeedRequest, FeederAtom> getFeedLoader() {
        return FeederClientFactory.getFeedLoader();
    }

    public FeederRequestProcessor<UserRelatedLoadFeedRequest, FeederAtom> getFeedLoaderForUser() {
        return FeederClientFactory.getUserRelatedFeedLoader();
    }

    public FeederRequestProcessor<GlobalSearchQueryRequest, Opml> getGlobalFeedFinder(){
        return FeederClientFactory.getGlobalFeedFinder();
    }

    public FeederRequestProcessor<GlobalSearchQueryEnrichedWithUserDataRequest, Opml> getGlobalFeedFinderForUser(){
        return FeederClientFactory.getGlobalFeedFinderForUser();
    }

    public FeederRequestProcessor<FeedSubscriptionRequest, GenericResponseResult> getFeedSubscriptionHandler(){
        return FeederClientFactory.getFeedSubscriptionHandler();
    }

    public FeederRequestProcessor<UserRelatedSearchQueryRequest, Opml> getUserRelatedFeedFinder() {
        return FeederClientFactory.getUserRelatedFeedFinder();
    }

    public FeederRequestProcessor<MarkFeedEntryReadabilityRequest, GenericResponseResult> getFeedReadStatusHandler() {
        return FeederClientFactory.getFeedEntryReadMarker();
    }

    public FeederRequestProcessor<MarkAllFeedsReadabilityRequest, GenericResponseResult> getAllFeedsReadStatusHandler() {
        return FeederClientFactory.getAllFeedsReadMarker();
    }
}
