package de.wsorg.feeder.server.domain.dao.user.wrapper;

import javax.inject.Named;

import de.wsorg.feeder.processor.api.client.abstraction.configuration.FeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.configuration.SimpleFeederClientConfiguration;
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor;
import de.wsorg.feeder.processor.api.client.user.UserClientFactory;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Named
public class UserClientFactoryWrapper {
    static {
        FeederClientConfiguration userConfiguration = new SimpleFeederClientConfiguration();
        userConfiguration.setFeederRestServiceUrl("http://localhost:8080/user-handling/");
        UserClientFactory.setClientConfig(userConfiguration);
    }

    public FeederRequestProcessor<HandleUserRequest, GenericResponseResult> getUserHandler() {
        return UserClientFactory.getUserHandler();
    }

    public FeederRequestProcessor<CreateUserRequest, GenericResponseResult> getUserCreator() {
        return UserClientFactory.getUserCreator();
    }

    public FeederRequestProcessor<UserLoginRequest, FeederClientSideUser> getUserValidator() {
        return UserClientFactory.getUserValidator();
    }
}
