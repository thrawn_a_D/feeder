package de.wsorg.feeder.server.domain.locator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.web.bindery.requestfactory.shared.Locator;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.server.ioc.FeederServerGuiceModule;
import org.apache.commons.lang.StringUtils;

import java.nio.ByteBuffer;

public class EntityLocator<T> extends Locator<T, Long> {

    final static Injector injector = Guice.createInjector(new FeederServerGuiceModule());

    @Override
    public T create(Class<? extends T> clazz) {
        return injector.getInstance(clazz);
    }

    @Override
    public T find(Class<? extends T> arg0, Long arg1) {
		return null;
        
    }

    @Override
    public Class<T> getDomainType() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Long getId(T arg0) {
        return getObjectId(arg0);
    }

    @Override
    public Class<Long> getIdType() {
        return Long.class;
    }

    @Override
    public Object getVersion(T arg0) {
        return 0;
    }

    private Long getObjectId(T arg) {
        Long id=0L;

        if(arg instanceof FeederAtom) {
            FeederAtom feederAtomObject = (FeederAtom) arg;
            id = getLongFromString(feederAtomObject.getId());
        } else if(arg instanceof Opml) {
            Opml opmlObject = (Opml) arg;
            id = new Integer(opmlObject.hashCode()).longValue();
        }
        return id;
    }

    private Long getLongFromString(final String relatedString) {
        Long longValue = null;
        if(StringUtils.isNotBlank(relatedString)) {
            ByteBuffer idAsByteBuffer = ByteBuffer.wrap(relatedString.getBytes());
            longValue = idAsByteBuffer.getLong();
        }

        return longValue;
    }

}
