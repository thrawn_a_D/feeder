package de.wsorg.feeder.server.domain.dao.feeder;

import com.google.inject.Inject;
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest;
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom;
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml;
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper;

public class FeedDAO {

    @Inject
    private FeedClientFactoryWrapper feedClientFactoryWrapper;

	public FeederAtom getFeed(final LoadFeedRequest loadFeedRequest) throws Exception {
        return feedClientFactoryWrapper.getFeedLoader().executeRequest(loadFeedRequest);
    }

    public FeederAtom getFeedForUser(final UserRelatedLoadFeedRequest loadFeedRequest) throws Exception {
        return feedClientFactoryWrapper.getFeedLoaderForUser().executeRequest(loadFeedRequest);
    }

    public Opml findGlobalFeed(final GlobalSearchQueryRequest query){
        return feedClientFactoryWrapper.getGlobalFeedFinder().executeRequest(query);
	}

	public Opml findGlobalFeedWithUserData(final GlobalSearchQueryEnrichedWithUserDataRequest query){
        return feedClientFactoryWrapper.getGlobalFeedFinderForUser().executeRequest(query);
	}

    public Opml findUserFeed(final UserRelatedSearchQueryRequest userRelatedSearchQueryRequest) {
        return feedClientFactoryWrapper.getUserRelatedFeedFinder().executeRequest(userRelatedSearchQueryRequest);
    }
}
