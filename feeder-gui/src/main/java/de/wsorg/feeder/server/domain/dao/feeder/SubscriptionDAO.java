package de.wsorg.feeder.server.domain.dao.feeder;

import com.google.inject.Inject;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest;
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class SubscriptionDAO {

    @Inject
    private FeedClientFactoryWrapper feedClientFactoryWrapper;

    public void subscribeToFeed(final FeedSubscriptionRequest feedSubscriptionRequest){
        feedClientFactoryWrapper.getFeedSubscriptionHandler().executeRequest(feedSubscriptionRequest);
    }
}
