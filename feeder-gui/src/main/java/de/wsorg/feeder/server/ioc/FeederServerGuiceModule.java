package de.wsorg.feeder.server.ioc;

import com.google.inject.AbstractModule;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class FeederServerGuiceModule extends AbstractModule {
    @Override
    protected void configure() {

    }
}
