package de.wsorg.feeder.server.domain.dao.feeder;

import com.google.inject.Inject;
import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest;
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult;
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatus;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatus;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkage;
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatus;

/**
 *
 *
 *
 */
public class ReadStatusDAO {

    @Inject
    private FeedClientFactoryWrapper feedClientFactoryWrapper;

    public void setFeedReadStatus(final SingleFeedEntryReadStatus feedEntryReadStatus){
        FeederRequestProcessor<MarkFeedEntryReadabilityRequest, GenericResponseResult> readStatusHandler;
        readStatusHandler = feedClientFactoryWrapper.getFeedReadStatusHandler();

        final MarkFeedEntryReadabilityRequest markAsRead = getReadMarkageRequest(feedEntryReadStatus);
        readStatusHandler.executeRequest(markAsRead);
    }

    public void setMultiFeedReadStatus(final FeedReadMarkage feedReadMarkage) {
        FeederRequestProcessor<MarkFeedEntryReadabilityRequest, GenericResponseResult> readStatusHandler;
        readStatusHandler = feedClientFactoryWrapper.getFeedReadStatusHandler();

        final MarkFeedEntryReadabilityRequest markAsRead = mapGuiModelToFeederModel(feedReadMarkage);

        readStatusHandler.executeRequest(markAsRead);
    }

    public void setAllEntriesReadStatus(final AllEntriesReadStatus readStatusForAllEntries) {
        MarkAllFeedsReadabilityRequest markAllFeedsReadabilityRequest = new MarkAllFeedsReadabilityRequest();
        markAllFeedsReadabilityRequest.setFeedsReadStatus(readStatusForAllEntries.getReadStatus());
        markAllFeedsReadabilityRequest.setUserId(readStatusForAllEntries.getUserId());
        markAllFeedsReadabilityRequest.setUserName(readStatusForAllEntries.getUserName());
        markAllFeedsReadabilityRequest.setPassword(readStatusForAllEntries.getPassword());

        feedClientFactoryWrapper.getAllFeedsReadStatusHandler().executeRequest(markAllFeedsReadabilityRequest);
    }

    /**
     * As GWT does not support Map's to be used in RPC-Calls, we need to use a custom domain object
     * and map this to an appropriate feeder domain object
     */
    private MarkFeedEntryReadabilityRequest mapGuiModelToFeederModel(final FeedReadMarkage feedReadMarkage) {
        final MarkFeedEntryReadabilityRequest markAsRead = new MarkFeedEntryReadabilityRequest();
        markAsRead.setFeedUrl(feedReadMarkage.getFeedUrl());
        markAsRead.setUserId(feedReadMarkage.getUserId());
        markAsRead.setUserName(feedReadMarkage.getUserName());
        markAsRead.setPassword(feedReadMarkage.getPassword());

        for (FeedEntryReadStatus feedEntryReadStatus : feedReadMarkage.getFeedEntriesReadMarkage()) {
            final Boolean readStatus = feedEntryReadStatus.getFeedEntryReadStatus();
            final String feedEntryId = feedEntryReadStatus.getFeedEntryId();
            markAsRead.getFeedEntryUidWithReadabilityStatus().put(feedEntryId, readStatus);
        }
        return markAsRead;
    }

    private MarkFeedEntryReadabilityRequest getReadMarkageRequest(final SingleFeedEntryReadStatus feedEntryReadStatus) {
        final MarkFeedEntryReadabilityRequest markAsRead = new MarkFeedEntryReadabilityRequest();
        markAsRead.setFeedUrl(feedEntryReadStatus.getFeedUrl());
        markAsRead.setUserId(feedEntryReadStatus.getUserId());
        markAsRead.setUserName(feedEntryReadStatus.getUserName());
        markAsRead.setPassword(feedEntryReadStatus.getPassword());
        markAsRead.getFeedEntryUidWithReadabilityStatus().put(feedEntryReadStatus.getFeedEntryId(), feedEntryReadStatus.getFeedEntryReadStatus());
        return markAsRead;
    }
}
