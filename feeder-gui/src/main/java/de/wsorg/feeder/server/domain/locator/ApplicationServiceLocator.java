package de.wsorg.feeder.server.domain.locator;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.web.bindery.requestfactory.shared.ServiceLocator;
import de.wsorg.feeder.server.ioc.FeederServerGuiceModule;

public class ApplicationServiceLocator implements ServiceLocator {
    final static Injector injector = Guice.createInjector(new FeederServerGuiceModule());

    public Object getInstance(Class<?> clazz) {
        return injector.getInstance(clazz);
    }
  }