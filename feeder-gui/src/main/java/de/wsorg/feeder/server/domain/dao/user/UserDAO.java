package de.wsorg.feeder.server.domain.dao.user;

import com.google.inject.Inject;
import de.wsorg.feeder.processor.api.client.abstraction.exception.FeederProcessingException;
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser;
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest;
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType;
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest;
import de.wsorg.feeder.server.domain.dao.user.wrapper.UserClientFactoryWrapper;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UserDAO {
    @Inject
    private UserClientFactoryWrapper userClientFactoryWrapper;

    public void create(final FeederClientSideUser createUserRequest) throws FeederProcessingException {
        CreateUserRequest userHandlingRequest = new CreateUserRequest();
        userHandlingRequest.setUserToBeHandled(createUserRequest);
        userClientFactoryWrapper.getUserCreator().executeRequest(userHandlingRequest);
    }

    public void update(final FeederClientSideUser updateUserRequest) throws FeederProcessingException {
        handleUser(updateUserRequest, UserHandlingType.UPDATE);
    }

    private void handleUser(final FeederClientSideUser handleUserRequest, final UserHandlingType handlingType) {
        HandleUserRequest userHandlingRequest = new HandleUserRequest();
        userHandlingRequest.setUserToBeHandled(handleUserRequest);
        userHandlingRequest.setHandlingType(handlingType);
        userClientFactoryWrapper.getUserHandler().executeRequest(userHandlingRequest);
    }

    public FeederClientSideUser loginUser(final UserLoginRequest credentials) throws FeederProcessingException {
        UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setNickName(credentials.getNickName());
        userLoginRequest.setPassword(credentials.getPassword());
        return userClientFactoryWrapper.getUserValidator().executeRequest(userLoginRequest);
    }
}
