package de.wsorg.feeder.client.request.modifying

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.RequestContext
import de.wsorg.feeder.client.request.caching.ClientCachePool
import de.wsorg.feeder.client.request.reading.AbstractFeedRequestManagerTest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 29.11.12
 */
class AbstractFeedModifierTest extends Specification {

    TestAbstractFeedModifier testAbstractFeedModifier
    ClientCachePool clientCachePool
    boolean actualModificationMethodIsCalled = false;

    static RequestContext requestContext

    def setup(){
        requestContext = Mock(RequestContext)

        clientCachePool = Mock(ClientCachePool)
        testAbstractFeedModifier = new TestAbstractFeedModifier()
        testAbstractFeedModifier.setClientCachePool(clientCachePool)
        actualModificationMethodIsCalled = false;
    }


    def "Make sure actual modification is executed"() {
        given:
        def param = "234"

        Receiver<Void> receiver = Mock(Receiver)

        when:
        testAbstractFeedModifier.processRequest(param, receiver, Void.class);

        then:
        actualModificationMethodIsCalled
    }

    def "Clear cache after every modification is performed"() {
        given:
        def param = "asd"

        Receiver<Void> receiver = Mock(Receiver)

        when:
        testAbstractFeedModifier.processRequest(param, receiver, Void.class);

        then:
        1 * clientCachePool.clearWholeCache()
    }

    class TestAbstractFeedModifier extends AbstractFeedModifier<Void, String> {
        @Override
        protected RequestContext getRequestContext() {
            return AbstractFeedRequestManagerTest.requestContext;
        }

        @Override
        void processActualModification(final String params, final Receiver<Void> responseReceiver) {
            actualModificationMethodIsCalled = true
        }
    }
}
