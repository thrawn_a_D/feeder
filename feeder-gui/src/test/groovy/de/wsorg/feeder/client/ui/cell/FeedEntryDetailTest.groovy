package de.wsorg.feeder.client.ui.cell

import com.google.gwt.i18n.client.DateTimeFormat
import com.google.gwt.safehtml.shared.SafeHtmlBuilder
import com.gwtplatform.mvp.client.proxy.PlaceManager
import de.wsorg.feeder.client.event.readstatus.singleentry.SingleReadStatusHandler
import de.wsorg.feeder.client.security.session.user.UserSession
import de.wsorg.feeder.client.ui.cell.wrapper.GwtDateTimeFormatter
import de.wsorg.feeder.client.utils.StringUtils
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper
import de.wsorg.feeder.shared.domain.feed.response.atom.attributes.AtomEntryLinkProxy
import de.wsorg.feeder.shared.domain.feed.response.atom.attributes.AtomLinkTypeProxy
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 09.12.12
 */
class FeedEntryDetailTest extends Specification {
    FeedEntryDetail feedEntryDetail
    PlaceManager placeManager
    StringUtils stringUtils
    GwtDateTimeFormatter dateTimeFormatProvider
    DateTimeFormat actualDateTimeFormatter
    SingleReadStatusHandler readStatusHandler
    UserSession userSession

    def setup(){
        stringUtils = Mock(StringUtils)
        stringUtils.isBlank('') >> true
        placeManager = Mock(PlaceManager)
        dateTimeFormatProvider = Mock(GwtDateTimeFormatter)
        actualDateTimeFormatter = Mock(DateTimeFormat)
        userSession = Mock(UserSession)
        dateTimeFormatProvider.getFormat(_) >> actualDateTimeFormatter
        feedEntryDetail = new FeedEntryDetail(placeManager,
                                              stringUtils,
                                              dateTimeFormatProvider,
                                              readStatusHandler,
                                              userSession)
    }

    def "Make sure provided entry values are set"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.description >> 'desc'
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.updated >> new Date()

        and:
        def expectedDateFormattedString = '11.12.2012 12:23:12 PM'
        actualDateTimeFormatter.format(feedEntryProxy.updated) >> expectedDateFormattedString

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains(feedEntryProxy.title)
        buildHtmlResult.toSafeHtml().asString().contains(feedEntryProxy.description)
        buildHtmlResult.toSafeHtml().asString().contains(expectedDateFormattedString)
    }

    def "If color is set make sure a relevant th tag is in place"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.feedUiHighlightingColor >> '#h9hu2'
        feedEntryProxy.updated >> new Date()
        feedEntryProxy.description >> 'asd'

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("th rowspan='5' class='coloring'")
        buildHtmlResult.toSafeHtml().asString().contains(feedEntryProxy.feedUiHighlightingColor)
    }

    def "Make sure the 'go to parent feed' button is set"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("btnShowParentFeed")
    }

    def "Make sure no coloring is set if no color is provided"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.feedUiHighlightingColor >> ''

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        !buildHtmlResult.toSafeHtml().asString().contains("th rowspan='5' class='coloring'")
    }

    def "Mark table as read if feed is also marked as read"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> false


        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("class='unreadEntryShadow'")
    }

    def "Table has no shadows when feed is already read"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> true


        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        !buildHtmlResult.toSafeHtml().asString().contains("class='unreadEntryShadow'")
    }

    def "Make sure coloring th rowspan depends on actual set fields"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.feedUiHighlightingColor >> '#h9hu2'
        feedEntryProxy.getDescription() >> description
        feedEntryProxy.getUpdated() >> updateDate

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("th rowspan='${expectedRowspanCount}' class='coloring'")
        buildHtmlResult.toSafeHtml().asString().contains(feedEntryProxy.feedUiHighlightingColor)

        where:
        description     |    updateDate    |    expectedRowspanCount
        'terstdesc'     |    new Date()    |    5
        ''              |    new Date()    |    4
        'asdasd'        |    null          |    4
        ''              |    null          |    3
    }

    def "Make sure entry is marked as not read if the related property says so"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> false


        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("class='unreadEntryGradient'")
    }

    def "Make sure entry is marked as read if the related property set to true"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> true


        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        !buildHtmlResult.toSafeHtml().asString().contains("class='unreadEntryGradient'")
    }

    def "Add 'Mark entry as read' button when feed is not read yet and user is logged in"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> false

        and:
        def relatedAtom = Mock(AtomWrapper)
        relatedAtom.isFavoured() >> true
        feedEntryDetail.setRelatedAtom(relatedAtom)

        and:
        def user = Mock(FeederUserProxy)
        userSession.getCurrentUser() >> user

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("button white small")
        buildHtmlResult.toSafeHtml().asString().contains("btnMarkAsRead")
        buildHtmlResult.toSafeHtml().asString().contains("Mark as read")
    }

    def "Add 'Unmark entry as read' button when feed is already read and user is logged in"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> true

        and:
        def relatedAtom = Mock(AtomWrapper)
        relatedAtom.isFavoured() >> true
        feedEntryDetail.setRelatedAtom(relatedAtom)

        and:
        def user = Mock(FeederUserProxy)
        userSession.getCurrentUser() >> user

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("button white small white_keep_active")
        buildHtmlResult.toSafeHtml().asString().contains("btnMarkAsRead")
        buildHtmlResult.toSafeHtml().asString().contains("Mark as read/unread")
    }

    def "Do not add any read markage button when user is not logged in"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.entryAlreadyRead >> false

        and:
        userSession.getCurrentUser() >> null

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        !buildHtmlResult.toSafeHtml().asString().contains("<span class='button white small' id='btnMarkAsRead'>")
        !buildHtmlResult.toSafeHtml().asString().contains("Mark as read")
    }

    def "Entry image is set, show this"() {
        given:
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(AtomEntryProxy)
        feedEntryProxy.description >> 'desc'
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.updated >> new Date()

        and:
        def imageLink = Mock(AtomEntryLinkProxy)
        def linkType = Mock(AtomLinkTypeProxy)

        and:
        linkType.isImage() >> true
        imageLink.getLinkType() >> linkType
        imageLink.getHref() >> 'hreflink'
        feedEntryProxy.links >> [imageLink]

        when:
        feedEntryDetail.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("<img src=\"" + imageLink.getHref() + "\" >")
    }
}
