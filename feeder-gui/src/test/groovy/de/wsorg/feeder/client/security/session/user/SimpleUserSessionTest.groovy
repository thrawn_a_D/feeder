package de.wsorg.feeder.client.security.session.user

import com.google.web.bindery.autobean.shared.AutoBean
import de.wsorg.feeder.client.request.caching.util.GwtBeanUtil
import de.wsorg.feeder.client.security.session.ClientSessionStore
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 15.01.13
 * Time: 15:59
 */
class SimpleUserSessionTest extends Specification {
    SimpleUserSession simpleUserSession
    GwtBeanUtil gwtBeanUtil
    SimpleUserSession.UserBeanFactory userBeanFactory
    ClientSessionStore clientSessionStore

    def setup(){
        gwtBeanUtil = Mock(GwtBeanUtil)
        userBeanFactory = Mock(SimpleUserSession.UserBeanFactory)
        clientSessionStore = Mock(ClientSessionStore)
        simpleUserSession = new SimpleUserSession(gwtBeanUtil, userBeanFactory, clientSessionStore)
    }

    def "Register user in the session object"() {
        given:
        def userObject = Mock(FeederUserProxy)

        and:
        def usedAutobean = Mock(AutoBean)
        def encodedJson = "asdd"

        when:
        simpleUserSession.registerUserInSession(userObject)

        then:
        1 * gwtBeanUtil.getAutoBean(userObject) >> usedAutobean
        1 * gwtBeanUtil.encodeAutoBeanToJson(usedAutobean) >> encodedJson
        1 * clientSessionStore.putSessionValue('loggedInUser', encodedJson)
    }

    def "Do not start serialization if the provided object is null"() {
        when:
        simpleUserSession.registerUserInSession(null)

        then:
        0 * gwtBeanUtil.getAutoBean(_)
        0 * gwtBeanUtil.encodeAutoBeanToJson(_)
        0 * clientSessionStore.putSessionValue(_, _)
    }

    def "No object is in session do not start to deserialize it"() {
        when:
        def currentUser = simpleUserSession.getCurrentUser()

        then:
        currentUser == null
        1 * clientSessionStore.getValueFromSession('loggedInUser') >> null
        0 * gwtBeanUtil.decodeJsonToAutoBean(_,_,_)
    }

    def "Get stored user from storage"() {
        given:
        def userObject = Mock(FeederUserProxy)

        and:
        def usedAutobean = Mock(AutoBean)
        def encodedJson = "asdd"

        when:
        def result = simpleUserSession.getCurrentUser()

        then:
        result == userObject
        1 * clientSessionStore.getValueFromSession('loggedInUser') >> encodedJson
        1 * gwtBeanUtil.decodeJsonToAutoBean(userBeanFactory, encodedJson, FeederUserProxy) >> usedAutobean
        1 * usedAutobean.as() >> userObject
    }

    def "Cancel user session"() {
        when:
        simpleUserSession.cancelUserSession()

        then:
        1 * clientSessionStore.removeSessionValue('loggedInUser')
    }
}
