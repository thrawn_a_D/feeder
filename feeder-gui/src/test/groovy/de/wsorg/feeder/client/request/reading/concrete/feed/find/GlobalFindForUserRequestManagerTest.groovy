package de.wsorg.feeder.client.request.reading.concrete.feed.find

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedRequest
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryForUserProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.01.13
 */
class GlobalFindForUserRequestManagerTest extends Specification {
    GlobalFindForUserRequestManager globalFindForUserRequestManager
    ApplicationRequestFactory factory
    GlobalFindRequestManager.OpmlAutoBeanFactory opmlAutoBeanFactory;
    FeedRequest feedRequest

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        feedRequest = Mock(FeedRequest)
        opmlAutoBeanFactory = Mock(GlobalFindRequestManager.OpmlAutoBeanFactory)
        factory.feedRequest() >> feedRequest
        globalFindForUserRequestManager = new GlobalFindForUserRequestManager(factory, opmlAutoBeanFactory)
    }

    def "Execute find request"() {
        given:
        def globalSearchForUser = Mock(GlobalSearchQueryForUserProxy)

        and:
        def receiver = Mock(Receiver)

        and:
        def request = Mock(Request)

        and:
        def newFeedRequest = Mock(FeedRequest)

        when:
        globalFindForUserRequestManager.executeRequest(globalSearchForUser, receiver)

        then:
        1 * feedRequest.findGlobalFeedWithUserData(globalSearchForUser) >> request
        1 * request.fire(receiver)
        1 * factory.feedRequest() >> newFeedRequest
        globalFindForUserRequestManager.feedRequest == newFeedRequest
    }

    def "Identify request properly"() {
        given:
        def globalSearchForUser = Mock(GlobalSearchQueryForUserProxy)
        globalSearchForUser.getUserName() >> 'userName'
        globalSearchForUser.getSearchTerm() >> 'term'

        when:
        def result = globalFindForUserRequestManager.getUniqueIdOutOfRequestParam(globalSearchForUser)

        then:
        result == globalSearchForUser.getUserName() + "#@#" + globalSearchForUser.getSearchTerm()
    }
}
