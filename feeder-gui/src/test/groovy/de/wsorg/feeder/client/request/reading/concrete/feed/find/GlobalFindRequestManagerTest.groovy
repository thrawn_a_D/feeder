package de.wsorg.feeder.client.request.reading.concrete.feed.find

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCache
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedRequest
import de.wsorg.feeder.shared.domain.feed.request.search.global.GlobalSearchQueryProxy
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.11.12
 * Time: 15:33
 */
class GlobalFindRequestManagerTest extends Specification {

    GlobalFindRequestManager<GlobalSearchQueryProxy> opmlRequestManager
    ApplicationRequestFactory factory
    ClientCache clientCache
    GlobalFindRequestManager.OpmlAutoBeanFactory opmlAutoBeanFactory;
    FeedRequest feedRequest

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        clientCache = Mock(ClientCache)
        opmlAutoBeanFactory = Mock(GlobalFindRequestManager.OpmlAutoBeanFactory)
        feedRequest = Mock(FeedRequest)
        factory.feedRequest() >> feedRequest

        opmlRequestManager = new GlobalFindRequestManager(factory, opmlAutoBeanFactory)
        opmlRequestManager.clientCache = clientCache
    }

    def "Execute search request"() {
        given:
        def receiver = Mock(Receiver)

        and:
        def globalSearchQueryProxy = Mock(GlobalSearchQueryProxy)

        and:
        def newFeedRequest = Mock(FeedRequest)

        and:
        def request = Mock(Request)

        when:
        opmlRequestManager.processRequest(globalSearchQueryProxy, receiver, OpmlWrapper)

        then:
        1 * feedRequest.findGlobalFeed(globalSearchQueryProxy) >> request
        1 * request.fire(_)
        1 * factory.feedRequest() >> newFeedRequest
        opmlRequestManager.feedRequest == newFeedRequest
    }

    def "Check search term is returned as unique id"() {
        given:
        def userRelatedSearchQueryRequest = Mock(GlobalSearchQueryProxy)
        userRelatedSearchQueryRequest.getSearchTerm() >> "searcg"

        when:
        def result = opmlRequestManager.getUniqueIdOutOfRequestParam(userRelatedSearchQueryRequest)

        then:
        result == userRelatedSearchQueryRequest.getSearchTerm()
    }

    def "Provide a auto build factory"() {
        when:
        def result = opmlRequestManager.getDomainAutoBean()

        then:
        result == opmlAutoBeanFactory
    }

    def "Make sure caching is wanted"() {
        when:
        def result = opmlRequestManager.cacheResponse()

        then:
        result == true
    }

    def "Provide a related feedRequestContext"() {
        when:
        def result = opmlRequestManager.getRequestContext()

        then:
        result == feedRequest
    }
}
