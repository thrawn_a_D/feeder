package de.wsorg.feeder.client.request.modifying.concrete.feed.domain

import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData
import spock.lang.Specification
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserPassword
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.PasswordType

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.01.13
 */
class ClientUserDataRequestTest extends Specification {
    def "Make sure validation goes well when all fields are set"() {
        given:
        def userModel = getPreFilledUserModel()

        when:
        userModel.validateMandatoryFields()

        then:
        true
    }

    def "Make sure userName is mandatory"() {
        given:
        def userModel = getPreFilledUserModel()
        userModel.setUserName('')


        when:
        userModel.validateMandatoryFields()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'User name is a mandatory field and has to be set.'
    }

    def "Make sure password is mandatory"() {
        given:
        def userModel = getPreFilledUserModel()
        userModel.setPassword(null)


        when:
        userModel.validateMandatoryFields()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'Password is a mandatory field and has to be set.'
    }

    def "Make sure eMail is mandatory"() {
        given:
        def userModel = getPreFilledUserModel()
        userModel.seteMail('')


        when:
        userModel.validateMandatoryFields()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'E-Mail is a mandatory field and has to be set.'
    }

    def "Make sure email is validated properly"() {
        given:
        def userModel = getPreFilledUserModel()
        userModel.seteMail('asdasd')


        when:
        userModel.validateMandatoryFields()

        then:
        def ex = thrown(IllegalStateException)
        ex.message == 'The provided E-Mail address is invalid. Please provide a correct mail address.'
        true
    }

    def getPreFilledUserModel(){
        def userModel = new ClientUserData()
        userModel.userName = 'username'
        userModel.password = new ClientUserPassword('asdasd', PasswordType.PLAIN_TEXT)
        userModel.eMail = 'Mail@mail.com'
        userModel.firstName = 'firstName'
        userModel.lastName = 'lastName'
        return userModel;
    }
}
