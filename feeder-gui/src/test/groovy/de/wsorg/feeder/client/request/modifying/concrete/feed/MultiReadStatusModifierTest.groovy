package de.wsorg.feeder.client.request.modifying.concrete.feed

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedReadStatusRequest
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkageProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.12.12
 * Time: 14:51
 */
class MultiReadStatusModifierTest extends Specification {
    MultiReadStatusModifier multiReadStatusModifier
    ApplicationRequestFactory applicationRequestFactory
    FeedReadStatusRequest requestContext

    def setup(){
        applicationRequestFactory = Mock(ApplicationRequestFactory)
        requestContext = Mock(FeedReadStatusRequest)
        applicationRequestFactory.feedReadStatusRequest() >> requestContext
        multiReadStatusModifier = new MultiReadStatusModifier(applicationRequestFactory)
    }

    def "Execute feed markage request"() {
        given:
        def markageProxy = Mock(FeedReadMarkageProxy)

        and:
        def receiver = Mock(Receiver)

        and:
        def request = Mock(Request)

        when:
        multiReadStatusModifier.processActualModification(markageProxy, receiver)

        then:
        1 * requestContext.setMultiFeedReadStatus(markageProxy) >> request
        1 * request.fire(receiver)
    }


    def "Make sure context is changed after the actual call is done"() {
        given:
        def markageProxy = Mock(FeedReadMarkageProxy)

        and:
        def receiver = Mock(Receiver)

        and:
        def request = Mock(Request)

        and:
        def newRequestContext = Mock(FeedReadStatusRequest)

        when:
        multiReadStatusModifier.processActualModification(markageProxy, receiver)

        then:
        multiReadStatusModifier.requestContext == newRequestContext
        1 * applicationRequestFactory.feedReadStatusRequest() >> newRequestContext
        1 * requestContext.setMultiFeedReadStatus(markageProxy) >> request
        1 * request.fire(receiver)
    }
}
