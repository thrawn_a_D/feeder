package de.wsorg.feeder.client.security.session

import de.wsorg.feeder.client.security.session.wrapper.GwtCookiesWrapper
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 15.01.13
 * Time: 14:32
 */
class SimpleClientSessionStoreTest extends Specification {
    SimpleClientSessionStore simpleClientSession
    GwtCookiesWrapper cookieWrapper;

    def setup(){
        cookieWrapper = Mock(GwtCookiesWrapper)
        simpleClientSession = new SimpleClientSessionStore(cookieWrapper)
    }

    def "Make sure the provided object is stored in the session object"() {
        given:
        def given = 'some object'
        def objectId = 'id'

        when:
        simpleClientSession.putSessionValue(objectId, given)

        then:
        1 * cookieWrapper.addCookieValue(objectId, given)
    }

    def "Get an object out of the session object"() {
        given:
        def given = 'some object'
        def objectId = 'id'

        and:
        cookieWrapper.getCookieValue(objectId) >> given

        when:
        def result = simpleClientSession.getValueFromSession(objectId)

        then:
        result == given
    }

    def "Clear object from session"() {
        given:
        def objectId = 'id'

        when:
        simpleClientSession.removeSessionValue(objectId)

        then:
        1 * cookieWrapper.clearCookieValue(objectId)
    }
}
