package de.wsorg.feeder.client.request.reading.concrete.feed.load

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCache
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedRequest
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedForUserRequestProxy
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 18.01.13
 * Time: 12:40
 */
class LoadAtomForUserRequestManagerTest extends Specification {

    LoadAtomForUserRequestManager atomRequestManager
    ApplicationRequestFactory factory
    ClientCache clientCache
    LoadAtomRequestManager.AtomAutoBeanFactory atomAutoBeanFactory;
    FeedRequest feedRequest

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        feedRequest = Mock(FeedRequest)
        factory.feedRequest() >> feedRequest
        clientCache = Mock(ClientCache)
        atomRequestManager = new LoadAtomForUserRequestManager(factory, atomAutoBeanFactory)
        atomRequestManager.clientCache = clientCache
    }

    def "Execute search request"() {
        given:
        def receiver = Mock(Receiver)

        and:
        def feedUrl = "http:&/&kjbn"
        def loadRequest = Mock(LoadFeedForUserRequestProxy)
        loadRequest.getFeedUrl() >> feedUrl

        and:
        def request = Mock(Request)

        and:
        def newFeedRequest = Mock(FeedRequest)

        when:
        atomRequestManager.executeRequest(loadRequest, receiver)

        then:
        1 * feedRequest.getFeedForUser(loadRequest) >> request
        1 * request.fire(_)
        1 * factory.feedRequest() >> newFeedRequest
        atomRequestManager.feedRequest == newFeedRequest
    }
}
