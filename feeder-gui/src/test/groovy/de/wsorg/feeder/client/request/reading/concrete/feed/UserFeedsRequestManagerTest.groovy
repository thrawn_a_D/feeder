package de.wsorg.feeder.client.request.reading.concrete.feed

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCache
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedRequest
import de.wsorg.feeder.shared.domain.feed.request.search.UserRelatedSearchQueryProxy
import de.wsorg.feeder.shared.domain.feed.response.opml.OpmlWrapper
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class UserFeedsRequestManagerTest extends Specification {

    UserFeedsRequestManager userFeedsRequestManager
    ApplicationRequestFactory factory
    ClientCache clientCache
    UserFeedsRequestManager.UserOpmlAutoBeanFactory userOpmlAutoBeanFactory;
    FeedRequest feedRequest
    FeederUserProxy userData

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        clientCache = Mock(ClientCache)
        userOpmlAutoBeanFactory = Mock(UserFeedsRequestManager.UserOpmlAutoBeanFactory)
        feedRequest = Mock(FeedRequest)
        factory.feedRequest() >> feedRequest
        userFeedsRequestManager = new UserFeedsRequestManager(factory, userOpmlAutoBeanFactory)
        userFeedsRequestManager.clientCache = clientCache
    }

    def "Get user subscribed feeds"() {
        given:
        def receiver = Mock(Receiver)
        def param = Mock(UserRelatedSearchQueryProxy)

        and:
        def request = Mock(Request)

        when:
        userFeedsRequestManager.executeRequest(param, receiver)

        then:
        1 * feedRequest.findUserFeed(param) >> request
        1 * request.fire(_)
    }

    def "Check search term is returned as unique id"() {
        given:
        def param = Mock(UserRelatedSearchQueryProxy)
        param.getUserId() >> '234432'
        param.getSearchTerm() >> 'sdfdsf'

        when:
        def result = userFeedsRequestManager.getUniqueIdOutOfRequestParam(param)

        then:
        result == param.getUserId() + "#@#" + param.getSearchTerm()
    }

    def "Provide a auto build factory"() {
        when:
        def result = userFeedsRequestManager.getDomainAutoBean()

        then:
        result == userOpmlAutoBeanFactory
    }

    def "Make sure caching is NOT wanted"() {
        when:
        def result = userFeedsRequestManager.cacheResponse()

        then:
        result == false
    }

    def "Provide a related feedRequestContext"() {
        when:
        def result = userFeedsRequestManager.getRequestContext()

        then:
        result == feedRequest
    }
}
