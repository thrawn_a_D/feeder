package de.wsorg.feeder.client.ui.cell

import com.google.gwt.safehtml.shared.SafeHtmlBuilder
import com.gwtplatform.mvp.client.proxy.PlaceManager
import de.wsorg.feeder.client.event.subscribtion.SubscriptionHandler
import de.wsorg.feeder.client.security.session.user.UserSession
import de.wsorg.feeder.client.ui.cell.wrapper.GwtDomElement
import de.wsorg.feeder.client.utils.StringUtils
import de.wsorg.feeder.shared.domain.feed.response.opml.OutlineProxy
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 26.11.12
 * Time: 15:36
 */
class FeedListItemTest extends Specification {
    FeedListItem feedListItem
    PlaceManager placeManager
    StringUtils stringUtils
    GwtDomElement gwtDomElement
    SubscriptionHandler subscriptionHandler
    UserSession userSession

    def setup(){
        stringUtils = Mock(StringUtils)
        stringUtils.isBlank('') >> true
        placeManager = Mock(PlaceManager)
        gwtDomElement = Mock(GwtDomElement)
        subscriptionHandler = Mock(SubscriptionHandler)
        userSession = Mock(UserSession)
        feedListItem = new FeedListItem(placeManager,
                                        stringUtils,
                                        gwtDomElement,
                                        subscriptionHandler,
                                        userSession)
    }

    def "Deliver call events to parent class"() {
        given:
        String [] callEvents = ["click", "keydown"]

        when:
        def feederCell = new FeedListItem(placeManager,
                                          stringUtils,
                                          gwtDomElement,
                                          subscriptionHandler,
                                          userSession)

        then:
        feederCell.getConsumedEvents().toArray().every {callEvents.any {it}}
    }

    def "Set a table two column entry"() {
        given:
        def title = 'some title'
        def description = 'bla value'
        def outlineProxy = Mock(OutlineProxy)
        def htmlBuilder = new SafeHtmlBuilder()

        and:
        outlineProxy.getTitle() >> title
        outlineProxy.getDescription() >> description
        outlineProxy.isFavoured() >> false

        when:
        feedListItem.render(null, outlineProxy, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains(title)
        htmlBuilder.toSafeHtml().asString().contains(description)
        htmlBuilder.toSafeHtml().asString().contains('btnPreview')
    }

    def "Make subscription button active if feed is favoured"() {
        given:
        def title = 'some title'
        def description = 'bla value'
        def outlineProxy = Mock(OutlineProxy)
        def htmlBuilder = new SafeHtmlBuilder()

        and:
        outlineProxy.getTitle() >> title
        outlineProxy.getDescription() >> description
        outlineProxy.isFavoured() >> true

        and:
        def user = Mock(FeederUserProxy)
        userSession.getCurrentUser() >> user

        when:
        feedListItem.render(null, outlineProxy, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains('button white small white_keep_active')
        htmlBuilder.toSafeHtml().asString().contains('Unsubscribe')
    }

    def "If user is logged in do show the subscription button"() {
        given:
        def title = 'some title'
        def outlineProxy = Mock(OutlineProxy)
        def htmlBuilder = new SafeHtmlBuilder()

        and:
        outlineProxy.getTitle() >> title

        and:
        def user = Mock(FeederUserProxy)
        userSession.getCurrentUser() >> user

        when:
        feedListItem.render(null, outlineProxy, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains('btnSubscribe')
    }

    def "If no user is logged in show a loginbutton for subscription"() {
        given:
        def title = 'some title'
        def outlineProxy = Mock(OutlineProxy)
        def htmlBuilder = new SafeHtmlBuilder()

        and:
        outlineProxy.getTitle() >> title

        when:
        feedListItem.render(null, outlineProxy, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains('btnLoginToSubscribe')
    }

    def "Make sure coloring th rowspan depends on actual set fields"() {
        given:
        def color = '#h9hu2'
        def buildHtmlResult = new SafeHtmlBuilder()
        def feedEntryProxy = Mock(OutlineProxy)
        feedEntryProxy.title >> 'titleer'
        feedEntryProxy.feedUiHighlightingColor >> color
        feedEntryProxy.getDescription() >> description

        when:
        feedListItem.render(null, feedEntryProxy, buildHtmlResult)

        then:
        buildHtmlResult.toSafeHtml().asString().contains("th rowspan='${expectedRowspanCount}' class='coloring'")
        buildHtmlResult.toSafeHtml().asString().contains(color)

        where:
        description     |    expectedRowspanCount
        'terstdesc'     |    4
        ''              |    3
    }
}
