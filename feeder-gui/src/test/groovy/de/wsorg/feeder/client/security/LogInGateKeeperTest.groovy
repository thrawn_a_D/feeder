package de.wsorg.feeder.client.security

import de.wsorg.feeder.client.security.session.user.UserSession
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 15.01.13
 * Time: 16:10
 */
class LogInGateKeeperTest extends Specification {
    LogInGateKeeper logInGateKeeper
    UserSession userSession

    def setup(){
        userSession = Mock(UserSession)
        logInGateKeeper = new LogInGateKeeper(userSession)
    }



    def "Make sure user session is checked before providing access information"() {
        given:
        def feedUser = Mock(FeederUserProxy)

        and:
        userSession.getCurrentUser() >> feedUser

        when:
        def result=logInGateKeeper.canReveal()

        then:
        result == true
    }

    def "Reveal of a presenter is not allowed when no user is provided"() {
        given:
        userSession.getCurrentUser() >> null


        when:
        def result=logInGateKeeper.canReveal()

        then:
        result == false
    }
}
