package de.wsorg.feeder.client.event.readstatus.singleentry

import de.wsorg.feeder.client.request.modifying.concrete.feed.SingleReadStatusModifier
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatusProxy
import spock.lang.Specification
import de.wsorg.feeder.client.presenter.MainPagePresenter

/**
 * @author wschneider
 * Date: 18.12.12
 * Time: 16:51
 */
class SimpleSingleReadStatusHandlerTest extends Specification {
    SimpleSingleReadStatusHandler simpleReadStatusHandler
    SingleReadStatusModifier readStatusModifier
    MainPagePresenter mainPagePresenter

    def setup(){
        readStatusModifier = Mock(SingleReadStatusModifier)
        mainPagePresenter = Mock(MainPagePresenter)
        simpleReadStatusHandler = new SimpleSingleReadStatusHandler(readStatusModifier, mainPagePresenter)
    }

    def "Mark a feed as read"() {
        given:
        def feedUrl = 'http://sdfsfd'
        def feedEntryId = 'httüp://asdasd#asda'

        and:
        def requestModel = Mock(SingleFeedEntryReadStatusProxy)
        readStatusModifier.getNewDomainObjectForParam(SingleFeedEntryReadStatusProxy) >> requestModel

        when:
        simpleReadStatusHandler.markFeedEntryAsRead(feedUrl, feedEntryId)

        then:
        1 * readStatusModifier.processRequest(requestModel, _, _)
        1 * requestModel.setFeedUrl(feedUrl)
        1 * requestModel.setFeedEntryId(feedEntryId)
        1 * requestModel.setFeedEntryReadStatus(true)
    }

    def "Mark a feed as unread"() {
        given:
        def feedUrl = 'http://sdfsfd'
        def feedEntryId = 'httüp://asdasd#asda'

        and:
        def requestModel = Mock(SingleFeedEntryReadStatusProxy)
        readStatusModifier.getNewDomainObjectForParam(SingleFeedEntryReadStatusProxy) >> requestModel

        when:
        simpleReadStatusHandler.markFeedEntryAsUnread(feedUrl, feedEntryId)

        then:
        1 * readStatusModifier.processRequest(requestModel, _, _)
        1 * requestModel.setFeedUrl(feedUrl)
        1 * requestModel.setFeedEntryId(feedEntryId)
        1 * requestModel.setFeedEntryReadStatus(false)
    }
}
