package de.wsorg.feeder.client.utils

import spock.lang.Specification
import de.wsorg.feeder.client.utils.wrapper.GwtStaticClassWrapper

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class StringUtilsTest extends Specification {

    StringUtils stringUtils
    GwtStaticClassWrapper gwtStaticClassWrapper

    def setup(){
        stringUtils = new StringUtils()
        gwtStaticClassWrapper = Mock(GwtStaticClassWrapper)
        stringUtils.gwtStaticClassWrapper = gwtStaticClassWrapper
    }



    def "Check if a string is NOT equals to an other"() {
        given:
        def stringA="a"
        def stringB="b"

        and:
        gwtStaticClassWrapper.isScript() >> false

        when:
        def result = stringUtils.unsafeEquals(stringA, stringB)

        then:
        result == false
    }

    def "Check if a string IS equals to an other"() {
        given:
        def stringA="a"
        def stringB="a"

        and:
        gwtStaticClassWrapper.isScript() >> false

        when:
        def result = stringUtils.unsafeEquals(stringA, stringB)

        then:
        result == true
    }

    def "Check if a string is NOT equals to an other in JavaScript context"() {
        given:
        def stringA="a"
        def stringB="b"

        and:
        gwtStaticClassWrapper.isScript() >> true

        when:
        def result = stringUtils.unsafeEquals(stringA, stringB)

        then:
        result == false
    }

    def "Check if a string IS equals to an other in JavaScript context"() {
        given:
        def stringA="a"
        def stringB="a"

        and:
        gwtStaticClassWrapper.isScript() >> true

        when:
        def result = stringUtils.unsafeEquals(stringA, stringB)

        then:
        result == true
    }

    def "Check if a string IS blank"() {
        given:
        def blankString = ''

        and:
        gwtStaticClassWrapper.isScript() >> false

        when:
        def result = stringUtils.isBlank(blankString)

        then:
        result == true
    }

    def "Check if a string is NOT blank"() {
        given:
        def blankString = 'asd'

        and:
        gwtStaticClassWrapper.isScript() >> false

        when:
        def result = stringUtils.isBlank(blankString)

        then:
        result == false
    }
}
