package de.wsorg.feeder.client.request.caching

import spock.lang.Specification

/**
 * @author wschneider
 * Date: 30.11.12
 * Time: 16:07
 */
class ClientCachePoolTest extends Specification {

    ClientCachePool clientCachePool

    def setup(){
        clientCachePool = new ClientCachePool()
    }

    def "Register a caching object"() {
        given:
        def clientCache = Mock(ClientCache)

        when:
        clientCachePool.registerClientCache(clientCache)

        then:
        clientCachePool.clientCachePool.contains(clientCache)
    }

    def "Clear cache on all caching objects"() {
        given:
        def clientCache1 = Mock(ClientCache)
        def clientCache2 = Mock(ClientCache)

        and:
        clientCachePool.registerClientCache(clientCache1)
        clientCachePool.registerClientCache(clientCache2)

        when:
        clientCachePool.clearWholeCache()

        then:
        1 * clientCache1.clearCache()
        1 * clientCache2.clearCache()
    }
}
