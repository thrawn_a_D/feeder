package de.wsorg.feeder.client.request.reading.concrete.feed.load

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCache
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedRequest
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy
import de.wsorg.feeder.shared.domain.feed.request.load.PagingRangeProxy
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomWrapper
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.11.12
 * Time: 15:33
 */
class LoadAtomRequestManagerTest extends Specification {

    LoadAtomRequestManager atomRequestManager
    ApplicationRequestFactory factory
    ClientCache clientCache
    LoadAtomRequestManager.AtomAutoBeanFactory atomAutoBeanFactory;
    FeedRequest feedRequest

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        feedRequest = Mock(FeedRequest)
        factory.feedRequest() >> feedRequest
        clientCache = Mock(ClientCache)
        atomRequestManager = new LoadAtomRequestManager(factory, atomAutoBeanFactory)
        atomRequestManager.clientCache = clientCache
    }

    def "Execute search request"() {
        given:
        def receiver = Mock(Receiver)

        and:
        def feedUrl = "http:&/&kjbn"
        def loadRequest = Mock(LoadFeedRequestProxy)
        loadRequest.getFeedUrl() >> feedUrl

        and:
        def request = Mock(Request)

        and:
        def newFeedRequest = Mock(FeedRequest)

        when:
        atomRequestManager.processRequest(loadRequest, receiver, AtomWrapper)

        then:
        1 * feedRequest.getFeed(loadRequest) >> request
        1 * request.fire(_)
        1 * factory.feedRequest() >> newFeedRequest
        atomRequestManager.feedRequest == newFeedRequest
    }

    def "Check search term is returned as unique id"() {
        given:
        def feedUrl = "http:&/&kjbn"
        def loadRequest = Mock(LoadFeedRequestProxy)
        def pageRange = Mock(PagingRangeProxy)
        loadRequest.getFeedUrl() >> feedUrl
        loadRequest.getFeedEntryPagingRange() >> pageRange

        and:
        pageRange.getStartIndex() >> 1
        pageRange.getEndIndex() >> 11

        when:
        def result = atomRequestManager.getUniqueIdOutOfRequestParam(loadRequest)

        then:
        result == feedUrl + "_${pageRange.getStartIndex()}_${pageRange.getEndIndex()}"
    }

    def "Provide a auto build factory"() {
        when:
        def result = atomRequestManager.getDomainAutoBean()

        then:
        result == atomAutoBeanFactory
    }

    def "Make sure caching is wanted"() {
        when:
        def result = atomRequestManager.cacheResponse()

        then:
        result == true
    }

    def "Provide a related feedRequestContext"() {
        when:
        def result = atomRequestManager.getRequestContext()

        then:
        result == feedRequest
    }
}
