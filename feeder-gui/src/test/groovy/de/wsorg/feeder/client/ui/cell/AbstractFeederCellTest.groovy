package de.wsorg.feeder.client.ui.cell

import com.google.gwt.safehtml.shared.SafeHtmlBuilder
import de.wsorg.feeder.client.utils.StringUtils
import de.wsorg.feeder.shared.domain.feed.response.opml.OutlineProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 26.11.12
 * Time: 09:38
 */
class AbstractFeederCellTest extends Specification {
    AbstractFeederCell abstractFeederCell
    StringUtils stringUtils

    def setup(){
        stringUtils = Mock(StringUtils)
        abstractFeederCell = new TestFeederCell(null,stringUtils)
    }

    def "Deliver call events to parent class"() {
        given:
        String [] callEvents = ['onclick', 'onmouseover']

        when:
        def feederCell = new TestFeederCell(callEvents, stringUtils)

        then:
        feederCell.getConsumedEvents().toArray() == callEvents
    }

    def "Set title and generate the related html"() {
        given:
        def title='title text'
        def feedUrl='kjnklj'
        def readStatus = true
        def htmlBuilder = new SafeHtmlBuilder()

        when:
        abstractFeederCell.setTitle(title, feedUrl, readStatus, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains(title)
        htmlBuilder.toSafeHtml().asString().contains("href=\"${feedUrl}\"")
        htmlBuilder.toSafeHtml().asString().contains('unreadEntryGradient')
        htmlBuilder.toSafeHtml().asString().contains('unreadSpec')
    }

    def "Set title read status"() {
        given:
        def title='title text'
        def feedUrl='kjnklj'
        def readStatus = false
        def htmlBuilder = new SafeHtmlBuilder()

        when:
        abstractFeederCell.setTitle(title, feedUrl, readStatus, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains("href=\"${feedUrl}\"")
        htmlBuilder.toSafeHtml().asString().contains(title)
        htmlBuilder.toSafeHtml().asString().contains('nobg')
    }

    def "add link to title"() {
        given:
        def title='title text'
        def feedUrl='kjnklj'
        def readStatus = false
        def articleLink = 'kjhkjhkh'
        def htmlBuilder = new SafeHtmlBuilder()

        when:
        abstractFeederCell.setTitle(title, articleLink, feedUrl, readStatus, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().contains("href=\"${articleLink}\" target=\"_blank\"")
        htmlBuilder.toSafeHtml().asString().contains(title)
        htmlBuilder.toSafeHtml().asString().contains('nobg')
    }

    def "Make sure an empty title is not appended"() {
        given:
        def title=''
        def feedUrl='kjnklj'
        def readStatus = true
        def htmlBuilder = new SafeHtmlBuilder()

        and:
        stringUtils.isBlank(title) >> true

        when:
        abstractFeederCell.setTitle(title, feedUrl, readStatus, htmlBuilder)

        then:
        htmlBuilder.toSafeHtml().asString().isEmpty()
    }

    def "Set a table two column entry"() {
        given:
        def title = 'some title'
        def value = 'bla value'
        def rowStyle = 'kjasd'
        def htmlBuilder = new SafeHtmlBuilder()
        def escape = false

        when:
        abstractFeederCell.setTwoColumnEntryInTable(title, value, rowStyle, htmlBuilder, escape)

        then:
        htmlBuilder.toSafeHtml().asString().contains(title)
        htmlBuilder.toSafeHtml().asString().contains(value)
        htmlBuilder.toSafeHtml().asString().contains("class='${rowStyle}'")
    }

    def "Escape text while setting the column"() {
        given:
        def title = 'some title'
        def value = '<div>bla value</div>'
        def rowStyle = 'kjasd'
        def htmlBuilder = new SafeHtmlBuilder()
        def escape = true

        when:
        abstractFeederCell.setTwoColumnEntryInTable(title, value, rowStyle, htmlBuilder, escape)

        then:
        htmlBuilder.toSafeHtml().asString().contains("&lt;div&gt;bla value&lt;/div&gt;")
    }

    def "make sure no column is inserted if no value is provided"() {
        given:
        def title = 'some title'
        def value = ''
        def rowStyle = 'kjasd'
        def htmlBuilder = new SafeHtmlBuilder()
        def escape = true

        and:
        stringUtils.isBlank(value) >> true

        when:
        abstractFeederCell.setTwoColumnEntryInTable(title, value, rowStyle, htmlBuilder, escape)

        then:
        htmlBuilder.toSafeHtml().asString().isEmpty()
    }

    class TestFeederCell extends AbstractFeederCell<OutlineProxy> {
        TestFeederCell(final String[] acceptableEvents, final StringUtils stringUtils) {
            super(acceptableEvents, stringUtils)
        }
    }
}
