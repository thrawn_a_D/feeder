package de.wsorg.feeder.client.request.modifying.concrete.feed

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCachePool
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedSubscriptionHandlerRequest
import de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion.FeedSubscriptionRequestProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 29.11.12
 */
class SubscriptionModifierTest extends Specification {
    SubscriptionModifier subscriptionModifier
    ClientCachePool clientCache
    ApplicationRequestFactory factory;
    FeedSubscriptionHandlerRequest subscriptionHandler

    def setup(){
        clientCache = Mock(ClientCachePool)
        factory = Mock(ApplicationRequestFactory)
        subscriptionHandler = Mock(FeedSubscriptionHandlerRequest)
        factory.feedSubscriptionHandlerRequest() >> subscriptionHandler
        subscriptionModifier = new SubscriptionModifier(factory)
        subscriptionModifier.setClientCachePool(clientCache)
        subscriptionModifier.factory = factory
    }

    def "Subscribe to a feed"() {
        given:
        def subscriptionRequest = Mock(FeedSubscriptionRequestProxy)

        and:
        def fireEvent = Mock(Request)

        and:
        def receiver = Mock(Receiver)

        when:
        subscriptionModifier.processActualModification(subscriptionRequest, receiver)

        then:
        1 * subscriptionHandler.subscribeToFeed(subscriptionRequest) >> fireEvent
        1 * fireEvent.fire(receiver)
    }

    def "Check that request context is switched after request has been done"() {
        given:
        def subscriptionRequest = Mock(FeedSubscriptionRequestProxy)

        and:
        def fireEvent = Mock(Request)

        and:
        def receiver = Mock(Receiver)

        and:
        def newRequestContext = Mock(FeedSubscriptionHandlerRequest)

        when:
        subscriptionModifier.processActualModification(subscriptionRequest, receiver)

        then:
        subscriptionModifier.requestContext == newRequestContext
        1 * factory.feedSubscriptionHandlerRequest() >> newRequestContext
        1 * subscriptionHandler.subscribeToFeed(subscriptionRequest) >> fireEvent
        1 * fireEvent.fire(receiver)
    }
}
