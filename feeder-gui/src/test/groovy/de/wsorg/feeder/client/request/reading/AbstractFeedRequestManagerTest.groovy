package de.wsorg.feeder.client.request.reading

import com.google.web.bindery.autobean.shared.AutoBeanFactory
import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.RequestContext
import de.wsorg.feeder.client.request.caching.ClientCache
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.11.12
 * Time: 13:36
 */
class AbstractFeedRequestManagerTest extends Specification {

    TestFeedRequestManager testFeedRequestManager
    ClientCache clientCache
    AutoBeanFactory autoBeanFactory

    Receiver currentUsedReceiver;
    String uniqueIdFromRequest="bla"
    boolean cacheResponse = true

    static RequestContext requestContext

    def setup(){
        testFeedRequestManager = new TestFeedRequestManager()
        clientCache = Mock(ClientCache)
        requestContext = Mock(RequestContext)
        autoBeanFactory = Mock(AutoBeanFactory)
        testFeedRequestManager.clientCache = clientCache
    }

    def "Execute a request"() {
        given:
        Receiver receiver = Mock(Receiver)
        def requestParameter = "request"

        when:
        testFeedRequestManager.processRequest(requestParameter, receiver, String)

        then:
        currentUsedReceiver != null
    }

    def "Use result from cache if exists"() {
        given:
        def receiver = Mock(Receiver)
        def requestParameter = "request"

        and:
        def cachedObject = ['bla']

        when:
        testFeedRequestManager.processRequest(requestParameter, receiver, String)

        then:
        1 * clientCache.hasEntryInCache(uniqueIdFromRequest) >> true
        1 * clientCache.getObjectFromCache(uniqueIdFromRequest, String, autoBeanFactory) >> cachedObject
        1 * receiver.onSuccess(cachedObject)
    }

    def "Use result from cache if exists, but also perform update call"() {
        given:
        def receiver = Mock(Receiver)
        def requestParameter = "request"

        when:
        testFeedRequestManager.processRequest(requestParameter, receiver, String)

        then:
        1 * clientCache.hasEntryInCache(uniqueIdFromRequest) >> true
        currentUsedReceiver != null
    }

    def "Make sure cache is clean after related call"() {
        when:
        testFeedRequestManager.clearCache()

        then:
        1 * clientCache.clearCache()
    }

    class TestFeedRequestManager extends AbstractFeedRequestManager<String, String> {
        @Override
        protected void executeRequest(final String requestParams, final Receiver<String> receiverOfRelatedCall) {
            currentUsedReceiver = receiverOfRelatedCall
        }

        @Override
        protected String getUniqueIdOutOfRequestParam(final String requestParam) {
            return uniqueIdFromRequest;
        }

        @Override
        protected AutoBeanFactory getDomainAutoBean() {
            return autoBeanFactory;
        }

        @Override
        protected boolean cacheResponse() {
            return cacheResponse
        }

        @Override
        protected RequestContext getRequestContext() {
            return AbstractFeedRequestManagerTest.requestContext;
        }
    }
}
