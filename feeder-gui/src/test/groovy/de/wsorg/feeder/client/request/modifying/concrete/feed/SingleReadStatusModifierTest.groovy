package de.wsorg.feeder.client.request.modifying.concrete.feed

import com.google.web.bindery.requestfactory.shared.Receiver
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedReadStatusRequest
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatusProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 18.12.12
 * Time: 16:33
 */
class SingleReadStatusModifierTest extends Specification {
    SingleReadStatusModifier readStatusModifier
    ApplicationRequestFactory applicationRequestFactory
    FeedReadStatusRequest requestContext

    def setup(){
        applicationRequestFactory = Mock(ApplicationRequestFactory)
        requestContext = Mock(FeedReadStatusRequest)
        applicationRequestFactory.feedReadStatusRequest() >> requestContext
        readStatusModifier = new SingleReadStatusModifier(applicationRequestFactory)
    }

    def "Execute feed markage request"() {
        given:
        def readStatusRequest = Mock(SingleFeedEntryReadStatusProxy)

        and:
        def request = Mock(com.google.web.bindery.requestfactory.shared.Request)

        and:
        def receiver = Mock(Receiver)

        when:
        readStatusModifier.processActualModification(readStatusRequest, receiver)

        then:
        1 * requestContext.setFeedReadStatus(readStatusRequest) >> request
        1 * request.fire(receiver)
    }

    def "Make sure context is changed after the actual call is done"() {
        given:
        def readStatusRequest = Mock(SingleFeedEntryReadStatusProxy)

        and:
        def request = Mock(com.google.web.bindery.requestfactory.shared.Request)

        and:
        def receiver = Mock(Receiver)

        and:
        def newRequestContext = Mock(FeedReadStatusRequest)


        when:
        readStatusModifier.processActualModification(readStatusRequest, receiver)

        then:
        readStatusModifier.requestContext == newRequestContext
        1 * applicationRequestFactory.feedReadStatusRequest() >> newRequestContext
        1 * requestContext.setFeedReadStatus(readStatusRequest) >> request
        1 * request.fire(receiver)
    }
}
