package de.wsorg.feeder.client.request.modifying.concrete.user

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.UserRequest
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.06.13
 */
class UpdateUserModifierTest extends Specification {
    UpdateUserModifier updateUserModifier
    ApplicationRequestFactory applicationRequestFactory
    UserRequest userRequest

    def setup(){
        applicationRequestFactory = Mock(ApplicationRequestFactory)
        userRequest = Mock(UserRequest)
        updateUserModifier = new UpdateUserModifier(applicationRequestFactory)
        applicationRequestFactory.userRequest() >> userRequest
    }

    def "Call update execution on server side"() {
        given:
        def userData = Mock(FeederUserProxy)

        and:
        def request = Mock(Request)

        and:
        def receiver = Mock(Receiver)

        when:
        updateUserModifier.processActualCall(userData, userRequest, receiver)

        then:
        1 * userRequest.update(userData) >> request
        1 * request.fire(receiver)
    }
}
