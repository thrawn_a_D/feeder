package de.wsorg.feeder.client.request.caching

import com.google.web.bindery.autobean.shared.AutoBean
import com.google.web.bindery.autobean.shared.AutoBeanFactory
import de.wsorg.feeder.client.request.caching.util.GwtBeanUtil
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 20.11.12
 * Time: 11:42
 */
class ClientCacheTest extends Specification {
    ClientCache<List> clientJsonPersistence
    GwtBeanUtil gwtBeanUtil;
    ClientCachePool clientCachePool

    def setup(){
        gwtBeanUtil = Mock(GwtBeanUtil)
        clientCachePool = Mock(ClientCachePool)
        clientJsonPersistence = new ClientCache(gwtBeanUtil, clientCachePool)
        clientJsonPersistence.gwtBeanUtil = gwtBeanUtil
    }

    def "Store an object as json"() {
        given:
        def someObject=['some value', 'bla']
        def objectId='123'

        and:
        def relatedAutoBean = Mock(AutoBean)

        and:
        def expectedJsonResult='someJson:'

        when:
        clientJsonPersistence.storeObjectInCache(objectId, someObject)

        then:
        1 * gwtBeanUtil.getAutoBean(someObject) >> relatedAutoBean
        1 * gwtBeanUtil.encodeAutoBeanToJson(relatedAutoBean) >> expectedJsonResult
        clientJsonPersistence.cachingMap.get(objectId) == expectedJsonResult
    }


    def "Receive object from json"() {
        given:
        def jsonString = 'objectJsonString:'
        def objectId = '123'

        and:
        clientJsonPersistence.cachingMap.put(objectId, jsonString);

        and:
        def expectedAutoBean = Mock(AutoBean)
        def expectedObject = ['bla']

        and:
        def autoBean = Mock(AutoBeanFactory)

        when:
        clientJsonPersistence.getObjectFromCache(objectId, List, autoBean);

        then:
        1 * gwtBeanUtil.decodeJsonToAutoBean(autoBean, jsonString, List) >> expectedAutoBean
        1 * expectedAutoBean.as();
    }

    def "Has object in cache"() {
        given:
        def objectId = "123"
        def objectValue = "kjbasd"

        and:
        clientJsonPersistence.cachingMap.put(objectId, objectValue)

        when:
        def result = clientJsonPersistence.hasEntryInCache(objectId)

        then:
        result == true
    }

    def "Make sure caching object is registered in the caching pool"() {
        given:
        def cachePool = new ClientCachePool()

        when:
        def clientCache = new ClientCache<String>(gwtBeanUtil, cachePool)

        then:
        cachePool.clientCachePool.contains(clientCache)
    }

    def "Make sure cache is clean after related call"() {
        given:
        def objectId = "123"
        def objectValue = "kjbasd"

        and:
        clientJsonPersistence.cachingMap.put(objectId, objectValue)

        when:
        clientJsonPersistence.clearCache()

        then:
        clientJsonPersistence.cachingMap.size() == 0
    }
}
