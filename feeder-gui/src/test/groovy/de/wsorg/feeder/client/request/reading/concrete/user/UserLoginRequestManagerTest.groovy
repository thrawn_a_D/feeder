package de.wsorg.feeder.client.request.reading.concrete.user

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.client.request.caching.ClientCache
import de.wsorg.feeder.client.request.wrapper.MD5JavaScriptWrapper
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.UserRequest
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import de.wsorg.feeder.shared.domain.user.login.UserLoginRequestProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 15.01.13
 * Time: 09:20
 */
class UserLoginRequestManagerTest extends Specification {

    UserLoginRequestManager userLoginRequestManager
    ApplicationRequestFactory factory
    ClientCache clientCache
    UserLoginRequestManager.FeedUserAutoBeanFactory feedUserAutoBeanFactory;
    UserRequest userRequest
    MD5JavaScriptWrapper md5JavaScriptWrapper

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        userRequest = Mock(UserRequest)
        factory.userRequest() >> userRequest
        clientCache = Mock(ClientCache)
        md5JavaScriptWrapper = Mock(MD5JavaScriptWrapper)
        feedUserAutoBeanFactory = Mock(UserLoginRequestManager.FeedUserAutoBeanFactory)
        userLoginRequestManager = new UserLoginRequestManager(feedUserAutoBeanFactory, factory)
        userLoginRequestManager.clientCache = clientCache
        userLoginRequestManager.md5JavaScriptWrapper = md5JavaScriptWrapper
    }

    def "Execute login request"() {
        given:
        def receiver = Mock(Receiver)

        and:
        def userName = "asd"
        def password = "passwd"
        def loginRequestProxy = Mock(UserLoginRequestProxy)
        loginRequestProxy.getPassword() >> password
        loginRequestProxy.getNickName() >> userName

        and:
        def request = Mock(Request)

        and:
        def newUserRequest = Mock(UserRequest)

        and:
        def hashedPassword = 'asd782h3d87d'

        when:
        userLoginRequestManager.processRequest(loginRequestProxy, receiver, FeederUserProxy)

        then:
        1 * md5JavaScriptWrapper.hashPassword(password) >> hashedPassword
        1 * userRequest.loginUser(loginRequestProxy) >> request
        1 * request.fire(_)
        1 * factory.userRequest() >> newUserRequest
        userLoginRequestManager.userRequest == newUserRequest
    }

    def "Check user name is returned as unique id"() {
        given:
        def userName = "asd"
        def password = "passwd"
        def loginRequestProxy = Mock(UserLoginRequestProxy)
        loginRequestProxy.getPassword() >> password
        loginRequestProxy.getNickName() >> userName

        when:
        def result = userLoginRequestManager.getUniqueIdOutOfRequestParam(loginRequestProxy)

        then:
        result == userName
    }

    def "Provide a auto build factory"() {
        when:
        def result = userLoginRequestManager.getDomainAutoBean()

        then:
        result == feedUserAutoBeanFactory
    }

    def "Make sure caching is disabled"() {
        when:
        def result = userLoginRequestManager.cacheResponse()

        then:
        result == false
    }

    def "Provide a related feedRequestContext"() {
        when:
        def result = userLoginRequestManager.getRequestContext()

        then:
        result == userRequest
    }
}
