package de.wsorg.feeder.client.request.modifying.concrete.user

import com.google.web.bindery.requestfactory.shared.Receiver
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserData
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.ClientUserPassword
import de.wsorg.feeder.client.request.modifying.concrete.user.domain.PasswordType
import de.wsorg.feeder.client.request.wrapper.MD5JavaScriptWrapper
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.UserRequest
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 12.06.13
 */
class AbstractUserModifierTest extends Specification {
    static def providedUserProxyToExecute;
    static def userRequest
    static def responseReceiver

    AbstractUserModifierTestClass abstractUserModifierTestClass
    ApplicationRequestFactory factory
    UserRequest requestContext
    MD5JavaScriptWrapper md5JavaScriptWrapper
    FeederUserProxy userProxy

    def setup(){
        providedUserProxyToExecute = null
        userRequest = null
        responseReceiver = null

        factory = Mock(ApplicationRequestFactory)
        requestContext = Mock(UserRequest)
        md5JavaScriptWrapper = Mock(MD5JavaScriptWrapper)
        userProxy = Mock(FeederUserProxy)

        factory.userRequest() >> requestContext
        requestContext.create(FeederUserProxy) >> userProxy

        abstractUserModifierTestClass = new AbstractUserModifierTestClass(factory)
        abstractUserModifierTestClass.md5JavaScriptWrapper = md5JavaScriptWrapper
    }

    def "Map user to a proxy and provide for execution"() {
        given:
        ClientUserData params = new ClientUserData()
        params.eMail = 'lkjlkj'
        params.firstName = 'lkjölkn'
        params.lastName = 'sdwer32'
        params.password = new ClientUserPassword('asdf23r3effw3', PasswordType.PLAIN_TEXT)
        params.userName = 'aasdaasas'

        and:
        def expectedMd5Hash = 'lknlkn'

        and:
        def receiver = Mock(Receiver)

        when:
        abstractUserModifierTestClass.processActualModification(params, receiver)

        then:
        1 * md5JavaScriptWrapper.hashPassword(params.password.passwordText) >> expectedMd5Hash

        providedUserProxyToExecute
        userRequest
        responseReceiver
        1 * userProxy.setEMail(params.eMail)
        1 * userProxy.setFirstName(params.firstName)
        1 * userProxy.setLastName(params.lastName)
        1 * userProxy.setPassword(expectedMd5Hash)
        1 * userProxy.setUserName(params.userName)
    }

    def "Modify password only if this is in plain text"() {
        given:
        ClientUserData params = new ClientUserData()
        params.password = new ClientUserPassword('asdf23r3effw3', PasswordType.MD5)

        and:
        def receiver = Mock(Receiver)

        when:
        abstractUserModifierTestClass.processActualModification(params, receiver)

        then:
        0 * md5JavaScriptWrapper.hashPassword(params.password.passwordText)
        1 * userProxy.setPassword(params.password.passwordText)
    }

    private static class AbstractUserModifierTestClass extends AbstractUserModifier {

        AbstractUserModifierTestClass(final ApplicationRequestFactory factory) {
            super(factory)
        }

        @Override
        protected void processActualCall(FeederUserProxy userProxy,
                                         UserRequest userRequest,
                                         Receiver<Void> responseReceiver) {
            AbstractUserModifierTest.providedUserProxyToExecute = userProxy;
            AbstractUserModifierTest.userRequest = userRequest;
            AbstractUserModifierTest.responseReceiver = responseReceiver;
        }
    }
}
