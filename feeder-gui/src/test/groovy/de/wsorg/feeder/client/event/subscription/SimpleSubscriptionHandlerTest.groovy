package de.wsorg.feeder.client.event.subscription

import com.google.web.bindery.requestfactory.shared.Receiver
import de.wsorg.feeder.client.event.subscribtion.SimpleSubscriptionHandler
import de.wsorg.feeder.client.request.modifying.concrete.feed.SubscriptionModifier
import de.wsorg.feeder.shared.domain.feed.request.modify.subscribtion.FeedSubscriptionRequestProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 30.11.12
 * Time: 16:24
 */
class SimpleSubscriptionHandlerTest extends Specification {

    SimpleSubscriptionHandler simpleSubscriptionHandler
    SubscriptionModifier subscriptionModifier

    def setup(){
        subscriptionModifier = Mock(SubscriptionModifier)
        simpleSubscriptionHandler = new SimpleSubscriptionHandler(subscriptionModifier)
    }

    def "Call for action when subscribe event is called"() {
        given:
        def feedUrl = 'asd'

        and:
        def requestDomainProxy = Mock(FeedSubscriptionRequestProxy)

        when:
        simpleSubscriptionHandler.onSubscribeFeed(feedUrl)

        then:
        1 * subscriptionModifier.getNewDomainObjectForParam(FeedSubscriptionRequestProxy) >> requestDomainProxy
        1 * subscriptionModifier.processRequest(requestDomainProxy,
                                                {Receiver receiver -> receiver != null},
                                                {it == Void})
        1 * requestDomainProxy.setFeedUrl(feedUrl)
        1 * requestDomainProxy.setFavourFeed(true)
    }

    def "Call for action when cancel subscription event is called"() {
        given:
        def feedUrl = 'asd'

        and:
        def requestDomainProxy = Mock(FeedSubscriptionRequestProxy)

        when:
        simpleSubscriptionHandler.onCancelSubscription(feedUrl)

        then:
        1 * subscriptionModifier.getNewDomainObjectForParam(FeedSubscriptionRequestProxy) >> requestDomainProxy
        1 * subscriptionModifier.processRequest(requestDomainProxy,
                                                {Receiver receiver -> receiver != null},
                                                {it == Void})
        1 * requestDomainProxy.setFeedUrl(feedUrl)
        1 * requestDomainProxy.setFavourFeed(false)
    }

}
