package de.wsorg.feeder.client.request.reading.concrete.feed

import de.wsorg.feeder.client.request.reading.concrete.feed.load.LoadAtomRequestManager
import de.wsorg.feeder.shared.ApplicationRequestFactory
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 21.12.12
 * Time: 13:09
 */
class TimeLineRequestManagerTest extends Specification {
    TimeLineRequestManager timeLineRequestManager
    ApplicationRequestFactory factory
    LoadAtomRequestManager.AtomAutoBeanFactory atomAutoBeanFactory;

    def setup(){
        factory = Mock(ApplicationRequestFactory)
        atomAutoBeanFactory = Mock(LoadAtomRequestManager.AtomAutoBeanFactory)
        timeLineRequestManager = new TimeLineRequestManager(factory, atomAutoBeanFactory)
    }

    def "Make sure caching is not wanted"() {
        when:
        def result = timeLineRequestManager.cacheResponse()

        then:
        result == false
    }
}
