package de.wsorg.feeder.client.event.readstatus.multyentry

import com.google.gwt.event.dom.client.ClickHandler
import de.wsorg.feeder.client.request.modifying.concrete.feed.MultiReadStatusModifier
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatusProxy
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkageProxy
import de.wsorg.feeder.shared.domain.feed.response.atom.AtomEntryProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 21.12.12
 * Time: 11:56
 */
class SimpleMultiEntryReadStatusHandlerTest extends Specification {
    SimpleMultiEntryReadStatusHandler simpleMultiEntryReadStatusHandler
    MultiReadStatusModifier multiReadStatusModifier

    FeedReadMarkageProxy markageDomainProxy
    FeedEntryReadStatusProxy markageEntryDomainProxy

    def setup(){
        multiReadStatusModifier = Mock(MultiReadStatusModifier)
        simpleMultiEntryReadStatusHandler = new SimpleMultiEntryReadStatusHandler(multiReadStatusModifier)
        markageDomainProxy = Mock(FeedReadMarkageProxy)
        markageDomainProxy.feedEntriesReadMarkage >> Mock(List)
        markageEntryDomainProxy = Mock(FeedEntryReadStatusProxy)
        multiReadStatusModifier.getNewDomainObjectForParam(FeedReadMarkageProxy) >> markageDomainProxy
        multiReadStatusModifier.getNewDomainObjectForParam(FeedEntryReadStatusProxy) >> markageEntryDomainProxy
    }

    def "Mark provided feed entries as read"() {
        given:
        def entry1 = Mock(AtomEntryProxy)
        def entry2 = Mock(AtomEntryProxy)
        entry1.feedUrl >> 'f1'
        entry1.id >> 'f1e1'
        entry2.feedUrl >> 'f2'
        entry2.id >> 'f2e1'
        def atomEntries = [entry1, entry2]

        and:
        def expectedEntry1Key = entry1.feedUrl + '@' + entry1.id
        def expectedEntry2Key = entry2.feedUrl + '@' + entry2.id

        when:
        simpleMultiEntryReadStatusHandler.setFeedEntryList(atomEntries)
        simpleMultiEntryReadStatusHandler.onClick(null)

        then:
        1 * multiReadStatusModifier.processRequest(_,_,_)
        1 * markageDomainProxy.setFeedUrl('http://feeder.net/timeline')
        1 * markageEntryDomainProxy.setFeedEntryId(expectedEntry1Key)
        1 * markageEntryDomainProxy.setFeedEntryId(expectedEntry2Key)
        1 * markageDomainProxy.setFeedEntriesReadMarkage(_)
    }

    def "Skip feed entries which already been marked"() {
        given:
        def entry1 = Mock(AtomEntryProxy)
        def entry2 = Mock(AtomEntryProxy)
        entry1.feedUrl >> 'f1'
        entry1.id >> 'f1e1'
        entry2.feedUrl >> 'f2'
        entry2.id >> 'f2e1'
        entry2.entryAlreadyRead >> true
        def atomEntries = [entry1, entry2]

        and:
        def expectedEntry1Key = entry1.feedUrl + '@' + entry1.id

        when:
        simpleMultiEntryReadStatusHandler.setFeedEntryList(atomEntries)
        simpleMultiEntryReadStatusHandler.onClick(null)

        then:
        1 * multiReadStatusModifier.processRequest(_,_,_)
        1 * markageDomainProxy.setFeedUrl('http://feeder.net/timeline')
        1 * markageEntryDomainProxy.setFeedEntryId(expectedEntry1Key)
    }

    def "No execution if no list is provided"() {
        when:
        simpleMultiEntryReadStatusHandler.onClick(null)

        then:
        0 * multiReadStatusModifier.processRequest(_,_,_)
    }

    def "No execution if provided list is null"() {
        when:
        simpleMultiEntryReadStatusHandler.setFeedEntryList(null)
        simpleMultiEntryReadStatusHandler.onClick(null)

        then:
        0 * multiReadStatusModifier.processRequest(_,_,_)
    }

    def "Execute event hooks set in the list"() {
        given:
        def eventHandler1 = Mock(ClickHandler)
        def eventHandler2 = Mock(ClickHandler)

        and:
        def entry1 = Mock(AtomEntryProxy)
        def entry2 = Mock(AtomEntryProxy)
        def atomEntries = [entry1, entry2]
        simpleMultiEntryReadStatusHandler.setFeedEntryList(atomEntries)

        when:
        simpleMultiEntryReadStatusHandler.addHookEvent(eventHandler1)
        simpleMultiEntryReadStatusHandler.addHookEvent(eventHandler2)
        simpleMultiEntryReadStatusHandler.onClick(null)

        then:
        1 * eventHandler1.onClick(_)
        1 * eventHandler2.onClick(_)
    }
}
