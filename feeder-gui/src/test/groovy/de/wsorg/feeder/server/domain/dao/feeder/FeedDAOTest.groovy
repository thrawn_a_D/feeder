package de.wsorg.feeder.server.domain.dao.feeder

import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.domain.request.feed.load.LoadFeedRequest
import de.wsorg.feeder.processor.api.domain.request.feed.load.UserRelatedLoadFeedRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryEnrichedWithUserDataRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import de.wsorg.feeder.processor.api.domain.request.feed.search.user.UserRelatedSearchQueryRequest
import de.wsorg.feeder.processor.api.domain.response.feed.atom.FeederAtom
import de.wsorg.feeder.processor.api.domain.response.feed.opml.Opml
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class FeedDAOTest extends Specification {

    FeedDAO feedDAO
    FeedClientFactoryWrapper feedClientFactoryWrapper

    def setup(){
        feedDAO = new FeedDAO()
        feedClientFactoryWrapper = Mock(FeedClientFactoryWrapper)
        feedDAO.feedClientFactoryWrapper = feedClientFactoryWrapper
    }

    def "Get a feed using a specific url"() {
        given:
        def feedUrl = 'http://feedUrl'
        def feedLoad = new LoadFeedRequest(feedUrl: feedUrl)

        and:
        FeederRequestProcessor<LoadFeedRequest, FeederAtom> feedLoader = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getFeedLoader() >> feedLoader

        def providedResult = Mock(FeederAtom)

        when:
        def result = feedDAO.getFeed(feedLoad)

        then:
        result == providedResult
        1 * feedLoader.executeRequest(feedLoad) >> providedResult
    }

    def "Get a feed for a user using a specific url"() {
        given:
        def feedUrl = 'http://feedUrl'
        def feedLoad = new UserRelatedLoadFeedRequest(feedUrl: feedUrl)

        and:
        FeederRequestProcessor<UserRelatedLoadFeedRequest, FeederAtom> feedLoader = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getFeedLoaderForUser() >> feedLoader

        def providedResult = Mock(FeederAtom)

        when:
        def result = feedDAO.getFeedForUser(feedLoad)

        then:
        result == providedResult
        1 * feedLoader.executeRequest(feedLoad) >> providedResult
    }

    def "Find a feed using a query"() {
        given:
        def searchQuery = new GlobalSearchQueryRequest(searchTerm: 'some term')

        and:
        FeederRequestProcessor<GlobalSearchQueryRequest, Opml> feederRequestProcessor = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getGlobalFeedFinder() >> feederRequestProcessor

        and:
        def expectedResult = Mock(Opml)

        when:
        def result = feedDAO.findGlobalFeed(searchQuery)

        then:
        result == expectedResult
        1 * feederRequestProcessor.executeRequest({GlobalSearchQueryRequest request-> request.searchTerm == searchQuery.searchTerm}) >> expectedResult
    }

    def "Find a feed using a query and user data to enrich the result"() {
        given:
        def searchQuery = new GlobalSearchQueryEnrichedWithUserDataRequest(searchTerm: 'some term', userId: '123')

        and:
        FeederRequestProcessor<GlobalSearchQueryEnrichedWithUserDataRequest, Opml> feederRequestProcessor = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getGlobalFeedFinderForUser() >> feederRequestProcessor

        and:
        def expectedResult = Mock(Opml)

        when:
        def result = feedDAO.findGlobalFeedWithUserData(searchQuery)

        then:
        result == expectedResult
        1 * feederRequestProcessor.executeRequest({GlobalSearchQueryEnrichedWithUserDataRequest request->
                                                    request.searchTerm == searchQuery.searchTerm &&
                                                    request.userId == searchQuery.userId}) >> expectedResult
    }

    def "Use a user related search to enrich user data"() {
        given:
        def searchQuery = new UserRelatedSearchQueryRequest(searchTerm: 'bla', userId: '123')

        and:
        FeederRequestProcessor<UserRelatedSearchQueryRequest, Opml> feederRequestProcessor = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getUserRelatedFeedFinder() >> feederRequestProcessor

        and:
        def expectedResult = Mock(Opml)

        when:
        def result = feedDAO.findUserFeed(searchQuery)

        then:
        result == expectedResult
        1 * feederRequestProcessor.executeRequest({UserRelatedSearchQueryRequest request-> request.searchTerm == searchQuery.searchTerm}) >> expectedResult
    }
}
