package de.wsorg.feeder.server.domain.dao.feeder

import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.FeedSubscriptionRequest
import de.wsorg.feeder.processor.api.domain.response.result.GenericResponseResult
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class SubscriptionDAOTest extends Specification {

    SubscriptionDAO subscriptionDAO
    FeedClientFactoryWrapper feedClientFactoryWrapper

    def setup(){
        subscriptionDAO = new SubscriptionDAO()
        feedClientFactoryWrapper = Mock(FeedClientFactoryWrapper)
        subscriptionDAO.feedClientFactoryWrapper = feedClientFactoryWrapper
    }

    def "Subscribe to a feed"() {
        given:
        def request = Mock(FeedSubscriptionRequest)

        FeederRequestProcessor<FeedSubscriptionRequest, GenericResponseResult> requestProcessor = Mock(FeederRequestProcessor)
        feedClientFactoryWrapper.getFeedSubscriptionHandler() >> requestProcessor

        when:
        subscriptionDAO.subscribeToFeed(request)

        then:
        1 * requestProcessor.executeRequest(request)
    }
}
