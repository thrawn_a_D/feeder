package de.wsorg.feeder.server.domain.locator

import de.wsorg.feeder.processor.api.domain.request.feed.search.global.GlobalSearchQueryRequest
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class EntityLocatorTest extends Specification {
    EntityLocator entityLocator

    def setup(){
        entityLocator = new EntityLocator()
    }

    def "Create an instance of an entity"() {
        given:
        def entityType = GlobalSearchQueryRequest

        when:
        GlobalSearchQueryRequest result = entityLocator.create(entityType)

        then:
        result != null
    }
}
