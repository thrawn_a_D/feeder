package de.wsorg.feeder.server.domain.locator

import de.wsorg.feeder.server.domain.dao.feeder.FeedDAO
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 25.11.12
 */
class ApplicationServiceLocatorTest extends Specification {
    ApplicationServiceLocator applicationServiceLocator

    def setup(){
        applicationServiceLocator = new ApplicationServiceLocator()
    }

    def "Create an instance of an entity"() {
        given:
        def entityType = FeedDAO

        when:
        FeedDAO result = applicationServiceLocator.getInstance(entityType)

        then:
        result != null
        result.feedClientFactoryWrapper != null
    }
}
