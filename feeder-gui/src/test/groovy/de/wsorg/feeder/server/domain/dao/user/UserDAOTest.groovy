package de.wsorg.feeder.server.domain.dao.user

import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.domain.request.user.FeederClientSideUser
import de.wsorg.feeder.processor.api.domain.request.user.handling.HandleUserRequest
import de.wsorg.feeder.processor.api.domain.request.user.handling.UserHandlingType
import de.wsorg.feeder.processor.api.domain.request.user.validation.UserLoginRequest
import de.wsorg.feeder.server.domain.dao.user.wrapper.UserClientFactoryWrapper
import spock.lang.Specification
import de.wsorg.feeder.processor.api.domain.request.user.create.CreateUserRequest

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 13.01.13
 */
class UserDAOTest extends Specification {
    UserClientFactoryWrapper userClientFactoryWrapper
    FeederRequestProcessor feederUserHandlingProcessor
    FeederRequestProcessor feedUserVerificationProcessor
    FeederRequestProcessor userCreatorProcessor
    UserDAO userDAO

    def setup(){
        userDAO = new UserDAO()
        userClientFactoryWrapper = Mock(UserClientFactoryWrapper)
        feederUserHandlingProcessor = Mock(FeederRequestProcessor)
        feedUserVerificationProcessor = Mock(FeederRequestProcessor)
        userCreatorProcessor = Mock(FeederRequestProcessor)
        userDAO.userClientFactoryWrapper = userClientFactoryWrapper
        userClientFactoryWrapper.getUserHandler() >> feederUserHandlingProcessor
        userClientFactoryWrapper.getUserValidator() >> feedUserVerificationProcessor
        userClientFactoryWrapper.getUserCreator() >> userCreatorProcessor
    }

    def "Create a new user"() {
        given:
        FeederClientSideUser createUserRequest = new FeederClientSideUser()

        when:
        userDAO.create(createUserRequest);

        then:
        1 * userClientFactoryWrapper.getUserCreator() >> userCreatorProcessor
        1 * userCreatorProcessor.executeRequest({it instanceof CreateUserRequest &&
                                                 it.userToBeHandled == createUserRequest})
    }

    def "Update user"() {
        given:
        FeederClientSideUser createUserRequest = new FeederClientSideUser()
        createUserRequest.firstName = 'asdasd'

        when:
        userDAO.update(createUserRequest);

        then:
        1 * userClientFactoryWrapper.getUserHandler() >> feederUserHandlingProcessor
        1 * feederUserHandlingProcessor.executeRequest({it instanceof HandleUserRequest &&
                                                        it.firstName == createUserRequest.firstName &&
                                                        it.handlingType == UserHandlingType.UPDATE})
    }

    def "Login as user"() {
        given:
        def userName='user'
        def password='paswd'
        def credentials = new UserLoginRequest(nickName: userName, password: password)

        and:
        def expectedLoggedInUser = new FeederClientSideUser()

        when:
        def result = userDAO.loginUser(credentials)

        then:
        result != null
        1 * feedUserVerificationProcessor.executeRequest({it instanceof UserLoginRequest &&
                                                          it.nickName == userName &&
                                                          it.password == password}) >> expectedLoggedInUser
    }
}
