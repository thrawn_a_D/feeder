package de.wsorg.feeder.server.domain.dao.feeder

import de.wsorg.feeder.processor.api.client.abstraction.executor.FeederRequestProcessor
import de.wsorg.feeder.processor.api.domain.request.feed.handling.single.MarkFeedEntryReadabilityRequest
import de.wsorg.feeder.server.domain.dao.feeder.wrapper.FeedClientFactoryWrapper
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedEntryReadStatus
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.multi.FeedReadMarkage
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.single.SingleFeedEntryReadStatus
import spock.lang.Specification
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatus
import de.wsorg.feeder.processor.api.domain.request.feed.handling.all.MarkAllFeedsReadabilityRequest

/**
 * @author wschneider
 * Date: 18.12.12
 * Time: 14:55
 */
class ReadStatusDAOTest extends Specification {
    ReadStatusDAO readStatusDAO
    FeedClientFactoryWrapper feedClientFactoryWrapper

    def setup(){
        readStatusDAO = new ReadStatusDAO()
        feedClientFactoryWrapper = Mock(FeedClientFactoryWrapper)
        readStatusDAO.feedClientFactoryWrapper = feedClientFactoryWrapper
    }

    def "Mark feed as read"() {
        given:
        def feedEntryReadStatus = new SingleFeedEntryReadStatus()
        def feedUrl = 'http://asd'
        def userId = '234234'
        def userName = 'asd'
        def password = '32d23d'
        def feedEntryId = 'http://asd#asd'
        def readStatus  = true
        feedEntryReadStatus.feedUrl = feedUrl
        feedEntryReadStatus.userId = userId
        feedEntryReadStatus.userName = userName
        feedEntryReadStatus.password = password
        feedEntryReadStatus.feedEntryId = feedEntryId
        feedEntryReadStatus.feedEntryReadStatus = readStatus

        and:
        def processor = Mock(FeederRequestProcessor)

        when:
        readStatusDAO.setFeedReadStatus(feedEntryReadStatus)

        then:
        1 * feedClientFactoryWrapper.getFeedReadStatusHandler() >> processor
        1 * processor.executeRequest({MarkFeedEntryReadabilityRequest it -> it.feedUrl == feedUrl &&
                                                                      it.userId == userId &&
                                                                      it.userName == userName &&
                                                                      it.password == password &&
                                                                      it.feedEntryUidWithReadabilityStatus[feedEntryId] == readStatus})
    }

    def "Mark several entries as read"() {
        given:
        def feedUrl = 'http://asd'
        def userId = '234234'

        and:
        def feedReadMarkage = new FeedReadMarkage()
        feedReadMarkage.setFeedUrl(feedUrl)
        feedReadMarkage.setUserId(userId)
        def readEntry1 = new FeedEntryReadStatus(feedEntryId: 'http://sdf#asd',
                                                 feedEntryReadStatus: true)
        def readEntry2 = new FeedEntryReadStatus(feedEntryId: 'http://sdf#asd2',
                                                 feedEntryReadStatus: true)
        feedReadMarkage.feedEntriesReadMarkage.add(readEntry1)
        feedReadMarkage.feedEntriesReadMarkage.add(readEntry2)

        and:
        def processor = Mock(FeederRequestProcessor)

        when:
        readStatusDAO.setMultiFeedReadStatus(feedReadMarkage)

        then:
        1 * feedClientFactoryWrapper.getFeedReadStatusHandler() >> processor
        1 * processor.executeRequest({MarkFeedEntryReadabilityRequest it -> it.feedUrl == feedUrl &&
                                                                      it.userId == userId &&
                                                                      it.feedEntryUidWithReadabilityStatus.get('http://sdf#asd') == true &&
                                                                      it.feedEntryUidWithReadabilityStatus.get('http://sdf#asd2') == true})
    }

    def "Mark all entries as read"() {
        given:
        def readStatusForAllEntries = new AllEntriesReadStatus()
        readStatusForAllEntries.userId = 'asdlij8hj'
        readStatusForAllEntries.userName = 'lojoih98ziuh'
        readStatusForAllEntries.password = 'asdasdasd'
        readStatusForAllEntries.readStatus = true

        and:
        def processor = Mock(FeederRequestProcessor)

        when:
        readStatusDAO.setAllEntriesReadStatus(readStatusForAllEntries)

        then:
        1 * feedClientFactoryWrapper.getAllFeedsReadStatusHandler() >> processor
        1 * processor.executeRequest({MarkAllFeedsReadabilityRequest it -> it.feedsReadStatus == readStatusForAllEntries.readStatus &&
                                                                           it.userId == readStatusForAllEntries.userId &&
                                                                           it.userName == readStatusForAllEntries.userName &&
                                                                           it.password == readStatusForAllEntries.password})
    }
}
