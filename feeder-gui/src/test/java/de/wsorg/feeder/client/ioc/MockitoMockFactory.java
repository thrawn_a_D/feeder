package de.wsorg.feeder.client.ioc;

import org.mockito.Mockito;

import com.gwtplatform.tester.MockFactory;

public class MockitoMockFactory implements MockFactory {
    public <T> T mock(Class<T> classToMock) {
      return Mockito.mock(classToMock);
    }
  }