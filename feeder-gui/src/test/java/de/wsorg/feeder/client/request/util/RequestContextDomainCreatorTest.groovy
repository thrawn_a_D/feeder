package de.wsorg.feeder.client.request.util

import com.google.web.bindery.requestfactory.shared.RequestContext
import de.wsorg.feeder.client.security.session.user.UserSession
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy
import de.wsorg.feeder.shared.domain.feed.request.search.UserRelatedSearchQueryProxy
import de.wsorg.feeder.shared.domain.user.FeederUserProxy
import spock.lang.Specification

/**
 * @author wschneider
 * Date: 17.01.13
 * Time: 09:08
 */
class RequestContextDomainCreatorTest extends Specification {
    RequestContextDomainCreator requestContextDomainCreator
    UserSession userSession
    RequestContext requestContext

    def setup(){
        userSession = Mock(UserSession)
        requestContext = Mock(RequestContext)
        requestContextDomainCreator = new RequestContextDomainCreator()
        requestContextDomainCreator.userSession = userSession
        requestContextDomainCreator.requestContext = requestContext
    }

    def "Create a model for a request"() {
        given:
        def createdBean = Mock(LoadFeedRequestProxy)

        and:
        requestContext.create(LoadFeedRequestProxy) >> createdBean

        when:
        def result = requestContextDomainCreator.getNewDomainObjectForParam(LoadFeedRequestProxy)

        then:
        result == createdBean
    }

    def "If model to create for a request is derived from AuthenticatedRequest then prefill the credentials"() {
        given:
        def createdBean = Mock(UserRelatedSearchQueryProxy)

        and:
        requestContext.create(UserRelatedSearchQueryProxy) >> createdBean

        and:
        def userData = Mock(FeederUserProxy)
        userData.getUserId() >> '23123'
        userData.userName >> 'nick'
        userData.getPassword() >> 'passwd'
        userSession.getCurrentUser() >> userData

        when:
        def result = requestContextDomainCreator.getNewDomainObjectForParam(UserRelatedSearchQueryProxy)

        then:
        result == createdBean
        1 * createdBean.setUserId(userData.getUserId())
        1 * createdBean.setUserName(userData.getUserName())
        1 * createdBean.setPassword(userData.getPassword())
    }

    def "If user is not logged in provide a simple model"() {
        given:
        def createdBean = Mock(UserRelatedSearchQueryProxy)

        and:
        requestContext.create(UserRelatedSearchQueryProxy) >> createdBean

        and:
        userSession.getCurrentUser() >> null

        when:
        def result = requestContextDomainCreator.getNewDomainObjectForParam(UserRelatedSearchQueryProxy)

        then:
        result == createdBean
        0 * createdBean.setUserId(_)
        0 * createdBean.setUserName(_)
        0 * createdBean.setPassword(_)
    }
}
