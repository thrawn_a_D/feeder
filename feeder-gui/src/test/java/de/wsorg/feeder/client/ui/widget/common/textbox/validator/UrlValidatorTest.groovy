package de.wsorg.feeder.client.ui.widget.common.textbox.validator

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.02.13
 */
class UrlValidatorTest extends Specification {
    UrlValidator urlValidator

    def setup(){
        urlValidator = new UrlValidator()
    }

    def "Validate a valid url"() {
        given:
        def url = 'http://www.google.de/feed'

        when:
        def result = urlValidator.validate(url)

        then:
        result == true
    }

    def "Validate an invalid url"() {
        given:
        def url = 'htttp://www.go/feed'


        when:
        def result = urlValidator.validate(url)

        then:
        result == false
    }
}
