package de.wsorg.feeder.client.request.modifying.concrete.feed

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.Request
import de.wsorg.feeder.shared.ApplicationRequestFactory
import de.wsorg.feeder.shared.FeedReadStatusRequest
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatusProxy
import spock.lang.Specification

/**
 *  .
 * @author wschneider
 * @version $Revision$
 * Date: 31.05.13
 * Time: 09:33
 */
class AllEntriesReadStatusModifierTest extends Specification {
    AllEntriesReadStatusModifier allEntriesReadStatusModifier
    ApplicationRequestFactory applicationRequestFactory
    FeedReadStatusRequest requestContext

    def setup(){
        applicationRequestFactory = Mock(ApplicationRequestFactory)
        requestContext = Mock(FeedReadStatusRequest)
        applicationRequestFactory.feedReadStatusRequest() >> requestContext
        allEntriesReadStatusModifier = new AllEntriesReadStatusModifier(applicationRequestFactory)
    }

    def "Execute feed markage request"() {
        given:
        def markageProxy = Mock(AllEntriesReadStatusProxy)

        and:
        def receiver = Mock(Receiver)

        and:
        def request = Mock(Request)

        when:
        allEntriesReadStatusModifier.processActualModification(markageProxy, receiver)

        then:
        1 * requestContext.setAllEntriesReadStatus(markageProxy) >> request
        1 * request.fire(receiver)
    }


    def "Make sure context is changed after the actual call is done"() {
        given:
        def markageProxy = Mock(AllEntriesReadStatusProxy)

        and:
        def receiver = Mock(Receiver)

        and:
        def request = Mock(Request)

        and:
        def newRequestContext = Mock(FeedReadStatusRequest)

        when:
        allEntriesReadStatusModifier.processActualModification(markageProxy, receiver)

        then:
        allEntriesReadStatusModifier.requestContext == newRequestContext
        1 * applicationRequestFactory.feedReadStatusRequest() >> newRequestContext
        1 * requestContext.setAllEntriesReadStatus(markageProxy) >> request
        1 * request.fire(receiver)
    }
}
