package de.wsorg.feeder.client.ui.widget.common.textbox.validator

import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 26.02.13
 */
class EmailValidatorTest extends Specification {

    EmailValidator emailValidator

    def setup(){
        emailValidator = new EmailValidator()
    }

    def "Validate a valid email"() {
        given:
        def validMail = 'kjhb@kjb.de'

        when:
        def result = emailValidator.validate(validMail)

        then:
        result == true
    }

    def "Validate an invalid email"() {
        given:
        def validMail = 'kjhbkjb.de'

        when:
        def result = emailValidator.validate(validMail)

        then:
        result == false
    }
}
