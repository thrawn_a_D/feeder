package de.wsorg.feeder.client.request

import com.google.web.bindery.requestfactory.shared.Receiver
import com.google.web.bindery.requestfactory.shared.RequestContext
import de.wsorg.feeder.client.request.util.RequestContextDomainCreator
import de.wsorg.feeder.shared.domain.feed.request.load.LoadFeedRequestProxy
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 19.01.13
 */
class CommonFeedRequestManagerTest extends Specification {
    static RequestContext requestContext

    TestCommonFeedRequestManager testCommonFeedRequestManager
    RequestContextDomainCreator domainProxyCreator

    def setup(){
        testCommonFeedRequestManager = new TestCommonFeedRequestManager()
        domainProxyCreator = Mock(RequestContextDomainCreator)
        testCommonFeedRequestManager.domainProxyCreator = domainProxyCreator
    }

    def "Delegate domain creation but before that provide a current request context"() {
        when:
        testCommonFeedRequestManager.getNewDomainObjectForParam(LoadFeedRequestProxy)

        then:
        1 * domainProxyCreator.setRequestContext(CommonFeedRequestManagerTest.requestContext)
        1 * domainProxyCreator.getNewDomainObjectForParam(LoadFeedRequestProxy)
    }

    private static class TestCommonFeedRequestManager extends CommonFeedRequestManager {

        Object requestParams

        @Override
        protected RequestContext getRequestContext() {
            return CommonFeedRequestManagerTest.requestContext;
        }

        @Override
        void processRequest(final Object requestParams, final Receiver responseReceiver, final Class responseType) {
        }
    }

}
