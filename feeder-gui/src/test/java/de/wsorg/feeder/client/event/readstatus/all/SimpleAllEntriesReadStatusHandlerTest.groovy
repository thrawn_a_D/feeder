package de.wsorg.feeder.client.event.readstatus.all

import com.google.web.bindery.requestfactory.shared.Receiver
import de.wsorg.feeder.client.request.modifying.concrete.feed.AllEntriesReadStatusModifier
import de.wsorg.feeder.shared.domain.feed.request.modify.readmark.all.AllEntriesReadStatusProxy
import spock.lang.Specification

/**
 *  .
 * @author wschneider
 * @version $Revision$
 * Date: 31.05.13
 * Time: 09:39
 */
class SimpleAllEntriesReadStatusHandlerTest extends Specification {

    SimpleAllEntriesReadStatusHandler simpleAllEntriesReadStatusHandler
    AllEntriesReadStatusModifier allEntriesReadStatusModifier

    def setup(){
        allEntriesReadStatusModifier = Mock(AllEntriesReadStatusModifier)
        simpleAllEntriesReadStatusHandler = new SimpleAllEntriesReadStatusHandler(allEntriesReadStatusModifier)
    }



    def "Mark all entries as read"() {
        given:
        def readStatusDomain = Mock(AllEntriesReadStatusProxy)
        allEntriesReadStatusModifier.getNewDomainObjectForParam(AllEntriesReadStatusProxy) >> readStatusDomain

        when:
        simpleAllEntriesReadStatusHandler.markAllFeedsAsRead()

        then:
        1 * allEntriesReadStatusModifier.processRequest(readStatusDomain, _, Void)
        1 * readStatusDomain.setReadStatus(true)
    }

    def "Mark all entries as unread"() {
        given:
        def readStatusDomain = Mock(AllEntriesReadStatusProxy)
        allEntriesReadStatusModifier.getNewDomainObjectForParam(AllEntriesReadStatusProxy) >> readStatusDomain

        when:
        simpleAllEntriesReadStatusHandler.markAllFeedsAsUnread()

        then:
        1 * allEntriesReadStatusModifier.processRequest(readStatusDomain, _, Void)
        1 * readStatusDomain.setReadStatus(false)
    }

    def "Use event receiver when there was one provided"() {
        given:
        def receiver = Mock(Receiver)

        and:
        def readStatusDomain = Mock(AllEntriesReadStatusProxy)
        allEntriesReadStatusModifier.getNewDomainObjectForParam(AllEntriesReadStatusProxy) >> readStatusDomain

        when:
        simpleAllEntriesReadStatusHandler.setEventReceiver(receiver)

        and:
        simpleAllEntriesReadStatusHandler.markAllFeedsAsRead()

        then:
        1 * allEntriesReadStatusModifier.processRequest(_, receiver, Void)
    }
}
