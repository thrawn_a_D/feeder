class elasticsearch::install {
    $ELASTIC_SEARCH_PACKAGE_NAME="elasticsearch-0.90.6.noarch.rpm"
    $ELASTIC_SEARCH_INSTALL_SOURCE="https://download.elasticsearch.org/elasticsearch/elasticsearch/${ELASTIC_SEARCH_PACKAGE_NAME}"

    exec { "elasticsearch-source":
      command => "/usr/bin/wget -nc ${ELASTIC_SEARCH_INSTALL_SOURCE}",
      cwd => "/home/vagrant/",
    }


    package { "elasticsearch":
      ensure   => present,
      source   => "/home/vagrant/${ELASTIC_SEARCH_PACKAGE_NAME}",
      provider => rpm,
      require  => [Exec[elasticsearch-source],
                   Class[java]]
    }

    exec { "install-head-plugin":
      command => "/usr/share/elasticsearch/bin/plugin -install mobz/elasticsearch-head",
      require => Package[elasticsearch],
      returns => [0, 74]
    }

    exec { "install-transport-couchbase":
      command => "/usr/share/elasticsearch/bin/plugin -install transport-couchbase -url http://packages.couchbase.com.s3.amazonaws.com/releases/elastic-search-adapter/1.2.0/elasticsearch-transport-couchbase-1.2.0.zip",
      require => Package[elasticsearch],
      returns => [0, 74]
    }

}
