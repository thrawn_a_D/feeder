class elasticsearch::service {
    service {"elasticsearch":
        ensure => "running",
        enable => "true",
        require => Class[elasticsearch::install]
    }

    exec {"wait-for-elasticsearch-service":
          command => "/usr/bin/wget --spider --tries 10 --retry-connrefused --no-check-certificate http://localhost:9200",
          require => Service[elasticsearch],
    }

}
