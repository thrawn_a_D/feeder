class couchbase {
  include couchbase::install
  include couchbase::configure
  include couchbase::service
}
