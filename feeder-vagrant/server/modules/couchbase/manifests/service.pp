class couchbase::service {
  service { "couchbase-server":
        ensure => running,
        enable => true,
        hasstatus => true,
        require   => Class[couchbase::install]
  }

  exec {"wait-for-couchbase-service":
        command => "/usr/bin/wget --spider --tries 10 --retry-connrefused --no-check-certificate http://localhost:8091/index.html",
        require => Service[couchbase-server],
  }
}
