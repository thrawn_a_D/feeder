class couchbase::install {
    $COUCHBASE_SERVER_FILE="couchbase-server-community_x86_64_2.0.0.rpm"

    exec { "couchbase-server-source":
      command => "/usr/bin/wget -nc http://packages.couchbase.com/releases/2.0.0/${COUCHBASE_SERVER_FILE}",
      cwd => "/home/vagrant/",
    }

    package { "openssl098e":
        ensure => present,
    }

    package { "couchbase-server":
      ensure   => present,
      source   => "/home/vagrant/${COUCHBASE_SERVER_FILE}",
      provider => rpm,
      require  => Exec[couchbase-server-source]
    }
}
