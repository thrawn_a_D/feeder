class couchbase::configure {
    $COUCHBASE_ADMIN_USER="Administrator"
    $COUCHBASE_REST_PASSWORD="password"
    $COUCHBASE_DEFAULT_BUCKET_PASSWORD="123qweasd"
    $COUCHBASE_MAX_RAM_MB_PER_SERVER="1200"
    $COUCHBASE_MAX_RAM_MB_FOR_DEFAULT_BUCKET="300"
    $COUCHBASE_REPLICAS_FOR_DEFAULT_BUCKET="1"

    exec { "couchbase_init":
      command => template("couchbase/init_couchbase.erb"),
      require => [Package[couchbase-server],
                  Class[couchbase::service]]
    }

    define add_bucket ($BUCKET_NAME = $title,
                       $user,
                       $password) {
      exec { "couchbase_create_${BUCKET_NAME}":
        command => template("couchbase/create_bucket.erb"),
        unless  => "/opt/couchbase/bin/couchbase-cli bucket-list -c localhost:8091 -u ${user} -p ${password} | grep -e \"${BUCKET_NAME}$\"",
        require => [Package[couchbase-server],
                    Exec[couchbase_init],
                    Class[couchbase::service]]
      }
    }

    add_bucket {"feederFeed":
        user     => $COUCHBASE_ADMIN_USER,
        password => $COUCHBASE_REST_PASSWORD
    }
    add_bucket {"feederFeedAssoziation":
        user     => $COUCHBASE_ADMIN_USER,
        password => $COUCHBASE_REST_PASSWORD,
    }
    add_bucket {"feederUser":
        user     => $COUCHBASE_ADMIN_USER,
        password => $COUCHBASE_REST_PASSWORD,
    }
    add_bucket {"feederLoadStatistics":
        user     => $COUCHBASE_ADMIN_USER,
        password => $COUCHBASE_REST_PASSWORD,
    }

    exec { "disable-firewall":
      command => "/etc/init.d/iptables save && /etc/init.d/iptables stop && chkconfig iptables off"
    }

}
