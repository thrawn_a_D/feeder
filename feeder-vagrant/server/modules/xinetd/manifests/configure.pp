class xinetd::configure {
    file {"/etc/xinetd.d/redirect_jetty":
      source => "puppet:///modules/xinetd/redirect_jetty",
      notify => Service[xinetd],
      require => [Class[xinetd::install],
                  Class[jetty]]
    }

    service {"xinetd":
      ensure  => "running",
      enable  => "true",
      require => Class[xinetd::install],
    }
}
