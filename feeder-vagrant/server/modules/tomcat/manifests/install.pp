class tomcat::install {
  package {"tomcat6":
    ensure => latest,
  }

  package {"tomcat6-admin-webapps":
    ensure  => latest,
    require => Package[tomcat6],
  }
}
