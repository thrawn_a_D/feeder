class tomcat::configure {
  file {"/etc/tomcat6/tomcat-users.xml":
        source  => "puppet:///modules/tomcat/tomcat-users.xml",
        require => Class[tomcat::install],
  }
}
