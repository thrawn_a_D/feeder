class jetty::service {
  service {"jetty":
     ensure => running,
     enable => true,
     hasstatus => true,
     require => [Class[jetty::configure],
                 Class[jetty::install]]
  }
}
