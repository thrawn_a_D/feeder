class jetty::install {
    $JETTY_SERVER_FILE_NAME="jetty-distribution-9.1.0.v20131115"
    $JETTY_SERVER_FILE="${JETTY_SERVER_FILE_NAME}.tar.gz"

    exec { "jetty-server-source":
      command => "/usr/bin/wget -nc http://ftp.halifax.rwth-aachen.de/eclipse//jetty/stable-9/dist/${JETTY_SERVER_FILE}",
      cwd => "/home/vagrant/",
    }

    exec {"extract-jetty-source":
      command => "/bin/tar -zxvf /home/vagrant/${JETTY_SERVER_FILE} && mv ${JETTY_SERVER_FILE_NAME} /opt && ln -sf /opt/${JETTY_SERVER_FILE_NAME}/ /opt/jetty",
      unless  => "/usr/bin/test -d /opt/${JETTY_SERVER_FILE_NAME}",
      require => [Exec[jetty-server-source],
                  Class[java]]
    }

    user {"jetty":
      ensure => present,
      managehome => true,
    }

    file {"/opt/${JETTY_SERVER_FILE_NAME}":
      group   => "jetty",
      owner   => "jetty",
      recurse => "true",
      require => [User["jetty"],
                  Exec["extract-jetty-source"]]
    }

    file {"/etc/init.d/jetty":
      ensure => link,
      target => "/opt/jetty/bin/jetty.sh",
      require => File["/opt/${JETTY_SERVER_FILE_NAME}"]
    }
}
