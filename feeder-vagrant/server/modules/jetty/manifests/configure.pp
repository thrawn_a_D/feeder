class jetty::configure {

  add_env_variable{"JETTY_HOME":
    value => "/opt/jetty"
  }

  add_env_variable{"JETTY_USER":
    value => "jetty"
  }

  add_env_variable{"JETTY_ARGS":
    value => "jetty.port=8090"
  }

  add_env_variable{"JETTY_LOGS":
    value => "/opt/jetty/logs/"
  }

  define add_env_variable ($ENV_NAME = $title, $value) {
    exec { "$ENV_NAME":
        environment => ["${ENV_NAME}=${value}"],
        command => "/bin/echo \"${ENV_NAME}=${value}\" >> /etc/default/jetty",
        unless => "/bin/grep \"${ENV_NAME}=${value}\" /etc/default/jetty",
        require => Class[jetty::install]
    }
  }
}
