class activemq::service {
  service { "activemq":
        ensure => running,
        enable => true,
        hasstatus => true,
        require   => Class[activemq::configure]
  }
}
