class activemq {
    include activemq::install
    include activemq::configure
    include activemq::service
}
