class activemq::configure {
   require activemq::install

   file {"/etc/init.d/activemq":
        source => "puppet:///modules/activemq/activemq",
        mode => 744
   }

   file {"/etc/init.d/activemqstart.sh":
        source => "puppet:///modules/activemq/activemqstart.sh",
        mode => 744
   }

  file {"/etc/init.d/activemqstop.sh":
        source => "puppet:///modules/activemq/activemqstop.sh",
        mode => 744
   }
}
