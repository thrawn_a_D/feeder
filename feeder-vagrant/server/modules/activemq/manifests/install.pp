class activemq::install {
    $ACTIVEMQ_SERVER_FILE_NAME="apache-activemq-5.9.0"
    $ACTIVEMQ_SERVER_FILE="${ACTIVEMQ_SERVER_FILE_NAME}-bin.tar.gz"

    require java

    exec { "activemq-server-source":
      command => "/usr/bin/wget -nc http://ftp.heikorichter.name/pub/apache/activemq/apache-activemq/5.9.0/${ACTIVEMQ_SERVER_FILE}",
      cwd => "/home/vagrant/",
    }

    exec {"extract-activemq-source":
      command => "/bin/tar -zxvf /home/vagrant/${ACTIVEMQ_SERVER_FILE} && mv ${ACTIVEMQ_SERVER_FILE_NAME} /opt && ln -sf /opt/${ACTIVEMQ_SERVER_FILE_NAME}/ /opt/activemq",
      unless  => "/usr/bin/test -d /opt/${ACTIVEMQ_SERVER_FILE_NAME}",
      require => Exec[activemq-server-source],
    }

    user {"activemq":
      ensure => present,
      managehome => true,
    }

    file {"/opt/${ACTIVEMQ_SERVER_FILE_NAME}":
      group => "activemq",
      owner  => "activemq",
      require => [User["activemq"],
                  Exec["extract-activemq-source"]]
    }
}
