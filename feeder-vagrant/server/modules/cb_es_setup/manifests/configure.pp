class cb_es_setup::configure {

    exec {"restart_el":
      command => "/etc/init.d/elasticsearch restart",
      require => Class[elasticsearch],
    }

    exec {"wait-for-elasticsearch-restart":
          command => "/usr/bin/wget --spider --tries 10 --retry-connrefused --no-check-certificate http://localhost:9200",
          require => Exec[restart_el],
    }

    file {"/etc/elasticsearch":
          ensure => directory
    }

    #Add couchbase password
    file {"/etc/elasticsearch/elasticsearch.yml":
          source  => "puppet:///modules/cb_es_setup/elasticsearch.yml",
          require => File["/etc/elasticsearch"]
    }

    exec {"add_template":
        command => "/usr/bin/curl -XPUT http://localhost:9200/_template/couchbase -d @plugins/transport-couchbase/couchbase_template.json",
        require => [Exec[wait-for-elasticsearch-service],
                    Class[elasticsearch]]
    }

    exec {"set_max_replications":
        command => "/usr/bin/curl -X POST -u Administrator:password http://localhost:8091/internalSettings -d xdcrMaxConcurrentReps=8",
        require => [Exec[wait-for-elasticsearch-service],
                    Class[elasticsearch]]
    }

    add_index {"feeder-feed": }
    add_index {"feeder-feed-assoziation": }
    add_index {"feeder-feeds-load-statistics": }

    define add_index ($INDEX_NAME = $title) {
      exec {$INDEX_NAME:
          command => "/usr/bin/curl -XPUT http://localhost:9200/$INDEX_NAME",
          require => [Exec[wait-for-elasticsearch-service],
                      Class[elasticsearch]]
      }
    }

    define add_cluster ($cluster_name = $title,
                        $user_name,
                        $password) {
      exec {"add_couchbase_cluster":
          command => template("cb_es_setup/add_cluster.erb"),
          require => [Class[couchbase],
                      Exec[wait-for-elasticsearch-restart]]
      }
    }

    add_cluster {"ElasticSearch":
      user_name => "Administrator",
      password  => "password"
    }

    define add_replication ($bucket_name = $title,
                            $cluster_name,
                            $index_name,
                            $user_name,
                            $password) {
      exec {"add_replication_${bucket_name}":
          command => template("cb_es_setup/add_replication.erb"),
          require => [Class[couchbase],
                      Exec[wait-for-elasticsearch-restart]]
      }
    }

    add_replication {"feederFeed":
      cluster_name => "ElasticSearch",
      index_name   => "feeder-feed",
      user_name    => "Administrator",
      password    => "password",
    }

    add_replication {"feederFeedAssoziation":
      cluster_name => "ElasticSearch",
      index_name   => "feeder-feed-assoziation",
      user_name    => "Administrator",
      password    => "password",
    }

    add_replication {"feederLoadStatistics":
      cluster_name => "ElasticSearch",
      index_name   => "feeder-feeds-load-statistics",
      user_name    => "Administrator",
      password    => "password",
    }
}
