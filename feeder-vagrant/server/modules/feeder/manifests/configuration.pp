class feeder::configuration {
    file {"/etc/feeder":
          ensure => directory
    }

    define copy_configuration ($config_file_name = $title) {
        file {"/etc/feeder/${config_file_name}":
              source  => "puppet:///modules/feeder/${config_file_name}",
              require => File["/etc/feeder"]
        }
    }

    copy_configuration {"feeder_usecase.properties": }
    copy_configuration {"update_manager.properties": }
    copy_configuration {"update_receiver.properties": }
    copy_configuration {"update_service.properties": }
    copy_configuration {"user_usecase.properties": }

    file {"/var/log/feeder":
      ensure => directory,
      owner  => tomcat,
      group  => tomcat,
      require => Class[tomcat]
    }

    file {["/var/log/feeder/feeder-usecase-feeds.log",
           "/var/log/feeder/feeder-usecase-user.log",
           "/var/log/feeder/feeder-updater-manager.log",
           "/var/log/feeder/feeder-updater-receiver.log",
           "/var/log/feeder/feeder-updater-service-endpoint.log"]:
      ensure => file,
      owner  => tomcat,
      group  => tomcat,
      require => Class[tomcat]
    }


    file {"/etc/tomcat6/context.xml":
      source  => "puppet:///modules/feeder/context.xml",
      require => Class[tomcat]
    }
}
