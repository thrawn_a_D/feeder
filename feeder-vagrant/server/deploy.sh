#!/bin/bash

cd bin
cp ../../../feeder-gui/target/feeder-gui-0.0.1-SNAPSHOT.war feeder-gui.war
cp ../../../feeder-processor/production/updater/feeder-update-service-endpoint/feeder-update-service-endpoint-webapp/target/update-service-endpoint.war update-service-endpoint.war
cp ../../../feeder-processor/production/updater/feeder-update-receiver/feeder-update-receiver-webapp/target/update-receiver.war update-receiver.war
cp ../../../feeder-processor/production/updater/feeder-update-manager/feeder-update-manager-webapp/target/update-manager.war update-manager.war
cp ../../../feeder-processor/production/execution/tomcat/user-handling-tomcat-execution/target/user-handling.war user-handling.war
cp ../../../feeder-processor/production/execution/tomcat/feeder-handling-tomcat-execution/target/feeder-handling.war feeder-handling.war
cd -
