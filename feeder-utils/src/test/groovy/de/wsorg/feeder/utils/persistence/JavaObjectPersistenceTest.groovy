package de.wsorg.feeder.utils.persistence

import de.wsorg.feeder.utils.hashing.HashCreator
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 21.09.12
 */
class JavaObjectPersistenceTest extends Specification {

    private static final String SYSTEM_TMP_FOLDER=System.getProperty("java.io.tmpdir") + File.separator + "feederStorage" + File.separator

    private final storageFolderName = "someFolderName"
    private final fileNameToUse = "asdkjasdkjn"
    private final expectFilePath = SYSTEM_TMP_FOLDER + File.separator + storageFolderName + File.separator + fileNameToUse;

    JavaObjectPersistence<Date> javaObjectStorage
    HashCreator hashCreator

    def setup(){
        javaObjectStorage = new JavaObjectPersistence<Date>()
        hashCreator = Mock(HashCreator)
        javaObjectStorage.hashCreator = hashCreator

        removeTetsFile()
    }

    def "Store a feed model in file"() {
        given:
        Date someObjectToSave = new Date()

        when:
        javaObjectStorage.save(storageFolderName, fileNameToUse, someObjectToSave)

        then:
        1 * hashCreator.getHashOfString(fileNameToUse) >> fileNameToUse
        new File(expectFilePath).exists()
    }

    def "Read object from file"() {
        given:
        prepareTestFile()

        and:
        def expectedFilePath = storageFolderName

        when:
        def loadedFeedModel = javaObjectStorage.getModelByFilePath(expectedFilePath, fileNameToUse)

        then:
        loadedFeedModel.getDate() == 8
        1 * hashCreator.getHashOfString(fileNameToUse) >> fileNameToUse
    }

    def "Test storage folder contains a final slash"() {
        when:
        def storageFolder = javaObjectStorage.storageFolder

        then:
        storageFolder == SYSTEM_TMP_FOLDER
        storageFolder.endsWith(File.separator)
    }

    def "Get all objects in a directory"() {
        given:
        prepareTestFile()
        def directory = storageFolderName

        when:
        def allFolderContent = javaObjectStorage.getFolderContent(directory)

        then:
        allFolderContent.size() > 0
    }

    def "Try to load a folder content but provide a file path"() {
        given:
        prepareTestFile()
        def expectedFilePath = storageFolderName + File.separator + fileNameToUse


        when:
        javaObjectStorage.getFolderContent(expectedFilePath)

        then:
        def ex = thrown(IllegalArgumentException)
        ex.message == "The provided path leads to a file instead of a folder"

    }

    def "Remove file from storage folder"() {
        given:
        prepareTestFile()


        when:
        javaObjectStorage.remove(storageFolderName, fileNameToUse)

        then:
        1 * hashCreator.getHashOfString(fileNameToUse) >> fileNameToUse
        new File(expectFilePath).exists() == false
    }

    private void prepareTestFile(){
        ensureFeederTempFolderExists()
        URL filePath = this.getClass().getResource("/dataAccess/asdkjasdkjn")
        def fileDestination = new File(expectFilePath)
        fileDestination.getParentFile().mkdirs()
        fileDestination << new File(filePath.toURI()).bytes
    }

    private void ensureFeederTempFolderExists() {
        new File(SYSTEM_TMP_FOLDER).mkdirs()
    }

    private void removeTetsFile() {
        def ourTestFile = new File(expectFilePath)
        if (ourTestFile.exists())
            ourTestFile.delete();
    }
}
