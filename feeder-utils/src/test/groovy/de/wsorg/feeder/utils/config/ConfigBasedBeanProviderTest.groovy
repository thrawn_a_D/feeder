package de.wsorg.feeder.utils.config;

import spock.lang.Specification;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 20.04.13
 */
public class ConfigBasedBeanProviderTest extends Specification {
    ConfigBasedBeanProvider configBasedBeanProvider

    def testObjectWhenTrue = 'true'
    def testObjectWhenFalse = 'false'

    def setup(){
        configBasedBeanProvider = new ConfigBasedBeanProvider()

        configBasedBeanProvider.provideObjectWhenPropertyIsTrue = testObjectWhenTrue
        configBasedBeanProvider.provideObjectWhenPropertyIsFalse = testObjectWhenFalse
    }

    def "Get object when property is true"() {
        given:
        configBasedBeanProvider.propertyToDetermineBean = 'true'

        when:
        def result = configBasedBeanProvider.getObjectToUse()

        then:
        result == testObjectWhenTrue
    }

    def "Get object when property is false"() {
        given:
        configBasedBeanProvider.propertyToDetermineBean = 'false'

        when:
        def result = configBasedBeanProvider.getObjectToUse()

        then:
        result == testObjectWhenFalse
    }

    def "Do not provide a property, return false object"() {
        when:
        def result = configBasedBeanProvider.getObjectToUse()

        then:
        result == testObjectWhenFalse
    }

    def "Do not provide an invalid property, return false object"() {
        when:
        def result = configBasedBeanProvider.getObjectToUse()

        then:
        result == testObjectWhenFalse
    }
}
