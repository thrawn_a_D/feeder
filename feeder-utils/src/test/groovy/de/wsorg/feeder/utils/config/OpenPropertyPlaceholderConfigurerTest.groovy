package de.wsorg.feeder.utils.config

import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult
import de.wsorg.feeder.utils.config.verification.ConfigVerifier
import de.wsorg.feeder.utils.config.verification.PropertyVerificationLogger
import org.springframework.core.io.Resource
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 29.03.13
 */
class OpenPropertyPlaceholderConfigurerTest extends Specification {
    OpenPropertyPlaceholderConfigurer openPropertyPlaceholderConfigurer
    PropertyVerificationLogger propertyVerificationLogger

    def setup(){
        openPropertyPlaceholderConfigurer = new OpenPropertyPlaceholderConfigurer()
        propertyVerificationLogger = Mock(PropertyVerificationLogger)
        openPropertyPlaceholderConfigurer.propertyVerificationLogger = propertyVerificationLogger
    }

    def "Validate properties, when some properties are provided"() {
        given:
        def properties = Mock(Properties)
        def verifier = Mock(ConfigVerifier)
        def validationResult = new ConfigVerificationResult('asd')

        when:
        openPropertyPlaceholderConfigurer.mergedProperties = properties
        openPropertyPlaceholderConfigurer.setConfigVerifier([verifier])

        then:
        1 * verifier.verifyConfig(_, properties) >> validationResult
    }

    def "Write result to log"() {
        given:
        def properties = Mock(Properties)
        def verifier = Mock(ConfigVerifier)
        def validationResult = new ConfigVerificationResult('asd')

        when:
        openPropertyPlaceholderConfigurer.mergedProperties = properties
        openPropertyPlaceholderConfigurer.setConfigVerifier([verifier])

        then:
        1 * verifier.verifyConfig(_, properties) >> validationResult
        1 * propertyVerificationLogger.logVerificationResult(validationResult)
    }

    def "Set location of property file"() {
        given:
        def filePath = '.'
        def propertyFile = new File(filePath)
        def resource = Mock(Resource)

        and:
        resource.getFile() >> propertyFile

        when:
        openPropertyPlaceholderConfigurer.setLocation(resource)

        then:
        openPropertyPlaceholderConfigurer.propertyLocation[0] == filePath
    }

    def "Set an array of locations"() {
        given:
        def filePath1 = '.'
        def filePath2 = '.'
        def propertyFile1 = new File(filePath1)
        def propertyFile2 = new File(filePath1)
        def resource1 = Mock(Resource)
        def resource2 = Mock(Resource)

        and:
        resource1.getFile() >> propertyFile1
        resource2.getFile() >> propertyFile2

        when:
        openPropertyPlaceholderConfigurer.setLocations([resource1, resource2] as Resource[])

        then:
        openPropertyPlaceholderConfigurer.propertyLocation[0] == filePath1
        openPropertyPlaceholderConfigurer.propertyLocation[1] == filePath2
    }

    def "Provided locations are concatenated and delegated to validation"() {
        given:
        def location1 = 'kjbkhb'
        def location2 = 'asdoiudsnf83'

        and:
        openPropertyPlaceholderConfigurer.propertyLocation = [location1, location2] as String[]

        and:
        def properties = Mock(Properties)
        def verifier = Mock(ConfigVerifier)
        def validationResult = new ConfigVerificationResult('asd')

        when:
        openPropertyPlaceholderConfigurer.mergedProperties = properties
        openPropertyPlaceholderConfigurer.setConfigVerifier([verifier])

        then:
        1 * verifier.verifyConfig(openPropertyPlaceholderConfigurer.propertyLocation, properties) >> validationResult
    }

    def "Properties are invalid, throw an exception"() {
        given:
        def properties = Mock(Properties)
        def verifier = Mock(ConfigVerifier)
        def validationResult = new ConfigVerificationResult('asd')

        and:
        validationResult.propertyErrors = [bla:'asd']

        when:
        openPropertyPlaceholderConfigurer.mergedProperties = properties
        openPropertyPlaceholderConfigurer.setConfigVerifier([verifier])

        then:
        1 * verifier.verifyConfig(_, properties) >> validationResult
        1 * propertyVerificationLogger.logVerificationResult(validationResult)
        def ex = thrown(IllegalArgumentException)
        ex.message == 'The provided configuration contains some errors. See Error log for more information.'
    }
}
