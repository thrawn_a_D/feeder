package de.wsorg.feeder.utils;


import de.wsorg.feeder.utils.hashing.HashCreator
import de.wsorg.feeder.utils.persistence.FileBasedPersistence
import de.wsorg.feeder.utils.xml.JaxbMarshaller
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller
import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 07.08.12
 */
public class UtilsFactoryTest extends Specification {

    def "Initialize a jaxbMarshaller"() {

        when:
        JaxbMarshaller marshaller = UtilsFactory.getJaxbMarshaller()

        then:
        marshaller != null
    }

    def "Initialize a jaxbUnmarshaller"() {

        when:
        JaxbUnmarshaller unmarshaller = UtilsFactory.getJaxbUnmarshaller()

        then:
        unmarshaller != null
    }

    def "Initialize an hash creator"() {
        when:
        HashCreator hashCreator = UtilsFactory.getHashCreator()

        then:
        hashCreator != null
    }

    def "Create file based persistance object"() {
        when:
        FileBasedPersistence persistence = UtilsFactory.getFileBasedPersistence()

        then:
        persistence != null
    }
}
