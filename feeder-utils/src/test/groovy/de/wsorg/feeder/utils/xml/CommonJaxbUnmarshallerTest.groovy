package de.wsorg.feeder.utils.xml;


import spock.lang.Specification

import javax.xml.bind.Unmarshaller

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.07.12
 */
public class CommonJaxbUnmarshallerTest extends Specification {

    JaxbUnmarshaller feederUnmarshaller
    Unmarshaller unmarshaller


    def setup(){
        feederUnmarshaller = new CommonJaxbUnmarshaller();
        unmarshaller = Mock(Unmarshaller)
        feederUnmarshaller.unmarshaller = unmarshaller
    }

    def "Unmarshall an xml"() {
        given:
        final String someXml='<bla></bla>'
        def testResultObject = new Object()

        when:
        def result = feederUnmarshaller.unmarshal(someXml)

        then:
        result != null
        result == testResultObject
        1 * unmarshaller.unmarshal({InputStream xmlStream->xmlStream.bytes==someXml.bytes})>> testResultObject
    }
}
