package de.wsorg.feeder.utils.hashing;


import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 31.08.12
 */
public class Md5HashCreatorTest extends Specification {
    Md5HashCreator md5HashCreator

    def setup(){
        md5HashCreator = new Md5HashCreator()
    }

    def "Hash a string to md5"() {
        given:
        def stringToHash="abcde"

        when:
        def hashedResult = md5HashCreator.getHashOfString(stringToHash)

        then:
        hashedResult.size() > 0
        hashedResult != stringToHash
    }

    def "Hash creation is repeatable"() {
        given:
        def stringToHash="abcde"

        when:
        def firstHashedResult = md5HashCreator.getHashOfString(stringToHash)
        def secondHashedResult = md5HashCreator.getHashOfString(stringToHash)

        then:
        firstHashedResult == secondHashedResult
    }

    def "Two different strings create different hash values"() {
        given:
        def firstStringToHash="abc"
        def secondStringToHash="cba"

        when:
        def firstHashedResult = md5HashCreator.getHashOfString(firstStringToHash)
        def secondHashedResult = md5HashCreator.getHashOfString(secondStringToHash)

        then:
        firstHashedResult != secondHashedResult
    }
}
