package de.wsorg.feeder.utils.xml;


import spock.lang.Specification

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.08.12
 */
public class CommonJaxbHandlerTest extends Specification {

    JaxbHandler jaxbHandler

    def setup(){
        jaxbHandler = new JaxbHandler()
    }

    def "Check resetting of package name is lead up to concatenated packages and reinitialization of jaxb"() {
        given:
        def packageName = "bla"
        def anotherPackageName = "blub"

        when:
        jaxbHandler.setJaxbGeneratedClassesPackage(packageName)
        jaxbHandler.setJaxbGeneratedClassesPackage(anotherPackageName)

        then:
        jaxbHandler.jaxbGeneratedClassesPackage == packageName + ":" + anotherPackageName
    }

    private static final class JaxbHandler extends CommonJaxbHandler {
        @Override
        protected void initializeHandler() {

        }
    }
}
