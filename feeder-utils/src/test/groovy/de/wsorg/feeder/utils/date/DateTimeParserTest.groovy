package de.wsorg.feeder.utils.date;


import spock.lang.Specification
import spock.lang.Unroll

import java.text.ParseException

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 04.07.12
 */
public class DateTimeParserTest extends Specification {

    @Unroll
    def "Parse date #dateToParse based on RFC 822"(){
        when:
        Date result = DateTimeParser.parse(dateToParse);

        then:
        result != null

        where:
        dateToParse << [
            'Sun, 22 Jan 2012 01:44:51 +0000',
            'Wed, 04 Jul 2012 11:12:16 +0000',
            'Fri, 06 Jul 2012 06:20:00 GMT',
            'Fri, 06 Jul 2012 08:41:14 +0200'
        ]
    }

    @Unroll
    def "Parse date #dateToParse as alternative"(){
        when:
        Date result = DateTimeParser.parse(dateToParse);

        then:
        result != null

        where:
        dateToParse << [
                'Mon, 31 Dec 2012 20:12:00 CST'
        ]
    }
}
