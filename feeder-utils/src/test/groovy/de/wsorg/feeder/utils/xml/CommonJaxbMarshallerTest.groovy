package de.wsorg.feeder.utils.xml;


import spock.lang.Specification

import javax.xml.bind.Marshaller

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 * User: wschneider
 * Date: 16.07.12
 */
public class CommonJaxbMarshallerTest extends Specification {

    JaxbMarshaller jaxbMarshaller
    Marshaller marshaller


    def setup(){
        jaxbMarshaller = new CommonJaxbMarshaller();
        marshaller = Mock(Marshaller)
        jaxbMarshaller.marshaller = marshaller
    }

    def "Unmarshall an xml"() {
        given:
        def someObjectToMarshall = new Object()

        when:
        def result = jaxbMarshaller.marshal(someObjectToMarshall)

        then:
        result != null
        1 * marshaller.marshal({it == someObjectToMarshall}, _)
    }
}
