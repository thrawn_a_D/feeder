package de.wsorg.feeder.utils;

import de.wsorg.feeder.utils.hashing.HashCreator;
import de.wsorg.feeder.utils.hashing.Md5HashCreator;
import de.wsorg.feeder.utils.persistence.FileBasedPersistence;
import de.wsorg.feeder.utils.persistence.JavaObjectPersistence;
import de.wsorg.feeder.utils.xml.CommonJaxbMarshaller;
import de.wsorg.feeder.utils.xml.CommonJaxbUnmarshaller;
import de.wsorg.feeder.utils.xml.JaxbMarshaller;
import de.wsorg.feeder.utils.xml.JaxbUnmarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UtilsFactory {

    public static JaxbMarshaller getJaxbMarshaller(){
        return new CommonJaxbMarshaller();
    }

    public static JaxbUnmarshaller getJaxbUnmarshaller(){
        return new CommonJaxbUnmarshaller();
    }

    public static HashCreator getHashCreator(){
        return new Md5HashCreator();
    }

    public static FileBasedPersistence getFileBasedPersistence() {
        return new JavaObjectPersistence();
    }
}
