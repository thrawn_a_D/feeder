package de.wsorg.feeder.utils.wrapper;

import java.util.UUID;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class UUIDWrapper {
    public String randomUUID(){
        return UUID.randomUUID().toString();
    }
}
