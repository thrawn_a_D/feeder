package de.wsorg.feeder.utils.config.verification;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class PropertyVerificationLogger {
    public void logVerificationResult(final ConfigVerificationResult configVerificationResult){
        if(configVerificationResult.isConfigErroneous()) {
            logConfigErrors(configVerificationResult);
        } else {
            PropertyVerificationLogger.log.info("################## PROVIDED CONFIG {} IS VALID ##########################", configVerificationResult.getConfigLocation());
        }
    }

    private void logConfigErrors(final ConfigVerificationResult configVerificationResult) {
        PropertyVerificationLogger.log.error("########################## CONFIG VALIDATION ERRONEOUS ####################");
        PropertyVerificationLogger.log.error("### Config {} contains errors: #######", configVerificationResult.getConfigLocation());

        final Map<String,List<String>> propertyErrors = configVerificationResult.getPropertyErrors();
        for (final String propertyKey : propertyErrors.keySet()) {
            final List<String> propertyError = propertyErrors.get(propertyKey);

            for (String error : propertyError) {
                PropertyVerificationLogger.log.error("### {} : {} ######", propertyKey, error);
            }
        }

        PropertyVerificationLogger.log.error("##############################################################################");
    }
}
