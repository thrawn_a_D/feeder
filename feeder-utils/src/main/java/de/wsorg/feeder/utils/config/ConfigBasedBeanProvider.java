package de.wsorg.feeder.utils.config;

import lombok.Data;
import org.apache.commons.lang.BooleanUtils;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class ConfigBasedBeanProvider<T> {

    private T provideObjectWhenPropertyIsTrue;
    private T provideObjectWhenPropertyIsFalse;

    private String propertyToDetermineBean;

    public T getObjectToUse() {
        if (BooleanUtils.toBoolean(propertyToDetermineBean)) {
            return provideObjectWhenPropertyIsTrue;
        } else {
            return provideObjectWhenPropertyIsFalse;
        }
    }
}
