package de.wsorg.feeder.utils.wrapper;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class DigestUtilsWrapper {
    public String md5Hex(final String stringToEncode) {
        return DigestUtils.md5Hex(stringToEncode);
    }
}
