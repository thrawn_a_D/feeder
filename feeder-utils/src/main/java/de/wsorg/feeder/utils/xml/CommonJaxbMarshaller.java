package de.wsorg.feeder.utils.xml;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class CommonJaxbMarshaller extends CommonJaxbHandler implements JaxbMarshaller{
    private Marshaller marshaller;
    private JaxbMarshallCreator jaxbMarshallCreator;

    {
        jaxbMarshallCreator = new JaxbMarshallCreator();
    }

    @Override
    public <T> String marshal(final T objectToMarshall) {
        try {
            StringWriter marshallResultWriter = new StringWriter();

            Marshaller marshaller = getMarshaller();
            marshaller.marshal(objectToMarshall, marshallResultWriter);
            return marshallResultWriter.toString();
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    private Marshaller getMarshaller() {
        if(marshaller==null){
            initializeHandler();
        }
        return marshaller;
    }

    @Override
    protected void initializeHandler() {
        try {
            this.marshaller = jaxbMarshallCreator.createMarshaller(this.jaxbGeneratedClassesPackage);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
