package de.wsorg.feeder.utils.config;

import de.wsorg.feeder.utils.config.verification.ConfigVerificationResult;
import de.wsorg.feeder.utils.config.verification.ConfigVerifier;
import de.wsorg.feeder.utils.config.verification.PropertyVerificationLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Slf4j
public class OpenPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    private PropertyVerificationLogger propertyVerificationLogger;

    Properties mergedProperties;
    private String[] propertyLocation;

    public Properties getMergedProperties() throws IOException {
        if (mergedProperties == null) {

            mergedProperties = mergeProperties();

        }
        return mergedProperties;

    }

    @Override
    public void setLocation(final Resource location) {
        super.setLocation(location);
        propertyLocation = new String[1];
        setPropertyLocation(location, 0);
    }

    private void setPropertyLocation(final Resource location, final int index) {
        try {
            propertyLocation[index] = location.getFile().getPath();
        } catch (IOException e) {
            OpenPropertyPlaceholderConfigurer.log.error("Could not determent property location.");
        }
    }

    @Override
    public void setLocations(final Resource[] locations) {
        super.setLocations(locations);

        propertyLocation = new String[locations.length];

        int index = 0;
        for (Resource location : locations) {
            setPropertyLocation(location, index);
            index++;
        }
    }

    public void setConfigVerifier(final List<ConfigVerifier> configVerifier) throws IOException {
        for (ConfigVerifier verifier : configVerifier) {
            final ConfigVerificationResult verificationResult = verifier.verifyConfig(propertyLocation, getMergedProperties());
            getPropertyVerificationLogger().logVerificationResult(verificationResult);
            throwExceptionIfConfigHasErrors(verificationResult);
        }
    }

    private void throwExceptionIfConfigHasErrors(final ConfigVerificationResult verificationResult) {
        if(verificationResult.isConfigErroneous()) {
            final String errorMessage = "The provided configuration contains some errors. See Error log for more information.";
            throw new IllegalArgumentException(errorMessage);
        }
    }

    private PropertyVerificationLogger getPropertyVerificationLogger() {
        if(propertyVerificationLogger == null) {
            propertyVerificationLogger = new PropertyVerificationLogger();
        }

        return propertyVerificationLogger;
    }
}
