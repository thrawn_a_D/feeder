package de.wsorg.feeder.utils.xml;

import javax.xml.bind.JAXBException;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface JaxbUnmarshaller extends JaxbHandler {
    Object unmarshal(final String xmlToUnmarshall) throws JAXBException;
}
