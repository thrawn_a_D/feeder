package de.wsorg.feeder.utils.config.verification;

import java.util.Properties;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface ConfigVerifier {
    public ConfigVerificationResult verifyConfig(final String[] configurationFilePath, final Properties configProperties);
}
