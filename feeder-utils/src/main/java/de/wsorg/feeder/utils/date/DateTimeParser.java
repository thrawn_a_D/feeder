package de.wsorg.feeder.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class DateTimeParser {
    private static final String DEFAULT_DATE_FORMAT = "EEE, d MMM yyyy HH:mm:ss z";
    private static final String ALTERNATIVE_DATE_FORMAT = "E, d M yyyy HH:mm:ss z";
    private static Locale defaultLocale = Locale.US;

    private static SimpleDateFormat dateFormat;
    private static SimpleDateFormat dateAlternateFormat;

    static {
        dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, defaultLocale);
        dateAlternateFormat = new SimpleDateFormat(ALTERNATIVE_DATE_FORMAT, defaultLocale);
    }

    public static Date parse(final String dateToParse){
        try {
            return dateFormat.parse(dateToParse);
        } catch (ParseException e) {
            try {
                return dateAlternateFormat.parse(dateToParse);
            } catch (ParseException e1) {
                //DO A LOG ENTRY
                return null;
            }
        }
    }

    public static String parse(final Date dateToParse){
        if(dateToParse != null)
            return dateFormat.format(dateToParse);
        else
            return "";
    }
}
