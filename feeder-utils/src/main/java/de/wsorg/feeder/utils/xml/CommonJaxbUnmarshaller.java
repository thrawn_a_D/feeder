package de.wsorg.feeder.utils.xml;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class CommonJaxbUnmarshaller extends CommonJaxbHandler implements JaxbUnmarshaller {

    private javax.xml.bind.Unmarshaller unmarshaller;
    private JaxbMarshallCreator jaxbMarshallCreator;

    {
        jaxbMarshallCreator = new JaxbMarshallCreator();
    }

    @Override
    public Object unmarshal(final String xmlToUnmarshall) throws JAXBException {
        InputStream xmlStream=new ByteArrayInputStream(xmlToUnmarshall.getBytes());
        return getUnmarshaller().unmarshal(xmlStream);
    }

    private javax.xml.bind.Unmarshaller getUnmarshaller() {
        if(unmarshaller==null){
            initializeHandler();
        }
        return unmarshaller;
    }

    @Override
    protected void initializeHandler() {
        try {
            this.unmarshaller = jaxbMarshallCreator.createUnmarshaller(this.jaxbGeneratedClassesPackage);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }
}
