package de.wsorg.feeder.utils.xml;

import org.apache.commons.lang.StringUtils;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public abstract class CommonJaxbHandler {
    protected String jaxbGeneratedClassesPackage;

    protected abstract void initializeHandler();

    public void setJaxbGeneratedClassesPackage(final String jaxbGeneratedClassesPackage) {
        if(StringUtils.isNotBlank(this.jaxbGeneratedClassesPackage)){
            if(!this.jaxbGeneratedClassesPackage.contains(jaxbGeneratedClassesPackage))
                this.jaxbGeneratedClassesPackage += ":" + jaxbGeneratedClassesPackage;
        } else {
            this.jaxbGeneratedClassesPackage = jaxbGeneratedClassesPackage;
        }

        this.initializeHandler();
    }
}
