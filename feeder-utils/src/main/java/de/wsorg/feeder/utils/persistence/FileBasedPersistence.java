package de.wsorg.feeder.utils.persistence;

import java.util.List;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface FileBasedPersistence<T> {
    void save(final String storageFolderName, final String identifier, final T model);
    T getModelByFilePath(final String parentFolder, final String identifier);
    String getStorageFolder();
    List<T> getFolderContent(final String folderPath);
    void remove(final String storageFolderName, final String identifier);
}
