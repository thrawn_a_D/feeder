package de.wsorg.feeder.utils.hashing;

import java.math.BigInteger;
import java.security.MessageDigest;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class Md5HashCreator implements HashCreator {
    @Override
    public String getHashOfString(final String valueToHash) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.update(valueToHash.getBytes(), 0, valueToHash.length());
            BigInteger i = new BigInteger(1,m.digest());
            return String.format("%1$032x", i);
        } catch (java.security.NoSuchAlgorithmException ignored) {
        }
        return null;
    }
}
