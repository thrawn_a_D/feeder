package de.wsorg.feeder.utils.xml;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface JaxbMarshaller extends JaxbHandler {
    <T> String marshal(final T objectToMarshall);
}
