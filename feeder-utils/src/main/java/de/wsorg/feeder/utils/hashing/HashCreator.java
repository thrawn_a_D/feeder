package de.wsorg.feeder.utils.hashing;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public interface HashCreator {
    String getHashOfString(final String valueToHash);
}
