package de.wsorg.feeder.utils.config.verification;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
@Data
public class ConfigVerificationResult {
    private final String[] configLocation;
    private Map<String, List<String>> propertyErrors = new HashMap<>();

    public boolean isConfigErroneous() {
        return !propertyErrors.isEmpty();
    }

    public void addVerificationError(final String propertyName, final String errorMessage) {
        List<String> errorsList = getErrorsList(propertyName, errorMessage);
        propertyErrors.put(propertyName, errorsList);
    }

    private List<String> getErrorsList(final String propertyName, final String errorMessage) {
        List<String> errorsList = propertyErrors.get(propertyName);

        if(errorsList == null)
            errorsList = new ArrayList<>();

        errorsList.add(errorMessage);
        return errorsList;
    }
}
