package de.wsorg.feeder.utils.persistence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import de.wsorg.feeder.utils.UtilsFactory;
import de.wsorg.feeder.utils.hashing.HashCreator;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JavaObjectPersistence<T> implements FileBasedPersistence<T> {
    private static final String SYSTEM_TMP_FOLDER=System.getProperty("java.io.tmpdir");
    private static final String FEEDER_STORAGE_FOLDER = SYSTEM_TMP_FOLDER + File.separator + "feederStorage/";

    private HashCreator hashCreator;

    @Override
    public void save(final String storageFolderName, final String identifier, final T model) {
        final String fileName = getHashCreator().getHashOfString(identifier);

        try {
            final String filePath = getFilePath(storageFolderName, fileName);
            FileOutputStream fos = new FileOutputStream(filePath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(model);
            oos.flush();
            oos.close();
        } catch (IOException e) {
            throw new RuntimeException("Some error occurred while storing the model to file.", e);
        }
    }

    @Override
    public T getModelByFilePath(final String parentFolder, final String identifier) {
        final String expectedFileName = getHashCreator().getHashOfString(identifier);
        return getFileByHashedName(getStorageFolder() + parentFolder, expectedFileName);
    }

    private T getFileByHashedName(final String parentFolder, final String expectedFileName) {
        File fileToLoad = new File(parentFolder, expectedFileName);
        if(fileToLoad.exists()){
            try {
                FileInputStream fis = new FileInputStream(fileToLoad);
                ObjectInputStream ois = new ObjectInputStream(fis);

                T feed = (T) ois.readObject();

                ois.close();
                return feed;
            } catch (Exception e) {
                throw new RuntimeException("Some error occurred while loading the model from file.", e);
            }
        } else {
            return null;
        }
    }

    @Override
    public List<T> getFolderContent(final String folderPath) {
        List<T> folderContentResult = new ArrayList<T>();
        final File folderWithContentToLoad = new File(getStorageFolder() + folderPath);

        if(folderWithContentToLoad.exists()){
            if(folderWithContentToLoad.isDirectory()){
                for (File currentFileToLoad : folderWithContentToLoad.listFiles()) {
                    T loadedResult = getFileByHashedName(currentFileToLoad.getParent(), currentFileToLoad.getName());
                    folderContentResult.add(loadedResult);
                }
            } else {
                throw new IllegalArgumentException("The provided path leads to a file instead of a folder");
            }
        }

        return folderContentResult;
    }

    @Override
    public void remove(final String storageFolderName, final String identifier) {
        final String fileName = getHashCreator().getHashOfString(identifier);

        final String filePath = getFilePath(storageFolderName, fileName);
        final File fileToRemove = new File(filePath);

        if(fileToRemove.exists()) {
            System.out.printf("Löschen fertig");
            fileToRemove.delete();
        }
    }

    private String getFilePath(final String storageFolderName, final String fileName) {
        String storageFolder = getStorageFolder() + File.separator + storageFolderName;

        File storage = new File(storageFolder);
        storage.mkdirs();

        return new File(storageFolder, fileName).getAbsolutePath();
    }

    public String getStorageFolder(){
        final String storageFolder = FEEDER_STORAGE_FOLDER;

        File storage = new File(storageFolder);
        storage.mkdirs();

        return storageFolder;
    }

    private HashCreator getHashCreator() {
        if(this.hashCreator == null){
            this.hashCreator = UtilsFactory.getHashCreator();
        }

        return hashCreator;
    }
}
