package de.wsorg.feeder.utils.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Copyright (C) 2010-2012 Schneider Waldemar
 * This file is part of the feeder project.
 *
 *
 *
 */
public class JaxbMarshallCreator {
    public Marshaller createMarshaller(final String jaxbGeneratedClassesPackage) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(jaxbGeneratedClassesPackage);
        return jaxbContext.createMarshaller();
    }

    public Unmarshaller createUnmarshaller(final String jaxbGeneratedClassesPackage) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(jaxbGeneratedClassesPackage);
        return jaxbContext.createUnmarshaller();
    }
}
